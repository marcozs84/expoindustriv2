<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::auth();

Route::group([
	'prefix' => 'resource',
], function(){

	$resource_actions = [
		'as' => 'resource',
		'only' => [
			'store',
			'update',
			'destroy'
		]
	];

	Route::post(	'actividad/ds',				[	'uses' => 'Resources\ActividadController@ds',					'as' => 'resource.actividad.ds'				]);
	Route::post(	'actividad/{id}/comentarios',		[	'uses' => 'Resources\ActividadController@getComentarios',		'as' => 'resource.actividad.getComentarios'	]);
	Route::get(		'actividad/{id}/comentarios',		[	'uses' => 'Resources\ActividadController@getComentarios',		'as' => 'resource.actividad.getComentarios'	]);
	Route::post(	'actividad/{id}/addComentario',	[	'uses' => 'Resources\ActividadController@addComentario',		'as' => 'resource.actividad.addComentario'	]);
	Route::resource('actividad',					'Resources\ActividadController', 						$resource_actions);

	Route::post(	'actividadTipo/ds',			[	'uses' => 'Resources\ActividadTipoController@ds',				'as' => 'resource.actividadTipo.ds'			]);
	Route::resource('actividadTipo',				'Resources\ActividadTipoController',					$resource_actions);

	Route::post(	'anuncio/ds',				[	'uses' => 'Resources\AnuncioController@ds',						'as' => 'resource.anuncio.ds'				]);
	Route::resource('anuncio',					'Resources\AnuncioController',							$resource_actions);


	Route::post(	'archivo/ds',				[	'uses' => 'Resources\ArchivoController@ds',						'as' => 'resource.archivo.ds'				]);
	Route::get(		'archivo/{id}/download',	[	'uses' => 'Resources\ArchivoController@download',				'as' => 'resource.archivo.download'			]);
	Route::get(		'archivo/{id}/stream',		[	'uses' => 'Resources\ArchivoController@stream',					'as' => 'resource.archivo.stream'			]);
	Route::resource('archivo',					'Resources\ArchivoController',							$resource_actions);

	Route::post(	'verificarTieneUsuario',	[	'uses' => 'Admin\UsuarioController@verificarTieneUsuario',		'as' => 'admin.usuario.verificarTieneUsuario']);
	Route::post(	'asignarRoles',				[	'uses' => 'Admin\UsuarioController@asignarRoles',				'as' => 'admin.usuario.asignarRoles'		]);
	Route::resource(	'usuario',				'Admin\UsuarioController');

	Route::post(	'banner/ds',				[	'uses' => 'Resources\BannerController@ds',						'as' => 'resource.banner.ds'				]);
	Route::post(	'banner/restore',			[	'uses' => 'Resources\BannerController@restore',					'as' => 'resource.banner.restore'			]);
	Route::resource('banner',						'Resources\BannerController',							$resource_actions);

	Route::post('calendario/ds',				[	'uses' => 'Resources\CalendarioController@ds',					'as' => 'resource.calendario.ds'			]);
	Route::resource('calendario', 				'Resources\CalendarioController', 						$resource_actions);

	Route::post(	'categoria/ds',				[	'uses' => 'Resources\CategoriaController@ds',					'as' => 'resource.categoria.ds'				]);
	Route::resource('categoria',					'Resources\CategoriaController',						$resource_actions);

	Route::post(	'categoryDefinition/ds',	[	'uses' => 'Resources\CategoryDefinitionController@ds',			'as' => 'resource.categoryDefinition.ds'	]);
	Route::resource('categoryDefinition',			'Resources\CategoryDefinitionController',				$resource_actions);

	Route::post(	'cliente/ds',				[	'uses' => 'Resources\ClienteController@ds',						'as' => 'resource.cliente.ds'				]);
	Route::resource('cliente',					'Resources\ClienteController',							$resource_actions);

	Route::post(	'comentario/ds',			[	'uses' => 'Resources\ComentarioController@ds',					'as' => 'resource.comentario.ds'			]);
	Route::resource('comentario',					'Resources\ComentarioController',						$resource_actions);

	Route::post(	'configuracion/ds',			[	'uses' => 'Resources\ConfiguracionController@ds',				'as' => 'resource.configuracion.ds'			]);
	Route::resource('configuracion',				'Resources\ConfiguracionController',					$resource_actions);

	Route::post(	'cotizacion/ds',			[	'uses' => 'Resources\CotizacionController@ds',					'as' => 'resource.cotizacion.ds'			]);
	Route::resource('cotizacion',					'Resources\CotizacionController',						$resource_actions);

	Route::post(	'destino/ds',				[	'uses' => 'Resources\DestinoController@ds', 					'as' => 'resource.destino.ds'				]);
	Route::resource('destino',					'Resources\DestinoController', 						$resource_actions);

	Route::post(	'excepcion/ds',				[	'uses' => 'Resources\ExcepcionController@ds', 					'as' => 'resource.excepcion.ds'				]);
	Route::resource('excepcion',					'Resources\ExcepcionController', 						$resource_actions);

	Route::post(	'FBCampaign/ds',			[	'uses' => 'Resources\FBCampaignController@ds',					'as' => 'resource.FBCampaign.ds'			]);
	Route::resource('FBCampaign',					'Resources\FBCampaignController', 						$resource_actions);

	Route::post(	'flotchart/getOfertas',		[	'uses' => 'Resources\FlotChartController@getOfertas', 			'as' => 'resource.flotchart.getOfertas'		]);
	Route::post(	'flotchart/getPublicaciones',[	'uses' => 'Resources\FlotChartController@getPublicaciones', 	'as' => 'resource.flotchart.getPublicaciones']);
	Route::post(	'flotchart/getSubscribers',	[	'uses' => 'Resources\FlotChartController@getSubscribers', 		'as' => 'resource.flotchart.getSubscribers'	]);
	Route::post(	'flotchart/getOrdenes',		[	'uses' => 'Resources\FlotChartController@getOrdenes', 			'as' => 'resource.flotchart.getOrdenes'		]);

	Route::post(	'galeria/ds',				[	'uses' => 'Resources\GaleriaController@ds',						'as' => 'resource.galeria.ds'				]);
	Route::resource('galeria',					'Resources\GaleriaController', 						$resource_actions);

	Route::post(	'gasto/ds', 				[	'uses' => 'Resources\GastoController@ds',						'as' => 'resource.gasto.ds'					]);
	Route::resource('gasto',						'Resources\GastoController', 							$resource_actions);

	Route::post(	'imagen/ds',				[	'uses' => 'Resources\ImagenController@ds', 						'as' => 'resource.imagen.ds'				]);
	Route::resource('imagen',						'Resources\ImagenController', 							$resource_actions);

	Route::post(	'lista/ds',					[	'uses' => 'Resources\ListaController@ds',						'as' => 'resource.lista.ds'					]);
	Route::resource('lista',						'Resources\ListaController',							$resource_actions);

	Route::post(	'listaItem/ds',				[	'uses' => 'Resources\ListaItemController@ds',					'as' => 'resource.listaItem.ds'				]);
	Route::resource('listaItem',					'Resources\ListaItemController', 						$resource_actions);

	Route::post(	'marca/ds',					[	'uses' => 'Resources\MarcaController@ds',						'as' => 'resource.marca.ds'					]);
	Route::resource('marca',						'Resources\MarcaController', 							$resource_actions);

	Route::post(	'modelo/ds',				[	'uses' => 'Resources\ModeloController@ds',						'as' => 'resource.modelo.ds'				]);
	Route::resource('modelo',						'Resources\ModeloController', 							$resource_actions);

	Route::post(	'metatag/ds',				[	'uses' => 'Resources\MetatagController@ds',						'as' => 'resource.metatag.ds'				]);
	Route::resource('metatag',					'Resources\MetatagController', 						$resource_actions);

	Route::post(	'newsletter/ds',			[	'uses' => 'Resources\NewsletterController@ds',					'as' => 'resource.newsletter.ds'			]);
	Route::resource('newsletter',					'Resources\NewsletterController', 						$resource_actions);

	Route::post(	'noticia/ds',				[	'uses' => 'Resources\NoticiaController@ds', 					'as' => 'resource.noticia.ds'				]);
	Route::resource('noticia',					'Resources\NoticiaController', 						$resource_actions);

	Route::post(	'oferta/ds',				[	'uses' => 'Resources\OfertaController@ds', 						'as' => 'resource.oferta.ds'				]);
	Route::post(	'oferta/getFlotData',		[	'uses' => 'Resources\OfertaController@getFlotData',				'as' => 'resource.oferta.getFlotData'		]);
	Route::resource('oferta',						'Resources\OfertaController', 							$resource_actions);

	Route::post(	'orden/ds',					[	'uses' => 'Resources\OrdenController@ds',						'as' => 'resource.orden.ds'					]);
	Route::resource('orden',						'Resources\OrdenController', 							$resource_actions);

	Route::post(	'permiso/ds',				[	'uses' => 'Resources\PermisoController@ds',						'as' => 'resource.permiso.ds'				]);
	Route::resource('permiso',					'Resources\PermisoController', 						$resource_actions);

	Route::post(	'precio/ds',				[	'uses' => 'Resources\PrecioController@ds',						'as' => 'resource.precio.ds'				]);
	Route::resource('precio',						'Resources\PrecioController', 							$resource_actions);

	Route::post(	'producto/{id}/update_tags',[	'uses' => 'Resources\ProductoController@update_tags',			'as' => 'resource.producto.update_tags'		]);
	Route::post(	'producto/ds',				[	'uses' => 'Resources\ProductoController@ds',					'as' => 'resource.producto.ds'				]);
	Route::post(	'producto/setActividades',	[	'uses' => 'Resources\ProductoController@setActividades',		'as' => 'resource.producto.setActividades'	]);
	Route::resource('producto',					'Resources\ProductoController', 						$resource_actions);

	Route::post(	'productoItem/ds',			[	'uses' => 'Resources\ProductoItemController@ds',				'as' => 'resource.productoItem.ds'			]);
	Route::resource('productoItem',				'Resources\ProductoItemController', 					$resource_actions);

	Route::post(	'proveedor/ds',				[	'uses' => 'Resources\ProveedorController@ds',					'as' => 'resource.proveedor.ds'				]);
	Route::resource('proveedor',					'Resources\ProveedorController', 						$resource_actions);


	Route::post(	'proveedorContacto/ds',		[	'uses' => 'Resources\ProveedorContactoController@ds',			'as' => 'resource.proveedorContacto.ds'		]);
	Route::resource('proveedorContacto',			'Resources\ProveedorContactoController', 				$resource_actions);

	Route::post(	'publicacion/ds',			[	'uses' => 'Resources\PublicacionController@ds',						'as' => 'resource.publicacion.ds'		]);
	Route::resource('publicacion',				'Resources\PublicacionController', 					$resource_actions);


	Route::post(	'role/ds',					[	'uses' => 'Resources\RoleController@ds',						'as' => 'resource.role.ds'					]);
	Route::post(	'role/guardarRolePermiso',	[	'uses' => 'Resources\RoleController@guardarRolePermiso',		'as' => 'resource.role.guardarRolePermiso'	]);
	Route::resource('role',						'Resources\RoleController',							$resource_actions);

	Route::post(	'searchAlert/ds',			[	'uses' => 'Resources\SearchAlertController@ds',					'as' => 'resource.searchAlert.ds'			]);
	Route::resource('searchAlert',				'Resources\SearchAlertController',						$resource_actions);

	Route::post(	'subscripcion/ds',			[	'uses' => 'Resources\SubscripcionController@ds',				'as' => 'resource.subscripcion.ds'			]);
	Route::resource('subscripcion',				'Resources\SubscripcionController', 					$resource_actions);

	Route::post(	'tag/ds',					[	'uses' => 'Resources\TagController@ds',							'as' => 'resource.tag.ds'					]);
	Route::resource('tag',						'Resources\TagController',								$resource_actions);

	Route::post(	'template/ds',				[	'uses' => 'Resources\TemplateController@ds',					'as' => 'resource.template.ds'				]);
	Route::resource('template',					'Resources\TemplateController',						$resource_actions);

	Route::post(	'transaccion/ds',			[	'uses' => 'Resources\TransaccionController@ds',					'as' => 'resource.transaccion.ds'			]);
	Route::resource('transaccion',				'Resources\TransaccionController',						$resource_actions);


	Route::post(	'transporte/ds',			[	'uses' => 'Resources\TransporteController@ds',					'as' => 'resource.transporte.ds'			]);
	Route::resource('transporte',					'Resources\TransporteController', 						$resource_actions);

	Route::post(	'usuario/ds',				[	'uses' => 'Resources\UsuarioController@ds',						'as' => 'resource.usuario.ds'				]);
	Route::post(	'usuario/{id}/restore',		[	'uses' => 'Resources\UsuarioController@restore',				'as' => 'resource.usuario.restore'			]);
	Route::resource('usuario',					'Resources\UsuarioController',							$resource_actions);

	Route::post(	'ubicacion/ds',				[	'uses' => 'Resources\UbicacionController@ds',					'as' => 'resource.ubicacion.ds'				]);
	Route::resource('ubicacion',					'Resources\UbicacionController',						$resource_actions);
});



$adminRoutes = function() {
//	Route::get('/', function () {
//		return view('pages/dashboard-v1');
//	});

	$i_s_c_e = [
		'as' => 'admin',
		'only' => [
			'index',
			'show',
			'create',
			'edit'
		]
	];

	$i_c_e_u_d = [
		'as' => 'admin',
		'only' => [
			'index',
			'create',
			'edit',
			'update',
			'destroy'
		]
	];

	$i_s_c_e_u = [
		'as' => 'admin',
		'only' => [
			'index',
			'show',
			'create',
			'edit',
			'update',
		]
	];

	Route::get(	'/check_product_thumbs',								[	'uses' => 'Admin\IndexController@check_product_thumbs', 					'as' => 'admin.check_product_thumbs'				]);
	Route::get(	'/auth_as/{id}',										[	'uses' => 'Admin\IndexController@auth_as', 									'as' => 'admin.auth_as'								]);
	Route::post('/changeLanguage',										[	'uses' => 'Admin\IndexController@changeLanguage',							'as' => 'admin.changeLanguage'						]);
	Route::get(	'/',													[	'uses' => 'Admin\IndexController@home'														]);
	Route::get(	'/login',												[	'uses' => 'AuthAdmin\LoginController@showLoginForm',						'as' => 'admin.loginForm'							]);
	Route::post('/login',												[	'uses' => 'AuthAdmin\LoginController@login',								'as' => 'admin.login'								]);
	Route::get(	'/logout',												[	'uses' => 'AuthAdmin\LoginController@logout',								'as' => 'admin.logout'								]);

	Route::get(	'/home',												[	'uses' => 'Admin\IndexController@home',										'as' => 'admin.home'								]);

	Route::resource('actividad',											'Admin\ActividadController',										$i_s_c_e);
	Route::resource('actividadTipo',										'Admin\ActividadTipoController',									$i_s_c_e);

	Route::get(	'anuncio/mapaFormularios',								[	'uses' => 'Admin\AnuncioController@mapaFormularios',						'as' => 'admin.anuncio.mapaFormularios'				]);
	Route::resource('anuncio',											'Admin\AnuncioController', 										$i_s_c_e);
	Route::resource('agenda',												'Admin\AgendaController', 											$i_s_c_e);

	Route::patch('announcementInbox/{idAnnounce}/updateCalculadora', 	[	'uses' => 'Admin\AnnouncementInboxController@updateCalculadora', 			'as' => 'admin.announcementInbox.updateCalculadora'	]);
	Route::get(	'announcementInbox/{id}/{lang?}', 						[	'uses' => 'Admin\AnnouncementInboxController@show', 						'as' => 'admin.announcementInbox.show'				]);
	Route::get(	'announcementInbox/trans_disp/{pubId}/{idTrans}', 		[	'uses' => 'Admin\AnnouncementInboxController@transporteDisponibilidad', 	'as' => 'admin.announcementInbox.transporteDisponibilidad']);
	Route::post('announcementInbox/trans_disp/{pubId}/{idTrans}', 		[	'uses' => 'Admin\AnnouncementInboxController@transporteDisponibilidad', 	'as' => 'admin.announcementInbox.transporteDisponibilidad']);
	Route::post('announcementInbox/estado',								[	'uses' => 'Admin\AnnouncementInboxController@estado', 						'as' => 'admin.announcementInbox.estado'			]);
	Route::resource('announcementInbox',									'Admin\AnnouncementInboxController',								$i_c_e_u_d);

	Route::post('bannerSuperior/sort',									[	'uses' => 'Admin\BannerSuperiorController@sort',							'as' => 'admin.bannerSuperior.sort']);
	Route::post('bannerSuperiorUpload',									[	'uses' => 'Admin\BannerSuperiorController@upload',							'as' => 'admin.bannerSuperior.upload']);
	Route::resource('bannerSuperior', 									'Admin\BannerSuperiorController',									$i_s_c_e_u);

	Route::post('bannerLateralUpload',									[	'uses' => 'Admin\BannerLateralController@upload',							'as' => 'admin.bannerLateral.upload']);
	Route::resource('bannerLateral',										'Admin\BannerLateralController', 									$i_s_c_e_u);

	Route::resource('cotizacion',											'Admin\CotizacionController',										$i_s_c_e);

	Route::get('getFormData/{idCategoria}',								[	'uses' => 'Admin\CategoryDefinitionController@getFormData',					'as' => 'admin.categoryDefinition.getFormData']);
	Route::resource('categoryDefinition',									'Admin\CategoryDefinitionController',								$i_s_c_e_u);

	Route::resource('calendario',											'Admin\CalendarioController',										$i_s_c_e );

	Route::get('cliente/subscripcionesTable/{idCliente}',				[	'uses' => 'Admin\ClienteController@subscripcionesTable',					'as' => 'admin.cliente.subscripcionesTable']);
	Route::resource('cliente',											'Admin\ClienteController',											$i_s_c_e_u);

	Route::resource('comentario',											'Admin\ComentarioController',										$i_s_c_e);

	Route::resource('configuracion',										'Admin\ConfiguracionController',									$i_s_c_e_u);

	Route::resource('destino',											'Admin\DestinoController',											$i_s_c_e_u);

	Route::resource('divisa',												'Admin\DivisaController',											$i_s_c_e_u);

	Route::post('excepcion/lookup',										[	'uses' => 'Admin\ExcepcionController@lookup',								'as' => 'admin.excepcion.lookup']);
	Route::resource('excepcion',											'Admin\ExcepcionController',	[
		'as' => 'admin',
		'only' => [
			'index',
			'show',
		]
	]);

	Route::resource('FBCampaign',											'Admin\FBCampaignController',										$i_s_c_e);

	Route::resource('galeria',											'Admin\GaleriaController',											$i_s_c_e);

	Route::post('gasto/upload',											[	'uses' => 'Admin\GastoController@upload',									'as' => 'admin.gasto.upload']);
	Route::resource('gasto',												'Admin\GastoController', 											$i_s_c_e_u);

	Route::resource('imagen',												'Admin\ImagenController',											$i_s_c_e);

	Route::get(	'importacion',											[	'uses' => 'Admin\ImportacionController@index',								'as' => 'admin.importacion.index']);
	Route::get(	'importacion/imagenes',									[	'uses' => 'Admin\ImportacionController@imagenes',							'as' => 'admin.importacion.imagenes']);
	Route::get(	'importacion/archivos',									[	'uses' => 'Admin\ImportacionController@archivos',							'as' => 'admin.importacion.archivos']);
	Route::get(	'importacion/form',										[	'uses' => 'Admin\ImportacionController@form',								'as' => 'admin.importacion.form']);
	Route::post('importacion/upload',									[	'uses' => 'Admin\ImportacionController@upload',								'as' => 'admin.importacion.upload']);
	Route::post('importacion/imp_imagen',								[	'uses' => 'Admin\ImportacionController@imp_imagen',							'as' => 'admin.importacion.imp_imagen']);
	Route::post('importacion/imp_archivo',								[	'uses' => 'Admin\ImportacionController@imp_archivo',						'as' => 'admin.importacion.imp_archivo']);
	Route::post('importacion/pull_precios',								[	'uses' => 'Admin\ImportacionController@pull_precios',						'as' => 'admin.importacion.pull_precios']);
	Route::post('importacion/importar_precios',							[	'uses' => 'Admin\ImportacionController@importar_precios',					'as' => 'admin.importacion.importar_precios']);

	Route::post('getNavigationCounters',								[	'uses' => 'Admin\NavigationController@getCounters',							'as' => 'admin.navigation.getCounters']);
	Route::post('getPendingFakturas',									[	'uses' => 'Admin\NavigationController@getPendingFakturas',					'as' => 'admin.navigation.getPendingFakturas']);

	Route::get('lista/{id}/edit_alt',									[	'uses' => 'Admin\ListaController@edit_alt',									'as' => 'admin.lista.edit_alt']);
	Route::resource('lista',												'Admin\ListaController',											$i_s_c_e);

	Route::get('listaItem/{id}/edit_alt',								[	'uses' => 'Admin\ListaItemController@edit_alt',								'as' => 'admin.listaItem.edit_alt']);
	Route::get('listaItem/create/{idLista?}',							[	'uses' => 'Admin\ListaItemController@create',								'as' => 'admin.listaItem.create']);
	Route::get('listaItem/create_alt/{idLista?}',						[	'uses' => 'Admin\ListaItemController@create_alt',							'as' => 'admin.listaItem.create_alt']);
	Route::resource('listaItem',											'Admin\ListaItemController',	[
		'as' => 'admin',
		'only' => [
			'index',
			'show',
			'edit',
		]
	]);

	Route::resource('marca',												'Admin\MarcaController',											$i_s_c_e);
	Route::resource('modelo',												'Admin\ModeloController',											$i_s_c_e);
	Route::resource('metatag',											'Admin\MetatagController',											$i_s_c_e);
	Route::resource('newsletter',											'Admin\NewsletterController',										$i_s_c_e);
	Route::post(	'noticia/ds',										[	'uses' => 'Admin\NoticiaController@ds', 									'as' => 'admin.noticia.ds']);
	Route::resource('noticia',											'Admin\NoticiaController',											$i_s_c_e_u);

//	Route::post('ordenInbox/{idAnnounce}/enviarMensajeLibre', 	['uses' => 'Admin\OrdenInboxController@enviarMensajeLibre', 'as' => 'admin.ordenInbox.enviarMensajeLibre']);
	Route::post('ordenInbox/{id}/crearFactura', 						[	'uses' => 'Admin\OrdenInboxController@crearFactura', 						'as' => 'admin.ordenInbox.crearFactura']);
	Route::get(	'ordenInbox/{id}/factura',								[	'uses' => 'Admin\OrdenInboxController@factura', 							'as' => 'admin.ordenInbox.factura']);
	Route::resource('ordenInbox',											'Admin\OrdenInboxController',										$i_s_c_e_u);

	Route::get('orden/create/{idProducto?}',							[	'uses' => 'Admin\OrdenController@create',									'as' => 'admin.orden.create']);
	Route::resource('orden',												'Admin\OrdenController',	[
		'as' => 'admin',
		'only' => [
			'index',
			'show',
			'store',
			'update',
			'edit',
		]
	]);

	Route::get('oferta/{id}/getNoOferta',								[	'uses' => 'Admin\OfertaController@getNoOferta', 								'as' => 'admin.oferta.getNoOferta']);
	Route::post('oferta/upload',										[	'uses' => 'Admin\OfertaController@upload', 									'as' => 'admin.oferta.upload']);
	Route::resource('oferta',												'Admin\OfertaController',											$i_s_c_e);

	Route::resource('permiso',											'Admin\PermisoController',											$i_s_c_e);


	Route::get(	'producto/{idProducto}/historia', 						[	'uses' => 'Admin\ProductoController@historia',								'as' => 'admin.producto.historia' ]);
	Route::post('producto/{idProducto}/move', 							[	'uses' => 'Admin\ProductoController@images_move',							'as' => 'admin.producto.images_move' ]);
	Route::post('producto/{idProducto}/resize', 						[	'uses' => 'Admin\ProductoController@images_resize',							'as' => 'admin.producto.images_resize' ]);
	Route::post('producto/{idProducto}/remove', 						[	'uses' => 'Admin\ProductoController@images_remove',							'as' => 'admin.producto.images_remove' ]);
	Route::get(	'producto/{idProducto}/create_item', 					[	'uses' => 'Admin\ProductoController@create_item',							'as' => 'admin.producto.create_item' ]);
	Route::get(	'producto/{idProductoItem}/create_precios', 			[	'uses' => 'Admin\ProductoController@create_precios',						'as' => 'admin.producto.create_precios' ]);

	Route::get(	'producto/{idProducto}/translation/{campo}',			[	'uses' => 'Admin\ProductoController@translation',							'as' => 'admin.producto.translation']);
	Route::post('producto/{idProducto}/translation/{campo}',			[	'uses' => 'Admin\ProductoController@translation',							'as' => 'admin.producto.translation']);
	Route::get(	'producto/{idProducto}/translationML/{campo}',			[	'uses' => 'Admin\ProductoController@translationML',							'as' => 'admin.producto.translationML']);
	Route::post('producto/{idProducto}/translationML/{campo}',			[	'uses' => 'Admin\ProductoController@translationML',							'as' => 'admin.producto.translationML']);

	Route::get(	'producto/getForm/{idCat}/{idProd?}',					[	'uses' => 'Admin\ProductoController@getForm', 								'as' => 'admin.producto.getForm']);
	Route::get(	'producto/getCampos/{idCategoria}',						[	'uses' => 'Admin\ProductoController@getCampos', 							'as' => 'admin.producto.getCampos']);
	Route::post('producto/getModelos',									[	'uses' => 'Admin\ProductoController@getModelos', 							'as' => 'admin.producto.getModelos']);
	Route::post('producto/upload',										[	'uses' => 'Admin\ProductoController@upload', 								'as' => 'admin.producto.upload']);
	Route::get(	'producto/create/{idUsuario?}',							[	'uses' => 'Admin\ProductoController@create', 								'as' => 'admin.producto.create']);

	Route::get(	'producto/{id}/asignarActividades',						[	'uses' => 'Admin\ProductoController@asignarActividades', 					'as' => 'admin.producto.asignarActividades']);
	Route::get(	'producto/asignarActividades',							[	'uses' => 'Admin\ProductoController@asignarActividades', 					'as' => 'admin.producto.asignarActividadesMass']);

	Route::resource('producto',											'Admin\ProductoController', [
		'as' => 'admin',
		'only' => [
			'index',
			'show',
			'store',
			'edit',
			'update',
		]
	]);

	Route::resource('proveedor',											'Admin\ProveedorController', 										$i_s_c_e_u);

//	Route::resource('proveedorContacto',									'Admin\ProveedorContactoController',								$i_s_c_e);

	Route::resource('publicacion',										'Admin\PublicacionController',										$i_s_c_e);


	Route::post(	'quoteInbox/{idAnnounce}/enviarMensajeLibre', 		[	'uses' => 'Admin\QuoteInboxController@enviarMensajeLibre',					'as' => 'admin.quoteInbox.enviarMensajeLibre']);
	Route::post(	'quoteInbox/{idAnnounce}/crearOrden', 				[	'uses' => 'Admin\QuoteInboxController@crearOrden',							'as' => 'admin.quoteInbox.crearOrden']);
	Route::resource('quoteInbox',											'Admin\QuoteInboxController',										$i_s_c_e_u);

	Route::get('reporte/oferta',										[ 'uses' => 'Admin\ReporteController@oferta', 									'as' => 'admin.reporte.oferta']);
	Route::get('reporte/venta',											[ 'uses' => 'Admin\ReporteController@venta', 									'as' => 'admin.reporte.venta']);

	Route::resource('role',												'Admin\RoleController',											$i_s_c_e_u);

	Route::post('searchAlertInbox/{idSearchAlert}/enviarMensajeLibre',	[	'uses' => 'Admin\SearchAlertInboxController@enviarMensajeLibre',			'as' => 'admin.searchAlertInbox.enviarMensajeLibre']);
	Route::resource('searchAlertInbox',									'Admin\SearchAlertInboxController',								$i_s_c_e);

	Route::resource('stock', 'Admin\StockController', [
		'as' => 'admin',
		'only' => [
			'index',
		]
	]);

//	Route::post('subscripcion/create/{idUsuario?}', ['uses' => 'Admin\SubscripcionController@create', 'as' => 'admin.subscripcion.create']);
	Route::resource('subscripcion',										'Admin\SubscripcionController',									$i_s_c_e);

	Route::get('tag/selector/{idProducto?}',								[	'uses' => 'Admin\TagController@selector', 									'as' => 'admin.tag.selector'						]);
	Route::resource('tag',												'Admin\TagController',	[
		'as' => 'admin',
		'only' => [
			'index',
			'show',
			'create',
			'store',
			'edit',
			'update',
		]
	]);

	Route::get('template/email',			 							[	'uses' => 'Admin\TemplateController@email', 								'as' => 'admin.template.email'						]);
	Route::post('template/sendEmail',		 							[	'uses' => 'Admin\TemplateController@sendEmail', 							'as' => 'admin.template.sendEmail'					]);
	Route::get('template/{template}/editor_pdf', 						[	'uses' => 'Admin\TemplateController@editor_pdf', 							'as' => 'admin.template.editor_pdf'					]);
	Route::get('template/{template}/getPDF', 							[	'uses' => 'Admin\TemplateController@getPDF', 								'as' => 'admin.template.getPDF'						]);
	Route::resource('template',											'Admin\TemplateController',										$i_s_c_e_u);

	Route::get(	'traduccion/{owner}/{owner_id}/{campo}/multiline',		[	'uses' => 'Admin\TraduccionController@multiline',							'as' => 'admin.traduccion.multiline']);
	Route::get(	'traduccion/{owner}/{owner_id}/{campo}/singleline',		[	'uses' => 'Admin\TraduccionController@singleline',							'as' => 'admin.traduccion.singleline']);
	Route::post('traduccion/{owner}/{owner_id}/{campo}/multiline',		[	'uses' => 'Admin\TraduccionController@multiline',							'as' => 'admin.traduccion.multiline']);
	Route::post('traduccion/{owner}/{owner_id}/{campo}/singleline',		[	'uses' => 'Admin\TraduccionController@singleline',							'as' => 'admin.traduccion.singleline']);
	Route::post('traduccion/gtranslate',								[	'uses' => 'Admin\TraduccionController@gtranslate',							'as' => 'admin.traduccion.gtranslate']);
	Route::resource('traduccion',											'Admin\TraduccionController',										$i_s_c_e);

	Route::get(	'transaccion/{transaccion}/process',					[	'uses' => 'Admin\TransaccionController@process',							'as' => 'admin.transaccion.process']);
	Route::resource('transaccion',										'Admin\TransaccionController',										$i_s_c_e);

	Route::resource('transporte',											'Admin\TransporteController',										$i_s_c_e_u);

	Route::resource('usuario',											'Admin\UsuarioController',		[
		'as' => 'admin',
	]);

	Route::post('uploader',											['uses' => 'Admin\UploaderController@upload', 'as' => 'admin.uploader.upload']);

//	Route::resource('usuario', 'Admin\UsuarioController');

//	Route::post('usuario/ds', 		['uses' => 'Resources\UsuarioController@ds', 					'as' => 'resource.usuario.ds'				]);
//	Route::resource('usuario', 		'Resources\UsuarioController');

};

$publicRoutes = function(){
	Route::get(	'/',								['uses' => 'Frontend\IndexController@index',					'as' => 'public.index']);
	Route::get(	'/sales',							['uses' => 'Frontend\IndexController@index_sales',				'as' => 'public.index_sales']);
	Route::get(	'/nuestros-servicios',				['uses' => 'Frontend\IndexController@index_servicios',			'as' => 'public.index_servicios']);
	Route::get(	'/sobre-nosotros',					['uses' => 'Frontend\IndexController@index_nosotros',			'as' => 'public.index_nosotros']);
	Route::get(	'/home',							['uses' => 'Frontend\IndexController@index',					'as' => 'public.home']);
	Route::get(	'/market',							['uses' => 'Frontend\IndexController@buy',						'as' => 'public.buy_page']);
	Route::get(	'/buy',								['uses' => 'Frontend\IndexController@oldbuy',					'as' => 'public.oldbuy']);
	Route::get(	'/marknadsplats',					['uses' => 'Frontend\IndexController@buy',						'as' => 'public.marknadsplats']);
	Route::post('/setWelcomeBanner',				['uses' => 'Frontend\IndexController@setWelcomeBanner',			'as' => 'public.setWelcomeBanner']);
	Route::post('/newsletter',						['uses' => 'Frontend\IndexController@newsletter',				'as' => 'public.newsletter']);

	Route::get(	'/expande_tu_negocio',				['uses' => 'Frontend\IndexController@landing_expand',			'as' => 'public.es.landing_expand']);
	Route::get(	'/expand_your_business',			['uses' => 'Frontend\IndexController@landing_expand',			'as' => 'public.en.landing_expand']);
	Route::get(	'/utoka_ditt_foretag',				['uses' => 'Frontend\IndexController@landing_expand',			'as' => 'public.se.landing_expand']);

	Route::get(	'/carousel',						['uses' => 'Frontend\IndexController@carousel',					'as' => 'public.carousel']);
//	Route::get(	/home',									['uses' => 'Frontend\IndexController@home',						'as' => 'public.home']);
	Route::get(	'/nosotros',						['uses' => 'Frontend\IndexController@nosotros',					'as' => 'public.es.nosotros']);
	Route::get(	'/about_us',						['uses' => 'Frontend\IndexController@nosotros',					'as' => 'public.en.nosotros']);
	Route::get(	'/om_oss',							['uses' => 'Frontend\IndexController@nosotros',					'as' => 'public.se.nosotros']);
	Route::get(	'/servicios/{s?}',					['uses' => 'Frontend\IndexController@services',					'as' => 'public.es.servicios']);
	Route::get(	'/services/{s?}',					['uses' => 'Frontend\IndexController@services',					'as' => 'public.en.servicios']);
	Route::get(	'/vara-tjanster/{s?}',				['uses' => 'Frontend\IndexController@services',					'as' => 'public.se.servicios']);
	Route::get(	'/faq',								['uses' => 'Frontend\IndexController@faq',						'as' => 'public.faq']);

	Route::get(	'/planes',							['uses' => 'Frontend\PricingController@pricing',				'as' => 'public.es.pricing']);
	Route::get(	'/our_plans',						['uses' => 'Frontend\PricingController@pricing',				'as' => 'public.en.pricing']);
	Route::get(	'/foretagspaket',					['uses' => 'Frontend\PricingController@pricing',				'as' => 'public.se.pricing']);
	Route::get(	'/authenticate',					['uses' => 'Frontend\PricingController@authenticate',			'as' => 'public.authenticate']);
	Route::get(	'/detalle_factura',					['uses' => 'Frontend\PricingController@factura_detail',			'as' => 'public.es.factura_detail']);
	Route::get(	'/factura_detail',					['uses' => 'Frontend\PricingController@factura_detail',			'as' => 'public.en.factura_detail']);
	Route::get(	'/faktura_detalj',					['uses' => 'Frontend\PricingController@factura_detail',			'as' => 'public.se.factura_detail']);
	Route::post('/detalle_factura',					['uses' => 'Frontend\PricingController@factura_detail',			'as' => 'public.es.factura_detail']);
	Route::post('/factura_detail',					['uses' => 'Frontend\PricingController@factura_detail',			'as' => 'public.en.factura_detail']);
	Route::post('/faktura_detalj',					['uses' => 'Frontend\PricingController@factura_detail',			'as' => 'public.se.factura_detail']);
	Route::get(	'/comprar_plan_business',			['uses' => 'Frontend\PricingController@get_business',			'as' => 'public.es.get_business']);
	Route::get(	'/get_business',					['uses' => 'Frontend\PricingController@get_business',			'as' => 'public.en.get_business']);
	Route::get(	'/skaffa_business',					['uses' => 'Frontend\PricingController@get_business',			'as' => 'public.se.get_business']);
	Route::get(	'/comprar_plan_plus',				['uses' => 'Frontend\PricingController@get_plus',				'as' => 'public.es.get_plus']);
	Route::get(	'/get_plus',						['uses' => 'Frontend\PricingController@get_plus',				'as' => 'public.en.get_plus']);
	Route::get(	'/skaffa_plus',						['uses' => 'Frontend\PricingController@get_plus',				'as' => 'public.se.get_plus']);
	Route::get(	'/canceled_business',				['uses' => 'Frontend\PricingController@canceled_business',		'as' => 'public.canceled_business']);
	Route::get(	'/canceled_plus',					['uses' => 'Frontend\PricingController@canceled_plus',			'as' => 'public.canceled_plus']);

	Route::get(	'/checkout',						['uses' => 'Frontend\PaymentController@checkout',				'as' => 'public.checkout']);	// DEPRECAR
	Route::get(	'/customer',						['uses' => 'Frontend\PaymentController@customer',				'as' => 'public.customer']);	// DEPRECAR
	Route::get(	'/customer/{id}',					['uses' => 'Frontend\PaymentController@customerInfo',			'as' => 'public.customerInfo']);
	Route::post('/createCustomer/{id}',				['uses' => 'Frontend\PaymentController@createCustomer',			'as' => 'public.createCustomer']);

	Route::get(	'/checkout_business',				['uses' => 'Frontend\PricingController@checkout_business',		'as' => 'public.checkout_business']);
	Route::get(	'/checkout_plus',					['uses' => 'Frontend\PricingController@checkout_plus',			'as' => 'public.checkout_plus']);

	Route::get(	'/contacto',						['uses' => 'Frontend\IndexController@contacto',					'as' => 'public.es.contact']);
	Route::get(	'/contact',							['uses' => 'Frontend\IndexController@contacto',					'as' => 'public.en.contact']);
	Route::get(	'/kontakt',							['uses' => 'Frontend\IndexController@contacto',					'as' => 'public.se.contact']);
	Route::post('/contact',							['uses' => 'Frontend\IndexController@contactoPost',				'as' => 'public.contactPost']);

	Route::get(	'perfil/{id}',						['uses' => 'Frontend\IndexController@perfilCliente',			'as' => 'public.es.perfilCliente']);
	Route::get(	'profile/{id}',						['uses' => 'Frontend\IndexController@perfilCliente',			'as' => 'public.en.perfilCliente']);
	Route::get(	'profill/{id}',						['uses' => 'Frontend\IndexController@perfilCliente',			'as' => 'public.se.perfilCliente']);

	Route::post('buscar',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.es.search']);
	Route::post('search',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.en.search']);
	Route::post('sok',								['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.se.search']);
	Route::get(	'buscar/{cat?}', 					['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.es.search']);
	Route::get(	'search/{cat?}', 					['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.en.search']);
	Route::get(	'sok/{cat?}', 						['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.se.search']);

	Route::get(	'catalogo',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.es.catalogo']);

//	Route::post(	'buscar',						['uses' => 'Frontend\SearchController@buscar', 						'as' => 'public.es.buscar']);
//	Route::post(	'search',						['uses' => 'Frontend\SearchController@buscar', 						'as' => 'public.en.buscar']);
//	Route::post(	'sok',							['uses' => 'Frontend\SearchController@buscar', 						'as' => 'public.se.buscar']);
//	Route::get(	'buscar/{cat?}', 					['uses' => 'Frontend\SearchController@buscar', 						'as' => 'public.es.buscar']);
//	Route::get(	'search/{cat?}', 					['uses' => 'Frontend\SearchController@buscar', 						'as' => 'public.en.buscar']);
//	Route::get(	'sok/{cat?}', 						['uses' => 'Frontend\SearchController@buscar', 						'as' => 'public.se.buscar']);

	// SE
	Route::get(	'industri', 						['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industri']);
	Route::get(	'industriell', 						['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industriell']);
	Route::get(	'transport', 						['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.transport']);
	Route::get(	'entreprenad', 						['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.entreprenad']);	// construccion
	Route::get(	'entreprenadmaskiner', 				['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.entreprenadmaskiner']);	// construccion
	Route::get(	'lantbruk', 						['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.lantbruk']);	// agricultura
	Route::get(	'lantbruksmaskiner', 				['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.lantbruksmaskiner']);	// agricultura
	Route::get(	'truck',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.truck']);
	Route::get(	'truckar',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.truckar']);
	Route::get(	'ovriga',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.ovriga']);
	Route::get(	'andra',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.andra']);

	// EN
	Route::get(	'industrial', 						['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industrial']);
//	Route::get(	'transport', 							['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.transport']);
	Route::get(	'equipment', 						['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.equipment']);		// construccion
	Route::get(	'agricultural', 					['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.agricultural']);	// agricultura
	Route::get(	'forklift',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.forklift']);
	Route::get(	'others',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.others']);

	// ES
//	Route::get(	'industrial', 							['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industrial']);
	Route::get(	'transporte', 						['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.transporte']);
	Route::get(	'equipo', 							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.equipo']);		// construccion
	Route::get(	'agricola', 						['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.agricola']);	// agricultura
	Route::get(	'montacarga',						['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.montacarga']);
	Route::get(	'otros',							['uses' => 'Frontend\SearchController@buscar', 					'as' => 'public.otros']);

	// SE
	Route::get(	'industri/{subcat}', 				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industri_alt']);
	Route::get(	'industriell/{subcat}', 			['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industriell_alt']);
	Route::get(	'transport/{subcat}', 				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.transport_alt']);
	Route::get(	'entreprenad/{subcat}', 			['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.entreprenad_alt']);	// construccion
	Route::get(	'lantbruk/{subcat}', 				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.lantbruk_alt']);	// agricultura
	Route::get(	'truck/{subcat}',					['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.truck_alt']);
	Route::get(	'andra/{subcat}',					['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.andra_alt']);

	// EN
	Route::get(	'industrial/{subcat}', 				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industrial_alt']);
//	Route::get(	'transport/{subcat}', 					['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.transport_alt']);
	Route::get(	'equipment/{subcat}', 				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.equipment_alt']);		// construccion
	Route::get(	'agricultural/{subcat}', 			['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.agricultural_alt']);	// agricultura
	Route::get(	'forklift/{subcat}',				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.forklift_alt']);
	Route::get(	'others/{subcat}',					['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.others_alt']);

	// ES
//	Route::get(	'industrial/{subcat}', 					['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.industrial_alt']);
	Route::get(	'transporte/{subcat}', 				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.transporte_alt']);
	Route::get(	'equipo/{subcat}', 					['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.equipo_alt']);		// construccion
	Route::get(	'agricola/{subcat}', 				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.agricola_alt']);	// agricultura
	Route::get(	'montacarga/{subcat}',				['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.montacarga_alt']);
	Route::get(	'otros/{subcat}',					['uses' => 'Frontend\SearchController@buscar',					'as' => 'public.otros_alt']);

	Route::get(	'announce/{id?}', 					['uses' => 'Frontend\PublicAnnounceController@show', 				'as' => 'public.producto']);
	Route::post('announce/{id?}', 					['uses' => 'Frontend\PublicAnnounceController@enviarCotizacion',	'as' => 'public.enviarCotizacion']);
	Route::get(	'announce/{id?}/preview', 			['uses' => 'Frontend\AnnouncementsController@preview',				'as' => 'public.announce.preview']);
	Route::post('announceFavoriteAdd',				['uses' => 'Frontend\PublicAnnounceController@addFavorite', 		'as' => 'public.addFavorite']);
	Route::post('announceFavoriteRemove', 			['uses' => 'Frontend\PublicAnnounceController@removeFavorite',		'as' => 'public.removeFavorite']);

	Route::get(	'viewCotizacion/{id?}',				['uses' => 'Frontend\PublicAnnounceController@viewCotizacion',		'as' => 'public.viewCotizacion']);
	Route::get(	'reenviarCotizacion/{id?}',			['uses' => 'Frontend\PublicAnnounceController@reenviarCotizacion',	'as' => 'public.reenviarCotizacion']);

	Route::get(	'reportarCotizacion/{id?}',			['uses' => 'Frontend\PublicAnnounceController@reportarCotizacion',	'as' => 'public.reportQuote']);


	Route::get(	'/compradores', 					['uses' => 'Frontend\IndexController@info_compradores', 		'as' => 'public.es.info_compradores']);
	Route::get(	'/buyers', 							['uses' => 'Frontend\IndexController@info_compradores', 		'as' => 'public.en.info_compradores']);
	Route::get(	'/for-kopare', 						['uses' => 'Frontend\IndexController@info_compradores', 		'as' => 'public.se.info_compradores']);
	Route::get(	'/anunciantes', 					['uses' => 'Frontend\IndexController@info_anunciantes', 		'as' => 'public.es.info_anunciantes']);
	Route::get(	'/advertisers', 					['uses' => 'Frontend\IndexController@info_anunciantes', 		'as' => 'public.en.info_anunciantes']);
	Route::get(	'/for-annonsorer', 					['uses' => 'Frontend\IndexController@info_anunciantes', 		'as' => 'public.se.info_anunciantes']);

	Route::post('/changeLanguage', 					['uses' => 'Frontend\IndexController@changeLanguage', 			'as' => 'public.changeLanguage']);
	Route::post('/changeCurrency', 					['uses' => 'Frontend\IndexController@changeCurrency', 			'as' => 'public.changeCurrency']);

	Route::get(	'/auth_as/{token}',					['uses' => 'Frontend\IndexController@auth_as', 					'as' => 'public.auth_as' ] );
	Route::get(	'/login', 							['uses' => 'AuthCliente\LoginController@showLoginForm', 		'as' => 'public.loginForm']);
	Route::post('/login', 							['uses' => 'AuthCliente\LoginController@login', 				'as' => 'public.login']);
	Route::post('/register', 						['uses' => 'AuthCliente\RegisterController@register', 			'as' => 'public.register']);
	Route::post('/registerAdPub', 					['uses' => 'AuthCliente\RegisterController@registerAdPublisher','as' => 'public.registerAdPublisher']);
	Route::get(	'/confirm/{token?}',				['uses' => 'AuthCliente\RegisterController@confirm', 			'as' => 'public.confirm']);
	Route::get(	'/confirmacion/{token?}',			['uses' => 'AuthCliente\RegisterController@confirm', 			'as' => 'public.confirmacion']);
	Route::get(	'/reportar/{token?}',				['uses' => 'AuthCliente\RegisterController@reportar', 			'as' => 'public.reportar']);
	Route::post('/forgotPassword', 					['uses' => 'AuthCliente\LoginController@forgotPassword',		'as' => 'public.forgotPassword']);
	Route::get('/resetPassword/{token?}', 			['uses' => 'AuthCliente\LoginController@resetPassword',			'as' => 'public.resetPassword']);
	Route::post('/resetPassword/{token?}', 			['uses' => 'AuthCliente\LoginController@resetPassword',			'as' => 'public.resetPassword']);

	Route::get(	'/logout', 							['uses' => 'AuthCliente\LoginController@logout', 				'as' => 'public.logout']);

	// ----------

	Route::get('component/relatedItems/{cat?}',		['uses' => 'Frontend\ComponentController@relatedItems', 		'as' => 'component.relatedItems']);
	Route::get('component/mostVisitedItems', 		['uses' => 'Frontend\ComponentController@mostVisitedItems', 	'as' => 'component.mostVisitedItems']);
	Route::get('component/latestAddedItems', 		['uses' => 'Frontend\ComponentController@latestAddedItems', 	'as' => 'component.latestAddedItems']);
	Route::get('component/latestAddedItems_footer', 		['uses' => 'Frontend\ComponentController@latestAddedItems_footer', 	'as' => 'component.latestAddedItems_footer']);
	Route::get('component/announcesByProvider/{id}',['uses' => 'Frontend\ComponentController@announcesByProvider',	'as' => 'component.announcesByProvider']);

	// ----------

	Route::get(	'/cuenta', 							['uses' => 'Frontend\PrivateController@privCuenta', 			'as' => 'public.es.privCuenta']);
	Route::get(	'/account', 						['uses' => 'Frontend\PrivateController@privCuenta', 			'as' => 'public.en.privCuenta']);
	Route::get(	'/konto', 							['uses' => 'Frontend\PrivateController@privCuenta', 			'as' => 'public.se.privCuenta']);
	Route::post('/requestRenewal',					['uses' => 'Frontend\PrivateController@requestRenewal', 		'as' => 'private.requestRenewal']);

	Route::get(	'/anuncios_rem', 					['uses' => 'Frontend\AnnouncementsController@index_removed',	'as' => 'private.announcements.removed']);
	Route::get(	'/anuncios', 						['uses' => 'Frontend\AnnouncementsController@index',			'as' => 'private.es.announcements']);
	Route::get(	'/announcements', 					['uses' => 'Frontend\AnnouncementsController@index',			'as' => 'private.en.announcements']);
	Route::get(	'/annonser', 						['uses' => 'Frontend\AnnouncementsController@index',			'as' => 'private.se.announcements']);

	Route::get(	'/anuncios/{id}', 					['uses' => 'Frontend\AnnouncementsController@show',				'as' => 'private.es.announcements.show']);
	Route::get(	'/announcements/{id}', 				['uses' => 'Frontend\AnnouncementsController@show',				'as' => 'private.en.announcements.show']);
	Route::get(	'/annonser/{id}', 					['uses' => 'Frontend\AnnouncementsController@show',				'as' => 'private.se.announcements.show']);

	Route::get(	'/anuncios/{id}/editar',			['uses' => 'Frontend\AnnouncementsController@edit',				'as' => 'private.es.announcements.edit']);
	Route::get(	'/announcements/{id}/edit',			['uses' => 'Frontend\AnnouncementsController@edit',				'as' => 'private.en.announcements.edit']);
	Route::get(	'/annonser/{id}/redigera',			['uses' => 'Frontend\AnnouncementsController@edit',				'as' => 'private.se.announcements.edit']);

	Route::get(	'/anuncios/{id}/factura',			['uses' => 'Frontend\AnnouncementsController@invoice',			'as' => 'private.es.announcements.invoice']);
	Route::get(	'/announcements/{id}/invoice',		['uses' => 'Frontend\AnnouncementsController@invoice',			'as' => 'private.en.announcements.invoice']);
	Route::get(	'/annonser/{id}/faktura',			['uses' => 'Frontend\AnnouncementsController@invoice',			'as' => 'private.se.announcements.invoice']);

	Route::post('/announcements/ds', 				['uses' => 'Frontend\AnnouncementsController@ds', 				'as' => 'private.announcements.ds']);
	Route::post('/announcements/{id}/editUpload',	['uses' => 'Frontend\AnnouncementsController@editUpload',		'as' => 'private.announcements.editUpload']);
	Route::patch('/announcements/{id}',				['uses' => 'Frontend\AnnouncementsController@update',			'as' => 'private.announcements.update']);
	Route::delete('/announcements/delete/{id}', 	['uses' => 'Frontend\AnnouncementsController@destroy', 			'as' => 'private.announcements.delete']);

	Route::get(	'/announcements/{idProducto}/getForm/{idCategoria}', 	['uses' => 'Frontend\AnnouncementsController@getForm', 			'as' => 'private.announcements.getForm']);
	Route::get(	'/announcements/{idProducto}/getCampos/{idCategoria}', 	['uses' => 'Frontend\AnnouncementsController@getCampos', 		'as' => 'private.announcements.getCampos']);
	Route::post('/announcements/getModelos', 		['uses' => 'Frontend\AnnouncementsController@getModelos', 	'as' => 'private.announcements.getModelos']);

	Route::get(	'/changePassword', 					['uses' => 'Frontend\PrivateController@changePassword',			'as' => 'private.changePassword']);
	Route::post('/changePassword',					['uses' => 'Frontend\PrivateController@changePassword',			'as' => 'private.changePassword']);
	Route::get(	'/providerProfileEdit', 			['uses' => 'Frontend\PrivateController@providerProfileEdit',	'as' => 'private.providerProfileEdit']);
	Route::post('/providerProfileEditSave',			['uses' => 'Frontend\PrivateController@providerProfileEditSave','as' => 'private.providerProfileEditSave']);
	Route::post('/logoUpload', 						['uses' => 'Frontend\PrivateController@logoUpload', 			'as' => 'private.logoUpload']);

	Route::get(	'/creditCards', 					['uses' => 'Frontend\PrivateController@privCreditcards',		'as' => 'private.privCreditCards']);	// DEPRECAR

	Route::get('/pedidos', 							['uses' => 'Frontend\OrderController@index', 					'as' => 'private.es.order.index']);
	Route::get('/orders', 							['uses' => 'Frontend\OrderController@index', 					'as' => 'private.en.order.index']);
	Route::get('/order', 							['uses' => 'Frontend\OrderController@index', 					'as' => 'private.se.order.index']);
	Route::get('/pedidos/{id}', 					['uses' => 'Frontend\OrderController@show', 					'as' => 'private.es.order.show']);
	Route::get('/orders/{id}', 						['uses' => 'Frontend\OrderController@show', 					'as' => 'private.en.order.show']);
	Route::get('/order/{id}', 						['uses' => 'Frontend\OrderController@show', 					'as' => 'private.se.order.show']);
	Route::post('/orders/ds', 						['uses' => 'Frontend\OrderController@ds', 						'as' => 'private.order.ds']);

	Route::get('/ordenes', 							['uses' => 'Frontend\PrivateController@privOrdenes', 			'as' => 'public.privOrdenes']);		// DEPRECAR
	Route::get('/ordenEstado/{id}',					['uses' => 'Frontend\PrivateController@privOrdenEstado', 		'as' => 'public.privOrdenEstado']);		// DEPRECAR

	Route::get('/favoritos', 						['uses' => 'Frontend\PrivateController@privFavoritos', 			'as' => 'public.es.privFavoritos']);
	Route::get('/favorites', 						['uses' => 'Frontend\PrivateController@privFavoritos', 			'as' => 'public.en.privFavoritos']);
	Route::get('/favoriter', 						['uses' => 'Frontend\PrivateController@privFavoritos', 			'as' => 'public.se.privFavoritos']);

	Route::get('/consultas', 						['uses' => 'Frontend\PrivateController@privConsultas', 			'as' => 'public.privConsultas']);	// IMPLEMENTAR
	Route::get('/consultaDetalle/{id}', 			['uses' => 'Frontend\PrivateController@privConsultaDetalle',	'as' => 'public.privConsultaDetalle']);	// IMPLEMENTAR


	Route::post('/consultaExterna',					['uses' => 'Frontend\ConsultaExternaController@store', 			'as' => 'public.consultaExterna.store']);

	Route::get(	'/seguimiento/{id}/{action?}',		['uses' => 'Frontend\IndexController@seguimiento',				'as' => 'public.seguimiento.index']);

//	Route::get('/login', 			['uses' => 'Frontend\AuthController@login', 	'as' => 'public.login']);
//	Route::get('/', function(){
//		return view('pages/dashboard-v1');
//	});

	Route::get('/dataProtectionPolicy',				['uses' => 'Frontend\IndexController@dataProtectionPolicy', 	'as' => 'public.dataProtectionPolicy']);
	Route::get('/termsAndConditions',				['uses' => 'Frontend\IndexController@termsAndConditions',		'as' => 'public.termsAndConditions']);
	Route::get('/toc', 								['uses' => 'Frontend\IndexController@dataProtectionPolicy',	 	'as' => 'public.toc']);
//	Route::get('/termsAndConditions', 					['uses' => 'Frontend\IndexController@dataProtectionPolicy',	 	'as' => 'public.toc']);

	Route::get(	'/publish/{np?}', 					['uses' => 'Frontend\PublishController@publish', 				'as' => 'public.publish']);
	Route::get(	'/publishForm/{id}',				['uses' => 'Frontend\PublishController@publishForm', 			'as' => 'public.publishForm']);
	Route::post('/publishFormPost',					['uses' => 'Frontend\PublishController@publishFormPost', 		'as' => 'public.publishFormPost']);
	Route::post('/publishUpload', 					['uses' => 'Frontend\PublishController@publishUpload', 			'as' => 'public.publishUpload']);
	Route::post('/publishRemove', 					['uses' => 'Frontend\PublishController@publishRemove', 			'as' => 'public.publishRemove']);
	Route::post('/getModelos', 						['uses' => 'Frontend\PublishController@getModelos', 			'as' => 'public.getModelos']);
	Route::post('/updateSession', 					['uses' => 'Frontend\PublishController@updateSession', 			'as' => 'public.updateSession']);
	Route::post('/publishValidate',					['uses' => 'Frontend\PublishController@publishValidate', 		'as' => 'public.publishValidate']);
	Route::post('/getCustomerCards',				['uses' => 'Frontend\PublishController@getCustomerCards', 		'as' => 'public.getCustomerCards']);
	Route::get(	'/checkout', 						['uses' => 'Frontend\PublishController@checkout', 				'as' => 'public.checkout']);
	Route::post('/payment_dibs', 					['uses' => 'Frontend\PublishController@payment_dibs',			'as' => 'public.payment_dibs']);
	Route::get(	'/getnews/{id}', 					['uses' => 'Frontend\IndexController@getNews',					'as' => 'public.getnews']);


	Route::get('/{cat}/{subcat}/{marca}/{modelo}/{idp}', ['uses' => 'Frontend\PublicAnnounceController@show_seo', 				'as' => 'public.producto_seo']);
	Route::get('/list_metas', ['uses' => 'Frontend\PublicAnnounceController@list_metas', 				'as' => 'public.list_metas']);
//	Route::get('/{cat}/{subcat}', ['uses' => 'Frontend\SearchController@dsearch', 'as' => 'public.dsearch']);

};

$publicRoutesSe = function() {
	Route::get( '/', [ 'uses' => 'Frontend\IndexController@index', 'as' => 'public.index' ] );
};

$accountRoutes = function() {
//	Route::get( '/', 						[ 'uses' => 'Account\IndexController@index', 'as' => 'account.index' ] );
	Route::get(	'/', 						['uses' => 'AuthCliente\LoginController@showLoginForm_v2','as' => 'account.loginForm_v2']);
	Route::get(	'/login', 					['uses' => 'AuthCliente\LoginController@showLoginForm_v2','as' => 'account.loginForm_v2']);
	Route::post('/login', 					['uses' => 'AuthCliente\LoginController@login', 		'as' => 'account.login']);
	Route::post('/register', 				['uses' => 'AuthCliente\RegisterController@register', 	'as' => 'account.register']);
};

if (App::environment('local')) {
	if (env('APP_LOCALENV') !== null && env('APP_LOCALENV') == true) {
		Route::group([
		], $adminRoutes);

		Route::group([
			'prefix' => 'public',
		], $publicRoutes);
	} else {
		Route::group([
			'domain' => 'admin.expoindustriv2',
//			'middleware' => 'auth:empleados',  // will not work if the login form is inside adminRoutes
		], $adminRoutes);

		Route::group([
			'domain' => 'www2.expoindustriv2',
		], $publicRoutes);

		Route::group([
			'domain' => 'cuenta.expoindustriv2',
		], $accountRoutes);
		Route::group([
			'domain' => 'account.expoindustriv2',
		], $accountRoutes);
		Route::group([
			'domain' => 'konto.expoindustriv2',
		], $accountRoutes);

		Route::get('/', function () {
			return view('frontend.underConstruction');
		});
	}
} else {
	Route::group([
		'domain' => 'admin.expoindustri.com',
//		'prefix' => 'admin',
	], $adminRoutes);

	Route::group([
		'domain' => 'cuenta.expoindustri.com',
	], $accountRoutes);
	Route::group([
		'domain' => 'account.expoindustri.com',
	], $accountRoutes);
	Route::group([
		'domain' => 'konto.expoindustri.com',
	], $accountRoutes);

	Route::group([
		'domain' => 'www.expoindustri.com',
	], $publicRoutes);

	if(preg_match('/expoindustri.se/', request()->url() ) ) {


		Route::group([
			'domain' => 'cuenta.expoindustri.se',
		], $accountRoutes);
		Route::group([
			'domain' => 'account.expoindustri.se',
		], $accountRoutes);
		Route::group([
			'domain' => 'konto.expoindustri.se',
		], $accountRoutes);

		Route::group([
			'domain' => 'expoindustri.se',
		], $publicRoutes);
		Route::group([
			'domain' => 'www.expoindustri.se',
		], $publicRoutes);
	}


//	Route::get('/', function () {
//		return view('frontend.underConstruction');
//	});
}

Route::get('template_light', function() {
	$pub = new stdClass();
	$pub->Owner = new stdClass();

	$content = __('registration.forgot_message', [
		'nombres_apellidos' => 'Nombres apellidos',
		'url_confirm' => "http://localhost/resetPassword/mail/code",
	]);

	$headers = "From: ExpoIndustri <" . strip_tags('system@expoindustri.com') . ">\r\n";
	$headers .= "Reply-To: ExpoIndustri <". strip_tags('system@expoindustri.com') . ">\r\n";
	$headers .= "BCC: marcozs84@gmail.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$ver = phpversion();
	$headers .= "X-Mailer: PHP{$ver}\r\n";
//	$sent = mail('joakimbyren@hotmail.com', 'Email prueba', view('frontend.email.template_light', ['content' => $content, 'titulo' => 'asunto del correo', 'local' => 'en'])->render(), $headers);



	return view('frontend.email.template_light', ['content' => $content, 'titulo' => 'asunto del correo', 'local' => 'en']);
});
Route::get('template_dark', function() {
	$pub = new stdClass();
	$pub->Owner = new stdClass();
	$content = __('registration.forgot_message', [
		'nombres_apellidos' => 'Nombres apellidos',
		'url_confirm' => "http://localhost/resetPassword/mail/code",
	]);

	$headers = "From: ExpoIndustri <" . strip_tags('system@expoindustri.com') . ">\r\n";
	$headers .= "Reply-To: ExpoIndustri <". strip_tags('system@expoindustri.com') . ">\r\n";
	$headers .= "BCC: marcozs84@gmail.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$ver = phpversion();
	$headers .= "X-Mailer: PHP{$ver}\r\n";
//	$sent = mail('info@expoindustri.com', 'Email prueba', view('frontend.email.template_dark', ['content' => $content, 'titulo' => 'asunto del correo', 'local' => 'en'])->render(), $headers);

	return view('frontend.email.template_dark', ['content' => $content, 'titulo' => 'asunto del correo', 'local' => 'en']);
});

Route::get('email_quote/{id?}', ['uses' => 'Frontend\PublicAnnounceController@template_quote', 'as' => 'public.template_quote']);
/*
Route::get('email_quote/{id?}', function($id = 0) {

	$pub = \App\Models\Publicacion::find($id);
	$cot = $pub->Cotizaciones->first()->definicion;
	$cot = \App\Models\Cotizacion::find(7);

//	return view('frontend.email.request_quote_inlined', [
	return view('frontend.email.request_quote', [
		'titulo' => 'Titulo',
		'content' => 'Contenido',
		'pub' => $pub,
		'cot' => $cot
	]);
});
*/

//Route::get('/chart/flot', function () {
//	return view('pages/chart-flot');
//});

