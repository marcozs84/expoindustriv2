<?php
/**
 * Created by PhpStorm.
 * User: MZ
 * Date: 5/30/17
 * Time: 9:16 AM
 */

return [
	[
		'title' => 'Navigation',
		'key' => 'navigation',
		'route' => '',
		'icon' => '',
		'type' => 'header',
	], [
		'title' => 'Dashboard',
		'key' => 'admin',
		'route' => 'admin.home',
		'icon' => 'fa fa-th-large',
		'type' => 'menu',
		'permission' => 'admin.index',
	], [
		'title' => 'Reportes',
		'key' => 'reporte',
		'icon' => 'fa fa-chart-line',
		'type' => 'menu',
//		'badge' => 20,
//		'permission' => [ // <- This works too
//			'admin.reporte.venta',
//			'admin.reporte.oferta'
//		],
		'childs' => [
			[
				'title' => 'Reporte de Ventas',
				'key' => 'reporteVenta',
				'icon' => '',
				'type' => 'menu',
				'route' => 'admin.reporte.venta',
				'permission' => 'admin.reporte.venta',
			], [
				'title' => 'Reporte de Ofertas',
				'key' => 'reporteOferta',
				'icon' => '',
				'type' => 'menu',
				'route' => 'admin.reporte.oferta',
				'permission' => 'admin.reporte.oferta',
			]
		]
	], [
		'title' => 'Ofertas',
		'key' => 'oferta',
		'route' => 'admin.oferta.index',
		'icon' => 'fa fa-envelope',
		'type' => 'menu',
		'permission' => 'admin.oferta.index',
	], [
		'title' => 'Gastos',
		'key' => 'gasto',
		'route' => 'admin.gasto.index',
		'icon' => 'fa fa-dollar-sign',
		'type' => 'menu',
		'permission' => 'admin.gasto.index',
	], [
		'title' => 'Ordenes',
		'key' => 'ordenInbox',
		'route' => 'admin.ordenInbox.index',
		'icon' => 'fa fa-shopping-cart',
		'type' => 'menu',
		'permission' => 'ordenInbox.index.index',
	], [
		'title' => 'Cotizaciones',
		'key' => 'quoteInbox',
		'route' => 'admin.quoteInbox.index',
		'icon' => 'fa fa-tags',
		'type' => 'menu',
//		'badge' => 5,
		'permission' => 'admin.index',
	], [
		'title' => 'Anuncios',
		'key' => 'announceInbox',
		'route' => 'admin.announcementInbox.index',
		'icon' => 'fa fa-tags',
		'type' => 'menu',
//		'badge' => 5,
		'permission' => 'anuncioInbox.index.index',
	], [
		'title' => 'Form. de Contacto',
		'key' => 'searchAlertInbox',
		'route' => 'admin.searchAlertInbox.index',
		'icon' => 'fa fa-tags',
		'type' => 'menu',
//		'badge' => 5,
		'permission' => 'alertaBusquedaInbox.index',
	], [
		'title' => 'Banners',
		'key' => 'banner',
//		'route' => 'admin.home',
		'icon' => 'fab fa-adversal',
		'type' => 'menu',
//		'badge' => 30,
		'permission' => 'admin.banner.index',
		'childs' => [
			[
				'title' => 'Banner superior',
				'key' => 'bannerSuperior',
				'icon' => '',
				'type' => 'menu',
				'route' => 'admin.bannerSuperior.index',
				'permission' => 'admin.bannerSuperior.index',
			], [
				'title' => 'Banner lateral',
				'key' => 'bannerLateral',
				'icon' => '',
				'type' => 'menu',
				'route' => 'admin.bannerLateral.index',
				'permission' => 'admin.bannerLateral.index',
			]
		]
	], [
		'title' => 'Atencion al cliente',
		'key' => 'customerService',
//		'route' => 'admin.home',
		'icon' => 'fab fa-rocketchat',
		'type' => 'menu',
//		'badge' => 20,
		'permission' => 'admin.index',
	], [
		'title' => 'Productos',
		'key' => 'producto',
		'route' => 'admin.producto.index',
		'icon' => 'fa fa-info-circle',
		'type' => 'menu',
		'permission' => 'admin.producto.index',
	], [
		'title' => 'Inventario',
		'key' => 'producto',
		'route' => 'admin.stock.index',
		'icon' => 'fa fa-info-circle',
		'type' => 'menu',
		'permission' => 'admin.producto.index',
	], [
		'title' => 'Agenda',
		'key' => 'agenda',
		'route' => 'admin.agenda.index',
		'icon' => 'fa fa-address-book',
		'type' => 'menu',
		'permission' => 'agenda.index.index',
	], [
		'title' => 'Suscripciones',
		'key' => 'suscripcion',
		'route' => 'admin.subscripcion.index',
		'icon' => 'fa fa-users',
		'type' => 'menu',
//		'badge' => 10,
		'permission' => 'admin.subscripcion.index',
	],[
		'title' => 'Noticias',
		'key' => 'noticia',
		'route' => 'admin.noticia.index',
		'icon' => 'fa fa-newspaper',
		'type' => 'menu',
//		'badge' => 10,
		'permission' => 'noticia.index.index',
	],[
		'title' => 'Divisas',
		'key' => 'noticia',
		'route' => 'admin.divisa.index',
		'icon' => 'fa fa-money-bill-alt',
		'type' => 'menu',
//		'badge' => 10,
		'permission' => 'divisa.index',
	],[
		'title' => 'Transaccion',
		'key' => 'transaccion',
		'route' => 'admin.transaccion.index',
		'icon' => 'fa fa-money-bill-alt',
		'type' => 'menu',
		'permission' => 'transaccion.index',
	],[
		'title' => 'Newsletter',
		'key' => 'newsletter',
		'route' => 'admin.newsletter.index',
		'icon' => 'fa fa-envelope-open-text',
		'type' => 'menu',
		'permission' => '', //'newsletter.index',
	], [
		'title' => 'Administracion',
		'key' => 'admin',
		'route' => 'javascript:;',
		'icon' => 'fa fa-laptop',
		'type' => 'menu',
		'permission' => 'admin.index',
		'childs' => [
			[
				'title' => 'Usuarios',
				'key' => 'usuarios',
				'route' => 'javascript:;',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.usuario.index',
				'childs' => [
					[
						'title' => 'Usuarios',
						'key' => 'usuario',
						'route' => 'admin.usuario.index',
						'icon' => '',
						'type' => 'menu',
						'permission' => 'admin.usuario.index',
					], [
						'title' => 'Roles',
						'key' => 'role',
						'route' => 'admin.role.index',
						'icon' => '', //'fa fa-gears',
						'type' => 'menu',
						'permission' => 'admin.role.index',
					], [
						'title' => 'Permisos',
						'key' => 'permiso',
						'route' => 'admin.permiso.index',
						'icon' => '',
						'type' => 'menu',
						'permission' => 'admin.permiso.index',
					]
				]
			], [
				'title' => 'Actividades',
				'key' => 'actividad',
				'route' => 'admin.actividad.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.actividad.index',
			], [
				'title' => 'Actividades - Tipos',
				'key' => 'actividadTipo',
				'route' => 'admin.actividadTipo.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.actividadTipo.index',
			], [
				'title' => 'Calendarios',
				'key' => 'calendario',
				'route' => 'admin.calendario.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.calendario.index',
			], [
				'title' => 'Campañas FB',
				'key' => 'FBCampaign',
				'route' => 'admin.FBCampaign.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.FBCampaign.index',
			], [
				'title' => 'Cotizaciones',
				'key' => 'orden',
				'route' => 'admin.cotizacion.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.cotizacion.index',
			], [
				'title' => 'Importacion',
				'key' => 'importacion',
				'route' => 'admin.importacion.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.importacion.index',
				'childs' => [
					[
						'title' => 'Anuncios',
						'key' => 'importacion',
						'route' => 'admin.importacion.index',
						'icon' => '',
						'type' => 'menu',
						'permission' => 'admin.importacion.index',
					], [
						'title' => 'Imágenes',
						'key' => 'importacion',
						'route' => 'admin.importacion.imagenes',
						'icon' => '', //'fa fa-gears',
						'type' => 'menu',
						'permission' => 'admin.importacion.imagenes',
					], [
						'title' => 'Archivos',
						'key' => 'importacion',
						'route' => 'admin.importacion.archivos',
						'icon' => '', //'fa fa-gears',
						'type' => 'menu',
						'permission' => 'admin.importacion.archivos',
					]
				]
			], [
				'title' => 'Ordenes',
				'key' => 'orden',
				'route' => 'admin.orden.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.orden.index',
			], [
				'title' => 'Provedores',
				'key' => 'proveedor',
				'route' => 'admin.proveedor.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.proveedor.index',
			],[
				'title' => 'Clientes',
				'key' => 'cliente',
				'route' => 'admin.cliente.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'cliente.index.index',
				'childs' => [
					[
						'title' => 'Clientes',
						'key' => 'usuario',
						'route' => 'admin.cliente.index',
						'icon' => '',
						'type' => 'menu',
						'permission' => 'cliente.index.index',
					],[
						'title' => 'Papelera de Rec.',
						'key' => 'usuario',
						'route' => 'javascript:;',
						'icon' => '',
						'type' => 'menu',
						'permission' => 'cliente.index.index',
					]
				]
			],[
				'title' => 'Anuncios',
				'key' => 'announce',
				'route' => 'admin.anuncio.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.announcement.index',
			],[
				'title' => 'Publicaciones',
				'key' => 'publicacion',
				'route' => 'admin.publicacion.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.publicacion.index',
			],[
				'title' => 'Category Definitions',
				'key' => 'categoryDefinition',
				'route' => 'admin.categoryDefinition.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.categoryDefinition.index',
			],[
				'title' => 'Destinos',
				'key' => 'destino',
				'route' => 'admin.destino.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.destino.index',
			],[
				'title' => 'Transporte',
				'key' => 'transporte',
				'route' => 'admin.transporte.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.transporte.index',
			],[
				'title' => 'Listas',
				'key' => 'lista',
				'route' => 'admin.lista.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.lista.index',
			],[
				'title' => 'Metatags',
				'key' => 'metatag',
				'route' => 'admin.metatag.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.metatag.index',
			],[
				'title' => 'Templates',
				'key' => 'template',
				'route' => 'admin.template.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.template.index',
				'childs' => [
					[
						'title' => 'Custom templates',
						'key' => 'template',
						'route' => 'admin.template.index',
						'icon' => '',
						'type' => 'menu',
						'permission' => 'admin.template.index',
					], [
						'title' => 'Email templates',
						'key' => 'template',
						'route' => 'admin.template.email',
						'icon' => '', //'fa fa-gears',
						'type' => 'menu',
						'permission' => 'admin.template.email',
					]
				]
			],[
				'title' => 'Publicidad',
				'key' => 'usuarios',
				'route' => 'javascript:;',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.usuario.index',
			],[
				'title' => 'Almacenes',
				'key' => 'usuarios',
				'route' => 'javascript:;',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.usuario.index',
			],[
				'title' => 'Calculadora',
				'key' => 'usuarios',
				'route' => 'javascript:;',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.usuario.index',
			],[
				'title' => 'Configuracion',
				'key' => 'usuarios',
				'route' => 'admin.configuracion.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.configuracion.index',
			],[
				'title' => 'Transaccion',
				'key' => 'transaccion',
				'route' => 'admin.transaccion.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.transaccion.index',
			],[
				'title' => 'Galerias',
				'key' => 'galeria',
				'route' => 'admin.galeria.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.galeria.index',
			],[
				'title' => 'Imagenes',
				'key' => 'imagen',
				'route' => 'admin.imagen.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.imagen.index',
			],[
				'title' => 'Marcas',
				'key' => 'marca',
				'route' => 'admin.marca.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.marca.index',
			],[
				'title' => 'Modelos',
				'key' => 'modelo',
				'route' => 'admin.modelo.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.modelo.index',
			],[
				'title' => 'Tags',
				'key' => 'tag',
				'route' => 'admin.tag.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.tag.index',
			],[
				'title' => 'Excepciones',
				'key' => 'excepcion',
				'route' => 'admin.excepcion.index',
				'icon' => '',
				'type' => 'menu',
				'permission' => 'admin.excepcion.index',
			],
		]
	]
];