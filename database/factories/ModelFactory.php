<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Agenda::class, function (Faker\Generator $faker) {
	$fullname = $faker->firstName.' '.$faker->firstName;
	$lastnames = $faker->lastName.' '.$faker->lastName;
	return [
		'nombres' => $fullname,
		'apellidos' => $lastnames,
		'sexo' => $faker->randomElement(['h','m']),
		'idEstadoCivil' => 1,
		'pais' => 'bo',
//		'direccion' => $faker->address,
//		'telefonos' => [$faker->phoneNumber, $faker->phoneNumber],
//		'correos' => [$faker->email, $faker->email],
		'ci' => $faker->numberBetween(1111111, 9999999),
		'nit' => $faker->numberBetween(111111, 999999),
		'nombreFactura' => $fullname.' '.$lastnames,
		'fechaNacimiento' => $faker->dateTimeBetween()
	];
});

$factory->define(App\Models\Lista::class, function (Faker\Generator $faker) {
	return [
		'idPadre' => 0,
		'nombre' => ''
	];
});

$factory->define(App\Models\ListaItem::class, function (Faker\Generator $faker) {
	return [
		'idLista' => 0,
		'texto' => '',
		'valor' => '',
		'orden' => 0,
	];
});

$factory->define(App\Models\Usuario::class, function (Faker\Generator $faker) {
	return [
		'idTipo' => 0,
		'cargo' => 'Nuevo miembro',
		'estado' => 0,
	];
});

$factory->define(App\Models\Empleado::class, function (Faker\Generator $faker) {
	return [
		'idUsuario' => 0,
		'idAgenda' => 0,
		'idEmpleadoTipo' => 0,
		'idEmpleadoEstado' => 0,
		'salario' => 0,
	];
});

$factory->define(App\Models\Categoria::class, function (Faker\Generator $faker) {
	return [
		'idPadre' => 0,
		'nivel' => 0,
		'nombre' => '',
		'descripcion' => '',
		'estado' => 1,
		'visible' => 1,
	];
});

$factory->define(App\Models\CategoriaDefinition::class, function(Faker\Generator $faker) {
	return [
		'idCategoria' => 0,
		'campos' => ''
	];
});

$factory->define(App\Models\Cliente::class, function (Faker\Generator $faker) {
	return [
		'idUsuario' => 0,
		'idAgenda' => 0,
		'idClienteTipo' => 0,
		'idClienteEstado' => 0,
	];
});

$factory->define(App\Models\Configuracion::class, function (Faker\Generator $faker) {
	return [
		'idUsuario' => 0,
		'descripcion' => '',
		'llave' => 0,
		'valor' => 0,
		'default' => 0,
		'esSistema' => 0,
	];
});

$factory->define(App\Models\Destino::class, function (Faker\Generator $faker) {
	return [
		'idPais' => 0,
		'nombre' => '',
		'precio' => 0,
		'estado' => 1,
		'moneda' => 'usd',
	];
});

$factory->define(App\Models\Transporte::class, function (Faker\Generator $faker) {
	return [
		'nombre' => '',
		'descripcion' => '',
		'precio' => 0,
		'esEspecial' => 0,
		'estado' => 1,
	];
});

$factory->define(App\Models\Marca::class, function (Faker\Generator $faker){
	return [
		'nombre' => '',
		'descripcion' => ''
	];
});

$factory->define(App\Models\Modelo::class, function (Faker\Generator $faker){
	return [
		'idMarca' => 0,
		'nombre' => '',
		'descripcion' => ''
	];
});

$factory->define(App\Models\Modulo::class, function (Faker\Generator $faker){
	return [
		'nombre' => '',
	];
});

$factory->define(\App\Models\Noticia::class, function(Faker\Generator $faker) {
	return [
		'titulo_es' => '',
		'resumen_es' => '',
		'titulo_en' => '',
		'resumen_en' => '',
		'titulo_se' => '',
		'resumen_se' => '',
		'fuente' => '',
		'fuenteUrl' => '',
		'fechaPublicacion' => '',
		'pais' => '',
	];
});

$factory->define(App\Models\Pais::class, function(Faker\Generator $faker) {
	return [
		'nombre' => 0,
		'codigo' => '',
		'codigoTelf' => '',
		'bandera' => '',
		'esDestino' => 0
	];
});

$factory->define(App\Models\Permiso::class, function(Faker\Generator $faker) {
	return [
		'nombre' => '',
		'descripcion' => '',
		'estado' => 1,
	];
});

$factory->define(App\Models\ProductoCampo::class, function(Faker\Generator $faker) {
	return [
		'idCategoria' => 0,
		'campo' => ''
	];
});

$factory->define(\App\Models\Proveedor::class, function(Faker\Generator $faker) {
	return [
		'idPadre' => null,
		'idCiudad' => null,
		'nombre' => $faker->company,
		'nit' => $faker->bankAccountNumber,
		'descripcion' => $faker->paragraph(2),
		'idProveedorEstado' => 1,
		'idProveedorTipo' => 0,
		'urlSitio' => $faker->url,
		'urlFacebook' => $faker->url,
		'urlTwitter' => $faker->url,
		'urlGooglePlus' => $faker->url,
	];
});

$factory->define(\App\Models\ProveedorContacto::class, function(Faker\Generator $faker){
	return [
		'idProveedor' => 0,
		'idUsuario' => 0,
		'idAgenda' => 0,
		'idProveedorContactoEstado' => 0,
		'idProveedorContactoTipo' => 0,
	];
});

$factory->define(\App\Models\Telefono::class, function (Faker\Generator $faker){
	return [
		'numero' => $faker->phoneNumber,
		'descripcion' => 'registro',
		'estado' => 1
	];
});

$factory->define(App\Models\Traduccion::class, function (Faker\Generator $faker) {
	return [
		'campo' => '',
		'es' => '',
		'en' => '',
		'se' => '',
	];
});

//$factory->define(App)
