<?php

use Illuminate\Database\Seeder;

class CotizacionEsquemaSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		
		$zonas = [
			'bo' => [
				'nombre' => 'Bolivia',
				'subs' => [
					[
						'nombre' => 'Aduana interior Cochabamba',
						'value' => 100
					],[
						'nombre' => 'Aduana interior La Paz',
						'value' => 200
					],[
						'nombre' => 'Aduana interior Oruro',
						'value' => 300
					],[
						'nombre' => 'Aduana interior Santa Cruz',
						'value' => 400
					]
				]
			],
			'cl' => [
				'nombre' => 'Chile',
				'subs' => [
					[
						'nombre' => 'Zona franca de Iquique',
						'value' => 500
					]
				]
			],
			'pe' => [
				'nombre' => 'Peru',
				'subs' => [
					[
						'nombre' => 'Zona franca de Tacna',
						'value' => 500
					]
				]
			],
		];
		
		$cats = \App\Models\Categoria::where('idPadre', 0)->get();
		$sum = 0.00;
		foreach($cats as $cat){
			$ce = new \App\Models\CotizacionEsquema();
			$ce->idCategoria = $cat->id;
			$ce->nombre = $cat->nombre;
			$sum += 0.10;
			$ce->descripcion = $cat->nombre.' desc';
			$ce->revisionTecnicaPrecio = 100.00 + $sum;
			$ce->fotografiasExtraPrecio = 150.00 + $sum;
			$ce->seguroPrecio = 200.00 + $sum;
			$ce->transporteCompartidoPrecio = 250.00 + $sum;
			$ce->transportePrivadoPrecio = 300.00 + $sum;
			$ce->transporteAbiertoPrecio = 350.00 + $sum;
			$ce->zonasPrecio = $zonas;
			$ce->save();
			$this->command->info('Seed for: '.$cat->nombre);
		}
		
		/*
		$ce = new \App\Models\CotizacionEsquema();
		$ce->nombre = 'Industrial';
		$ce->descripcion = 'Industrial desc';
		$ce->revisionTecnicaPrecio = 100.00;
		$ce->fotografiasExtraPrecio = 150.00;
		$ce->seguroPrecio = 200.00;
		$ce->transporteCompartidoPrecio = 250.00;
		$ce->transportePrivadoPrecio = 300.00;
		$ce->transporteAbiertoPrecio = 350.00;
		$ce->zonasPrecio = $zonas;
		$ce->save();

		$ce = new \App\Models\CotizacionEsquema();
		$ce->nombre = 'Transporte';
		$ce->descripcion = 'Transporte desc';
		$ce->revisionTecnicaPrecio = 100.10;
		$ce->fotografiasExtraPrecio = 150.10;
		$ce->seguroPrecio = 200.10;
		$ce->transporteCompartidoPrecio = 250.10;
		$ce->transportePrivadoPrecio = 300.10;
		$ce->transporteAbiertoPrecio = 350.10;
		$ce->zonasPrecio = $zonas;
		$ce->save();

		$ce = new \App\Models\CotizacionEsquema();
		$ce->nombre = 'Equipo';
		$ce->descripcion = 'Equipo desc';
		$ce->revisionTecnicaPrecio = 100.20;
		$ce->fotografiasExtraPrecio = 150.20;
		$ce->seguroPrecio = 200.20;
		$ce->transporteCompartidoPrecio = 250.20;
		$ce->transportePrivadoPrecio = 300.20;
		$ce->transporteAbiertoPrecio = 350.20;
		$ce->zonasPrecio = $zonas;
		$ce->save();

		$ce = new \App\Models\CotizacionEsquema();
		$ce->nombre = 'Agricola';
		$ce->descripcion = 'Agricola desc';
		$ce->revisionTecnicaPrecio = 100.30;
		$ce->fotografiasExtraPrecio = 150.30;
		$ce->seguroPrecio = 200.30;
		$ce->transporteCompartidoPrecio = 250.30;
		$ce->transportePrivadoPrecio = 300.30;
		$ce->transporteAbiertoPrecio = 350.30;
		$ce->zonasPrecio = $zonas;
		$ce->save();

		$ce = new \App\Models\CotizacionEsquema();
		$ce->nombre = 'Montacarga';
		$ce->descripcion = 'Montacarga desc';
		$ce->revisionTecnicaPrecio = 100.40;
		$ce->fotografiasExtraPrecio = 150.40;
		$ce->seguroPrecio = 200.40;
		$ce->transporteCompartidoPrecio = 250.40;
		$ce->transportePrivadoPrecio = 300.40;
		$ce->transporteAbiertoPrecio = 350.40;
		$ce->zonasPrecio = $zonas;
		$ce->save();

		$ce = new \App\Models\CotizacionEsquema();
		$ce->nombre = 'Otros';
		$ce->descripcion = 'Otros desc';
		$ce->revisionTecnicaPrecio = 100.50;
		$ce->fotografiasExtraPrecio = 150.50;
		$ce->seguroPrecio = 200.50;
		$ce->transporteCompartidoPrecio = 250.50;
		$ce->transportePrivadoPrecio = 300.50;
		$ce->transporteAbiertoPrecio = 350.50;
		$ce->zonasPrecio = $zonas;
		$ce->save();
		*/
		
	}
}
