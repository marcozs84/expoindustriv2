<?php

use Illuminate\Database\Seeder;

class DestinoSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$precio = 0;

		$destino = factory(\App\Models\Destino::class)->create([
			'nombre' => 'Aduana interior Cochabamba',
			'precio' => $precio,
			'idPais' => \App\Models\Pais::where('codigo', 'bo')->first()->id
		]);
//		$precio += 10;

		$destino = factory(\App\Models\Destino::class)->create([
			'nombre' => 'Aduana interior La Paz',
			'precio' => $precio,
			'idPais' => \App\Models\Pais::where('codigo', 'bo')->first()->id
		]);
//		$precio += 10;

		$destino = factory(\App\Models\Destino::class)->create([
			'nombre' => 'Aduana interior Oruro',
			'precio' => $precio,
			'idPais' => \App\Models\Pais::where('codigo', 'bo')->first()->id
		]);
//		$precio += 10;

		$destino = factory(\App\Models\Destino::class)->create([
			'nombre' => 'Aduana interior Santa Cruz',
			'precio' => $precio,
			'idPais' => \App\Models\Pais::where('codigo', 'bo')->first()->id
		]);
//		$precio += 10;

		// ----------- CHILE
//		$precio = 300;

		$destino = factory(\App\Models\Destino::class)->create([
			'nombre' => 'Zona franca de Iquique',
			'precio' => $precio,
			'idPais' => \App\Models\Pais::where('codigo', 'cl')->first()->id
		]);
//		$precio += 10;

		// ----------- PERU
//		$precio = 400;

		$destino = factory(\App\Models\Destino::class)->create([
			'nombre' => 'Zona franca de Tacna',
			'precio' => $precio,
			'idPais' => \App\Models\Pais::where('codigo', 'pe')->first()->id
		]);
//		$precio += 10;

	}
}

