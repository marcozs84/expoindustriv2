<?php

use Illuminate\Database\Seeder;

class ListasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
		// ---------- LISTA: SEXO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'sexo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'h',
			'texto' => 'Hombre'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'm',
			'texto' => 'Mujer'
		]));

		// ---------- LISTA: CLIENTE TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'cliente_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'ClienteTipo1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'ClienteTipo2'
		]));

		// ---------- LISTA: CLIENTE ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'cliente_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'ClienteEstado1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'ClienteEstado2'
		]));

		// ---------- LISTA: EMPLEADO TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'empleado_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'EmpleadoTipo1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'EmpleadoTipo2'
		]));

		// ---------- LISTA: EMPLEADO ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'empleado_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'EmpleadoEstado1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'EmpleadoEstado2'
		]));

		// ---------- LISTA: ESTADO CIVIL
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'estado_civil']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 's',
			'texto' => 'Soltero'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'c',
			'texto' => 'Casado'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'c',
			'texto' => 'Divorciado(a)'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'c',
			'texto' => 'Viudo(a)'
		]));

		// ---------- LISTA: GALERIA TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'galeria_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'GaleriaTipo1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'GaleriaTipo2'
		]));

		// ---------- LISTA: GALERIA ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'galeria_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'GaleriaEstado1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'GaleriaEstado2'
		]));

		// ---------- LISTA: HORARIO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'horario']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => '08:00 a 09:00'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => '09:00 a 10:00'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => '10:00 a 11:00'
		]));

		// ---------- LISTA: DIMENSION (xs, S, M, L)
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'dimension']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'XS',
			'texto' => 'XS'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'S',
			'texto' => 'S'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'M',
			'texto' => 'M'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'L',
			'texto' => 'L'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'XL',
			'texto' => 'XL'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'XXL',
			'texto' => 'XXL'
		]));
		// ---------- LISTA: RECURSO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'recurso']);

//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => 1,
//			'texto' => 'OrdenTipo1'
//		]));
		// ---------- LISTA: MARCA
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'marca']);

//		$i = 1;
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'John Deere'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'Caterpilar'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'Mercedes Benz'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'Iveco'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'Challenger'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'JCB'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'New Holland'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'Krone'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'Ferguson'
//		]));
//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => $i++,
//			'texto' => 'Valtra'
//		]));

		// ---------- LISTA: MODELO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'modelo']);

//		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
//			'valor' => 1,
//			'texto' => 'Modelo1'
//		]));
		// ---------- LISTA: ORDEN TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'orden_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'OrdenTipo1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'OrdenTipo2'
		]));

		// ---------- LISTA: ORDEN ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'orden_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'active'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'closed'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'rejected'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'in_progress'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 5,
			'texto' => 'finished'
		]));

		// ---------- LISTA: PRODUCTO TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'producto_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'Maquinaria'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'Herramienta'
		]));

		// ---------- LISTA: PRODUCTO ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'producto_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'active'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'inactive'
		]));

		// ---------- LISTA: PROVEEDOR TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'proveedor_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'ProveedorTipo1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'ProveedorTipo2'
		]));

		// ---------- LISTA: PROVEEDOR ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'proveedor_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'active'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'inactive'
		]));

		// ---------- LISTA: PROVEEDOR CONTACTO TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'proveedor_contacto_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'ProveedorContactoTipo1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'ProveedorContactoTipo2'
		]));

		// ---------- LISTA: PROVEEDOR CONTACTO ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'proveedor_contacto_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'active'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'inactive'
		]));

		// ---------- LISTA: PUBLICIDAD ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'publicidad_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'active'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'inactive'
		]));

		// ---------- LISTA: PUBLICIDAD ESQUEMA TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'publicidad_esquema_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'PublicidadEsquemaTipo1'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'PublicidadEsquemaTipo2'
		]));

		// ---------- LISTA: USUARIO TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'usuario_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'client'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'provider'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'employee'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'administrator'
		]));

		// ---------- LISTA: USUARIO ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'usuario_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'active'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'inactive'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'awaiting_approval'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'reported'
		]));

		// ---------- LISTA: MONEDA
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'moneda']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'usd',
			'texto' => 'dollars'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'eur',
			'texto' => 'euros'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 'sek',
			'texto' => 'crowns'
		]));

		// ---------- LISTA: PUBLICACION ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'publicacion_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'draft'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'awaiting_approval'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'approved'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'rejected'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 5,
			'texto' => 'disabled'
		]));

		// ---------- LISTA: CONSULTA EXTERNA ESTADO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'consulta_externa_estado']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'new'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'under_review'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'resolved'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'closed'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 5,
			'texto' => 'rejected'
		]));

		// ---------- LISTA: BANNER TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'banner_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'Anuncio'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'Imagen'
		]));

		// ---------- LISTA: TEMPLATE TIPO
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'template_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'Html'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'Pdf'
		]));
		// ---------- LISTA: MODULOS
		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'modulo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'Anuncio'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'PreOrden'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'Orden'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'Publicidad'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 5,
			'texto' => 'Banner'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 6,
			'texto' => 'CRM'
		]));

		// ---------- LISTA: ProductoItem->Disponibilidad

		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'productoItem_disponibilidad']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'Disponible'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'Reservado'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'Vendido'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'Otro'
		]));

		// ---------- LISTA: PAGO_TIPO: Subscripcion ( 32 )

		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'pago_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'Stripe'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'Faktura'
		]));
		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'Subscripcion'
		]));


		// ---------- LISTA: GASTO_TIPO ( 35 )

		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'gasto_tipo']);

    }
}
