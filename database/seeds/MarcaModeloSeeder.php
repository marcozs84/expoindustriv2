<?php

use App\Models\Categoria;
use Illuminate\Database\Seeder;

class MarcaModeloSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'John Deere']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Caterpilar']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Mercedes Benz']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Iveco']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Challenger']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'JCB']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'New Holland']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Krone']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Ferguson']);
//		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Valtra']);


		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'VOLVO']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'SCANIA']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'MERCEDEZ']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'IVECO']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'DAF']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'CAT']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'KOMATSU']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'LIEBHERR']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'JHON DEERE']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'NEW HOLLAND']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'VALTRA']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'MASSEY FERGUSON']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'FIAT ALI']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'KRON']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Atlas copco']);
		$marca = factory(\App\Models\Marca::class)->create(['nombre' => 'Ingersoll rand']);
		
	}
}
