<?php

use Illuminate\Database\Seeder;

class ConfiguracionSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Tipo de cambio SEK',
			'llave' => 'tipo_cambio_sek',
			'valor' => 6.90,
			'default' => 6.90,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Tipo de cambio SEK -> USD',
			'llave' => 'tipo_cambio_sek_usd',
			'valor' => 6.90,
			'default' => 6.90,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Tipo de cambio SEK -> EUR',
			'llave' => 'tipo_cambio_sek_eur',
			'valor' => 6.90,
			'default' => 6.90,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Tipo de cambio EUR -> SEK',
			'llave' => 'tipo_cambio_eur_sek',
			'valor' => 6.90,
			'default' => 6.90,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Tipo de cambio EUR -> USD',
			'llave' => 'tipo_cambio_eur_usd',
			'valor' => 6.90,
			'default' => 6.90,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Tipo de cambio USD -> SEK',
			'llave' => 'tipo_cambio_usd_sek',
			'valor' => 6.90,
			'default' => 6.90,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Tipo de cambio USD -> EUR',
			'llave' => 'tipo_cambio_usd_eur',
			'valor' => 6.90,
			'default' => 6.90,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Precio por dia anunciado',
			'llave' => 'precio_dia_anuncio',
			'valor' => 4.5,
			'default' => 4.5,
			'esSistema' => 1,
		]);

		$configuracion = factory(\App\Models\Configuracion::class)->create([
			'descripcion' => 'Precio por anuncio "Tiempo extendido',
			'llave' => 'precio_anuncio_tiempo_extendido',
			'valor' => 25,
			'default' => 25,
			'esSistema' => 1,
		]);

	}
}

