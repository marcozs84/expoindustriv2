<?php

use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
		// ---------- CATEGORIA: Industrial
		$industrial = factory(\App\Models\Categoria::class)->create([
			'idPadre' => 0,
			'nivel' => 0,
			'nombre' => 'Industrial',
			'descripcion' => '',
			'estado' => 1,
			'visible' => 1
		]);

		$industrial->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Industrial',
			'en' => 'Industrial',
			'se' => 'Industriell',
		]));

		$subIndustrial = [
			'Tornos',
			'Fresas',
			'Cilindradoras',
			'Plegadoras',
			'Guillotinas',
			'Rectificadoras', //'Cepilladoras',
			'Taladros',
			'Soldadoras',
			'Herramientas',
		];
		$subIndustrialEn = [
			'Lathes,',
			'Milling cutter',
			'Cilindradoras',
			'Folders',
			'Guillotines',
			'Grinding machines', //'Cepilladoras',
			'Drills',
			'Welders',
			'Tools',
		];
		$subIndustrialSe = [
			'Svarvar',
			'Fräsmaskiner',
			'Rundvalsar',
			'Pressar',
			'Gradsaxar',
			'Slipmaskiner', //'Cepilladoras',
			'Borrmaskiner',
			'Svetsar',
			'Verktyg',
		];
		for($i = 0 ; $i < count($subIndustrial) ; $i++){
			$item = $subIndustrial[$i];
			$subcat = $industrial->SubCategorias()->save(factory(\App\Models\Categoria::class)->make([
//				'idPadre' => 0,
				'nivel' => 1,
				'nombre' => $item,
				'descripcion' => '',
				'estado' => 1,
				'visible' => 1
			]));

			$subcat->Traduccion()->create([
				'campo' => 'nombre',
				'es' => $item,
				'en' => $subIndustrialEn[$i],
				'se' => $subIndustrialSe[$i],
			]);
		}











		// ---------- CATEGORIA: Transporte
		$transporte = factory(\App\Models\Categoria::class)->create([
			'idPadre' => 0,
			'nivel' => 0,
			'nombre' => 'Transporte',
			'descripcion' => '',
			'estado' => 1,
			'visible' => 1
		]);

		$transporte->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Transporte',
			'en' => 'Transport',
			'se' => 'Transport',
		]));

		$subTransporte = [
			'Camiones > 3,5ton',
			'Camiones < 3 ton',
			'Camion tractor', // 'Trailer',
			'Motores',
			'Otros',
		];
		$subTransporteEn = [
			'Trucks > 3,5ton',
			'Trucks < 3 ton',
			'Tractor trucks', //'Trailer',
			'Engines',
			'Others',
		];
		$subTransporteSe = [
			'Lastbil > 3,5ton',
			'Lastbil < 3 ton',
			'Dragbil', //'Trailer',
			'Motorer',
			'Andra',
		];
		for($i = 0 ; $i < count($subTransporte) ; $i++){
			$item = $subTransporte[$i];
			$subcat = $transporte->SubCategorias()->save(factory(\App\Models\Categoria::class)->make([
//				'idPadre' => 0,
				'nivel' => 1,
				'nombre' => $item,
				'descripcion' => '',
				'estado' => 1,
				'visible' => 1
			]));

			$subcat->Traduccion()->create([
				'campo' => 'nombre',
				'es' => $item,
				'en' => $subTransporteEn[$i],
				'se' => $subTransporteSe[$i],
			]);
		}









		// ---------- CATEGORIA: Equipo
		$equipo = factory(\App\Models\Categoria::class)->create([
			'idPadre' => 0,
			'nivel' => 0,
			'nombre' => 'Equipo',
			'descripcion' => '',
			'estado' => 1,
			'visible' => 1
		]);

		$equipo->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Equipo',
			'en' => 'Equipment',
			'se' => 'Entreprenad',
		]));

		$subEquipo = [
			'Palas frontales',
			'Excavadoras',
			'Orugas',	// NEW
			'Compactadoras',
			'Alza hombres',
			'Trituradoras', //'Chancadoras',
			'Generadores', // NEW
			'Compresoras', // NEW
			'Otros',
		];
		$subEquipoEn = [
			'Palas frontales',
			'Excavadoras',
			'Dozers',	// NEW
			'Compactors',
			'Lift men',
			'Crushers', //'Chancadoras',
			'Generators', // NEW
			'Compressors', // NEW
			'Others',
		];
		$subEquipoSe = [
			'Ljullastare',
			'Grävmaskiner',
			'Bandschaktare',	// NEW
			'Vältar',
			'Liftar',
			'Krossverk', //'Chancadoras',
			'Generatorer', // NEW
			'Kompressorer', // NEW
			'Andra',
		];
		for($i = 0 ; $i < count($subEquipo) ; $i++){
			$item = $subEquipo[$i];
			$subcat = $equipo->SubCategorias()->save(factory(\App\Models\Categoria::class)->make([
//				'idPadre' => 0,
				'nivel' => 1,
				'nombre' => $item,
				'descripcion' => '',
				'estado' => 1,
				'visible' => 1
			]));

			$subcat->Traduccion()->create([
				'campo' => 'nombre',
				'es' => $item,
				'en' => $subEquipoEn[$i],
				'se' => $subEquipoSe[$i],
			]);
		}









		// ---------- CATEGORIA: Agricola
		$agricola = factory(\App\Models\Categoria::class)->create([
			'idPadre' => 0,
			'nivel' => 0,
			'nombre' => 'Agricola',
			'descripcion' => '',
			'estado' => 1,
			'visible' => 1
		]);

		$agricola->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Agricola',
			'en' => 'Agricultural',
			'se' => 'Lantbruk',
		]));

		$subAgricola = [
			'Tractores',
			'Cosechadoras agrícolas', // 'Segadoras',
			'Máquina para Heno y forraje', // 'Enfardadoras',
			'Maquinaria de labranza', // 'Arados',
			'Otros',
		];
		$subAgricolaEn = [
			'Tractores',
			'Agricultural Harvesters', // 'Segadoras',
			'Hay and forage machines', // 'Enfardadoras',
			'Tillage machinery', // 'Arados',
			'Others',
		];
		$subAgricolaSe = [
			'Traktorer',
			'Tröskor', // 'Segadoras',
			'Vallskörd', // 'Enfardadoras',
			'Jordbearbetning', // 'Arados',
			'Andra',
		];
		for($i = 0 ; $i < count($subAgricola) ; $i++){
			$item = $subAgricola[$i];
			$subcat = $agricola->SubCategorias()->save(factory(\App\Models\Categoria::class)->make([
//				'idPadre' => 0,
				'nivel' => 1,
				'nombre' => $item,
				'descripcion' => '',
				'estado' => 1,
				'visible' => 1
			]));

			$subcat->Traduccion()->create([
				'campo' => 'nombre',
				'es' => $item,
				'en' => $subAgricolaEn[$i],
				'se' => $subAgricolaSe[$i],
			]);
		}









		// ---------- CATEGORIA: Montacarga
		$montacarga = factory(\App\Models\Categoria::class)->create([
			'idPadre' => 0,
			'nivel' => 0,
			'nombre' => 'Montacarga',
			'descripcion' => '',
			'estado' => 1,
			'visible' => 1
		]);

		$montacarga->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Montacarga',
			'en' => 'Forklift',
			'se' => 'Truck',
		]));

		$subMontacarga = [
			'Montacargas',
			'Gruas',
			'Otros',
		];
		$subMontacargaEn = [
			'Lift truck',
			'Cranes',
			'Others',
		];
		$subMontacargaSe = [
			'Truckar',
			'Kranar',
			'Andra',
		];
		for($i = 0 ; $i < count($subMontacarga) ; $i++){
			$item = $subMontacarga[$i];
			$subcat = $montacarga->SubCategorias()->save(factory(\App\Models\Categoria::class)->make([
//				'idPadre' => 0,
				'nivel' => 1,
				'nombre' => $item,
				'descripcion' => '',
				'estado' => 1,
				'visible' => 1
			]));

			$subcat->Traduccion()->create([
				'campo' => 'nombre',
				'es' => $item,
				'en' => $subMontacargaEn[$i],
				'se' => $subMontacargaSe[$i],
			]);
		}










		// ---------- CATEGORIA: Otros
		$otros = factory(\App\Models\Categoria::class)->create([
			'idPadre' => 0,
			'nivel' => 0,
			'nombre' => 'Otros',
			'descripcion' => '',
			'estado' => 1,
			'visible' => 1
		]);

		$otros->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Otros',
			'en' => 'Others',
			'se' => 'Andra',
		]));

		$subOtros = [
			'Otros',
		];
		$subOtrosEn = [
			'Others',
		];
		$subOtrosSe = [
			'Andra',
		];
		for($i = 0 ; $i < count($subOtros) ; $i++){
			$item = $subOtros[$i];
			$subcat = $otros->SubCategorias()->save(factory(\App\Models\Categoria::class)->make([
//				'idPadre' => 0,
				'nivel' => 1,
				'nombre' => $item,
				'descripcion' => '',
				'estado' => 1,
				'visible' => 1
			]));

			$subcat->Traduccion()->create([
				'campo' => 'nombre',
				'es' => $item,
				'en' => $subOtrosEn[$i],
				'se' => $subOtrosSe[$i],
			]);
		}

    }
}
