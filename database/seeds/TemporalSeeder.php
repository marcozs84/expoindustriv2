<?php

use Illuminate\Database\Seeder;

class TemporalSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// ---------- LISTA: ProductoItem->Disponibilidad


		$lista = factory(\App\Models\Lista::class)->create(['nombre' => 'consulta_externa_tipo']);

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 1,
			'texto' => 'Form. Contacto'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 2,
			'texto' => 'Banner publicitario'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 3,
			'texto' => 'Alerta de búsqueda'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 4,
			'texto' => 'Servicios: Inspección Técnica'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 5,
			'texto' => 'Servicios: Soporte Logístico'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 6,
			'texto' => 'Servicios: Tienda Online'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 7,
			'texto' => 'Form. Contacto (ROBOT)'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 8,
			'texto' => 'Máquina: Consulta precio'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 9,
			'texto' => 'Subscripción: renovación'
		]));

		$lista->Items()->save(factory(\App\Models\ListaItem::class)->make([
			'valor' => 10,
			'texto' => 'Importación automática'
		]));

	}

}
