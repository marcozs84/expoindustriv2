<?php

use Illuminate\Database\Seeder;

class TransporteSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$precio = 0;

		$destino = factory(\App\Models\Transporte::class)->create([
			'nombre' => 'Contenedor compartido',
			'descripcion' => '(Tiempo de llegada 70-180 días)',
		]);

		$destino->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Contenedor compartido',
			'en' => 'Shared container',
			'se' => 'Delad container',
		]));

		$destino->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'descripcion',
			'es' => '(Tiempo de llegada 70-180 días)',
			'en' => '(ETA 70 to 180 days)',
			'se' => '(ETA 70 to 180 days)',
		]));

		// ----------

		$destino = factory(\App\Models\Transporte::class)->create([
			'nombre' => 'Contenedor exclusivo',
			'descripcion' => '(Tiempo de llegada 65-100 días)',
		]);

		$destino->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Contenedor exclusivo',
			'en' => 'Exclusive container',
			'se' => 'Exklusiv container',
		]));

		$destino->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'descripcion',
			'es' => '(Tiempo de llegada 65-100 días)',
			'en' => '(ETA 65 to 100 days)',
			'se' => '(ETA 65 to 100 days)',
		]));

		// ----------

		$destino = factory(\App\Models\Transporte::class)->create([
			'nombre' => 'Envío abierto',
			'descripcion' => '(Tiempo de llegada 70-100 días)',
			'esEspecial' => 1
		]);

		$destino->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'nombre',
			'es' => 'Envio abierto',
			'en' => 'Open container',
			'se' => 'Öppen container',
		]));

		$destino->Traduccion()->save(factory(\App\Models\Traduccion::class)->make([
			'campo' => 'descripcion',
			'es' => '(Tiempo de llegada 70-100 días)',
			'en' => '(ETA 70 to 100 days)',
			'se' => '(ETA 70 to 100 days)',
		]));


	}
}

