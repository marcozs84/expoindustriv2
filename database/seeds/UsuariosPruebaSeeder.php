<?php

use Illuminate\Database\Seeder;

class UsuariosPruebaSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

    	// ---------- EMPLEADO
    	$u = new \App\Models\Usuario();
    	$u->username = 'admin@mail.com';
    	$u->email = 'admin@mail.com';
    	$u->password = bcrypt('admin');
    	$u->cargo = 'Administrador';
    	$u->estado = 1;

		$a = new \App\Models\Agenda();
		$a->nombres = 'Admin';
		$a->apellidos = '';
		$a->sexo = 'h';
		$a->fechaNacimiento = \Carbon\Carbon::today();

		$e = new \App\Models\Empleado();
		$e->salario = '0.00';
		$e->idEmpleadoEstado = 1;
		$e->save();

		$e->Agenda()->save($a);
		$e->Usuario()->save($u);

//		// ---------- CLIENTE
//
//    	$u = new \App\Models\Usuario();
//    	$u->username = 'cliente@mail.com';
//    	$u->email = 'cliente@mail.com';
//    	$u->password = bcrypt('cliente');
//    	$u->cargo = '';
//    	$u->estado = 1;
//    	$u->save();
//
//		$a = factory(\App\Models\Agenda::class)->make();
//
//		$c = factory(\App\Models\Cliente::class)->create();
//
//		$c->Agenda()->save($a);
////		$c->Usuario()->save($u);
//		$c->idUsuario = $u->id;
//		$c->save();
//
//		// ---------- CLIENTE2
//
//		$u = new \App\Models\Usuario();
//		$u->username = 'cliente2@mail.com';
//		$u->email = 'cliente2@mail.com';
//		$u->password = bcrypt('cliente2');
//		$u->cargo = '';
//		$u->estado = 1;
//		$u->save();
//
//		$a = factory(\App\Models\Agenda::class)->make();
//
//		$c = factory(\App\Models\Cliente::class)->create();
//
//		$c->Agenda()->save($a);
////		$c->Usuario()->save($u);
//		$c->idUsuario = $u->id;
//		$c->save();
//
//		// ---------- CLIENTE3
//
//		$u = new \App\Models\Usuario();
//		$u->username = 'cliente3@mail.com';
//		$u->email = 'cliente3@mail.com';
//		$u->password = bcrypt('cliente3');
//		$u->cargo = '';
//		$u->estado = 1;
//		$u->save();
//
//		$a = factory(\App\Models\Agenda::class)->make();
//
//		$c = factory(\App\Models\Cliente::class)->create();
//
//		$c->Agenda()->save($a);
////		$c->Usuario()->save($u);
//		$c->idUsuario = $u->id;
//		$c->save();

		// ---------- PROVEEDOR

		$u = new \App\Models\Usuario();
		$u->username = 'cliente@mail.com';
		$u->email = 'cliente@mail.com';
		$u->password = bcrypt('cliente');
		$u->cargo = '';
		$u->estado = 1;

		$p = factory(\App\Models\Proveedor::class)->create();
		$a = factory(\App\Models\Agenda::class)->make();
		$pc = factory(\App\Models\ProveedorContacto::class)->make([
			'idProveedor' => $p->id,
			'idAgenda' => $a->id,
			'idUsuario' => $u->id,
		]);

		$pc1 = $p->ProveedorContactos()->save($pc);
		$pc1->Agenda()->save($a);
		$pc1->Usuario()->save($u);
		$pc1->idUsuario = $pc1->Usuario->id;
		$pc1->save();

		$c = factory(\App\Models\Cliente::class)->create();
		$c->idUsuario = $pc1->Usuario->id;
		$c->save();

			$p->Telefonos()->save(factory(\App\Models\Telefono::class)->make([
			'descripcion' => 'registro'
		]));
		$pc1->Agenda->Telefonos()->save(factory(\App\Models\Telefono::class)->make([
			'descripcion' => 'registro'
		]));

    }
}
