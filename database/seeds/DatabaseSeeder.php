<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		$this->call(ConfiguracionSeeder::class);
		$this->call(PaisSeeder::class);
		$this->call(ListasSeeder::class);
		$this->call(UsuariosPruebaSeeder::class);
		$this->call(CategoriaSeeder::class);
		$this->call(CotizacionEsquemaSeeder::class);
		$this->call(CategoriaDefinitionSeeder::class);
		$this->call(MarcaModeloSeeder::class);
		$this->call(DestinoSeeder::class);
		$this->call(TransporteSeeder::class);
		$this->call(PermisoSeeder::class);
    }
}
