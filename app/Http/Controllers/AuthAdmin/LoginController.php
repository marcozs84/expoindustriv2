<?php

namespace App\Http\Controllers\AuthAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

//    protected $gu

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
//		$this->middleware('guest:empleados')->except('logout');
    }

	protected function guard() {
		return Auth::guard('empleados');
	}

	/**
	 * Show the application's login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showLoginForm() {
		if (Auth::guard('empleados')->check()) {
			return redirect()->intended($this->redirectPath());
		}
		return view('admin.pages.auth.loginForm');
	}

	/**
	 * Get the login username to be used by the controller.
	 *
	 * @return string
	 */
	public function username() {
		return 'email';
	}

//	public function redirectTo(){
////		$parts = explode('.', $_SERVER['SERVER_NAME']);
////		array_shift($parts);
////		$uriDomain = implode('.', $parts);
////		return redirect()->route('public.home', $uriDomain);
//		// return '/home';
//		return route('admin.home');
//	}


	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
	 */
	public function login(Request $request)
	{
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if ($this->hasTooManyLoginAttempts($request)) {

			if($request->ajax()){
				$seconds = $this->limiter()->availableIn(
					$this->throttleKey($request)
				);

				$message = Lang::get('auth.throttle', ['seconds' => $seconds]);
				$response = [
					$this->username() => $message
				];
				return response()->make($response, 422);
			}

			$this->fireLockoutEvent($request);
			return $this->sendLockoutResponse($request);
		}

		if ($this->attemptLogin($request)) {
			if($request->ajax()){
				$response = Array(
					'result' => true,
					'message' => 'Logeado exitosamente.',
					'data' => []
				);
				return response()->json($response);
			}
			return $this->sendLoginResponse($request);
		}

		if($request->ajax()){
			$errors = [$this->username() => trans('auth.failed')];

//			if ($request->expectsJson()) {
				return response()->json($errors, 422);
//			}
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}

	/**
	 * Validate the user login request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return void
	 */
	protected function validateLogin(Request $request)
	{
		$this->validate($request, [
			$this->username() => 'required|string|email',
			'password' => 'required|string',
		]);
	}
}
