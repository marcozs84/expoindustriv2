<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;

use App\Models\Configuracion;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function reportDev($mensaje, $caller = null){
		$mail = [];
		$mail['titulo'] = "";
		$mail['asunto'] = "";
		$mail['to'] = [];
		$mail['cc'] = [];
		$mail['bcc'] = [];
		$mail['attachments'] = [];

		$mail['asunto'] = "Alerta Sitio: ExpoIndustri";
		$mail['to'][] = 'marcozs84@gmail.com';


		Mail::send('frontend.email.template_light', [
			'titulo' => 'ALERTA',
			'content' => $mensaje,
			'local' => session('visitor_location', 'en')
		], function ($m) use ($mail) {
			$m->from('info@expoindustri.com', 'ExpoIndustri');
//			$m->replyTo('info@expoindustri.com', 'Soporte ExpoIndustri');
			$m->subject($mail['asunto']);
			$m->to($mail['to']);
		});


//		if(env('APP_ENV') == 'local'){
//			Mail::send('frontend.email.template_light', [
//				'content' => $mensaje,
//				'titulo' => 'ALERTA!',
//				'local' => session('visitor_location', 'en')
//			], function ($m) use ($mail) {
//
//				$m->from('info@expoindustri.com', 'ExpoIndustri');
//				$m->replyTo('info@expoindustri.com', 'Soporte ExpoIndustri');
//				$m->subject($mail['asunto']);
//				$m->to($mail['to']);
//			});
//		} else {
//			$headers = "From: " . strip_tags('no-reply@expoindustri.com') . "\r\n";
//			$headers .= "Reply-To: ". strip_tags('no-reply@expoindustri.com') . "\r\n";
////			$headers .= "BCC: marcozs84@gmail.com\r\n";
//			$headers .= "MIME-Version: 1.0\r\n";
//			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//			$sent = mail(implode(',', $mail['to']), $mail['asunto'], view('frontend.email.template_light', [
//				'titulo' => 'ALERTA!',
//				'content' => $mensaje,
//			])->render(), $headers);
//		}

	}

	var $mat = null;
	var $exchange = 0;

	public function exchange($currencyFrom, $currencyTo = null, $amount) {

		if($amount > 0) {

		} else {
			return 0;
		}

		if($currencyTo == null) {
			$currencyTo = session('current_currency', 'usd');
		}

		$currencyFrom = strtolower( $currencyFrom );
		$currencyTo = strtolower( $currencyTo );

		if($currencyFrom != $currencyTo) {

			$cambios = Configuracion::all();

			$llave = "tipo_cambio_{$currencyFrom}_{$currencyTo}";
			$exchange = $cambios->where('llave', $llave)->first()->valor;

			$new_amount = 0;

/*
			switch($llave) {
				case 'tipo_cambio_eur_usd':
				case 'tipo_cambio_sek_usd':
				case 'tipo_cambio_usd_eur':
				case 'tipo_cambio_sek_eur':
				case 'tipo_cambio_eur_sek':
				case 'tipo_cambio_usd_sek':
					break;
			}
 */
			$new_amount = round($amount * $exchange, 2);
//			$new_amount = number_format($amount * $exchange, 2, '.', ',');

			if(($amount != 0) && ($new_amount == 0 || $new_amount < 0)) {
				$linea = __LINE__;
				$file = __FILE__;
				$this->reportDev("Cambio de divisa esta retornando 0, <br>Llave: {$llave}<br>Tipo de cambio: {$exchange}<br>Archivo: {$file}:{$linea}");
			}

			return $new_amount;
		} else {
			return $amount;
		}
	}

	public function randomChars($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function check($param, $alternative = null) {
		return ( isset($param) ? trim($param) : $alternative );
	}

	function getPureSql($sql, $binds) {
		$result = "";

		$sql_chunks = explode('?', $sql);
		foreach ($sql_chunks as $key => $sql_chunk) {
			if (isset($binds[$key])) {
				$result .= $sql_chunk . '\'' . $binds[$key] . '\'';
			}
		}

		$result .= end($sql_chunks);

		return $result;
	}
}
