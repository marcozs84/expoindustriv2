<?php

namespace App\Http\Controllers\Resources;

use App\Models\Subscripcion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscripcionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
        	'idSubscripcionTipo' => 'required',
			'idUsuario' => 'required',
			'fechaDesde' => 'required',
			'fechaHasta' => 'required',
		]);

        $sub = new Subscripcion();
        $sub->idSubscripcionTipo = (int) $request['idSubscripcionTipo'];
        $sub->idUsuario = (int) $request['idUsuario'];
//        $sub->fechaDesde = Carbon::parse($request['fechaDesde']);
        $sub->fechaDesde = Carbon::createFromFormat('d/m/Y', $request['fechaDesde']);
        $sub->fechaHasta = Carbon::createFromFormat('d/m/Y', $request['fechaHasta']);
        $saved = $sub->save();
        if($saved) {
			$response = Array(
				'result' => true,
				'message' => 'Registro creado correctamente.',
				'object' => $sub->toArray()
			);
			return response()->json($response);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'Error al crear el registro.',
				'object' => []
			);
			return response()->json($response);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Subscripcion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Subscripcion::select(['*']);
		$builder->with([
			'Tipo',
			'Usuario'
		]);
//		$builder->with([
//			'Producto.ProductoItem.Ubicacion',
//			'Producto.Categorias',
//			'Producto.Marca',
//			'Producto.Modelo',
//			'Producto.Categorias.Padre',
//			'Producto.Galerias.Imagenes',
//			'Owner.Owner.Agenda'
//		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
//			if($request['filtro'] == 'aprobados'){
//				$builder->where('estado', Publicacion::ESTADO_APPROVED);
//			}
		}

//		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
//		}

		if(isset($request['draw'])) {	// llamada DataTable
//			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
