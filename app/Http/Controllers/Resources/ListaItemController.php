<?php

namespace App\Http\Controllers\Resources;

use App\Models\ListaItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListaItemController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->validate($request, [
		    'idLista'   => 'numeric',
			'texto'     => '',
			'valor'     => '',
			'orden'     => 'numeric',
			
		]);

		$listaItem = ListaItem::create($request->all());

		if($listaItem){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'object' => $listaItem
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'object' => $request->all()
			);
		}

		return response()->json($response);

	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idLista'   => 'numeric',
			'texto'     => '',
			'valor'     => '',
			'orden'     => 'numeric',
			
		]);

		$listaItem = ListaItem::findOrFail($id);

		if( isset($request['idLista']) )  $listaItem->idLista   = (int) $request['idLista'];
		if( isset($request['texto']) )    $listaItem->texto     = trim($request['texto']);
		if( isset($request['valor']) )    $listaItem->valor     = trim($request['valor']);
		if( isset($request['orden']) )    $listaItem->orden     = (int) $request['orden'];
		
		$listaItem->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'object' => $listaItem
		);
		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
	 * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = ListaItem::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){

    	$paging = true;
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);

			if ( $request['length'] < 0 ) {
				$paging = false;
			}
		}

		if ( isset( $request['paging'] ) ) {
			$paging = ( $request['paging'] == 1 ) ? true : false;
		}

		$builder = ListaItem::select(['*']);
		$builder->orderBy( 'orden', 'asc' );
		$builder->orderBy( 'texto', 'asc' );

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			//$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if ( isset( $request['idLista'] ) ) {
			$builder->where( 'idLista', (int) $request['idLista'] );
		}

		if(isset($request['draw'])) {	// llamada DataTable
//			$builder->orderBy('id', 'asc');
			if ( $paging ) {
				$ds = $builder->paginate($request['length'], ['*']);
			} else {
				$ds = $builder->get();

				$response = [
					'result' => true,
					'message' => '',
					'data' => $ds,
					'recordsTotal' => $ds->count(),
					'recordsFiltered' => $ds->count(),
					'draw' => $request['draw']
				];

				return response()->json($response);
			}

		} else {		// llamada Select2
			if ( $paging ) {
				return $builder->paginate( $request['length'], [ '*' ] );
			} else {
				$ds = $builder->get();

				$response = [
					'result' => true,
					'message' => '',
					'data' => $ds,
					'recordsTotal' => $ds->count(),
					'recordsFiltered' => $ds->count(),
					'draw' => $request['draw']
				];

				return response()->json($response);
			}
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function restore(Request $request) {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);
        $ids = $request['ids'];

        $count = 0;
        $listaItems = ListaItem::whereIn('id', $ids)->withTrashed()->get();
        foreach($listaItems as $listaItem) {
            if($listaItem->restore()) {
                $count++;
            }
        }

        if($count == count($ids)){
            $result = Array(
                'result' => true,
                'message' => 'Registro(s) restaurados(s) correctamente.',
                'request' => $request->all()
            );
        } else {
            $result = Array(
                'result' => false,
                'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
                'request' => $request->all()
            );
        }
        return response()->json($result);
    }

}
