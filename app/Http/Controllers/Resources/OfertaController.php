<?php

namespace App\Http\Controllers\Resources;

use App\Models\Oferta;
use App\Models\Role;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class OfertaController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->validate($request, [
			'idProductoItem' => 'required',
			'idVendedor' => 'required|numeric',
			'idCliente' => 'required|numeric',
			'idOrden' => '',
			'noOferta' => '',
			'monto' => '',
			'fechaEnvio' => '',
			'tipoVisita' => '',
			'estado' => '',
		]);

		$modelo = new Oferta();

		if(isset($request['idProductoItem']) && $request['idProductoItem'] > 0) {
			$modelo->idProductoItem = (int) trim($request['idProductoItem']);
		}
		if(isset($request['idVendedor'])) {
			$modelo->idVendedor = (int) trim($request['idVendedor']);
		}
		if(isset($request['idCliente'])) {
			$modelo->idCliente = (int) trim($request['idCliente']);
		}
		if(isset($request['idOrden'])) {
			$modelo->idOrden = (int) trim($request['idOrden']);
		}

		$seq = Oferta::where('idVendedor', $modelo->idVendedor)
			->max('noOferta');

		$modelo->noOferta = ( !$seq ) ? 1 : $seq + 1;

		if(isset($request['monto']) && $request['monto'] > 0) {
			$modelo->monto = trim($request['monto']);
		} else {
			$modelo->monto = 0;
		}

		if(isset($request['fechaEnvio'])) {
			$modelo->fechaEnvio = Carbon::createFromFormat('d/m/Y', $request['fechaEnvio']);
		}
		if(isset($request['esClienteNuevo'])) {
			$modelo->esClienteNuevo = (int) $request['nuevoCliente'];
		}
		if(isset($request['telefono'])) {
			$modelo->telefono = trim($request['telefono']);
		}
		if(isset($request['lugar'])) {
			$modelo->lugar = trim($request['lugar']);
		}
		if(isset($request['email'])) {
			$modelo->email = trim($request['email']);
		}
		if(isset($request['estado'])) {
			$modelo->estado = (int) $request['estado'];
		} else {
			$modelo->estado = 1;
		}
		if(isset($request['observaciones'])) {
			$modelo->observaciones = trim($request['observaciones']);
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
	 * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if(Gate::denies('admin.gasto.delete')){
			$response = [
				'Permiso' => [
					'No tiene permisos para ejecutar esta acción'
				]
			];
			return response()->make($response, 422);
		}

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Oferta::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Oferta::select(['*']);
		$builder->with([
			'ProductoItem.Producto.Publicacion',
			'ProductoItem.Producto.Marca',
			'ProductoItem.Producto.Modelo',
			'Cliente.Agenda',
			'Vendedor.Owner.Agenda',
		]);

		if ( isset( $request['fechaInicio'] ) ) {
			$fechaInicio = Carbon::createFromFormat( 'Y/m/d', $request['fechaInicio'] );
			$builder->where( 'created_at', '>=', "{$fechaInicio->format('Y/m/d')}" );
		}

		if ( isset( $request['fechaFin'] ) ) {
			$fechaFin = Carbon::createFromFormat( 'Y/m/d', $request['fechaFin'] );
			$builder->where( 'created_at', '<', "{$fechaFin->addDays(1)->format('Y/m/d')}" );
		}

		if ( isset( $request['idVendedor'] ) ) {
			if ( is_array( $request['idVendedor'] ) ) {
				$vendedorIds = $request['idVendedor'];
			} else {
				$vendedorIds = [ $request['idVendedor'] ];
			}
			$builder->whereIn( 'idVendedor', $vendedorIds );
		}

		if ( Gate::denies( 'admin.oferta.adminTerceros' ) ) {
			$builder->where( 'idVendedor', Auth::user()->id );
		}

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){

			$ids = DB::select(
				"SELECT
				  pi.id as id
				FROM
				  Producto p
				  INNER JOIN Marca ma ON p.idMarca = ma.id
				  INNER JOIN Modelo mo ON p.idModelo = mo.id
				  INNER JOIN ProductoItem pi ON p.id = pi.idProducto
				WHERE
				  CONCAT_WS(' ', ma.nombre, mo.nombre) LIKE '%{$searchValue}%' OR
				  CONCAT_WS(' ', mo.nombre, ma.nombre) LIKE '%{$searchValue}%'"
			// CONCAT(ma.nombre,' ', mo.nombre) LIKE '%{$searchValue}%'"
			);

			$ids = array_pluck($ids, 'id');
			$builder->whereIn('idProductoItem', $ids);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function getFlotData(Request $request){

		$this->validate($request, [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
			'vendedoresId' => '',
			'agrupacion' => '',
		]);

		// ---------- FECHAS -> TICKS

		$fechaInicio = Carbon::parse($request['fechaInicio']);
		$fechaFin = Carbon::parse($request['fechaFin']);

		$vendedores = [];
		if(isset($request['vendedoresId'])) {
//			$vendedores = $request['vendedoresId'];
			$vendedores = Usuario::with([
				'Owner.Agenda',
			])
				->whereIn('id', $request['vendedoresId'])->get();
		} else {
			$vendedores = Usuario::with([
				'Roles',
				'Owner.Agenda',
			])
				->whereHas('Roles', function($q) {
					$q->where('id', Role::VENDEDOR);
				})
				->get();
		}

		if(!$fechaInicio->lessThanOrEqualTo($fechaFin)) {
			$response = [
				'Calendario' => [
					'Fecha de inicio debe ser menor a la fecha final'
				]
			];
			return response()->make($response, 422);
		}

		$ticks = [];
		$ticks2 = [];

		for( $fechaIt = clone $fechaInicio ; $fechaIt->lessThanOrEqualTo($fechaFin) ; $fechaIt->addDay(1)){
			$ticks[] = $fechaIt->format('d.m.Y');
			$ticks2[] = $fechaIt->format('M j \'y');
		}

		$res0 = [];
		$res1 = [];
		$res2 = [];
		$res3 = [];
		$res4 = [];
		$res5 = [];

//		DB::enableQueryLog();

		$select = [
			DB::raw("count('id') as count"),
			DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
		];
		$groupBy = [ DB::raw('date') ];

		if($request['agrupacion'] == 'individual') {
			$select = [
				'idVendedor',
				DB::raw("count('id') as count"),
				DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
			];
			$groupBy = [ 'idVendedor', DB::raw('date') ];
		}

		$builder = Oferta::select($select);
		$builder->where('created_at', '>=', "{$fechaInicio->format('Y/m/d')}")
			->where('created_at', '<', "{$fechaFin->addDays(1)->format('Y/m/d')}");

		if(count($vendedores) > 0) {
			$builder->whereIn('idVendedor', $vendedores->pluck('id')->toArray());
		}

		$builder->groupBy($groupBy)
			->orderBy('created_at');

		$result = $builder->get();

//		logger(DB::getQueryLog());

//		$com1 = $result2->toArray();
//		$com1 = $result->where('puntos', 1)->toArray();
//		$com2 = $result->where('puntos', 2)->toArray();
//		$com3 = $result->where('puntos', 3)->toArray();
//		$com4 = $result->where('puntos', 4)->toArray();
//		$com5 = $result->where('puntos', 5)->toArray();

		$resultados = [];

		if($request['agrupacion'] == 'individual') {
			foreach($vendedores as $vendedor) {
				$datos = $result->where('idVendedor', $vendedor->id)->toArray();
				$datosVendedor = $this->flot_format($datos, $ticks);
				$datosVendedorSpark = $this->spark_format($datos, $ticks);
				$resultados[] = [
					'id' => $vendedor->id,
					'nombre' => $vendedor->Owner->Agenda->nombres_apellidos,
					'datos' => $datosVendedor,
					'spark' => $datosVendedorSpark,
				];
			}
		} else {
			$com0 = $result->toArray();
			$resGrupal = $this->flot_format($com0, $ticks);
			$resSpark = $this->spark_format($com0, $ticks);
			$resultados[] = [
				'id' => 0,
				'nombre' => 'General',
				'datos' => $resGrupal,
				'spark' => $resSpark,
			];
		}

		$data = [
			'datos' => $resultados,
//			'datos' => [
//				[
//					'id' => 1,
//					'nombre' => 1,
//					'data' => $datos[],
//				]
//			],
			'ticks' => $ticks2
		];
		$response = Array(
			'result' => true,
			'message' => 'Descarga de datos correcta.',
			'data' => $data
		);
		return response()->json($response);

		//return response()->json($data);
	}

	private function flot_format($data, $ticks){
		$count = 0;
		$result = [];
//		foreach($data as $ar){
//			$pos = array_search($ar['date'], $ticks);
//			$result[] = [$pos, $ar['count'], $ar['date']];
//			//         (  x  ,      y      ,  otros adicionales )
//			$count++;
//		}


		for($i = 0 ; $i < count($ticks) ; $i++) {

			$countD = 0;
			foreach($data as $ar) {
				if($ar['date'] == $ticks[$i]) {
					$countD = $ar['count'];
				}
			}
			$result[] = [$i, $countD, $ticks[$i]];
			//         (  x  ,  y   ,  otros adicionales )
		}

		return $result;
	}

	private function spark_format($data, $ticks){
		$count = 0;
		$result = [];

		for($i = 0 ; $i < count($ticks) ; $i++) {
			$countD = 0;
			foreach($data as $ar) {
				if($ar['date'] == $ticks[$i]) {
					$countD = $ar['count'];
				}
			}
			$result[] = $countD;
		}

		return $result;
	}
}
