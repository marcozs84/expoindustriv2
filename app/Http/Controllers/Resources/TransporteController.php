<?php

namespace App\Http\Controllers\Resources;

use App\Models\Transporte;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransporteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'nombre'        => '',
			'descripcion'   => '',
			'precio'        => 'numeric',
			'esEspecial'    => 'numeric',
			'estado'        => 'numeric',
		]);

		//$transporte = Transporte::create($request->all());
		$transporte = new Transporte();
		$transporte->nombre        = trim($request['nombre']);
		$transporte->descripcion   = trim($request['descripcion']);
		$transporte->precio        = $request['precio'];
		$transporte->esEspecial    = (int) $request['esEspecial'];
		$transporte->estado        = (int) $request['estado'];
		
		$transporte->save();

		if($transporte){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $transporte
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'nombre'        => '',
			'descripcion'   => '',
			'precio'        => 'numeric',
			'esEspecial'    => 'numeric',
			'estado'        => 'numeric',
		]);

		$transporte = Transporte::findOrFail($id);

		if( isset($request['nombre']) )       $transporte->nombre        = trim($request['nombre']);
		if( isset($request['descripcion']) )  $transporte->descripcion   = trim($request['descripcion']);
		if( isset($request['precio']) )       $transporte->precio        = $request['precio'];
		if( isset($request['esEspecial']) )   $transporte->esEspecial    = (int) $request['esEspecial'];
		if( isset($request['estado']) )       $transporte->estado        = (int) $request['estado'];
		$transporte->save();

		$traduccion = $transporte->Traduccion->where('campo', 'nombre')->first();
		if( !$traduccion ) {
			$traduccion = $transporte->Traduccion()->create([
				'campo' => 'nombre',
				'es' => '',
				'se' => '',
				'en' => '',
			]);
		}

		if( isset($request['nombre_es']) )    $traduccion->es     = $request['nombre_es'];
		if( isset($request['nombre_se']) )    $traduccion->se     = $request['nombre_se'];
		if( isset($request['nombre_en']) )    $traduccion->en     = $request['nombre_en'];
		$traduccion->save();

		$traduccion = $transporte->Traduccion->where('campo', 'descripcion')->first();
		if( !$traduccion ) {
			$traduccion = $transporte->Traduccion()->create([
				'campo' => 'descripcion',
				'es' => '',
				'se' => '',
				'en' => '',
			]);
		}
		if( isset($request['desc_es']) )    $traduccion->es     = $request['desc_es'];
		if( isset($request['desc_se']) )    $traduccion->se     = $request['desc_se'];
		if( isset($request['desc_en']) )    $traduccion->en     = $request['desc_en'];
		$traduccion->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $transporte
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Transporte::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$transportes = Transporte::whereIn('id', $ids)->withTrashed()->get();
		foreach($transportes as $transporte) {
			if($transporte->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Transporte::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			//$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('nombre', 'asc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
