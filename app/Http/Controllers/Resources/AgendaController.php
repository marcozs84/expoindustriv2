<?php

namespace App\Http\Controllers\Resources;

use App\Models\Agenda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgendaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->validate($request, [
			'idEstadoCivil' => '',
			'nombres' => 'required',
			'apellidos' => '',
			'sexo' => '',
			'ci' => '',
			'pais' => '',
			'nit' => '',
			'nombreFactura' => '',
			'puntosAcumulados' => '',
			'fechaNacimiento' => '',
		]);

		$modelo = Agenda::create($request->all());

		if($modelo){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'object' => $modelo
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'object' => $request->all()
			);
		}

		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idEstadoCivil' => '',
			'nombres' => '',
			'apellidos' => '',
			'sexo' => '',
			'ci' => '',
			'pais' => '',
			'nit' => '',
			'nombreFactura' => '',
			'puntosAcumulados' => '',
			'fechaNacimiento' => '',
		]);

		$modelo = Agenda::findOrFail($id);

		if(isset($request['idEstadoCivil'])) {
			$modelo->idEstadoCivil = (int) $request['idEstadoCivil'];
		}
		if(isset($request['nombres'])) {
			$modelo->nombres = trim($request['nombres']);
		}
		if(isset($request['apellidos'])) {
			$modelo->apellidos = trim($request['apellidos']);
		}
		if(isset($request['sexo'])) {
			$modelo->sexo = trim($request['sexo']);
		}
		if(isset($request['ci'])) {
			$modelo->ci = trim($request['ci']);
		}
		if(isset($request['pais'])) {
			$modelo->pais = trim($request['pais']);
		}
		if(isset($request['nit'])) {
			$modelo->nit = trim($request['nit']);
		}
		if(isset($request['nombreFactura'])) {
			$modelo->nombreFactura = trim($request['nombreFactura']);
		}
		if(isset($request['puntosAcumulados'])) {
			$modelo->puntosAcumulados = trim($request['puntosAcumulados']);
		}
		if(isset($request['fechaNacimiento'])) {
			$modelo->fechaNacimiento = trim($request['fechaNacimiento']);
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Agenda::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}
}
