<?php

namespace App\Http\Controllers\Resources;

use App\Models\Transaccion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransaccionController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'owner_type'            => '',
			'owner_id'              => 'numeric',
			'idTransaccionTipo'     => 'numeric',
			'idUsuario'             => 'numeric',
			'idStripeCustomer'      => '',
			'idStripeTransaction'   => '',
			'producto'              => '',
			'monto'                 => 'numeric',
			'moneda'                => '',
			'estado'                => '',
			'extrainfo'             => '',
			
		]);

		$transaccion = Transaccion::create($request->all());

		if($transaccion){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $transaccion
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'owner_type'            => '',
			'owner_id'              => 'numeric',
			'idTransaccionTipo'     => 'numeric',
			'idUsuario'             => 'numeric',
			'idStripeCustomer'      => '',
			'idStripeTransaction'   => '',
			'producto'              => '',
			'monto'                 => 'numeric',
			'moneda'                => '',
			'estado'                => '',
			'procesada'             => '',
			'extrainfo'             => '',
			
		]);

		$transaccion = Transaccion::findOrFail($id);

		if( isset($request['owner_type']) )           $transaccion->owner_type            = trim($request['owner_type']);
		if( isset($request['owner_id']) )             $transaccion->owner_id              = (int) $request['owner_id'];
		if( isset($request['idTransaccionTipo']) )    $transaccion->idTransaccionTipo     = (int) $request['idTransaccionTipo'];
		if( isset($request['idUsuario']) )            $transaccion->idUsuario             = (int) $request['idUsuario'];
		if( isset($request['idStripeCustomer']) )     $transaccion->idStripeCustomer      = trim($request['idStripeCustomer']);
		if( isset($request['idStripeTransaction']) )  $transaccion->idStripeTransaction   = trim($request['idStripeTransaction']);
		if( isset($request['producto']) )             $transaccion->producto              = trim($request['producto']);
		if( isset($request['monto']) )                $transaccion->monto                 = $request['monto'];
		if( isset($request['moneda']) )               $transaccion->moneda                = trim($request['moneda']);
		if( isset($request['estado']) )               $transaccion->estado                = trim($request['estado']);
		if( isset($request['procesada']) )            $transaccion->procesada             = trim($request['procesada']);
		if( isset($request['extrainfo']) )            $transaccion->extrainfo             = trim($request['extrainfo']);
		
		$transaccion->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $transaccion
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Transaccion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$transaccions = Transaccion::whereIn('id', $ids)->withTrashed()->get();
		foreach($transaccions as $transaccion) {
			if($transaccion->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Transaccion::select(['*']);
		$builder->with([
			'Usuario.Owner.Agenda',
			'TransaccionTipo',
			'TransaccionEstado',
		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			//$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function pendientes(Request $request) {

	}

}
