<?php

namespace App\Http\Controllers\Resources;

use App\Models\Galeria;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GaleriaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idGaleriaTipo'   => 'numeric',
			'owner_type'      => '',
			'owner_id'        => 'numeric',
			'nombre'          => '',
			'descripcion'     => '',
			'estado'          => 'numeric',
			
		]);

		//$galeria = Galeria::create($request->all());
		$galeria = new Galeria();
		$galeria->idGaleriaTipo   = (int) $request['idGaleriaTipo'];
		$galeria->owner_type      = trim($request['owner_type']);
		$galeria->owner_id        = (int) $request['owner_id'];
		$galeria->nombre          = trim($request['nombre']);
		$galeria->descripcion     = trim($request['descripcion']);
		$galeria->estado          = (int) $request['estado'];
		
		$galeria->save();

		if($galeria){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $galeria
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idGaleriaTipo'   => 'numeric',
			'owner_type'      => '',
			'owner_id'        => 'numeric',
			'nombre'          => '',
			'descripcion'     => '',
			'estado'          => 'numeric',
			
		]);

		$galeria = Galeria::findOrFail($id);

		if( isset($request['idGaleriaTipo']) )  $galeria->idGaleriaTipo   = (int) $request['idGaleriaTipo'];
		if( isset($request['owner_type']) )     $galeria->owner_type      = trim($request['owner_type']);
		if( isset($request['owner_id']) )       $galeria->owner_id        = (int) $request['owner_id'];
		if( isset($request['nombre']) )         $galeria->nombre          = trim($request['nombre']);
		if( isset($request['descripcion']) )    $galeria->descripcion     = trim($request['descripcion']);
		if( isset($request['estado']) )         $galeria->estado          = (int) $request['estado'];
		
		$galeria->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $galeria
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Galeria::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$galerias = Galeria::whereIn('id', $ids)->withTrashed()->get();
		foreach($galerias as $galeria) {
			if($galeria->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Galeria::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

}
