<?php

namespace App\Http\Controllers\Resources;

use App\Models\Orden;
use App\Models\OrdenDetalle;
use App\Models\Producto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class OrdenController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {


		/* ========================================================================================
		 *
		 * La creación de ordenes está ligada a otras entidades como:
		 * Producto, ProductoItem, OrdenDetalle
		 * Se recomienda utilizar el flujo de creación usado en Admin/OrdenController@store
		 *
		 * ========================================================================================
		 */

		$response = Array(
			'result' => false,
			'message' => 'No se pudo crear el registro.  Funcion inactiva, utilizar Admin/OrdenController@store.',
			'object' => $request->all()
		);

		return response()->json($response);
//		if($orden){
//			$response = Array(
//				'result' => true,
//				'message' => 'Registro creado.',
//				'object' => $orden
//			);
//		} else {
//			$response = Array(
//				'result' => false,
//				'message' => 'No se pudo crear el registro.',
//				'object' => $request->all()
//			);
//		}

//		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
			'idPadre' => '',
			'idCliente' => '',
			'idCotizacion' => '',
			'idUbicacionEntrega' => '',
			'idVendedor' => '',
			'idHorario' => '',
			'idOrdenTipo' => '',
			'idOrdenEstado' => '',
			'fechaEntrega' => '',
			'precioTransporte' => '',
			'precioFacturado' => '',
			'nombreFactura' => '',
			'nit' => '',
        ]);

		$orden = Orden::findOrFail($id);

		if( isset($request['idPadre']) )
			$orden->idPadre = (int) $request['idPadre'];

		if( isset($request['idCliente']) )
			$orden->idCliente = (int) $request['idCliente'];

		if( isset($request['idCotizacion']) )
			$orden->idCotizacion = (int) $request['idCotizacion'];

		if( isset($request['idUbicacionEntrega']) )
			$orden->idUbicacionEntrega = (int) $request['idUbicacionEntrega'];

		if( isset($request['idVendedor']) )
			$orden->idVendedor = (int) $request['idVendedor'];

		if( isset($request['idHorario']) )
			$orden->idHorario = (int) $request['idHorario'];

		if( isset($request['idOrdenTipo']) )
			$orden->idOrdenTipo = (int) $request['idOrdenTipo'];

		if( isset($request['idOrdenEstado']) )
			$orden->idOrdenEstado = (int) $request['idOrdenEstado'];

		if( isset($request['fechaEntrega']) )
			$orden->fechaEntrega = Carbon::createFromFormat('d/m/Y', $request['fechaEntrega']);

		if( isset($request['precioTransporte']) )
			$orden->precioTransporte = trim($request['precioTransporte']);

		if( isset($request['precioFacturado']) )
			$orden->precioFacturado = trim($request['precioFacturado']);

		if( isset($request['nombreFactura']) )
			$orden->nombreFactura = trim($request['nombreFactura']);

		if( isset($request['nit']) )
			$orden->nit = trim($request['nit']);


		$orden->save();

        $response = [
            'result' => true,
            'message' => 'Registro guardado.',
            'data' => $orden->toArray()
        ];

        return response()->son($response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Orden::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$request['length'] = 50;

		$builder = Orden::select(['*']);
		$builder->with([
			'Cotizacion',
			'Cliente.Agenda',
			'Cliente.Usuario',
            'Vendedor.Owner.Agenda',
            'ProductoItems.Ubicacion',
            'ProductoItems.Producto.Marca',
            'ProductoItems.Producto.Modelo',
		]);
//		$builder->has('ProductoItems.Producto');

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
			if($request['filtro'] == 'activos'){
				$builder->where('estado', Orden::ESTADO_ACTIVE);
			}
			if($request['filtro'] == 'cerrados'){
				$builder->where('estado', Orden::ESTADO_CLOSED);
			}
			if($request['filtro'] == 'rechazados'){
				$builder->where('estado', Orden::ESTADO_REJECTED);
			}
			if($request['filtro'] == 'en_progreso'){
				$builder->where('estado', Orden::ESTADO_IN_PROGRESS);
			}
			if($request['filtro'] == 'finalizados'){
				$builder->where('estado', Orden::ESTADO_FINISHED);
			}
			if($request['filtro'] == 'eliminados'){
				$builder->withTrashed();
			}
		}

		if ( isset( $request['fechaInicio'] ) ) {
			$fechaInicio = Carbon::createFromFormat( 'Y/m/d', $request['fechaInicio'] );
			$builder->where( 'created_at', '>=', "{$fechaInicio->format('Y/m/d')}" );
		}

		if ( isset( $request['fechaFin'] ) ) {
			$fechaFin = Carbon::createFromFormat( 'Y/m/d', $request['fechaFin'] );
			$builder->where( 'created_at', '<', "{$fechaFin->addDays(1)->format('Y/m/d')}" );
		}

		if ( Gate::denies( 'ordenInbox.index.adminTerceros' ) ) {
			$builder->where( 'idVendedor', Auth::user()->id );
		} else {
			if ( isset( $request['idVendedor'] ) ) {
				if ( is_array( $request['idVendedor'] ) ) {
					$vendedorIds = $request['idVendedor'];
				} else {
					$vendedorIds = [ $request['idVendedor'] ];
				}
				$builder->whereIn( 'idVendedor', $vendedorIds );
			}
		}

		// Searches over Marca and Modelo or viceversa
		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
			$ids = DB::select(
				"SELECT
				  od.id         as idOrdenDetalle,
				  od.idOrden    as idOrden,
				  pi.id         as idProductoItem,
				  pi.idProducto as idProducto
				FROM
				  OrdenDetalle od
				  INNER JOIN ProductoItem pi ON od.idProductoItem = pi.id
				WHERE
				  pi.id IN (
					SELECT
					  pi2.id
					FROM
					  Producto p
					  INNER JOIN Marca ma ON p.idMarca = ma.id
					  INNER JOIN Modelo mo ON p.idModelo = mo.id
					  INNER JOIN ProductoItem pi2 ON p.id = pi2.idProducto
					WHERE
					  CONCAT_WS(' ', ma.nombre, mo.nombre) LIKE '%{$searchValue}%' OR
					  CONCAT_WS(' ', mo.nombre, ma.nombre) LIKE '%{$searchValue}%'
					) OR
					CONCAT('ord-', lpad(od.idOrden, 5, '0')) like '%{$searchValue}'"
			);

			$ids = array_pluck($ids, 'idOrden');
			$builder->whereIn('id', $ids);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$ordens = Orden::whereIn('id', $ids)->withTrashed()->get();
		foreach($ordens as $orden) {
			if($orden->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}
}
