<?php

namespace App\Http\Controllers\Resources;

use App\Models\Precio;
use App\Models\ProductoItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrecioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'owner_id'     => 'numeric',
			'owner_type'   => '',
			'idTipo'       => 'numeric',
			'precio'       => 'numeric',
			'moneda'       => '',
			'estado'       => 'numeric',
		]);

		$edicion = false;
		//$precio = Precio::create($request->all());
		if( isset( $request['modelo_id'] ) && $request['modelo_id'] > 0 ) {
			switch( $request['modelo_type'] ) {
				case 'producto_item':
					$mod = ProductoItem::findOrFail( (int)$request['modelo_id'] );
					break;
				default:
					$mod = ProductoItem::findOrFail( (int)$request['modelo_id'] );
			}
			$precio = $mod->Precios->where('moneda', trim( strtolower( $request['moneda'] ) ))->first();

			if( $precio ) {
				$edicion = true;
			} else {
				$precio = new Precio();
			}
		} else {
			$precio = new Precio();
		}

		if( isset($request['owner_id']) )    $precio->owner_id     = (int) $request['owner_id'];
		if( isset($request['owner_type']) )  $precio->owner_type   = trim($request['owner_type']);
		$precio->idTipo       = (int) $request['idTipo'];
		$precio->precio       = $request['precio'];
		$precio->moneda       = trim($request['moneda']);
		$precio->estado       = (int) $request['estado'];

		if( isset( $request['modelo_id'] ) && $request['modelo_id'] > 0 ) {
			if( $edicion == true ) {
				$precio->save();
			} else {
				switch( $request['modelo_type'] ) {
					case 'producto_item':
						$mod = ProductoItem::findOrFail( (int)$request['modelo_id'] );
						break;
					default:
						$mod = ProductoItem::findOrFail( (int)$request['modelo_id'] );
				}
				$mod->Precios()->save( $precio );
			}
		} else {
			$precio->save();
		}

		if($precio){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $precio
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'owner_id'     => 'numeric',
			'owner_type'   => '',
			'idTipo'       => 'numeric',
			'precio'       => 'numeric',
			'moneda'       => '',
			'estado'       => 'numeric',
			
		]);

		$precio = Precio::findOrFail($id);

		if( isset($request['owner_id']) )    $precio->owner_id     = (int) $request['owner_id'];
		if( isset($request['owner_type']) )  $precio->owner_type   = trim($request['owner_type']);
		if( isset($request['idTipo']) )      $precio->idTipo       = (int) $request['idTipo'];
		if( isset($request['precio']) )      $precio->precio       = $request['precio'];
		if( isset($request['moneda']) )      $precio->moneda       = trim($request['moneda']);
		if( isset($request['estado']) )      $precio->estado       = (int) $request['estado'];
		
		$precio->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $precio
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Precio::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$precios = Precio::whereIn('id', $ids)->withTrashed()->get();
		foreach($precios as $precio) {
			if($precio->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Precio::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			//$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

}
