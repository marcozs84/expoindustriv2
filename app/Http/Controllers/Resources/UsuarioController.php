<?php

namespace App\Http\Controllers\Resources;

use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

		/* ========================================================================================
		 *
		 * La creación de usuarios está ligada a otras entidades como:
		 * Agenda, Cliente, ProveedorContacto.
		 * Se recomienda utilizar el flujo de creación usado en Admin/UsuarioController@store
		 *
		 * ========================================================================================
		 */

		$response = Array(
			'result' => false,
			'message' => 'No se pudo crear el registro.  Funcion inactiva, utilizar Admin/UsuarioController@store.',
			'object' => $request->all()
		);

		return response()->json($response);


//		$this->validate($request, [
//			'idTipo' => 'numeric',
//			'idStripe' => '',
//			'idStripeTest' => '',
//			'username' => '',
//			'email' => '',
//			'password' => 'confirmed',
//			'cargo' => '',
//			'estado' => '',
//			'remember_token' => '',
//			'forgot_token' => '',
//			'confirmation_token' => '',
//		]);
//
//		$modelo = Usuario::create($request->all());
//
//		if($modelo){
//			$response = Array(
//				'result' => true,
//				'message' => 'Registro creado.',
//				'object' => $modelo
//			);
//		} else {
//			$response = Array(
//				'result' => false,
//				'message' => 'No se pudo crear el registro.',
//				'object' => $request->all()
//			);
//		}
//
//		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idTipo' => 'numeric',
			'idStripe' => '',
			'idStripeTest' => '',
			'username' => '',
			'email' => '',
			'password' => 'confirmed',
			'cargo' => '',
			'estado' => '',
			'remember_token' => '',
			'forgot_token' => '',
			'confirmation_token' => '',
		]);

		$modelo = Usuario::findOrFail($id);

		if(isset($request['idTipo'])){
			$modelo->idTipo = (int) $request['idTipo'];
		}
		if(isset($request['idStripe'])){
			$modelo->idStripe = trim($request['idStripe']);
		}
		if(isset($request['idStripeTest'])){
			$modelo->idStripeTest = trim($request['idStripeTest']);
		}
		if(isset($request['username'])){
			$modelo->username = trim($request['username']);
		}
		if(isset($request['email'])){
			$modelo->email = trim($request['email']);
		}
		if(isset($request['password'])){
			$modelo->password = trim($request['password']);
		}
		if(isset($request['cargo'])){
			$modelo->cargo = trim($request['cargo']);
		}
		if(isset($request['estado'])){
			$modelo->estado = trim($request['estado']);
		}
		if(isset($request['remember_token'])){
			$modelo->remember_token = trim($request['remember_token']);
		}
		if(isset($request['forgot_token'])){
			$modelo->forgot_token = trim($request['forgot_token']);
		}
		if(isset($request['confirmation_token'])){
			$modelo->confirmation_token = trim($request['confirmation_token']);
		}


		if ( trim($request['password']) == '' ) {
			$response = [
				'password' => [
					'Debe proveer una contraseña válida, dejar el campo en blanco no actualizará el registro.'
				]
			];
			return response()->make($response, 422);
		}

		if(isset($request['password_current']) && trim($request['password_current']) != ''){
//			if(bcrypt($request['password_current']) != $modelo->password){
			if(Hash::check($request['password_current'], $modelo->password)){
				$response = [
					'contrasenia' => [
						'El password actual no coincide con el registro actual.'
					]
				];
				return response()->make($response, 422);
			}
		}

		if(isset($request['password']) && trim($request['password']) != ''){
			$modelo->password = bcrypt(trim($request['password']));
		}

		if(isset($request['username']) && trim($request['username']) != ''){
			$modelo->username = trim($request['username']);

			//TODO: Enviar el nuevo correo electronico al correo del usuario despues de actualizarlo
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

	// ---------- CUSTOM

	public function restore(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Usuario::withTrashed()->whereIn('id', $ids)->restore();

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron restaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Usuario::select(['*']);
		$builder->with([
			'Owner.Agenda.Telefonos'
		]);
//		$builder->with([
//			'Producto.ProductoItem.Ubicacion',
//			'Producto.Categorias',
//			'Producto.Marca',
//			'Producto.Modelo',
//			'Producto.Categorias.Padre',
//			'Producto.Galerias.Imagenes',
//			'Owner.Owner.Agenda'
//		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
//			if($request['filtro'] == 'aprobados'){
//				$builder->where('estado', Publicacion::ESTADO_APPROVED);
//			}
		}

		$estados = [];
		if( isset( $request['idTipo'] ) ) {
			if( is_array($request['idTipo']) && count($request['idTipo']) > 0 ) {
				$builder->whereIn('idTipo', $request['idTipo'] );
			} else {
				$builder->where('idTipo', $request['idTipo'] );
			}
		}
		if( isset( $request['estado'] ) && is_array($request['estado']) && count($request['estado']) > 0 ) {
			$estados = $request['estado'];
			$pos = array_search(100, $estados);
			if( $pos !== false ) {
				array_splice( $estados, $pos, 1);
				if( count($estados) > 0 ) {
					$builder->withTrashed();
				} else {
					$builder->onlyTrashed();
				}
			}
		}

		if( count($estados) > 0 ) {
			$builder->whereIn('estado', $estados);
		}

		if($searchValue != ''){
			$builder->where('username', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
//			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
