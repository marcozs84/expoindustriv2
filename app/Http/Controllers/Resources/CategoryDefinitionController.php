<?php

namespace App\Http\Controllers\Resources;

use App\Models\CategoriaDefinition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryDefinitionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idCategoria' => '',
			'campos' => '',
			'elemento' => '',
		]);

		$modelo = CategoriaDefinition::findOrFail($id);

		if(isset($request['idCategoria'])) {
			logger('saving idCategoria');
			$modelo->idCategoria = trim($request['idCategoria']);
		}

		if(isset($request['campos'])) {
			$array = json_decode(trim($request['campos']));
			$modelo->campos = $array;
		}

		if(isset($request['elemento'])) {
			$array = $modelo->campos;
//			$elem = json_decode(trim($request['elemento']));
			$elem = $request['elemento'];

			for($i = 0 ; $i < count($array) ; $i++) {
				if($array[$i]['key'] == $elem['key']){
					logger("found: " . $elem['key']);
					logger($array[$i]);
					logger($elem);

					$elem['indent'] = (int) $elem['indent'];
					$elem['primary'] = (int) $elem['primary'];

					$array[$i] = $elem;
				}
			}
//			foreach($array as &$element) {
//				if($element['key'] == $elem['key']){
//					logger("found: " . $elem['key']);
//					$element = $elem;
//					logger($element);
//				}
//			}

			$modelo->campos = $array;
		}

		$res = $modelo->save();

		if($res){
			$response = Array(
				'result' => true,
				'message' => 'Registro guardado.',
				'data' => $modelo->toArray()
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'Error al guardar el registro.',
				'data' => $modelo->toArray()
			);
		}

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = CategoriaDefinition::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['idPadre'])){
			$builder->where('idPadre', $request['idPadre']);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('nombre', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
