<?php

namespace App\Http\Controllers\Resources;

use App\Models\Convenio;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Modulo;
use App\Models\Template;
use App\Models\TemplateTipo;
use App\Models\TFT;
use App\Utilities\TemplateRender;
use Illuminate\Http\Request;
use PDF;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TemplateController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

		$convenios = Convenio::all()->pluck('nombreCorto', 'id')->toArray();
		$modulos = Modulo::all()->pluck('nombre', 'id')->toArray();
		$tipos = TemplateTipo::all()->pluck('nombre', 'id')->toArray();

        return view('admin.template.index', [
			'convenios' => $convenios,
			'modulos' => $modulos,
			'tipos' => $tipos,
		]);
    }

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Template::with([
			'Modulo'
		])->select(['*']);

		$search = $request['search'];
		if(trim($search['value']) != ''){
			$searchValue = $search['value'];
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['modulo']) && count($request['modulo']) > 0 && $request['modulo'] != ''){
			$builder->whereIn('idModulo', $request['modulo']);
		}
		if(isset($request['tipo']) && count($request['tipo']) > 0 && $request['tipo'] != ''){
			$builder->whereIn('idTemplateTipo', $request['tipo']);
		}

		$builder->orderBy('id', 'desc');

		if(isset($request['draw'])) {	// llamada DataTable
			$ds = $builder->paginate($request['length'], ['*']);
		} else {						// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $templateTipos = ListaItem::listado(Lista::TEMPLATE_TIPO)->get()->pluck('texto', 'valor')->toArray();
		$modulos = Modulo::get()->pluck('nombre', 'id');

		return view('admin.template.create', [
			'templateTipos' => $templateTipos,
			'modulos' => $modulos
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
			'idModulo' => 'required|integer',
			'nombre' => 'required|string',
			'descripcion' => 'required',
			'asunto' => 'required',
			'mensaje' => 'required',
			'estado' => ''
		]);

		$template = Template::create($request->all());

		if($template){
			$response = Array(
				'result' => true,
				'message' => 'Template creado correctamente.',
				'object' => $template->toArray()
			);
			return response()->json($response);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'Error al crear el template.',
				'object' => []
			);
			return response()->json($response);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $template = Template::findOrFail($id);

		$templateTipos = TemplateTipo::all()->pluck('nombre', 'id')->toArray();
		$convenios = Convenio::all()->pluck('nombreCorto', 'id')->toArray();
		$modulos = Modulo::getModulos();

		return view('admin.template.edit', [
			'template' => $template,
			'templateTipos' => $templateTipos,
			'convenios' => $convenios,
			'modulos' => $modulos
		]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idModulo' => 'integer',
			'idTemplateTipo' => 'integer',
			'idConvenio' => 'integer'
		]);

		//$template = Template::where('id', $id)->update([
		//	'mensaje' => $request['mensaje']
		//]);

		$template = Template::find($id);
		if(isset($request['idModulo'])){
			$template->idModulo = $request['idModulo'];
		}
		if(isset($request['idTemplateTipo'])){
			$template->idTemplateTipo = $request['idTemplateTipo'];
		}
		if(isset($request['nombre'])){
			$template->nombre = $request['nombre'];
		}
		if(isset($request['descripcion'])){
			$template->descripcion = $request['descripcion'];
		}
		if(isset($request['asunto'])){
			$template->asunto = $request['asunto'];
		}
		if(isset($request['mensaje'])){
			$template->mensaje = $request['mensaje'];
		}
		if(isset($request['estado'])){
			$template->estado = $request['estado'];
		}

		if(isset($request['bg']) && trim($request['bg'] != '')){
			$extrainfo = $template->extrainfo;
			$extrainfo['bg'] = trim($request['bg']);
			$extrainfo['bg_w'] = trim($request['bg_w']);
			$extrainfo['bg_h'] = trim($request['bg_h']);
			$extrainfo['bg_x'] = trim($request['bg_x']);
			$extrainfo['bg_y'] = trim($request['bg_y']);
			$pags = trim($request['bg_pags']);
			logger("guardando");
			logger($pags);
			$pags = explode(',', $pags);
			foreach($pags as &$pag){
				$pag = (int) trim($pag);
			}
			$extrainfo['bg_pags'] = $pags;
			$template->extrainfo = $extrainfo;
		}
		if(isset($request['bg']) && trim($request['bg'] == '')){
			$extrainfo = $template->extrainfo;
			$extrainfo['bg'] = trim($request['bg']);
			$extrainfo['bg_w'] = 0;
			$extrainfo['bg_h'] = 0;
			$extrainfo['bg_x'] = 0;
			$extrainfo['bg_y'] = 0;
			$extrainfo['bg_pags'] = [0];
			$template->extrainfo = $extrainfo;
		}

		if(isset($request['header_mensaje'])){ // && trim($request['header_mensaje']) != ''
			$extrainfo = $template->extrainfo;
			$extrainfo['header']['mensaje'] = trim($request['header_mensaje']);
			$template->extrainfo = $extrainfo;
		}

		if(isset($request['footer_mensaje']) && trim($request['footer_mensaje']) != ''){
			$extrainfo = $template->extrainfo;
			$extrainfo['footer']['mensaje'] = trim($request['footer_mensaje']);
			$template->extrainfo = $extrainfo;
		}

		if(isset($request['margenes'])){
			$extrainfo = $template->extrainfo;
			$extrainfo['margenes']['t'] = trim($request['margenes']['t']);
			$extrainfo['margenes']['b'] = trim($request['margenes']['b']);
			$extrainfo['margenes']['l'] = trim($request['margenes']['l']);
			$extrainfo['margenes']['r'] = trim($request['margenes']['r']);
			$template->extrainfo = $extrainfo;
		}

		$template->save();

		$response = Array(
			'result' => true,
			'message' => 'Template actualizado.',
			'object' => $template
		);

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Template::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

}
