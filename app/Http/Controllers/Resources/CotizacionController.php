<?php

namespace App\Http\Controllers\Resources;

use App\Models\Cliente;
use App\Models\Cotizacion;
use App\Models\Publicacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CotizacionController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idAlmacen'                => 'numeric',
			'idCliente'                => 'numeric',
			'idCotizacionEsquema'      => 'numeric',
			'idPublicacion'            => 'numeric',
			'idProducto'               => 'numeric',
			'revisionTecnica'          => '',
			'revisionTecnicaPrecio'    => 'numeric',
			'fotografiasExtra'         => '',
			'fotografiasExtraPrecio'   => 'numeric',
			'seguro'                   => '',
			'seguroPrecio'             => 'numeric',
			'transporte'               => '',
			'transportePrecio'         => 'numeric',
			'destinoPrecio'            => 'numeric',
			'definicion'               => '',
			'estado'                   => 'numeric',

		]);

		$request['definicion'] = [];

		$cotizacion = Cotizacion::create($request->all());

		if($cotizacion){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'object' => $cotizacion
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'object' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idAlmacen'                => 'numeric',
			'idCliente'                => 'numeric',
			'idCotizacionEsquema'      => 'numeric',
			'idPublicacion'            => 'numeric',
			'idProducto'               => 'numeric',
			'revisionTecnica'          => '',
			'revisionTecnicaPrecio'    => 'numeric',
			'fotografiasExtra'         => '',
			'fotografiasExtraPrecio'   => 'numeric',
			'seguro'                   => '',
			'seguroPrecio'             => 'numeric',
			'transporte'               => '',
			'transportePrecio'         => 'numeric',
			'destinoPrecio'            => 'numeric',
//			'definicion'               => '',
			'estado'                   => 'numeric',

		]);

		$cotizacion = Cotizacion::findOrFail($id);

		if( isset($request['idAlmacen']) )               $cotizacion->idAlmacen                = (int) $request['idAlmacen'];
		if( isset($request['idCliente']) )               $cotizacion->idCliente                = (int) $request['idCliente'];
		if( isset($request['idCotizacionEsquema']) )     $cotizacion->idCotizacionEsquema      = (int) $request['idCotizacionEsquema'];
		if( isset($request['idPublicacion']) )           $cotizacion->idPublicacion            = (int) $request['idPublicacion'];
		if( isset($request['idProducto']) )              $cotizacion->idProducto               = (int) $request['idProducto'];
		if( isset($request['revisionTecnica']) )         $cotizacion->revisionTecnica          = trim($request['revisionTecnica']);
		if( isset($request['revisionTecnicaPrecio']) )   $cotizacion->revisionTecnicaPrecio    = Carbon::createFromFormat('d/m/Y', $request['revisionTecnicaPrecio']);
		if( isset($request['fotografiasExtra']) )        $cotizacion->fotografiasExtra         = trim($request['fotografiasExtra']);
		if( isset($request['fotografiasExtraPrecio']) )  $cotizacion->fotografiasExtraPrecio   = Carbon::createFromFormat('d/m/Y', $request['fotografiasExtraPrecio']);
		if( isset($request['seguro']) )                  $cotizacion->seguro                   = trim($request['seguro']);
		if( isset($request['seguroPrecio']) )            $cotizacion->seguroPrecio             = Carbon::createFromFormat('d/m/Y', $request['seguroPrecio']);
		if( isset($request['transporte']) )              $cotizacion->transporte               = trim($request['transporte']);
		if( isset($request['transportePrecio']) )        $cotizacion->transportePrecio         = Carbon::createFromFormat('d/m/Y', $request['transportePrecio']);
		if( isset($request['destinoPrecio']) )           $cotizacion->destinoPrecio            = Carbon::createFromFormat('d/m/Y', $request['destinoPrecio']);
//		if( isset($request['definicion']) )              $cotizacion->definicion               = trim($request['definicion']);
		if( isset($request['estado']) )                  $cotizacion->estado                   = (int) $request['estado'];

		$cotizacion->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'object' => $cotizacion
		);
		return response()->json($response);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Publicacion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$request['length'] = 50;

		$builder = Cotizacion::select(['*']);
		$builder->with([
			'Publicacion.Producto.ProductoItem.Ubicacion',
			'Publicacion.Producto.Categorias',
			'Publicacion.Producto.Marca',
			'Publicacion.Producto.Modelo',
			'Publicacion.Producto.Categorias.Padre',
			'Publicacion.Producto.Galerias.Imagenes',
			'Publicacion.Owner.Owner.Agenda',
			'Cliente.Agenda',
			'Cliente.Usuario',
		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
			if($request['filtro'] == 'aprobados'){
				$builder->where('estado', Cotizacion::ESTADO_ACTIVE);
			}
		}

//		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
//		}

		$builder->orderBy('created_at', 'DESC');

		if(isset($request['draw'])) {	// llamada DataTable
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
