<?php

namespace App\Http\Controllers\Resources;

use App\Models\Imagen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImagenController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Imagen::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Imagen::select(['*']);
		$builder->with([
			'Galeria',
		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

//		if(isset($request['filtro'])){
//			if($request['filtro'] == 'aprobados'){
//				$builder->where('estado', Publicacion::ESTADO_APPROVED);
//			}
//		}

		if($searchValue != ''){
			$builder->where('id', $searchValue);
		}

		if(isset($request['estados']) && $request['estados'] > 0){
			if( !is_array($request['estados']) ) {
				$request['estados'] = [ $request['estados'] ];
			}
			$builder->whereIn('estado', $request['estados']);
		}

		if(isset($request['estado']) && $request['estado'] > 0){
			$builder->where('estado', $request['estado']);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}


	public function image_resize( $imagen, &$message = ''  ) {
		// ---------- THUMBNAIL SIZE
		$w = Imagen::DIMENSION_MAX_WIDTH; // 1024;
		$h = Imagen::DIMENSION_MAX_HEIGHT; // 1024;
//		$tw = 260;
//		$th = 120;
		$tw = Imagen::DIMENSION_MAX_WIDTH_THUMB; // 305;
		$th = Imagen::DIMENSION_MAX_HEIGHT_THUMB; // 150;

		$resized_imgs = [];
		$failed_imgs = [];

		$file = storage_path( $imagen->ruta );

		$filename = pathinfo( $file, PATHINFO_BASENAME);
		$newname = str_replace('.', '_res.', $filename);
		$resized = $this->resize_source( $file, $w, $h, false, $newname, $message );


		if( $resized ) {
			$resized_imgs[] = $file;
		} else {
			$failed_imgs[] = $file;
//			return false;
		}


		$filename = pathinfo( $file, PATHINFO_BASENAME);
		$newname = str_replace('.', '_thumb.', $filename);
		$thumb = $this->resize_source( $file, $tw, $th, false, $newname, $message );

		if( $thumb ) {
			$resized_imgs[] = $file;
		} else {
			$failed_imgs[] = $file;
//			return false;
		}

//		sleep(1);
//		return true;

		if( count($failed_imgs) == 0 ) {
			$message = 'Imágenes redimensionadas a nueva ubicacion.';
		}


		$response = Array(
			'result' => true,
			'message' => $message,
			'data' => [
				'success' => $resized_imgs,
				'failed' => $failed_imgs,
			]
		);

		return response()->json( $response );
	}

	/**
	 * Ref.: https://stackoverflow.com/a/60630053/2367718
	 * @param $file
	 * @param $w
	 * @param $h
	 * @param bool $crop
	 * @param string $newname
	 * @param string $message
	 * @return bool|mixed
	 */
	public function resize_source( $file, $w, $h, $crop = false, $newname = '', &$message = '' ) {

		try {
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$ext = strtolower($ext);
			list($width, $height) = getimagesize( $file );
			// if the image is smaller we dont resize

			if ($w > $width && $h > $height) {
				$message .= ' La imagen es menor a '.$w.', no se redimensionará.';
				return false;
			}
			$r = $width / $height;
			if ($crop) {
				if ($width > $height) {
					$width = ceil($width - ($width * abs($r - $w / $h)));
				} else {
					$height = ceil($height - ($height * abs($r - $w / $h)));
				}
				$newwidth = $w;
				$newheight = $h;
			} else {
				if ($w / $h > $r) {
					$newwidth = $h * $r;
					$newheight = $h;
				} else {
					$newheight = $w / $r;
					$newwidth = $w;
				}
			}
			$dst = imagecreatetruecolor($newwidth, $newheight);

			switch ($ext) {
				case 'jpg':
				case 'jpeg':
					$src = imagecreatefromjpeg($file);
					break;
				case 'png':
					$src = imagecreatefrompng($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
				case 'gif':
					$src = imagecreatefromgif($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
//				case 'bmp':
//					$src = imagecreatefrombmp($file);
					break;
				default:
					$message.= ' Unsupported image extension found: ' . $ext;
					return false;
			}
			$result = imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			if( $newname != '' ) {
				$filename = pathinfo( $file, PATHINFO_BASENAME);
				$file = str_replace($filename, $newname, $file);
			}

			switch ($ext) {
				case 'bmp':
					imagewbmp($dst, $file);
					break;
				case 'gif':
					imagegif($dst, $file);
					break;
				case 'jpg':
				case 'jpeg':
					imagejpeg($dst, $file);
					break;
				case 'png':
					imagepng($dst, $file);
					break;
			}

			imagedestroy($dst);
			imagedestroy($src);

			return $file;

		} catch (Exception $err) {
			// LOG THE ERROR HERE
			$message .= ' ' . $err->getMessage();
			logger($message);
			return false;
		}
	}
}
