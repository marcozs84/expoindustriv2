<?php

namespace App\Http\Controllers\Resources;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriaController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idPadre' => 'required',
			'nivel' => 'required',
			'nombre' => 'required',
			'descripcion' => 'required',
			'estado' => 'required',
			'visible' => 'required',
		]);

		$modelo = Categoria::create($request->all());

		if($modelo){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $modelo
			);

			return response()->json($response);
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idPadre' => '',
			'nivel' => '',
			'nombre' => '',
			'descripcion' => '',
			'estado' => '',
			'visible' => '',
		]);

		$modelo = Categoria::findOrFail($id);

		if(isset($request['idPadre'])){
			$modelo->idPadre = trim($request['idPadre']);
		}
		if(isset($request['nivel'])){
			$modelo->nivel = trim($request['nivel']);
		}
		if(isset($request['nombre'])){
			$modelo->nombre = trim($request['nombre']);
		}
		if(isset($request['descripcion'])){
			$modelo->descripcion = trim($request['descripcion']);
		}
		if(isset($request['estado'])){
			$modelo->estado = trim($request['estado']);
		}
		if(isset($request['visible'])){
			$modelo->visible = trim($request['visible']);
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Categoria::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Categoria::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
			if($request['filtro'] == 'subcategorias'){
				$builder->with('Padre');
				$builder->orderBy('idPadre', 'asc');
				$builder->orderBy('nombre', 'desc');
				$builder->where('idPadre', '>', 0);
			}
		}

		if($searchValue != ''){
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['idPadre'])){
			$builder->where('idPadre', $request['idPadre']);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('nombre', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
