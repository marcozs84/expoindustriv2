<?php

namespace App\Http\Controllers\Resources;

use App\Models\Producto;
use App\Models\ProductoItem;
use App\Models\Traduccion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductoItemController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idProducto'            => 'numeric',
			'idLote'                => 'numeric',
			'idDisponibilidad'      => 'numeric',
			'idTipoImportacion'     => 'numeric',
			'owner_type'            => '',
			'owner_id'              => 'numeric',
			'nombre'                => '',
			'descripcionPrimaria'   => '',
			'descripcionDetalle'    => '',
			'ean'                   => '',
			'sku'                   => '',
			'precioDistribuidor'    => 'numeric',
			'precioVenta'           => 'numeric',
			'monedaVenta'           => '',
			'precioPromocion'       => 'numeric',
			'color'                 => '',
			'dimension'             => '',
			'fechaElaboracion'      => '',
			'fechaVencimiento'      => '',
			'definicion'            => '',
			'permalink'             => '',

		]);

		$idProducto = (int) $request['idProducto'];
		$owner_type = trim($request['owner_type']);
		$owner_id = (int) $request['owner_id'];
		if( $idProducto > 0 ) {
			$producto = Producto::find( $idProducto );
			$owner_type = $producto->owner_type;
			$owner_id = $producto->owner_id;
		}

		//$productoItem = ProductoItem::create($request->all());
		$productoItem = new ProductoItem();
		$productoItem->idProducto            = $idProducto;
		$productoItem->idLote                = ((int) $request['idLote'] > 0) ? (int) $request['idLote'] : null;
		$productoItem->idDisponibilidad      = (int) $request['idDisponibilidad'];
		$productoItem->idTipoImportacion     = (int) $request['idTipoImportacion'];
		$productoItem->owner_type            = $owner_type;
		$productoItem->owner_id              = $owner_id;
		$productoItem->nombre                = trim($request['nombre']);
		$productoItem->descripcionPrimaria   = trim($request['descripcionPrimaria']);
		$productoItem->descripcionDetalle    = trim($request['descripcionDetalle']);
		$productoItem->ean                   = trim($request['ean']);
		$productoItem->sku                   = trim($request['sku']);
		$productoItem->precioDistribuidor    = $request['precioDistribuidor'];
		$productoItem->precioVenta           = $request['precioVenta'];
		$productoItem->monedaVenta           = trim($request['monedaVenta']);
		$productoItem->precioPromocion       = $request['precioPromocion'];
		$productoItem->color                 = trim($request['color']);
		$productoItem->dimension             = trim($request['dimension']);
		$productoItem->fechaElaboracion      = (trim($request['fechaElaboracion']) != '') ? Carbon::createFromFormat('d/m/Y', $request['fechaElaboracion']) : null;
		$productoItem->fechaVencimiento      = (trim($request['fechaVencimiento']) != '') ? Carbon::createFromFormat('d/m/Y', $request['fechaVencimiento']) : null;
		$productoItem->definicion            = trim($request['definicion']);
		$productoItem->permalink             = trim($request['permalink']);

		$productoItem->save();

		if( isset( $request['make_item_trans'] ) && $request['make_item_trans'] == 1 ) {
			$traduccion = new Traduccion();
			$traduccion->campo = 'nombre';
			$traduccion->se = $productoItem->nombre;
			$traduccion->en = $productoItem->nombre;
			$traduccion->es = $productoItem->nombre;
			$productoItem->Traducciones()->save($traduccion);

			$traduccion = new Traduccion();
			$traduccion->campo = 'descripcion';
			$traduccion->se = $productoItem->descripcionDetalle;
			$traduccion->en = $productoItem->descripcionDetalle;
			$traduccion->es = $productoItem->descripcionDetalle;
			$productoItem->Traducciones()->save($traduccion);
		}

		if($productoItem){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $productoItem
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idProducto'            => 'numeric',
			'idLote'                => 'numeric',
			'idDisponibilidad'      => 'numeric',
			'idTipoImportacion'     => 'numeric',
			'owner_type'            => '',
			'owner_id'              => 'numeric',
			'nombre'                => '',
			'descripcionPrimaria'   => '',
			'descripcionDetalle'    => '',
			'ean'                   => '',
			'sku'                   => '',
			'precioDistribuidor'    => 'numeric',
			'precioVenta'           => 'numeric',
			'monedaVenta'           => '',
			'precioPromocion'       => 'numeric',
			'color'                 => '',
			'dimension'             => '',
			'fechaElaboracion'      => 'required',
			'fechaVencimiento'      => 'required',
			'definicion'            => '',
			'permalink'             => '',
		]);

		$productoItem = ProductoItem::findOrFail($id);

		if( isset($request['idProducto']) )           $productoItem->idProducto            = (int) $request['idProducto'];
		if( isset($request['idLote']) )               $productoItem->idLote                = (int) $request['idLote'];
		if( isset($request['idDisponibilidad']) )     $productoItem->idDisponibilidad      = (int) $request['idDisponibilidad'];
		if( isset($request['idTipoImportacion']) )    $productoItem->idTipoImportacion     = (int) $request['idTipoImportacion'];
		if( isset($request['owner_type']) )           $productoItem->owner_type            = trim($request['owner_type']);
		if( isset($request['owner_id']) )             $productoItem->owner_id              = (int) $request['owner_id'];
		if( isset($request['nombre']) )               $productoItem->nombre                = trim($request['nombre']);
		if( isset($request['descripcionPrimaria']) )  $productoItem->descripcionPrimaria   = trim($request['descripcionPrimaria']);
		if( isset($request['descripcionDetalle']) )   $productoItem->descripcionDetalle    = trim($request['descripcionDetalle']);
		if( isset($request['ean']) )                  $productoItem->ean                   = trim($request['ean']);
		if( isset($request['sku']) )                  $productoItem->sku                   = trim($request['sku']);
		if( isset($request['precioDistribuidor']) )   $productoItem->precioDistribuidor    = $request['precioDistribuidor'];
		if( isset($request['precioVenta']) )          $productoItem->precioVenta           = $request['precioVenta'];
		if( isset($request['monedaVenta']) )          $productoItem->monedaVenta           = trim($request['monedaVenta']);
		if( isset($request['precioPromocion']) )      $productoItem->precioPromocion       = $request['precioPromocion'];
		if( isset($request['color']) )                $productoItem->color                 = trim($request['color']);
		if( isset($request['dimension']) )            $productoItem->dimension             = trim($request['dimension']);
		if( isset($request['fechaElaboracion']) )     $productoItem->fechaElaboracion      = Carbon::createFromFormat('d/m/Y', $request['fechaElaboracion']);
		if( isset($request['fechaVencimiento']) )     $productoItem->fechaVencimiento      = Carbon::createFromFormat('d/m/Y', $request['fechaVencimiento']);
		if( isset($request['definicion']) )           $productoItem->definicion            = trim($request['definicion']);
		if( isset($request['permalink']) )            $productoItem->permalink             = trim($request['permalink']);

		$productoItem->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $productoItem
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = ProductoItem::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$productoItems = ProductoItem::whereIn('id', $ids)->withTrashed()->get();
		foreach($productoItems as $productoItem) {
			if($productoItem->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = ProductoItem::select(['*']);
		$builder->with([
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Owner.Owner.Agenda',
			'Producto.Owner.Owner.Proveedor',
//			'Producto.Publicacion.Owner.Owner.Agenda',
//			'Producto.Publicacion.Owner.Owner.Proveedor',
			'Producto.Categorias.Padre',
		]);
		$builder->has('Producto');

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){

			$ids = DB::select(
				"SELECT
				  pi.id as id
				FROM
				  Producto p
				  INNER JOIN Marca ma ON p.idMarca = ma.id
				  INNER JOIN Modelo mo ON p.idModelo = mo.id
				  INNER JOIN ProductoItem pi ON p.id = pi.idProducto
				WHERE
				  CONCAT_WS(' ', ma.nombre, mo.nombre) LIKE '%{$searchValue}%' OR
				  CONCAT_WS(' ', mo.nombre, ma.nombre) LIKE '%{$searchValue}%'"
			// CONCAT(ma.nombre,' ', mo.nombre) LIKE '%{$searchValue}%'"
			);

			$ids = array_pluck($ids, 'id');
			$builder->whereIn('id', $ids);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('nombre', 'asc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
