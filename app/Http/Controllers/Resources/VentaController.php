<?php

namespace App\Http\Controllers\Resources;

use App\Models\Imagen;
use App\Models\Publicacion;
use App\Models\Venta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VentaController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Venta::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){

			$ids = DB::select(
				"SELECT
				  pi.id as id
				FROM
				  Producto p
				  INNER JOIN Marca ma ON p.idMarca = ma.id
				  INNER JOIN Modelo mo ON p.idModelo = mo.id
				  INNER JOIN ProductoItem pi ON p.id = pi.idProducto
				WHERE
				  CONCAT_WS(' ', ma.nombre, mo.nombre) LIKE '%{$searchValue}%' OR
				  CONCAT_WS(' ', mo.nombre, ma.nombre) LIKE '%{$searchValue}%'"
				// CONCAT(ma.nombre,' ', mo.nombre) LIKE '%{$searchValue}%'"
			);

			$ids = array_pluck($ids, 'id');
			$builder->whereIn('idProductoItem', $ids);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('nombre', 'asc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function getFlotData(Request $request){

		// ---------- FECHAS -> TICKS

		$fechaInicio = Carbon::parse($request['fechaInicio']);
		$fechaFin = Carbon::parse($request['fechaFin']);

		if(!$fechaInicio->lessThanOrEqualTo($fechaFin)) {
			$response = [
				'Calendario' => [
					'Fecha de inicio debe ser menor a la fecha final'
				]
			];
			return response()->make($response, 422);
		}

		logger($fechaInicio);
		logger($fechaFin);

		$ticks = [];
		$ticks2 = [];

		for( $fechaIt = clone $fechaInicio ; $fechaIt->lessThanOrEqualTo($fechaFin) ; $fechaIt->addDay(1)){
			$ticks[] = $fechaIt->format('d.m.Y');
			$ticks2[] = $fechaIt->format('M j \'y');
		}

		$res0 = [];
		$res1 = [];
		$res2 = [];
		$res3 = [];
		$res4 = [];
		$res5 = [];

		DB::enableQueryLog();


		$result = $builder = Publicacion::select([
			DB::raw("count('id') as count"),
			DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
		])
			->where('created_at', '>=', "{$fechaInicio->format('Y/m/d')}")
			->where('created_at', '<=', "{$fechaFin->format('Y/m/d')}")
		->groupBy([ DB::raw('date')])
		->orderBy('created_at')->get();


		$result2 = $builder = Imagen::select([
			DB::raw("count('id') as count"),
			DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
		])
			->where('created_at', '>=', "{$fechaInicio->format('Y/m/d')}")
			->where('created_at', '<=', "{$fechaFin->format('Y/m/d')}")
		->groupBy([ DB::raw('date')])
		->orderBy('created_at')->get();

		logger(DB::getQueryLog());

//		logger("result:");
//		logger(print_r($result->toArray(), true));
		//logger(print_r($result->pluck('fecha1')->toArray(), true));



//		$fechas = $result->pluck('date')->toArray();
//		$ticks = array_unique($fechas);
//		$ticks = array_flatten($ticks);

		$com0 = $result->toArray();
		$com1 = $result2->toArray();
//		$com1 = $result->where('puntos', 1)->toArray();
//		$com2 = $result->where('puntos', 2)->toArray();
//		$com3 = $result->where('puntos', 3)->toArray();
//		$com4 = $result->where('puntos', 4)->toArray();
//		$com5 = $result->where('puntos', 5)->toArray();

		//$com0 = ComunicacionRegistro::select([
		//		DB::raw("count('id') as count"),
		//		DB::raw('DATE_FORMAT(fecha, "%d.%m.%Y") as fecha1'),
		//	])
		//	->groupBy(DB::raw('DATE(fecha)'))
		//	//->where('tbDestinatario', 'tbModulo')
		//	//->where('extrainfo', 'like', '%"pt":0,%')
		//	->get();

		logger($com1);

//		$res0 = $com0;
		$res0 = $this->flot_format($com0, $ticks);
		$res1 = $this->flot_format($com1, $ticks);
//		$res2 = $this->flat_array($com2, $ticks);
//		$res3 = $this->flat_array($com3, $ticks);
//		$res4 = $this->flat_array($com4, $ticks);
//		$res5 = $this->flat_array($com5, $ticks);

		$data = [
			'com0' => $res0,
			'com1' => $res1,
//			'com2' => $res2,
//			'com3' => $res3,
//			'com4' => $res4,
//			'com5' => $res5,
			'ticks' => $ticks2
		];
		$response = Array(
			'result' => true,
			'message' => 'Descarga de datos correcta.',
			'data' => $data
		);
		return response()->json($response);

		//return response()->json($data);
	}

	private function flot_format($com, $ticks){
		$count = 0;
		$result = [];
//		logger($this->ticks);
		foreach($com as $ar){
			$pos = array_search($ar['date'], $ticks);
			$result[] = [$pos, $ar['count'], $ar['date']];
			$count++;
		}
		return $result;
	}

}
