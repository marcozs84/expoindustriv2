<?php

namespace App\Http\Controllers\Resources;

use App\Models\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoleController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->validate($request, [
			'nombre' => 'required',
			'descripcion' => '',
			'estado' => 'required',
		]);

		$modelo = Role::create($request->all());

		if($modelo){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'object' => $modelo
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'object' => $request->all()
			);
		}

		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'nombre' => '',
			'descripcion' => '',
			'estado' => '',
			'idConvenio' => ''
		]);

		$role = Role::findOrFail($id);

		if(isset($request['nombre'])){
			$role->nombre = trim($request['nombre']);
		}
		if(isset($request['descripcion'])){
			$role->descripcion = trim($request['descripcion']);
		}
		if(isset($request['estado'])){
			$role->estado = trim($request['estado']);
		}
		if(isset($request['idConvenio'])){
			$role->idConvenio = trim($request['idConvenio']);
		}

		$role->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'object' => $role
		);
		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Role::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Role::select(['*']);

		$search = $request['search'];
		if(trim($search['value']) != ''){
			$searchValue = $search['value'];
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['nombre']) && trim($request['nombre']) != ''){
			$searchValue = $request['nombre'];
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('id', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function guardarRolePermiso(Request $request){
		$this->validate($request, [
			'status' => 'required|numeric',
			'idRole' => 'required|numeric',
			'idPermiso' => 'required|numeric',
		]);

		$status = (int)$request['status'];
		$idRole = (int)$request['idRole'];
		$idPermiso = (int)$request['idPermiso'];
		$role = Role::findOrFail($idRole);

		$data = [];
		if($status == 1){
			$role->Permisos()->detach([$idPermiso]);
			$role->Permisos()->attach([$idPermiso]);
		} elseif($status == 0) {
			$role->Permisos()->detach([$idPermiso]);
		}

		$permisos = $role->Permisos()->orderBy('nombre')->get();
		$response = Array(
			'result' => true,
			'message' => 'Permisos actualizados.',
			'data' => $permisos->toArray()
		);

		return response()->json($response);

//		logger(print_r($role->Permisos->toArray(), true));

	}

	public function eliminarRolePermiso(Request $request){
		$this->validate($request, [
			'idRole' => 'required|numeric',
			'idPermiso' => 'required|numeric',
		]);

		$idRole = (int)$request['idRole'];
		$role = Role::findOrFail($idRole);

		if($role){

		}

	}
}
