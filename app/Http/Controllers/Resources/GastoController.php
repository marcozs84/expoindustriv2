<?php

namespace App\Http\Controllers\Resources;

use App\Models\Gasto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class GastoController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

		$this->validate($request, [
			'idUsuario' => 'required|numeric',
			'idGastoTipo' => 'required|numeric',
			'monto' => 'required|numeric',
			'moneda' => 'required',
			'fecha' => '',
			'descripcion' => '',
		]);

		$modelo = new Gasto();

		if(isset($request['idUsuario']) && $request['idUsuario'] > 0) {
			$modelo->idUsuario = (int) trim($request['idUsuario']);
		}
		if(isset($request['idGastoTipo']) && $request['idGastoTipo'] > 0) {
			$modelo->idGastoTipo = (int) trim($request['idGastoTipo']);
		}
		if(isset($request['monto'])) {
			$modelo->monto = trim($request['monto']);
		}
		if(isset($request['moneda'])) {
			$modelo->moneda = trim($request['moneda']);
		}
		if(isset($request['fecha'])) {
			$modelo->fecha = Carbon::createFromFormat('d/m/Y', $request['fecha']);
		}
		if(isset($request['descripcion'])) {
			$modelo->descripcion = trim($request['descripcion']);
		}

		$user = Auth::guard('empleados')->user();
		if ( $modelo->idUsuario != $user->id ) {
			if ( Gate::denies( 'admin.gasto.adminTerceros' ) ) {

				$this->reportDev("Alguien intentó registrar un GASTO a nombre de otro usuario. Intento por parte de: {$user->id} a nombre de: {$modelo->idUsuario}" );

				$response = Array(
					'result' => false,
					'message' => 'No tiene permisos para guardar un gasto relacionado a un tercero.  Este incidente ha sido notificado a la administración.',
					'data' => $modelo
				);

				return response()->json($response);
			}
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idUsuario' => 'required|numeric',
			'idGastoTipo' => 'required|numeric',
			'monto' => 'required|numeric',
			'moneda' => 'required',
			'fecha' => '',
			'descripcion' => '',
		]);

		$fecha = Carbon::createFromFormat('d/m/Y', $request['fecha']);
		if( $fecha >= Carbon::today()->addDays(1) ) {
			$response = [
				'fecha' => [
					'La fecha no puede ser mayor a la fecha actual.'
				]
			];
			return response()->make($response, 422);
		}

		$modelo = Gasto::findOrFail($id);

		if(isset($request['idUsuario']) && $request['idUsuario'] > 0) {
			$modelo->idUsuario = (int) trim($request['idUsuario']);
		}
		if(isset($request['idGastoTipo']) && $request['idGastoTipo'] > 0) {
			$modelo->idGastoTipo = (int) trim($request['idGastoTipo']);
		}
		if(isset($request['monto'])) {
			$modelo->monto = trim($request['monto']);
		}
		if(isset($request['moneda'])) {
			$modelo->moneda = trim($request['moneda']);
		}
		if(isset($request['fecha'])) {
			$modelo->fecha = Carbon::createFromFormat('d/m/Y', $request['fecha']);
		}
		if(isset($request['descripcion'])) {
			$modelo->descripcion = trim($request['descripcion']);
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
	 * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {

		if(Gate::denies('admin.gasto.delete')){
			$response = [
				'Permiso' => [
					'No tiene permisos para ejecutar esta acción'
				]
			];
			return response()->make($response, 422);
		}

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Gasto::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Gasto::select(['*']);
		$builder->with([
			'Usuario.Owner.Agenda',
			'Archivos',
			'GastoTipo',
		]);

		// ----------

		if(isset($request['idUsuario'])) {
			if(is_array($request['idUsuario'])) {
				$usuarioIds = $request['idUsuario'];
			} else {
				$usuarioIds = [$request['idUsuario']];
			}
			$builder->whereIn('idUsuario', $usuarioIds);
		}

		if ( Gate::denies( 'admin.gasto.adminTerceros' ) ) {
			$builder->where( 'idUsuario', Auth::user()->id );
		}

		// ----------

		if(isset($request['fechaInicio'])) {
			$fechaInicio = Carbon::createFromFormat('Y-m-d', $request['fechaInicio']);
			$builder->where('fecha', '>=', "{$fechaInicio->format('Y-m-d')}");
		}
		if(isset($request['fechaFin'])) {
			$fechaFin = Carbon::createFromFormat('Y-m-d', $request['fechaFin']);
			$fechaFin->addDays(1);
			$builder->where('fecha', '<', "{$fechaFin->format('Y-m-d')}");
		}

		// ----------

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
//			if($request['filtro'] == 'aprobados'){
//				$builder->where('estado', Publicacion::ESTADO_APPROVED);
//			}
		}

		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
			$builder->orWhereHas('Usuario.Owner.Agenda', function($q) use($searchValue) {
//				$q->where('nombre')
				$q->where(DB::raw("CONCAT_WS(' ', nombres, apellidos) LIKE '%{$searchValue}%' OR
				  CONCAT_WS(' ', apellidos, nombres) LIKE '%{$searchValue}%'"));
			});
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
