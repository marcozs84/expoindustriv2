<?php

namespace App\Http\Controllers\Resources;

use App\Models\Actividad;
use App\Models\Calendario;
use App\Models\Producto;
use App\Models\ProductoItem;
use App\Models\Tag;
use App\Models\Traduccion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductoController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Producto::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Producto::select(['*']);
		$builder->with([
			'Marca',
			'Modelo',
			'Traducciones',
			'Categorias.Traduccion',
			'Categorias.Padre.Traduccion',
			'Publicacion.Owner.Owner.Agenda',
			'Publicacion.Owner.Owner.Proveedor',
			'Galerias.Imagenes',
			'ProductoItem.Disponibilidad',
			'ProductoItem.Traducciones'
		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if( isset( $request['idDisponibilidad'])) {
			$idsDisponibilidad = [];
			if( is_array( $request['idDisponibilidad'])) {
				$idsDisponibilidad = $request['idDisponibilidad'];
			} else {
				$idsDisponibilidad[] = $request['idDisponibilidad'];
			}
			$builder->whereHas('ProductoItem', function($q) use($idsDisponibilidad) {
				$q->whereIn('idDisponibilidad', $idsDisponibilidad);
			});
		}

		if(isset($request['idUsuarios']) && is_array($request['idUsuarios']) && count($request['idUsuarios']) > 0) {
			$builder->whereIn('owner_id', $request['idUsuarios']);
			$builder->where('owner_type', 'App\Models\Usuario');
		}

		if($searchValue != ''){

			if( preg_match('/,/', $searchValue) > 0 ) {
				$parts_comma = explode(',', $searchValue);
				$allnumeric = true;
				foreach($parts_comma as $part) {
					if(!is_numeric($part) ) {
						$allnumeric = false;
					}
				}
				if ( $allnumeric ) {
					$builder->whereIn('id', $parts_comma);
				}
			} elseif( is_numeric($searchValue) ) {
				$builder->where('id', $searchValue);
			} else {
				$ids = DB::select(
					"SELECT
				  pi.id as id
				FROM
				  Producto p
				  INNER JOIN Marca ma ON p.idMarca = ma.id
				  INNER JOIN Modelo mo ON p.idModelo = mo.id
				  INNER JOIN ProductoItem pi ON p.id = pi.idProducto
				WHERE
				  CONCAT_WS(' ', ma.nombre, mo.nombre) LIKE '%{$searchValue}%' OR
				  CONCAT_WS(' ', mo.nombre, ma.nombre) LIKE '%{$searchValue}%'"
				// CONCAT(ma.nombre,' ', mo.nombre) LIKE '%{$searchValue}%'"
				);

				$ids = array_pluck($ids, 'id');
//				logger(count($ids));

				// ---------- ids traducciones ProductoItem
				$tpi_ids = Traduccion::where('campo', 'nombre')
					->where('owner_type', 'App\\Models\\ProductoItem')
					->where( function($q) use($searchValue) {
						$q->where('es', 'like', "%{$searchValue}%")
							->orWhere('en', 'like', "%{$searchValue}%")
							->orWhere('se', 'like', "%{$searchValue}%");
					})
					->pluck('owner_id')->toArray();
				$pi_ids = ProductoItem::whereIn('id', $tpi_ids)->pluck('idProducto')->toArray();
//				logger(count($pi_ids));

				// ---------- ids traducciones Producto
				$p_ids = Traduccion::where('campo', 'nombre')
					->where('owner_type', 'App\\Models\\Producto')
					->where( function($q) use($searchValue) {
						$q->where('es', 'like', "%{$searchValue}%")
							->orWhere('en', 'like', "%{$searchValue}%")
							->orWhere('se', 'like', "%{$searchValue}%");
					})
					->pluck('owner_id')->toArray();
//				logger(count($p_ids));

//				$mids = array_merge($ids, $pi_ids);
				$mids = [];
//				$mids = $ids + $pi_ids + $p_ids;
				$mids = array_merge($ids, $pi_ids, $p_ids);
				$mids = array_unique($mids);
//				$mids = $ids + $pi_ids;
//				$mids = $ids + $p_ids;
//				logger($mids);
//				logger(count($mids));
//
//				logger($ids);
//				logger($pi_ids);
//				logger($p_ids);
//				logger('---------------------');
//				logger($mids);

				$builder->whereIn('id', $mids);
//				$builder->where('')
			}
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public static function get_grouped_tags() {
		$lang = 'es';

		$tag_counts = DB::select( DB::raw("select count(idProducto) as countp, idTag from ProductoTag GROUP BY idTag") );
		$tag_counts = array_pluck($tag_counts, 'countp', 'idTag');

		$tags2 = Tag::with([
			'Traduccion',
			'Hijos.Traduccion',
		])->where('idPadre', 0)->get();

		$grouped_tags = [];
		foreach( $tags2 as $tag) {
			if( !isset($grouped_tags[ $tag->Traduccion->$lang ]) ) {
				$grouped_tags[ $tag->Traduccion->$lang ] = [];
			}
			foreach( $tag->Hijos as $key => $hijo ) {
				if( $hijo->Traduccion) {
					$grouped_tags[ $tag->Traduccion->$lang ][$hijo->id] = $hijo->Traduccion->$lang;
				}
			}
		}
		return $grouped_tags;
	}

	public function update_tags(Request $request, $id) {
		$this->validate($request, [
			'tags' => 'required',
		]);

		$prod = Producto::findOrFail($id);

		if( isset( $request['tags'] ) ) {
			$prod->Tags()->sync( $request['tags'] );
		}

		$prod->load('Tags');

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $prod
		);
		return response()->json($response);
	}

	public function getActividades( Request $request ) {
		$this->validate($request, [
			'ids' => 'required|array',
		]);

		$p = Producto::with([
//			'Calendarios' => function( $q ) {
//				$q->where('idCalendarioTipo', Calendario::TIPO_HISTORIA);
//			},
			'Calendarios.Actividades.Responsables',
		])
			->whereIn('id', $request['ids'])
			->get();

		$response = Array(
			'result' => true,
			'message' => 'Productos con actividades.',
			'data' => $p->toArray()
		);

		return response()->json( $response );
	}

	public function setActividades( Request $request ) {
		$this->validate($request, [
			'idProductos' => 'required|array',
			'idResponsables' => 'array',
			'idActividadTipos' => 'required|array',
			'descripcion' => '',
		]);

		$productos = Producto::with([
			'Calendarios'
		])->whereIn( 'id', $request['idProductos'] )->get();

		$response_actividades = [];

		foreach( $productos as $producto ) {
			$cal = $producto->Calendarios->where('idCalendarioTipo', Calendario::TIPO_HISTORIA )->first();
			if( !$cal ) {
				$cal = $producto->Calendarios()->create( [
					'idCalendarioTipo' => Calendario::TIPO_HISTORIA,
				] );
			}

			foreach( $request['idActividadTipos'] as $idActividadTipo ) {

				if( $cal->Actividades->where('idActividadTipo', $idActividadTipo)->count() > 0 ) {
					$a = $cal->Actividades->where('idActividadTipo', $idActividadTipo)->first();
				} else {
					$a = $cal->Actividades()->create( [
						'idActividadTipo' => (int)$idActividadTipo,
						'fechaInicio' => Carbon::now(),
						'descripcion' => $request['descripcion'],
					] );
//				$response_actividades[] = $a->load('Responsables');
				}

				if( isset( $request['idResponsables'] ) && count($request['idResponsables']) > 0) {
					$idResponsables = $a->Responsables->pluck('id')->toArray();
					$idResponsables_merge = array_merge( $idResponsables, $request['idResponsables'] );
					$a->Responsables()->sync( $idResponsables_merge, ['created_at' => Carbon::today() ] );
				}

				$a->load('ActividadTipo');
				$a->load('Responsables');
				$response_actividades[] = $a;

			}

		}

		$response = Array(
			'result' => true,
			'message' => 'Listo!',
			'data' => $response_actividades
		);

		return response()->json( $response );
	}

}
