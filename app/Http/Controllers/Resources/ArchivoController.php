<?php

namespace App\Http\Controllers\Resources;

use App\Models\Archivo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArchivoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'owner_type'    => '',
			'owner_id'      => 'numeric',
			'realname'      => '',
			'ruta'          => '',
			'filename'      => '',
			'mimetype'      => '',
			'descripcion'   => '',
			'estado'        => 'numeric',
			'extrainfo'     => '',
			'fuente'        => '',

		]);

		//$archivo = Archivo::create($request->all());
		$archivo = new Archivo();
		$archivo->owner_type    = trim($request['owner_type']);
		$archivo->owner_id      = (int) $request['owner_id'];
		$archivo->realname      = trim($request['realname']);
		$archivo->ruta          = trim($request['ruta']);
		$archivo->filename      = trim($request['filename']);
		$archivo->mimetype      = trim($request['mimetype']);
		$archivo->descripcion   = trim($request['descripcion']);
		$archivo->estado        = (int) $request['estado'];
		$archivo->extrainfo     = trim($request['extrainfo']);
		$archivo->fuente        = trim($request['fuente']);

		$archivo->save();

		if($archivo){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $archivo
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'owner_type'    => '',
			'owner_id'      => 'numeric',
			'realname'      => '',
			'ruta'          => '',
			'filename'      => '',
			'mimetype'      => '',
			'descripcion'   => '',
			'estado'        => 'numeric',
			'extrainfo'     => '',
			'fuente'        => '',
		]);

		$archivo = Archivo::findOrFail($id);

		if( isset($request['owner_type']) )   $archivo->owner_type    = trim($request['owner_type']);
		if( isset($request['owner_id']) )     $archivo->owner_id      = (int) $request['owner_id'];
		if( isset($request['realname']) )     $archivo->realname      = trim($request['realname']);
		if( isset($request['ruta']) )         $archivo->ruta          = trim($request['ruta']);
		if( isset($request['filename']) )     $archivo->filename      = trim($request['filename']);
		if( isset($request['mimetype']) )     $archivo->mimetype      = trim($request['mimetype']);
		if( isset($request['descripcion']) )  $archivo->descripcion   = trim($request['descripcion']);
		if( isset($request['estado']) )       $archivo->estado        = (int) $request['estado'];
		if( isset($request['extrainfo']) )    $archivo->extrainfo     = trim($request['extrainfo']);
		if( isset($request['fuente']) )       $archivo->fuente        = trim($request['fuente']);

		$archivo->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $archivo
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Archivo::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$archivos = Archivo::whereIn('id', $ids)->withTrashed()->get();
		foreach($archivos as $archivo) {
			if($archivo->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Archivo::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			//$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['estados']) && $request['estados'] > 0){
			if( !is_array($request['estados']) ) {
				$request['estados'] = [ $request['estados'] ];
			}
			$builder->whereIn('estado', $request['estados']);
		}
		if(isset($request['estado']) && $request['estado'] > 0){
			$builder->where('estado', $request['estado']);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function download($id) {
		$archivo = Archivo::findOrFail($id);
		return response()->download(storage_path($archivo->ruta), $archivo->realname);
	}

	public function stream($id) {
		$archivo = Archivo::findOrFail($id);
		$headers = [
			'Content-Type', $archivo->mimetype,
		];
		return response()->file(storage_path($archivo->ruta), $headers);
//		return response()->download(storage_path($archivo->ruta));
	}

}
