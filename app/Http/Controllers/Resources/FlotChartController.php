<?php

namespace App\Http\Controllers\Resources;

use App\Models\Cliente;
use App\Models\Oferta;
use App\Models\Orden;
use App\Models\Publicacion;
use App\Models\Role;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FlotChartController extends Controller {

	public function getPublicaciones(Request $request){

		$this->validate($request, [
//			'fechaInicio' => 'required',
//			'fechaFin' => 'required',
//			'vendedoresId' => '',
//			'agrupacion' => '',
		]);

		// ---------- FECHAS -> TICKS

		$ticks = [];
		$ticks2 = [];

//		DB::enableQueryLog();

		$select = [

			DB::raw("count('id') as count"),
			DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
		];
		$groupBy = [ DB::raw('date') ];


		$builder = Publicacion::select($select);
//		$builder->where('created_at', '>=', "{$fechaInicio->format('Y/m/d')}")
//			->where('created_at', '<', "{$fechaFin->addDays(1)->format('Y/m/d')}");

//		if(count($vendedores) > 0) {
//			$builder->whereIn('idVendedor', $vendedores->pluck('id')->toArray());
//		}

		$builder->groupBy($groupBy)
			->orderBy('created_at');

		$result = $builder->get();

		$inicio = Carbon::createFromFormat('d.m.Y', $result->first()->date);
		$fin = Carbon::createFromFormat('d.m.Y', $result->last()->date);

//		logger(DB::getQueryLog());

		$resultados = [];

		$com0 = $result->toArray();
		$resGrupal = $this->flot_format($com0, $ticks);
		$resSpark = $this->spark_format($com0, $ticks);
		$resultados[] = [
			'id' => 0,
			'nombre' => 'Publicaciones',
			'datos' => $resGrupal,
			'spark' => $resSpark,
		];

		$data = [
			'datos' => $resultados,
//			'datos' => [
//				[
//					'id' => 1,
//					'nombre' => 1,
//					'data' => $datos[],
//				]
//			],
			'ticks' => $ticks2,
			'begin' => $inicio->getTimestamp(),
			'end' => $fin->getTimestamp(),
		];
		$response = Array(
			'result' => true,
			'message' => 'Descarga de datos correcta.',
			'data' => $data
		);
		return response()->json($response);

	}

	public function getSubscribers(Request $request){

		$this->validate($request, [
			'fechaInicio' => '',
			'fechaFin' => '',
			'agrupacion' => '',
		]);

		// ---------- FECHAS -> TICKS

		if ( isset( $request['fechaInicio'] ) )
			$fechaInicio = Carbon::createFromFormat('Y/m/d', $request['fechaInicio']);
		if ( isset( $request['fechaFin'] ) )
			$fechaFin = Carbon::createFromFormat('Y/m/d', $request['fechaFin']);

		if ( isset( $request['fechaInicio'] ) && isset( $request['fechaFin'] ) ) {
			if(!$fechaInicio->lessThanOrEqualTo($fechaFin)) {
				$response = [
					'Fechas' => [
						'Fecha de inicio debe ser menor a la fecha final'
					]
				];
				return response()->make($response, 422);
			}
		}

//		DB::enableQueryLog();

		if($request['agrupacion'] == 'individual') {
			$select = [
				'idVendedor',
				DB::raw("count('id') as count"),
				DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
			];
			$groupBy = [ 'idVendedor', DB::raw('date') ];
		} else {
			$select = [
				DB::raw("count('id') as count"),
				DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
			];
			$groupBy = [ DB::raw('date') ];
		}

		$builder = Cliente::select($select);

		if ( isset( $request['fechaInicio'] ) )
			$builder->where('created_at', '>=', "{$fechaInicio->format('Y/m/d')}");

		if ( isset( $request['fechaFin'] ) )
			$builder->where('created_at', '<', "{$fechaFin->addDays(1)->format('Y/m/d')}");


		$builder->groupBy($groupBy)
			->orderBy('created_at');

		$result = $builder->get();

		if ( $result->count() > 0 ) {
			$inicio = Carbon::createFromFormat('d.m.Y', $result->first()->date);
			$fin = Carbon::createFromFormat('d.m.Y', $result->last()->date);
		} else {
			$inicio = Carbon::today();
			$fin = Carbon::today();
		}

		// ----------

		$ticks = [];
		$ticks2 = [];

		for( $fechaIt = clone $inicio ; $fechaIt->lessThanOrEqualTo($fin) ; $fechaIt->addDay(1)){
			$ticks[] = $fechaIt->format('d.m.Y');
			$ticks2[] = $fechaIt->format('M j \'y');
		}

//		logger(DB::getQueryLog());
//		$log = DB::getQueryLog();
//		$log = end($log);
//		logger($this->getPureSql($log['query'], $log['bindings']));

		$resultados = [];

		$com0 = $result->toArray();
		$resGrupal = $this->flot_format($com0, $ticks);
		$resSpark = $this->spark_format($com0, $ticks);
		$resultados[] = [
			'id' => 0,
			'nombre' => 'General',
			'datos' => $resGrupal,
			'spark' => $resSpark,
		];

		$data = [
			'datos' => $resultados,
//			'datos' => [
//				[
//					'id' => 1,
//					'nombre' => 1,
//					'data' => $datos[],
//				]
//			],
			'ticks' => $ticks2,
			'begin' => $inicio->getTimestamp(),
			'end' => $fin->getTimestamp(),
		];
		$response = Array(
			'result' => true,
			'message' => 'Descarga de datos correcta.',
			'data' => $data
		);
		return response()->json($response);

	}

	public function getOfertas(Request $request){

		$this->validate($request, [
			'fechaInicio' => '',
			'fechaFin' => '',
			'idVendedor' => '', // Can be one or an array of IDs
			'agrupacion' => '',
		]);

		// ---------- FECHAS -> TICKS

		if ( isset( $request['fechaInicio'] ) )
			$fechaInicio = Carbon::createFromFormat('Y/m/d', $request['fechaInicio']);
		if ( isset( $request['fechaFin'] ) )
			$fechaFin = Carbon::createFromFormat('Y/m/d', $request['fechaFin']);

		if ( isset( $request['fechaInicio'] ) && isset( $request['fechaFin'] ) ) {
			if(!$fechaInicio->lessThanOrEqualTo($fechaFin)) {
				$response = [
					'Fechas' => [
						'Fecha de inicio debe ser menor a la fecha final'
					]
				];
				return response()->make($response, 422);
			}
		}

		$vendedores = [];
		if ( isset( $request['idVendedor'] ) ) {
			if ( is_array( $request['idVendedor'] ) ) {
				$vendedorIds = $request['idVendedor'];
			} else {
				$vendedorIds = [ $request['idVendedor'] ];
			}
			$vendedores = Usuario::with([
				'Owner.Agenda',
			])
				->whereIn('id', $request['idVendedor'])->get();
		} else {
			$vendedores = Usuario::with([
				'Roles',
				'Owner.Agenda',
			])
				->whereHas('Roles', function($q) {
					$q->where('id', Role::VENDEDOR);
				})
				->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR)
				->get();
		}

//		DB::enableQueryLog();

		if($request['agrupacion'] == 'individual') {
			$select = [
				'idVendedor',
				DB::raw("count('id') as count"),
				DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
			];
			$groupBy = [ 'idVendedor', DB::raw('date') ];
		} else {
			$select = [
				DB::raw("count('id') as count"),
				DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
			];
			$groupBy = [ DB::raw('date') ];
		}

		$builder = Oferta::select($select);

		if ( isset( $request['fechaInicio'] ) )
			$builder->where('created_at', '>=', "{$fechaInicio->format('Y/m/d')}");

		if ( isset( $request['fechaFin'] ) )
			$builder->where('created_at', '<', "{$fechaFin->addDays(1)->format('Y/m/d')}");

		if(count($vendedores) > 0)
			$builder->whereIn('idVendedor', $vendedores->pluck('id')->toArray());

		$builder->groupBy($groupBy)
			->orderBy('created_at');

		$result = $builder->get();

		if ( $result->count() > 0 ) {
			$inicio = Carbon::createFromFormat('d.m.Y', $result->first()->date);
			$fin = Carbon::createFromFormat('d.m.Y', $result->last()->date);
		} else {
			$inicio = Carbon::today();
			$fin = Carbon::today();
		}

		// ----------

		$ticks = [];
		$ticks2 = [];

		for( $fechaIt = clone $inicio ; $fechaIt->lessThanOrEqualTo($fin) ; $fechaIt->addDay(1)){
			$ticks[] = $fechaIt->format('d.m.Y');
			$ticks2[] = $fechaIt->format('M j \'y');
		}

//		logger(DB::getQueryLog());
//		$log = DB::getQueryLog();
//		$log = end($log);
//		logger($this->getPureSql($log['query'], $log['bindings']));

		$resultados = [];

		if($request['agrupacion'] == 'individual') {
			foreach($vendedores as $vendedor) {
				$datos = $result->where('idVendedor', $vendedor->id)->toArray();
				$datosVendedor = $this->flot_format($datos, $ticks);
				$datosVendedorSpark = $this->spark_format($datos, $ticks);
				$resultados[] = [
					'id' => $vendedor->id,
					'nombre' => $vendedor->Owner->Agenda->nombres_apellidos,
					'datos' => $datosVendedor,
					'spark' => $datosVendedorSpark,
				];
			}
		} else {
			$com0 = $result->toArray();
			$resGrupal = $this->flot_format($com0, $ticks);
			$resSpark = $this->spark_format($com0, $ticks);
			$resultados[] = [
				'id' => 0,
				'nombre' => 'General',
				'datos' => $resGrupal,
				'spark' => $resSpark,
			];
		}

		$data = [
			'datos' => $resultados,
//			'datos' => [
//				[
//					'id' => 1,
//					'nombre' => 1,
//					'data' => $datos[],
//				]
//			],
			'ticks' => $ticks2,
			'begin' => $inicio->getTimestamp(),
			'end' => $fin->getTimestamp(),
		];
		$response = Array(
			'result' => true,
			'message' => 'Descarga de datos correcta.',
			'data' => $data
		);
		return response()->json($response);

	}

	public function getOrdenes(Request $request) {
		$this->validate($request, [
			'fechaInicio' => '',
			'fechaFin' => '',
			'idVendedor' => '', // Can be one or an array of IDs
			'agrupacion' => '',
		]);

		// ---------- FECHAS -> TICKS

		if ( isset( $request['fechaInicio'] ) )
			$fechaInicio = Carbon::createFromFormat('Y/m/d', $request['fechaInicio']);
		if ( isset( $request['fechaFin'] ) )
			$fechaFin = Carbon::createFromFormat('Y/m/d', $request['fechaFin']);

		if ( isset( $request['fechaInicio'] ) && isset( $request['fechaFin'] ) ) {
			if(!$fechaInicio->lessThanOrEqualTo($fechaFin)) {
				$response = [
					'Fechas' => [
						'Fecha de inicio debe ser menor a la fecha final'
					]
				];
				return response()->make($response, 422);
			}
		}

		$vendedores = [];
		if ( isset( $request['idVendedor'] ) ) {
			if ( is_array( $request['idVendedor'] ) ) {
				$vendedorIds = $request['idVendedor'];
			} else {
				$vendedorIds = [ $request['idVendedor'] ];
			}
			$vendedores = Usuario::with([
				'Owner.Agenda',
			])
				->whereIn('id', $request['idVendedor'])->get();
		} else {
			$vendedores = Usuario::with([
				'Roles',
				'Owner.Agenda',
			])
				->whereHas('Roles', function($q) {
					$q->where('id', Role::VENDEDOR);
				})
				->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR)
				->get();
		}

//		DB::enableQueryLog();

		if($request['agrupacion'] == 'individual') {
			$select = [
				'idVendedor',
				DB::raw("count('id') as count"),
				DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
			];
			$groupBy = [ 'idVendedor', DB::raw('date') ];
		} else {
			$select = [
				DB::raw("count('id') as count"),
				DB::raw('DATE_FORMAT(created_at, "%d.%m.%Y") as date')
			];
			$groupBy = [ DB::raw('date') ];
		}

		$builder = Orden::select($select);

		if ( isset( $request['fechaInicio'] ) )
			$builder->where('created_at', '>=', "{$fechaInicio->format('Y/m/d')}");

		if ( isset( $request['fechaFin'] ) )
			$builder->where('created_at', '<', "{$fechaFin->addDays(1)->format('Y/m/d')}");

		if(count($vendedores) > 0)
			$builder->whereIn('idVendedor', $vendedores->pluck('id')->toArray());

		$builder->groupBy($groupBy)
			->orderBy('created_at');

		$result = $builder->get();

		if ( $result->count() > 0 ) {
			$inicio = Carbon::createFromFormat('d.m.Y', $result->first()->date);
			$fin = Carbon::createFromFormat('d.m.Y', $result->last()->date);
		} else {
			$inicio = Carbon::today();
			$fin = Carbon::today();
		}

		// ----------

		$ticks = [];
		$ticks2 = [];

		for( $fechaIt = clone $inicio ; $fechaIt->lessThanOrEqualTo($fin) ; $fechaIt->addDay(1)){
			$ticks[] = $fechaIt->format('d.m.Y');
			$ticks2[] = $fechaIt->format('M j \'y');
		}

//		logger(DB::getQueryLog());
//		$log = DB::getQueryLog();
//		$log = end($log);
//		logger($this->getPureSql($log['query'], $log['bindings']));

		$resultados = [];

		if($request['agrupacion'] == 'individual') {
			foreach($vendedores as $vendedor) {
				$datos = $result->where('idVendedor', $vendedor->id)->toArray();
				$datosVendedor = $this->flot_format($datos, $ticks);
				$datosVendedorSpark = $this->spark_format($datos, $ticks);
				$resultados[] = [
					'id' => $vendedor->id,
					'nombre' => $vendedor->Owner->Agenda->nombres_apellidos,
					'datos' => $datosVendedor,
					'spark' => $datosVendedorSpark,
				];
			}
		} else {
			$com0 = $result->toArray();
			$resGrupal = $this->flot_format($com0, $ticks);
			$resSpark = $this->spark_format($com0, $ticks);
			$resultados[] = [
				'id' => 0,
				'nombre' => 'General',
				'datos' => $resGrupal,
				'spark' => $resSpark,
			];
		}

		$data = [
			'datos' => $resultados,
//			'datos' => [
//				[
//					'id' => 1,
//					'nombre' => 1,
//					'data' => $datos[],
//				]
//			],
			'ticks' => $ticks2,
			'begin' => $inicio->getTimestamp(),
			'end' => $fin->getTimestamp(),
		];
		$response = Array(
			'result' => true,
			'message' => 'Descarga de datos correcta.',
			'data' => $data
		);
		return response()->json($response);
	}


	// ---------- TOOLS

	private function flot_format($data, $ticks){
		$count = 0;
		$result = [];
		foreach($data as $ar){
			$pos = array_search($ar['date'], $ticks);

			$timestamp = Carbon::createFromFormat('d.m.Y', $ar['date'])->getTimestamp();

			$result[] = [$timestamp, $ar['count'], $ar['date'], $timestamp];
			//         (  x  ,      y      ,  otros adicionales )
			$count++;
		}

//		for($i = 0 ; $i < count($ticks) ; $i++) {
//
//			$countD = 0;
//			foreach($data as $ar) {
//				if($ar['date'] == $ticks[$i]) {
//					$countD = $ar['count'];
//				}
//			}
//			$result[] = [$i, $countD, $ticks[$i]];
//			//         (  x  ,  y   ,  otros adicionales )
//		}

		return $result;
	}

	private function spark_format($data, $ticks){
		$count = 0;
		$result = [];

		for($i = 0 ; $i < count($ticks) ; $i++) {
			$countD = 0;
			foreach($data as $ar) {
				if($ar['date'] == $ticks[$i]) {
					$countD = $ar['count'];
				}
			}
			$result[] = $countD;
		}

		return $result;
	}
}
