<?php

namespace App\Http\Controllers\Resources;

use App\Models\Actividad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ActividadController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'owner_type'        => '',
			'owner_id'          => 'numeric',
			'idActividadTipo'   => 'numeric',
			'idCalendario'      => 'numeric',
			'titulo'            => '',
			'descripcion'       => '',
			'fechaInicio'       => 'required',
			'fechaFin'          => 'required',
			'linked'            => '',
			'completado'        => 'numeric',
			'extrainfo'         => '',
			
		]);

		//$actividad = Actividad::create($request->all());
		$actividad = new Actividad();
		$actividad->owner_type        = trim($request['owner_type']);
		$actividad->owner_id          = (int) $request['owner_id'];
		$actividad->idActividadTipo   = (int) $request['idActividadTipo'];
		$actividad->idCalendario      = (int) $request['idCalendario'];
		$actividad->titulo            = trim($request['titulo']);
		$actividad->descripcion       = trim($request['descripcion']);
		$actividad->fechaInicio       = Carbon::createFromFormat('d/m/Y H:i:s', $request['fechaInicio']);
		$actividad->fechaFin          = Carbon::createFromFormat('d/m/Y H:i:s', $request['fechaFin']);
		$actividad->linked            = trim($request['linked']);
		$actividad->completado        = (int) $request['completado'];
		$actividad->extrainfo         = trim($request['extrainfo']);
		
		$actividad->save();

		if($actividad){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $actividad
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'owner_type'        => '',
			'owner_id'          => 'numeric',
			'idActividadTipo'   => 'numeric',
			'idCalendario'      => 'numeric',
			'titulo'            => '',
			'descripcion'       => '',
			'fechaInicio'       => 'required',
			'fechaFin'          => '',
			'linked'            => '',
			'completado'        => 'numeric',
			'extrainfo'         => '',
			
		]);

		$actividad = Actividad::findOrFail($id);

		if( isset($request['owner_type']) )       $actividad->owner_type        = trim($request['owner_type']);
		if( isset($request['owner_id']) )         $actividad->owner_id          = (int) $request['owner_id'];
		if( isset($request['idActividadTipo']) )  $actividad->idActividadTipo   = (int) $request['idActividadTipo'];
		if( isset($request['idCalendario']) )     $actividad->idCalendario      = (int) $request['idCalendario'];
		if( isset($request['titulo']) )           $actividad->titulo            = trim($request['titulo']);
		if( isset($request['descripcion']) )      $actividad->descripcion       = trim($request['descripcion']);
		if( isset($request['fechaInicio']) )      $actividad->fechaInicio       = Carbon::createFromFormat('d/m/Y H:i:s', $request['fechaInicio']);
		if( isset($request['fechaFin']) )         $actividad->fechaFin          = Carbon::createFromFormat('d/m/Y H:i:s', $request['fechaFin']);
		if( isset($request['linked']) )           $actividad->linked            = trim($request['linked']);
		if( isset($request['completado']) )       $actividad->completado        = (int) $request['completado'];
		if( isset($request['extrainfo']) )        $actividad->extrainfo         = trim($request['extrainfo']);
		
		$actividad->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $actividad
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Actividad::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$actividads = Actividad::whereIn('id', $ids)->withTrashed()->get();
		foreach($actividads as $actividad) {
			if($actividad->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Actividad::select(['*']);
		$builder->with([
			'ActividadTipo',
			'Calendario.Owner',
		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			//$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('id', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function addComentario( Request $request, $id ) {

		$this->validate($request, [
			'idUsuario' => '',
			'idPadre' => '',
			'asunto' => '',
			'mensaje' => '',
		]);

		$idUsuario = 0;
		$esCliente = false;
		if( Auth::guard('empleados')->user() ) {
			$idUsuario = Auth::guard('empleados')->user()->id;
		} elseif( Auth::guard('clientes')->user() ) {
			$idUsuario = Auth::guard('clientes')->user()->id;
		}

		$actividad = Actividad::findOrFail( $id );

		if( $idUsuario == 0 || $esCliente ) {
			$idProducto = $actividad->Calendario->Owner->id;
			$this->reportDev('Nuevo mensaje recibido en Actividad: ' . $id . '<br><b>URL</b>: '. route('admin.producto.historia', [ $idProducto ]) . '<br><b>Mensaje:</b> <br>'. $request['mensaje']);
		}

		$comentario = $actividad->Comentarios()->create([
			'idUsuario'	=> $idUsuario,
			'idPadre'	=> (int)$request['idPadre'],
			'asunto'	=> $request['asunto'],
			'mensaje'	=> trim($request['mensaje']),
		]);

//		$comentarios = $actividad->Comentarios;

		$response = Array(
			'result'	=> true,
			'message'	=> 'Listo!.',
			'data'		=> $comentario->toArray()
		);

		return response()->json( $response );

	}

	public function getComentarios( Request $request, $id ) {

		$builder = Actividad::select(['*']);
		$builder->with([
			'Comentarios.Usuario'
		]);

		if( isset( $request['ids'] ) ) {
			$builder->whereIn('id', $request['ids'] );
			$actividades = $builder->get();
			$comentarios = [];
			foreach($actividades as $actividad) {
				$comentarios[ $actividad->id ] = $actividad->Comentarios;
			}

		} else {
			$builder->where( 'id', $id );
			$actividad = $builder->first();
			$comentarios = $actividad->Comentarios->toArray();
		}

//		$actividad = Actividad::with([
//			'Comentarios.Usuario'
//		])->findOrFail( $id );
//		$comentarios = $actividad->Comentarios;

		$response = Array(
			'result' => true,
			'message' => 'Listo!.',
			'data' => $comentarios
		);

		return response()->json( $response );
	}

}
