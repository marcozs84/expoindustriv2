<?php

namespace App\Http\Controllers\Resources;

use App\Models\ActividadTipo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActividadTipoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idPadre'       => '', //'numeric',
			'nombre'        => 'required',
			'descripcion'   => '',
			'orden'         => '', //'numeric',
			
		]);

		//$actividadTipo = ActividadTipo::create($request->all());
		$actividadTipo = new ActividadTipo();
		$actividadTipo->idPadre       = (int) $request['idPadre'];
		$actividadTipo->nombre        = trim($request['nombre']);
		$actividadTipo->descripcion   = trim($request['descripcion']);
		$actividadTipo->orden         = (int) $request['orden'];
		
		$actividadTipo->save();

		if($actividadTipo){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $actividadTipo
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idPadre'       => '', //'numeric',
			'nombre'        => '',
			'descripcion'   => '',
			'orden'         => '', //'numeric',
			
		]);

		$actividadTipo = ActividadTipo::findOrFail($id);

		if( isset($request['idPadre']) )      $actividadTipo->idPadre       = (int) $request['idPadre'];
		if( isset($request['nombre']) )       $actividadTipo->nombre        = trim($request['nombre']);
		if( isset($request['descripcion']) )  $actividadTipo->descripcion   = trim($request['descripcion']);
		if( isset($request['orden']) )        $actividadTipo->orden         = (int) $request['orden'];
		
		$actividadTipo->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $actividadTipo
		);
		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = ActividadTipo::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$actividadTipos = ActividadTipo::whereIn('id', $ids)->withTrashed()->get();
		foreach($actividadTipos as $actividadTipo) {
			if($actividadTipo->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'data' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'data' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = ActividadTipo::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if ( isset( $request['soloGrupos'] ) && $request['soloGrupos'] == 1 ) {
			$builder->where('idPadre', 0);
		}

		if ( isset( $request['soloHijos'] ) && $request['soloHijos'] == 1 ) {
			$builder->where('idPadre', '<>', 0);
		}

		if ( isset( $request['idPadre'] ) ) {
			if ( is_array( $request['idPadre'] ) ) {
				$builder->whereIn( 'id', $request['idPadre'] );
				$builder->orWhereIn( 'idPadre', $request['idPadre'] );
			} else {
				$builder->where( 'id', $request['idPadre'] );
				$builder->orWhereIn( 'idPadre', $request['idPadre'] );
			}
		}

		if ( $searchValue != '' ) {
			$builder->where( 'nombre', 'like', '%' . $searchValue . '%' );
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('idPadre', 'asc');
			$builder->orderBy('orden', 'asc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

}
