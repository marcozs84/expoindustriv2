<?php

namespace App\Http\Controllers\Resources;

use App\Models\Configuracion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfiguracionController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
    	logger("ACTUALIZANDO");
		$this->validate($request, [
			'idUsuario' => '',
			'llave' => '',
			'valor' => '',
			'default' => '',
			'esSistema' => '',
		]);

		$modelo = Configuracion::findOrFail($id);

		if(isset($request['idUsuario'])){
			$modelo->idUsuario = trim($request['idUsuario']);
		}
		if(isset($request['llave'])){
			$modelo->llave = trim($request['llave']);
		}
		if(isset($request['valor'])){
			$modelo->valor = trim($request['valor']);
		}
		if(isset($request['default'])){
			$modelo->default = trim($request['default']);
		}
		if(isset($request['esSistema'])){
			$modelo->esSistema = trim($request['esSistema']);
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Configuracion::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
			if($request['filtro'] == 'divisas'){
				$builder->whereIn('llave', [
					'tipo_cambio_sek_usd',
					'tipo_cambio_sek_eur',
					'tipo_cambio_eur_sek',
					'tipo_cambio_eur_usd',
					'tipo_cambio_usd_sek',
					'tipo_cambio_usd_eur',
				]);
			}
		}

//		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
//		}

		if(isset($request['draw'])) {	// llamada DataTable
//			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
