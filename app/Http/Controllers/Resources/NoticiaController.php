<?php

namespace App\Http\Controllers\Resources;

use App\Models\Noticia;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoticiaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->validate($request, [
			'titulo_es' => 'required',
			'resumen_es' => 'required',
			'titulo_en' => 'required',
			'resumen_en' => 'required',
			'titulo_se' => 'required',
			'resumen_se' => 'required',
			'fuente' => 'required',
			'fuenteUrl' => '',
			'pais' => '',
			'fechaPublicacion' => 'required',
		]);

		$modelo = new Noticia();

		if(isset($request['titulo_es'])) {
			$modelo->titulo_es = trim($request['titulo_es']);
		}
		if(isset($request['resumen_es'])) {
			$modelo->resumen_es = trim($request['resumen_es']);
		}
		if(isset($request['titulo_en'])) {
			$modelo->titulo_en = trim($request['titulo_en']);
		}
		if(isset($request['resumen_en'])) {
			$modelo->resumen_en = trim($request['resumen_en']);
		}
		if(isset($request['titulo_se'])) {
			$modelo->titulo_se = trim($request['titulo_se']);
		}
		if(isset($request['resumen_se'])) {
			$modelo->resumen_se = trim($request['resumen_se']);
		}
		if(isset($request['fuente'])) {
			$modelo->fuente = trim($request['fuente']);
		}
		if(isset($request['fuenteUrl'])) {
			$modelo->fuenteUrl = trim($request['fuenteUrl']);
		}
		if(isset($request['pais'])) {
			$modelo->pais = trim($request['pais']);
		}
		if(isset($request['fechaPublicacion'])) {
			$modelo->fechaPublicacion = Carbon::createFromFormat('d/m/Y', trim($request['fechaPublicacion']));
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'titulo_es' => '',
			'resumen_es' => '',
			'titulo_en' => '',
			'resumen_en' => '',
			'titulo_se' => '',
			'resumen_se' => '',
			'fuente' => '',
			'fuenteUrl' => '',
			'pais' => '',
			'fechaPublicacion' => '',
		]);

		$modelo = Noticia::findOrFail($id);

		if(isset($request['titulo_es'])) {
			$modelo->titulo_es = trim($request['titulo_es']);
		}
		if(isset($request['resumen_es'])) {
			$modelo->resumen_es = trim($request['resumen_es']);
		}
		if(isset($request['titulo_en'])) {
			$modelo->titulo_en = trim($request['titulo_en']);
		}
		if(isset($request['resumen_en'])) {
			$modelo->resumen_en = trim($request['resumen_en']);
		}
		if(isset($request['titulo_se'])) {
			$modelo->titulo_se = trim($request['titulo_se']);
		}
		if(isset($request['resumen_se'])) {
			$modelo->resumen_se = trim($request['resumen_se']);
		}
		if(isset($request['fuente'])) {
			$modelo->fuente = trim($request['fuente']);
		}
		if(isset($request['fuenteUrl'])) {
			$modelo->fuenteUrl = trim($request['fuenteUrl']);
		}
		if(isset($request['pais'])) {
			$modelo->pais = trim($request['pais']);
		}
		if(isset($request['fechaPublicacion'])) {
			$modelo->fechaPublicacion = Carbon::createFromFormat('d/m/Y', trim($request['fechaPublicacion']));
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Noticia::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Noticia::select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if(isset($request['filtro'])){
//			if($request['filtro'] == 'aprobados'){
//				$builder->where('estado', Publicacion::ESTADO_APPROVED);
//			}
		}

		if($searchValue != ''){
			$builder->where('titulo_es', 'like', '%'.$searchValue.'%');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
