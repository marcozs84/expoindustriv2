<?php

namespace App\Http\Controllers\Resources;

use App\Models\Proveedor;
use App\Models\ProveedorContacto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProveedorContactoController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		/* ========================================================================================
		 *
		 * La creación de contactos está ligada a otras entidades como:
		 * Agenda, Cliente, ProveedorContacto.
		 * Se recomienda utilizar el flujo de creación usado en Admin/UsuarioController@store
		 *
		 * ========================================================================================
		 */
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idPadre' => '',
			'idCiudad' => '',
			'nombre' => 'required',
			'nit' => '',
			'idProveedorEstado' => '',
			'urlSitio' => '',
			'urlFacebook' => '',
			'urlTwitter' => '',
//			'tieneStore' => ',
			'descripcion' => '',
		]);

		$modelo = Proveedor::findOrFail($id);

		if(isset($request['idPadre'])) {
			$modelo->idPadre = (int) trim($request['idPadre']);
		}
		if(isset($request['idCiudad'])) {
			$modelo->idCiudad = (int) trim($request['idCiudad']);
		}

		if($modelo->idCiudad == 0) {
			$modelo->idCiudad = null;
		}

		if(isset($request['nombre'])) {
			$modelo->nombre = trim($request['nombre']);
		}

		if(isset($request['nit'])) {
			$modelo->nit = trim($request['nit']);
		}
		if(isset($request['descripcion'])) {
			$modelo->descripcion = trim($request['descripcion']);
		}
		if(isset($request['idProveedorEstado'])) {
			$modelo->idProveedorEstado = trim($request['idProveedorEstado']);
		}
//		if(isset($request['idProveedorTipo'])) {
		$modelo->idProveedorTipo = 0; //trim($request['idProveedorTipo']);
//		}
//		if(isset($request['tieneStore'])) {
//			$modelo->tieneStore = (int) $request['tieneStore'];
//		}
		if(isset($request['urlSitio'])) {
			$modelo->urlSitio = trim($request['urlSitio']);
		}
		if(isset($request['urlFacebook'])) {
			$modelo->urlFacebook = trim($request['urlFacebook']);
		}
		if(isset($request['urlTwitter'])) {
			$modelo->urlTwitter = trim($request['urlTwitter']);
		}

		$modelo->save();

		if( isset($request['telefono']) ) {
			$modelo->load('Telefonos');
			$telefonos = $modelo->Telefonos;
			if($telefonos->count() > 0) {
				$telefono = $telefonos[0];
				$telefono->numero = $request['telefono'];
				$telefono->save();
			} else {
				$modelo->Telefonos()->create([
					'numero' => $request['telefono'],
					'descripcion' => '',
					'estado' => 1
				]);
			}
		}

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo->load('Telefonos')
		);

		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Proveedor::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}


		$builder = ProveedorContacto::select(['*']);
		$builder->with([
			'Usuario.Cliente',
			'Agenda',
			'Proveedor.Telefonos',
			'Proveedor.Direcciones',
			'Proveedor.ProveedorContactos.Agenda',
			'Proveedor.ProveedorContactos.Usuario',
		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

//		if(isset($request['filtro'])){
//			if($request['filtro'] == 'aprobados'){
//				$builder->where('estado', Publicacion::ESTADO_APPROVED);
//			}
//		}

		if(isset($request['idProveedor'])){
			$idProveedores = $request['idProveedor'];
			$builder->whereIn('idProveedor', $idProveedores);
		}

		if($searchValue != ''){
			$builder->whereHas('Proveedor', function($q) use($searchValue) {
				$q->where('nombre', 'like', '%'.$searchValue.'%');
			});
			$builder->orWhereHas('Agenda', function($q) use($searchValue) {
				$q->where(DB::raw("CONCAT_WS(' ', nombres, apellidos) LIKE '%{$searchValue}%' OR
				  						CONCAT_WS(' ', apellidos, nombres) LIKE '%{$searchValue}%'"));
			});
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
