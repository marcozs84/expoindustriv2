<?php

namespace App\Http\Controllers\Resources;

use App\Models\RolePermiso;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RolePermisoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = RolePermiso::with([
			'Role',
			'Permiso',
			'Convenio'
		])->select(['*']);

		$search = $request['search'];
		if(trim($search['value']) != ''){
			$searchValue = $search['value'];
		}

		if(isset($request['nombre']) && trim($request['nombre']) != ''){
			$searchValue = $request['nombre'];
		}

		if(trim($searchValue) != ''){
			$builder->whereHas('Role', function($q) use($searchValue){
				$q->where('nombre', 'like', '%'.$searchValue.'%');
			});
			$builder->orWhereHas('Permiso', function($q) use($searchValue){
				$q->where('nombre', 'like', '%'.$searchValue.'%');
			});
		}

		if(isset($request['idConvenios'])){
			if(is_array($request['idConvenios'])){
				$builder->whereIn('idConvenio', $request['idConvenios']);
			} else {
				if(trim($request['idConvenios']) != '' && $request['idConvenios'] != 0){
					$builder->where('idConvenio', $request['idConvenios']);
				}
			}
		}

		if(isset($request['idRoles'])){
			if(is_array($request['idRoles'])){
				$builder->whereIn('idRole', $request['idRoles']);
			} else {
				if(trim($request['idRoles']) != '' && $request['idRoles'] != 0){
					$builder->where('idRole', $request['idRoles']);
				}
			}
		}

		if(isset($request['idPermisos'])){
			if(is_array($request['idPermisos'])){
				$builder->whereIn('idPermiso', $request['idPermisos']);
			} else {
				if(trim($request['idPermisos']) != '' && $request['idPermisos'] != 0){
					$builder->where('idPermiso', $request['idPermisos']);
				}
			}
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('id', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function guardarRolePermiso(Request $request){
		$this->validate($request, [
			'status' => 'required|numeric',
			'idRole' => 'required|numeric',
			'idPermiso' => 'required|numeric',
			'idConvenios' => 'array'
		]);

		$status = (int)$request['status'];
		$idRole = (int)$request['idRole'];
		$idPermiso = (int)$request['idPermiso'];
		$idConvenios = $request['idConvenios'];
		$role = Role::findOrFail($idRole);

		$data = [];
		if($status == 1){
			if(is_array($idConvenios) && count($idConvenios) > 0){

				$role->Permisos()->detach([$idPermiso]);
				foreach($idConvenios as $idConvenio){
					$role->Permisos()->attach([$idPermiso => ['idConvenio' => $idConvenio]]);
				}

			} else {
				$role->Permisos()->detach([$idPermiso]);
				$role->Permisos()->attach([$idPermiso]);
			}
		} elseif($status == 0) {
			$role->Permisos()->detach([$idPermiso]);
		}

		$permisos = $role->Permisos()->orderBy('nombre')->get();
		$response = Array(
			'result' => true,
			'message' => 'Permisos actualizados.',
			'data' => $permisos->toArray()
		);

		return response()->json($response);

//		logger(print_r($role->Permisos->toArray(), true));

	}

	public function eliminarRolePermiso(Request $request){
		$this->validate($request, [
			'idRole' => 'required|numeric',
			'idPermiso' => 'required|numeric',
			'idConvenios' => 'required|array'
		]);

		$idRole = (int)$request['idRole'];
		$role = Role::findOrFail($idRole);

		if($role){

		}

	}
}
