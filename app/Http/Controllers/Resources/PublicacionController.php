<?php

namespace App\Http\Controllers\Resources;

use App\Models\Publicacion;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicacionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->validate($request, [
		    'idProducto'    => '',
			'idTipoPago'    => 'numeric',
			'idUsuario'     => 'required',
			'fechaInicio'   => 'required',
			'fechaFin'      => 'required',
			'visitas'       => 'numeric',
			'estado'        => '',
			'token'         => '',
			'dias'          => 'numeric',
			'precio'        => 'numeric',
			'moneda'        => '',
		]);

		$idUsuario = $request['idUsuario'];
		if($idUsuario <= 0){
			$response = [
				'idUsuario' => [
					'Empresa / Cliente es requerido'
				]
			];
			return response()->make($response, 422);
		}
		$idProducto = $request['idProducto'];
		if($idProducto <= 0){
			$response = [
				'idProducto' => [
					'Debe seleccionar un producto'
				]
			];
			return response()->make($response, 422);
		}

		$fechaIni = Carbon::createFromFormat('d/m/Y', $request['fechaInicio']);
		$fechaFin = Carbon::createFromFormat('d/m/Y', $request['fechaFin']);

		$request['fechaInicio'] = $fechaIni;
		$request['fechaFin'] = $fechaFin;
		$dias = $fechaFin->diffInDays( $fechaIni );
		$request['dias'] = $dias;

		if ( $dias <= 0 ) {
			$response = [
				'fechaFin' => [
					'La fecha final debe ser mayor a la inicial.'
				]
			];
			return response()->make($response, 422);
		}

		$publicacion = Publicacion::create( $request->all() );

		$usuario = Usuario::findOrFail( $request['idUsuario'] );
		$publicacion->Owner()->associate( $usuario );
		$publicacion->save();

		if ( $publicacion ) {
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $publicacion
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);

	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$this->validate($request, [
			'idProducto'    => 'numeric',
			'idTipoPago'    => '',
			'idUsuario'     => '',
			'fechaInicio'   => '',
			'fechaFin'      => '',
			'visitas'       => 'numeric',
			'estado'        => '',
			'token'         => '',
			'dias'          => 'numeric',
			'precio'        => 'numeric',
			'moneda'        => '',
		]);

		$publicacion = Publicacion::findOrFail($id);

		if( isset($request['fechaInicio']) ) {
			$fechaIni = Carbon::createFromFormat('d/m/Y', $request['fechaInicio']);
		} else {
			$fechaIni = $publicacion->fechaInicio;
		}

		if( isset($request['fechaFin']) ) {
			$fechaFin = Carbon::createFromFormat('d/m/Y', $request['fechaFin']);
		} else {
			$fechaFin = $publicacion->fechaFin;
		}

		if( isset($request['fechaInicio']) || isset($request['fechaFin']) ) {

			$dias = $fechaFin->diffInDays( $fechaIni );

			if ( $dias <= 0 ) {
				$response = [
					'fechaFin' => [
						'La fecha final debe ser mayor a la inicial.'
					]
				];
				return response()->make($response, 422);
			}

			$publicacion->fechaInicio   = $fechaIni;
			$publicacion->fechaFin      = $fechaFin;
			$publicacion->dias          = $dias;
		}

		if ( isset($request['idUsuario']) ) {
			$usuario = Usuario::findOrFail( $request['idUsuario'] );
			$publicacion->Owner()->associate( $usuario );
		}


		if( isset($request['idProducto']) )   $publicacion->idProducto    = (int) $request['idProducto'];
		if( isset($request['idTipoPago']) )   $publicacion->idTipoPago    = trim($request['idTipoPago']);
		if( isset($request['owner_type']) )   $publicacion->owner_type    = trim($request['owner_type']);
		if( isset($request['owner_id']) )     $publicacion->owner_id      = (int) $request['owner_id'];

		if( isset($request['visitas']) )      $publicacion->visitas       = (int) $request['visitas'];
		if( isset($request['estado']) )       $publicacion->estado        = trim($request['estado']);
		if( isset($request['token']) )        $publicacion->token         = trim($request['token']);
//		if( isset($request['dias']) )         $publicacion->dias          = (int) $request['dias'];
		if( isset($request['precio']) )       $publicacion->precio        = Carbon::createFromFormat('d/m/Y', $request['precio']);
		if( isset($request['moneda']) )       $publicacion->moneda        = trim($request['moneda']);
		
		$publicacion->save();
		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $publicacion
		);
		return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
	 * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {

		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Publicacion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Publicacion::select(['*']);
		$builder->with([
			'Producto.ProductoItem.Traducciones' => function( $q ){
				$q->where('campo', 'nombre');
			},
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Traduccion',
			'Producto.Categorias.Padre.Traduccion',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda'
		]);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
			$builder->where('id', $searchValue);
		}

		if( isset( $request['estados'] ) && $request['estados'] > 0 ) {
			if( !is_array($request['estados']) ) {
				logger("no es array");
				$request['estados'] = [ $request['estados'] ];
			}
			logger($request['estados']);
			$builder->whereIn('estado', $request['estados']);
		}

		if( isset($request['idMarca']) && $request['idMarca'] > 0 ){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('idMarca', $request['idMarca']);
			});
		}

		if( isset($request['idMarcas']) ){
			if( !is_array( $request['idMarcas'] ) ) {
				$request['idMarcas'] = [ $request['idMarcas'] ];
			}
			$builder->whereHas('Producto', function($q) use($request){
				$q->whereIn('idMarca', $request['idMarcas']);
			});
		}

		if( isset($request['idModelo']) && $request['idModelo'] > 0){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('idModelo', $request['idModelo']);
			});
		}

		if( isset($request['idModelos']) ){
			if( !is_array( $request['idModelos'] ) ) {
				$request['idModelos'] = [ $request['idModelos'] ];
			}
			$builder->whereHas('Producto', function($q) use($request){
				$q->whereIn('idModelo', $request['idModelos']);
			});
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('id', 'desc');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->count(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function restore(Request $request) {
        $this->validate($request, [
            'ids' => 'required|array'
        ]);
        $ids = $request['ids'];

        $count = 0;
        $publicacions = Publicacion::whereIn('id', $ids)->withTrashed()->get();
        foreach($publicacions as $publicacion) {
            if($publicacion->restore()) {
                $count++;
            }
        }

        if($count == count($ids)){
            $result = Array(
                'result' => true,
                'message' => 'Registro(s) restaurados(s) correctamente.',
                'request' => $request->all()
            );
        } else {
            $result = Array(
                'result' => false,
                'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
                'request' => $request->all()
            );
        }
        return response()->json($result);
    }

}
