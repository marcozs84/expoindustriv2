<?php

namespace App\Http\Controllers\Resources;

use App\Models\Ubicacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UbicacionController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idUbicacionTipo' => 'required',
			'titulo' => 'required',
			'descripcion' => '',
			'ubicacion' => '',
			'fechaInicio' => '',
			'fechaFin' => '',
			'texto' => '',
			'precio' => '',
			'enlace' => '',
			'estado' => 'required',
		]);

		$ban_orden = Ubicacion::where('idUbicacionTipo', (int)$request['idUbicacionTipo'])->orderBy('orden', 'desc')->first();
		if($ban_orden){
			$orden = $ban_orden->orden + 1;
		} else {
			$orden = 0;
		}

		$request['orden'] = $orden;
		$request['precio'] = (float)$request['precio'];
		$modelo = Ubicacion::create($request->all());

		if($modelo){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $modelo
			);

			return response()->json($response);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idUbicacionTipo' => '',
			'titulo' => '',
			'descripcion' => '',
			'ubicacion' => '',
			'fechaInicio' => '',
			'fechaFin' => '',
			'texto' => '',
			'precio' => '',
			'enlace' => '',
		]);

		$modelo = Ubicacion::findOrFail($id);

		if(isset($request['idUbicacionTipo'])) {
			$modelo->idUbicacionTipo = trim($request['idUbicacionTipo']);
		}

		if(isset($request['titulo'])) {
			$modelo->titulo = trim($request['titulo']);
		}

		if(isset($request['descripcion'])) {
			$modelo->descripcion = trim($request['descripcion']);
		}

		if(isset($request['ubicacion'])) {
			$modelo->ubicacion = trim($request['ubicacion']);
		}

		if(isset($request['fechaInicio'])) {
			$modelo->fechaInicio = trim($request['fechaInicio']);
		}

		if(isset($request['fechaFin'])) {
			$modelo->fechaFin = trim($request['fechaFin']);
		}

		if(isset($request['texto'])) {
			$modelo->texto = trim($request['texto']);
		}

		if(isset($request['precio'])) {
			$modelo->precio = (float) trim($request['precio']);
		}

		if(isset($request['enlace'])) {
			$modelo->enlace = trim($request['enlace']);
		}

		$modelo->save();

		if($modelo){
			$response = Array(
				'result' => true,
				'message' => 'Registro guardado.',
				'data' => $modelo
			);

			return response()->json($response);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Ubicacion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}


//		if(isset($request['filter']) && $request['filter'] == 'trashed'){
//			$builder = Ubicacion::select(['*']);
//			$builder->with([
//				'Galeria.Imagenes' => function($q) {
//					$q->withTrashed();
//				},
//				'Galeria' => function($q) {
//					$q->withTrashed();
//				},
//			]);
//		} else {
			$builder = Ubicacion::select(['*']);
//			$builder->with([
//				'Galeria.Imagenes',
//			]);
//		}

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		$builder->orderBy('created_at', 'desc');

//		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
//		}

		if(isset($request['filter'])){
			if($request['filter'] == 'trashed'){
				$builder->onlyTrashed();
			}
		}

		if(isset($request['draw'])) {	// llamada DataTable
//			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function restore(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array'
		]);
		$ids = $request['ids'];

		$count = 0;
		$ubicacions = Ubicacion::whereIn('id', $ids)->withTrashed()->get();
		foreach($ubicacions as $ubicacion) {
			if($ubicacion->restore()) {
				$count++;
			}
		}

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) restaurados(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron retaurar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

}
