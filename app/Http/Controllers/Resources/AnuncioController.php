<?php

namespace App\Http\Controllers\Resources;

use App\Models\Categoria;
use App\Models\Cliente;
use App\Models\Publicacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class AnuncioController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idProducto' => 'required',
			'idCliente' => 'required|numeric',
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
			'estado' => 'required',
			'dias' => 'required',
			'precio' => 'required',
			'moneda' => 'required',
		]);

		$modelo = Publicacion::create($request->all());

		$cliente = Cliente::find($request['idCliente']);
		$modelo->Owner()->associate($cliente);

		if($modelo){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $modelo
			);

			return response()->json($response);
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idProducto' => '',
			'idCliente' => '',
			'fechaInicio' => '',
			'fechaFin' => '',
			'estado' => '',
			'dias' => '',
			'precio' => '',
			'moneda' => '',
		]);

		$modelo = Publicacion::findOrFail($id);

		if(isset($request['idProducto'])){
			$modelo->idProducto = trim($request['idProducto']);
		}
		if(isset($request['idCliente'])){
			$cliente = Cliente::find($request['idCliente']);
			$modelo->Owner()->associate($cliente);
		}
		if(isset($request['fechaInicio'])){
			$modelo->fechaInicio = trim($request['fechaInicio']);
		}
		if(isset($request['fechaFin'])){
			$modelo->fechaFin = trim($request['fechaFin']);
		}
		if(isset($request['estado'])){
			$modelo->estado = trim($request['estado']);
		}
		if(isset($request['dias'])){
			$modelo->dias = trim($request['dias']);
		}
		if(isset($request['precio'])){
			$modelo->precio = trim($request['precio']);
		}
		if(isset($request['moneda'])){
			$modelo->moneda = trim($request['moneda']);
		}

		$modelo->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $modelo
		);

		return response()->json($response);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Publicacion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron eliminar el/los registro(s). Eliminados: '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	// ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$builder = Publicacion::select(['*']);
		$builder->with([
			'Producto.ProductoItem.Ubicacion',
			'Producto.Traducciones' => function( $q ){
				$q->where('campo', 'nombre');
			},
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Traduccion',
			'Producto.Categorias.Padre.Traduccion',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda',
			'Owner.Owner.Proveedor.Galerias.Imagenes'
		]);
		$builder->has('Producto');

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		$title = '';
		$message = '';
		$glc = App::getLocale();

		if( isset( $request['estados'] ) && $request['estados'] > 0 ) {
			if( !is_array($request['estados']) ) {
				$request['estados'] = [$request['estados']];
			}
			$builder->whereIn('estado', $request['estados']);
		}

		if( isset($request['idProductoTipo']) && $request['idProductoTipo'] > 0 ){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('idProductoTipo', (int)$request['idProductoTipo']);
			});
		}

		if( isset($request['idPublicacionEstado']) && $request['idPublicacionEstado'] > 0 ){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('idPublicacionEstado', (int)$request['idPublicacionEstado']);
			});
		}

		if( isset($request['esNuevo']) && $request['esNuevo'] > 0 ){
			$builder->whereHas('Producto', function($q) use($request){
				// IF != 1 THEN SET TO 0 "NOT NEW"
				$esNuevo = ( (int)$request['esNuevo'] == 1 ) ? 1 : 0;
				$q->where('esNuevo', $esNuevo);
			});
		}

		if(isset($request['idCategoria']) && $request['idCategoria'] > 0){
			$title = Categoria::with('Traduccion')->find( $request['idCategoria'])->Traduccion->$glc;
			$message = __('categories.categoria_' . $request['idCategoria']);
			$builder->whereHas('Producto.Categorias.Padre', function($q) use($request){
				$q->where('id', $request['idCategoria']);
			});
		}

		if(isset($request['idSubCategoria']) && $request['idSubCategoria'] > 0){
			$title = Categoria::with('Traduccion')->find( $request['idSubCategoria'])->Traduccion->$glc;
			$message = __('categories.categoria_' . $request['idSubCategoria']);
			$builder->whereHas('Producto.Categorias', function($q) use($request){
				$q->where('id', $request['idSubCategoria']);
			});
		}

		if( isset($request['idMarca']) && $request['idMarca'] > 0 ){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('idMarca', $request['idMarca']);
			});
		}

		if( isset($request['idMarcas']) ){
			if( !is_array( $request['idMarcas'] ) ) {
				$request['idMarcas'] = [ $request['idMarcas'] ];
			}
			$builder->whereHas('Producto', function($q) use($request){
				$q->whereIn('idMarca', $request['idMarcas']);
			});
		}

		if( isset($request['idModelo']) && $request['idModelo'] > 0){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('idModelo', $request['idModelo']);
			});
		}

		if( isset($request['idModelos']) ){
			if( !is_array( $request['idModelos'] ) ) {
				$request['idModelos'] = [ $request['idModelos'] ];
			}
			$builder->whereHas('Producto', function($q) use($request){
				$q->whereIn('idModelo', $request['idModelos']);
			});
		}

		if(isset($request['precioFrom']) && $request['precioFrom'] > 0){
			$builder->whereHas('Producto', function($q) use($request){
//				$q->where('precioUnitario', '>=', $request['precioFrom']);
				$q->where('precioProveedor', '>=', $request['precioFrom']);
			});
		}

		if(isset($request['precioTo']) && $request['precioTo'] > 0){
			$builder->whereHas('Producto', function($q) use($request){
//				$q->where('precioUnitario', '<=', $request['precioTo']);
				$q->where('precioProveedor', '<=', $request['precioTo']);
			});
		}

		// TODO: mejorar la busqueda por precio considerando la moneda de cada producto y si el precio es fijo.
		if(isset($request['anioFrom']) && $request['anioFrom'] > 0){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('anio', '>=', $request['anioFrom']);
			});
		}

		if(isset($request['anioTo']) && $request['anioTo'] > 0){
			$builder->whereHas('Producto', function($q) use($request){
				$q->where('anio', '<=', $request['anioTo']);
			});
		}

		if(isset($request['tags']) && is_array($request['tags']) && count($request['tags']) > 0){

			$concat_ids = implode(',', $request['tags']);

			$prod_ids = DB::select( DB::raw("select idProducto from ProductoTag WHERE idTag in ( {$concat_ids} )") );
			$prod_ids = array_pluck($prod_ids, 'idProducto');

			if( count($prod_ids) > 0 ) {
				$builder->whereHas('Producto', function($q) use($prod_ids){
					$q->whereIn('id', $prod_ids);
				});
			}
		}

		if(isset($request['idTipoImportacion']) && $request['idTipoImportacion'] > 0){
			$builder->whereHas('Producto.ProductoItem', function($q) use($request){
				$q->where('idTipoImportacion', (int)$request['idTipoImportacion']);
			});
		}

		if(isset($request['filtro'])){
			if($request['filtro'] == 'aprobados'){
				$builder->where('estado', Publicacion::ESTADO_APPROVED);
			}
		}

		if($searchValue != ''){
//			$builder->where('nombre', 'like', '%'.$searchValue.'%');
			$builder->where('id', $searchValue);
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('created_at', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			$builder->orderBy('created_at', 'DESC');
			$response = $builder->paginate($request['length'], ['*']);
			$custom = collect(['result' => true]);
			$response = $custom->merge($response);
			return $response;
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'title' => $title,				//	 <- CARACTERÍSTICA ESPECIAL PARA BUSCADOR
			'message' => $message,
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
