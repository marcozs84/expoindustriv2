<?php

namespace App\Http\Controllers\Frontend;

use App\Models\ConsultaExterna;
use App\Models\Galeria;
use App\Models\Imagen;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Noticia;
use App\Models\Proveedor;
use App\Models\ProveedorContacto;
use App\Models\Publicacion;
use App\Models\Subscripcion;
use App\Models\Ubicacion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class PrivateController extends Controller {
	public function __construct(){
		$this->middleware('auth:clientes');
	}

	public function privCuenta() {

		$usuario = Auth::guard('clientes')->user();
		$provContacto = ProveedorContacto::where('idUsuario', $usuario->id)->first();

		$esProveedor = false;
		if($provContacto){
			$esProveedor = true;
		}

		// ---------- SUBSCRIPCIONES

		$subscripciones = Subscripcion::where('idUsuario', $usuario->id)
			->where('fechaDesde', '<', Carbon::today())
			->where('fechaHasta', '>', Carbon::today())
			->get();
		$subscripcionesInactive = Subscripcion::where('idUsuario', $usuario->id)
			->where('fechaHasta', '<', Carbon::today())
			->get();

		$prov = $provContacto->Proveedor;
		if( $subscripciones->count() > 0 ) {
			if( $prov->tieneStore != 1) {
				$prov->tieneStore = 1;
				$prov->save();
			}
		} else {
			if ( $prov->tieneStore != 0 ) {
				$prov->tieneStore = 0;
				$prov->save();
			}
		}

		$masVisitados = Publicacion::with([
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Galerias',
		])
			->has('Producto')
			->where('owner_id', $usuario->id)
			->orderBy('visitas', 'desc')
			->limit(5)->get();

		// ----------

		$noticias = Noticia::all();

//		logger($subscripciones);

		return view('frontend.pages.privCuenta', [
			'esProveedor' => $esProveedor,
			'subscripciones' => $subscripciones,
			'subscripcionesInactive' => $subscripcionesInactive,
			'noticias' => $noticias,
			'masVisitados' => $masVisitados,
		]);
	}

//	public function privPublicaciones(){
//		return view('frontend.pages.private.publicaciones');
//	}

	public function privOrdenes() {
		return view('frontend.pages.privOrdenes');
	}

	public function privOrdenEstado() {
		return view('frontend.pages.privOrdenEstado');
	}

	public function privFavoritos() {
		$estados = ListaItem::where('idLista', Lista::PUBLICACION_ESTADO)->get();
		$estadosMap = $estados->map(function($item, $key){
			return $item->texto = __('messages.'.$item->texto);
		});
		return view('frontend.pages.privFavoritos', [
			'estados' => $estadosMap->pluck('texto', 'valor')->toArray()
		]);
	}

	public function privConsultas() {
		return view('frontend.pages.privConsultas');
	}

	public function privConsultaDetalle() {
		return view('frontend.pages.privConsultaDetalle');
	}

	public function changePassword(Request $request){
		if($request->method() == 'POST'){
			$this->validate($request, [
				'password_current' => 'required',
				'password' => 'required|confirmed',
				'password_confirmation' => 'required'
			]);

			$usuario = Auth::guard('clientes')->user();
			if(!Hash::check(trim($request['password_current']), $usuario->password)){
				$response = [
					'Password' => [
						__('messages.old_password_different')
					]
				];
				return response()->make($response, 422);
//				return redirect()->back()->withErrors($response);
			} else {
				$usuario->password = bcrypt(trim($request['password']));
				$usuario->save();

				$response = Array(
					'result' => true,
					'message' => __('messages.password_updated_succesfully'),
					'data' => []
				);

				return response()->json($response);
			}
		}

		return view('frontend.pages.privContrasenia', []);
	}

	public function privCreditCards(Request $request){
		return view('frontend.pages.privCreditCards', []);
	}
	
	public function providerProfileEdit(Request $request){

		$usuario = Auth::guard('clientes')->user();
		$provContacto = ProveedorContacto::where('idUsuario', $usuario->id)->first();
		$agenda = $usuario->Owner->Agenda;
		$galeria = null;

		$companyName = '';
		$companyNit = '';
		$companyPhone = '';
		$logoUrl = '';
		$companyDesc = '';
		$urlSitio = '';
		$urlFacebook = '';
		$urlTwitter = '';
		$urlGooglePlus = '';
		$imagen = null;
		$prov = null;
		$ubicacionPrimaria = new Ubicacion();
		$ubicacionesSecundarias = [];

		if($provContacto){
			$prov = $provContacto->Proveedor;
			$galeria = $prov->Galerias->first();
			if(!$galeria){
				$galeria = $prov->Galerias()->save(new Galeria());
			}
			if($galeria->Imagenes->count() > 1) {
				$mensajeDev = "Se encontró mas de un logo en la galeria del Proveedor: {$prov->nombre} ({$prov->id})<br>";
				foreach($galeria->Imagenes as $imagen) {
					$mensajeDev .= $imagen->ruta." -> ".$imagen->filename."<br>";
				}
				$mensajeDev .= __FILE__.":".__LINE__;

				$this->reportDev($mensajeDev);
			}

			if($galeria->Imagenes->count() > 0){
				$imagen = $galeria->Imagenes->first();
				$logoUrl = '/company_logo/'.$imagen->filename;
			}
			$companyName = $prov->nombre;
			$companyNit = $prov->nit;
			$companyDesc = $prov->descripcion;

			// ---------- TRADUCCION DESCRIPCION
			$lang = App::getLocale();
			$traducciones = $prov->Traducciones;
			if( $traducciones && $traducciones->count() > 0 ) {
				if( $traducciones->where('campo', 'descripcion')->count() > 0 ) {
					$traduccion = $traducciones->where('campo', 'descripcion')->first();
					$companyDesc = $traduccion->$lang;
				}
			}
			// ----------

			if($prov->Telefonos->count() == 0){
				if($usuario->Owner->Agenda->Telefonos->count() > 0){
					$prov->Telefonos()->create([
						'numero' => $agenda->Telefonos->first()->numero,
						'descripcion' => $agenda->Telefonos->first()->descripcion,
						'estado' => $agenda->Telefonos->first()->estado,
					]);
				}
			}

			$companyPhone = $prov->Telefonos->first()->numero;

			$urlSitio = $prov->urlSitio;
			$urlFacebook = $prov->urlFacebook;
			$urlTwitter = $prov->urlTwitter;
			$urlGooglePlus = $prov->urlGooglePlus;

			$ups = $prov->Ubicaciones->where('idUbicacionTipo', 1)->first();
			if($ups) {
				$ubicacionPrimaria = $ups;
			}

			$upsec = $prov->Ubicaciones->where('idUbicacionTipo', 2);
			if($upsec) {
				$ubicacionesSecundarias = $upsec;
			}
		}

		return view('frontend.pages.privProviderProfileEdit', [
			'images' => [],
			'prov' => $prov,
			'companyName' => $companyName,
			'companyNit' => $companyNit,
			'companyPhone' => $companyPhone,
			'imagen' => $imagen,
			'logoUrl' => $logoUrl,
			'companyDesc' => $companyDesc,
			'urlSitio' => $urlSitio,
			'urlFacebook' => $urlFacebook,
			'urlTwitter' => $urlTwitter,
			'urlGooglePlus' => $urlGooglePlus,
			'ubicacionPrimaria' => $ubicacionPrimaria,
			'ubicacionesSecundarias' => $ubicacionesSecundarias,
		]);
	}

	public function providerProfileEditSave(Request $request){

		$this->validate($request, [
			'companyName' => 'required',
			'companyNit' => 'required',
			'companyLogo' => '',
			'companyBackground' => '',
			'companyDesc' => '',
			'direcciones' => ''
		]);

		$usuario = Auth::guard('clientes')->user();
		$provContacto = ProveedorContacto::where('idUsuario', $usuario->id)->first();
		$galeria = null;

		//TODO: definir tipos de galerias para Proveedores: Logo, Otras imagenes.  Luego jalar la galeria que corresponde para el logo.

		$companyName = '';
		if($provContacto){
			logger("Con empresa, actualizando datos");
			$prov = $provContacto->Proveedor;
//			$galeria = $prov->Galerias()->save(new Galeria());
			$galeria = $prov->Galerias->first();
			$prov->nombre = $request['companyName'];
			$prov->nit = $request['companyNit'];
			$prov->descripcion = $request['companyDesc'];

			$prov->urlSitio = $request['urlSitio'];
			$prov->urlFacebook = $request['urlFacebook'];
			$prov->urlTwitter = $request['urlTwitter'];
			$prov->urlGooglePlus = $request['urlGooglePlus'];

			$galeria = $prov->Galerias->first();
			$imagen = $galeria->Imagenes->first();
			if($imagen) {
				$imagen->backgroundColor = $request['companyBackground'];
				$imagen->save();
			}

			$prov->save();
		} else {
			logger("Sin empresa, creando proveedor, galerias y contacto");
			$prov = new Proveedor();
			$prov->nombre = $request['companyName'];
			$prov->nit = $request['companyNit'];
			$prov->descripcion = $request['companyDesc'];

			$prov->urlSitio = $request['urlSitio'];
			$prov->urlFacebook = $request['urlFacebook'];
			$prov->urlTwitter = $request['urlTwitter'];
			$prov->urlGooglePlus = $request['urlGooglePlus'];

			$prov->save();

			$galeria = $prov->Galerias()->save(new Galeria());

			$agenda = $usuario->Owner->Agenda;

			$pc = new ProveedorContacto();
			$pc->idProveedor = $prov->id;
			$pc->idUsuario = $usuario->id;
			$pc->idAgenda = $agenda->id;
			$pc->idProveedorContactoEstado = 1;
			$pc->idProveedorContactoTipo = 0;
			$pc->save();
		}

		$ups = $prov->Ubicaciones->where('idUbicacionTipo', 1);

		if($ups->count() > 0) {
			if($ups->count() > 1) {
				$this->reportDev('Multiples direcciones primarias para Proveedor: '.print_r($prov, true).'<br>'.__FILE__.':'.__LINE__);

			}

			$up = $ups->first();

			$up->direccion = $request['direccion_primaria'];
			$up->latitud = $request['latitud'];
			$up->longitud = $request['longitud'];
			$up->save();
		} else {
			$prov->Ubicaciones()->save(new Ubicacion([
				'direccion' => $request['direccion_primaria'],
				'latitud' => $request['latitud'],
				'longitud' => $request['longitud'],
				'idUbicacionTipo' => 1,
			]));
		}

		if(isset($request['direcciones'])) {
			$newUbicaciones = $request['direcciones'];

			for($i = 0 ; $i < count($request['direcciones']) ; $i++) {
				$prov->Ubicaciones()->save(new Ubicacion([
					'descripcion' => $request['dir_nombre'][$i],
					'direccion' => $request['direcciones'][$i],
					'idUbicacionTipo' => 2,
				]));
			}

//			foreach($newUbicaciones as $ubicacion) {
//				$prov->Ubicaciones()->save(new Ubicacion([
//					'direccion' => $ubicacion,
//					'idUbicacionTipo' => 2,
//				]));
//			}
		}


		// ---------------- START UPLOAD

		$errors = [];

		if($request->hasFile('companyLogo')){

			logger("has a file");

			$file = $request->file('companyLogo');
			$files = [$file];
			foreach($files as $file){
				$filename_original = $file->getClientOriginalName();
				logger($filename_original);
				$extension_original = $file->getClientOriginalExtension();
				logger($extension_original);
				$mimetype = $file->getMimeType();
				logger($mimetype);
				$extension = $file->extension();
				logger($extension);

				if(!in_array($file->getMimeType(), ['image/jpeg', 'image/gif', 'image/png'])){
//					$response = __('messages.invalid_image_file');
//					return response()->make($response, 422);
					$errors['companyLogo'] = [__('messages.invalid_image_file')];
				}

				if(!$file->isValid()){
//					$response = __('messages.invalid_file');
//					return response()->make($response, 422);
					$errors['companyLogo'] = [__('messages.invalid_file')];
				}

				$relativePath = "app".DIRECTORY_SEPARATOR."company_logo";
				$destinationPath = storage_path($relativePath);

				logger('$destinationPath: '.$destinationPath);

				if(!is_dir($destinationPath)){
					//$mkdir = mkdir($destinationPath, 0755);
					$mkdir = File::makeDirectory($destinationPath, 0755, true);
				}

				$randChars = $this->randomChars(5);
				$filename = "logo_{$randChars}.{$extension}";

				$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
				if(File::exists($fullpath)){
					$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
					File::delete($fullpath);
				}

				$file->move($destinationPath, $filename);

				if(!File::exists($fullpath)){
//					$response = 'Archivo no encontrado despues de subir.';
//					return response()->make($response, 422);
					$errors['companyLogo'] = ['Archivo no encontrado despues de subir.'];
				}

				if($galeria->Imagenes->count() > 1) {
					$mensajeDev = "Se encontró mas de un logo en la galeria del Proveedor: {$prov->nombre} ({$prov->id})<br>";
					foreach($galeria->Imagenes as $imagen) {
						$mensajeDev .= $imagen->ruta." -> ".$imagen->filename."<br>";
					}
					$mensajeDev .= __FILE__.":".__LINE__;

					$this->reportDev($mensajeDev);
				}

				if($galeria->Imagenes->count() > 0){
					$imagen = $galeria->Imagenes->first();
					$imagen->ruta = $relativePath.DIRECTORY_SEPARATOR.$filename;
					$imagen->filename = $filename;
					$imagen->mimetype = $mimetype;
					$imagen->backgroundColor = $request['companyBackground'];
					$imagen->save();
				} else {
					logger("CREANDO nueva imagen");
					$imagen = Imagen::create([
						'idGaleria' => $galeria->id,
//				'ruta' => $imagen['ruta'],
						'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
						'filename' => $filename,
						'backgroundColor' => (trim($request['companyBackground']) == '') ? trim($request['companyBackground']) : '#ffffff' ,
						'mimetype' => $mimetype,
					]);
				}

//				logger($imagen);

			}

			$jsonArchivos = [];

//			$response = 'File uploaded correctly';
//			return response()->json($response);

		} else {
			logger("DOES NOT HAVE A FILE");
////			$response = 'There is no file';
////			return response()->make($response, 422);
//			$errors['companyLogo'] = ['There is no file'];

		}

		// ---------------- END UPLOAD

		return redirect()->route('private.providerProfileEdit')->withErrors($errors);

//		return view('frontend.pages.privProviderProfileEdit', [
//			'images' => []
//		]);
	}

	public function randomChars($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function requestRenewal(Request $request) {
		$this->validate($request, [
			'idSubscripcion' => 'required',
		]);

		$subscripcion = Subscripcion::with([
			'Tipo'
		])->findOrFail($request['idSubscripcion']);

		$ce = new ConsultaExterna();
		$ce->idTipoConsulta = ConsultaExterna::TIPO_SUBSCRIPCION_RENEWAL;
		$ar = [];
		$ar['idSub'] = (int)$request['idSubscripcion'];
		$ce->extrainfo = $ar;

		$response = Array(
			'result' => true,
			'message' => 'Listo.',
			'data' => $subscripcion->toArray()
		);

		return response()->json($response);
	}

}
