<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Calculadora;
use App\Models\Categoria;
use App\Models\CategoriaDefinition;
use App\Models\Configuracion;
use App\Models\CotizacionEsquema;
use App\Models\Favorito;
use App\Models\Imagen;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Producto;
use App\Models\Publicacion;
use App\Models\Subscripcion;
use App\Models\Traduccion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class AnnouncementsController extends Controller {

	public function __construct(){
		$this->middleware('auth:clientes');
	}

	public function index(){
		$estados = ListaItem::where('idLista', Lista::PUBLICACION_ESTADO)->get();
		$estadosMap = $estados->mapWithKeys(function($item, $key){
//			return $item->texto = __('messages.'.$item->texto);
			return [$item->valor => __('messages.'.$item->texto)];
		});

		// ---------- SUBSCRIPCIONES
		$user = Auth::guard('clientes')->user();
		$subs = Subscripcion::where('idUsuario', $user->id)
			->where('fechaDesde', '<', Carbon::today())
			->where('fechaHasta', '>', Carbon::today())
			->get();

		$puedeCrear = false;
		if($subs->count() > 0){
			$puedeCrear = true;
		}

		return view('frontend.pages.private.announcements.index', [
			'estados' => $estadosMap->toArray(),
			'puedeCrear' => $puedeCrear,
		]);
	}

	public function index_removed(){
		$estados = ListaItem::where('idLista', Lista::PUBLICACION_ESTADO)->get();
		$estadosMap = $estados->mapWithKeys(function($item, $key){
//			return $item->texto = __('messages.'.$item->texto);
			return [$item->valor => __('messages.'.$item->texto)];
		});

		// ---------- SUBSCRIPCIONES
		$user = Auth::guard('clientes')->user();
		$subs = Subscripcion::where('idUsuario', $user->id)
			->where('fechaDesde', '<', Carbon::today())
			->where('fechaHasta', '>', Carbon::today())
			->get();

		$puedeCrear = false;
		if($subs->count() > 0){
			$puedeCrear = true;
		}

		return view('frontend.pages.private.announcements.index_removed', [
			'estados' => $estadosMap->toArray(),
			'puedeCrear' => $puedeCrear,
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		$anuncio = Publicacion::with([
			'Producto',
			'Producto.ProductoItem',
		])->findOrFail($id);

		if($anuncio->estado != Publicacion::ESTADO_APPROVED){
			$glc = App::getLocale();
			return redirect()->route("public.{$glc}.search");
		}

		$producto = $anuncio->Producto;

		// ---------- CATEGORIAS

		$categorias = Categoria::with([
//			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
//			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		return view('frontend.pages.private.announcements.show', [
			'pub' => $anuncio,
			'categorias' => $categorias,
		]);

	}

	public function preview($id = 0) {
		$ceId = 0;

		$pub = Publicacion::with([
			'Producto.ProductoItem.Traducciones',
			'Producto.Categorias.Traduccion',
			'Producto.Categorias.Padre.Traduccion',
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Padre',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda'
		])
			->findOrFail($id);

		$esquema = null;

		if(!$pub){

			$subcat = $pub->Producto->Categorias()->first();
			$producto = $pub->Producto;
			$cat = $subcat->Padre;

			switch($cat->id){
				case Categoria::INDUSTRIAL:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::TRANSPORTE:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::EQUIPO:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::AGRICOLA:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::MONTACARGA:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::OTROS:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				default:
					$esquema = null;
			}

			$ce = CotizacionEsquema::find(1);
		}


		// ----------

		$campos = $primaryFields = [];

		if( $pub->Producto->Categorias->count() > 0 ) {
			$idCategoria = $pub->Producto->Categorias[0]->id;
			$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
			if($cd->count() == 0){
				$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
			}
			if($cd->count() > 1){
				$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
			}
			$campos = $cd->first()->campos;

			$primaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
			});
		}


		$lang = App::getLocale(); //session('lang', 'en');

		$primaryData = [];
		$secondaryData = [];

		if( $pub->Producto->ProductoItem->idTipoImportacion == 1 ) {
		} else {

			$def = $pub->Producto->ProductoItem->definicion;

			if($def == null){
				$this->reportDev("Hay una publicacion SIN DEFINICION!: {$pub->id}");
			}

			$primaryKeys = array_keys($def);

			// ----------

			foreach($primaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
							if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = $def[$value['key']];
								}
							} else {
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$primaryData[] = $value;
				}
			}

			$secondaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
			});

//		$secondaryData = [];
//		foreach($secondaryFields as $value){
//			if(in_array($value['key'], $primaryKeys)){
//				$value['value'] = $def[$value['key']];
//				$secondaryData[] = $value;
//			}
//		}

			foreach($secondaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
							if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = $def[$value['key']];
								}
							} else {
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$secondaryData[] = $value;
				}
			}
		}

		// ---- calculadora

		$calc = $pub->Producto->ProductoItem->Calculadora;
		if(!$calc){
			$calc = new Calculadora();
			$calc->informe_tecnico = 0;
			$calc->seguro_extra = 0;
			$calc->contenedor_compartido = 0;
			$calc->contenedor_exclusivo = 0;
			$calc->envio_abierto = 0;
			$calc->destino_1 = 0;
			$calc->destino_2 = 0;
			$calc->destino_3 = 0;
			$calc->destino_4 = 0;
			$calc->destino_5 = 0;
			$calc->destino_6 = 0;
			$pub->Producto->ProductoItem->Calculadora = $calc;
		}

		$nombre = $resumen = $descripcion = '';

		// ----------
		$galeria = $pub->Owner->Owner->Proveedor->Galerias->first();


		$imagen = null;
		$logoUrl = null;
		$tieneImagen = false;
		if($galeria->Imagenes->count() > 0){
			$imagen = $galeria->Imagenes->first();
			if($imagen) {
				$tieneImagen = true;
			}
			$logoUrl = '/company_logo/'.$imagen->filename;
		}
		// ----------

		if( $pub->Producto->ProductoItem->idTipoImportacion == 1 ) {
			$view = 'frontend.pages.productoPreview_import';

			$traducciones = $pub->Producto->Traducciones;
			$nombre_ds = $traducciones->where('campo', 'nombre')->first();
			if( $nombre_ds ) {
				$nombre = $nombre_ds->$lang;
			}
			$resumen_ds = $traducciones->where('campo', 'resumen')->first();
			if( $resumen_ds ) {
				$resumen = $resumen_ds->$lang;
			}
			$descripcion_ds = $traducciones->where('campo', 'descripcion')->first();
			if( $descripcion_ds ) {
				$descripcion = $descripcion_ds->$lang;
			}
		} else {
			$view = 'frontend.pages.productoPreview';
		}

		return view($view, [
			'id' => $id,
			'esquema' => $esquema,
			'pub' => $pub,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,

			'logoUrl' => $logoUrl,
			'tieneImagen' => $tieneImagen,
			'nombre' => $nombre,
			'resumen' => $resumen,
			'descripcion' => $descripcion,
		]);
	}

	public function edit( $id ) {


		$anuncio = Publicacion::withTrashed()->find($id);

		$producto = Producto::with([
			'ProductoItem.Ubicacion',
			'Categorias.Padre',
			'Marca',
			'Modelo',
			'Publicacion',
			'Galerias',
			'Owner.Owner.Agenda',
		])->findOrFail($anuncio->idProducto);

		$marcas = Marca::all()->pluck('nombre');

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		// ---------- CURRENCY

		$currency = ListaItem::where('idLista', Lista::MONEDA)->get();
		$moneda = $currency->pluck('texto', 'valor')->toArray();
		foreach($moneda as $k => $v){
			$moneda[$k] = __('messages.'.$v);
		}

		// ---------- FORM DEFINITION

//		$lang = 'es';
		$lang = App::getLocale();

		$def = $producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		$campos = [];

		if( $producto->ProductoItem->idTipoImportacion == 1 ) {
			$view = 'frontend.pages.private.announcements.edit_import';
		} else {
			$idCategoria = $producto->Categorias[0]->id;
			$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
			if($cd->count() == 0){
				$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
			}
			if($cd->count() > 1){
				$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
			}
			$campos = $cd->first()->campos;

			$view = 'frontend.pages.private.announcements.edit';
		}

		if( $producto->Categorias->count() > 0 ) {
			$subcats_list = $categorias->where('id', $producto->Categorias[0]->idPadre)->first()->Subcategorias->pluck('traduccion.'.App::getLocale(), 'id');
		} else {
			$subcats_list = [];
		}


		return view($view, [
			'producto' => $producto,
			'marcas' => $marcas,
			'categorias' => $categorias,
			'subcats_list' => $subcats_list,
			'currency' => $currency,
			'moneda' => $moneda,
			'campos' => $campos,
			'lang' => $lang,
		]);
	}

	public function editUpload(Request $request, $idProducto) {

		$this->validate($request, [
			'idProducto' => 'required',
		]);

//		$idProducto = (int) $request['idProducto'];

		if($idProducto <= 0 ) {
			$response = [
				'idProducto' => [
					"Not a valid product ID = {$idProducto}"
				]
			];
			return response()->make($response, 422);
		}

		$prod = Producto::findOrFail($idProducto);
		$galeria = $prod->Galerias->first();


		if($request->hasFile('file')){

			$file = $request->file('file');
//			$files = [$file];
//			foreach($files as $file){
				$filename_original = $file->getClientOriginalName();
				logger($filename_original);
				$extension_original = $file->getClientOriginalExtension();
				logger($extension_original);
				$mimetype = $file->getMimeType();
				logger($mimetype);
				$extension = $file->extension();
				logger($extension);

				if(!in_array($file->getMimeType(), ['image/jpeg', 'image/gif', 'image/png'])){
					$response = __('messages.invalid_image_file');
					return response()->make($response, 422);
				}

				if(!$file->isValid()){
					$response = __('messages.invalid_file');
					return response()->make($response, 422);
				}

				$relativePath = "app".DIRECTORY_SEPARATOR."published".DIRECTORY_SEPARATOR.$prod->id;
				$destinationPath = storage_path($relativePath);

				if(!is_dir($destinationPath)){
					//$mkdir = mkdir($destinationPath, 0755);
					$mkdir = File::makeDirectory($destinationPath, 0755, true);
				}

				$randChars = $this->randomChars(5);
				$filename = "img_{$randChars}.{$extension}";

				$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
				if(File::exists($fullpath)){
					$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
					File::delete($fullpath);
				}

				$file->move($destinationPath, $filename);

				if(!File::exists($fullpath)){

					$response = 'File not found.'; //'Archivo no encontrado despues de subir.';
					return response()->make($response, 422);
				}

				// ----------

				$imagen = Imagen::create([
					'idGaleria' => $galeria->id,
//				'ruta' => $imagen['ruta'],
					'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
					'filename' => $filename,
					'mimetype' => $mimetype,
					'estado' => 1,
				]);

				// ----------

//				$session = session('pubsess');
//				$session['imagenes'][] = [
//					'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
//					'filename_orig' => $filename_original,
//					'filename' => $filename,
//					'mimetype' => $mimetype
//				];
//				session(['pubsess' => $session]);

//			}

			$jsonArchivos = [];

//			$response = 'File uploaded correctly';
			$response = Array(
				'result' => true,
				'message' => 'File uploaded correctly',
				'data' => $imagen
			);
			return response()->json($response);

		} else {
			$response = 'There is no file';
			return response()->make($response, 422);

		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$validationRules = [
			'marca' => '',
			'modelo' => '',
			'esNuevo' => '',
			'idProductoTipo' => '',
			'idDisponibilidad' => '',
//			'idCategoria' => 'required',   // Cannot be changed when editing.

			'idUsuario' => 'required',
//			'precio' => 'required',
//			'currency' => 'required',
//			'precioVenta' => '',
//			'currencyVenta' => '',
//			'precioMinimo' => '',
//			'currencyMinimo' => '',
//			'priceSouthamerica' => '',

			'pais' => 'required',
			'ciudad' => '',
			'codigo_postal' => '',
			'direccion' => '',

//			'longitud' => '',
//			'ancho' => '',
//			'alto' => '',
//			'peso_bruto_fix' => '',
//			'paletizado' => '',
		];

//		if( Gate::allows('admin.producto.editPrecio')){
			$validationRules['precio'] = 'required';
			$validationRules['currency'] = 'required';
			$validationRules['precioVenta'] = '';
			$validationRules['currencyVenta'] = '';
			$validationRules['precioMinimo'] = '';
			$validationRules['currencyMinimo'] = '';
			$validationRules['priceSouthamerica'] = '';
//		}

		$this->validate($request, $validationRules);

		$prod = Producto::findOrFail($id);

		$idCategoria = $prod->Categorias->first()->id;
		$cds = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cds->count() > 1){
			$this->reportDev('Hay mas de un CategoriaDefinition para la categoria: '.$idCategoria);
		}

		$campos = [];

		if( $prod->ProductoItem->idTipoImportacion == 1 ) {
//			$prod->Categorias()->attach((int)$request['subcategoria']);
//			if( isset($request['subcategoria']) && $request['subcategoria'] > 0 ) {
//				$prod->Categorias()->sync([(int)$request['subcategoria']]);
//			}

			$traducciones = $prod->Traducciones;
			$traduccion = $traducciones->where('campo', 'nombre')->first();
			if(!$traduccion){
				$traduccion = new Traduccion();
				$traduccion->campo = 'nombre';
//				$traduccion->en = trim($request['nombre_en']);
//				$traduccion->es = trim($request['nombre_es']);
				$traduccion->se = trim($request['nombre_se']);
				$prod->Traducciones()->save($traduccion);
			} else {
//				$traduccion->en = trim($request['nombre_en']);
//				$traduccion->es = trim($request['nombre_es']);
				$traduccion->se = trim($request['nombre_se']);
				$traduccion->save();
			}
			$traduccion = $traducciones->where('campo', 'resumen')->first();
			if(!$traduccion){
				$traduccion = new Traduccion();
				$traduccion->campo = 'resumen';
//				$traduccion->en = trim($request['resumen_en']);
//				$traduccion->es = trim($request['resumen_es']);
				$traduccion->se = trim($request['resumen_se']);
				$prod->Traducciones()->save($traduccion);
			} else {
//				$traduccion->en = trim($request['resumen_en']);
//				$traduccion->es = trim($request['resumen_es']);
				$traduccion->se = trim($request['resumen_se']);
				$traduccion->save();
			}
			$traduccion = $traducciones->where('campo', 'descripcion')->first();
			if(!$traduccion){
				$traduccion = new Traduccion();
				$traduccion->campo = 'descripcion';
//				$traduccion->en = trim($request['descripcion_en']);
//				$traduccion->es = trim($request['descripcion_es']);
				$traduccion->se = trim($request['descripcion_se']);
				$prod->Traducciones()->save($traduccion);
			} else {
//				$traduccion->en = trim($request['descripcion_en']);
//				$traduccion->es = trim($request['descripcion_es']);
				$traduccion->se = trim($request['descripcion_se']);
				$traduccion->save();
			}
		} else {

			$cd = $cds->first();

			$campos = $cd->campos;

			$validations = [];
			foreach($campos as $campo){
				if(isset($campo['validation'])){
					$validations[$campo['key']] = $campo['validation'];
				}
			}
			$this->validate($request, $validations);

		}



		// ---------- MARCA Y MODELO

		$marcas = Marca::where('nombre', trim($request['marca']))->get();
		if($marcas->count() > 1){
			$marca = $marcas->first();
			$this->reportDev('Se encontró mas de una marca coincidente con: '. $request['marca']);
		} elseif ($marcas->count() == 1){
			$marca = $marcas->first();
		} else {
			$marca = Marca::create(['nombre' => trim($request['marca'])]);
		}

		$idMarca = $marca->id;

		// ---------- MODELO

		$modelos = Modelo::where('idMarca', $idMarca)
			->where('nombre', trim($request['modelo']))
			->get();
		if($modelos->count() > 1){
			$modelo = $modelos->first();
			$this->reportDev('Se encontró mas de un modelo coincidente con: '. $request['modelo']. ' en la marcaID: '.$idMarca);
		} elseif ($modelos->count() == 1){
			$modelo = $modelos->first();
		} else {
			$modelo = Modelo::create(['idMarca' => $idMarca, 'nombre' => trim($request['modelo'])]);
		}

		$idModelo = $modelo->id;

		// ---------- USUARIO

//		$usuario = Usuario::findOrFail($request['idUsuario']);

		// ---------- PRODUCTO + PRODUCTO_ITEM

//		$prod = Producto::findOrFail($id);  // Moved to the very beginning
		$prod->idMarca = $idMarca;
		$prod->idModelo = $idModelo;
		$prod->idProductoTipo = (int)$request['idProductoTipo'];
		$prod->esNuevo = $request['esNuevo'];
//		if( Gate::allows('admin.producto.editPrecio')){
			$prod->precioMinimo = $request['precioMinimo'];
			$prod->monedaMinimo = trim($request['currencyMinimo']);
			$prod->precioProveedor = $request['precio'];
			$prod->monedaProveedor = trim($request['currency']);
			$prod->precioEuropa = $request['precioEuropa'];
			$prod->monedaEuropa = trim($request['currencyEuropa']);
			$prod->precioUnitario = $request['precioVenta'];
			$prod->monedaUnitario = trim($request['currencyVenta']);
			$prod->precioSudamerica = ( isset($request['priceSouthamerica']) && $request['priceSouthamerica'] == 1 ) ? true : false;
//		}

		$prod->save();

		$prodItem = $prod->ProductoItem;

//		if ( $request['idDisponibilidad'] > 0 ) {
//			$prodItem->idDisponibilidad = (int) $request['idDisponibilidad'];
//			$prodItem->save();
//		}

//		if($prod->Owner->id != $usuario->id) {
//			$prod->Owner()->associate($usuario);
//			$prod->save();
//
//			$prodItem->Owner()->associate($usuario);
//			$prodItem->save();
//		}

		// ---------- UBICACION

		$ubicacion = $prodItem->Ubicacion;
		$ubicacion->pais = trim($request['pais']);
		$ubicacion->ciudad = trim($request['ciudad']);
		$ubicacion->cpostal = trim($request['codigo_postal']);
		$ubicacion->direccion = trim($request['direccion']);
		$ubicacion->save();

		// ---------- DIMENSIONES
//		$prodItem->dimension = [
//			'lng' => (int) $request['longitud'],
//			'wdt' => (int) $request['ancho'],
//			'hgt' => (int) $request['alto'],
//			'wgt' => (int) $request['peso_bruto_fix'],
//			'pal' => (int) $request['paletizado'],
//		];
//		$prodItem->save();

		if( $prod->ProductoItem->idTipoImportacion == 1 ) {

		} else {
			// ---------- INFORMACION ADICIONAL
			$pdValues = [];
			foreach($campos as $campo){
				$val = trim($request[$campo['key']]);
				//TODO: Agregar validación y casting para tipo de campo numeric
				if(in_array($campo['type'], ['text', 'multiline'])){
					if($val != ''){
						$pdValues[$campo['key']] = $val;
					}

					$campo['translate'] = 1;
					if($prod->ProductoItem->Traducciones != null && $prod->ProductoItem->Traducciones->count() > 0){
						if($prod->ProductoItem->Traducciones->where('campo', $campo['key'])->count() > 0){
							$trans_field = $prod->ProductoItem->Traducciones->where('campo', $campo['key'])->first();
							$trans_field->delete();
						}
					}
				} elseif($campo['type'] == 'multiselect'){
					$pdValues[$campo['key']] = [];
					if(isset($request['multiselects']) && is_array($request['multiselects'])){
						foreach($request['multiselects'][$campo['key']] as $req){
							$pdValues[$campo['key']][] = $req;
						}
					}
				} else {
					if($val != ''){
						$pdValues[$campo['key']] = $val;
					}
				}

				if($campo['key'] == 'ano_de_fabricacion' && $val != ''){
					$prod->anio = (int) trim($request[$campo['key']]);
					$prod->save();
				}

			}
			$prodItem->definicion = $pdValues;
			$prodItem->save();
		}


		$pub = $prod->Publicacion;
		$pub->estado = Publicacion::ESTADO_AWAITING_APPROVAL;
		$pub->save();

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $prod
		);

		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Publicacion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => __('messages.delete_message_not', ['count' => $count, 'total' => count($ids)]), //'No se pudo eliminar el/los registro(s). Se eliminó : '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$user = Auth::user();
		$builder = Publicacion::with([
			'Producto.ProductoItem',
			'Producto.ProductoItem.Traducciones' => function($q) {
				$q->where('campo', 'nombre');
			},
			'Producto.Categorias',
			'Producto.Marca',
			'Producto.Modelo',
		])
			->has('Producto')
//			->onlyTrashed()
			->select(['*']);

		if( isset( $request['removed'] ) && $request['removed'] == 1 ) {
			$builder->onlyTrashed();
		}

		if( isset( $request['estado'] ) && $request['estado'] > 0 ) {
			$builder->where('estado', $request['estado']);
		}

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

//		$builder->where('owner_id', $user->id);
//		$builder->where('owner_type', 'App\Models\Usuario');

		// ---------- FILTERS

		if(isset($request['filters'])){
			logger("filters: ". $request['filters']);
			$filters = explode('|', $request['filters']);
			if(in_array('favoritos', $filters)){
				logger("tiene favoritos");
				$idFavs = [];
				$user = Auth::guard('clientes')->user();
				$idFavs = Favorito::where('idCliente', $user->Owner->id)->get()->pluck('idPublicacion')->toArray();
				logger($idFavs);
				$builder->whereIn('id', $idFavs);
			} else {
				$builder->where('owner_id', $user->id);
				$builder->where('owner_type', 'App\Models\Usuario');
			}
		} else {
			$builder->where('owner_id', $user->id);
			$builder->where('owner_type', 'App\Models\Usuario');
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('id', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}

	public function invoice($idAnnouncement) {



	}

	public function getForm($idProducto = 0, $idCategoria = 0) {

		if($idCategoria <= 0) {
			$response = [
				'idCategoria' => [
					'Campo idCategoria es requerido.'
				]
			];
			return response()->make($response, 422);
		}

		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}

		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		// ----------

		$lang = App::getLocale();

		if($idProducto > 0 ) {
			$producto = Producto::findOrFail($idProducto);
			$def = $producto->ProductoItem->definicion;
			$primaryKeys = array_keys($def);

			$hashOptions = [];

			$primaryData = [];
			foreach($primaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
//					if(in_array($value['type'], ['text', 'multiline'])){
//						if ( $producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0 ) {
//							if ( $producto->ProductoItem->Traducciones->where( 'campo', $value['key'] )->count() > 0 ) {
//								$value['value'] = $producto->ProductoItem->Traducciones->where( 'campo', $value['key'] )->first()->$lang;
//								if ( trim( $value['value'] ) == '' ) {
//									$value['value'] = 'MISSING TRANSLATION';
//								}
//							} else {
//								$value['value'] = $def[$value['key']];
//							}
//						} else {
//							$value['value'] = 'MISSING TRANSLATION';
//						}
//					} elseif( in_array($value['type'], ['multiselect']) ) {
////						foreach($def[$value['key']] as $mele) {
////							$hashOptions[$value['key']] = [
////								'key' =>
////							];
////						}
//
//						$value['value'] = $def[$value['key']];
//					} else {
						$value['value'] = $def[$value['key']];
//					}
					$value['html'] = '';
//				} else {
//					$primaryData[] = $value;
				}
				$primaryData[] = $value;
			}

			$secondaryData = [];
			foreach($secondaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
//					if(in_array($value['type'], ['text', 'multiline'])){
//						if($producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0){
//							if($producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
//								$value['value'] = $producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
//								if(trim($value['value']) == ''){
//									$value['value'] = 'MISSING TRANSLATION';
//								}
//							} else {
//								$value['value'] = $def[$value['key']];
//							}
//						} else {
//							$value['value'] = 'MISSING TRANSLATION';
//						}
////					} elseif(in_array($value['type'], ['titulo1', 'titulo2'])) {
////						$value['value'] = $def[$value['key']];
//					} elseif( in_array($value['type'], ['multiselect']) ) {
////						$hashOptions[$value['key']] = $def[$value['key']];
//						$value['value'] = $def[$value['key']];
//					} else {
						$value['value'] = $def[$value['key']];
//					}
					$value['html'] = '';
//				} else {
//					$primaryData[] = $value;
				}
				$secondaryData[] = $value;
			}

			return view('frontend.pages.private.announcements.form', [
				'idCategoria' => $idCategoria,
				'campos' => $campos,
//				'primaryFields' => $primaryFields,
//				'secondaryFields' => $secondaryFields,
				'primaryFields' => $primaryData,
				'secondaryFields' => $secondaryData,
				'hashOptions' => $hashOptions,
			]);
		}

		return view('frontend.pages.private.announcements.form', [
			'idCategoria' => $idCategoria,
			'campos' => $campos,
			'primaryFields' => $primaryFields,
			'secondaryFields' => $secondaryFields,
		]);

	}

	public function getCampos($idProducto = 0, $idCategoria = 0) {
		$id = (int) $idCategoria;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$response = Array(
			'result' => true,
			'message' => 'Campos.',
			'data' => $campos
		);

		return response()->json($response);
	}

	public function getModelos(Request $request){

		$this->validate($request, [
			'marca' => 'required'
		]);

		$marca = $request['marca'];

		$marca = Marca::where('nombre', trim($marca))->first();

		$modelos = [];
		if($marca){
			$modelos = Modelo::where('idMarca', $marca->id)->get()->pluck('nombre')->toArray();
		}

		$response = Array(
			'result' => true,
			'message' => 'Modelos relacionados.',
			'data' => $modelos
		);

		return response()->json($response);

	}
}
