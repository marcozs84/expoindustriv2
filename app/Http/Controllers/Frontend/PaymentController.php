<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Customer;
use Stripe\Stripe;

class PaymentController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function customer() {


    	$usuarios = Usuario::with('Owner.Agenda')->get();

    	return view('frontend.pages.payment.customers', [
    		'usuarios' => $usuarios
		]);

	}

	public function customerInfo(Request $request, $id) {
    	$usuario = Usuario::find($id);


		Stripe::setApiKey(env('STRIPE_KEY_SK'));

		$sCustomer = Customer::retrieve($usuario->getStripeId());

		$response = Array(
			'result' => true,
			'message' => 'Listo.',
			'data' => $sCustomer
		);

		return response()->json( $response );


	}

	public function createCustomer(Request $request, $id) {

    	$usuario = Usuario::find($id);

		Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB

		if(trim($usuario->getStripeId()) == ''){
			$customer = Customer::create([
				'email' => $usuario->username,
//				'source' => $request['token'],
			]);

			logger(print_r($customer, true));

			$usuario->setStripeId($customer['id']);

			$usuario->save();
		}

		$response = Array(
			'result' => true,
			'message' => 'Listo.',
			'data' => $usuario
		);

		return response()->json( $response );
	}

}
