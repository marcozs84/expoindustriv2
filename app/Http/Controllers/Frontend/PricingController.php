<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Banner;
use App\Models\Calculadora;
use App\Models\Categoria;
use App\Models\CategoriaDefinition;
use App\Models\Cliente;
use App\Models\Configuracion;
use App\Models\ConsultaExterna;
use App\Models\CotizacionEsquema;
use App\Models\Favorito;
use App\Models\Noticia;
use App\Models\Pais;
use App\Models\Producto;
use App\Models\ProveedorContacto;
use App\Models\Publicacion;
use App\Models\Subscripcion;
use App\Models\Transaccion;
use App\Models\Ubicacion;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Stripe\Checkout\Session;
use Stripe\Customer;
use Stripe\Stripe;

class PricingController extends Controller {
	
	public function __construct(){
//		$this->middleware('auth:clientes')->except(['index']);
//		$this->middleware('auth:clientes');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function pricing() {

		return view('frontend.pages.pricing');
	}

	public function authenticate(Request $request, $plan = '') {

//		if ( Auth::guard('clientes')->check() ) {
//			response()->redirectToRoute('');
//		}

		$plan = $request['plan'];
		$lang = App::getLocale();

		if ( $plan == 'business' ) {
			$route = "public.{$lang}.get_business";
		} elseif ( $plan == 'plus' ) {
			$route = "public.{$lang}.get_plus";
		} else {
			$route = "public.{$lang}.privCuenta";
		}

		return view('frontend.pages.authenticate', [
			'route' => $route,
			'plan' => $plan,
		]);

	}

    public function get_business(Request $request ) {

		if ( ! Auth::guard('clientes')->check() ) {
			return response()->redirectToRoute('public.authenticate', [
				'plan' => 'business'
			]);
		}

		$usuario = Auth::guard('clientes')->user();
		Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB

		if ( trim( $usuario->getStripeId() ) == '' ) {
			$customer = Customer::create( [
				'email' => $usuario->username,
			] );

			$usuario->setStripeId( $customer['id'] );
			$usuario->save();
		}

		if(env('APP_ENV') == 'local'){
			$product_key = 'pk_test_jo9qYMCsfdSwFBb1N4ay8vdR';
			$product_id = 'plan_Gc4XgFBNMi9Kht'; //11;
			$successUrl = 'http://www2.expoindustriv2/checkout_business?session_id={CHECKOUT_SESSION_ID}';
			$cancelUrl = 'http://www2.expoindustriv2/canceled_business';
		} else {
			if(preg_match('/expoindustri.se/', request()->url() ) ) {
				$product_key = 'pk_live_cakmM0iNjts9ZebkKgmdq2eZ';
				$product_id = 'plan_Gc7GZ3ICFPa7eD'; //2;
				$successUrl = 'https://www.expoindustri.se/checkout_business?session_id={CHECKOUT_SESSION_ID}';
				$cancelUrl = 'https://www.expoindustri.se/canceled_business';
			} else {
				$product_key = 'pk_live_cakmM0iNjts9ZebkKgmdq2eZ';
				$product_id = 'plan_Gc7GZ3ICFPa7eD'; //2;
				$successUrl = 'https://www.expoindustri.com/checkout_business?session_id={CHECKOUT_SESSION_ID}';
				$cancelUrl = 'https://www.expoindustri.com/canceled_business';
			}

		}

		$locale = App::getLocale();
		if( $locale == 'se' ) {
			$locale = 'sv';
		}

		$sessO = Session::create([
			'client_reference_id' => $usuario->id,
			'customer' => $usuario->getStripeId(),
			'success_url' => $successUrl,
			'cancel_url' => $cancelUrl,
			'payment_method_types' => ['card'],
			'locale' => $locale,
//			'mode' => 'subscription',
			'subscription_data' => [
				'items' => [
					[
						'plan' => $product_id,
						'quantity' => 1
					]
				]
			]
		]);

		return view('frontend.pages.get_business', [
			'usuario' => $usuario,
			'stripe_session_id' => $sessO['id'],
			'product_key' => $product_key,
		]);
	}

    public function get_plus(Request $request ) {

		if ( ! Auth::guard('clientes')->check() ) {
			return response()->redirectToRoute('public.authenticate', [
				'plan' => 'plus'
			]);
		}

		$usuario = Auth::guard('clientes')->user();
		Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB

		if ( trim( $usuario->getStripeId() ) == '' ) {
			$customer = Customer::create( [
				'email' => $usuario->username,
//				'source' => $request['token'],
			] );

			$usuario->setStripeId( $customer['id'] );
			$usuario->save();
		}

		if(env('APP_ENV') == 'local'){
			$product_key = 'pk_test_jo9qYMCsfdSwFBb1N4ay8vdR';
			$product_id = 'plan_Gc6MROgr16qrey'; //22;
			$successUrl = 'http://www2.expoindustriv2/checkout_plus?session_id={CHECKOUT_SESSION_ID}';
			$cancelUrl = 'http://www2.expoindustriv2/canceled_plus';
		} else {
			if(preg_match('/expoindustri.se/', request()->url() ) ) {
				$product_key = 'pk_live_cakmM0iNjts9ZebkKgmdq2eZ';
				$product_id = 'plan_Gc7HkFJpcvXaUx'; //3;
				$successUrl = 'https://www.expoindustri.se/checkout_plus?session_id={CHECKOUT_SESSION_ID}';
				$cancelUrl = 'https://www.expoindustri.se/canceled_plus';
			} else {
				$product_key = 'pk_live_cakmM0iNjts9ZebkKgmdq2eZ';
				$product_id = 'plan_Gc7HkFJpcvXaUx'; //3;
				$successUrl = 'https://www.expoindustri.com/checkout_plus?session_id={CHECKOUT_SESSION_ID}';
				$cancelUrl = 'https://www.expoindustri.com/canceled_plus';
			}

		}

		$locale = App::getLocale();
		if( $locale == 'se' ) {
			$locale = 'sv';
		}

		$sessO = Session::create([
			'client_reference_id' => $usuario->id,
			'customer' => $usuario->getStripeId(),
			'success_url' => $successUrl,
			'cancel_url' => $cancelUrl,
			'payment_method_types' => ['card'],
			'locale' => $locale,
//			'mode' => 'subscription',
			'subscription_data' => [
				'items' => [
					[
						'plan' => $product_id,
						'quantity' => 1
					]
				]
			]
		]);

		return view('frontend.pages.get_plus', [
			'usuario' => $usuario,
			'stripe_session_id' => $sessO['id'],
			'product_key' => $product_key,
		]);
	}

    public function checkout_business( Request $request ) {

    	$plan_name = 'plan_business';
		$transaction_id = 0;
		$transaccion = new Transaccion();
		$payment_type = 'credit card';

		$idTransaccionTipo = Transaccion::TIPO_CARD;
		$esFaktura = false;
		if( isset($_GET['type']) && $_GET['type'] == 'faktura' ) {
			$esFaktura = true;

//			$usuario = Auth::guard('clientes')->user();
//			$idUser = $usuario->id;
//			$monto = 8625;
//			$moneda = 'sek';

			// Alto riesgo de duplicidad de datos en TagManager

			$transaccion = Transaccion::find((int)$request['session_id']);
			if( $transaccion ) {
				$referencia = str_pad($transaccion->id, 5, '0', STR_PAD_LEFT);
				$transaction_id = $transaccion->id;
				$payment_type = 'faktura';
			} else {
				$referencia = 0;
				$transaction_id = 0;
				$transaccion = new Transaccion();
				$payment_type = '';
			}
		} else {

			if( isset( $request['session_id']) ) {
				$session_id = $request['session_id'];

				$transactions = Transaccion::where('idStripeTransaction', $session_id )->get();

				if( $transactions->count() > 0 ) {
					$transaccion = $transactions->first();
					$referencia = str_pad($transaccion->id, 5, '0', STR_PAD_LEFT);
					$transaction_id = $transaccion->id; // TRansaction already exists, so no new data should be added to DataLayer
				} else {

					Stripe::setApiKey( env( 'STRIPE_KEY_SK' ) );    // JB
					$session = Session::retrieve( $request['session_id'] );

					$idUser = $session['client_reference_id'];
					$usuario = Usuario::findOrFail($session->client_reference_id);

					$monto = $session['display_items'][0]['amount'] / 100;
					$moneda = $session['display_items'][0]['currency'];

					$monto = 6900; //8625;
					$impuesto = (6900 / 100) * 25; //8625;

					$transaccion = new Transaccion( [
						'idTransaccionTipo' => $idTransaccionTipo,
						'idUsuario' => $usuario->id,
						'idStripeCustomer' => $usuario->getStripeId(),
						'idStripeTransaction' => $request['session_id'],
						'producto' => $plan_name,
						'monto' => $monto,
						'impuesto' => $impuesto,
						'moneda' => $moneda,
						'estado' => 1,
					] );

					$sub = new Subscripcion();
					$sub->idSubscripcionTipo = Subscripcion::PLAN_BUSINESS;
					$sub->idUsuario = $idUser;
					$sub->fechaDesde = Carbon::today()->subDays(1);
					$sub->fechaHasta = Carbon::today()->addYears(1);
					$sub->save();

					$sub->Transacciones()->save($transaccion);

					$referencia = str_pad($transaccion->id, 5, '0', STR_PAD_LEFT);
					$transaction_id = $transaccion->id;

				}

			} else {
				$referencia = 0;
			}

		}

		return view('frontend.pages.checkout_business', [
			'referencia' => $referencia,
			'esFaktura' => $esFaktura,
			'transaction_id' => $transaction_id,
			'plan_name' => $plan_name,
			'transaccion' => $transaccion,
			'plan_id' => 1,
			'payment_type' => $payment_type
		]);
	}

    public function checkout_plus( Request $request ) {

		$plan_name = 'plan_plus';
		$transaction_id = 0;
		$transaccion = new Transaccion();
		$payment_type = 'credit card';

		$idTransaccionTipo = Transaccion::TIPO_CARD;
		$esFaktura = false;
		if( isset($_GET['type']) && $_GET['type'] == 'faktura' ) {
			$esFaktura = true;

//			$usuario = Auth::guard('clientes')->user();
//			$idUser = $usuario->id;
//			$monto = 16125;
//			$moneda = 'sek';

			$transaccion = Transaccion::find((int)$request['session_id']);
			if( $transaccion ) {
				$referencia = str_pad($transaccion->id, 5, '0', STR_PAD_LEFT);
				$transaction_id = $transaccion->id;
				$payment_type = 'faktura';
			} else {
				$referencia = 0;
				$transaction_id = 0;
				$transaccion = new Transaccion();
				$payment_type = '';
			}

		} else {

			if( isset( $request['session_id']) ) {
				$session_id = $request['session_id'];

				$transactions = Transaccion::where('idStripeTransaction', $session_id )->get();

				if( $transactions->count() > 0 ) {
					$transaccion = $transactions->first();
					$referencia = str_pad($transaccion->id, 5, '0', STR_PAD_LEFT);
					$transaction_id = $transaccion->id; // TRansaction already exists, so no new data should be added to DataLayer
				} else {

					Stripe::setApiKey( env( 'STRIPE_KEY_SK' ) );    // JB
					$session = Session::retrieve( $request['session_id'] );

					$idUser = $session['client_reference_id'];
					$usuario = Usuario::findOrFail($session->client_reference_id);

					$monto = $session['display_items'][0]['amount'] / 100;
					$moneda = $session['display_items'][0]['currency'];

					$monto = 12900; //8625;
					$impuesto = (12900 / 100) * 25; //8625;

					$transaccion = new Transaccion( [
						'idTransaccionTipo' => $idTransaccionTipo,
						'idUsuario' => $usuario->id,
						'idStripeCustomer' => $usuario->getStripeId(),
						'idStripeTransaction' => $request['session_id'],
						'producto' => $plan_name,
						'monto' => $monto,
						'impuesto' => $impuesto,
						'moneda' => $moneda,
						'estado' => 1,
					] );

					$sub = new Subscripcion();
					$sub->idSubscripcionTipo = Subscripcion::PLAN_PLUS;
					$sub->idUsuario = $idUser;
					$sub->fechaDesde = Carbon::today()->subDays(1);
					$sub->fechaHasta = Carbon::today()->addYears(1);
					$sub->save();

					$sub->Transacciones()->save($transaccion);

					$referencia = str_pad($transaccion->id, 5, '0', STR_PAD_LEFT);
					$transaction_id = $transaccion->id;

				}

			} else {
				$referencia = 0;
			}

		}

		return view('frontend.pages.checkout_plus', [
			'referencia' => $referencia,
			'esFaktura' => $esFaktura,
			'transaction_id' => $transaction_id,
			'plan_name' => $plan_name,
			'transaccion' => $transaccion,
			'plan_id' => 2,
			'payment_type' => $payment_type
		]);
	}

	public function canceled_business( Request $request ) {
		$lang = App::getLocale();
		return redirect()->route("public.{$lang}.get_business");
	}

	public function canceled_plus( Request $request ) {
		$lang = App::getLocale();
		return redirect()->route("public.{$lang}.get_plus");
	}

	public function factura_detail( Request $request ) {

		$plan = $request['plan'];
		$type = $request['type'];

		$usuario = Auth::guard('clientes')->user();

		if( ! $usuario ) {
			return redirect()->route('public.authenticate', ['plan' => $plan]);
		}

		$provContacto = ProveedorContacto::where('idUsuario', $usuario->id)->first();
		$agenda = $usuario->Owner->Agenda;
		$prov = $provContacto->Proveedor;
		$companyName = $prov->nombre;
		$companyNit = $prov->nit;
		$companyDesc = $prov->descripcion;
		$ubicacionFacturacion = new Ubicacion();
		$ubicacionFacturacion->idUbicacionTipo = Ubicacion::TIPO_FACTURACION;

		if($prov->Telefonos->count() == 0){
			if($usuario->Owner->Agenda->Telefonos->count() > 0){
				$prov->Telefonos()->create([
					'numero' => $agenda->Telefonos->first()->numero,
					'descripcion' => $agenda->Telefonos->first()->descripcion,
					'estado' => $agenda->Telefonos->first()->estado,
				]);
			}
		}
		$companyPhone = $prov->Telefonos->first()->numero;
		$ups = $prov->Ubicaciones->where('idUbicacionTipo', Ubicacion::TIPO_FACTURACION)->first();
		if($ups) {
			$ubicacionFacturacion = $ups;
		}

		$postal_code = '';


		if( !$request->isMethod('post')) {

			return view('frontend.pages.factura_detail', [
				'referencia' => '0000',
				'esFaktura' => true,
				'plan' => $plan,
				'type' => $type,

				'email' => $usuario->username,
				'postal_code' => $postal_code,
				'prov' => $prov,
				'companyName' => $companyName,
				'companyNit' => $companyNit,
				'companyPhone' => $companyPhone,
				'ubicacionFacturacion' => $ubicacionFacturacion,
			]);


		} else {

			$this->validate( $request, [
				'email' => 'required',
				'lugar' => 'required',
				'zipcode' => 'required',
				'direccion' => 'required',
			]);
			/*
				[empresa] =>
				[email] =>
				[nit] =>
				[zipcode] =>
				[direccion] =>
				[plan] => plus

			*/

			$prov->nombre = trim( $request['empresa'] );
			$prov->nit = trim( $request['nit'] );
			$ubicacionFacturacion->email = trim( $request['email'] );
			$ubicacionFacturacion->lugar = trim( $request['lugar'] );
			$ubicacionFacturacion->cpostal = trim( $request['zipcode'] );
			$ubicacionFacturacion->direccion = trim( $request['direccion'] );

			$ups = $prov->Ubicaciones->where('idUbicacionTipo', Ubicacion::TIPO_FACTURACION)->first();
			if($ups) {
				$ubicacionFacturacion->save();
			} else {
				$prov->Ubicaciones()->save($ubicacionFacturacion);
			}

			// ---------- GENERATES FACTURA

			if( $plan == 'business' ) {
				$plan_name = 'plan_business';
				$monto = 6900; //8625;
				$impuesto = (6900 / 100) * 25; //8625;
				$moneda = 'sek';
				$idPlan = Subscripcion::PLAN_BUSINESS;
			} elseif( $plan == 'plus' ) {
				$plan_name = 'plan_plus';
				$monto = 12900; //16125;
				$impuesto = (12900 / 100) * 25; //8625;
				$moneda = 'sek';
				$idPlan = Subscripcion::PLAN_PLUS;
			} else {
				$plan_name = 'plan_desconocido';
				$monto = 0;
				$impuesto = 0;
				$moneda = 'sek';
				$idPlan = 0;
			}

			$transaccion = new Transaccion( [
				'idTransaccionTipo' => Transaccion::TIPO_FAKTURA,
				'idUsuario' => $usuario->id,
				'idStripeCustomer' => 0,
				'idStripeTransaction' => '',
				'producto' => $plan_name,
				'monto' => $monto,
				'impuesto' => $impuesto,
				'moneda' => $moneda,
				'estado' => 1,
			] );


			$sub = new Subscripcion();
			$sub->idSubscripcionTipo = $idPlan;
			$sub->idUsuario = $usuario->id;
			$sub->fechaDesde = Carbon::today()->subDays(1);
			$sub->fechaHasta = Carbon::today()->addYears(1);
			$sub->save();

			$sub->Transacciones()->save($transaccion);

			$referencia = str_pad($transaccion->id, 5, '0', STR_PAD_LEFT);


			// ----------

    		if( $plan == 'business' ) {
				return redirect()->route('public.checkout_business', ['type' => $type, 'session_id' => $transaccion->id]);
			} elseif( $plan == 'plus' ) {
				return redirect()->route('public.checkout_plus', ['type' => $type, 'session_id' => $transaccion->id]);
			} else {
				$lang = App::getLocale();
				return redirect()->route("public.{$lang}.pricing");
			}

		}

	}

}
