<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Banner;
use App\Models\Calculadora;
use App\Models\Calendario;
use App\Models\Categoria;
use App\Models\CategoriaDefinition;
use App\Models\Cliente;
use App\Models\Configuracion;
use App\Models\ConsultaExterna;
use App\Models\CotizacionEsquema;
use App\Models\Favorito;
use App\Models\FBCampaign;
use App\Models\Metatag;
use App\Models\Newsletter;
use App\Models\Noticia;
use App\Models\Pais;
use App\Models\Producto;
use App\Models\ProveedorContacto;
use App\Models\Publicacion;
use App\Models\Tag;
use App\Models\Ubicacion;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller {
	
	public function __construct(){
//		$this->middleware('auth:clientes')->except(['index']);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index_sales(Request $request ) {
		$noticias = Noticia::orderBy('fechaPublicacion', 'DESC')->get();

		$newsletter_banner = false;
		if( isset($_GET['news']) ) {
			$newsletter_banner = true;
		}

		return view('frontend.pages.index_sales', [
			'newsletter_banner' => $newsletter_banner,
			'noticias' => $noticias
		]);
	}

    public function index_servicios(Request $request ) {
		$noticias = Noticia::orderBy('fechaPublicacion', 'DESC')->get();

		$newsletter_banner = false;
		if( isset($_GET['news']) ) {
			$newsletter_banner = true;
		}

		return view('frontend.pages.index_sales_services', [
			'newsletter_banner' => $newsletter_banner,
			'noticias' => $noticias
		]);
	}
    public function index_nosotros(Request $request ) {
		$noticias = Noticia::orderBy('fechaPublicacion', 'DESC')->get();

		$newsletter_banner = false;
		if( isset($_GET['news']) ) {
			$newsletter_banner = true;
		}

		return view('frontend.pages.index_sales_nosotros', [
			'newsletter_banner' => $newsletter_banner,
			'noticias' => $noticias
		]);
	}

    public function index(Request $request) {

		$countryProv = ['dk','no','nl','de','be','fr','es','uk']; //['se','dk','no','nl','de','be','fr','es','uk'];
		$latinoamerica = ['bo', 'cl', 'pe', 'co', 'ar', 'br', 'cr', 'cu', 'sv', 'gt', 'ht', 'hn', 'mx', 'ni', 'pa', 'py', 'do', 'uy', 've'];

		$fc = new FBCampaign();
		if( isset( $_GET['fbclid'] ) ) {
			$fc->fbclid = trim( $request['fbclid'] );
		}
		if( isset( $_GET['fbc'] ) ) {
			$fc->idFBC = (int)$request['fbc'];
		}
		if( isset( $_GET['fbc'] ) ||  isset( $_GET['fbclid'] ) ) {
			$fc->save();
		}

		// ----------

		$noticias = Noticia::orderBy('fechaPublicacion', 'DESC')->get();

		$countryAbr = session('visitor_location', 'se');
		logger('$countryAbr: '.$countryAbr);
		if(in_array($countryAbr, $countryProv) || $countryAbr == 'se'){

			$newsletter_banner = false;
			if( isset($_GET['news']) ) {
				$newsletter_banner = true;
			}

			return view('frontend.pages.landing_expand', [
				'newsletter_banner' => $newsletter_banner,
				'noticias' => $noticias
			]);
		}
//    	if( session('visitor_location', 'se') == 'se' ) {
//			return view('frontend.pages.landing_expand');
//		}

		// ----------

		return view('frontend.pages.index_sales');

		// ----------

		$welcomeBanner = true;
		$value = $request->cookie('newbie');
		if($value == 1) {
			$welcomeBanner = false;
		}

    	// ----------

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		// ----------

//		$banners = Banner::with([
//			'Galeria.Imagenes',
//		])
//			->where('estado', 1)
//			->orderBy('orden', 'asc')
//			->get();
//
////		$imgBanners = [];
//		$location = 'se';
//		if( in_array($countryAbr, $countryProv) ){
//			$location = 'europa';
//		} elseif( in_array($countryAbr, $latinoamerica) ) {
//			$location = 'latam';
//		}
//
//		foreach($banners as $key => $banner) {
//
//			if( is_array($banner->visibilidad) ) {
//				if( !in_array($location, $banner->visibilidad) ) {
//					$banners->forget($key);
//				}
//			}
//
//			if( empty( $banner->visibilidad ) ) {
//				$banners->forget($key);
//			}
//
////			$imgBanners[] = $banner->Galeria->Imagenes->first()->ruta_publica;
//		}

		$banners = [];

		// ---------- DIRECT LINK TO VIDEO

		$autoVideo = false;
		if( isset( $request['av'] ) ){
			$autoVideo = true;
		}

		// ---------- TAGS

		$tags_obj = $this->tag_groups();

		return view('frontend.pages.index', [
			'categorias' => $categorias,
			'banners' => $banners,
//			'imgBanners' => $imgBanners,
			'noticias' => $noticias,
			'welcomeBanner' => $welcomeBanner,
			'autoVideo' => $autoVideo,
			'tag_counts' => $tags_obj['counts'], //$tag_counts,
			'grouped_tags' => $tags_obj['groups'], //$grouped_tags,
		]);
    }

    public function oldbuy(Request $request) {
		if( App::getLocale() == 'se' ) {
			return redirect()->route('public.marknadsplats')->setStatusCode(301);
		} else {
			return redirect()->route('public.buy_page')->setStatusCode(301);
		}
	}

	public function buy(Request $request) {

		if( App::getLocale() == 'se' && $request->routeIs('public.buy_page') ) {
			return redirect()->route('public.marknadsplats')->setStatusCode(301);
		}

		$welcomeBanner = true;
		$value = $request->cookie('newbie');
		if($value == 1) {
			$welcomeBanner = false;
		}

		// ----------

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		// ----------

		$countryProv = ['dk','no','nl','de','be','fr','es','uk'];  //['se','dk','no','nl','de','be','fr','es','uk'];
		$latinoamerica = ['bo', 'cl', 'pe', 'co', 'ar', 'br', 'cr', 'cu', 'sv', 'gt', 'ht', 'hn', 'mx', 'ni', 'pa', 'py', 'do', 'uy', 've'];
		$countryAbr = session('visitor_location', 'se');

		$banners = Banner::with([
			'Galeria.Imagenes',
		])
			->where('estado', 1)
			->orderBy('orden', 'asc')
			->get();

		$imgBanners = [];

		$location = 'se';
		if( in_array($countryAbr, $countryProv) ){
			$location = 'europa';
		} elseif( in_array($countryAbr, $latinoamerica) ) {
			$location = 'latam';
		}

		foreach($banners as $key => $banner) {

			if( is_array($banner->visibilidad) ) {
				if( !in_array($location, $banner->visibilidad) ) {
					$banners->forget($key);
				}
			}

			if( empty( $banner->visibilidad ) ) {
				$banners->forget($key);
			}

//			$imgBanners[] = $banner->Galeria->Imagenes->first()->ruta_publica;
		}

		// ----------

		$noticias = Noticia::all();

		// ---------- DIRECT LINK TO VIDEO

		$autoVideo = false;
		if( isset( $request['av'] ) ){
			$autoVideo = true;
		}


		// ---------- TAGS

		$tags_obj = $this->tag_groups();

		return view('frontend.pages.index', [
			'categorias' => $categorias,
			'banners' => $banners,
			'imgBanners' => $imgBanners,
			'noticias' => $noticias,
			'welcomeBanner' => $welcomeBanner,
			'autoVideo' => $autoVideo,
			'tag_counts' => $tags_obj['counts'], //$tag_counts,
			'grouped_tags' => $tags_obj['groups'], //$grouped_tags,
		]);
	}

	public function carousel() {

    	$banners = Banner::with([
    		'Galeria.Imagenes',
		])
			->orderBy('orden', 'asc')
			->get();

    	$imagenes = [];

    	foreach($banners as $banner) {
			$imagenes[] = $banner->Galeria->Imagenes->first()->ruta_publica_banner;
		}

		return view('frontend.pages.carousel', [
			'imagenes' => $imagenes
		]);
	}

//	public function home() {
//		return view('frontend.pages.home');
//	}

    public function nosotros() {
    	return view('frontend.pages.nosotros');
	}

    public function services(Request $request, $s = 'none') {

    	return view('frontend.pages.services', [
    		'service' => $s
		]);
	}

    public function faq() {
    	return view('frontend.pages.faq');
	}

    public function pricing() {
    	return view('frontend.pages.pricing');
	}

    public function buy_business() {
    	return view('frontend.pages.buy_business');
	}

    public function checkout_business() {
    	return view('frontend.pages.checkout_business');
	}

    public function checkout_plus() {
    	return view('frontend.pages.checkout_plus');
	}

    public function buy_plus() {
    	return view('frontend.pages.buy_plus');
	}

    public function contacto() {
    	return view('frontend.pages.contacto');
	}

	public function contactoPost(Request $request) {

    	$this->validate($request, [
    		'nombres' => 'required',
    		'apellidos' => 'required',
			'email' => 'required|string|email|max:255',
			'pais' => 'required',
			'asunto' => 'required',
			'mensaje' => 'required',
		]);

		// ---------- RE-CAPTCHA

		$tipoConsulta = ConsultaExterna::TIPO_CONTACT;
		if( isset( $request['referrer'] ) ) {
			if ( $request['referrer'] == 'landing_expand' ) {
				$tipoConsulta = ConsultaExterna::TIPO_CONTACT;
			} else {
				$public_key = '6LdcWocUAAAAAKiSh4xFKlB6HrCw--LWOnZU17pK';
				$private_key = '6LdcWocUAAAAAMbiWnFAeELlY-ejaC9bXkMzimam';
				$url = 'https://www.google.com/recaptcha/api/siteverify';
				$ip = $_SERVER['REMOTE_ADDR'];

				$response_key = $request['g-recaptcha-response'];

				$fullUrl = $url . "?secret={$private_key}&response={$response_key}&remoteip={$ip}";

				$response = file_get_contents($fullUrl);
				$response = json_decode($response);

				if($response->success == 1) {
					$tipoConsulta = ConsultaExterna::TIPO_CONTACT;
				} else {
					$tipoConsulta = ConsultaExterna::TIPO_CONTACT_ROBOT;
				}
			}
		}

		// ----------

    	$pais = Pais::where('codigo', $request['pais'])->first();
    	$html = <<<xxx
<table class="table table-striped table-condensed" cellpadding="4" cellspacing="4" style="background-color:#d7e0e8;">
<tr>
	<td style="padding:3px; vertical-align: top; font-weight:bold; text-align:right; background-color:#ffffff; width:70px;">Nombres: </td>
	<td style="padding:3px 3px 3px 6px;">{$request['nombres']} {$request['apellidos']}</td>
</tr>
<tr>
	<td style="padding:3px; vertical-align: top; font-weight:bold; text-align:right; background-color:#ffffff;">Email: </td>
	<td style="padding:3px 3px 3px 6px;">{$request['email']}</td>
</tr>
<tr>
	<td style="padding:3px; vertical-align: top; font-weight:bold; text-align:right; background-color:#ffffff;">Pais: </td>
	<td style="padding:3px 3px 3px 6px;">{$pais->nombre}</td>
</tr>
<tr>
	<td style="padding:3px; vertical-align: top; font-weight:bold; text-align:right; background-color:#ffffff;">Asunto: </td>
	<td style="padding:3px 3px 3px 6px;">{$request['asunto']}</td>
</tr>
<tr>
	<td style="padding:3px; vertical-align: top; font-weight:bold; text-align:right; background-color:#ffffff;">Mensaje: </td>
	<td style="padding:3px 3px 3px 6px;">{$request['mensaje']}</td>
</tr>
</table>
xxx;

    	if($tipoConsulta != ConsultaExterna::TIPO_CONTACT_ROBOT) {

			Mail::send('frontend.email.template_light', [
				'titulo' => __('messages.contact_form'),
				'content' => $html,
				'local' => session('visitor_location', 'en')
			], function ($m) use ($request) {
				$m->from('info@expoindustri.com', 'ExpoIndustri');
//			$m->to( 'marco.zeballos@expoindustri.com' );
				$m->to('info+formcontacto@expoindustri.com', 'Marco Zeballos');
//				$m->bcc( 'marco.zeballos+formcontacto@expoindustri.com', 'Marco Zeballos' );
				$m->replyTo( $request['email'], $request['nombres'] . ' ' . $request['apellidos']);
				$m->subject(__('messages.contact_form'));
			});

//			Mail::send('frontend.email.template_dark', [
//				'content' => $html,
//				'titulo' => 'Consulta por formulario de contacto'
//			], function ($m) {
//				$m->from('system@expoindustri.com', 'ExpoIndustri');
//				$m->to('marcozs84@gmail.com', 'Marco Zeballos');
////			$m->bcc('marcozs84@gmail.com', 'Marco Zeballos');
//				$m->subject('Consulta por formulario de contacto');
//			});
		}



//		$this->validate($request, [
//			'nombres' => 'required',
//			'apellidos' => 'required',
//			'email' => 'required|email',
//			'pais' => 'required',
//			'telefono' => 'required',
//			'descripcion' => 'required'
//		]);

		$ce = new ConsultaExterna($request->all());
		$ce->estado = ConsultaExterna::ESTADO_NEW;
		$ce->idTipoConsulta = $tipoConsulta;
		$ce->descripcion = trim($request['mensaje']);

		$result = $ce->save();

//		$response = Array(
//			'result' => $result,
//			'message' => 'Registro creado.',
//			'data' => $ce->toArray()
//		);
//
//		return response()->json($response);

		if( isset( $request['referrer'] ) ) {
			switch( $request['referrer'] ) {
				case 'landing_expand':
					return view('frontend.pages.landing_expand', [
						'mail_enviado' => true,
					]);
					break;
				case 'landing_sales':
					return view('frontend.pages.index_sales', [
						'mail_enviado' => true,
					]);
					break;
				case 'landing_services':
					return view('frontend.pages.index_sales_services', [
						'mail_enviado' => true,
					]);
					break;

			}
		}


		return view('frontend.pages.contacto', [
			'mail_enviado' => true
		]);
	}

	public function changeLanguage(Request $request){
    	$this->validate($request, [
    		'lang' => 'required'
		]);

    	session(['lang' => $request['lang']]);

		$response = Array(
			'result' => true,
			'message' => 'Language selected.',
			'data' => []
		);

		return response()->json($response);
	}

	public function changeCurrency(Request $request){
    	$this->validate($request, [
    		'cur' => 'required'
		]);

    	session(['current_currency' => $request['cur']]);

		$response = Array(
			'result' => true,
			'message' => 'Currency selected.',
			'data' => []
		);

		return response()->json($response);
	}

	public function dataProtectionPolicy(){
		return view('frontend.pages.dataProtectionPolicy');
	}

	public function termsAndConditions(){
		return view('frontend.pages.termsAndConditions');
	}

	public function info_compradores(Request $request) {
		return view('frontend.pages.info_compradores');
	}

	public function info_anunciantes(Request $request) {
		return view('frontend.pages.info_anunciantes');
	}

	public function perfilCliente($id){

		$cliente = Cliente::find($id);
		$usuario = $cliente->Usuario;

		$provContacto = ProveedorContacto::where('idUsuario', $usuario->id)->first();
		$prov = $provContacto->Proveedor;
		$galeria = null;

		$companyName = '';
		$companyNit = '';
		$logoUrl = '';
		$companyDesc = '';
		$urlSitio = '';
		$urlSitioHttp = '';
		$imagen = null;
		$ubicacionPrimaria = new Ubicacion();
		$ubicacionesSecundarias = [];

		if($provContacto){
			$prov = $provContacto->Proveedor;
			$galeria = $prov->Galerias->first();
			if(!$galeria){
				$galeria = $prov->Galerias()->save(new Galeria());
			}
			if($galeria->Imagenes->count() > 0){
				$imagen = $galeria->Imagenes->first();
				$logoUrl = $galeria->Imagenes->first()->filename;
			}
			$companyName = $prov->nombre;
			$companyNit = $prov->nit;
			$companyDesc = $prov->descripcion;
			$urlSitio = $prov->urlSitio;

			$urlSitioHttp = $urlSitio;
			if(preg_match('/http/', $urlSitio) == 0) {
				$urlSitioHttp = 'http://'.$urlSitio;
			}


			$ups = $prov->Ubicaciones->where('idUbicacionTipo', 1)->first();
			if($ups) {
				$ubicacionPrimaria = $ups;
			}

			$upsec = $prov->Ubicaciones->where('idUbicacionTipo', 2);
			if($upsec) {
				$ubicacionesSecundarias = $upsec;
			}

			$lang = App::getLocale();
			$traduccion = $prov->Traducciones->where('campo', 'descripcion')->first();
			if( $traduccion ) {
				$companyDesc = $traduccion->$lang;
			}
//
		}

//		return view('frontend.pages.privProviderProfileEdit', [
//			'images' => [],
//			'companyName' => $companyName,
//			'companyNit' => $companyNit,
//			'logoUrl' => $logoUrl,
//			'companyDesc' => $companyDesc,
//		]);

		return view('frontend.pages.perfilCliente', [
			'cliente' => $cliente,
			'companyName' => $companyName,
			'companyNit' => $companyNit,
			'imagen' => $imagen,
			'logoUrl' => $logoUrl,
			'companyDesc' => $companyDesc,
			'prov' => $prov,
			'ubicacionPrimaria' => $ubicacionPrimaria,
			'ubicacionesSecundarias' => $ubicacionesSecundarias,
			'urlSitio' => $urlSitio,
			'urlSitioHttp' => $urlSitioHttp,
		]);
	}


	public function toc(){
		return view('frontend.pages.toc');
	}

	public function getnews($id) {

		$noticia = Noticia::findOrFail($id);

		$response = Array(
			'result' => true,
			'message' => 'listo.',
			'data' => $noticia->toArray()
		);

		return view('frontend.pages.noticia', [
			'titulo' => $noticia['titulo_'.App::getLocale()],
			'resumen' => $noticia['resumen_'.App::getLocale()],
			'noticia' => $noticia
		]);

	}

	public function setWelcomeBanner(Request $request) {

		return response('Cookie created')->cookie(
			//'newbie', '1', 10080
			'newbie', '1', 3
		);

	}

	public function landing_expand(Request $request) {

		$fc = new FBCampaign();
		if( isset( $_GET['fbclid'] ) ) {
			$fc->fbclid = trim( $request['fbclid'] );
		}
		if( isset( $_GET['fbc'] ) ) {
			$fc->idFBC = (int)$request['fbc'];
		}
		if( isset( $_GET['fbc'] ) ||  isset( $_GET['fbclid'] ) ) {
			$fc->save();
		}

		// ----------

		$noticias = Noticia::orderBy('fechaPublicacion', 'DESC')->get();

		return view('frontend.pages.landing_expand', [
			'noticias' => $noticias
		]);
	}

	public function auth_as( $token ) {

		$u = Usuario::where('impersonation_token', $token)->first();
		if( $u ) {
			$u->impersonation_token = '';
			$u->save();

			Auth::guard('clientes')->login( $u );
			$lang = App::getLocale();
//			return redirect()->route("public.{$lang}.privCuenta");
			return redirect()->route("private.{$lang}.announcements");
		} else {
			return redirect('/');
		}
	}

	public function newsletter(Request $request) {

		if($request->isMethod('post')) {

			$this->validate($request, [
				'email' => 'required',
			]);

			$n = new Newsletter();
			$n->email = $request['email'];
			$n->status = 1;
			$n->save();

			$response = Array(
				'result' => true,
				'message' => 'Subscribed.',
				'data' => []
			);

			return response()->json( $response );
		}
	}

	public function seguimiento( $id, $action = '' ) {

		$producto = Producto::with([
			'Calendarios' => function( $q ) {
				$q->where('idCalendarioTipo', Calendario::TIPO_HISTORIA);
			},
			'Calendarios.Actividades',
			'Calendarios.Actividades.ActividadTipo' => function( $q ) {
				$q->orderBy('orden', 'asc');
			}
		])->findOrFail($id);

		$actividades = [];
		if( $producto->Calendarios->count() > 0 ) {
			$actividades = $producto->Calendarios[0]->Actividades;
		}

		$meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Noviembre', 'Diciembre'];

		foreach( $actividades as &$actividad ) {
			$created_at = $actividad->created_at;
			$actividad->created_at_literal = $created_at->format('d').' '.$meses[ $created_at->format('n') ].' '.$created_at->format('Y');
		}


		return view('frontend.pages.seguimiento', [
			'producto' => $producto,
			'action' => $action,
			'actividades' => $actividades
		]);
	}

	// ---------- S H A R E D

	private function tag_groups() {
		$lang = App::getLocale();

		$tag_counts = DB::select( DB::raw("select count(idProducto) as countp, idTag from ProductoTag GROUP BY idTag") );
		$tag_counts = array_pluck($tag_counts, 'countp', 'idTag');

		$tags2 = Tag::with([
			'Traduccion',
			'Hijos.Traduccion',
		])->where('idPadre', 0)->get();

		$grouped_tags = [];
		foreach( $tags2 as $tag) {
			if( !isset($grouped_tags[ $tag->Traduccion->$lang ]) ) {
				$grouped_tags[ $tag->Traduccion->$lang ] = [];
			}
			foreach( $tag->Hijos as $key => $hijo ) {
				if( $hijo->Traduccion) {
					$grouped_tags[ $tag->Traduccion->$lang ][$hijo->id] = $hijo->Traduccion->$lang;
				}
			}
		}

		return [
			'counts' => $tag_counts,
			'groups' => $grouped_tags,
		];
	}
}
