<?php

namespace App\Http\Controllers\Frontend;

use App\Models\ConsultaExterna;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConsultaExternaController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    	$this->validate($request, [
			'nombres' => 'required',
			'apellidos' => 'required',
			'email' => 'email',
			'pais' => 'required',
			'telefono' => 'required',
            'descripcion' => 'required',
            'tipo' => 'required',
        ]);

        if(!in_array(trim($request['tipo']), [
			'banner',
			'search',
			'inspection',
			'support',
			'istore',
			'product_contact_consultant',
			'product_client_automation',
		])) {
			$response = [
				'Tipo' => [
					__('messages.service_request_mistype'),
				]
			];
			return response()->make($response, 422);
		}

		$ce = new ConsultaExterna($request->all());
        $ce->estado = ConsultaExterna::ESTADO_NEW;
        switch(trim($request['tipo'])){
			case 'banner':
				$ce->idTipoConsulta = ConsultaExterna::TIPO_BANNER;
				break;
			case 'search':
				$ce->idTipoConsulta = ConsultaExterna::TIPO_SEARCH_ALERT;
				break;
			case 'inspection':
				$ce->idTipoConsulta = ConsultaExterna::TIPO_INSPECTION;
				break;
			case 'support':
				$ce->idTipoConsulta = ConsultaExterna::TIPO_SUPPORT;
				break;
			case 'istore':
				$ce->idTipoConsulta = ConsultaExterna::TIPO_ISTORE;
				break;

			case 'product_contact_consultant':
				$ce->idTipoConsulta = ConsultaExterna::TIPO_PRODUCT_CONTACT_CONSULTANT;
				$ar = [];
				$ar['idPub'] = (int)$request['pub_id'];
				$ce->extrainfo = $ar;
				break;

			case 'product_client_automation':
				$ce->idTipoConsulta = ConsultaExterna::TIPO_CLIENT_AUTOMATIC_IMPORT;
				break;
			default:
				$ce->idTipoConsulta = ConsultaExterna::TIPO_CONTACT;
		}

		$result = $ce->save();

		$response = Array(
			'result' => $result,
			'message' => __('messages.record_created'),
			'data' => $ce->toArray()
		);

		return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
