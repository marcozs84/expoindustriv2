<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Calculadora;
use App\Models\Categoria;
use App\Models\CategoriaDefinition;
use App\Models\CotizacionEsquema;
use App\Models\Favorito;
use App\Models\Publicacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AnnounceController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$ceId = 0;

		$pub = Publicacion::with([
			'Producto.ProductoItem',
			'Producto.Categorias',
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Padre',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda'
		])
			->findOrFail($id);

		$esquema = null;

		if(!$pub){

			$subcat = $pub->Producto->Categorias()->first();
			$producto = $pub->Producto;
			$cat = $subcat->Padre;

			switch($cat->id){
				case Categoria::INDUSTRIAL:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::TRANSPORTE:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::EQUIPO:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::AGRICOLA:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::MONTACARGA:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::OTROS:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				default:
					$esquema = null;
			}

			$ce = CotizacionEsquema::find(1);
		}


		// ----------

		$def = $pub->Producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		$idCategoria = $pub->Producto->Categorias[0]->id;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$lang = App::getLocale();

		$primaryData = [];
		foreach($primaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				if(in_array($value['type'], ['text', 'multiline'])){
					if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
						if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
							$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
							if(trim($value['value']) == ''){
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
					} else {
						$value['value'] = 'SIN TRADUCCION';
					}
				} else {
					$value['value'] = $def[$value['key']];
				}
				$value['html'] = '';
				$primaryData[] = $value;
			}
		}

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		$secondaryData = [];
		foreach($secondaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				if(in_array($value['type'], ['text', 'multiline'])){
					if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
						if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
							$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
							if(trim($value['value']) == ''){
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
					} else {
						$value['value'] = 'SIN TRADUCCION';
					}
				} else {
					$value['value'] = $def[$value['key']];
				}
				$value['html'] = '';
				$secondaryData[] = $value;
			}
		}

		// ---- calculadora

		$calc = $pub->Producto->ProductoItem->Calculadora;
		if(!$calc){
			$calc = new Calculadora();
			$calc->informe_tecnico = 0;
			$calc->seguro_extra = 0;
			$calc->contenedor_compartido = 0;
			$calc->contenedor_exclusivo = 0;
			$calc->envio_abierto = 0;
			$calc->destino_1 = 0;
			$calc->destino_2 = 0;
			$calc->destino_3 = 0;
			$calc->destino_4 = 0;
			$calc->destino_5 = 0;
			$calc->destino_6 = 0;
			$pub->Producto->ProductoItem->Calculadora = $calc;
		}

		// ---------- FAVORITOS

		$esFavorito = false;
		if(Auth::guard('clientes')->user()){
			$user = Auth::guard('clientes')->user();
			$f = Favorito::where('idPublicacion', $pub->id)->where('idCliente', $user->Owner->id)->get();
			logger($f);
			if($f->count() > 0){
				$esFavorito = true;
			}
		}

		// ---------- SEO
		$seo_metas = [];
		$meta_description = '';
		if( $pub->Producto ) {
			$global_localization = App::getLocale();
			$keywords = new \stdClass();
			$keywords->name = 'keywords';
			$keywords->content = $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] . ' ' . $pub->Producto->Categorias[0]->Traduccion[$global_localization] . ' ' . $pub->Producto->Marca->nombre . ' ' . $pub->Producto->Modelo->nombre;
			$description = new \stdClass();
			$description->name = 'description';
			$description->content = $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] . ' ' . $pub->Producto->Categorias[0]->Traduccion[$global_localization] . ' ' . $pub->Producto->Marca->nombre . ' ' . $pub->Producto->Modelo->nombre;
			$meta_description = $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] . ' ' . $pub->Producto->Categorias[0]->Traduccion[$global_localization] . ' ' . $pub->Producto->Marca->nombre . ' ' . $pub->Producto->Modelo->nombre;

			array_push($seo_metas, $keywords);
			array_push($seo_metas, $description);
		}

//		$values = [];
//		foreach($primaryData as $pd ) {
//			$values[] = $pd['value'];
//		}
//		$meta_description = implode(', ', $values);


		return view('frontend.pages.producto', [
			'id' => $id,
			'esquema' => $esquema,
			'pub' => $pub,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'esFavorito' => $esFavorito,
			'seo_metas' => $seo_metas,
			'meta_description' => $meta_description,
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
