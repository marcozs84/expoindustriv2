<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Producto;
use App\Models\Publicacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentController extends Controller {

	public function relatedItems(Request $request, $cat = 0){
		$this->validate($request, [
			'cat' => '',
			'subcat' => ''
		]);

		$builder = Publicacion::with([
			'Producto.ProductoItem',
			'Producto.Galerias',
			'Producto.Categorias.Padre',
		])
			->has('Producto')
			->where('estado', Publicacion::ESTADO_APPROVED);

//		logger("cat: ".$cat);

		if($cat > 0){
			$builder->whereHas('Producto.Categorias.Padre', function($q) use($cat){
				$q->where('id', $cat);
			});
		}

		$publications = $builder->paginate(6);

		return view('frontend.components.relatedItems', [
			'publications' => $publications
		]);

	}

	public function mostVisitedItems(Request $request){
		$this->validate($request, [
			'cat' => '',
			'subcat' => ''
		]);

		$publications = Publicacion::with([
			'Producto.ProductoItem',
			'Producto.Galerias',
		])
			->has('Producto')
			->where('estado', Publicacion::ESTADO_APPROVED)
			->orderBy('visitas', 'desc')
			->paginate(6);

		return view('frontend.components.mostVisitedItems', [
			'publications' => $publications
		]);

	}

	public function latestAddedItems(Request $request){
		$this->validate($request, [
			'cat' => '',
			'subcat' => ''
		]);

		$publications = Publicacion::with([
			'Producto.ProductoItem',
			'Producto.Galerias',
		])
			->has('Producto')
			->where('estado', Publicacion::ESTADO_APPROVED)
			->whereHas('Producto', function($q ) {
				$q->where('idProductoTipo', Producto::TIPO_MAQUINARIA);
			})
			->orderBy('fechaInicio', 'DESC')
			->paginate(8);

		return view('frontend.components.latestAddedItems', [
			'publications' => $publications
		]);

	}

	public function latestAddedItems_footer(Request $request){
		$this->validate($request, [
			'cat' => '',
			'subcat' => ''
		]);

		$publications = Publicacion::with([
			'Producto.ProductoItem',
			'Producto.Galerias',
		])
			->has('Producto')
			->where('estado', Publicacion::ESTADO_APPROVED)
			->orderBy('fechaInicio', 'DESC')
			->paginate(6);

		return view('frontend.components.latestAddedItems_footer', [
			'publications' => $publications
		]);

	}

	public function announcesByProvider(Request $request, $idUsuario){
		$this->validate($request, [
			'cat' => '',
			'subcat' => ''
		]);

		$publications = Publicacion::with([
			'Producto.ProductoItem',
			'Producto.Galerias',
		])
			->has('Producto')
			->where('estado', Publicacion::ESTADO_APPROVED)
			->where('owner_id', $idUsuario)
			->orderBy('fechaInicio', 'DESC')
			->paginate(52);

		return view('frontend.components.announcesByProviderItems', [
			'publications' => $publications
		]);

	}

}
