<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Categoria;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller {

	public function buscar(Request $request, $cat = 0) {

		$none = false;
		$subcat = 0;

    	if($request->method() == 'POST'){
    		$this->validate($request, [
    			'sideSearchCategoria' => '',
				'sideSearchSubCategoria' => '',
			]);

			$cat = (int) $request['sideSearchCategoria'];
			$subcat = (int) $request['sideSearchSubCategoria'];

			if( $cat > 0 ) {

				$categorias = Categoria::with('Traduccion')->whereIn('id', [ $cat, $subcat ])->get();

				$lang = App::getLocale();
				$strCat = $this->cleanUrl( $categorias->where('id', $cat)->first()->Traduccion->$lang );

				if( $subcat > 0 ) {
					$strSubCat = $this->cleanUrl( $categorias->where('id', $subcat)->first()->Traduccion->$lang );
					return response()->redirectTo("/{$strCat}/{$strSubCat}");
				}
				return response()->redirectTo("/{$strCat}");

			}



//			if(isset( $request['sideSearchMarca'])) {
//				session(['search_idmarca' => $request['sideSearchMarca']]);
//			}
//			if(isset( $request['sideSearchPrecioFrom'])) {
//				session(['search_PrecioFrom' => $request['sideSearchPrecioFrom']]);
//			}
//			if(isset( $request['sideSearchPrecioTo'])) {
//				session(['search_PrecioTo' => $request['sideSearchPrecioTo']]);
//			}
//			if(isset( $request['sideSearchAnioFrom'])) {
//				session(['search_AnioFrom' => $request['sideSearchAnioFrom']]);
//			}
//			if(isset( $request['sideSearchAnioTo'])) {
//				session(['search_AnioTo' => $request['sideSearchAnioTo']]);
//			}
//			if(isset( $request['sideSearchTags'])) {
//				session(['search_tags' => $request['sideSearchTags']]);
//			}



//			$catList = $categorias->pluck('Traduccion.'.App::getLocale(), 'id')->toArray();
//			$subCatList_init = $catList;
//
//			foreach($catList as $id => $val ) {
//				$val = $this->cleanUrl( $val );
//				$catList[$id] = $val;
//			}
		} else {
			if( session('search_idmarca', 'none') != 'none') {
				$request['sideSearchMarca'] = session('search_idmarca');
			}
			if( session('search_PrecioFrom', 'none') != 'none') {
				$request['sideSearchPrecioFrom'] = session('search_PrecioFrom');
			}
			if( session('search_PrecioTo', 'none') != 'none') {
				$request['sideSearchPrecioTo'] = session('search_PrecioTo');
			}
			if( session('search_AnioFrom', 'none') != 'none') {
				$request['sideSearchAnioFrom'] = session('search_AnioFrom');
			}
			if( session('search_AnioTo', 'none') != 'none') {
				$request['sideSearchAnioTo'] = session('search_AnioTo');
			}
			if( session('search_tags', 'none') != 'none') {
				$request['sideSearchTags'] = session('search_tags');
			}
		}

		$categorias = Categoria::with('Traduccion')->get();
//    	$catList = $categorias->where('nivel', 0)->pluck('Traduccion.es')->toArray();
		$catList = $categorias->where('nivel', 0)->pluck('id')->toArray();


//		if(isset($request['cat'])){
//
//			$cat = $request['cat'];
//			logger("isset cat: {$cat}");
//			if(!in_array($cat, $catList)){
//				$cat = 0;
//			}
//		}

		// 'Industrial' => 1,
		// 'Transporte' => 11,
		// 'Equipo' => 17,
		// 'Agricola' => 27,
		// 'Montacarga' => 33,
		// 'Otros' => 37,

		switch( $request->route()->getName() ) {

			// SE
			case 'public.industri':						$cat = 1;	break;
			case 'public.industriell':						$cat = 1;	break;
			case 'public.transport':					$cat = 11;	break;
			case 'public.entreprenad':					$cat = 17;	break;
			case 'public.entreprenadmaskiner':			$cat = 17;	break;
			case 'public.lantbruksmaskiner':			$cat = 27;	break;
			case 'public.lantbruk':						$cat = 27;	break;
			case 'public.truckar':						$cat = 33;	break;
			case 'public.truck':						$cat = 33;	break;
			case 'public.ovriga':						$cat = 37;	break;

			// EN
			case 'public.industrial':					$cat = 1;	break;
//			case 'public.transport':					$cat = 11;	break;
			case 'public.equipment':					$cat = 17;	break;
			case 'public.agricultural':					$cat = 27;	break;
			case 'public.forklift':						$cat = 33;	break;
			case 'public.others':						$cat = 37;	break;

			// ES
//			case 'public.industrial':					$cat = 1;	break;
			case 'public.transporte':					$cat = 11;	break;
			case 'public.equipo':						$cat = 17;	break;
			case 'public.agricola':						$cat = 27;	break;
			case 'public.montacarga':					$cat = 33;	break;
			case 'public.otros':						$cat = 37;	break;

			// SE
			case 'public.industri_alt':					$subcat = $cat;	$cat = 1;	break;
			case 'public.industriell_alt':				$subcat = $cat;	$cat = 1;	break;
			case 'public.transport_alt':				$subcat = $cat;	$cat = 11;	break;
			case 'public.entreprenad_alt':				$subcat = $cat;	$cat = 17;	break;
			case 'public.lantbruk_alt':					$subcat = $cat;	$cat = 27;	break;
			case 'public.truck_alt':					$subcat = $cat;	$cat = 33;	break;
			case 'public.andra_alt':					$subcat = $cat;	$cat = 37;	break;

			// EN
			case 'public.industrial_alt':				$subcat = $cat;	$cat = 1;	break;
//			case 'public.transport_alt':				$subcat = $cat;	$cat = 11;	break;
			case 'public.equipment_alt':				$subcat = $cat;	$cat = 17;	break;
			case 'public.agricultural_alt':				$subcat = $cat;	$cat = 27;	break;
			case 'public.forklift_alt':					$subcat = $cat;	$cat = 33;	break;
			case 'public.others_alt':					$subcat = $cat;	$cat = 37;	break;

			// ES
//			case 'public.industrial_alt':				$subcat = $cat;	$cat = 1;	break;
			case 'public.transporte_alt':				$subcat = $cat;	$cat = 11;	break;
			case 'public.equipo_alt':					$subcat = $cat;	$cat = 17;	break;
			case 'public.agricola_alt':					$subcat = $cat;	$cat = 27;	break;
			case 'public.montacarga_alt':				$subcat = $cat;	$cat = 33;	break;
			case 'public.otros_alt':					$subcat = $cat;	$cat = 37;	break;

			default:									$none = true;
		}

		if( $none == true ) {
			if ( is_string( $cat ) ) {
				switch( $cat ) {
					case 'industri':						$cat = 1;	break;
					case 'transport':						$cat = 11;	break;
					case 'entreprenad':						$cat = 17;	break;
					case 'lantbruk':						$cat = 27;	break;
					case 'truckar':							$cat = 33;	break;
					case 'ovriga':							$cat = 37;	break;
				}
			}
		}

		if($cat != 0) {
			$request['sideSearchCategoria'] = $cat;
		}

		$subCatList_init = [];

		if($subcat != '' && is_string($subcat)) {

			$categorias = Categoria::with('Traduccion')->where('idPadre', $cat)->get();
			$catList = $categorias->pluck('Traduccion.'.App::getLocale(), 'id')->toArray();
			$subCatList_init = $catList;

			foreach($catList as $id => $val ) {
				$val = $this->cleanUrl( $val );
				$catList[$id] = $val;
			}

			$key = array_search( $subcat, $catList );
			if( $key >= 0 ) {
				$request['sideSearchSubCategoria'] = $key;
			} else {
				if( !isset($request['sideSearchSubCategoria']) ) {
					$request['sideSearchSubCategoria'] = 0;
				}
			}
		} else {
			if(is_numeric($subcat) && $subcat > 0) {
				$categoria = Categoria::with('Traduccion')->where('id', $subcat)->first();
				$lang = App::getLocale();
				$subcat = strtolower( $categoria->Traduccion->$lang );
			}
			$categorias = Categoria::with('Traduccion')->where('idPadre', $cat)->get();
			$catList = $categorias->pluck('Traduccion.'.App::getLocale(), 'id')->toArray();
			$subCatList_init = $catList;
		}

		// ----------

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		foreach( $categorias as &$categorialang ) {
			$categorialang->urlname = $this->cleanUrl( $categorialang->nombre );
			$categorialang->Traduccion->url_es = $this->cleanUrl( $categorialang->Traduccion->es );
			$categorialang->Traduccion->url_se = $this->cleanUrl( $categorialang->Traduccion->se );
			$categorialang->Traduccion->url_en = $this->cleanUrl( $categorialang->Traduccion->en );
			foreach( $categorialang->Subcategorias as &$subcatlang ) {
				$subcatlang->urlname = $this->cleanUrl( $subcatlang->nombre );
				$subcatlang->Traduccion->url_es = $this->cleanUrl( $subcatlang->Traduccion->es );
				$subcatlang->Traduccion->url_se = $this->cleanUrl( $subcatlang->Traduccion->se );
				$subcatlang->Traduccion->url_en = $this->cleanUrl( $subcatlang->Traduccion->en );
			}
		}
		// ----------
		$page_title = '';
		$subcat_search = $request['sideSearchSubCategoria'];
		if( $cat > 0 ) {
			$categorias2 = Categoria::with('Traduccion')->whereIn('id', [ $cat, $subcat_search ])->get();

			$lang = App::getLocale();
			$page_title = $categorias2->where('id', $cat)->first()->Traduccion->$lang;

			if( $subcat_search > 0 ) {
				$page_title = $categorias2->where('id', $subcat_search)->first()->Traduccion->$lang;
			}
		}

		// ---------- TAGS

		$lang = App::getLocale();

		$tag_counts = DB::select( DB::raw("select count(idProducto) as countp, idTag from ProductoTag GROUP BY idTag") );
		$tag_counts = array_pluck($tag_counts, 'countp', 'idTag');

		$tags2 = Tag::with([
			'Traduccion',
			'Hijos.Traduccion',
		])->where('idPadre', 0)->get();

		$grouped_tags = [];
		foreach( $tags2 as $tag) {
			if( !isset($grouped_tags[ $tag->Traduccion->$lang ]) ) {
				$grouped_tags[ $tag->Traduccion->$lang ] = [];
			}
			foreach( $tag->Hijos as $key => $hijo ) {
				if( $hijo->Traduccion) {
					$grouped_tags[ $tag->Traduccion->$lang ][$hijo->id] = $hijo->Traduccion->$lang;
				}
			}
		}

		return view('frontend.pages.buscar', [
			'cat' => $cat,
			'categorias' => $categorias,
			'search_category' => $cat,
			'page_title' => $page_title,
			'search_subcategory' => $subcat,
			'request' => $request,
			'subCatList_init' => $subCatList_init,
			'tag_counts' => $tag_counts,
			'grouped_tags' => $grouped_tags,
		]);
	}

	private function cleanUrl( $string ) {
		$string = strtolower( $string );
//		$string = urlencode( $string );

		// Ref.: https://stackoverflow.com/a/14114419/2367718
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}

}
