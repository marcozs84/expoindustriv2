<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Categoria;
use App\Models\CategoriaDefinition;
use App\Models\Configuracion;
use App\Models\Galeria;
use App\Models\Imagen;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Producto;
use App\Models\ProductoItem;
use App\Models\Publicacion;
use App\Models\Subscripcion;
use App\Models\Transaccion;
use App\Models\Ubicacion;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class PublishController extends Controller {

	public function publish(Request $request, $np = null) {

//		$request->session()->flush();
//		$request->session()->forget('pubsess');

		$session = session('pubsess', 'none');

		$pubDraft = ListaItem::listado(Lista::PUBLICACION_ESTADO)->where('texto', 'draft')->first();
		if(!$pubDraft){
			$this->reportDev('No se encontró Estado DRAFT para crear la publicaciòn de announce here. File:'.__FILE__.' Line:'.__LINE__);
		}

		if($session == 'none'){
			$random = substr(md5(mt_rand()), 0, 5);
			$sessionName = date('YmdHis').'_'.$random;
			$session = [
				'id' => $sessionName,
				'marca' => '',
				'modelo' => '',
				'type' => '',
				'status' => '',
				'categoria' => 0,
				'subcat' => 0,
				'ciudad' => '',
				'pcode' => '',
				'direccion' => '',
				'longitud' => '',
				'ancho' => '',
				'alto' => '',
				'peso' => '',
				'palet' => '',
				'imagenes' => []
			];
			session(['pubsess' => $session]);
		}

		// ----------

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		// ---------- MODELOS

		$marcas = Marca::all()->pluck('nombre');

		$scat = (int) str_replace('_sb_', '', $session['categoria']);
		$ssub = (int) str_replace('_sb_', '', $session['subcat']);
		$spal = (int) str_replace('_sb_', '', $session['palet']);

		// ---------- SUBSCRIPCIONES

		$tipo_pago = '';
		$subs = [];
		if(Auth::guard('clientes')->user()) {
			$user = Auth::guard('clientes')->user();
			$subs = Subscripcion::where('idUsuario', $user->id)
				->where('fechaDesde', '<', Carbon::today())
				->where('fechaHasta', '>', Carbon::today())
				->get();
			if($subs->count() > 0){
				$tipo_pago = 'subscripcion';
				$np = 1;
			}
		}

		return view('frontend.pages.publish', [
			'categorias' => $categorias,
			'marcas' => $marcas,
			'sesna' => $session['id'],
			'marca' => $session['marca'],
			'modelo' => $session['modelo'],
			'type' => $session['type'],
			'status' => $session['status'],
//			'categoria' => $session['categoria'],
//			'categoria' => session('categoria', 0),
			'categoria' => $scat,
//			'subcat' => $session['subcat'],
//			'subcat' => session('subcat', 0),
			'subcat' => $ssub,
			'ciudad' => $session['ciudad'],
			'pcode' => $session['pcode'],
			'direccion' => $session['direccion'],
			'longitud' => (int) str_replace('_sb_', '', $session['longitud']),
			'ancho' => (int) str_replace('_sb_', '', $session['ancho']),
			'alto' => (int) str_replace('_sb_', '', $session['alto']),
			'peso' => (int) str_replace('_sb_', '', $session['peso']),
			'palet' => ($spal === 1) ? true : false,
			'np' => $np,
			'tipo_pago' => $tipo_pago,
		]);
	}

	public function getModelos(Request $request){

		$this->validate($request, [
			'marca' => 'required'
		]);

		$marca = $request['marca'];

		$marca = Marca::where('nombre', trim($marca))->first();

		$modelos = [];
		if($marca){
			$modelos = Modelo::where('idMarca', $marca->id)->get()->pluck('nombre')->toArray();
		}

		$response = Array(
			'result' => true,
			'message' => 'Modelos relacionados.',
			'data' => $modelos
		);

		return response()->json($response);

	}

	public function publishForm(Request $request, $id = 0){

		if($id == 0){
			$this->validate($request, [
				'idCategoria' => 'required|numeric'
			]);

			$id = $request['idCategoria'];
		}

		$cd = CategoriaDefinition::where('idCategoria', $id)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$id}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$id}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		// ---------- CURRENCY

		$currency = ListaItem::where('idLista', Lista::MONEDA)->get();
		$moneda = $currency->pluck('texto', 'valor')->toArray();
		foreach($moneda as $k => $v){
			$moneda[$k] = __('messages.'.$v);
		}

		$session = session('pubsess');
		logger("publish form");
		logger($session);

		return view('frontend.pages.publishForm', [
			'idCategoria' => $id,
			'campos' => $campos,
			'primaryFields' => $primaryFields,
			'secondaryFields' => $secondaryFields,
			'sessionId' => $session['id'],
//			'pub' => $pub,
			'moneda' => $moneda,
			'sesna' => $session['id'],
			'images' => (isset($session['imagenes'])) ? $session['imagenes'] : [],
		]);

	}

	public function publishRemove(Request $request){
		$session = session('pubsess', 'none');
		if($session == 'none'){
			$response = 'Your session has timed out, please refresh the page.';
			return response()->make($response, 422);
		}

		$this->validate($request, [
			'image' => 'required'
		]);

		logger($session);

		$name = trim($request['image']);
		$newImgs = [];
		foreach($session['imagenes'] as $img){
//			if($img['filename'] != $name){
			if(trim($img['filename']) != $name){
				$newImgs[] = $img;
			} else {
				logger("ruta");
				logger($img['ruta']);
				$destinationPath = storage_path($img['ruta']);
				if(File::exists($destinationPath)){
					logger("deleting: {$destinationPath}");
					File::delete($destinationPath);
				}
			}
		}

		$session['imagenes'] = $newImgs;

		session(['pubsess' => $session]);

		logger($session);

		$response = Array(
			'result' => true,
			'message' => 'Image removed.',
			'data' => []
		);

		return response()->json($response);

	}

	public function publishUpload(Request $request){

		$session = session('pubsess', 'none');
		if($session == 'none'){
			$response = 'Your session has timed out, please refresh the page.';
			return response()->make($response, 422);
		}

		$this->validate($request, [
			'sesna' => 'required',
			'file' => 'max:3072',
		]);

		// ----------

		if($request->hasFile('file')){

			$file = $request->file('file');
			$files = [$file];
			foreach($files as $file){
				$filename_original = $file->getClientOriginalName();
				logger($filename_original);
				$extension_original = $file->getClientOriginalExtension();
				logger($extension_original);
				$mimetype = $file->getMimeType();
				logger($mimetype);
				$extension = $file->extension();
				logger($extension);

				if(!in_array($file->getMimeType(), ['image/jpeg', 'image/gif', 'image/png'])){
					$response = __('messages.invalid_image_file');
					return response()->make($response, 422);
				}

				if(!$file->isValid()){
					$response = __('messages.invalid_file');
					return response()->make($response, 422);
				}

				$relativePath = "app".DIRECTORY_SEPARATOR."publish_temp";
				$destinationPath = storage_path($relativePath);

				if(!is_dir($destinationPath)){
					//$mkdir = mkdir($destinationPath, 0755);
					$mkdir = File::makeDirectory($destinationPath, 0755, true);
				}

				$randChars = $this->randomChars(5);
				$filename = "img_{$randChars}.{$extension}";

				$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
				if(File::exists($fullpath)){
					$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
					File::delete($fullpath);
				}

				$file->move($destinationPath, $filename);

				if(!File::exists($fullpath)){

					$response = 'Archivo no encontrado despues de subir.';
					return response()->make($response, 422);
				}

				$session = session('pubsess');
				$session['imagenes'][] = [
					'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
					'filename_orig' => $filename_original,
					'filename' => $filename,
					'mimetype' => $mimetype
				];
				session(['pubsess' => $session]);

			}

			$jsonArchivos = [];

			$response = 'File uploaded correctly';
			return response()->json($response);

		} else {
			$response = 'There is no file';
			return response()->make($response, 422);

		}

	}

	public function updateSession(Request $request){

		$session = session('pubsess', 'none');
		if($session == 'none'){
			$response = 'Your session has timed out, please refresh the page.';
			return response()->make($response, 422);
		}

		if(isset($request['marca'])){
			$session['marca'] = trim($request['marca']);
		}
		if(isset($request['modelo'])){
			$session['modelo'] = trim($request['modelo']);
		}
		if(isset($request['type'])){
			$session['type'] = trim($request['type']);
		}
		if(isset($request['status'])){
			$session['status'] = trim($request['status']);
		}
		if(isset($request['categoria'])){
			session(['categoria' => trim($request['marca']).'_sb_'.trim($request['categoria'])]);
			$session['categoria'] = '_sb_'.trim($request['categoria']);
		}
		if(isset($request['subcat'])){
			session(['subcat' => trim($request['modelo']).'_sb_'.trim($request['subcat'])]);
			$session['subcat'] = '_sb_'.trim($request['subcat']);
		}
		if(isset($request['pais'])){
			$session['pais'] = trim($request['pais']);
		}
		if(isset($request['ciudad'])){
			$session['ciudad'] = trim($request['ciudad']);
		}
		if(isset($request['pcode'])){
			$session['pcode'] = trim($request['pcode']);
		}
		if(isset($request['direccion'])){
			$session['direccion'] = trim($request['direccion']);
		}
		if(isset($request['longitud'])){
			$session['longitud'] = trim($request['modelo']).'_sb_'.trim($request['longitud']);
		}
		if(isset($request['ancho'])){
			$session['ancho'] = trim($request['modelo']).'_sb_'.trim($request['ancho']);
		}
		if(isset($request['alto'])){
			$session['alto'] = '_sb_'.trim($request['alto']);
		}
		if(isset($request['peso'])){
			$session['peso'] = '_sb_'.trim($request['peso']);
		}
		if(isset($request['palet'])){
			$session['palet'] = '_sb_'.trim($request['palet']);
		}

		session(['pubsess' => $session]);
		$session = session('pubsess', 'none');

		sleep(1);
	}

	public function randomChars($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function publishValidate(Request $request){
		$session = session('pubsess', 'none');
		if($session == 'none'){
			$response = 'Your session has timed out, please refresh the page.';
			return response()->make($response, 422);
		}

		// ----------
		$imagenes = $session['imagenes'];
		if(count($imagenes) == 0){
			$response = [
				'AnnounceImages' => [
					__('messages.publish_missing_images')
				]
			];
			return response()->make($response, 422);
		}
		// ----------

		$this->validate($request, [
			'idc' => 'required|numeric',
		]);

		$idc = $request['idc'];
		$cds = CategoriaDefinition::where('idCategoria', $idc)->get();
		if($cds->count() > 1){
			$this->reportDev('Hay mas de un CategoriaDefinition para la categoria: '.$idc);
		}

		$cd = $cds->first();

		$campos = $cd->campos;
		$validations = [];
		foreach($campos as $campo){
			if(isset($campo['validation'])){
				$validations[$campo['key']] = $campo['validation'];
			}
		}
		$validations['precio'] = 'required';
		$this->validate($request, $validations);

		$response = Array(
			'result' => true,
			'message' => 'Formulario válido.',
			'data' => []
		);

		return response()->json($response);

	}

	public function publishFormPost(Request $request){

		$session = session('pubsess', 'none');
		if($session == 'none'){
			$response = 'Your session has timed out, please refresh the page.';
			return response()->make($response, 422);
		}

		$imagenes = $session['imagenes'];
		if(count($imagenes) == 0){
			$response = [
				'AnnounceImages' => [
					__('messages.publish_missing_images')
				]
			];
			return response()->make($response, 422);
		}

		// ----------

		$this->validate($request, [
			'idc' => 'required|numeric',
			'marca' => 'required',
			'modelo' => 'required',
//			'categoria' => 'required',
			'subcat' => 'required',
			'payment_type' => 'required',
		]);

		$session['marca'] = trim($request['marca']);
		$session['modelo'] = trim($request['modelo']);
		session(['pubsess' => $session]);

		$idc = $request['idc'];
		$cds = CategoriaDefinition::where('idCategoria', $idc)->get();
		if($cds->count() > 1){
			$this->reportDev('Hay mas de un CategoriaDefinition para la categoria: '.$idc);
		}

		$cd = $cds->first();

		$campos = $cd->campos;
		$validations = [];
		foreach($campos as $campo){
			if(isset($campo['validation'])){
				$validations[$campo['key']] = $campo['validation'];
			}
		}
		$validations['precio'] = 'required';
		$this->validate($request, $validations);

//		$this->validate($request, [
//			'subcat' => 'required',
//			'precio' => 'required',
//			'currency' => 'required',
//		]);

		logger("listo para guardar");
		logger($session);

		// ---------- CONFIGURACIONES

		$precio_dia = Configuracion::where('llave', 'precio_dia_anuncio')->first()->valor;
		$precio_extendido = Configuracion::where('llave', 'precio_anuncio_tiempo_extendido')->first()->valor;

		// ---------- STRIPE CUSTOMER PAYMENT

		$usuario = Auth::guard('clientes')->user();

		$days = (float)$request['days'];
		$price = $days * $precio_dia;
		if($request['time_extended'] == 1){
			$price += $precio_extendido;
		}

		if($request['payment_type'] == 'stripe' && $request['token'] != 'none') {
//				Stripe::setApiKey('sk_test_mntDijCBJikZmtWTWuPC3eB5');	// MZ
			Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB

			if(trim($usuario->getStripeId()) == ''){
				$customer = Customer::create([
					'email' => $usuario->username,
//					'source' => $request['token'],
				]);
				$usuario->setStripeId($customer['id']);

				$usuario->save();
			}

			$priceStripe = $price * 100;

			$chargeData = [];
			if($request['current_card'] == '') {
				$customer = Customer::retrieve($usuario->getStripeId());
				$card = $customer->sources->create(["source" => $request['token']]);

				$chargeData = [
					'amount' => $priceStripe,
//					'currency' => $request['currency'],
					'currency' => 'sek',
					'description' => 'Announcement fee',
					'source' => $card['id'],
					'customer' => $usuario->getStripeId(),
				];
			} else {
				logger("CURRENT_CARD = ".$request['current_card']);
				$chargeData = [
					'amount' => $priceStripe,
//					'currency' => $request['currency'],
					'currency' => 'sek',
					'description' => 'Announcement fee',
					'card' => $request['current_card'],
					'customer' => $usuario->getStripeId(),
				];
			}

			try {
				$charge = Charge::create($chargeData);

				$transaccion = new Transaccion([
					'idTransaccionTipo' => Transaccion::TIPO_CARD,
					'idUsuario' => $usuario->id,
					'idStripeCustomer' => $charge->customer,
					'idStripeTransaction' => $charge->id,
					'producto' => $charge->description,
					'monto' => $charge->amount,
					'moneda' => $charge->currency,
					'estado' => $charge->status
				]);
			} catch(\Stripe\Error\Card $e) {
				// Since it's a decline, \Stripe\Error\Card will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];

				logger(print_r($err, true));
				$response = [
					'CardError' => [
						$err['message'].' More info here: <a href="'.$err['doc_url'].'" target="_blank">'.$err['doc_url'].'</a>'
					]
				];
				return response()->make($response, 422);
			} catch (Exception $e) {

				logger($e->getMessage());
				logger($e->getCode());

				$e_message = <<<xxx
					Error al ejecutar un pago.<br> 
					Usuario: {$e->getCode()}<br> 
					Codigo: {$e->getCode()}<br> 
					Mensaje: '.$e->getMessage()<br>
xxx;
				$this->reportDev($e_message);
			}


		} elseif ($request['payment_type'] == 'faktura') {
			// Nada por aqui, buscar mas abajo el mismo criterio.
			$transaccion = new Transaccion([
				'idTransaccionTipo' => Transaccion::TIPO_FAKTURA,
				'idUsuario' => $usuario->id,
//				'idStripeCustomer' => $charge->customer,
//				'idStripeTransaction' => $charge->id,
				'producto' => 'Announcement fee', //$charge->description,
				'monto' => $price,
				'moneda' => 'sek',
				'estado' => Transaccion::ESTADO_SUCCEEDED
			]);
		}

		// ----------

//		$pubDraft = ListaItem::listado(Lista::PUBLICACION_ESTADO)->where('texto', 'draft')->first();
		$pubAA = ListaItem::listado(Lista::PUBLICACION_ESTADO)->where('texto', 'awaiting_approval')->first();
		if(!$pubAA){
			$this->reportDev('No se encontró Estado awaiting_approval para crear la publicación de announce here. File:'.__FILE__.' Line:'.__LINE__);
		}

		// ---------- MARCA Y MODELO

		$marcas = Marca::where('nombre', trim($request['marca']))->get();
		if($marcas->count() > 1){
			$marca = $marcas->first();
			$this->reportDev('Se encontró mas de una marca coincidente con: '. $request['marca']);
		} elseif ($marcas->count() == 1){
			$marca = $marcas->first();
		} else {
			$marca = Marca::create(['nombre' => trim($request['marca'])]);
		}

		$idMarca = $marca->id;

		$modelos = Modelo::where('idMarca', $idMarca)
			->where('nombre', trim($request['modelo']))
			->get();
		if($modelos->count() > 1){
			$modelo = $modelos->first();
			$this->reportDev('Se encontró mas de un modelo coincidente con: '. $request['modelo']. ' en la marcaID: '.$idMarca);
		} elseif ($modelos->count() == 1){
			$modelo = $modelos->first();
		} else {
			$modelo = Modelo::create(['idMarca' => $idMarca, 'nombre' => trim($request['modelo'])]);
		}

		$idModelo = $modelo->id;

		$idCategoria = (int) $request['subcat'];

		// ----------

		$pub = new Publicacion();
		$pub->token = $session['id'];
		$pub->estado = $pubAA->valor;
		$pub->dias = $days;
		$pub->precio = $price;
		$pub->moneda = 'sek';
		$pub->fechaInicio = Carbon::today();
		$pub->fechaFin = Carbon::today()->addDays($days);

		$pub = $usuario->Publicaciones()->save($pub);

		if($request['payment_type'] == 'stripe' && $request['token'] != 'none') {
			$pub->Transacciones()->save($transaccion);
			$pub->idTipoPago = Publicacion::PAGO_STRIPE;
			$pub->save();
		} elseif ($request['payment_type'] == 'faktura') {
			$pub->Transacciones()->save($transaccion);
			$pub->idTipoPago = Publicacion::PAGO_FAKTURA;
			$pub->save();
		} elseif ($request['payment_type'] == 'subscripcion') {
			$pub->idTipoPago = Publicacion::PAGO_SUBSCRIPTION;
			$pub->save();
		}

		$prod = new Producto();
		$prod->idMarca = $idMarca;
		$prod->idModelo = $idModelo;
		$prod->idProductoTipo = $request['type'];
		$prod->esNuevo = $request['status'];
		$prod->precioProveedor = $request['precio'];
		$prod->monedaProveedor = trim($request['currency']);
		$prod->precioMinimo = $request['precioMinimo'];
		$prod->monedaMinimo = trim($request['currencyMinimo']);
		$prod->precioEuropa = $request['precioEuropa'];
		$prod->monedaEuropa = trim($request['currencyEuropa']);
		$prod->precioSudamerica = trim($request['priceSouthamerica']);
		$prod->save();

		$prod->Owner()->associate($usuario);
		$prod->save();

		$prod->Categorias()->attach($idCategoria);
		$galeria = $prod->Galerias()->save(new Galeria());
		$prodItem = $prod->ProductoItem()->save(new ProductoItem());
		$prodItem->idDisponibilidad = 1;

		$ubicacion = $prodItem->Ubicacion()->save(new Ubicacion([
			'pais' => $session['pais'],
			'ciudad' => $session['ciudad'],
			'cpostal' => $session['pcode'],
			'direccion' => $session['direccion']
		]));

		$pub->Producto()->associate($prod);
		$pub->save();


		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->first();

		$campos = $cd->campos;
		$pdValues = [];
		foreach($campos as $campo){
			$val = trim($request[$campo['key']]);
			//TODO: Agregar validación y casting para tipo de campo numeric
			if($campo['type'] == 'multiselect'){
				$pdValues[$campo['key']] = [];
				if(isset($request['multiselects']) && is_array($request['multiselects'])){
					foreach($request['multiselects'][$campo['key']] as $req){
						$pdValues[$campo['key']][] = $req;
					}
				}
			} else {
				if($val != ''){
					$pdValues[$campo['key']] = $val;
				}
			}

			if($campo['key'] == 'ano_de_fabricacion' && $val != ''){
				$prod->anio = (int) trim($request[$campo['key']]);
				$prod->save();
			}

		}

//		$pd = $prod->ProductoItems->first();
		$prodItem->dimension = [
			'lng' => (int) str_replace('_sb_', '', $session['longitud']),
			'wdt' => (int) str_replace('_sb_', '', $session['ancho']),
			'hgt' => (int) str_replace('_sb_', '', $session['alto']),
			'wgt' => (int) str_replace('_sb_', '', $session['peso']),
			'pal' => (int) str_replace('_sb_', '', $session['palet']),
		];
		$prodItem->definicion = $pdValues;
		$prodItem->save();

		$prodItem->Owner()->associate($usuario);
		$prodItem->save();

		// ---------- IMAGENES

		$relativePath = "app".DIRECTORY_SEPARATOR."published".DIRECTORY_SEPARATOR.$prod->id;
		$destinationPath = storage_path($relativePath);

		if(!is_dir($destinationPath)){
			//$mkdir = mkdir($destinationPath, 0755);
			$mkdir = File::makeDirectory($destinationPath, 0755, true);
		}

		// ----------

		$imagenes = $session['imagenes'];

		$missingImages = [];
		foreach($imagenes as $imagen){

			if(File::exists(storage_path($imagen['ruta']))){
				File::move(storage_path($imagen['ruta']), $destinationPath.DIRECTORY_SEPARATOR.$imagen['filename']);

				$imagen = Imagen::create([
					'idGaleria' => $galeria->id,
//				'ruta' => $imagen['ruta'],
					'ruta' => $relativePath.DIRECTORY_SEPARATOR.$imagen['filename'],
					'filename' => $imagen['filename'],
					'mimetype' => $imagen['mimetype'],
				]);
			} else {
				$missingImages[] = storage_path($imagen['ruta']);
			}
		}

		if(count($missingImages) > 0){
			$imagenesStr = implode('<br>', $missingImages);
			$this->reportDev("Imagenes faltantes, producto:{$prod->id}, token:{$session['id']}, imagenes: <br> {$imagenesStr} ");
		}

		// ----------

//		$request->session()->flush();
		session(['pubsess' => 'none']);
		$request->session()->forget('pubsess');

		$response = Array(
			'result' => true,
			'message' => __('messages.record_created'),
			'data' => [
				'pubId' => $pub->id
			]
		);

		return response()->json($response);
	}

	public function getCards(Request $request){
		$this->validate($request, [
			'idUser' => 'required|numeric',
		]);

		$usuario = Usuario::find($request['idUser']);

		if(trim($usuario->getStripeId()) == ''){
//			Stripe::setApiKey('sk_test_mntDijCBJikZmtWTWuPC3eB5');	// MZ
			Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB
			Customer::create([
				'email' => $usuario->username,
				'source' => [
					'object' => 'card',
					'number' => '',
				]
			]);
		}

	}

	public function checkout(Request $request){

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		return view('frontend.pages.checkout', [
			'categorias' => $categorias,
//			'paymentId' => $paymentId,
		]);
	}

	/**
	 * @deprecated
	 */
	public function payment_dibs(Request $request) {

		if($request->isMethod('post')) {

			$checkoutKey = [
				'checkoutKey' => 'test-secret-key-92b3761f53e0493e8206147a432e4412'
			];

			$ch = curl_init('https://test.api.dibspayment.eu/v1/payments');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getDataString());
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Accept: application/json',
					'Authorization: '.$checkoutKey['checkoutKey'])
			);
			$result = curl_exec($ch);

			logger($result);
			$data = json_decode($result, true);

			$dataPlus = array_merge($data, $checkoutKey);
			logger($dataPlus);

			$response = [
				'result' => true,
				'message' => '',
				'data' => $dataPlus
			];
			return response()->json($response);
		}


		return view('frontend.pages.payment_dibs');

	}

	private function getDataString() {
		$res = [
			"order" => [
				"items" => [
					[
						"reference" => "13",
						"name" => "testproduct 1",
						"quantity" => 2,
						"unit" => "pcs",
						"unitPrice" => 48648,
						"taxRate" => 2500,
						"taxAmount" => 24324,
						"grossTotalAmount" => 121620,
						"netTotalAmount" => 97296
					],[
						"reference" => "21",
						"name" => "testproduct 2",
						"quantity" => 2,
						"unit" => "kg",
						"unitPrice" => 111840,
						"taxRate" => 2500,
						"taxAmount" => 55920,
						"grossTotalAmount" => 279600,
						"netTotalAmount" => 223680
					]
				],
				"amount" => 401220,
				"currency" => "sek",
				"reference" => "Demo Order"
			],
			"checkout" => [
				"url" => "https://www2.expoindustri.com/checkout",
				"termsUrl" => "https://www2.expoindustri.com/toc",
//				"shippingCountries" => [
//					[
//						"countryCode" => "SWE"
//					],[
//						"countryCode" => "NOR"
//					]
//				],
				"consumerType" => [
					"supportedTypes" => [ "B2C", "B2B" ],
					"default" => "B2C"
				]
			],

//You can extend the datastring with optional webhook-parameters for status such as "payment.created". Click for more info
//			"notifications" => [
//				"webhooks" => [
//					[
//						"eventName" => "payment.created",
//						"url" => "string",
//						"authorization" => "authorizationKey"
//					]
//				]
//			]
		];


		return json_encode($res);
	}

	public function getCustomerCards(Request $request) {

		$this->validate($request, [
			'customerToken' => 'required|numeric',
		]);

		$usuario = Usuario::findOrFail((int) $request['customerToken']);
		$token = $usuario->getStripeId();

		// ---------- STRIPE THING

		Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB

		if(trim($usuario->getStripeId()) != ''){
			$customer = Customer::retrieve($usuario->getStripeId());

			if($customer->sources->total_count > 0){
				$response = Array(
					'result' => true,
					'message' => __('messages.'),
					'data' => $customer->sources->data
				);
			} else {
				$response = Array(
					'result' => false,
					'message' => __('messages.no_cards_available'),
					'data' => $customer->sources->data
				);
			}


		} else {
			$response = Array(
				'result' => true,
				'message' => __('messages.no_cards_available'),
				'data' => []
			);
		}

		return response()->json($response);


//		$charge = Charge::create([
////			'amount' => $priceStripe,
////					'currency' => 'sek',
//			'currency' => $request['currency'],
//			'description' => 'Announcement fee',
////					'customer' => $usuario->idStripe,
////					'source' => $request['token'],
//			'customer' => $usuario->getStripeId(),
//		]);
//
//		$transaccion = new Transaccion([
//			'idUsuario' => $usuario->id,
//			'idStripeCustomer' => $charge->customer,
//			'idStripeTransaction' => $charge->id,
//			'producto' => $charge->description,
//			'monto' => $charge->amount,
//			'moneda' => $charge->currency,
//			'estado' => $charge->status
//		]);

	}

}
