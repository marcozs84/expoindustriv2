<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Calculadora;
use App\Models\Categoria;
use App\Models\CategoriaDefinition;
use App\Models\Configuracion;
use App\Models\Cotizacion;
use App\Models\CotizacionEsquema;
use App\Models\Destino;
use App\Models\Favorito;
use App\Models\Pais;
use App\Models\Publicacion;
use App\Models\Subscripcion;
use App\Models\Transaccion;
use App\Models\Transporte;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class PublicAnnounceController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		return $this->show_producto($id);
	}

	public function list_metas() {

		$pubs = Publicacion::with([
			'Producto.ProductoItem.Traducciones',
			'Producto.Categorias.Traduccion',
			'Producto.Categorias.Padre.Traduccion',
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Padre',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda'
		])->get();

		$global_localization = App::getLocale();

		foreach($pubs as $pub) {
			try{
				if( $pub->Producto ) {
					print $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] . ' ' . $pub->Producto->Categorias[0]->Traduccion[$global_localization] . ' ' . $pub->Producto->Marca->nombre . ' ' . $pub->Producto->Modelo->nombre.'<br>';
				} else {
					print "error: ". $pub->id."<br>";
				}

			} catch(Throwable $e) {
				print $pub->id;
				print $e->getMessage();
			}
		}


	}

	public function show_seo($categoria, $subcat, $marca, $modelo, $idp) {
		return $this->show_producto($idp);
	}

	private function show_producto($id) {
		$ceId = 0;

		$pub = Publicacion::with([
			'Producto.ProductoItem.Traducciones',
			'Producto.Categorias.Traduccion',
			'Producto.Categorias.Padre.Traduccion',
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Padre',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda'
		])
			->findOrFail($id);

		if($pub->estado != Publicacion::ESTADO_APPROVED){
			$glc = App::getLocale();
			return redirect()->route("public.{$glc}.search");
		}

		$esquema = null;

		if(!$pub){

			$subcat = $pub->Producto->Categorias()->first();
			$producto = $pub->Producto;
			$cat = $subcat->Padre;

			switch($cat->id){
				case Categoria::INDUSTRIAL:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::TRANSPORTE:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::EQUIPO:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::AGRICOLA:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::MONTACARGA:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				case Categoria::OTROS:
					$esquema = CotizacionEsquema::where('idCategoria', $producto->idCategoria);
					break;
				default:
					$esquema = null;
			}

			$ce = CotizacionEsquema::find(1);
		}


		// ----------

		$visits = session('visits', []);
		if(!in_array($id, $visits)) {
			$pub->visitas++;
			$pub->save();
			$visits[] = $id;
			session(['visits' => $visits]);
		}

		// ----------


		// ----------

		$def = $pub->Producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		$idCategoria = $pub->Producto->Categorias[0]->id;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$lang = App::getLocale(); //session('lang', 'en');

		$primaryData = [];
		foreach($primaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				if(in_array($value['type'], ['text', 'multiline'])){
					if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
						if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
							$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
							if(trim($value['value']) == ''){
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
				} else {
					$value['value'] = $def[$value['key']];
				}
				$value['html'] = '';
				$primaryData[] = $value;
			}
		}

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		$secondaryData = [];
		foreach($secondaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				if(in_array($value['type'], ['text', 'multiline'])){
					if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
						if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
							$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
							if(trim($value['value']) == ''){
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
				} else {
					$value['value'] = $def[$value['key']];
				}
				$value['html'] = '';
				$secondaryData[] = $value;
			}
		}

		// ---- calculadora

		$calc = $pub->Producto->ProductoItem->Calculadora;
		if(!$calc){
			$calc = new Calculadora();
			$calc->informe_tecnico = 0;
			$calc->seguro_extra = 0;
//			$calc->contenedor_compartido = 0;
//			$calc->contenedor_exclusivo = 0;
//			$calc->envio_abierto = 0;
//			$calc->destino_1 = 0;
//			$calc->destino_2 = 0;
//			$calc->destino_3 = 0;
//			$calc->destino_4 = 0;
//			$calc->destino_5 = 0;
//			$calc->destino_6 = 0;
			$pub->Producto->ProductoItem->Calculadora = $calc;
		}

		// ---------- FAVORITOS

		$esFavorito = false;
		if(Auth::guard('clientes')->user()){
			$user = Auth::guard('clientes')->user();
			$f = Favorito::where('idPublicacion', $pub->id)->where('idCliente', $user->Owner->id)->get();
			if($f->count() > 0){
				$esFavorito = true;
			}
		}

		// ---------- DESTINOS INDEX
		$destinosIndex = Destino::all();
		$destinosPub = $pub->Destinos->pluck('pivot.precio', 'id');
		$currencyFrom = 'usd';
		$currencyTo = session('current_currency', 'usd');

/*
		if($currencyFrom != $currencyTo) {
			$llave = "tipo_cambio_{$currencyFrom}_{$currencyTo}";
			$exchange = Configuracion::where('llave', $llave)->first()->valor;
			foreach($destinosPub as $key => $value){
				$destinosPub[$key] = round($value / $exchange, 2);
			}
		}
*/

		foreach($destinosPub as $key => $value){
			$destinosPub[$key] = $this->exchange('usd', null, $value);
		}

		$destinosHabId = $pub->Destinos->pluck('id')->toArray();

		// ---------- DESTINOS

		$paises_destino = Pais::with([
			'Destinos' => function($q) use($destinosHabId){
				$q->where('estado', 1);
				$q->whereIn('id', $destinosHabId);
			}
		])
			->whereHas('Destinos', function($q) use($destinosHabId){
				$q->where('estado', 1);
				$q->whereIn('id', $destinosHabId);
			})
			->where('esDestino', 1)
			->get();
		$destinosAr = [];
		foreach($paises_destino as $pais){
			if(!isset($destinosAr[$pais->codigo])){
				$destinosAr[$pais->codigo] = [
					'text' => $pais->nombre,
					'subs' => []
				];
			}

			$destinos = $pais->Destinos;
			foreach($pais->Destinos as $destino){
				$destinosAr[$pais->codigo]['subs'][] = [
					'value' => $destino->id,
					'price' => $destino->precio_publicacion,
					'text' => $destino->nombre
				];
			}
		}

		// ---------- TRANSPORTE
		$transportesIndex = Transporte::with(
			'Traduccion'
		)->whereEstado(1)->get();
		$transportesPub = $pub->Transportes->pluck('pivot.precio', 'id');
		$transportesPubListbb = $pub->Transportes->keyBy('id');
/*
		$currencyFrom = 'usd';
		$currencyTo = session('current_currency', 'usd');
		if($currencyFrom != $currencyTo) {
			$llave = "tipo_cambio_{$currencyFrom}_{$currencyTo}";
			$exchange = Configuracion::where('llave', $llave)->first()->valor;
			foreach($transportesPub as $key => $value){
				$transportesPub[$key] = round($value / $exchange, 2);
			}
		}
*/
		foreach($transportesPub as $key => $value){
			$transportesPub[$key] = $this->exchange('usd', null, $value);
		}
		$destinosHabId = $pub->Destinos->pluck('id')->toArray();

		// ---------- PRECIO INFORME TECNICO
/*
		if($currencyFrom != $currencyTo) {
			$precio_informe = round($pub->Producto->ProductoItem->Calculadora->informe_tecnico / $exchange, 2);
		} else {
			$precio_informe = $pub->Producto->ProductoItem->Calculadora->informe_tecnico;
		}
*/
		$precio_informe = $this->exchange('usd', null, $pub->Producto->ProductoItem->Calculadora->informe_tecnico);

		// ---------- PRECIO SEGURO
/*
		if($currencyFrom != $currencyTo) {
			$precio_seguro = round($pub->Producto->ProductoItem->Calculadora->seguro_extra / $exchange, 2);
		} else {
			$precio_seguro = $pub->Producto->ProductoItem->Calculadora->seguro_extra;
		}
*/
		$precio_seguro = $this->exchange('usd', null, $pub->Producto->ProductoItem->Calculadora->seguro_extra);

		// ---------- PRECIO MANIPULEO

		$tasa_tramite = $this->exchange('usd', null, $pub->Producto->ProductoItem->Calculadora->tasa_tramite);

		$galeria = $pub->Owner->Owner->Proveedor->Galerias->first();


		$imagen = null;
		$logoUrl = null;
		$tieneImagen = false;
		if($galeria && $galeria->Imagenes->count() > 0){
			$imagen = $galeria->Imagenes->first();
			if($imagen) {
				$tieneImagen = true;
			}
			$logoUrl = '/company_logo/'.$imagen->filename;
		}

		$subscripciones = Subscripcion::where('idUsuario', $pub->Owner->id)
			->where('fechaDesde', '<', Carbon::today())
			->where('fechaHasta', '>', Carbon::today())
			->get();

		$prov = $pub->Owner->Owner->Proveedor;
		if( $subscripciones->count() > 0 ) {
			if( $prov->tieneStore != 1) {
				$prov->tieneStore = 1;
				$prov->save();
			}
		} else {
			if ( $prov->tieneStore != 0 ) {
				$prov->tieneStore = 0;
				$prov->save();
			}
		}

//		$enableStripe = ( env('APP_ENV') == 'local' ) ? false : true;
		$enableStripe = false;


		// ---------- SEO
		$seo_metas = [];
		$meta_description = '';
		if( $pub->Producto ) {
			$global_localization = App::getLocale();
			$keywords = new \stdClass();
			$keywords->name = 'keywords';
			$keywords->content = $pub->Producto->getKeywords();
			$description = new \stdClass();
			$description->name = 'description';
			$description->content = $pub->Producto->getKeywords();
			$meta_description = $description->content;

			array_push($seo_metas, $keywords);
			array_push($seo_metas, $description);
		}

//		$values = [];
//		foreach($primaryData as $pd ) {
//			$values[] = $pd['value'];
//		}
//		$meta_description = implode(', ', $values);

		$nombre = $resumen = $descripcion = '';

		if ( $pub->Producto->ProductoItem->idTipoImportacion == 1 ) {
			$view = 'frontend.pages.producto_import';

			$traducciones = $pub->Producto->Traducciones;
			$nombre_ds = $traducciones->where('campo', 'nombre')->first();
			if( $nombre_ds ) {
				$nombre = $nombre_ds->$lang;
			}
			$resumen_ds = $traducciones->where('campo', 'resumen')->first();
			if( $resumen_ds ) {
				$resumen = $resumen_ds->$lang;
			}
			$descripcion_ds = $traducciones->where('campo', 'descripcion')->first();
			if( $descripcion_ds ) {
				$descripcion = $descripcion_ds->$lang;
			}

		} else {
//			$view = 'frontend.pages.producto';
			$view = 'frontend.pages.producto_nocalc';
		}


		// ----------

		$lang = App::getLocale();
		$traduccion = $prov->Traducciones->where('campo', 'descripcion')->first();
		$companyDesc = '';
		if( $traduccion ) {
			$companyDesc = $traduccion->$lang;
		}

		return view($view, [
			'idPub' => $id,
			'esquema' => $esquema,
			'pub' => $pub,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'esFavorito' => $esFavorito,
			'paises_destino' => $paises_destino,
			'destinos' => $destinosAr,
			'destinosIndex' => $destinosIndex,
			'destinosPub' => $destinosPub,
			'transportesIndex' => $transportesIndex,
			'transportesPub' => $transportesPub,
			'transportesPubList' => $transportesPubListbb,
			'precio_informe' => $precio_informe,
			'precio_seguro' => $precio_seguro,
			'tasa_tramite' => $tasa_tramite,
			'imagen' => $imagen,
			'logoUrl' => $logoUrl,
			'tieneImagen' => $tieneImagen,
			'enableStripe' => $enableStripe,
			'seo_metas' => $seo_metas,
			'meta_description' => $meta_description,

			'nombre' => $nombre,
			'resumen' => $resumen,
			'descripcion' => $descripcion,
			'companyDesc' => $companyDesc,
		]);
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }


	public function enviarCotizacion(Request $request, $id = 0){
		$this->validate($request, [
			'id' => 'required|numeric|min:1',
			'informe_tecnico' => 'required', 
			'seguro_extra' => 'required', 
			'contenedor_compartido' => 'required', 
			'contenedor_exclusivo' => 'required', 
			'envio_abierto' => 'required', 
			'se_direccion' => '',
			'se_pcode' => '',
			'idDestino' => 'required',
			'destino_1' => 'required',
			'destino_2' => 'required',
			'destino_3' => 'required',
			'destino_4' => 'required',
			'destino_5' => 'required',
			'destino_6' => 'required',
		]);

		$currencyFrom = 'usd';
		$currencyTo = session('current_currency', 'usd');
		if($currencyFrom != $currencyTo){
			$llave = "tipo_cambio_{$currencyFrom}_{$currencyTo}";
			$exchange = Configuracion::where('llave', $llave)->first()->valor;
		} else {
			$exchange = 1;
		}

		$informe_tecnico = $request['informe_tecnico'];
		$seguro_extra = $request['seguro_extra'];
		$contenedor_compartido = $request['contenedor_compartido'];
		$contenedor_exclusivo = $request['contenedor_exclusivo'];
		$envio_abierto = $request['envio_abierto'];
		$se_direccion = $request['se_direccion'];
		$se_pcode = $request['se_pcode'];
		$idDestino = $request['idDestino'];
		
		$cotDef = [];
		$calcD = [];
		$precioInforme = 0;

		$pub = Publicacion::findOrFail((int)$request['id']);
		$calc = $pub->Producto->ProductoItem->Calculadora;

		$fPrice = $pub->Producto->precio_publicacion;
//		$fPrice += ($request['informe_tecnico'] === 'true') ? $calc->informe_tecnico : 0;
		if($request['seguro_extra'] === 'true'){
			$precioSeguro = $this->exchange('usd', null, $calc->seguro_extra);
			$fPrice += $precioSeguro;
		}
		if($request['informe_tecnico'] === 'true'){
			$precioInforme = $this->exchange('usd', null, $calc->informe_tecnico);
//			$fPrice += $precioInforme;
		}

		$tasa_tramite = $this->exchange('usd', null, $calc->tasa_tramite);
		$fPrice += $tasa_tramite;

		$destinosPub = $pub->Destinos->pluck('pivot.precio', 'id')->toArray();
		$transportesPub = $pub->Transportes->pluck('pivot.precio', 'id');
		$idTransporte = 0;
		$transporteCot = [];
		foreach($transportesPub as $id => $precio){
			if($request['transporte_'.$id] === 'true'){
				$idTransporte = $id;
			}
		}

		$codigo_pais = Destino::find($idDestino)->Pais->codigo;
		if( $codigo_pais == 'se' ) {
			$idDestino = 9; // Destino 9: Tenhult idPais = 201: Sweden
			$idTransporte = Transporte::TIPO_TERRESTRE;
		}

		if( $idTransporte == 0 ) {
			$response = [
				'idTransporte' => [
					'Type of transport is required.'
				]
			];
			return response()->make( $response, 422 );
		}

		$precioDestino = $this->exchange('usd', null, $destinosPub[$idDestino]);
		if( isset( $transportesPub[$idTransporte] ) ) {
			$precioTransporte = $this->exchange('usd', null, $transportesPub[$idTransporte]);
		} else {
			$precioTransporte = 0;
		}


		$fPrice += $precioDestino;
		$fPrice += $precioTransporte;
		$fPrice += $tasa_tramite;

		$cotDef['moneda'] = $currencyTo;
		$cotDef['precio_original'] = $pub->Producto->precio_publicacion;
		$cotDef['precio_final'] = $fPrice;
		$cotDef['tasa_tramite'] = $tasa_tramite;

		$cotDef['destino'] = [
			'id' => (int) $idDestino,
			'precio' => (float) round($precioDestino, 2)
		];

		$cotDef['se_direccion'] = $se_direccion;
		$cotDef['se_pcode'] = $se_pcode;

		$cotDef['transporte'] = [
			'id' => (int) $idTransporte,
			'precio' => $precioTransporte
		];

		if($request['informe_tecnico'] === 'true'){
			$cotDef['informe_tecnico'] = true;
			$calcD['informe_tecnico'] = $precioInforme;
		}
		if($request['seguro_extra'] === 'true'){
			$calcD['seguro_extra'] = $precioSeguro;
		}
		$calcD['tasa_tramite'] = $tasa_tramite;
		$cotDef['calc'] = $calcD;

		$cot = new Cotizacion();
//		$cot->idCliente = Auth::guard('clientes')->user()->Owner->id;
		$cot->idCliente = Auth::guard('clientes')->user()->Cliente->id;
		$cot->definicion = $cotDef;
		$cot->idProducto = $pub->Producto->id;
		$cot->estado = Cotizacion::ESTADO_ACTIVE;
		//$cot->save();

		$cotS = $pub->Cotizaciones()->save($cot);


		// ---------- STRIPE CUSTOMER CREATION
		$usuario = Auth::guard('clientes')->user();

//		Stripe::setApiKey('sk_test_mntDijCBJikZmtWTWuPC3eB5');	// MZ
		Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB

		if(trim($usuario->getStripeId()) == ''){
			$customer = Customer::create([
				'email' => $usuario->username,
				'source' => $request['token'],
			]);

			$usuario->setStripeId($customer['id']);
			$usuario->save();

		}

		// ---------- PRECIO INFORME TECNICO

		$cotDef['transacciones'] = [];

		if($request['informe_tecnico'] === 'true'){
//			$charge = Charge::create([
//				'amount' => $precioInforme * 100,
////			'currency' => 'usd',
//				'currency' => session('current_currency', 'usd'),
//				'description' => "Pay Inf. Tec. Cotizacion: {$cot->id} Pub: {$pub->id}",
//				'customer' => $usuario->getStripeId(),
//			]);
//
//			$transaccion = Transaccion::create([
//				'idUsuario' => $usuario->id,
//				'idStripeCustomer' => $charge->customer,
//				'idStripeTransaction' => $charge->id,
//				'producto' => $charge->description,
//				'monto' => $charge->amount,
//				'moneda' => $charge->currency,
//				'estado' => $charge->status
//			]);

//			$cotDef['transacciones'][] = $transaccion->id;

			$cotS->definicion = $cotDef;
		}

		$cotS->save();


		// ----------

		$this->notificarCliente($cot);

		$response = [
			'result' => true,
			'message' => '',
			'data' => [
				'Producto->precioUnitario' => $pub->Producto->precioUnitario,
				'fPrice' => $fPrice,
				'request' => $request->toArray()
			]
		];
		return response()->json($response);
	}

	public function reenviarCotizacion(Request $request, $id){
		$cot = Cotizacion::with([
			'Cliente',
			'Cliente.Agenda'
		])->find($id);

		$this->notificarCliente($cot);
		return $cot;
	}

	public function viewCotizacion(Request $request, $idCotizacion) {
		$cot = Cotizacion::with([
			'Cliente',
			'Cliente.Agenda'
		])->find($idCotizacion);
		return $this->notificarCliente($cot, true);
	}

	public function notificarCliente(Cotizacion $cotizacion, $render = false){
		//$host = $_SERVER['SERVER_NAME'];
		$agenda = $cotizacion->Cliente->Agenda;
		$user = Auth::guard('clientes')->user();
		$email = $user->email;

		// TODO: traducir el/los mensaje que se envía al correo al momento de pedir cotizacion.

		$cot = $cotizacion;
		$pub = $cotizacion->Publicacion;

		$pub = Publicacion::with([
			'Producto.ProductoItem.Traducciones',
			'Producto.Categorias.Traduccion',
			'Producto.Categorias.Padre.Traduccion',
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Padre',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda'
		])
			->findOrFail($cotizacion->Publicacion->id);

//		$pub = \App\Models\Publicacion::find($id);
//		$cot = $pub->Cotizaciones->first()->definicion;
//		$cot = \App\Models\Cotizacion::find(7);

		// ----------

		$def = $pub->Producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		$idCategoria = $pub->Producto->Categorias[0]->id;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$lang = App::getLocale(); //session('lang', 'en');

		$primaryData = [];
		foreach($primaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				if(in_array($value['type'], ['text', 'multiline'])){
					if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
						if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
							$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
							if(trim($value['value']) == ''){
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
				} else {
					$value['value'] = $def[$value['key']];
				}
				$value['html'] = '';
				$primaryData[] = $value;
			}
		}

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		$secondaryData = [];
		foreach($secondaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				if(in_array($value['type'], ['text', 'multiline'])){
					if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
						if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
							$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
							if(trim($value['value']) == ''){
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
				} else {
					$value['value'] = $def[$value['key']];
				}
				$value['html'] = '';
				$secondaryData[] = $value;
			}
		}

//	return view('frontend.email.request_quote_inlined', [
//		return view('frontend.email.request_quote', [
//			'titulo' => 'Titulo',
//			'content' => 'Contenido',
//			'pub' => $pub,
//			'cot' => $cot,
//			'primaryData' => $primaryData
//		]);

		// ----------

		if($render == true) {
			return view('frontend.email.request_quote_inlined', [
				'titulo' => __('mail.preo_subject'),
				'auth_user' => $user,
				'pub' => $pub,
				'cot' => $cot,
				'primaryData' => $primaryData,
				'currency' => strtoupper(session('current_currency', 'usd')),
			]);
		}

		Mail::send('frontend.email.request_quote_inlined', [
			'titulo' => __('mail.preo_subject'),
			'auth_user' => $user,
			'pub' => $pub,
			'cot' => $cot,
			'primaryData' => $primaryData,
			'currency' => strtoupper(session('current_currency', 'usd')),
		], function ($m) use($user) {
			$m->from('info@expoindustri.com', 'ExpoIndustri');
			$m->to( $user->email );
			$m->bcc( 'marco.zeballos+quote_request@expoindustri.com', 'Marco Zeballos');
			$m->subject(__('mail.preo_subject'));
		});

//		if(env('APP_ENV') == 'local'){
//			Mail::send('frontend.email.request_quote_inlined', [
//				'titulo' => __('mail.preo_subject'),
//				'auth_user' => $user,
//				'pub' => $pub,
//				'cot' => $cot,
//				'primaryData' => $primaryData,
//				'currency' => strtoupper(session('current_currency', 'usd')),
//			], function ($m) use($user) {
//				$m->from('info@expoindustri.com', 'ExpoIndustri');
//				$m->to( $user->email );
//				$m->bcc( 'marco.zeballos+quote_request@expoindustri.com', 'Marco Zeballos');
//				$m->subject(__('mail.preo_subject'));
//			});
//		} else {
//			$headers = "From: " . strip_tags('info@expoindustri.com') . "\r\n";
//			$headers .= "Reply-To: ". strip_tags('info@expoindustri.com') . "\r\n";
//			$headers .= "BCC: marcozs84@gmail.com\r\n";
////			$headers .= "BCC: joakimbyren@hotmail.com\r\n";
//			$headers .= "MIME-Version: 1.0\r\n";
//			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//			$sent = mail($email, __('mail.preo_subject'), view('frontend.email.request_quote_inlined', [
//				'titulo' => __('mail.preo_subject'),
//				'auth_user' => $user,
//				'pub' => $pub,
//				'cot' => $cot,
//				'primaryData' => $primaryData,
//				'currency' => strtoupper(session('current_currency', 'usd')),
//			])->render(), $headers);
//		}


//		if($sent){
//			logger("enviado");
//		} else {
//			logger("no enviado");
//		}


		$response = Array(
			'result' => true,
			'message' => 'Registro exitoso.',
			'data' => []
		);

		return response()->json($response);
	}

	public function addFavorite(Request $request){
		$this->validate($request, [
			'idPublicacion' => 'required|numeric'
		]);

		$f = new Favorito();
		$f->idCliente = Auth::guard('clientes')->user()->Owner->id;
		$f->idPublicacion= $request['idPublicacion'];
		$f->save();

		$response = [
			'result' => true,
			'message' => 'success',
			'data' => [
				'user' => $f->toArray()
			]
		];
		return response()->json($response);
	}

	public function removeFavorite(Request $request){
		$this->validate($request, [
			'idPublicacion' => 'required|numeric'
		]);

		if(Auth::guard('clientes')->user()){
			$user = Auth::guard('clientes')->user();
			Favorito::where('idCliente', $user->Owner->id)
				->where('idPublicacion', $request['idPublicacion'])
				->delete();

			$response = [
				'result' => true,
				'message' => 'Removed succesfuly',
				'data' => [
					'user' => []
				]
			];
			return response()->json($response);
		} else {
			$response = [
				'result' => false,
				'message' => __('messages.addFavorite'),
				'data' => [
					'user' => []
				]
			];
			return response()->json($response);
		}
	}

	public function reportarCotizacion(){
		return view('frontend.pages.reportarCotizacion', [
			'invalidToken' => false,
			'titulo' => __('messages.preorder_reported'), //'Cotización reportada',
			'mensaje' => __('messages.preorder_reported_message'),
		]);
	}

	public function template_quote($id = 0){
		$pub = \App\Models\Publicacion::find($id);
		$cot = $pub->Cotizaciones->first()->definicion;
		$cot = \App\Models\Cotizacion::find(7);

		// ----------

		$def = $pub->Producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		$idCategoria = $pub->Producto->Categorias[0]->id;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$primaryData = [];
		foreach($primaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$primaryData[] = $value;
			}
		}

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		$secondaryData = [];
		foreach($secondaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$secondaryData[] = $value;
			}
		}

//	return view('frontend.email.request_quote_inlined', [
		return view('frontend.email.request_quote', [
			'titulo' => 'Titulo',
			'content' => 'Contenido',
			'pub' => $pub,
			'cot' => $cot,
			'primaryData' => $primaryData
		]);
	}
}
