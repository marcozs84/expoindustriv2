<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Cotizacion;
use App\Models\Favorito;
use App\Models\Lista;
use App\Models\ListaItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller {

	public function __construct(){
		$this->middleware('auth:clientes');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$estados = ListaItem::where('idLista', Lista::ORDEN_ESTADO)->get();
		$estadosMap = $estados->mapWithKeys(function($item, $key){
//			return $item->texto = __('messages.'.$item->texto);
			return [$item->valor => __('messages.'.$item->texto)];
		});
		return view('frontend.pages.private.order.index', [
			'estados' => $estadosMap->toArray()
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

    	$cotizacion = Cotizacion::with([
    		'Publicacion.Producto'
		])->findOrFail($id);

    	// ----------

		$estados = ListaItem::where('idLista', Lista::ORDEN_ESTADO)->get();
		$estadosMap = $estados->mapWithKeys(function($item, $key){
//			return $item->texto = __('messages.'.$item->texto);
			return [$item->valor => __('messages.'.$item->texto)];
		});

        return view('frontend.pages.private.order.show', [
			'order' => $cotizacion,
			'estados' => $estadosMap->toArray()
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    // ---------- CUSTOM

	public function ds(Request $request){
		if(isset($request['draw'])) {	// llamada DataTable
			/**
			 * Parametro $request['page'] no existe (no es provisto por el DataTable), pero es necesario para el paginador
			 * lo calculamos a partir de los parametros 'start' y 'length' provistos por el DataTable
			 */
			$request['page'] = 1 + ($request['start']/$request['length']);
		}

		$user = Auth::user();
		$builder = Cotizacion::with([
			'Publicacion.Producto.ProductoItem',
			'Publicacion.Producto.Categorias',
			'Publicacion.Producto.Marca',
			'Publicacion.Producto.Modelo',
		])->select(['*']);

		$searchValue = '';
		if(isset($request['draw'])) {	// llamada DataTable
			if($request['search']['value'] != ''){
				$searchValue = $request['search']['value'];
			}
		} else {						// llamada Select2
			if(isset($request['q'])){
				$searchValue = $request['q'];
			}
		}

		if($searchValue != ''){
			$builder->where('nombre', 'like', '%'.$searchValue.'%');
		}

		$builder->where('idCliente', Auth::guard('clientes')->user()->Owner->id);

		// ---------- FILTERS

		if(isset($request['filters'])){
			logger("filters: ". $request['filters']);
			$filters = explode('|', $request['filters']);
			if(in_array('favoritos', $filters)){
				logger("tiene favoritos");
				$idFavs = [];
				$user = Auth::guard('clientes')->user();
				$idFavs = Favorito::where('idCliente', $user->Owner->id)->get()->pluck('idPublicacion')->toArray();
				logger($idFavs);
				$builder->whereIn('id', $idFavs);
			}
		}

		if(isset($request['draw'])) {	// llamada DataTable
			$builder->orderBy('id', 'DESC');
			$ds = $builder->paginate($request['length'], ['*']);
		} else {		// llamada Select2
			return $builder->paginate($request['length'], ['*']);
		}

		/**
		 * Los campos: data, recordsTotal, recordsFiltered, draw
		 * son necesarios para que el datatable pueda
		 * interpretar la informacion de la tabla
		 */
		$response = [
			'result' => true,
			'message' => '',
			'data' => $ds->items(),
			'recordsTotal' => $ds->total(),
			'recordsFiltered' => $ds->total(),
			'draw' => $request['draw']
		];

		return response()->json($response);
	}
}
