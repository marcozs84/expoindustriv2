<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transporte;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransporteController extends Controller {

	public function __construct() {
		$this->middleware('auth:empleados');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		return view('admin.pages.transporte.index', []);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		return view('admin.pages.transporte.create', []);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$transporte = Transporte::findOrFail($id);

		return view('admin.pages.transporte.show', [
			'transporte' => $transporte,
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$transporte = Transporte::findOrFail($id);
		$traduccion = $transporte->Traduccion;



		if( $traduccion->count() == 0 ) {
			$transporte->Traduccion()->create([
				'campo' => 'nombre',
				'es' => '',
				'se' => '',
				'en' => '',
			]);
			$transporte->Traduccion()->create([
				'campo' => 'descripcion',
				'es' => '',
				'se' => '',
				'en' => '',
			]);

			$nombre_es = $nombre_se = $nombre_en = '';
			$desc_es = $desc_se = $desc_en = '';
		} else {
			$nombre = $traduccion->where('campo', 'nombre')->first();
			$nombre_es = $nombre->es;
			$nombre_se = $nombre->se;
			$nombre_en = $nombre->en;
			$desc = $traduccion->where('campo', 'descripcion')->first();
			$desc_es = $desc->es;
			$desc_se = $desc->se;
			$desc_en = $desc->en;
		}

		return view('admin.pages.transporte.edit', [
			'transporte' => $transporte,
			'nombre_es' => $nombre_es,
			'nombre_se' => $nombre_se,
			'nombre_en' => $nombre_en,
			'desc_es' => $desc_es,
			'desc_se' => $desc_se,
			'desc_en' => $desc_en,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
