<?php

namespace App\Http\Controllers\Admin;

use App\Models\Destino;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Pais;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DestinoController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		return view('admin.pages.destino.index', []);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$paises = Pais::get()->pluck('nombre', 'id')->toArray();
		$monedas = ListaItem::listado(Lista::MONEDA)->get()->pluck('texto', 'valor');
		return view('admin.pages.destino.create', [
			'paises' => $paises,
			'monedas' => $monedas,
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$destino = Destino::findOrFail($id);

		return view('admin.pages.destino.show', [
			'destino' => $destino,
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$destino = Destino::findOrFail($id);
		$paises = Pais::get()->pluck('nombre', 'id')->toArray();
		$monedas = ListaItem::listado(Lista::MONEDA)->get()->pluck('texto', 'valor');

		return view('admin.pages.destino.edit', [
			'destino' => $destino,
			'paises' => $paises,
			'monedas' => $monedas,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
