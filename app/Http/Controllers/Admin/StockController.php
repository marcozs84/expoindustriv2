<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lista;
use App\Models\ListaItem;

class StockController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

		$disponibilidad = ListaItem::where('idLista', Lista::PRODUCTOITEM_DISPONIBILIDAD)->get();

		return view('admin.pages.stock.index', [
			'disponibilidad' => $disponibilidad->pluck('texto', 'valor')->toArray()
		]);

    }

}
