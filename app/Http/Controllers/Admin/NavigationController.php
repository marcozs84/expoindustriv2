<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orden;
use App\Models\Transaccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cotizacion;
use App\Models\Publicacion;
use App\Models\ConsultaExterna;

class NavigationController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    public function getCounters(Request $request) {
        $this->validate($request, [
            'llaves' => '',
        ]);


        $counts = [];

		if( isset($request['llaves']) && count($request['llaves']) > 0 ) {
			foreach ($request['llaves'] as $llave) {
				$oLlave = $llave;
				$llave = ucfirst($llave);
				$methodName = "get{$llave}Count";
				if ( method_exists($this, $methodName) ) {
					$counts[$oLlave] = $this->{$methodName}();
				} else {
					$counts[$oLlave] = 0;
				}
			}
		}

        $response = [
            'result' => true,
            'message' => 'Llaves encontradas',
            'data' => $counts,
        ];
        return response()->json($response);
    }

    private function getOrdenInboxCount() {
        return Orden::select('id')->where('idOrdenestado', Orden::ESTADO_ACTIVE)->count();
    }

    private function getQuoteInboxCount() {
        return Cotizacion::select('id')->where('estado', Cotizacion::ESTADO_ACTIVE)->count();
    }

    private function getAnnounceInboxCount() {
        return Publicacion::select('id')->where('estado', Publicacion::ESTADO_AWAITING_APPROVAL)->count();
    }

    private function getSearchAlertInboxCount() {
        return ConsultaExterna::select('id')->where('estado', ConsultaExterna::ESTADO_NEW)->count();
    }

    public function getPendingFakturas() {
		$data = Transaccion::with([
//			'Owner',
			'Usuario.Owner.Agenda',
		])
			->where('procesada', 0)
			->where('idTransaccionTipo', Transaccion::TIPO_FAKTURA)
			->orderBy('created_at', 'asc')->paginate(5, ['*']);

		$response = [
			'result' => true,
			'message' => 'Llaves encontradas',
			'data' => $data,
		];
		return response()->json($response);
	}
}
