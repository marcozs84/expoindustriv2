<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProveedorContacto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgendaController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

	public function index() {
		return view('admin.pages.agenda.index');
	}

	public function show( $id ) {

		$pc = ProveedorContacto::with([
			'Agenda',
			'Usuario'
		])->findOrFail($id);

		$obj = $pc->Proveedor;

		$descripcion_se = $descripcion_es = $descripcion_en = '';

		$traducciones = $obj->Traducciones;
		if( $traducciones && $traducciones->count() > 0 ) {
			if( $traducciones->where('campo', 'descripcion')->count() > 0 ) {
				$traduccion = $traducciones->where('campo', 'descripcion')->first();
				$descripcion_se = $traduccion->se;
				$descripcion_es = $traduccion->es;
				$descripcion_en = $traduccion->en;
			} else {
				$descripcion_se = $obj->descripcion;
			}
		} else {
			$descripcion_se = $obj->descripcion;
		}

		return view('admin.pages.agenda.show', [
			'pc' => $pc,
			'descripcion_se' => $descripcion_se,
			'descripcion_es' => $descripcion_es,
			'descripcion_en' => $descripcion_en,
		]);
	}
}
