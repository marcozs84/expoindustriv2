<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lista;
use App\Models\ListaItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ListaController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('admin.pages.lista.index', [
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    	return view('admin.pages.lista.create', [
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$lista = Lista::findOrFail($id);

		$listaItems = ListaItem::where('idLista', $id)->orderBy('orden')->get();

		return view('admin.pages.lista.show', [
			'lista' => $lista,
			'listaItems' => $listaItems,
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $lista = Lista::findOrFail($id);
		$listaItems = ListaItem::where('idLista', $id)->orderBy('orden')->get();

		return view('admin.pages.lista.edit', [
			'lista' => $lista,
			'listaItems' => $listaItems,
		]);
    }
    public function edit_alt($id) {

        $lista = Lista::findOrFail($id);
		$listaItems = ListaItem::where('idLista', $id)->orderBy('orden')->get();

		return view('admin.pages.lista.edit_alt', [
			'lista' => $lista,
			'listaItems' => $listaItems,
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
