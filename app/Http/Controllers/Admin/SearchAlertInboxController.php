<?php

namespace App\Http\Controllers\Admin;

use App\Models\ConsultaExterna;
use App\Models\Lista;
use App\Models\ListaItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SearchAlertInboxController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$counterQuotes = ConsultaExterna::all()->count();
		$counterQuotesNew = ConsultaExterna::where('estado', ConsultaExterna::ESTADO_NEW)->count();

		$ceEstados = ListaItem::listado(Lista::CONSULTA_EXTERNA_ESTADO)->get()->pluck('texto', 'valor')->toArray();

		$ceTipos = ListaItem::listado(Lista::CONSULTA_EXTERNA_TIPO)->get()->pluck('texto', 'valor')->toArray();

		return view('admin.pages.searchAlertInbox.index', [
			'counterConsultaExternas' => $counterQuotes,
			'ceEstados' => $ceEstados,
			'ceTipos' => $ceTipos,
			'counterQuotesNew' => $counterQuotesNew
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $ce = ConsultaExterna::findOrFail($id);

		$ceEstados = ListaItem::listado(Lista::CONSULTA_EXTERNA_ESTADO)->get()->pluck('texto', 'valor')->toArray();

        return view('admin.pages.searchAlertInbox.show', [
        	'mod' => $ce,
			'ceEstados' => $ceEstados
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

	public function enviarMensajeLibre(Request $request){

		$this->validate($request, [
			'asunto' => 'required',
			'mensaje' => 'required',
			'idSearchAlert' => 'required'
		]);

		$ce = ConsultaExterna::find($request['idSearchAlert']);
		$email = 'marcozs84@gmail.com';

		$content = $request['mensaje'];
		$subject = $request['asunto'];
//		$subject = 'Consulta maquinaria';

		$consultor = Auth::guard('empleados')->user();
		$email = $ce->email;

		Mail::send('frontend.email.template_light', [
			'titulo' => $subject,
			'content' => $content,
			'local' => session('visitor_location', 'en')
		], function ($m) use ( $email, $subject, $consultor ) {
			$m->from('info@expoindustri.com', 'ExpoIndustri');
//			$m->to( 'marco.zeballos@expoindustri.com' );
			$m->to( $email );
			$m->bcc( 'marco.zeballos+formcontacto@expoindustri.com', 'Marco Zeballos' );
			$m->replyTo( $consultor->username, $consultor->Owner->Agenda->nombres_apellidos);
			$m->subject( $subject );
		});

//		if(env('APP_ENV') == 'local'){
//			Mail::send('frontend.email.template_light', [
//				'content' => $content,
//				'titulo' => $subject
//			], function ($m) use($user, $subject, $email) {
//				$m->from($user->username, $user->Owner->Agenda->nombres_apellidos);
//				$m->to($email);
//				$m->bcc('marcozs84@gmail.com', 'Marco Zeballos');
//				$m->subject($subject);
//			});
//		} else {
//			$headers = "From: " . strip_tags($user->username) . "\r\n";
//			$headers .= "Reply-To: ". strip_tags($user->username) . "\r\n";
//			$headers .= "BCC: marcozs84@gmail.com\r\n";
//			$headers .= "MIME-Version: 1.0\r\n";
//			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//			$sent = mail($email, $subject, view('frontend.email.template_light', ['content' => $content, 'titulo' => $subject])->render(), $headers);
//		}

		$ce->estado = ConsultaExterna::ESTADO_UNDER_REVIEW;
		$ce->idUsuarioRevisor = $consultor->id;
		$ce->save();

		$response = Array(
			'result' => true,
			'message' => 'Mensaje enviado correctamente.',
			'data' => []
		);

		return response()->json($response);

	}
}
