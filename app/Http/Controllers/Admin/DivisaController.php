<?php

namespace App\Http\Controllers\Admin;

use App\Models\Configuracion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DivisaController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

	public function index() {
		return view('admin.pages.divisa.index');
	}

	public function edit($id) {
		$configuracion = Configuracion::find($id);

		return view('admin.pages.divisa.edit', [
			'configuracion' => $configuracion,
		]);
	}
}
