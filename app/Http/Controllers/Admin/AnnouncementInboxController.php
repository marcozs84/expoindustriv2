<?php

namespace App\Http\Controllers\Admin;

use App\Models\Calculadora;
use App\Models\CategoriaDefinition;
use App\Models\Destino;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Producto;
use App\Models\Publicacion;
use App\Models\PublicacionTransporte;
use App\Models\Role;
use App\Models\Traduccion;
use App\Models\Transporte;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AnnouncementInboxController extends Controller
{
	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

		$id = 0;
    	if(session('idMessage', 'none') != 'none'){
    		$id = session('idMessage');
		}

		$lang = App::getLocale();

		$pubEstados = ListaItem::listado(Lista::PUBLICACION_ESTADO)->get()->pluck('texto', 'valor')->toArray();
		foreach($pubEstados as $idEstado => &$estado ) {
			$estado = __('messages.'.$estado);
		}

		// ---------- Vendedores
		$builder = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			});

		if ( Gate::denies( 'admin.gasto.adminTerceros' ) ) {
			$builder->where( 'id', Auth::user()->id );
		} else {
			$builder->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR);
		}

		$vendedores = $builder->get();

        return view('admin.pages.announcementInbox.index', [
			'idMessage' => $id,
			'lang' => $lang,
			'pubEstados' => $pubEstados,
			'vendedores' => $vendedores,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id = 0, $lang='--') {

		if($lang == '--'){
			$lang = App::getLocale();
		} else {
			App::setLocale(session('lang', $lang));
		}

    	if(!$request->ajax()){
			if($id > 0 ) {
				$lang = App::getLocale();
				$pubEstados = ListaItem::listado(Lista::PUBLICACION_ESTADO)->get()->pluck('texto', 'valor')->toArray();

				// ---------- Vendedores
				$builder = Usuario::with([
					'Roles',
					'Owner.Agenda',
				])
					->whereHas('Roles', function($q) {
						$q->where('id', Role::VENDEDOR);
					});

				if ( Gate::denies( 'admin.gasto.adminTerceros' ) ) {
					$builder->where( 'id', Auth::user()->id );
				} else {
					$builder->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR);
				}

				$vendedores = $builder->get();

				return view('admin.pages.announcementInbox.index', [
					'idMessage' => $id,
					'lang' => $lang,
					'pubEstados' => $pubEstados,
					'vendedores' => $vendedores,
				]);
			}
//			return redirect()->route('admin.announcementInbox.index')->with('idMessage', $id);
		}

		$pub = Publicacion::with([
			'Producto.ProductoItem.Disponibilidad',
			'Producto.ProductoItem.Traducciones',
			'Producto.Categorias',
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Categorias.Padre',
			'Producto.Galerias.Imagenes',
			'Owner.Owner.Agenda',
			'Transacciones'
		])
			->findOrFail($id);

		// ----------

		$primaryData = [];
		$secondaryData = [];

		$def = $pub->Producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		if( $pub->Producto->Categorias->count() > 0 ) {
			$idCategoria = $pub->Producto->Categorias[0]->id;
			$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
			if($cd->count() == 0){
				$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
			}
			if($cd->count() > 1){
				$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
			}
			$campos = $cd->first()->campos;

			$primaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
			});

			foreach($primaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
							if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = 'SIN TRADUCCION';
								}
							} else {
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$primaryData[] = $value;
				}
			}

			$secondaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
			});

			foreach($secondaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($pub->Producto->ProductoItem->Traducciones != null && $pub->Producto->ProductoItem->Traducciones->count() > 0){
							if($pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $pub->Producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = 'SIN TRADUCCION';
								}
							} else {
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
//				} elseif(in_array($value['type'], ['titulo1', 'titulo2'])) {
//					$value['value'] = $def[$value['key']];
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$secondaryData[] = $value;
				}
			}
		}


		// ----------
		$calc = $pub->Producto->ProductoItem->Calculadora;
		if(!$calc){
			$calc = new Calculadora();
			$calc->informe_tecnico = 0;
			$calc->seguro_extra = 0;
			$calc->contenedor_compartido = 0;
			$calc->contenedor_exclusivo = 0;
			$calc->envio_abierto = 0;
			$calc->tasa_tramite = 0;
		}

		// ---------- DESTINOS
		$destinos = Destino::all();
		$destinosPub = $pub->Destinos->pluck('pivot.precio', 'id');

		// ---------- TRANSPORTE
		$transportes = Transporte::all();
		$transportePub = $pub->Transportes->pluck('pivot.precio', 'id');

		$pubEstados = ListaItem::listado(Lista::PUBLICACION_ESTADO)->get()->pluck('texto', 'valor')->toArray();

		// ----------
		$disponibilidad = ListaItem::listado(Lista::PRODUCTOITEM_DISPONIBILIDAD)->get()->pluck('texto', 'valor')->toArray();

		if( $pub->Producto->ProductoItem->idTipoImportacion == 1 ) {
			$view = 'admin.pages.announcementInbox.show_import';
		} else {
			$view = 'admin.pages.announcementInbox.show';
		}

		return view($view,[
			'pub' => $pub,
			'producto' => $pub->Producto,
			'calc' => $calc,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'pubEstados' => $pubEstados,
			'destinos' => $destinos,
			'destinosPub' => $destinosPub,
			'transportes' => $transportes,
			'transportePub' => $transportePub,
			'lang' => $lang,
			'disponibilidad' => $disponibilidad,
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

		$this->validate($request, [
//			'precio_final' => 'required|numeric',
//			'precio_usd' => 'required|numeric',
			'precioFijo' => 'required|numeric',
			'estado' => 'required',
			'disponibilidad' => 'required',
		]);

		if($request['precioFijo'] == 1) {
			$this->validate($request, [
				'precio_usd' => 'required|numeric',
				'precio_eur' => 'required|numeric',
				'precio_sek' => 'required|numeric',
			]);
		}

		$pub = Publicacion::with([
			'Producto.ProductoItem.Traducciones',
		])->findOrFail($id);

		// ----------

		$calc = $pub->Producto->ProductoItem->Calculadora;
		if(!$calc){
			$response = [
				'Calculadora' => [
					'No se han asignado valores para la calculadora, debe guardar primero la calculadora.'
				]
			];
			return response()->make($response, 422);
		}

		switch($request['estado']){
			case 'rejected':
				$pub->estado = Publicacion::ESTADO_REJECTED;
				break;
			case 'approved':
				$pub->estado = Publicacion::ESTADO_APPROVED;
				break;
			case 'disabled':
				$pub->estado = Publicacion::ESTADO_DISABLED;
				break;
		}

		$pub->save();

		$prod = $pub->Producto;
//		$prod->precioUnitario = $request['precio_final'];
//		$prod->monedaUnitario = 'usd';
		$prod->precioFijo = $request['precioFijo'];
		$prod->precioSudamerica = $request['precioSudamerica'];
		$prod->precioFijoUsd = $request['precio_usd'];
		$prod->precioFijoEur = $request['precio_eur'];
		$prod->precioFijoSek = $request['precio_sek'];
		$prod->save();

		$prodItem = $prod->ProductoItem;
		$prodItem->idDisponibilidad = $request['disponibilidad'];
		$prodItem->save();

//		logger($pub->toArray());

		$response = Array(
			'result' => true,
			'message' => 'Registro guardado correctamente.',
			'data' => []
		);

		return response()->json($response);

    }

    public function updateCalculadora(Request $request, $id){


		$valFields = [
			'informe_tecnico' => 'required',
			'seguro_extra' => 'required',
			'tasa_tramite' => 'required',
//			'contenedor_compartido' => 'required',
//			'contenedor_exclusivo' => 'required',
//			'envio_abierto' => 'required',
		];

		// ---------- DESTINOS
		$destinos = Destino::whereEstado(1)->get();
		foreach($destinos as $destino){
			if($request['chkDest_'.$destino->id] == 1){
//				$valFields['destino_'.$destino->id] = 'required';
			}
		}

		// ---------- TRANSPORTES
		$transportes = Transporte::whereEstado(1)->get();
		foreach($transportes as $transporte){
			if($request['chkTrans_'.$transporte->id] == 1){
//				$valFields['transporte_'.$transporte->id] = 'required';
			}
		}

		$this->validate($request, $valFields);

		$pub = Publicacion::with([
			'Producto.ProductoItem',
		])->findOrFail($id);

		// ---------- DESTINOS
//		$dataSync = [];
//		foreach($destinos as $destino){
//			if($request['chkDest_'.$destino->id] == 1){
//				$dataSync[$destino->id] = ['precio' => $request['destino_'.$destino->id]];
//			}
//		}
//		if(count($dataSync) == 0){
//			$response = [
//				'Destinos' => [
//					'Es necesario que seleccione al menos 1 destino.'
//				]
//			];
//			return response()->make($response, 422);
//		}
//		$pub->Destinos()->sync($dataSync);

		// ---------- TRANSPORTES
//		$dataSyncT = [];
//		foreach($transportes as $transporte){
//			if($request['chkTrans_'.$transporte->id] == 1){
//				$dataSyncT[$transporte->id] = ['precio' => $request['transporte_'.$transporte->id]];
//			}
//		}
//		if(count($dataSyncT) == 0){
//			$response = [
//				'Transporte' => [
//					'Es necesario que seleccione al menos 1 tipo de transporte.'
//				]
//			];
//			return response()->make($response, 422);
//		}
//		$pub->Transportes()->sync($dataSyncT);

		// ----------

		$calc = $pub->Producto->ProductoItem->Calculadora;
		if(!$calc){
			logger("no tiene calculadora, creando...");
			$calc = $pub->Producto->ProductoItem->Calculadora()->save(new Calculadora());
		}

		$calc->informe_tecnico = $request['informe_tecnico'];
		$calc->seguro_extra = $request['seguro_extra'];
		$calc->tasa_tramite = $request['tasa_tramite'];
		$calc->contenedor_compartido = $request['contenedor_compartido'];
		$calc->contenedor_exclusivo = $request['contenedor_exclusivo'];
		$calc->envio_abierto = $request['envio_abierto'];

		if($calc->save()){
			$response = Array(
				'result' => true,
				'message' => 'Calculadora guardada correctamente.',
				'data' => []
			);

			return response()->json($response);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'Error al guardar la calculadora.',
				'data' => []
			);

			return response()->json($response);
		}
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
		if($id != 0){
			$ids = [$id];
		} else {
			$this->validate($request, [
				'ids' => 'required|array'
			]);
			$ids = $request['ids'];
		}

		$count = Publicacion::destroy($ids);

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) eliminado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => __('messages.delete_message_not', ['count' => $count, 'total' => count($ids)]), //'No se pudo eliminar el/los registro(s). Se eliminó : '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
    }

    // ---------- CUSTOM

	public function transporteDisponibilidad(Request $request, $idPublicacion, $idTransporte) {

		$transPub = PublicacionTransporte::where('idPublicacion', $idPublicacion)->where('idTransporte', $idTransporte)->first();

		if( !$transPub ) {
			$transPub = PublicacionTransporte::create([
				'idPublicacion' => $idPublicacion,
				'idTransporte' => $idTransporte,
				'precio' => 0,
			]);
		}

		if($request->isMethod('post')) {

			$ids = [];
			if( is_array( $request['idDestinos'] ) ) {
				foreach( $request['idDestinos'] as $idDestino ) {
					$ids[] = (int) $idDestino;
				}
			}

			$transPub->disponibilidad = json_encode( $ids );
			$transPub->save();
			$response = Array(
				'result' => true,
				'message' => 'Registro guardado correctamente.',
				'data' => $transPub->toArray()
			);

			return response()->json($response);
		}

		$destinosPub = [];
		if( $transPub->disponibilidad != '') {
			$destinosPub = json_decode($transPub->disponibilidad);
		}

		$destinos = Destino::all();
//		$transportes = Transaccion::all();
		return view('admin.pages.announcementInbox.transporte_disponibilidad', [
			'destinos' => $destinos,
			'idPublicacion' => $idPublicacion,
			'idTransporte' => $idTransporte,
			'destinosPub' => $destinosPub,
		]);
	}

	/**
	 * Aprueba los anuncios provistos.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function estado(Request $request) {
		$this->validate($request, [
			'ids' => 'required|array',
			'idEstado' => 'required'
		]);
		$ids = $request['ids'];
		$idEstado = (int)$request['idEstado'];

//		$publicaciones = Publicacion::whereIn('id', $ids)->get();

		$count = Publicacion::whereIn( 'id', $ids )->update( [ 'estado' => $idEstado ] );

		if($count == count($ids)){
			$result = Array(
				'result' => true,
				'message' => 'Registro(s) aprobado(s) correctamente.',
				'request' => $request->all()
			);
		} else {
			$result = Array(
				'result' => false,
				'message' => 'No se pudieron aprobar: '. $count .' de: '.count($ids), //'No se pudo eliminar el/los registro(s). Se eliminó : '.$count.' de: '.count($ids),
				'request' => $request->all()
			);
		}
		return response()->json($result);
	}
}
