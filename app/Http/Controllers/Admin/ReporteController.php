<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReporteController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Reporte de Ofertas / visitas realizadas por los vendedores
     *
     * @return \Illuminate\Http\Response
     */
    public function oferta() {

		$vendedores = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			})
			->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR)
			->get();


//		$arVendedores = [0 => 'Todos'] + $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id')->toArray();

		return view('admin.pages.reporte.reporteOferta', [
			'vendedores' => $vendedores
		]);
    }

	/**
	 * Reporte de Ofertas / visitas realizadas por los vendedores
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function venta() {

		$vendedores = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			})
			->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR)
			->get();





//		$arVendedores = [0 => 'Todos'] + $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id')->toArray();

		return view('admin.pages.reporte.reporteVenta', [
			'vendedores' => $vendedores
		]);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
