<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Resources\ProductoController as Resource_ProductoController;
use App\Models\Actividad;
use App\Models\ActividadTipo;
use App\Models\Calendario;
use App\Models\Categoria;
use App\Models\CategoriaDefinition;
use App\Models\Galeria;
use App\Models\Imagen;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Producto;
use App\Models\ProductoItem;
use App\Models\Tag;
use App\Models\Traduccion;
use App\Models\Ubicacion;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class ProductoController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

		$disponibilidad = ListaItem::where('idLista', Lista::PRODUCTOITEM_DISPONIBILIDAD)->get();

        return view('admin.pages.producto.index', [
        	'disponibilidad' => $disponibilidad->pluck('texto', 'valor')->toArray()
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idUsuario = 0) {

		$usuario = null;
		if($idUsuario > 0 ) {
			$usuario = Usuario::findOrFail($idUsuario);
		}

		$marcas = Marca::all()->pluck('nombre');

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		// ---------- CURRENCY

		$currency = ListaItem::where('idLista', Lista::MONEDA)->get();
		$moneda = $currency->pluck('texto', 'valor')->toArray();
		foreach($moneda as $k => $v){
			$moneda[$k] = __('messages.'.$v);
		}

		// ---------- TAGS
		$grouped_tags = Resource_ProductoController::get_grouped_tags();

        return view('admin.pages.producto.create', [
			'marcas' => $marcas,
			'categorias' => $categorias,
			'currency' => $currency,
			'moneda' => $moneda,
			'idUsuario' => $idUsuario,
			'usuario' => $usuario,
			'grouped_tags' => $grouped_tags,
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

		$this->validate($request, [
			'marca' => 'required',
			'modelo' => 'required',
			'idCategoria' => 'required',

			'idUsuario' => 'required',
			'precio' => 'required',
			'currency' => 'required',
			'precioVenta' => '',
			'currencyVenta' => '',
			'precioMinimo' => '',
			'currencyMinimo' => '',
			'precioEuropa' => '',
			'currencyEuropa' => '',
			'priceSouthamerica' => '',

			'pais' => 'required',
			'ciudad' => '',
			'codigo_postal' => '',
			'direccion' => '',

			'longitud' => '',
			'ancho' => '',
			'alto' => '',
			'peso_bruto_fix' => '',
			'paletizado' => '',
		]);

		$idCategoria = (int)$request['idCategoria'];
		$cds = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cds->count() > 1){
			$this->reportDev('Hay mas de un CategoriaDefinition para la categoria: '.$idCategoria);
		}

		$cd = $cds->first();

		$campos = $cd->campos;
		$validations = [];
		foreach($campos as $campo){
			if(isset($campo['validation'])){
				$validations[$campo['key']] = $campo['validation'];
			}
		}
		$this->validate($request, $validations);

		$idUsuario = (int)$request['idUsuario'];

		if($idUsuario <= 0 ) {
			$response = [
				'Usuario' => [
					'Usuario es requerido'
				]
			];
			return response()->make($response, 422);
		}



		// ---------- MARCA Y MODELO

		$marcas = Marca::where('nombre', trim($request['marca']))->get();
		if($marcas->count() > 1){
			$marca = $marcas->first();
			$this->reportDev('Se encontró mas de una marca coincidente con: '. $request['marca']);
		} elseif ($marcas->count() == 1){
			$marca = $marcas->first();
		} else {
			$marca = Marca::create(['nombre' => trim($request['marca'])]);
		}

		$idMarca = $marca->id;

		// ---------- MODELO

		$modelos = Modelo::where('idMarca', $idMarca)
			->where('nombre', trim($request['modelo']))
			->get();
		if($modelos->count() > 1){
			$modelo = $modelos->first();
			$this->reportDev('Se encontró mas de un modelo coincidente con: '. $request['modelo']. ' en la marcaID: '.$idMarca);
		} elseif ($modelos->count() == 1){
			$modelo = $modelos->first();
		} else {
			$modelo = Modelo::create(['idMarca' => $idMarca, 'nombre' => trim($request['modelo'])]);
		}

		$idModelo = $modelo->id;

		// ---------- USUARIO

		$usuario = Usuario::findOrFail($request['idUsuario']);


    	// ---------- PRODUCTO + PRODUCTO_ITEM

		$prod = new Producto();
		$prod->idMarca = $idMarca;
		$prod->idModelo = $idModelo;
		$prod->idProductoTipo = $request['idProductoTipo'];
		$prod->esNuevo = $request['esNuevo'];
//		$prod->precioMinimo = $request['precioMinimo'];
//		$prod->monedaMinimo = trim($request['currencyMinimo']);
		$prod->precioProveedor = $request['precio'];
		$prod->monedaProveedor = trim($request['currency']);
//		$prod->precioUnitario = $request['precioVenta'];
//		$prod->monedaUnitario = trim($request['currencyVenta']);
		$prod->precioEuropa = $request['precioEuropa'];
		$prod->monedaEuropa = trim($request['currencyEuropa']);
		$prod->precioSudamerica = ( isset($request['priceSouthamerica']) && $request['priceSouthamerica'] == 1 ) ? true : false;
//		$prod->save();
		$prod = $usuario->Productos()->save($prod);

		$prod->Categorias()->attach($idCategoria);
		$galeria = $prod->Galerias()->save(new Galeria());
		$prodItem = $prod->ProductoItem()->save(new ProductoItem());
		$prodItem->idDisponibilidad = ProductoItem::DISPONIBILIDAD_DISPONIBLE;
		$prodItem->save();

		$prodItem->Owner()->associate($usuario);
		$prodItem->save();

		$ubicacion = $prodItem->Ubicacion()->save(new Ubicacion([
			'pais' => trim($request['pais']),
			'ciudad' => trim($request['ciudad']),
			'cpostal' => trim($request['codigo_postal']),
			'direccion' => trim($request['direccion'])
		]));

		// ----------
		$campos = $cd->campos;
		$pdValues = [];
		foreach($campos as $campo){
			$val = trim($request[$campo['key']]);
			//TODO: Agregar validación y casting para tipo de campo numeric
			if($campo['type'] == 'multiselect'){
				$pdValues[$campo['key']] = [];
				if(isset($request['multiselects']) && is_array($request['multiselects'])){
					foreach($request['multiselects'][$campo['key']] as $req){
						$pdValues[$campo['key']][] = $req;
					}
				}
			} else {
				if($val != ''){
					$pdValues[$campo['key']] = $val;
				}
			}

			if($campo['key'] == 'ano_de_fabricacion' && $val != ''){
				$prod->anio = (int) trim($request[$campo['key']]);
				$prod->save();
			}
		}
		$prodItem->definicion = $pdValues;

		// ----------
		$prodItem->dimension = [
			'lng' => (int) $request['longitud'],
			'wdt' => (int) $request['ancho'],
			'hgt' => (int) $request['alto'],
			'wgt' => (int) $request['peso_bruto_fix'],
			'pal' => (int) $request['paletizado'],
		];
		$prodItem->save();

		// ---------- TAGS

		if( isset( $request['tags'] ) ) {
			$prod->Tags()->sync( $request['tags'] );
		}

		$response = Array(
			'result' => true,
			'message' => 'Producto creado.',
			'data' => $prod
		);

		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$producto = Producto::with([
			'ProductoItem.Ubicacion',
			'Categorias.Padre',
			'Marca',
			'Modelo',
			'Publicacion',
			'Galerias',
			'Owner.Owner.Agenda',
		])->findOrFail($id);

		$marcas = Marca::all()->pluck('nombre');

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		// ---------- CURRENCY

		$currency = ListaItem::where('idLista', Lista::MONEDA)->get();
		$moneda = $currency->pluck('texto', 'valor')->toArray();
		foreach($moneda as $k => $v){
			$moneda[$k] = __('messages.'.$v);
		}

		// ---------- FORM DEFINITION

		$primaryData = [];
		$secondaryData = [];
		$lang = 'es';

		$def = $producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		if( $producto->Categorias->count() > 0 ) {
			$idCategoria = $producto->Categorias[0]->id;
			$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
			if($cd->count() == 0){
				$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
			}
			if($cd->count() > 1){
				$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
			}
			$campos = $cd->first()->campos;

			$primaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
			});


			foreach($primaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0){
							if($producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = 'SIN TRADUCCION';
								}
							} else {
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$primaryData[] = $value;
				}
			}

			$secondaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
			});


			foreach($secondaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0){
							if($producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = 'SIN TRADUCCION';
								}
							} else {
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
//				} elseif(in_array($value['type'], ['titulo1', 'titulo2'])) {
//					$value['value'] = $def[$value['key']];
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$secondaryData[] = $value;
				}
			}
		}


		// ----------

		$gals = $producto->Galerias;
		$img_paths = [];
		foreach($gals as $gal ) {
			$imgs = $gal->Imagenes;
			foreach($imgs as $img) {
				$path = pathinfo( storage_path($img->ruta), PATHINFO_DIRNAME);
				$files = File::files($path);
				$findstr = explode('.', $img->filename)[0];
				foreach($files as $file) {
					if( preg_match("/{$findstr}/", $file) == 1 ) {
						$img_paths[ $img->id ][] = pathinfo($file, PATHINFO_BASENAME);
					}
				}
			}
		}

		if( (int)$producto->ProductoItem->idTipoImportacion === 1) {
			$view = 'admin.pages.producto.show_import';
		} else {
			$view = 'admin.pages.producto.show';
		}

		return view($view, [
			'producto' => $producto,
			'marcas' => $marcas,
			'categorias' => $categorias,
			'currency' => $currency,
			'moneda' => $moneda,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'img_paths' => $img_paths,
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

		$producto = Producto::with([
			'ProductoItem.Ubicacion',
			'Categorias.Padre',
			'Marca',
			'Modelo',
			'Publicacion',
			'Galerias',
			'Owner.Owner.Agenda',
		])->findOrFail($id);

		$marcas = Marca::all()->pluck('nombre');

		$categorias = Categoria::with([
			'Traduccion',
			'Subcategorias' => function($q){
				$q->select('id', 'nombre', 'idPadre');
			},
			'Subcategorias.Traduccion'
		])
			->select('id', 'nombre')
			->where('nivel', 0)
			->get();

		// ---------- CURRENCY

		$currency = ListaItem::where('idLista', Lista::MONEDA)->get();
		$moneda = $currency->pluck('texto', 'valor')->toArray();
		foreach($moneda as $k => $v){
			$moneda[$k] = __('messages.'.$v);
		}

		// ---------- FORM DEFINITION

		$lang = 'es';

		$primaryData = [];
		$secondaryData = [];
		$campos = [];

		$def = $producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		if( $producto->Categorias->count() > 0 ) {
			$idCategoria = $producto->Categorias[0]->id;
			$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
			if($cd->count() == 0){
				$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
			}
			if($cd->count() > 1){
				$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
			}
			$campos = $cd->first()->campos;

			$primaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
			});

			foreach($primaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0){
							if($producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = 'SIN TRADUCCION';
								}
							} else {
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$primaryData[] = $value;
				}
			}

			$secondaryFields = array_where($campos, function($value, $key){
				return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
			});

			foreach($secondaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0){
							if($producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = 'SIN TRADUCCION';
								}
							} else {
								$value['value'] = 'SIN TRADUCCION';
							}
						} else {
							$value['value'] = 'SIN TRADUCCION';
						}
//				} elseif(in_array($value['type'], ['titulo1', 'titulo2'])) {
//					$value['value'] = $def[$value['key']];
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
					$secondaryData[] = $value;
				}
			}
		}


		// ----------
		$gals = $producto->Galerias;
		$img_paths = [];
		foreach($gals as $gal ) {
			$imgs = $gal->Imagenes;
			foreach($imgs as $img) {
				$path = pathinfo( storage_path($img->ruta), PATHINFO_DIRNAME);
				$files = File::files($path);
				$findstr = explode('.', $img->filename)[0];
				foreach($files as $file) {
					if( preg_match("/{$findstr}/", $file) == 1 ) {
						$img_paths[ $img->id ][] = pathinfo($file, PATHINFO_BASENAME);
					}
				}
			}
		}
		// ----------
		$disponibilidad = ListaItem::listado(Lista::PRODUCTOITEM_DISPONIBILIDAD)->get()->pluck('texto', 'valor')->toArray();

		if( $producto->ProductoItem->idTipoImportacion == 1) {
			$view = 'admin.pages.producto.edit_import';
		} else {
			$view = 'admin.pages.producto.edit';
		}

		// ----------

		if( $producto->Categorias->count() > 0 ) {
			$subcats_list = $categorias->where('id', $producto->Categorias[0]->idPadre)->first()->Subcategorias->pluck('traduccion.'.App::getLocale(), 'id');
		} else {
			$subcats_list = [];
		}

		if( isset( $_GET['mirror'] ) && $_GET['mirror'] == 1 ) {
			$mirror = 1;
		} else {
			$mirror = 0;
		}

		// ---------- TAGS
		$grouped_tags = Resource_ProductoController::get_grouped_tags();

		$prod_tags = $producto->Tags->pluck('id');

		return view($view, [
			'producto' => $producto,
			'marcas' => $marcas,
			'categorias' => $categorias,
			'currency' => $currency,
			'moneda' => $moneda,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'campos' => $campos,
			'disponibilidad' => $disponibilidad,
			'subcats_list' => $subcats_list,
			'img_paths' => $img_paths,
			'mirror' => $mirror,
			'grouped_tags' => $grouped_tags,
			'prod_tags' => $prod_tags,
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

    	$validationRules = [
			'marca' => '',
			'modelo' => '',
			'idDisponibilidad' => '',
			'esNuevo' => '',
			'idProductoTipo' => '',
//			'idCategoria' => 'required',   // Cannot be changed when editing.

			'idUsuario' => 'required',
//			'precio' => 'required',
//			'currency' => 'required',
//			'precioVenta' => '',
//			'currencyVenta' => '',
//			'precioMinimo' => '',
//			'currencyMinimo' => '',
//			'priceSouthamerica' => '',

			'pais' => 'required',
			'ciudad' => '',
			'codigo_postal' => '',
			'direccion' => '',

			'longitud' => '',
			'ancho' => '',
			'alto' => '',
			'peso_bruto_fix' => '',
			'paletizado' => '',
		];

		if( Gate::allows('admin.producto.editPrecio')){
			$validationRules['precioProveedor'] = 'required';
			$validationRules['currencyProveedor'] = 'required';
			$validationRules['precioVenta'] = '';
			$validationRules['currencyVenta'] = '';
			$validationRules['precioMinimo'] = '';
			$validationRules['currencyMinimo'] = '';
			$validationRules['priceSouthamerica'] = '';
		}

		$prod = Producto::with([
			'ProductoItem.Traducciones',
		])->findOrFail($id);

		if( $prod->ProductoItem->idTipoImportacion == 1 ) {
			$validationRules['marca'] = 'required';
			$validationRules['modelo'] = 'required';
			$validationRules['subcategoria'] = 'required';
		}
		if( $prod->idProductoTipo == Producto::TIPO_ACCESORIO ) {
			$validationRules['marca'] = '';
			$validationRules['modelo'] = '';
			$validationRules['subcategoria'] = 'required';
		}

		$this->validate($request, $validationRules);

		// ---------- MARCA Y MODELO

		if( trim($request['marca']) != '' ) {
			$marcas = Marca::where('nombre', trim($request['marca']))->get();
			if($marcas->count() > 1){
				$marca = $marcas->first();
				$this->reportDev('Se encontró mas de una marca coincidente con: '. $request['marca']);
			} elseif ($marcas->count() == 1){
				$marca = $marcas->first();
			} else {
				$marca = Marca::create(['nombre' => trim($request['marca'])]);
			}
			$idMarca = $marca->id;
		} else {
			$idMarca = 0;
		}




		// ---------- MODELO

		if( $idMarca > 0 ) {
			$modelos = Modelo::where('idMarca', $idMarca)
				->where('nombre', trim($request['modelo']))
				->get();
			if($modelos->count() > 1){
				$modelo = $modelos->first();
				$this->reportDev('Se encontró mas de un modelo coincidente con: '. $request['modelo']. ' en la marcaID: '.$idMarca);
			} elseif ($modelos->count() == 1){
				$modelo = $modelos->first();
			} else {
				$modelo = Modelo::create(['idMarca' => $idMarca, 'nombre' => trim($request['modelo'])]);
			}

			$idModelo = $modelo->id;
		} else {
			$idModelo = 0;
		}


		if( $prod->ProductoItem->idTipoImportacion == 1 ) {
//			$prod->Categorias()->attach((int)$request['subcategoria']);
			$prod->Categorias()->sync([(int)$request['subcategoria']]);
		}

		// ---------- PRODUCTO + PRODUCTO_ITEM

//		$prod = Producto::findOrFail($id);  // Moved to the very beginning
		$prod->idMarca			= $idMarca;
		$prod->idModelo 		= $idModelo;
		$prod->esNuevo			= $request['esNuevo'];
		$prod->idProductoTipo	= $request['idProductoTipo'];
		if( Gate::allows('admin.producto.editPrecio')){
			$prod->precioMinimo = $request['precioMinimo'];
			$prod->monedaMinimo = trim($request['currencyMinimo']);
			$prod->precioProveedor = $request['precioProveedor'];
			$prod->monedaProveedor = trim($request['currencyProveedor']);
			$prod->precioEuropa = $request['precioEuropa'];
			$prod->monedaEuropa = trim($request['currencyEuropa']);
			$prod->precioUnitario = 0; //$request['precioVenta'];
			$prod->monedaUnitario = ''; //trim($request['currencyVenta']);
			$prod->precioSudamerica = ( isset($request['priceSouthamerica']) && $request['priceSouthamerica'] == 1 ) ? true : false;
		}

		$prod->save();

		$prodItem = $prod->ProductoItem;

		if ( $request['idDisponibilidad'] > 0 ) {
			$prodItem->idDisponibilidad = (int) $request['idDisponibilidad'];
			$prodItem->save();
		}

		// ---------- USUARIO

		$usuario = Usuario::findOrFail($request['idUsuario']);

		if($prod->Owner->id != $usuario->id) {
			$prod->Owner()->associate($usuario);
			$prod->save();

			$prodItem->Owner()->associate($usuario);
			$prodItem->save();
		}

		// ---------- UBICACION

		$ubicacion = $prodItem->Ubicacion;
		$ubicacion->pais = trim($request['pais']);
		$ubicacion->ciudad = trim($request['ciudad']);
		$ubicacion->cpostal = trim($request['codigo_postal']);
		$ubicacion->direccion = trim($request['direccion']);
		$ubicacion->save();

		// ---------- DIMENSIONES
		$prodItem->dimension = [
			'lng' => (int) $request['longitud'],
			'wdt' => (int) $request['ancho'],
			'hgt' => (int) $request['alto'],
			'wgt' => (int) $request['peso_bruto_fix'],
			'pal' => (int) $request['paletizado'],
		];
		$prodItem->save();

		if( $prod->ProductoItem->idTipoImportacion != 1 ) {

			$idCategoria = $prod->Categorias->first()->id;
			$cds = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
			if($cds->count() > 1){
				$this->reportDev('Hay mas de un CategoriaDefinition para la categoria: '.$idCategoria);
			}

			$cd = $cds->first();

			$campos = $cd->campos;

			$validations = [];
			foreach($campos as $campo){
				if(isset($campo['validation'])){
					$validations[$campo['key']] = $campo['validation'];
				}
			}
			$this->validate($request, $validations);

			// ---------- INFORMACION ADICIONAL
			$pdValues = [];
			foreach($campos as $campo){
				$val = trim($request[$campo['key']]);
				//TODO: Agregar validación y casting para tipo de campo numeric
				if($campo['type'] == 'multiselect'){
					$pdValues[$campo['key']] = [];
					if(isset($request['multiselects']) && is_array($request['multiselects'])){
						foreach($request['multiselects'][$campo['key']] as $req){
							$pdValues[$campo['key']][] = $req;
						}
					}
				} else {
					if($val != ''){
						$pdValues[$campo['key']] = $val;
					}
				}

				if($campo['key'] == 'ano_de_fabricacion' && $val != ''){
					$prod->anio = (int) trim($request[$campo['key']]);
					$prod->save();
				}

			}
			$prodItem->definicion = $pdValues;
			$prodItem->save();
		}

		// ---------- TAGS

		if( isset( $request['tags'] ) ) {
			$prod->Tags()->sync( $request['tags'] );
		}


		$response = Array(
			'result' => true,
			'message' => 'Registro guardado.',
			'data' => $prod
		);

		return response()->json($response);

    }

    /**
     * Remove the specified resource from storage.
     *
	 * @param Request $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id){
		//
	}

	// ---------- CUSTOM


	public function getForm($idCategoria = 0, $idProducto = 0) {

		$id = (int) $idCategoria;

		if($id <= 0) {
			$response = [
				'idCategoria' => [
					'Campo idCategoria es requerido.'
				]
			];
			return response()->make($response, 422);
		}

		$cd = CategoriaDefinition::where('idCategoria', $id)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$id}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$id}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		// ----------

//		$lang = 'es';
		$lang = App::getLocale();

		if($idProducto > 0 ) {
			$producto = Producto::findOrFail($idProducto);
			$def = $producto->ProductoItem->definicion;
			$primaryKeys = array_keys($def);

			$hashOptions = [];

			$primaryData = [];
			foreach($primaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						$value['translate'] = 1;
						if($producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0){
							if($producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = $def[$value['key']];
								}
							} else {
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
					} elseif( in_array($value['type'], ['multiselect']) ) {
//						foreach($def[$value['key']] as $mele) {
//							$hashOptions[$value['key']] = [
//								'key' =>
//							];
//						}

						$value['value'] = $def[$value['key']];
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
//				} else {
//					$primaryData[] = $value;
				}
				$primaryData[] = $value;
			}

			$secondaryData = [];
			foreach($secondaryFields as $value){
				if(in_array($value['key'], $primaryKeys)){
					if(in_array($value['type'], ['text', 'multiline'])){
						if($producto->ProductoItem->Traducciones != null && $producto->ProductoItem->Traducciones->count() > 0){
							if($producto->ProductoItem->Traducciones->where('campo', $value['key'])->count() > 0){
								$value['value'] = $producto->ProductoItem->Traducciones->where('campo', $value['key'])->first()->$lang;
								if(trim($value['value']) == ''){
									$value['value'] = $def[$value['key']];
								}
							} else {
								$value['value'] = $def[$value['key']];
							}
						} else {
							$value['value'] = $def[$value['key']];
						}
//					} elseif(in_array($value['type'], ['titulo1', 'titulo2'])) {
//						$value['value'] = $def[$value['key']];
					} elseif( in_array($value['type'], ['multiselect']) ) {
//						$hashOptions[$value['key']] = $def[$value['key']];
						$value['value'] = $def[$value['key']];
					} else {
						$value['value'] = $def[$value['key']];
					}
					$value['html'] = '';
//				} else {
//					$primaryData[] = $value;
				}
				$secondaryData[] = $value;
			}

			return view('admin.pages.producto.form', [
				'idCategoria' => $id,
				'campos' => $campos,
//				'primaryFields' => $primaryFields,
//				'secondaryFields' => $secondaryFields,
				'primaryFields' => $primaryData,
				'secondaryFields' => $secondaryData,
				'hashOptions' => $hashOptions,
			]);
		}

		return view('admin.pages.producto.form', [
			'idCategoria' => $id,
			'campos' => $campos,
			'primaryFields' => $primaryFields,
			'secondaryFields' => $secondaryFields,
		]);

	}

	public function getCampos($idCategoria = 0) {
		$id = (int) $idCategoria;
		$cd = CategoriaDefinition::where('idCategoria', $id)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$id}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$id}");
		}
		$campos = $cd->first()->campos;

		$response = Array(
			'result' => true,
			'message' => 'Campos.',
			'data' => $campos
		);

		return response()->json($response);
	}

	public function upload(Request $request){

		$this->validate($request, [
			'idProducto' => 'required',
		]);

		$idProducto = (int) $request['idProducto'];

		if($idProducto <= 0 ) {
			$response = [
				'idProducto' => [
					"Producto no definido: ID = {$idProducto}"
				]
			];
			return response()->make($response, 422);
		}

		$prod = Producto::findOrFail($idProducto);
		$galeria = $prod->Galerias->first();

		if($request->hasFile('file')){

			$file = $request->file('file');

			$filename_original = $file->getClientOriginalName();
			$extension_original = $file->getClientOriginalExtension();
			$mimetype = $file->getMimeType();
			$extension = $file->extension();

			$esZip = false;
			$esImagen = false;
			if(in_array($file->getMimeType(), ['application/zip','application/octet-stream','application/x-zip-compressed','multipart/x-zip'])){
				$esZip = true;
			} elseif (in_array($file->getMimeType(), ['image/gif', 'image/jpeg', 'image/png'])) {
				$esImagen = true;
			}

			if(!$file->isValid()){
				$response = [
					'error' => 'Archivo INVALIDO'
				];
				return response()->make($response, 422);
			}

			$relativePath = "app".DIRECTORY_SEPARATOR."published".DIRECTORY_SEPARATOR.$prod->id;
			$destinationPath = storage_path($relativePath);

			if(!is_dir($destinationPath)){
				//$mkdir = mkdir($destinationPath, 0755);
				$mkdir = File::makeDirectory($destinationPath, 0755, true);
			}

			$randChars = $this->randomChars(5);
			$filename = "img_{$randChars}.{$extension}";

			$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
			if(File::exists($fullpath)){
				$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
				File::delete($fullpath);
			}

			$file->move($destinationPath, $filename);

			if(!File::exists($fullpath)){
				$response = [
					'error' => 'Archivo no encontrado despues de subir.'
				];
				return response()->make($response, 422);
			}

			// ----------

			$imagen = Imagen::create([
				'idGaleria' => $galeria->id,
//				'ruta' => $imagen['ruta'],
				'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
				'filename' => $filename,
				'mimetype' => $mimetype,
				'estado' => 1,
			]);

			// ----------

			$response = Array(
				'result' => true,
				'message' => 'Archivo subido correctamente.',
				'data' => $imagen
			);
		} else {
			$response = [
				'error' => 'No tiene archivo.'
			];
		}
		return response()->json($response);
	}

	public function getModelos(Request $request){

		$this->validate($request, [
			'marca' => 'required'
		]);

		$marca = $request['marca'];

		$marca = Marca::where('nombre', trim($marca))->first();

		$modelos = [];
		if($marca){
			$modelos = Modelo::where('idMarca', $marca->id)->get()->pluck('nombre')->toArray();
		}

		$response = Array(
			'result' => true,
			'message' => 'Modelos relacionados.',
			'data' => $modelos
		);

		return response()->json($response);

	}

	public function images_move( Request $request, $idProducto ) {
		$p = Producto::findOrFail( $idProducto );

		$relativePath = "app".DIRECTORY_SEPARATOR."published".DIRECTORY_SEPARATOR.$p->id;
		$destinationPath = storage_path($relativePath);
		if(!is_dir($destinationPath)){
			//$mkdir = mkdir($destinationPath, 0755);
			$mkdir = File::makeDirectory($destinationPath, 0755, true);
		}

		$gals = $p->Galerias;
		$moved_imgs = [];
		foreach( $gals as $gal ) {
			$imagenes = $gal->Imagenes;
			foreach( $imagenes as $imagen ) {

				$newfilepath = $destinationPath.DIRECTORY_SEPARATOR.$imagen->filename;

				if(File::exists(storage_path( $imagen->ruta ))){
					$moved = File::move(storage_path( $imagen->ruta ), $newfilepath);

					if( $moved ) {
						$imagen->ruta = $relativePath.DIRECTORY_SEPARATOR.$imagen->filename;
						$imagen->save();
						$moved_imgs[] = $imagen->ruta;
					}
				}

			}
		}
		$response = Array(
			'result' => true,
			'message' => 'Imágenes transferidas a nueva ubicacion.',
			'data' => $moved_imgs
		);

		return response()->json( $response );
	}

	public function images_resize( Request $request, $idProducto ) {
		$p = Producto::findOrFail( $idProducto );

		// ---------- THUMBNAIL SIZE
		$w = Imagen::DIMENSION_MAX_WIDTH; // 1024;
		$h = Imagen::DIMENSION_MAX_HEIGHT; // 1024;
//		$tw = 260;
//		$th = 120;
		$tw = Imagen::DIMENSION_MAX_WIDTH_THUMB; // 305;
		$th = Imagen::DIMENSION_MAX_HEIGHT_THUMB; // 150;

		$resized_imgs = [];
		$failed_imgs = [];

		$gals = $p->Galerias;
		foreach( $gals as $gal ) {
			$imagenes = $gal->Imagenes;
			foreach( $imagenes as $imagen ) {

				$file = storage_path( $imagen->ruta );

				$filename = pathinfo( $file, PATHINFO_BASENAME);
				$newname = str_replace('.', '_res.', $filename);
				$resized = $this->resize_source( $file, $w, $h, false, $newname, $message );


				if( $resized ) {
					$resized_imgs[] = $file;
				} else {
					$failed_imgs[] = $file;
				}


				$filename = pathinfo( $file, PATHINFO_BASENAME);
				$newname = str_replace('.', '_thumb.', $filename);
				$thumb = $this->resize_source( $file, $tw, $th, false, $newname, $message );

				if( $thumb ) {
					$resized_imgs[] = $file;
				} else {
					$failed_imgs[] = $file;
				}

				sleep(1);

			}
		}

		$response = Array(
			'result' => true,
			'message' => 'Imágenes redimensionadas a nueva ubicacion.',
			'data' => [
				'success' => $resized_imgs,
				'failed' => $failed_imgs,
			]
		);

		return response()->json( $response );
	}

	public function images_remove( Request $request, $idProducto ) {

		$this->validate($request, [
			'path' => 'required'
		]);

		$destinationPath = storage_path( $request['path']);
		if(is_dir($destinationPath)){
			$response = Array(
				'result' => false,
				'message' => 'No es un archivo válido.',
				'data' => []
			);

			return response()->json( $response );
		}
		if(!file_exists($destinationPath)){
			$response = Array(
				'result' => false,
				'message' => 'Archivo no existe',
				'data' => []
			);

			return response()->json( $response );
		}

		if( File::delete($destinationPath) ) {
			$response = Array(
				'result' => true,
				'message' => 'Archivo eliminado: '.$destinationPath,
				'data' => []
			);

			return response()->json( $response );
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo eliminar',
				'data' => []
			);

			return response()->json( $response );
		}

	}

	/**
	 * Ref.: https://stackoverflow.com/a/60630053/2367718
	 * @param $file
	 * @param $w
	 * @param $h
	 * @param bool $crop
	 * @param string $newname
	 * @param string $message
	 * @return bool|mixed
	 */
	public function resize_source( $file, $w, $h, $crop = false, $newname = '', &$message = '' ) {

		try {
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$ext = strtolower($ext);
			list($width, $height) = getimagesize( $file );
			// if the image is smaller we dont resize

			if ($w > $width && $h > $height) {
				$message = 'La imagen es menor a '.$w.', no se redimensionará.';
				return false;
			}
			$r = $width / $height;
			if ($crop) {
				if ($width > $height) {
					$width = ceil($width - ($width * abs($r - $w / $h)));
				} else {
					$height = ceil($height - ($height * abs($r - $w / $h)));
				}
				$newwidth = $w;
				$newheight = $h;
			} else {
				if ($w / $h > $r) {
					$newwidth = $h * $r;
					$newheight = $h;
				} else {
					$newheight = $w / $r;
					$newwidth = $w;
				}
			}
			$dst = imagecreatetruecolor($newwidth, $newheight);

			switch ($ext) {
				case 'jpg':
				case 'jpeg':
					$src = imagecreatefromjpeg($file);
					break;
				case 'png':
					$src = imagecreatefrompng($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
				case 'gif':
					$src = imagecreatefromgif($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
//				case 'bmp':
//					$src = imagecreatefrombmp($file);
					break;
				default:
					$message = 'Unsupported image extension found: ' . $ext;
					return false;
			}
			$result = imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			if( $newname != '' ) {
				$filename = pathinfo( $file, PATHINFO_BASENAME);
				$file = str_replace($filename, $newname, $file);
			}

			switch ($ext) {
				case 'bmp':
					imagewbmp($dst, $file);
					break;
				case 'gif':
					imagegif($dst, $file);
					break;
				case 'jpg':
				case 'jpeg':
					imagejpeg($dst, $file);
					break;
				case 'png':
					imagepng($dst, $file);
					break;
			}

			imagedestroy($dst);
			imagedestroy($src);

			return $file;

		} catch (Exception $err) {
			// LOG THE ERROR HERE
			$message = $err->getMessage();
			return false;
		}
	}


	public function translation( Request $request, $idProducto, $campo){

		$prod = Producto::with([
			'ProductoItem.Traducciones'
		])
			->find($idProducto);

		$espaniol = $sueco = $ingles = '';
		$definicion = $prod->ProductoItem->definicion;
		$sueco = $definicion[$campo];

		$traducciones = $prod->ProductoItem->Traducciones;
		$traduccion = null;

		// ----------

		if($traducciones){
			if($traducciones->where('campo', $campo)->count() > 1){
				$this->reportDev("Multiples traducciones para Producto:{$idProducto}, campo:{$campo}");
			}
			if($traducciones->where('campo', $campo)->count() > 0){
				$traduccion = $traducciones->where('campo', $campo)->first();
				$espaniol = $traduccion->es;
				$ingles = $traduccion->en;
				$sueco = $traduccion->se;
			}
		}


		if($request->isMethod('post')) {

			if(!$traduccion){
				$traduccion = new Traduccion();
				$traduccion->campo = $campo;
				$traduccion->en = trim($request['en']);
				$traduccion->es = trim($request['es']);
				$traduccion->se = trim($request['se']);
				$prod->ProductoItem->Traducciones()->save($traduccion);
			} else {
				$traduccion->en = trim($request['en']);
				$traduccion->es = trim($request['es']);
				$traduccion->se = trim($request['se']);
				$traduccion->save();
			}

			$response = Array(
				'result' => true,
				'message' => 'Registro guardado correctamente.',
				'data' => $traduccion->toArray()
			);

			return response()->json($response);
		}


		return view('admin.pages.producto.translation', [
			'key' => $campo,
			'prodId' => $idProducto,
			'espaniol' => $espaniol,
			'sueco' => $sueco,
			'ingles' => $ingles,

		]);

	}

	public function translationML( Request $request, $idProducto, $campo){

		$prod = Producto::with([
			'ProductoItem.Traducciones'
		])
			->find($idProducto);

		$espaniol = $sueco = $ingles = '';
		$definicion = $prod->ProductoItem->definicion;
		$sueco = $definicion[$campo];

		$traducciones = $prod->ProductoItem->Traducciones;
		$traduccion = null;

		// ----------

		if($traducciones){
			if($traducciones->where('campo', $campo)->count() > 1){
				$this->reportDev("Multiples traducciones para Publicacion:{$idProducto}, campo:{$campo}");
			}
			if($traducciones->where('campo', $campo)->count() > 0){
				$traduccion = $traducciones->where('campo', $campo)->first();
				$espaniol = $traduccion->es;
				$ingles = $traduccion->en;
				$sueco = $traduccion->se;
			}
		}

		if($request->isMethod('post')) {

			if(!$traduccion){
				$traduccion = new Traduccion();
				$traduccion->campo = $campo;
				$traduccion->en = trim($request['en']);
				$traduccion->es = trim($request['es']);
				$traduccion->se = trim($request['se']);
				$prod->ProductoItem->Traducciones()->save($traduccion);
			} else {
				$traduccion->en = trim($request['en']);
				$traduccion->es = trim($request['es']);
				$traduccion->se = trim($request['se']);
				$traduccion->save();
			}

			$response = Array(
				'result' => true,
				'message' => 'Registro guardado correctamente.',
				'data' => $traduccion->toArray()
			);

			return response()->json($response);
		}


		return view('admin.pages.producto.translationML', [
			'key' => $campo,
			'prodId' => $idProducto,
			'espaniol' => $espaniol,
			'sueco' => $sueco,
			'ingles' => $ingles,
		]);

	}

	public function create_item( $idProducto ) {

		$producto = Producto::findOrFail( $idProducto );

		return view('admin.pages.producto.create_producto_item', [
			'producto' => $producto,
		]);

	}

	public function create_precios( $idProductoItem ) {

		$productoItem = ProductoItem::findOrFail( $idProductoItem );

		$precios = $productoItem->Precios;
		$usd = $precios->where('moneda', 'usd')->first();
		$usd = ($usd) ? $usd->precio : 0;
		$sek = $precios->where('moneda', 'sek')->first();
		$sek = ($sek) ? $sek->precio : 0;
		$clp = $precios->where('moneda', 'clp')->first();
		$clp = ($clp) ? $clp->precio : 0;

		return view('admin.pages.producto.create_producto_item_precio', [
			'productoItem' => $productoItem,
			'usd' => $usd,
			'sek' => $sek,
			'clp' => $clp,
		]);

	}

	public function historia( $id ) {

		$producto = Producto::with([
			'Calendarios' => function( $q ) {
				$q->where('idCalendarioTipo', Calendario::TIPO_HISTORIA);
			},
			'Calendarios.Actividades'
		])->findOrFail($id);

		$actividades = [];
		if( $producto->Calendarios->count() > 0 ) {
			$actividades = $producto->Calendarios[0]->Actividades;
		}

		$meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Noviembre', 'Diciembre'];

		foreach( $actividades as &$actividad ) {
			$created_at = $actividad->created_at;
			$actividad->created_at_literal = $created_at->format('d').' '.$meses[ $created_at->format('n') ].' '.$created_at->format('Y');
		}

		return view('admin.pages.producto.historia', [
			'producto' => $producto,
			'actividades' => $actividades,
			'meses' => $meses
		]);

	}

	public function asignarActividades( $id = 0 ) {

		$producto = null;
		if( $id > 0 ) {
			$producto = Producto::with([
				'Galerias.Imagenes',
				'Calendarios' => function( $q ) {
					$q->where('idCalendarioTipo', Calendario::TIPO_HISTORIA);
				},
				'Calendarios.Actividades.Responsables',
			])->findOrFail($id);
		}

		$actividadTipos = ActividadTipo::where('idPadre', '>', 0)->get();


		return view('admin.pages.producto.asignar_actividades', [
			'idProducto' => $id,
			'producto' => $producto,
			'actividadTipos' => $actividadTipos,
		]);

	}

}
