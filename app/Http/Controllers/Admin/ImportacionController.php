<?php

namespace App\Http\Controllers\Admin;

use App\Models\Archivo;
use App\Models\Galeria;
use App\Models\Imagen;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Precio;
use App\Models\Producto;
use App\Models\ProductoItem;
use App\Models\Publicacion;
use App\Models\Traduccion;
use App\Models\Ubicacion;
use App\Models\Usuario;
use Carbon\Carbon;
use Google\Cloud\Translate\V2\TranslateClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ImportacionController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

	public function index() {

		$this->reportDev('Mensaje de prueba');

		$estados = ListaItem::where('idLista', Lista::PUBLICACION_ESTADO)->get();
		return view('admin.pages.importacion.index', [
			'publicacionEstados' => $estados->pluck('texto', 'valor')->toArray()
		]);
	}

	public function form() {

	}

	public function upload(Request $request){
		$rand = $request['randBanner'];

		$this->validate($request, [
			'idUsuario' => 'required',
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
			'idioma' => 'required',
			'pais' => 'required',
			'ciudad' => '',
			'cpostal' => '',
			'direccion' => '',
		]);

		if($request->hasFile('file')) {

			$file = $request->file( 'file' );

			$filename_original = $file->getClientOriginalName();
			$extension_original = $file->getClientOriginalExtension();
			$mimetype = $file->getMimeType();
			$extension = $file->extension();

			$esZip = false;
			$esImagen = false;
			if ( in_array( $file->getMimeType(), [ 'application/zip', 'application/octet-stream', 'application/x-zip-compressed', 'multipart/x-zip' ] ) ) {
				$esZip = true;
			} elseif ( in_array( $file->getMimeType(), [ 'image/gif', 'image/jpeg', 'image/png' ] ) ) {
				$esImagen = true;
			}

			if ( !$file->isValid() ) {
				$response = [
					'error' => 'Archivo INVALIDO'
				];
				return response()->make( $response, 422 );
			}

			$relativePath = "app" . DIRECTORY_SEPARATOR . "importacion";
			$destinationPath = storage_path( $relativePath );

			if ( !is_dir( $destinationPath ) ) {
				//$mkdir = mkdir($destinationPath, 0755);
				$mkdir = File::makeDirectory( $destinationPath, 0755, true );
			}

			$randChars = $this->randomChars( 5 );
//			$filename = "import_{$randChars}.{$extension}";
			$filename = $filename_original;

			$source_file = $destinationPath . DIRECTORY_SEPARATOR . $filename;
			if ( File::exists( $source_file ) ) {
//				$this->reportDev( 'Archivo de importación duplicado, hubo un intento de sobreescribirlo: ' . $source_file );
				File::delete( $source_file );
			}

			$file->move( $destinationPath, $filename );

			if ( !File::exists( $source_file ) ) {
				$response = Array(
					'result' => true,
					'message' => 'Archivo VACIO.',
					'data' => []
				);
				return response()->make( $response, 422 );
			}

			// ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ---------- ----------
			// ---------- P A C K A G E

			$myfile = fopen( $source_file, "r" ) or die( "Unable to open file!" );
			$content = fread( $myfile, filesize( $source_file ) );
			fclose( $myfile );

			if( trim($content) == '' ) {
				$this->reportDev('paqute vacío: '.$source_file);
				$response = [
					'error' => 'No tiene archivo.'
				];
				return response()->json($response);
			}

			$package = json_decode( $content, true );

			logger( $package );
//			logger(print_r($package, true));
//			logger("productos: " . count( $package['productos'] ) );
//			return '';

//			$package = [
//				'name' => '',
//				'link' => '',
//				'sold' => 0,
//				'img_portada' => '',
//				'category' => '',
//				'location' => '',
//				'price_bruto' => '',
//				'price' => '',
//				'price_currency' => '',
//				'price_old' => '',
//				'price_old_currency' => '',
//				'description' => '',
//				'details' => [
//					'imgs' => [],
//					'attachments' => [],
//					'videos' => [],
//					'sections' => [
//						[
//							'title' => '',
//							'fields' => []
//						]
//					],
//					'types' => [						// PRODUCTS SHOULD HAVE AT LEAST 1 TYPE
//						[
//							'name' => '',
//							'title' => '',
//							'description' => '',
//							'sku' => '',
//							'availability' => '',
//							'price_bruto' => '',
//							'price' => '',
//							'price_currency' => '',
//						]
//					],
//				],
//			];

			// ---------- P U B L I C A C I O N

			$estado = ListaItem::listado( Lista::PUBLICACION_ESTADO )->where( 'texto', 'awaiting_approval' )->first();
			if ( !$estado ) {
				$this->reportDev( 'No se encontró Estado awaiting_approval para crear la publicación de announce here. File:' . __FILE__ . ' Line:' . __LINE__ );
			}

			$idUsuario = (int)$request['idUsuario'];
			$lang = $idioma = $request['idioma'];
			$usuario = Usuario::findOrFail( $idUsuario );
			$fechaIni = Carbon::createFromFormat( 'd/m/Y', $request['fechaInicio'] );
			$fechaFin = Carbon::createFromFormat( 'd/m/Y', $request['fechaFin'] );
			$days = $fechaFin->diffInDays( $fechaIni );


			// ----------

			$link = $package['link'];

			$productos_existentes = Producto::where('permalink', trim($link) )->get();
			if( $productos_existentes->count() > 1 ) {
				$this->reportDev('Existe mas de un producto con el link: '. $link);
				logger('########################################################################################################');
				logger('####################### ');
				logger('####################### EXISTEN MAS DE 1 PRODUCTOS CON EL MISMO PERMALINK: ################');
				logger('####################### '. $link );
				logger('####################### ');
				logger('########################################################################################################');
			}
			if( $productos_existentes->count() > 0 ) {
				$prod = $productos_existentes->first();
				logger('########################################################################################################');
				logger('####################### ');
				logger('####################### ACTUALIZANDO PRODUCTO YA EXISTENTE ID: ' . $prod->id );
				logger('####################### '. $link );
				logger('####################### ');
				logger('########################################################################################################');

				$response = [
					'code' => 422,
					'error' => 'YA EXISTE UN PRODUCTO CON EL MISMO PERMALINK, ID:' . $prod->id,
					'source' => $source_file,
					'permalink' => $link,
				];
				return response()->make( $response, 422 );
			} else {
				$prod = new Producto();
			}

			$prod->idMarca = 0; //$idMarca;
			$prod->idModelo = 0; //$idModelo;
			$prod->esNuevo = $request['esNuevo'];
			$prod->idProductoTipo = $request['idProductoTipo'];
			$prod->fuente_import = $relativePath . DIRECTORY_SEPARATOR . $filename;
			$prod->permalink = trim( $package['link'] );

			if( in_array(trim($package['price_currency']), ['usd', 'sek', 'kr', 'eur']) ) {
				if( trim($package['price_currency']) == 'kr' )
					$package['price_currency'] = 'sek';
				$package['price'] = str_replace(',','.', $package['price']);
//					$prod->precioProveedor = ( empty($package['price']) ) ? 0 : $package['price']; //$request['precio'];
//					$prod->monedaProveedor = ( empty($package['price_currency']) ? 'sek' : $package['price_currency'] ); //trim($request['currency']);
				$prod->precioProveedor = 0;
				$prod->monedaProveedor = '';
			} else {
				$prod->precioProveedor = 0;
				$prod->monedaProveedor = '';
			}

			$prod->precioMinimo = 0; //$request['precioMinimo'];
			$prod->monedaMinimo = ''; //trim($request['currencyMinimo']);
			$prod->precioEuropa = 0; //$request['precioEuropa'];
			$prod->monedaEuropa = ''; //trim($request['currencyEuropa']);
			$prod->precioSudamerica = 0; //trim($request['priceSouthamerica']);
			$prod->save();

			$this->crearTraducciones( $prod, $package, $lang );

			$prod->Owner()->associate( $usuario );
			$prod->save();

			// ---------- VIDEOS

			$def = [];
			if( count($package['details']['videos']) > 0 ) {
				foreach( $package['details']['videos'] as $video) {
					if( trim($video) != '') {
						$def['videos'][] = trim($video);
					} else {
						logger('VIDEO VACÍO');
					}
				}
			}

			// ---------- TYPES

			$galeria = $prod->Galerias()->save( new Galeria() );

			if( isset( $package['details']['types'] ) && count( $package['details']['types'] ) > 0 ) {

				foreach( $package['details']['types'] as $type ) {
					$prodItem = $prod->ProductoItem()->save( new ProductoItem() );
					$prodItem->idDisponibilidad = 1;
					$prodItem->idTipoImportacion = 1;
					$prodItem->sku = trim( $type['sku'] );
					$prodItem->definicion = $def;
//						$prodItem->permalink = trim( $package['link'] );

					$prodItem->nombre = trim( $type['name'] );
					$prodItem->descripcionDetalle = trim( $type['description'] );
//						$prodItem->precioVenta = trim( $type['price'] );
//						$prodItem->monedaVenta = trim( $type['price_currency'] );
					$prodItem->precioVenta = 0;
					$prodItem->monedaVenta = '';

					$prodItem->save();
					$prodItem->Owner()->associate( $usuario );

					$prodItem->Ubicacion()->save(new Ubicacion([
						'pais' => trim($request['pais']),
						'ciudad' => trim($request['ciudad']),
						'cpostal' => trim($request['cpostal']),
						'direccion' => trim($request['direccion'])
					]));
					$prodItem->save();

					// ----------

					$this->crearTraducciones( $prodItem, $type, $lang );
				}

			} else {
				$prodItem = $prod->ProductoItem()->save( new ProductoItem() );
				$prodItem->idDisponibilidad = 1;
				$prodItem->idTipoImportacion = 1;
				$prodItem->sku = trim( $package['sku'] );
				$prodItem->definicion = $def;
//					$prodItem->permalink = trim( $package['link'] );
				$prodItem->save();
				$prodItem->Owner()->associate( $usuario );

				$prodItem->Ubicacion()->save(new Ubicacion([
					'pais' => trim($request['pais']),
					'ciudad' => trim($request['ciudad']),
					'cpostal' => trim($request['cpostal']),
					'direccion' => trim($request['direccion'])
				]));
				$prodItem->save();

				$this->crearTraducciones( $prodItem, $package, $lang );

				// ----------

//					$this->crearTraducciones( $prodItem, $package, $lang );
			}

			// ---------- DOWNLOAD IMAGES

			$relativePath = "app".DIRECTORY_SEPARATOR."published".DIRECTORY_SEPARATOR.$prod->id;
			$destinationPath = storage_path($relativePath);
			if(!is_dir($destinationPath)){
				//$mkdir = mkdir($destinationPath, 0755);
				$mkdir = File::makeDirectory($destinationPath, 0755, true);
			}

			foreach( $package['details']['imgs'] as $img ) {

				if( trim($img) != '' ) {
					// Use basename() function to return the base name of file
					logger('$img: '.$img);
					$filename = basename($img);

//					$randChars = $this->randomChars(5);
//					$filename = "img_{$randChars}.{$extension}";
					$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
					if(File::exists($fullpath)){
						$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
//						File::delete($fullpath);
					}

					logger('$fullpath: '.$fullpath);

					// Use file_get_contents() function to get the file
					// from url and use file_put_contents() function to
					// save the file by using base name
					// Initialize a file URL to the variable
//					$url = 'https://contribute.geeksforgeeks.org/wp-content/uploads/gfg-40.png';
					$url = $img;
					logger('$url: '.$url);
//					if ( file_put_contents( $fullpath, file_get_contents( $url ) ) ) {
//						logger("File downloaded successfully");
//					} else {
//						logger("File downloading failed.");
//					}

					$imagen = Imagen::create([
						'idGaleria' => $galeria->id,
//					'ruta' => $imagen['ruta'],
						'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
						'filename' => $filename,
						'mimetype' => '',
						'estado' => Imagen::ESTADO_PENDIENTE,
						'fuente' => $img,
					]);
				}


			}

			foreach( $package['details']['attachments'] as $archivo ) {

				if( trim($archivo) != '' ) {
					// Use basename() function to return the base name of file
//						logger('$archivo: '.$archivo);
					$filename = basename($archivo);

//					$randChars = $this->randomChars(5);
//					$filename = "img_{$randChars}.{$extension}";
					$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
					if(File::exists($fullpath)){
						$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
//						File::delete($fullpath);
					}

//						logger('$fullpath: '.$fullpath);

					// Use file_get_contents() function to get the file
					// from url and use file_put_contents() function to
					// save the file by using base name
					// Initialize a file URL to the variable
//					$url = 'https://contribute.geeksforgeeks.org/wp-content/uploads/gfg-40.png';
					$url = $archivo;
//						logger('$url: '.$url);
//					if ( file_put_contents( $fullpath, file_get_contents( $url ) ) ) {
//						logger("File downloaded successfully");
//					} else {
//						logger("File downloading failed.");
//					}

					$prod->Archivos()->save( Archivo::create([
						'realname' => $filename,
						'filename' => $filename,
						'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
						'mimetype' => '',
						'estado' => Archivo::ESTADO_PENDIENTE,
						'fuente' => $archivo
					]) );

				} else {
					logger('ATTACHMENT VACÍO');
				}
			}

			$pub = new Publicacion();
			$pub->token = '';
			$pub->estado = Publicacion::ESTADO_DRAFT; // $estado->valor;
			$pub->dias = $days;
			$pub->precio = 0;
			$pub->moneda = '';
			$pub->fechaInicio = $fechaIni;
			$pub->fechaFin = $fechaFin;

			$pub = $usuario->Publicaciones()->save( $pub );

			$pub->Producto()->associate( $prod );
			$pub->save();

			$skus = [];

			$pis = $prod->ProductoItems;
			foreach($pis as $pi) {
				$skus[] = $pi->sku;
			}

			logger('################################# LOADING PRICES ');

//			$response = $this->cargar_precios_desde_planilla($skus);
			$response = $this->cargar_precios_desde_database($skus);
			logger($response);
			if( $response['result'] == 1 ) {
				$data = $response['data'];
				foreach( $data as $sku => $obj ) {
					$pi = $pis->where('sku', $sku)->first();
//						$pi->Precios
					if( count($obj) > 0 ) {
						$obj = $obj[0];
						$pi->Precios()->save( Precio::create([
							'idTipo' => Precio::TIPO_PRECIO_VENTA,
							'precio' => $obj['sek'],
							'moneda' => 'sek',
							'estado' => 1,
						]) );
						$pi->Precios()->save( Precio::create([
							'idTipo' => Precio::TIPO_PRECIO_VENTA,
							'precio' => $obj['usd'],
							'moneda' => 'usd',
							'estado' => 1,
						]) );
						$pi->Precios()->save( Precio::create([
							'idTipo' => Precio::TIPO_PRECIO_VENTA,
							'precio' => $obj['clp'],
							'moneda' => 'clp',
							'estado' => 1,
						]) );
					}

				}
			}


			// ----------

			$response = Array(
				'result' => true,
				'message' => 'Archivo subido correctamente.',
				'data' => []
			);
		} else {
			$response = [
				'error' => 'No tiene archivo.'
			];
		}
		return response()->json($response);
	}

	private function crearTraducciones( $entity, $package, $lang ) {
		$traducciones = $entity->Traducciones;
		$traduccion = $traducciones->where('campo', 'nombre')->first();
		if(!$traduccion){
			$traduccion = new Traduccion();
			$traduccion->campo = 'nombre';
			$traduccion->se = '';
			$traduccion->en = '';
			$traduccion->es = '';
			$traduccion->$lang = trim($package['name']);
			$entity->Traducciones()->save($traduccion);
		} else {
			$traduccion->$lang = trim($package['name']);
			$traduccion->save();
		}

//		$traducciones = $entity->Traducciones;
		$traduccion_desc = $traducciones->where('campo', 'descripcion')->first();
		if(!$traduccion_desc){
			$traduccion_desc = new Traduccion();
			$traduccion_desc->campo = 'descripcion';
			$traduccion_desc->se = '';
			$traduccion_desc->en = '';
			$traduccion_desc->es = '';
			$descripcion = trim( $package['description'] );
//			$descripcion = preg_replace('/class="[a-zA-Z0-9:;_\.\s\(\)\-\,]*"/', '', $descripcion);
//			$descripcion = preg_replace('/style="[a-zA-Z0-9:;_#\.\s\(\)\-\,]*"/', '', $descripcion);
			$descripcion = preg_replace('/(class|style)=("|\')[a-zA-Z0-9:;#_\.\s\(\)\-\,]*("|\')/', '', $descripcion);
			$descripcion = preg_replace('/Artnr/', 'ID', $descripcion);
			$descripcion = preg_replace('/<br>--------------------------<br>/', '<hr>', $descripcion);
			$traduccion_desc->$lang = $descripcion;
			$entity->Traducciones()->save($traduccion_desc);
		} else {
			$traduccion_desc->$lang = trim($package['description']);
			$traduccion_desc->save();
		}

		// ---------- GTRANSLATIONS

//		$json_path = storage_path('app/exind-temp-85c435c79c70.json');
		$json_path = storage_path('app/expoindustri-translate-f8c3037baea3.json');
		putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $json_path);


		$valid_tos = [];

		switch( $lang ) {
			case 'se' :
				$valid_tos = ['es', 'en'];
				break;
			case 'es' :
				$valid_tos = ['se', 'en'];
				break;
			case 'en' :
				$valid_tos = ['se', 'es'];
				break;
		}

		$valid_from	= TraduccionController::valid_languages( $lang );
		foreach( $valid_tos as $lang_to ) {
			$valid_to	= TraduccionController::valid_languages( $lang_to );

			$text = [
				$traduccion->$lang,
				$traduccion_desc->$lang,
			];

			logger('============ TRANSLATING ');
			logger("FROM: {$valid_from} TO: {$valid_to}");
			logger($text);

			$translate = new TranslateClient();
			$result = $translate->translateBatch($text, [
				'source' => $valid_from,
				'target' => $valid_to,
				'format' => 'html',
				'model' => 'base',
			]);

			logger($result);

			$traduccion->$lang_to = $result[0]['text'];
			$traduccion->save();
			$traduccion_desc->$lang_to = $result[1]['text'];
			$traduccion_desc->save();
		}
	}

	public function imagenes() {
		return view('admin.pages.importacion.imagenes', []);
	}

	public function archivos() {
		return view('admin.pages.importacion.archivos', []);
	}

	public function imp_imagen( Request $request ) {

		$this->validate($request, [
			'id_imagen' => 'required',
		]);

		$result = 0;
		$message = '';
		$id_imagen = (int)$request['id_imagen'];
		$img = Imagen::findOrFail( $id_imagen );

		if( $img ) {

			$fullpath = storage_path( $img->ruta );
			$randChars = $this->randomChars(5);

			$extension = pathinfo($img->ruta, PATHINFO_EXTENSION);
			$old_name = pathinfo($img->ruta, PATHINFO_BASENAME);
			$filename = "img_{$randChars}_{$img->id}.{$extension}";

			$img->ruta = str_replace($old_name, $filename, $img->ruta);
			$img->filename = $filename;
			$img->save();

			$fullpath = storage_path( $img->ruta );

			if ( @file_put_contents( $fullpath, file_get_contents( $img->fuente ) ) ) {
				$result = 1;
				$message = "File downloaded successfully.";
				$img->estado = Imagen::ESTADO_ACTIVO;
				$img->save();

				logger("DOWNLOAD SUCCESFUL");
				logger($img->toArray());

				for( $time = 0 ; $time < 3 ; $time++ ) {
					logger("awaiting downloaded file availability");
					if( !File::exists( storage_path( $img->ruta ) ) ) {
						sleep(3);
						logger("sleeping 3");
					} else {
						break;
					}
				}

				if( !File::exists( storage_path( $img->ruta ) ) ) {
					$result = 0;
					$img->estado = Imagen::ESTADO_FALLIDO;
					$img->save();
					$message = "File downloaded FAILED";
					$this->reportDev('Falló descarga de imagen ID: '.$img->id);
				}

//				$message = '';
				$res_result = $this->image_resize( $img, $message );
				if( !$res_result ) {
//					$result = 0;
					$img->estado = Imagen::ESTADO_FALLIDO_RESIZE;
					$img->save();
					$message .= " Downloaded. Resize Message: ".$message;
				}
			} else {
				$result = 0;
				$img->estado = Imagen::ESTADO_FALLIDO;
				$img->save();
				$message = "File downloaded FAILED";
				$this->reportDev('Falló descarga de imagen ID: '.$img->id);
			}
		} else {
			$result = 0;
			$message = "Obj Imagen con ID: {$id_imagen} no encontrado.";
			$img = new \stdClass();
		}

		$response = Array(
			'result' => $result,
			'message' => $message,
			'data' => $img->toArray()
		);

		return response()->json( $response );
	}

	public function imp_archivo( Request $request ) {

		$this->validate($request, [
			'id_archivo' => 'required',
		]);

		$result = 0;
		$message = '';
		$id_archivo = (int)$request['id_archivo'];
		$img = Archivo::findOrFail( $id_archivo );

		if( $img ) {

			$fullpath = storage_path( $img->ruta );
			$randChars = $this->randomChars(5);

			$extension = pathinfo($img->ruta, PATHINFO_EXTENSION);
			$old_name = pathinfo($img->ruta, PATHINFO_BASENAME);
			$filename = "file_{$randChars}_{$img->id}.{$extension}";

			$img->ruta = str_replace($old_name, $filename, $img->ruta);
			$img->filename = $filename;
			$img->save();

			$fullpath = storage_path( $img->ruta );

			if ( @file_put_contents( $fullpath, file_get_contents( $img->fuente ) ) ) {
				$result = 1;
				$message = "File downloaded successfully.";
				$img->estado = Archivo::ESTADO_ACTIVO;
				$img->save();

				logger("DOWNLOAD SUCCESFUL");
				logger($img->toArray());

				for( $time = 0 ; $time < 3 ; $time++ ) {
					logger("awaiting downloaded file availability");
					if( !File::exists( storage_path( $img->ruta ) ) ) {
						sleep(3);
						logger("sleeping 3");
					} else {
						break;
					}
				}

				if( !File::exists( storage_path( $img->ruta ) ) ) {
					$result = 0;
					$img->estado = Archivo::ESTADO_FALLIDO;
					$img->save();
					$message = "File downloaded FAILED";
					$this->reportDev('Falló descarga de Archivo ID: '.$img->id);
				}

			} else {
				$result = 0;
				$img->estado = Archivo::ESTADO_FALLIDO;
				$img->save();
				$message = "File downloaded FAILED";
				$this->reportDev('Falló descarga de Archivo ID: '.$img->id);
			}
		} else {
			$result = 0;
			$message = "Obj Archivo con ID: {$id_archivo} no encontrado.";
			$img = new \stdClass();
		}

		$response = Array(
			'result' => $result,
			'message' => $message,
			'data' => $img->toArray()
		);

		return response()->json( $response );
	}

	public function image_resize( $imagen, &$message = ''  ) {
		// ---------- THUMBNAIL SIZE

		$w = Imagen::DIMENSION_MAX_WIDTH; // 1024;
		$h = Imagen::DIMENSION_MAX_HEIGHT; // 1024;
//		$tw = 260;
//		$th = 120;
		$tw = Imagen::DIMENSION_MAX_WIDTH_THUMB; // 305;
		$th = Imagen::DIMENSION_MAX_HEIGHT_THUMB; // 150;

		$resized_imgs = [];
		$failed_imgs = [];

		$file = storage_path( $imagen->ruta );

		$filename = pathinfo( $file, PATHINFO_BASENAME);
		$newname = str_replace('.', '_res.', $filename);
		$resized = $this->resize_source( $file, $w, $h, false, $newname, $message );


		if( !$resized ) {
//			$resized_imgs[] = $file;
//		} else {
			$failed_imgs[] = $file;
//			return false;
		}


		$filename = pathinfo( $file, PATHINFO_BASENAME);
		$newname = str_replace('.', '_thumb.', $filename);
		$thumb = $this->resize_source( $file, $tw, $th, false, $newname, $message );

		if( !$thumb ) {
//			$resized_imgs[] = $file;
//		} else {
			$failed_imgs[] = $file;
//			return false;
		}

//		sleep(1);
		return true;

//		$response = Array(
//			'result' => true,
//			'message' => 'Imágenes redimensionadas a nueva ubicacion.',
//			'data' => [
//				'success' => $resized_imgs,
//				'failed' => $failed_imgs,
//			]
//		);
//
//		return response()->json( $response );
	}

	/**
	 * Ref.: https://stackoverflow.com/a/60630053/2367718
	 * @param $file
	 * @param $w
	 * @param $h
	 * @param bool $crop
	 * @param string $newname
	 * @param string $message
	 * @return bool|mixed
	 */
	public function resize_source( $file, $w, $h, $crop = false, $newname = '', &$message = '' ) {

		try {
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$ext = strtolower($ext);

			$mime_ext = mime_content_type ( $file );
			logger('$mime_ext: '.$file);
			logger($mime_ext);

			switch( $mime_ext ) {
				case 'image/jpg':
				case 'image/jpeg':
					$ext = 'jpeg';
					break;
				case 'image/png':
					$ext = 'png';
					break;
				case 'image/gif':
					$ext = 'gif';
					break;
			}

			list($width, $height) = getimagesize( $file );
			// if the image is smaller we dont resize

			if ($w > $width && $h > $height) {
				$message .= ' La imagen es menor a '.$w.', no se redimensionará.';
				return true;
			}
			$r = $width / $height;
			if ($crop) {
				if ($width > $height) {
					$width = ceil($width - ($width * abs($r - $w / $h)));
				} else {
					$height = ceil($height - ($height * abs($r - $w / $h)));
				}
				$newwidth = $w;
				$newheight = $h;
			} else {
				if ($w / $h > $r) {
					$newwidth = $h * $r;
					$newheight = $h;
				} else {
					$newheight = $w / $r;
					$newwidth = $w;
				}
			}
			$dst = imagecreatetruecolor($newwidth, $newheight);

			switch ($ext) {
				case 'jpg':
				case 'jpeg':
					$src = imagecreatefromjpeg($file);
					break;
				case 'png':
					$src = imagecreatefrompng($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
				case 'gif':
					$src = imagecreatefromgif($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
//				case 'bmp':
//					$src = imagecreatefrombmp($file);
					break;
				default:
					$message.= ' Unsupported image extension found: ' . $ext;
					return false;
			}
			$result = imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			if( $newname != '' ) {
				$filename = pathinfo( $file, PATHINFO_BASENAME);
				$file = str_replace($filename, $newname, $file);
			}

			switch ($ext) {
				case 'bmp':
					imagewbmp($dst, $file);
					break;
				case 'gif':
					imagegif($dst, $file);
					break;
				case 'jpg':
				case 'jpeg':
					imagejpeg($dst, $file);
					break;
				case 'png':
					imagepng($dst, $file);
					break;
			}

			imagedestroy($dst);
			imagedestroy($src);

			return $file;

		} catch (Exception $err) {
			// LOG THE ERROR HERE
			$message .= $err->getMessage();
			logger($message);
			return false;
		}
	}

	public function pull_precios(Request $request) {

		$this->validate( $request, [
			'skus' => 'required',
		]);

		logger("skus:");
		logger($request['skus']);

		$skus = $request['skus'];

//		$response = $this->cargar_precios_desde_planilla($skus);
		$response = $this->cargar_precios_desde_database($skus);

		$response = Array(
			'result' => true,
			'message' => '.',
			'data' => $response
		);

		return response()->json( $response );
	}

	public function cargar_precios_desde_planilla( $skus ) {

		$post = [
			'skus' => implode(',', $skus),
		];

		$url = 'https://script.google.com/macros/s/AKfycbxRkkR--kvQKNXzKEpUaFMeEazA2MwKShXCZBpKCoIKTf58aCOV/exec';
//		$url = 'https://script.google.com/macros/s/AKfycby2oYg37lCL1K5Dfd_wnT_giUqdf7jjQI-h3uy8LtFT/dev';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $post ));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POST, true);

		logger($post);
//		logger(http_build_query( $post ));

		// execute!
		$response = curl_exec($ch);

		$response = json_decode($response, true);

		logger(print_r($response, true));

		// close the connection, release resources used
		curl_close($ch);

		return $response;
	}

	public function cargar_precios_desde_database( $skus ) {

		logger($skus);
		$post = [
			'skus' => implode(',', $skus),
		];

		$skus_string = implode("','", $skus);
		$query = "Select codigo, expoprissasek, expoprissausd, exproprissaclp FROM csv_import WHERE codigo IN ('" . $skus_string . "')";

		$query_result = DB::select( DB::raw( $query ) );
		logger($query_result);

//		$result_array = array_map(function($item) {
//			$ar = [];
//			$item = (array) $item;
//			return $item['codigo'] = [
//				'sek' => $item['expoprissasek'],
//				'usd' => $item['expoprissausd'],
//				'clp' => $item['exproprissaclp'],
//			];
//		}, $query_result );


		$result_array = array_map( function ( $val ) {
			return json_decode( json_encode( $val ), true );
		}, $query_result);

		$result2 = [];

		foreach($result_array as $row ) {
			if( !isset( $result2[ (string) $row['codigo'] ] ) ) {
				$result2[ (string) $row['codigo'] ] = [];
			}
			$result2[ (string) $row['codigo'] ][] = [
				'sek' => (int) str_replace('.', '', $row['expoprissasek']),
				'usd' => (int) str_replace('.', '', $row['expoprissausd']),
				'clp' => (int) str_replace('.', '', $row['exproprissaclp']),
			];;
		}

		logger("PARSED RESULT");
		logger($result2);

//		$url = 'https://script.google.com/macros/s/AKfycbxRkkR--kvQKNXzKEpUaFMeEazA2MwKShXCZBpKCoIKTf58aCOV/exec';
////		$url = 'https://script.google.com/macros/s/AKfycby2oYg37lCL1K5Dfd_wnT_giUqdf7jjQI-h3uy8LtFT/dev';
//		$ch = curl_init($url);
//		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $post ));
//		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//		curl_setopt($ch, CURLOPT_POST, true);
//
//		logger($post);
////		logger(http_build_query( $post ));
//
//		// execute!
//		$response = curl_exec($ch);
//
//		$response = json_decode($response, true);
//
//		logger(print_r($response, true));
//
//		// close the connection, release resources used
//		curl_close($ch);



		$result = [
			'result' => 1,
			'message' => $post['skus'],
			'data' => $result2
		];

		return $result;
	}

	public function importar_precios( Request $request ) {

		$this->validate( $request, [
			'idProducto' => 'required|numeric',
			'idProductoItem' => 'required|numeric'
		]);

		$idProducto = $request['idProducto'];
		$idProductoItem = $request['idProductoItem'];

		$pis = [];
		$skus = [];
		if( $idProductoItem > 0 ) {
			$pi = ProductoItem::findOrFail( $idProductoItem );
			$skus = [ $pi->sku ];
			$pis = collect([ $pi ]);
		} elseif( $idProducto > 0 ) {

			$prod = Producto::findOrFail($idProducto );
			$pis = $prod->ProductoItems;
			foreach($pis as $pi) {
				$skus[] = $pi->sku;
			}
		}

		logger('################################# LOADING PRICES ');

//		$response = $this->cargar_precios_desde_planilla($skus);
		$response = $this->cargar_precios_desde_database($skus);

		if( $response['result'] == 1 ) {
			$data = $response['data'];
			foreach( $data as $sku => $obj ) {
				$pi = $pis->where('sku', $sku)->first();
				logger( $pi->toArray() );

				if( count($obj) > 1 ) {
					$obj_str = print_r( $obj, true );
					$this->reportDev('Hay mas de un precio en listado para SKU: ' . $sku .'. Utilizando el primero de la lista. ' . $obj_str );
				}
				if( count($obj) > 0 ) {
					$obj = $obj[0];

					$m_sek = $pi->Precios->where('moneda', 'sek')->first();
					if( $m_sek ) {
						$m_sek->precio = $obj['sek'];
						$m_sek->save();
					} else {
						$pi->Precios()->save( Precio::create([
							'idTipo' => Precio::TIPO_PRECIO_VENTA,
							'precio' => $obj['sek'],
							'moneda' => 'sek',
							'estado' => 1,
						]) );
					}

					$m_usd = $pi->Precios->where('moneda', 'usd')->first();
					if( $m_usd ) {
						$m_usd->precio = $obj['usd'];
						$m_usd->save();
					} else {
						$pi->Precios()->save( Precio::create([
							'idTipo' => Precio::TIPO_PRECIO_VENTA,
							'precio' => $obj['usd'],
							'moneda' => 'usd',
							'estado' => 1,
						]) );
					}

					$m_clp = $pi->Precios->where('moneda', 'clp')->first();
					if( $m_clp ) {
						$m_clp->precio = $obj['clp'];
						$m_clp->save();
					} else {
						$pi->Precios()->save( Precio::create([
							'idTipo' => Precio::TIPO_PRECIO_VENTA,
							'precio' => $obj['clp'],
							'moneda' => 'clp',
							'estado' => 1,
						]) );
					}
				}

			}

			$response = Array(
				'result' => true,
				'message' => 'Precios descargados.',
				'data' => $data
			);
			return response()->json( $response );
		}

		$response = Array(
			'result' => false,
			'message' => 'Descarga de precios fallida',
			'data' => []
		);

		return response()->json( $response );
	}
}
