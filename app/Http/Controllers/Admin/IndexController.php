<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cliente;
use App\Models\Producto;
use App\Models\Publicacion;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		return view('admin.pages.index');
    }

    public function home(){

		$totalVisitors = Publicacion::sum('visitas');
		$totalAnuncios = Publicacion::all()->count();
		$totalClientes = Cliente::all()->count();
		$totalProductos = Producto::all()->count();

		$masVisitados = Publicacion::with([
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Galerias',
		])
			->has('Producto')
			->orderBy('visitas', 'desc')
			->limit(10)->get();


		$masRecientes = Publicacion::with([
			'Producto.Marca',
			'Producto.Modelo',
			'Producto.Galerias',
		])
			->has('Producto')
			->orderBy('created_at', 'desc')
			->limit(10)->get();

		return view('admin.pages.home', [
			'totalVisitors' => $totalVisitors,
			'totalAnuncios' => $totalAnuncios,
			'totalClientes' => $totalClientes,
			'totalProductos' => $totalProductos,
			'masVisitados' => $masVisitados,
			'masRecientes' => $masRecientes,
		]);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function changeLanguage(Request $request){
		$this->validate($request, [
			'lang' => 'required'
		]);

		session(['lang' => $request['lang']]);

		$response = Array(
			'result' => true,
			'message' => 'Language selected.',
			'data' => []
		);

		return response()->json($response);
	}

	public function auth_as( $id ) {
		$u = Usuario::find( $id );
		Auth::guard('clientes')->login( $u );
		$token = str_random(32);
		$u->impersonation_token = $token;
		$u->save();
		return redirect()->route("public.auth_as", [ $token ]);
	}

	public function check_product_thumbs() {

		$scan = scandir(storage_path('app/published'));

//		print "<pre>";
//		print_r($scan);

		$alldirs = [];

		foreach( $scan as $dir ) {
			if( !in_array($dir, ['.','..']) ) {
				if( is_dir(storage_path('app/published/'.$dir)) ) {
					$files = scandir(storage_path('app/published/'.$dir));
					if( count($files) > 0 ) {

						$files_list = [];
						$thumb_found = false;

						foreach($files as $file ) {
							if( !in_array($file, ['.','..']) ) {
//								print $file ."<br>";
								$files_list[] = $file;

								if( preg_match("/thumb/", $file) ) {
									$thumb_found = true;
								}
							}
						}

						if(!$thumb_found && count($files_list) > 0) {
							print '====================================================================== NOTHUMB<br>';
							print $dir . " - " . storage_path('app/published/'.$dir) . "<br>";
							$alldirs[] = $dir;

							print implode('<br>', $files_list);

							print "<br>";
//						} else {
//							print '======================================================================<br>';
						}



					}
				}
			}
		}

		print_r( implode( ',', $alldirs ) );

	}
}
