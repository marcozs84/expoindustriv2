<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoriaDefinition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryDefinitionController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.pages.categoryDefinition.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idCategoria) {
    	$categoryDefinition = CategoriaDefinition::where('idCategoria', $idCategoria)->first();
        return view('admin.pages.categoryDefinition.show', [
        	'catd' => $categoryDefinition
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idCategoria) {
		$categoryDefinition = CategoriaDefinition::where('idCategoria', $idCategoria)->first();
		return view('admin.pages.categoryDefinition.edit', [
			'catd' => $categoryDefinition
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function getFormData($idCategoriaDefinition) {
		$categoryDefinition = CategoriaDefinition::findOrFail($idCategoriaDefinition);

		$response = Array(
			'result' => true,
			'message' => '.',
			'data' => $categoryDefinition
		);

		return response()->json($response);
	}
}
