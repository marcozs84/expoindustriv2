<?php

namespace App\Http\Controllers\Admin;

use App\Models\Convenio;
use App\Models\Permiso;
use App\Models\Role;
use App\Models\RolePermiso;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PermisoController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$selectedNav = 'administracion.permiso';
		return view('admin.pages.permiso.index', [
			'selectedNav' => $selectedNav
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.permiso.create', [
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$permiso = Permiso::findOrFail($id);

		$rps = RolePermiso::with(['Role'])->where('idPermiso', $id)->get();
		$rolesP = [];
		foreach($rps as $rp){
			if(!isset($rolesP[$rp->Role->nombre])){
				$rolesP[$rp->Role->id] = [];
			}
//			if($rp->Convenio){
//				if(!isset($rolesP[$rp->Role->id][$rp->Convenio->nombreCorto])){
//					$rolesP[$rp->Role->id][] = $rp->Convenio->nombreCorto;
//				}
//			}

		}

		$roles = Role::all()->pluck('nombre', 'id')->toArray();

		return view('admin.pages.permiso.edit', [
			'permiso' => $permiso,
			'roles' => $roles,
			'rolesP' => $rolesP
		]);
    }

	public function arbol(){
		return Permiso::getArbol();
	}

}
