<?php

namespace App\Http\Controllers\Admin;

use App\Models\Producto;
use App\Models\ProductoItem;
use App\Models\Proveedor;
use App\Models\Tag;
use App\Models\Traduccion;
use Google\Cloud\Translate\V2\TranslateClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TraduccionController extends Controller {

	public function multiline(Request $request, $owner, $owner_id, $campo) {

		return $this->process( $request, $owner, $owner_id, $campo, 'multiline' );
	}

	public function singleline(Request $request, $owner, $owner_id, $campo) {

		return $this->process( $request, $owner, $owner_id, $campo, 'singleline' );
	}

	public function process(Request $request, $owner, $owner_id, $campo, $type) {

//		$obj = Publicacion::with([
//			'Producto.ProductoItem.Traducciones'
//		])
//			->find($owner_id);

		$ck_enabled = false;
		if( isset($_GET['ck_enabled']) && $_GET['ck_enabled'] != '' ) {
			$ck_enabled = ( $_GET['ck_enabled'] == 1 ) ? true : false;
		}


		$es = $se = $en = '';

		switch( $owner ) {
			case 'proveedor' :
				$obj = Proveedor::findOrFail( $owner_id );
				$se = $obj->$campo;
				break;
			case 'producto' :
				$obj = Producto::findOrFail( $owner_id );
				break;
			case 'producto_item' :
				$obj = ProductoItem::findOrFail( $owner_id );
				break;
			case 'tag' :
				$obj = Tag::findOrFail( $owner_id );
				$se = $obj->$campo;
				break;
			default :
				$response = Array(
					'result' => true,
					'message' => 'Modelo inexistente: '.$owner,
					'data' => []
				);

				return response()->json( $response );
		}

		$traducciones = $obj->Traducciones;
//		$definicion = $obj->Producto->ProductoItem->definicion;
//		$se = $definicion[$campo];

		// ----------

		$traduccion = null;

		if( $traducciones && $traducciones->count() > 0){
			if($traducciones->where('campo', $campo)->count() > 1){
				$this->reportDev("Multiples traducciones para Publicacion:{$owner_id}, campo:{$campo}");
			}
			if($traducciones->where('campo', $campo)->count() > 0){
				$traduccion = $traducciones->where('campo', $campo)->first();
				$es = $traduccion->es;
				$en = $traduccion->en;
				$se = $traduccion->se;
			} else {
				$traduccion = new Traduccion();
				$traduccion->campo = $campo;
				$traduccion = $obj->Traducciones()->save($traduccion);
			}
		} else {
			$traduccion = new Traduccion();
			$traduccion->campo = $campo;
			$traduccion = $obj->Traducciones()->save($traduccion);
		}


		if($request->isMethod('post')) {

			if(!$traduccion){
				$traduccion = new Traduccion();
				$traduccion->campo = $campo;
				$traduccion->en = trim($request['en']);
				$traduccion->es = trim($request['es']);
				$traduccion->se = trim($request['se']);
				$obj->Traducciones()->save($traduccion);
			} else {
				$traduccion->en = trim($request['en']);
				$traduccion->es = trim($request['es']);
				$traduccion->se = trim($request['se']);
				$traduccion->save();
			}

			$response = Array(
				'result' => true,
				'message' => 'Registro guardado correctamente.',
				'data' => $traduccion->toArray()
			);

			return response()->json($response);
		}

		return view("admin.pages.traduccion.{$type}", [
			'owner' => $owner,
			'owner_id' => $owner_id,
			'campo' => $campo,
			'traduccion' => $traduccion,
			'es' => $es,
			'se' => $se,
			'en' => $en,
			'ck_enabled' => $ck_enabled,
		]);

	}

	public function gtranslate(Request $request) {
		$this->validate($request, [
			'idTraduccion' => 'required',
			'from' => 'required',
			'to' => 'required',
			'text' => 'required',
		]);

//		logger( storage_path('app/exind-temp-85c435c79c70.json'));
//		$json_path = storage_path('app/exind-temp-85c435c79c70.json');
		$json_path = storage_path('app/expoindustri-translate-f8c3037baea3.json');
		putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $json_path);

		$valid_from = $this->valid_languages( $request['from'] );
		$valid_to = $this->valid_languages( $request['to'] );

		$text = trim( $request['text'] );

//		$traduccion = Traduccion::findOrFail( $request['idTraduccion'] );
//		$traduccion->$to

		$translate = new TranslateClient();
		$result = $translate->translate($text, [
			'source' => $valid_from,
			'target' => $valid_to,
			'format' => 'html',
			'model' => 'base',
		]);

		logger($result);

		$response = Array(
			'result' => true,
			'message' => '.',
			'data' => $result
		);

		return response()->json( $response );

	}

	public static function valid_languages ( $input ) {
		switch( $input ) {
			case 'se' :
				return 'sv';
				break;
			default :
				return $input;
				break;
		}
	}
}
