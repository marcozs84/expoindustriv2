<?php

namespace App\Http\Controllers\Admin;

use App\Models\Agenda;
use App\Models\Cliente;
use App\Models\Empleado;
use App\Models\Galeria;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Proveedor;
use App\Models\ProveedorContacto;
use App\Models\Role;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class UsuarioController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

    	$estados = ListaItem::where('idLista', Lista::USUARIO_ESTADO)->get()->pluck('texto', 'valor')->toArray();

//    	if( Gate::allows('admin.usuario.restore') ) {
			$estados[100] = 'Eliminados';
//		}

		return view('admin.pages.usuario.index', [
			'estados' => $estados
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {

		$idTipo = null;
		if( isset($request['tipo']) ) {
			if($request['tipo'] == 'cliente') {
				$idTipo = Usuario::TIPO_CLIENTE;
			} elseif($request['tipo'] == 'empleado') {
				$idTipo = Usuario::TIPO_EMPLEADO;
			} elseif($request['tipo'] == 'administrador') {
				$idTipo = Usuario::TIPO_ADMINISTRADOR;
			}
		}

		$proveedores = Proveedor::all()->pluck('nombre', 'id');


    	$roles = Role::all()->pluck('nombre', 'id')->toArray();
        return view('admin.pages.usuario.create', [
        	'roles' => $roles,
			'idTipo' => $idTipo,
			'proveedores' => $proveedores,
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

	public function store(Request $request) {

		$this->validate($request, [
			'username' => 'required',
//			'password' => 'confirmed',
			'nombres' => '',
			'apellidos' => '',
			'cargo' => '',
			'idTipo' => 'numeric',
			'idRole' => 'numeric',
			'idEstado' => 'numeric',
		]);

		if( isset($request['password']) && trim($request['password'] != '') ) {
			$this->validate($request, [
				'password' => 'confirmed',
			]);
		} else {
			$request['password'] = 'expo_324';
		}

		$usuarios = Usuario::where('username', trim($request['username']))->get();
		if($usuarios->count() > 0) {
			$response = [
				'username' => [
					'Ya existe un usuario utilizando esta cuenta.'
				]
			];
			return response()->make($response, 422);
		}

		if(trim($request['username']) == '---') {
			$response = [
				'username' => [
					'Nombre de usuario inválido.'
				]
			];
			return response()->make($response, 422);
		}

//		$modelo = Usuario::create($request->all());

		if($request['idTipo'] == Usuario::TIPO_CLIENTE) {
			$usuario = $this->createCliente($request);
		} elseif (in_array($request['idTipo'], [ Usuario::TIPO_ADMINISTRADOR, Usuario::TIPO_EMPLEADO ])) {
			$usuario = $this->createEmpleado($request);
		} else {
			$response = [
				'Calendario' => [
					'Tipo de acceso no definido.'
				]
			];
			return response()->make($response, 422);
		}


		if($usuario){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $usuario
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);
    }

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

		$usuario = Usuario::with([
			'Roles',
			'Owner.Agenda.Telefonos',
		])->findOrFail($id);

		$roles = Role::all()->pluck('nombre', 'id')->toArray();

		$permisos = $usuario->Permisos();

		if ( get_class($usuario->Owner) == 'App\Models\ProveedorContacto' ) {
			$tipos = [
				Usuario::TIPO_CLIENTE => 'Cliente',
			];
		} else {
			$tipos = [
				Usuario::TIPO_EMPLEADO => 'Empleado',
				Usuario::TIPO_ADMINISTRADOR => 'Administrador',
			];
		}

        return view('admin.pages.usuario.show', [
			'usuario' => $usuario,
			'roles' => $roles,
			'permisos' => $permisos,
			'tipos' => $tipos,
		] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

		$usuario = Usuario::with([
			'Roles',
			'Owner.Agenda.Telefonos',
		])->findOrFail($id);


		$roles = Role::all()->pluck('nombre', 'id')->toArray();

		$permisos = $usuario->Permisos();

//		$permisosAr = [];
//		foreach($permisos as $permiso){
//			if(!isset($permisosAr[$permiso->nombre])){
//				$permisosAr[$permiso->nombre][] = '';
//			}
//		}

		if ( get_class($usuario->Owner) == 'App\Models\ProveedorContacto' ) {
			$tipos = [
				Usuario::TIPO_CLIENTE => 'Cliente',
			];
		} else {
			$tipos = [
				Usuario::TIPO_EMPLEADO => 'Empleado',
				Usuario::TIPO_ADMINISTRADOR => 'Administrador',
			];
		}


        return view('admin.pages.usuario.edit', [
			'usuario' => $usuario,
			'roles' => $roles,
			'permisos' => $permisos,
			'tipos' => $tipos,
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

		$this->validate($request, [
			'username' => '',
			'password' => 'confirmed',
			'nombres' => '',
			'apellidos' => '',
			'cargo' => '',
			'idTipo' => 'numeric',
			'idRole' => 'numeric',
			'idEstado' => 'numeric',
		]);

		$usuarios = Usuario::where('username', trim($request['username']))
			->where('id', '<>', $id)
			->get();

		if($usuarios->count() > 0) {
			$response = [
				'username' => [
					'Ya existe un usuario utilizando esta cuenta.'
				]
			];
			return response()->make($response, 422);
		}

    	$usuario = Usuario::findOrFail($id);

    	$usuario->username = trim($request['username']);
    	$usuario->email = trim($request['username']);
    	$usuario->cargo = trim($request['cargo']);
		if(isset($request['password']) && trim($request['password']) != ''){
			$usuario->password = bcrypt(trim($request['password']));
		}
		$usuario->idTipo = $request['idTipo'];
		$usuario->estado = $request['idEstado'];

		if(isset($request['empresa']) && $request['empresa'] > 0) {
			$usuario->Owner->idProveedor = (int) $request['empresa'];
			$usuario->Owner->save();
		}

		$usuario->save();

		$agenda = $usuario->Owner->Agenda;
		$agenda->nombres = trim($request['nombres']);
		$agenda->apellidos = trim($request['apellidos']);
		$agenda->nit = trim($request['nit']);
		$agenda->save();

		if($agenda->Telefonos->count() > 0) {
			$telefono = $agenda->Telefonos[0];
			$telefono->numero = trim($request['telefono']);
			$telefono->save();
		} else {
			$agenda->Telefonos()->create([
				'numero' => trim($request['telefono']),
				'descripcion' => '',
				'estado' => 1,
			]);
		}


		if ( get_class($usuario->Owner) == 'App\Models\ProveedorContacto' ) {
//			$proveedor = $usuario->Owner->Proveedor;
//			$proveedor->nombre = trim($request['empresa']);
//			$proveedor->nit = trim($request['nit']);
//			if(trim($request['telefono']) != '') {
//				$telefono = $proveedor->Telefonos->first();
//				$telefono->numero = trim($request['telefono']);
//				$telefono->save();
//			}
//			$proveedor->save();
		}

		if($usuario){
			$response = Array(
				'result' => true,
				'message' => 'Registro actualizado.',
				'data' => $usuario
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

	// ------------------------

	public function verificarTieneUsuario(Request $request){

		$this->validate($request, [
			'idAgenda' => 'required|integer|min:1'
		]);

		if(isset($request['idUsuario']) && $request['idUsuario'] > 0){
			$idUsuario = (int)$request['idUsuario'];
			$agenda = Agenda::with(['Usuario' => function($q) use($idUsuario){
				$q->where('id', '<>', $idUsuario);
			}])
				->where('id', $request['idAgenda'])
				//->whereHas('Usuario', function($q) use($idUsuario){
				//	$q->where('id', '<>', $idUsuario);
				//})
				->first();
		} else {
			$agenda = Agenda::with('Usuario')->findOrFail($request['idAgenda']);
		}


		if($agenda->Usuario){
			$tieneUsuario = true;
		} else {
			$tieneUsuario = false;
		}

		if($tieneUsuario){
			$response = Array(
				'result' => true,
				'message' => 'Usuario '.$agenda->nombres_apellidos.' tiene un usuario existente.',
				'data' => [
					'idUsuario' => $agenda->Usuario->id
				]
			);

		} else {
			$response = [
				'result' => false,
				'message' => 'No tiene Usuario relacionado',
				'data' => $agenda
			];
		}
		return response()->json($response);
	}

	public function asignarRoles(Request $request){
		$this->validate($request, [
			'idUsuario' => 'required|numeric',
			'idRoles' => 'array'
		]);

		if(!isset($request['idRoles'])) {
			$request['idRoles'] = [];
		}

		$usuario = Usuario::findOrFail($request['idUsuario']);

		$usuario->Roles()->sync($request['idRoles']);

		$response = Array(
			'result' => true,
			'message' => 'Roles actualizados.',
			'data' => $usuario->Roles->pluck('id')->toArray()
		);
		return response()->json($response);

	}

	private function createCliente($request) {

		$idEmpresa = (int) $request['empresa'];

		if($idEmpresa > 0) {
			$prov = Proveedor::findOrFail($request['empresa']);
		} else {
			/**
			 * Si no se selecciona una empresa
			 * es probable que no pertenezca a ninguna,
			 * por lo tanto se crea una empresa SIN NOMBRE
			 */

			$prov = new Proveedor();
			$prov->nombre = $request['empresa'];
			$prov->nit = $request['nit'];
			$prov->save();

			$prov->Telefonos()->create([
				'numero' => '',
				'descripcion' => 'registro',
				'estado' => 1
			]);
		}



		$pc = new ProveedorContacto();
		$pc->idProveedor = $prov->id;
		$pc->idProveedorContactoEstado = 1;
		$pc->idProveedorContactoTipo = 0;
		$pc->save();

		$agenda = $pc->Agenda()->create([
			'nombres' => $request['nombres'],
			'apellidos' => $request['apellidos'],
//			'pais' => $request['pais'],
		]);

		$agenda->Correos()->create([
			'correo' => trim($request['username']),
			'descripcion' => 'login',
			'estado' => 1
		]);

		$agenda->Telefonos()->create([
			'numero' => $request['telefono'],
			'descripcion' => 'registro',
			'estado' => 1
		]);

		$usuario = $pc->Usuario()->create([
			'username' => trim($request['username']),
			'email' => trim($request['username']),
			'password' => bcrypt(trim($request['password'])),
			'estado' => $request['idEstado'],
			'idTipo' => $request['idTipo'],
			'cargo' => 'Cliente',
			'confirmation_token' => ''
		]);

		$pc->idUsuario = $usuario->id;
		$pc->idAgenda = $agenda->id;
		$pc->save();
		$cliente = Cliente::create();

		$cliente->idAgenda = $agenda->id;
		$cliente->idUsuario = $usuario->id;
		$cliente->save();

		$usuario = $cliente->Usuario;

		return $usuario->load([
			'Owner.Agenda.Telefonos',
			'Owner.Agenda.Correos',
			'Cliente.Agenda',
			'Owner.Proveedor.Telefonos',
			'Owner.Proveedor.Direcciones',
			'Owner.Proveedor.ProveedorContactos.Usuario',
			'Owner.Proveedor.ProveedorContactos.Agenda',
		]);
	}

	private function createEmpleado($request) {
		$empleado = new Empleado();
		$empleado->idEmpleadoTipo = 0;
		$empleado->idEmpleadoEstado = 1;
		$empleado->salario = 0;
		$empleado->save();

		$agenda = $empleado->Agenda()->create([
			'nombres' => $request['nombres'],
			'apellidos' => $request['apellidos'],
//			'pais' => $request['pais'],
		]);

		$agenda->Correos()->create([
			'correo' => trim($request['username']),
			'descripcion' => 'login',
			'estado' => 1
		]);

		$agenda->Telefonos()->create([
			'numero' => $request['telefono'],
			'descripcion' => 'registro',
			'estado' => 1
		]);

		$usuario = $empleado->Usuario()->create([
			'username' => trim($request['username']),
			'email' => trim($request['username']),
			'password' => bcrypt(trim($request['password'])),
			'estado' => $request['idEstado'],
			'idTipo' => $request['idTipo'],
			'cargo' => $request['cargo'],
			'confirmation_token' => ''
		]);

		$usuario->Roles()->sync([$request['idRole']]);

		$empleado->idUsuario = $usuario->id;
		$empleado->idAgenda = $agenda->id;
		$empleado->save();

		return $usuario->load([
			'owner.agenda.telefonos',
			'owner.agenda.correos',
		]);
	}

}
