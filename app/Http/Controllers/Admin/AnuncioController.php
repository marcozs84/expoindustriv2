<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoriaDefinition;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Publicacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnuncioController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

    	$estados = ListaItem::where('idLista', Lista::PUBLICACION_ESTADO)->get();
		$estadosMap = $estados->map(function($item, $key){
			return $item->texto = __('messages.'.$item->texto);
		});

		$marcas = Marca::all();
		$marcas = $marcas->pluck('nombre', 'id')->toArray();
		$modelos = Modelo::all();
		$modelos = $modelos->pluck('nombre', 'id')->toArray();
        return view('admin.pages.anuncio.index', [
			'publicacionEstados' => $estados->pluck('texto', 'valor')->toArray(),
			'marcas' => $marcas,
			'modelos' => $modelos,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.pages.anuncio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

    	$anuncio = Publicacion::with([
    		'Producto.ProductoItem',
			'Estado'
		])->findOrFail($id);

    	return view('admin.pages.anuncio.show', [
    		'pub' => $anuncio
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function mapaFormularios(){

    	$cats = CategoriaDefinition::with([
    		'Categoria'
		])->get();

    	foreach($cats as $c){
    		foreach($c->campos as $campo){
    			logger($campo['key']);
			}
		}

    	return view('admin.pages.mapaFormularios', [
    		'formularios' => $cats
		]);
	}
}
