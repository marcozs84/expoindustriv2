<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permiso;
use App\Models\Role;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		return view('admin.pages.role.index', [
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

		return view('admin.pages.role.create', [
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$role = Role::with([
			'Permisos',
		])->findOrFail($id);
		$permisos = Permiso::all();
		$convenios = [];

		$asignaciones = [];
	    $permisos = $role->Permisos()->orderBy('nombre')->get();
		foreach($permisos as $permiso){
			//$asignaciones[] = [
			//	'idRole' => $role->id,
			//	'idPermiso' => $permiso->id,
			//];
			if($permiso->pivot->idConvenio){
				$asignaciones[$permiso->id][] = $permiso->pivot->idConvenio;
			} else {
				$asignaciones[$permiso->id] = [];
			}
		}

		logger($asignaciones);

		$arbol = Permiso::getTree();

		logger($arbol);

		return view('admin.pages.role.edit', [
			'role' => $role,
			'convenios' => $convenios,
			'permisos' => $permisos,
			'arbol' => $arbol,
			'asignaciones' => $asignaciones
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
