<?php

namespace App\Http\Controllers\Admin;

use App\Models\Oferta;
use App\Models\Role;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class OfertaController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$builder = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			});

		if ( Gate::denies( 'admin.oferta.adminTerceros' ) ) {
			$builder->where( 'id', Auth::user()->id );
		} else {
			$builder->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR);
		}

		$vendedores = $builder->get();

		return view('admin.pages.oferta.index', [
			'vendedores' => $vendedores,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {


		// ----------
    	$vendedores = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			})
			->get();

		// ----------

		$user = Auth::guard('empleados')->user();

		$idSelectedVendedor = null;
		if(in_array($user->id, $vendedores->pluck('id')->toArray())) {
			$idSelectedVendedor = $user->id;
		}

    	$seq = Oferta::where('idVendedor', $user->id)
			->max('noOferta');

    	$noOferta = ( !$seq ) ? 1 : $seq + 1;

        return view('admin.pages.oferta.create', [
        	'usuario' => $user,
        	'noOferta' => $noOferta,
			'vendedores' => $vendedores,
			'idSelectedVendedor' => $idSelectedVendedor,
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$oferta = Oferta::with([
			'Vendedor.Owner.Agenda',
			'ProductoItem.Producto',
		])->findOrFail($id);

		return view('admin.pages.oferta.show', [
			'oferta' => $oferta,
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    // ---------- CUSTOM

	public function getNoOferta(Request $request, $id) {
		$seq = Oferta::where('idVendedor', $id)
			->max('noOferta');

		$noOferta = ( !$seq ) ? 1 : $seq + 1;

		$response = Array(
			'result' => true,
			'message' => 'Listo.',
			'data' => $noOferta
		);

		return response()->json($response);
	}

	public function upload(Request $request){
		$rand = $request['randBanner'];
		if($request->hasFile('file')){

			$file = $request->file('file');

			$filename_original = $file->getClientOriginalName();
			$extension_original = $file->getClientOriginalExtension();
			$mimetype = $file->getMimeType();
			$extension = $file->extension();

			$esZip = false;
			$esImagen = false;
			if(in_array($file->getMimeType(), ['application/zip','application/octet-stream','application/x-zip-compressed','multipart/x-zip'])){
				$esZip = true;
			} elseif (in_array($file->getMimeType(), ['image/gif', 'image/jpeg', 'image/png'])) {
				$esImagen = true;
			}

//			if(!$esZip && !$esImagen){
//			$tiposPermitidos = [
//				'gif', 'jpeg', 'jpg', 'png',
//				'txt',
//				'pdf',
//				'xls', 'xlsx',
//				'ppt', 'pptx',
//			];
////			if(!in_array($file->getMimeType(), [
////				'image/gif', 'image/jpeg', 'image/png',
////				'text/plain',
////				'application/pdf',
////				'application/vnd.ms-excel',
////				'application/vnd.ms-powerpoint',
////			])){
//			if(!in_array($extension_original, $tiposPermitidos)){
//				$response = [
//					'error' => 'No es un archivo válido, los tipos aceptados son: ' . implode(', ', $tiposPermitidos)
//				];
//				return response()->make($response, 422);
//			}

			if(!$file->isValid()){
				$response = [
					'error' => 'Archivo INVALIDO'
				];
				return response()->make($response, 422);
			}

			$relativePath = "app".DIRECTORY_SEPARATOR."oferta";
			$destinationPath = storage_path($relativePath);

			if(!is_dir($destinationPath)){
				//$mkdir = mkdir($destinationPath, 0755);
				$mkdir = File::makeDirectory($destinationPath, 0755, true);
			}

			$randChars = $this->randomChars(5);
			$filename = "ofr_{$randChars}.{$extension}";

			$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
			if(File::exists($fullpath)){
				$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
				File::delete($fullpath);
			}

			$file->move($destinationPath, $filename);

			if(!File::exists($fullpath)){
				$response = [
					'error' => 'Archivo no encontrado despues de subir.'
				];
				return response()->make($response, 422);
			}


			// ----------

			$idOferta = (int) $request['oferta_id'];
			$oferta = Oferta::findOrFail($idOferta);

			$archivo = $oferta->Archivos()->create([
				'realname' => $filename_original,
				'filename' => $filename,
				'mimetype' => $mimetype,
				'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
				'estado' => 1,
			]);

			$response = Array(
				'result' => true,
				'message' => 'Archivo subido correctamente.',
				'data' => $archivo
			);
		} else {
			$response = [
				'error' => 'No tiene archivo.'
			];
		}
		return response()->json($response);
	}
}
