<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Modulo;
use App\Models\Template;
use App\Utilities\FakturaRender;
use App\Utilities\TemplateRender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use PDF;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TemplateController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

		$modulos = Modulo::all()->pluck('nombre', 'id')->toArray();
		$templateTipos = ListaItem::listado(Lista::TEMPLATE_TIPO)->get()->pluck('texto', 'valor')->toArray();

        return view('admin.pages.template.index', [
			'modulos' => $modulos,
			'tipos' => $templateTipos,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
		$templateTipos = ListaItem::listado(Lista::TEMPLATE_TIPO)->get()->pluck('texto', 'valor')->toArray();
		$modulos = Modulo::get()->pluck('nombre', 'id');

		return view('admin.pages.template.create', [
			'templateTipos' => $templateTipos,
			'modulos' => $modulos
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $template = Template::findOrFail($id);

		$templateTipos = ListaItem::listado(Lista::TEMPLATE_TIPO)->get()->pluck('texto', 'valor')->toArray();
		$modulos = Modulo::get()->pluck('nombre', 'id');

		return view('admin.pages.template.edit', [
			'template' => $template,
			'templateTipos' => $templateTipos,
			'modulos' => $modulos,
		]);

    }

	public function editor_pdf($id){

		$template = Template::with('Modulo')->findOrFail($id);

		$imagenes = [
			'' => 'Ninguna',
			'app/unap/bg_membrete_certificado_unap.png' => 'app/unap/bg_membrete_certificado_unap.png',
			'app/usal/bg_membrete_certificado_usal.png' => 'app/usal/bg_membrete_certificado_usal.png'
		];

		$bg = '';
		$bg_w = 0;
		$bg_h = 0;
		$bg_x = 0;
		$bg_y = 0;
		$bg_pags = [0];
		$header = '';
		$footer = '';
		$m_top = 0;
		$m_bottom = 0;
		$m_left = 0;
		$m_right = 0;

		$margenes = [];
		$margenes['t'] = 10;
		$margenes['b'] = 10;
		$margenes['l'] = 10;
		$margenes['r'] = 10;

		$extrainfo = $template->extrainfo;
		if(isset($extrainfo['bg']) && trim($extrainfo['bg']) != ''){
			$bg = $extrainfo['bg'];
			$bg_w = (int) $extrainfo['bg_w'];
			$bg_h = (int) $extrainfo['bg_h'];
			$bg_x = (int) (isset($extrainfo['bg_x'])) ? $extrainfo['bg_x'] : 0;
			$bg_y = (int) (isset($extrainfo['bg_y'])) ? $extrainfo['bg_y'] : 0;
			//$bg_pags = (isset($extrainfo['bg_pags'])) ? $extrainfo['bg_pags'] : [0];
			//logger($extrainfo['bg_pags']);
			if(isset($extrainfo['bg_pags'])){
				$bg_pags = $extrainfo['bg_pags'];
				if($extrainfo['bg_pags'][0] === ''){
					$bg_pags = [0];
				}
			}

		}

		if(isset($extrainfo['header'])){
			$header = $extrainfo['header'];
			//$header_mensaje = (isset($extrainfo['header']['mensaje'])) ? $extrainfo['header']['mensaje'] : 0;
			//$head_pos_x = (isset($extrainfo['header']['x'])) ? $extrainfo['header']['x'] : 0;
			//$head_pos_y = (isset($extrainfo['header']['y'])) ? $extrainfo['header']['y'] : 0;
		}

		if(isset($extrainfo['footer'])){
			$footer = $extrainfo['footer'];
			//$footer_mensaje = (isset($extrainfo['footer']['mensaje'])) ? $extrainfo['footer']['mensaje'] : 0;
			//$head_pos_x = (isset($extrainfo['header']['x'])) ? $extrainfo['header']['x'] : 0;
			//$head_pos_y = (isset($extrainfo['header']['y'])) ? $extrainfo['header']['y'] : 0;
		}


		if(isset($extrainfo['margenes'])){
			$margenes['t'] = $extrainfo['margenes']['t'];
			$margenes['b'] = $extrainfo['margenes']['b'];
			$margenes['l'] = $extrainfo['margenes']['l'];
			$margenes['r'] = $extrainfo['margenes']['r'];
		}

		return view('admin.pages.template.editor_pdf', [
			'template' => $template,
			'imagenes' => $imagenes,
			'bg' => $bg,
			'bg_w' => $bg_w,
			'bg_h' => $bg_h,
			'bg_x' => $bg_x,
			'bg_y' => $bg_y,
			'bg_pags' => $bg_pags,
			'header' => $header,
			'footer' => $footer,
			'margenes' => $margenes,
			'sideBarStatus' => 'minified'
		]);
	}

	public function getPDF($id, FakturaRender $renderer){

		$template = Template::findOrFail($id);

		$html = $template->mensaje;

		$extrainfo = $template->extrainfo;

		// -------- PDF CONFIGURATION

		PDF::setHeaderCallback(function ($pdf) use($extrainfo){
			$mostrarBg = false;
			if(isset($extrainfo['bg']) && trim($extrainfo['bg']) != ''){
				$bg_pags = (isset($extrainfo['bg_pags'])) ? $extrainfo['bg_pags'] : [0] ;
				if(count($bg_pags) == 1 && $bg_pags[0] == 0){
					$mostrarBg = true;
				} else {
					if(in_array($pdf->pageNo(), $bg_pags)){
						$mostrarBg = true;
					}
				}

				if($mostrarBg){

					// get the current page break margin
					$bMargin = $pdf->getBreakMargin();
					// get current auto-page-break mode
					$auto_page_break = $pdf->getAutoPageBreak();
					// disable auto-page-break
					$pdf->setAutoPageBreak(false, 0);
					// set bacground image
					$file_bg = $extrainfo['bg'];
					$file_bg = storage_path($file_bg);
					$bg_w = (isset($extrainfo['bg_w'])) ? $extrainfo['bg_w'] : 0 ;
					$bg_h = (isset($extrainfo['bg_h'])) ? $extrainfo['bg_h'] : 0 ;
					$bg_x = (isset($extrainfo['bg_x'])) ? $extrainfo['bg_x'] : 0 ;
					$bg_y = (isset($extrainfo['bg_y'])) ? $extrainfo['bg_y'] : 0 ;

					//PDF::Image($file_bg, 0, 0, 1279, 1654, '', '', '', false, 300, 'C', false, false, 0);
					//PDF::Image($file_bg, 0, 0, 218, 0, '', '', '', false, 300, '', false, false, 0);
					PDF::Image($file_bg, $bg_x, $bg_y, $bg_w, $bg_h, '', '', '', false, 300, '', false, false, 0);

					// restore auto-page-break status
					$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
					// set the starting point for the page content
					$pdf->setPageMark();


					//// Set font
					//$pdf->SetFont('helvetica', 'B', 20);
					//// Title
					//$pdf->Cell(0, 15, 'Something new right here!!!', 0, false, 'C', 0, '', 0, false, 'M', 'M');

				}
			}

			if(isset($extrainfo['header'])){
				//if(1 == 1){
				// Position at 15 mm from bottom
				//$pdf->SetY(-15);
				// Set font
				//$pdf->SetFont('helvetica', 'I', 8);

				$html = '';
				$html = $extrainfo['header']['mensaje'];
				$pdf->writeHtml($html, true, false, true, false, '');
				//// Set font
				//$pdf->SetFont('helvetica', 'B', 20);
				//// Title
				//$pdf->Cell(0, 15, 'Something new right here!!!', 0, false, 'C', 0, '', 0, false, 'M', 'M');

			}
		});

		PDF::setFooterCallback(function ($pdf) use($extrainfo){
			if(isset($extrainfo['footer'])){
			//if(1 == 1){
				// Position at 15 mm from bottom
				$pdf->SetY(-15);
				// Set font
				//$pdf->SetFont('helvetica', 'I', 8);

				$html = '';
				$html = $extrainfo['footer']['mensaje'];

//				$html = preg_replace('/{{ paginas->actual }}/', 					$pdf->pageNo(), $html);
				$html = preg_replace('/{{ paginas->actual }}/', 					$pdf->getAliasNumPage(), $html);
				$html = preg_replace('/{{ paginas->total }}/', 					$pdf->getAliasNbPages(), $html);

				/**
				 * NumPages no funciona porque no conoce el numero de paginas mientras las va creando
				 */
//				$html = preg_replace('/{{ paginas->total }}/', 					$pdf->getNumPages(), $html);

				$pdf->writeHtml($html, true, false, true, false, '');
				//// Set font
				//$pdf->SetFont('helvetica', 'B', 20);
				//// Title
				//$pdf->Cell(0, 15, 'Something new right here!!!', 0, false, 'C', 0, '', 0, false, 'M', 'M');

			}
		});

		PDF::SetTitle('Faktura');

		if(isset($extrainfo['margenes'])){
			PDF::SetMargins($extrainfo['margenes']['l'], $extrainfo['margenes']['t'], $extrainfo['margenes']['r']);
		} else {
			//PDF::SetMargins(10, 10, PDF_MARGIN_RIGHT);
			PDF::SetMargins(10, 10, 10);
		}

		//PDF::SetMargins(10, 40, 10);
		PDF::AddPage();

		// ---------- BACKGROUND IMAGE OLD FASHION
		//if(isset($extrainfo['bg']) && trim($extrainfo['bg']) != ''){
		//	$bMargin = PDF::getBreakMargin();
		//	$auto_page_break = PDF::getAutoPageBreak();
		//	PDF::SetAutoPageBreak(false, 0);
		//	$file_bg = $extrainfo['bg'];
		//	$file_bg = storage_path($file_bg);
		//	$bg_w = (isset($extrainfo['bg_w'])) ? $extrainfo['bg_w'] : 0 ;
		//	$bg_h = (isset($extrainfo['bg_h'])) ? $extrainfo['bg_h'] : 0 ;
		//	$bg_x = (isset($extrainfo['bg_x'])) ? $extrainfo['bg_x'] : 0 ;
		//	$bg_y = (isset($extrainfo['bg_y'])) ? $extrainfo['bg_y'] : 0 ;
		//	//PDF::Image($file_bg, 0, 0, 1279, 1654, '', '', '', false, 300, 'C', false, false, 0);
		//	//PDF::Image($file_bg, 0, 0, 218, 0, '', '', '', false, 300, '', false, false, 0);
		//	PDF::Image($file_bg, $bg_x, $bg_y, $bg_w, $bg_h, '', '', '', false, 300, '', false, false, 0);
		//	PDF::SetAutoPageBreak($auto_page_break, $bMargin);
		//	PDF::setPageMark();
		//}

		//PDF::SetFont('', '', 20);
		//PDF::SetFontSize(30);

		$html = str_replace('{{', '<b>{{', $html);
		$html = str_replace('}}', '}}</b>', $html);

		PDF::writeHTML($html, true, false, true, false, '');
		//PDF::Write(0, 'Hello World');
		//if($guardarArchivo != ''){
		//	return PDF::Output($guardarArchivo, 'F');
		//} else {
		return PDF::Output('certificado.pdf');
		//return $html;
		//}
	}

	public function sendEmail( Request $request ) {

		$this->validate($request, [
			'template_name' => 'required',
			'email' => 'required',
			'subject' => '',
			'message' => '',
		]);

		$mail = [];
		$mail['titulo'] = "";
		$mail['asunto'] = "";
		$mail['to'] = [];
		$mail['cc'] = [];
		$mail['bcc'] = [];
		$mail['attachments'] = [];

		$mail['asunto'] = "Prueba de templates";
		$mail['to'][] = 'marcozs84@gmail.com';

		$template_name = 'frontend.email.'.$request['template_name'];
		if( ! View::exists( $template_name) ) {
			$response = [
				'View' => [
					'La vista no existe. '.$template_name
				]
			];
			return response()->make( $response, 422 );
//			$template_name = 'frontend.email.template_dark';
		}

		$message_text = $request['message'];
		$subject = $request['subject'];


		if(env('APP_ENV') == 'local'){
//			Mail::send('frontend.email.blank', [
			Mail::send($template_name, [
				'titulo' => $subject,
				'content' => $message_text,
				'local' => session('visitor_location', 'en')
			], function($message) use ($mail){
				//$message->from('soporte.ttm@unapvirtual.cl', 'Soporte Académico');
				//$message->sender(Config::get('mail.from.address'), 'Soporte Académico');
				//$message->replyTo(Config::get('mail.from.address'), 'Soporte Académico');
				$message->from('info@expoindustri.com', 'ExpoIndustri');
				$message->replyTo('marco.zeballos+templatetest@expoindustri.com', 'ExpoIndustri');
				$message->subject( $mail['asunto'] );
				$message->to( $mail['to'] );
			});

			if(count(Mail::failures()) > 0){
				$response = Array(
					'result' => false,
					'message' => '.',
					'data' => Mail::failures()
				);

				return response()->json( $response );
			}
//		} else {
//			$headers = "From: " . strip_tags('no-reply@expoindustri.com') . "\r\n";
//			$headers .= "Reply-To: ". strip_tags('no-reply@expoindustri.com') . "\r\n";
////			$headers .= "BCC: marcozs84@gmail.com\r\n";
//			$headers .= "MIME-Version: 1.0\r\n";
//			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//			$sent = mail(implode(',', $mail['to']), $mail['asunto'], view('frontend.email.template_light', [
//				'titulo' => 'ALERTA!',
//				'content' => $message_text,
//			])->render(), $headers);
		}
	}

	public function email() {
		return view('admin.pages.template.email');
	}

}
