<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Resources\ProductoController;
use App\Models\Producto;
use App\Models\Tag;
use App\Models\Traduccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller {

	public function __construct() {
		$this->middleware('auth:empleados');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		return view('admin.pages.tag.index', []);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		$padres = Tag::with(['Traduccion'])->where('idPadre', 0)->get()->pluck('Traduccion.es', 'id')->toArray();
		$padres = [ 0 => 'Ninguno' ] + $padres;
//		array_unshift( $padres, 'Ninguno');
		return view('admin.pages.tag.create', [
			'padres' => $padres,
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'idPadre' => 'required',
			'es' => 'required',
			'en' => 'required',
			'se' => 'required',
		]);

		//$tag = Tag::create($request->all());
		$tag = new Tag();
		$tag->nombre   = '';
		$tag->idPadre  = (int)$request['idPadre'];
		$tag->save();

		$traduccion = new Traduccion();
		$traduccion->campo = 'nombre';
		$traduccion->en = trim($request['en']);
		$traduccion->es = trim($request['es']);
		$traduccion->se = trim($request['se']);
		$tag->Traduccion()->save($traduccion);

		$tag->load('Traduccion');

		if($tag){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $tag
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$tag = Tag::findOrFail($id);

		return view('admin.pages.tag.show', [
			'tag' => $tag,
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$tag = Tag::findOrFail($id);

		$padres = Tag::with(['Traduccion'])->where('idPadre', 0)->get()->pluck('Traduccion.es', 'id')->toArray();

		$padres = [ 0 => 'Ninguno' ] + $padres;
//		array_unshift( $padres, 'Ninguno');

		return view('admin.pages.tag.edit', [
			'tag' => $tag,
			'padres' => $padres,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'idPadre' => 'required',
			'es' => 'required',
			'en' => 'required',
			'se' => 'required',
		]);

		//$tag = Tag::create($request->all());
		$tag = Tag::find($id);
		$tag->idPadre = (int) $request['idPadre'];
		$tag->save();

		$traduccion = $tag->Traduccion;
//		$traduccion->campo = 'nombre';
		$traduccion->en = trim($request['en']);
		$traduccion->es = trim($request['es']);
		$traduccion->se = trim($request['se']);
		$traduccion->save();

		if($tag){
			$response = Array(
				'result' => true,
				'message' => 'Registro creado.',
				'data' => $tag
			);
		} else {
			$response = Array(
				'result' => false,
				'message' => 'No se pudo crear el registro.',
				'data' => $request->all()
			);
		}

		return response()->json($response);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function selector( $idProducto = 0 ) {
		// ---------- TAGS
		$grouped_tags = ProductoController::get_grouped_tags();

		$prod_tags = [];
		if( $idProducto > 0 ) {
			$producto = Producto::findOrFail($idProducto);
			$prod_tags = $producto->Tags->pluck('id')->toArray();
		}

		return view('admin.pages.tag.selector', [
			'grouped_tags' => $grouped_tags,
			'prod_tags' => $prod_tags,
			'idProducto' => $idProducto,
		]);
	}
}
