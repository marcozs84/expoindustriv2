<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ciudad;
use App\Models\Proveedor;
use App\Models\ProveedorContacto;
use App\Models\Ubicacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Stripe;

class ProveedorController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index() {
		return view('admin.pages.proveedor.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$proveedores = Proveedor::all()->pluck('nombre', 'id');
		$ciudades = Ciudad::all()->pluck('nombre', 'id');

		return view('admin.pages.proveedor.create', [
			'proveedores' => $proveedores,
			'ciudades' => $ciudades,
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		$obj = Proveedor::with([
			'ProveedorContactos',
			'ProveedorContactos.Usuario',
			'ProveedorContactos.Agenda',
		])->findOrFail($id);

		$ubicacionPrimaria = null;

//		$obj = ProveedorContacto::with([
//			'Proveedor',
//			'Agenda',
//			'Usuario',
//		])->findOrFail($id);

//		$prov = $obj->Proveedor;

		$descripcion_se = $descripcion_es = $descripcion_en = '';

		$traducciones = $obj->Traducciones;
		if( $traducciones && $traducciones->count() > 0 ) {
			if( $traducciones->where('campo', 'descripcion')->count() > 0 ) {
				$traduccion = $traducciones->where('campo', 'descripcion')->first();
				$descripcion_se = $traduccion->se;
				$descripcion_es = $traduccion->es;
				$descripcion_en = $traduccion->en;
			} else {
				$descripcion_se = $obj->descripcion;
			}
		} else {
			$descripcion_se = $obj->descripcion;
		}

		$ubicacionPrimaria = $obj->Ubicaciones->where('idUbicacionTipo', 1)->first();
		if(!$ubicacionPrimaria) {
			$ubicacionPrimaria = new Ubicacion();
		}

		$ubicacionesSecundarias = [];
		$ubicacionesSecundarias = $obj->Ubicaciones->where('idUbicacionTipo', 2);

//		$linkStripe = '';
//		$charges = [];

		// ---------- STRIPE CUSTOMER INFORMATION
//		if($obj->Usuario->idStripe != ''){
//			$usuario = $obj->Usuario;
//
//			Stripe::setApiKey('sk_test_mntDijCBJikZmtWTWuPC3eB5');	// MZ
//			Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB
//
//			$charges = Charge::all([
//				'customer' => $obj->Usuario->idStripe
//			]);
//
//			$linkStripe = "https://dashboard.stripe.com/test/customers/{$obj->Usuario->idStripe}";
//		}

		// ---------- INFO EMPRESA

//		$cci = null;
//		$cc = ProveedorContacto::with([
//			'Proveedor',
//			'Proveedor.ProveedorContactos'
//		])->where('idUsuario', $obj->Usuario->id)->get();
//		if($cc->count() > 1){
//			$this->reportDev('Hay mas de un ProveedorContacto con idUsuario: '.$obj->Usuario->id);
//		}

//		if($cc->count() > 0){
//			$cci = $cc->first();
//		}

//		logger($cci->toArray());

		return view('admin.pages.proveedor.show', [
			'obj' => $obj,
//			'linkStripe' => $linkStripe,
//			'pagos' => $charges,
//			'cci' => $cci
			'ubicacionPrimaria' => $ubicacionPrimaria,
			'ubicacionesSecundarias' => $ubicacionesSecundarias,
			'descripcion_se' => $descripcion_se,
			'descripcion_es' => $descripcion_es,
			'descripcion_en' => $descripcion_en,
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$proveedor = Proveedor::findOrFail($id);
		$proveedores = Proveedor::all()->pluck('nombre', 'id');
		$ciudades = Ciudad::all()->pluck('nombre', 'id');

		$proveedor->load([
			'Telefonos',
			'Direcciones'
		]);

		if($proveedor->Direcciones->count() > 0) {
			$proveedorDireccion = $proveedor->Direcciones[0]->direccion;
		} else {
			$proveedorDireccion = '';
		}

		return view('admin.pages.proveedor.edit', [
			'proveedor' => $proveedor,
			'proveedores' => $proveedores,
			'ciudades' => $ciudades,
			'proveedorDireccion' => $proveedorDireccion,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
        //
    }
}
