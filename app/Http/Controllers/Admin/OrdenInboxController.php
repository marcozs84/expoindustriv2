<?php

namespace App\Http\Controllers\Admin;

use App\Models\Calculadora;
use App\Models\CategoriaDefinition;
use App\Models\Cotizacion;
use App\Models\Factura;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Orden;
use App\Models\OrdenDetalle;
use App\Models\Producto;
use App\Models\Publicacion;
use App\Models\Role;
use App\Models\Transaccion;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

class OrdenInboxController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

		$id = 0;
    	$counterOrders = Orden::all()->count();

		// ---------- VENDEDORES
		$builder = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			});

		if ( Gate::denies( 'ordenInbox.index.adminTerceros' ) ) {
			$builder->where( 'id', Auth::user()->id );
		} else {
			$builder->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR);
		}

		$vendedores = $builder->get();

		return view('admin.pages.ordenInbox.index', [
			'idMessage' => $id,
			'counterOrders' => $counterOrders,
			'vendedores' => $vendedores,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id = 0) {

		if(!$request->ajax()){
			if($id > 0 ) {
				$counterOrders = Orden::all()->count();

				// ---------- VENDEDORES
				$builder = Usuario::with([
					'Roles',
					'Owner.Agenda',
				])
					->whereHas('Roles', function($q) {
						$q->where('id', Role::VENDEDOR);
					});

				if ( Gate::denies( 'ordenInbox.index.adminTerceros' ) ) {
					$builder->where( 'id', Auth::user()->id );
				} else {
					$builder->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR);
				}

				$vendedores = $builder->get();

				return view('admin.pages.ordenInbox.index', [
					'idMessage' => $id,
					'counterOrders' => $counterOrders,
					'vendedores' => $vendedores,
				]);
			}
		}

    	$orden = Orden::with([
			'OrdenDetalles.ProductoItem',
			'Cliente.Agenda',
			'Cliente.Usuario',
			'Cotizacion',
			'Vendedor',
			'ProductoItems.Producto',
			'ProductoItems.Producto.Publicacion',
			'ProductoItems.Ubicacion',
			'ProductoItems.Producto.Marca',
			'ProductoItems.Producto.Modelo',
		])->findOrFail($id);

		$prodItem = $orden->OrdenDetalles->first()->ProductoItem;
		$prod = $prodItem->Producto;

		$cot = $orden->Cotizacion;
		$pub = null;

		// ----------
		if($cot && in_array($orden->idProducto, [0,''])){
			$cot->idProducto = $cot->Publicacion->Producto->id;
			$cot->save();

			$pub = $cot->Publicacion;
		}

		// ----------

		$def = $prodItem->definicion;
		$primaryKeys = array_keys($def);

		$idCategoria = $prod->Categorias[0]->id;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$primaryData = [];
		foreach($primaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$value['html'] = <<<xxx
				<td class="tblLabel">{$value['text']}</td><td class="tblValue">{$value['value']}</td>
xxx;
				$primaryData[] = $value;
			}
		}

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		$secondaryData = [];
		foreach($secondaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$secondaryData[] = $value;
			}
		}

		// ----------
		$calc = $prod->ProductoItem->Calculadora;
		if(!$calc){
			$calc = new Calculadora();
			$calc->informe_tecnico = 0;
			$calc->seguro_extra = 0;
			$calc->contenedor_compartido = 0;
			$calc->contenedor_exclusivo = 0;
			$calc->envio_abierto = 0;
			$calc->destino_1 = 0;
			$calc->destino_2 = 0;
			$calc->destino_3 = 0;
			$calc->destino_4 = 0;
			$calc->destino_5 = 0;
			$calc->destino_6 = 0;
		}

		$pubEstados = ListaItem::listado(Lista::PUBLICACION_ESTADO)->get()->pluck('texto', 'valor')->toArray();
		$ordenEstados = ListaItem::listado(Lista::ORDEN_ESTADO)->get()->pluck('texto', 'valor')->toArray();

		// ---------- transacciones

		$transacciones = [];
		if(isset($cot->definicion['transacciones'])){
			$transacciones = Transaccion::whereIn('id', $cot->definicion['transacciones'])->get();
		}

		return view('admin.pages.ordenInbox.show',[
			'orden' => $orden,
			'cot' => $cot,
			'pub' => $pub,
			'prod' => $prod,
			'calc' => $calc,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'pubEstados' => $pubEstados,
			'ordenEstados' => $ordenEstados,
			'transacciones' => $transacciones
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function enviarMensajeLibre(Request $request){

    	$this->validate($request, [
    		'mensaje' => 'required',
			'idCotizacion' => 'required'
		]);

		$consultor = Auth::guard('empleados')->user();

    	$c = Cotizacion::find($request['idCotizacion']);
//    	$email = $c->Cliente->Usuario->email;
		$user = $c->Cliente->Usuario;
//    	$email = 'marcozs84@gmail.com';
		$email = $user->username;

		$content = $request['mensaje'];
		$subject = 'Cotización maquinaria';

		Mail::send('frontend.email.template_light', [
			'titulo' => $subject,
			'content' => $content,
			'local' => session('visitor_location', 'en')
		], function ($m) use ( $user, $subject, $consultor ) {
			$m->from('info@expoindustri.com', 'ExpoIndustri');
//			$m->to( 'marco.zeballos@expoindustri.com' );
			$m->to( $user->email );
			$m->bcc( 'marco.zeballos+orderinbox@expoindustri.com', 'Marco Zeballos' );
			$m->replyTo( $consultor->username, $consultor->Owner->Agenda->nombres_apellidos);
			$m->subject( $subject );
		});

//		if(env('APP_ENV') == 'local'){
//			Mail::send( 'frontend.email.template_dark', [
//				'titulo' => $subject,
//				'content' => $content,
//			], function ( $m ) use ( $user, $subject, $consultor ) {
//				$m->from( $consultor->username, 'ExpoIndustri' );
//				$m->to( $user->email );
//				$m->bcc( 'marcozs84@gmail.com', 'Marco Zeballos' );
//				$m->subject( $subject );
//			} );
//		} else {
//			$headers = "From: " . strip_tags($consultor->username) . "\r\n";
//			$headers .= "Reply-To: ". strip_tags($consultor->username) . "\r\n";
//			$headers .= "BCC: marcozs84@gmail.com\r\n";
//			$headers .= "MIME-Version: 1.0\r\n";
//			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//			$sent = mail($email, $subject, view('frontend.email.template_dark', ['content' => $content, 'titulo' => $subject])->render(), $headers);
//		}

		$response = Array(
			'result' => true,
			'message' => 'Mensaje enviado correctamente.',
			'data' => []
		);

		return response()->json($response);

	}

	public function crearOrden(Request $request){

    	$this->validate($request, [
    		'idCotizacion' => 'required',
		]);

    	$cot = Cotizacion::findOrFail($request['idCotizacion']);
    	$pub = Publicacion::findOrFail($cot->idPublicacion);
    	$prod = Producto::with('ProductoItem')->findOrFail($cot->idProducto);
    	$user = Auth::guard('empleados')->user();

    	$ods = OrdenDetalle::where('idProductoItem', $prod->ProductoItem->id)
			->get();

    	if($ods->count() > 0){
			$response = [
				'Orden' => [
					'Ya existe una orden en progreso con este producto. <a href="javascript:;">Ver</a>'
				]
			];
			return response()->make($response, 422);
		} else {

			$o = new Orden();
			$o->idCliente = $cot->idCliente;
			$o->idVendedor = $user->id;
			$o->idOrdenEstado = Orden::ESTADO_ACTIVE;
			$o->idOrdenTipo = 0;
			$o->save();

			$od = new OrdenDetalle();
			$od->idOrden = $o->id;
			$od->idProductoItem = $prod->ProductoItem->id;
			$od->save();

			$response = Array(
				'result' => true,
				'message' => 'Orden creada correctamente. <a href="javascript:;">Ir a la orden</a>.',
				'data' => $o->toArray()
			);

			return response()->json($response);
		}
	}

	public function crearFactura(Request $request, $id) {

		$this->validate($request, [
			'precioFacturado' => 'required',
			'nombreFactura' => 'required',
			'numeroFactura' => 'required',
			'nitFactura' => 'required',
		]);

		$orden = Orden::with([
			'Cliente.Agenda',
			'Cliente.Usuario',
			'Cotizacion',
			'Vendedor',
			'ProductoItems.Producto',
			'ProductoItems.Producto.Publicacion',
			'ProductoItems.Ubicacion',
			'ProductoItems.Producto.Marca',
			'ProductoItems.Producto.Modelo',
		])->findOrFail($id);

		$orden->precioFacturado = $request['precioFacturado'];

		$fa = new Factura();
		$fa->idOrden = $orden->id;
		$fa->serial = $request['numeroFactura'];
		$fa->nombre = $request['nombreFactura'];
		$fa->save();

		$cliente = $orden->Cliente->Agenda;
		$cliente->nit = $request['nitFactura'];
		$cliente->save();

		$response = Array(
			'result' => true,
			'message' => 'Factura generada correctamente.',
			'data' => $orden->toArray()
		);

		return response()->json($response);

		$cot = $orden->Cotizacion;

		// ----------
		if(in_array($cot->idProducto, [0,''])){
			$cot->idProducto = $cot->Publicacion->Producto->id;
			$cot->save();
		}

		// ----------
		$pub = $cot->Publicacion;

		$def = $pub->Producto->ProductoItem->definicion;
		$primaryKeys = array_keys($def);

		$idCategoria = $pub->Producto->Categorias[0]->id;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$primaryData = [];
		foreach($primaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$value['html'] = <<<xxx
				<td class="tblLabel">{$value['text']}</td><td class="tblValue">{$value['value']}</td>
xxx;
				$primaryData[] = $value;
			}
		}

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		$secondaryData = [];
		foreach($secondaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$secondaryData[] = $value;
			}
		}

		// ----------
		$calc = $pub->Producto->ProductoItem->Calculadora;
		if(!$calc){
			$calc = new Calculadora();
			$calc->informe_tecnico = 0;
			$calc->seguro_extra = 0;
			$calc->contenedor_compartido = 0;
			$calc->contenedor_exclusivo = 0;
			$calc->envio_abierto = 0;
			$calc->destino_1 = 0;
			$calc->destino_2 = 0;
			$calc->destino_3 = 0;
			$calc->destino_4 = 0;
			$calc->destino_5 = 0;
			$calc->destino_6 = 0;
		}

		$pubEstados = ListaItem::listado(Lista::PUBLICACION_ESTADO)->get()->pluck('texto', 'valor')->toArray();
		$ordenEstados = ListaItem::listado(Lista::ORDEN_ESTADO)->get()->pluck('texto', 'valor')->toArray();

		// ---------- transacciones

		$transacciones = [];
		if(isset($cot->definicion['transacciones'])){
			$transacciones = Transaccion::whereIn('id', $cot->definicion['transacciones'])->get();
		}

		return view('admin.pages.ordenInbox.factura', [
			'orden' => $orden,
			'cot' => $cot,
			'pub' => $pub,
			'calc' => $calc,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'pubEstados' => $pubEstados,
			'ordenEstados' => $ordenEstados,
			'transacciones' => $transacciones
		]);
	}

	public function factura($id) {
		$orden = Orden::with([
			'Cliente.Agenda',
			'Cliente.Usuario',
			'Cotizacion',
			'Vendedor',
			'ProductoItems.Producto',
			'ProductoItems.Producto.Publicacion',
			'ProductoItems.Ubicacion',
			'ProductoItems.Producto.Marca',
			'ProductoItems.Producto.Modelo',
		])->findOrFail($id);

		$prodItem = $orden->OrdenDetalles->first()->ProductoItem;
		$prod = $prodItem->Producto;

		$cot = $orden->Cotizacion;
		$pub = null;

		$factura = $orden->Facturas->last();

		// ----------
		if($cot && in_array($cot->idProducto, [0,''])){
			$cot->idProducto = $cot->Publicacion->Producto->id;
			$cot->save();

			$pub = $cot->Publicacion;
		}

		// ----------

		$def = $prodItem->definicion;
		$primaryKeys = array_keys($def);

		$idCategoria = $prod->Categorias[0]->id;
		$cd = CategoriaDefinition::where('idCategoria', $idCategoria)->get();
		if($cd->count() == 0){
			$this->reportDev("No se encontró CategoriaDefinition con ID: {$idCategoria}");
		}
		if($cd->count() > 1){
			$this->reportDev("Hay mas de 1 CategoriaDefinition para Categoria: {$idCategoria}");
		}
		$campos = $cd->first()->campos;

		$primaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? true : false ;
		});

		$primaryData = [];
		foreach($primaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$value['html'] = <<<xxx
				<td class="tblLabel">{$value['text']}</td><td class="tblValue">{$value['value']}</td>
xxx;
				$primaryData[] = $value;
			}
		}

		$secondaryFields = array_where($campos, function($value, $key){
			return (isset($value['primary']) && $value['primary'] == 1) ? false : true ;
		});

		$secondaryData = [];
		foreach($secondaryFields as $value){
			if(in_array($value['key'], $primaryKeys)){
				$value['value'] = $def[$value['key']];
				$secondaryData[] = $value;
			}
		}

		// ----------
		$calc = $prodItem->Calculadora;
		if(!$calc){
			$calc = new Calculadora();
			$calc->informe_tecnico = 0;
			$calc->seguro_extra = 0;
			$calc->contenedor_compartido = 0;
			$calc->contenedor_exclusivo = 0;
			$calc->envio_abierto = 0;
			$calc->destino_1 = 0;
			$calc->destino_2 = 0;
			$calc->destino_3 = 0;
			$calc->destino_4 = 0;
			$calc->destino_5 = 0;
			$calc->destino_6 = 0;
		}

		$pubEstados = ListaItem::listado(Lista::PUBLICACION_ESTADO)->get()->pluck('texto', 'valor')->toArray();
		$ordenEstados = ListaItem::listado(Lista::ORDEN_ESTADO)->get()->pluck('texto', 'valor')->toArray();

		// ---------- transacciones

		$transacciones = [];
		if(isset($cot->definicion['transacciones'])){
			$transacciones = Transaccion::whereIn('id', $cot->definicion['transacciones'])->get();
		}

		return view('admin.pages.ordenInbox.factura', [
			'orden' => $orden,
			'cot' => $cot,
			'pub' => $pub,
			'calc' => $calc,
			'primaryData' => $primaryData,
			'secondaryData' => $secondaryData,
			'pubEstados' => $pubEstados,
			'ordenEstados' => $ordenEstados,
			'transacciones' => $transacciones,
			'factura' => $factura,
		]);
	}
}
