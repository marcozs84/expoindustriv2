<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transaccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class UploaderController extends Controller {

	// ---------- CUSTOM

	public function upload(Request $request){

		$this->validate($request, [
			'object' => 'required',
			'object_id' => 'required',
		]);

		$object = ucfirst($request['object']);
		$directory = strtolower($request['object']);
		$object_id = (int) $request['object_id'];

		if($request->hasFile('file')){

			$file = $request->file('file');

			$filename_original = $file->getClientOriginalName();
			$extension_original = $file->getClientOriginalExtension();
			$mimetype = $file->getMimeType();
			$extension = $file->extension();

			$esZip = false;
			$esImagen = false;
			if(in_array($file->getMimeType(), ['application/zip','application/octet-stream','application/x-zip-compressed','multipart/x-zip'])){
				$esZip = true;
			} elseif (in_array($file->getMimeType(), ['image/gif', 'image/jpeg', 'image/png'])) {
				$esImagen = true;
			}

//			if(!$esZip && !$esImagen){
//			$tiposPermitidos = [
//				'gif', 'jpeg', 'jpg', 'png',
//				'txt',
//				'pdf',
//				'xls', 'xlsx',
//				'ppt', 'pptx',
//			];
////			if(!in_array($file->getMimeType(), [
////				'image/gif', 'image/jpeg', 'image/png',
////				'text/plain',
////				'application/pdf',
////				'application/vnd.ms-excel',
////				'application/vnd.ms-powerpoint',
////			])){
//			if(!in_array($extension_original, $tiposPermitidos)){
//				$response = [
//					'error' => 'No es un archivo válido, los tipos aceptados son: ' . implode(', ', $tiposPermitidos)
//				];
//				return response()->make($response, 422);
//			}

			if(!$file->isValid()){
				$response = [
					'error' => 'Archivo INVALIDO'
				];
				return response()->make($response, 422);
			}

			$relativePath = "app".DIRECTORY_SEPARATOR.$directory;
			$destinationPath = storage_path($relativePath);

			if(!is_dir($destinationPath)){
				//$mkdir = mkdir($destinationPath, 0755);
				$mkdir = File::makeDirectory($destinationPath, 0755, true);
			}

			$randChars = $this->randomChars(5);
			$filename = "{$directory}_{$object_id}_{$randChars}.{$extension}";

			$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
			if(File::exists($fullpath)){
				$response = [
					'error' => 'Ya existe un archivo con el mismo nombre, por favor intente nuevamente ó contacte al administrador.'
				];
				return response()->make($response, 422);
//				File::delete($fullpath);
			}

			$file->move($destinationPath, $filename);

			if(!File::exists($fullpath)){
				$response = [
					'error' => 'Archivo no encontrado despues de subir.'
				];
				return response()->make($response, 422);
			}

			// ----------

			$objectPath = "\\App\\Models\\{$object}";
			$object = new $objectPath;
			$object = $object->find($object_id);

			$archivo = $object->Archivos()->create([
				'realname' => $filename_original,
				'filename' => $filename,
				'mimetype' => $mimetype,
				'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
				'estado' => 1,
			]);

			$response = Array(
				'result' => true,
				'message' => 'Archivo subido correctamente.',
				'data' => $archivo
			);
		} else {
			$response = [
				'error' => 'No tiene archivo.'
			];
		}
		return response()->json($response);
	}

}
