<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cliente;
use App\Models\ProveedorContacto;
use App\Models\Subscripcion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Stripe;

class ClienteController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index() {
		return view('admin.pages.cliente.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		$cliente = Cliente::with([
			'Usuario.Subscripciones',
			'Agenda'
		])->findOrFail($id);

		$linkStripe = '';
		$charges = [];
		$userIdStripe = $cliente->Usuario->getStripeId();

		// ---------- STRIPE CUSTOMER INFORMATION
		if($userIdStripe != ''){
			$usuario = $cliente->Usuario;

//			Stripe::setApiKey('sk_test_mntDijCBJikZmtWTWuPC3eB5');	// MZ
			Stripe::setApiKey(env('STRIPE_KEY_SK'));	// JB

			$charges = Charge::all([
				'customer' => $userIdStripe
			]);

			$linkStripe = "https://dashboard.stripe.com/test/customers/{$userIdStripe}";
		}

		// ---------- INFO EMPRESA

		$cci = null;
		$cc = ProveedorContacto::with([
			'Proveedor',
			'Proveedor.ProveedorContactos'
		])->where('idUsuario', $cliente->Usuario->id)->get();
		if($cc->count() > 1){
			$this->reportDev('Hay mas de un ProveedorContacto con idUsuario: '.$cliente->Usuario->id);
		}

		if($cc->count() > 0){
			$cci = $cc->first();
		}

//		logger($cci->toArray());

		return view('admin.pages.cliente.show', [
			'obj' => $cliente,
			'linkStripe' => $linkStripe,
			'pagos' => $charges,
			'cci' => $cci
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
        //
    }

	public function subscripcionesTable($idCliente) {
		$cliente = Cliente::findOrFail($idCliente);
		$subscripciones = [];
		$subscripciones = Subscripcion::where('idUsuario', $cliente->Usuario->id)->orderBy('fechaDesde', 'desc')->get();
		return view('admin.pages.cliente.subscripcionesTable', [
			'subscripciones' => $subscripciones
		]);
	}
}
