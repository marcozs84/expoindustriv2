<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orden;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\OrdenDetalle;
use App\Models\Producto;
use App\Models\ProductoItem;
use App\Models\Role;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class OrdenController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

		$vendedores = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			})
			->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR)
			->get();

        return view('admin.pages.orden.index', [
			'vendedores' => $vendedores,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idProducto = 0) {

		$producto = null;
		if ( $idProducto > 0 ) {
			$producto = Producto::with([
				'ProductoItem.Ubicacion',
				'Categorias.Padre',
				'Marca',
				'Modelo',
				'Publicacion',
				'Galerias',
				'Owner.Owner.Agenda',
			])->findOrFail($idProducto);
		}
		// ----------
		$vendedores = Usuario::with([
			'Roles',
			'Owner.Agenda',
		])
			->whereHas('Roles', function($q) {
				$q->where('id', Role::VENDEDOR);
			})
			->orWhere('idTipo', Usuario::TIPO_ADMINISTRADOR)
			->get();

		// ----------

		$user = Auth::guard('empleados')->user();

		$idSelectedVendedor = null;
		if(in_array($user->id, $vendedores->pluck('id')->toArray())) {
			$idSelectedVendedor = $user->id;
		}

		$ordenEstados = ListaItem::listado(Lista::ORDEN_ESTADO)->get()->pluck('texto', 'valor')->toArray();

		$currency = ListaItem::where('idLista', Lista::MONEDA)->get();
		$moneda = $currency->pluck('texto', 'valor')->toArray();
		foreach($moneda as $k => $v){
			$moneda[$k] = __('messages.'.$v);
		}

    	return view('admin.pages.orden.create', [
			'usuario' => $user,
			'vendedores' => $vendedores,
			'idSelectedVendedor' => $idSelectedVendedor,
			'ordenEstados' => $ordenEstados,
			'moneda' => $moneda,
			'producto' => $producto,
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->validate($request, [
			'idPadre' 				=> 'numeric',
			'idCliente' 			=> 'required|numeric',
			'idCotizacion' 			=> 'numeric',
			'idUbicacionEntrega' 	=> 'numeric',
			'idVendedor' 			=> 'required|numeric',
			'idHorario' 			=> 'numeric',
			'idOrdenTipo' 			=> 'numeric',
			'idOrdenEstado' 		=> 'numeric',
			'idProducto' 			=> 'required|numeric',
			'fechaEntrega' 			=> '',
			'precioTransporte' 		=> 'numeric',
			'precioFacturado' 		=> 'required|numeric',
			'moneda' => '',
			'nombreFactura'			=> '',
			'nit' 					=> '',
		]);

		$idProducto = (int) $request['idProducto'];
		$idUsuarioVendedor = (int) $request['idVendedor'];
		$idUsuario = (int) $request['idCliente'];

		if( $idProducto <= 0 ) {
			$response = [
				'idProducto' => [
					'Producto es requerido.'
				]
			];
			return response()->make($response, 422);
		}

		$prod = Producto::with('ProductoItem')->findOrFail($idProducto);
		$ods = OrdenDetalle::where('idProductoItem', $prod->ProductoItem->id)
			->get();

		if($ods->count() > 0){
			$response = [
				'Orden' => [
					'Ya existe una orden en progreso con este producto.'
				]
			];
			return response()->make($response, 422);
		}

		$o = new Orden();
		$o->idPadre 		= $this->check( $request['idPadre'] );
		$o->idCliente 		= $idUsuario;
		$o->idCotizacion	= $this->check( $request['idCotizacion'], 0 );
		$o->idVendedor 		= $idUsuarioVendedor;
		$o->idHorario 		= $this->check( $request['idHorario'] );
		$o->idOrdenEstado 	= $request['idOrdenEstado']; //Orden::ESTADO_ACTIVE;
		$o->precioFacturado = 0; // Precio facturado más abajo en OrdenDetalle
		$o->nombreFactura 	= $request['nombreFactura'];
		$o->nit = $request['nit'];
		$o->idOrdenTipo = 0;

		if( $request['fechaEntrega'] != '' ) {
			$o->fechaEntrega = Carbon::createFromFormat('d/m/Y', $request['fechaEntrega']);
		}

		$o->save();

		$od = new OrdenDetalle();
		$od->idOrden = $o->id;
		$od->idProductoItem = $prod->ProductoItem->id;
		$od->precioVenta = $request['precioFacturado'];
		$od->monedaVenta = $request['moneda'];
		$od->save();

		$pi = $prod->ProductoItem;
		$pi->idDisponibilidad = ProductoItem::DISPONIBILIDAD_VENDIDO;
		$pi->save();

		$response = Array(
			'result' => true,
			'message' => 'Orden creada correctamente. <a href="javascript:;">Ir a la orden</a>.',
			'data' => $o->load('OrdenDetalles')->toArray()
		);

		return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$orden = Orden::findOrFail($id);

		return view('admin.pages.orden.show', [
			'orden' => $orden,
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $orden = Orden::findOrFail($id);

		return view('admin.pages.orden.edit', [
			'orden' => $orden,
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
