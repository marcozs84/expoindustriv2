<?php

namespace App\Http\Controllers\Admin;

use App\Models\ListaItem;
use App\Models\Lista;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ListaItemController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('admin.pages.listaItem.index', [
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idLista = 0) {

		$valor = 1;
		$orden = 1;
		$lista = null;
    	if($idLista > 0) {
    		$lista = Lista::findOrFail($idLista);
			$lis = ListaItem::where('idLista', $idLista)->get();
			if( $lis->count() > 0 ) {
				$list = $lis->sortByDesc('valor');
				$li = $list->first();
				$valor = (int) $li->valor;
				$valor++;
				$orden = (int) $li->orden;
				$orden++;
			}
		}

    	return view('admin.pages.listaItem.create', [
    		'lista' => $lista,
    		'idLista' => $idLista,
			'valor' => $valor,
			'orden' => $orden,
		]);
    }

	public function create_alt( $idLista = 0 ) {

		$valor = 1;
		$orden = 1;
		$lista = null;
		if ( $idLista > 0 ) {
			$lista = Lista::findOrFail( $idLista );
			$lis = ListaItem::where('idLista', $idLista)->get();
			if( $lis->count() > 0 ) {
				$list = $lis->sortByDesc('valor');
				$li = $list->first();
				$valor = (int) $li->valor;
				$valor++;
				$orden = (int) $li->orden;
				$orden++;
			}
		}

		return view( 'admin.pages.listaItem.create_alt', [
			'lista' => $lista,
			'idLista' => $idLista,
			'valor' => $valor,
			'orden' => $orden,
		] );
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$listaItem = ListaItem::findOrFail($id);

		return view('admin.pages.listaItem.show', [
			'listaItem' => $listaItem,
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $listaItem = ListaItem::findOrFail($id);

		return view('admin.pages.listaItem.edit', [
			'listaItem' => $listaItem,
		]);
    }

    public function edit_alt($id) {

        $listaItem = ListaItem::findOrFail($id);

		return view('admin.pages.listaItem.edit_alt', [
			'listaItem' => $listaItem,
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
