<?php

namespace App\Http\Controllers\Admin;

use App\Models\Excepcion;
use App\Models\LookIp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExcepcionController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.pages.excepcion.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

    	$e = Excepcion::find($id);

    	$e->stackTrace = preg_replace('/\/home\/expoindu\/lvlapp/i', '', $e->stackTrace);

        return view('admin.pages.excepcion.show', [
        	'e' => $e
		]);
    }

    // ---------- CUSTOM

	public function lookup(Request $request) {

    	$ip = $request['ip'];
    	$pais = $request['pais'];

    	$oldies = LookIp::whereIp($ip)->get()->count();
    	if($oldies <= 0) {
			$look = new LookIp();
			$look->ip = $ip;
			$look->pais = $pais;

			$look->save();

			$response = Array(
				'result' => true,
				'message' => 'Added to lookup.',
				'data' => $look
			);

			return response()->json($response);
		} else {
			$response = Array(
				'result' => true,
				'message' => 'Already in look up.',
				'data' => []
			);

			return response()->json($response);
		}
	}
}
