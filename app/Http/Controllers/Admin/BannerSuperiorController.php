<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use App\Models\Categoria;
use App\Models\Galeria;
use App\Models\Imagen;
use App\Models\Lista;
use App\Models\ListaItem;
use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class BannerSuperiorController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$bannerTipos = ListaItem::where('idLista', Lista::BANNER_TIPO)->get()->pluck('texto', 'valor')->toArray();

		return view('admin.pages.bannerSuperior.index', [
//			 'sidebarHide' => true,
//			 'topMenuHide' => true
			 'bannerTipos' => $bannerTipos
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$rand = rand(1, 9999);

		$categorias = Categoria::where('idPadre', 0)->get()->pluck('nombre', 'id');

		return view('admin.pages.bannerSuperior.create', [
			'rand' => $rand,
			'fondos' => $categorias
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$modelo = Banner::findOrFail($id);

		return view('admin.pages.bannerSuperior.edit', [
			'modelo' => $modelo,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		// Moved to Resources/BannerController@update
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {

	}

	// ---------- CUSTOM

	public function upload(Request $request){
		$rand = $request['randBanner'];
		if($request->hasFile('file')){

			$file = $request->file('file');

			$filename_original = $file->getClientOriginalName();
			$extension_original = $file->getClientOriginalExtension();
			$mimetype = $file->getMimeType();
			$extension = $file->extension();

			$esZip = false;
			$esImagen = false;
			if(in_array($file->getMimeType(), ['application/zip','application/octet-stream','application/x-zip-compressed','multipart/x-zip'])){
				$esZip = true;
			} elseif (in_array($file->getMimeType(), ['image/gif', 'image/jpeg', 'image/png'])) {
				$esImagen = true;
			}

			if(!$esZip && !$esImagen){
				$response = [
					'error' => 'No es un archivo válido, los tipos aceptados son: zip, gif, jpg.'
				];
				return response()->make($response, 422);
			}

			if(!$file->isValid()){
				$response = [
					'error' => 'Archivo INVALIDO'
				];
				return response()->make($response, 422);
			}

			$relativePath = "app".DIRECTORY_SEPARATOR."banner";
			$destinationPath = storage_path($relativePath);

			if(!is_dir($destinationPath)){
				//$mkdir = mkdir($destinationPath, 0755);
				$mkdir = File::makeDirectory($destinationPath, 0755, true);
			}

			$randChars = $this->randomChars(5);
			$filename = "bnr_{$randChars}.{$extension}";

			$fullpath = $destinationPath.DIRECTORY_SEPARATOR.$filename;
			if(File::exists($fullpath)){
				$this->reportDev('Archivo de imagen duplicado, hubo un intento de sobreescribirlo: '.$fullpath);
				File::delete($fullpath);
			}

			$file->move($destinationPath, $filename);

			if(!File::exists($fullpath)){
				$response = [
					'error' => 'Archivo no encontrado despues de subir.'
				];
				return response()->make($response, 422);
			}


			// ----------

			$banner = Banner::find($request['banner_id']);
			$gal = $banner->Galeria;

			if(!$gal){
				$gal = new Galeria([
					'idGaleriaTipo' => 1
				]);
				$gal->save();
				$gal = $banner->Galeria()->save($gal);
//				logger($banner->toArray());
//				$gal = $banner->Galeria;
			}

//			logger($gal->toArray());

			$galImagenes = $gal->Imagenes;
			if($galImagenes->count() > 0){
				$imagen = $galImagenes->first();

				File::delete(storage_path($imagen->ruta));

			} else {
				$imagen = new Imagen();
			}


			$imagen->filename = $filename;
			$imagen->ruta = $relativePath.DIRECTORY_SEPARATOR.$filename;
			$imagen->mimetype = $mimetype;
			$imagen->estado = 1;
			$imagen->titulo = '';

			$gal->Imagenes()->save($imagen);

			$response = Array(
				'result' => true,
				'message' => 'Archivo subido correctamente.',
				'object' => [
					'ruta' => $relativePath.DIRECTORY_SEPARATOR.$filename,
					'filename_orig' => $filename_original,
					'filename' => $filename,
					'mimetype' => $mimetype
				]
			);
		} else {
			$response = [
				'error' => 'No tiene archivo.'
			];
		}
		return response()->json($response);
	}

	public function sort(Request $request) {
		$this->validate($request, [
			'direction' => 'required',
			'orden' => 'required',
		]);

		$actual = (int) $request['orden'];
		$bano = Banner::where('orden', $actual)->first();

		if($request['direction'] == 'up') {

			$ban1 = Banner::where('orden', '<', $actual)->orderBy('orden', 'desc')->first();
			if(!$ban1) {
				$response = Array(
					'result' => true,
					'message' => 'Nada que ordenar, ya es el primero.',
					'data' => []
				);
				return response()->json($response);
			}

			$anterior = $ban1->orden;
			$ban1->orden = $actual;
			$ban1->save();

			$bano->orden = $anterior;
			$bano->save();
		} else {
			$ban1 = Banner::where('orden', '>', $actual)->orderBy('orden', 'asc')->first();
			if(!$ban1) {
				$response = Array(
					'result' => true,
					'message' => 'Nada que ordenar, ya es el ultimo.',
					'data' => []
				);
				return response()->json($response);
			}

			$siguiente = $ban1->orden;
			$ban1->orden = $actual;
			$ban1->save();

			$bano->orden = $siguiente;
			$bano->save();
		}

		$response = Array(
			'result' => true,
			'message' => 'Cambio de orden completo.',
			'data' => []
		);
		return response()->json($response);
	}


}
