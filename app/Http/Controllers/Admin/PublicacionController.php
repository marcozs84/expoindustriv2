<?php

namespace App\Http\Controllers\Admin;

use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Publicacion;
use App\Models\Lista;
use App\Models\ListaItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PublicacionController extends Controller {

	public function __construct() {
		$this->middleware('auth:empleados');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		$estados = ListaItem::where('idLista', Lista::PUBLICACION_ESTADO)->get();
		$estadosMap = $estados->map(function($item, $key){
			return $item->texto = __('messages.'.$item->texto);
		});

		$marcas = Marca::all();
		$marcas = $marcas->pluck('nombre', 'id')->toArray();
		$modelos = Modelo::all();
		$modelos = $modelos->pluck('nombre', 'id')->toArray();

        return view('admin.pages.publicacion.index', [
			'publicacionEstados' => $estados->pluck('texto', 'valor')->toArray(),
			'marcas' => $marcas,
			'modelos' => $modelos,
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    	return view('admin.pages.publicacion.create', [
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

		$publicacion = Publicacion::with([
			'Producto.ProductoItem',
			'Estado'
		])->findOrFail($id);

		return view('admin.pages.publicacion.show', [
			'pub' => $publicacion
		]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $publicacion = Publicacion::with([
			'Owner.Owner.Agenda',
			'Owner.Owner.Proveedor',
			'Producto.Marca',
			'Producto.Modelo',
		])->findOrFail($id);

		return view('admin.pages.publicacion.edit', [
			'publicacion' => $publicacion,
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
