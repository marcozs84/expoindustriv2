<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscripcionController extends Controller {

	public function __construct(){
		$this->middleware('auth:empleados');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.pages.subscripcion.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {

    	$tipos = ListaItem::listado(Lista::SUBSCRIPCION_TIPO)->get()->pluck('texto', 'valor')->toArray();

    	$idUsuario = 0;
    	$usuario = null;
    	if(isset($request['idUsuario'])){
    		$idUsuario = (int)$request['idUsuario'];
    		$usuario = Usuario::find($idUsuario);

		} else {
			logger("NO existe usuario");
		}

        return view('admin.pages.subscripcion.create', [
        	'tipos' => $tipos,
			'idUsuario' => $idUsuario,
			'usuario' => $usuario,
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
