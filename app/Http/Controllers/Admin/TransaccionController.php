<?php

namespace App\Http\Controllers\Admin;

use App\Models\Transaccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Charge;
use Stripe\Checkout\Session;
use Stripe\Stripe;

class TransaccionController extends Controller {

	public function __construct() {
		$this->middleware('auth:empleados');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		return view('admin.pages.transaccion.index', []);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		return view('admin.pages.transaccion.create', []);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$transaccion = Transaccion::findOrFail($id);

		return view('admin.pages.transaccion.show', [
			'transaccion' => $transaccion,
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

		$transaccion = Transaccion::findOrFail($id);

		return view('admin.pages.transaccion.edit', [
			'transaccion' => $transaccion,
		]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function process($id) {

		$transaccion = Transaccion::with([
			'Usuario'
		])->findOrFail($id);

		$payments = [];

//		if( $transaccion->idTransaccionTipo == Transaccion::TIPO_CARD ) {
//
//			if ( $transaccion->owner_type )
//
//			Stripe::setApiKey(env('STRIPE_KEY_SK'));
//			$stripe = Session::retrieve($transaccion->idStripeTransaction);
//
//			logger($stripe);
//
//			$payments[] = $stripe;
//		}

		return view('admin.pages.transaccion.process', [
			'transaccion' => $transaccion,
			'payments' => $payments,
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
