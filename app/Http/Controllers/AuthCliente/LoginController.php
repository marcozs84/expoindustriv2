<?php

namespace App\Http\Controllers\AuthCliente;

use App\Http\Controllers\Controller;
use App\Models\Subscripcion;
use App\Models\Usuario;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/cuenta';

//    protected $gu

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

	protected function guard() {
		return Auth::guard('clientes');
	}

	/**
	 * Show the application's login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showLoginForm() {
		return view('frontend.pages.loginForm');
	}

	/**
	 * Show the application's login form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showLoginForm_v2() {
		return view('admin.pages.auth.loginForm_v2');
	}

	/**
	 * Get the login username to be used by the controller.
	 *
	 * @return string
	 */
	public function username() {
		return 'email';
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
	 */
	public function login(Request $request)
	{
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if ($this->hasTooManyLoginAttempts($request)) {

			if($request->ajax()){
				$seconds = $this->limiter()->availableIn(
					$this->throttleKey($request)
				);

				$message = Lang::get('auth.throttle', ['seconds' => $seconds]);
				$response = [
					$this->username() => $message
				];
				return response()->make($response, 422);
			}

			$this->fireLockoutEvent($request);
			return $this->sendLockoutResponse($request);
		}

		if ($this->attemptLogin($request)) {
			if($request->ajax()){

				$subs_activas = 0;
				$subs = Subscripcion::where('idUsuario', $this->guard()->user()->id)
					->where('fechaDesde', '<', Carbon::today())
					->where('fechaHasta', '>', Carbon::today())
					->get();
				if($subs->count() > 0) {
					$subs_activas = 1;
				}

				$response = Array(
					'result' => true,
					'message' => 'Login successful.',
					'data' => $this->guard()->user(),
					'subs_activas' => $subs_activas,
				);

				$request->session()->regenerate();
				$this->clearLoginAttempts($request);
				$this->authenticated($request, $this->guard()->user());

				return response()->json($response);
			} else {
				return $this->sendLoginResponse($request);
			}
		}

		if($request->ajax()){
			$errors = [$this->username() => trans('auth.failed')];

//			if ($request->expectsJson()) {
				return response()->json($errors, 422);
//			}
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}

	/**
	 * Validate the user login request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return void
	 */
	protected function validateLogin(Request $request)
	{
		$this->validate($request, [
			$this->username() => 'required|string|email',
			'password' => 'required|string',
		]);
	}

	/**
	 * The user has been authenticated.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  mixed  $user
	 * @return mixed
	 */
	protected function authenticated(Request $request, $user) {
		$usuario = $user;
		$agenda = $user->Owner->Agenda;
		$fecha = Carbon::now()->format('d.m.Y H:i:s');

		if(env('APP_ENV') == 'local'){
			$content = <<<xxxx
Usuario {$agenda->nombres_apellidos} - [{$usuario->username}]<br>
Fecha: {$fecha}<br>
xxxx;
//			logger($content);
//			Mail::send('frontend.email.template_dark', ['content' => $content, 'titulo' => 'Ingreso de usuario: '.$usuario->username], function ($m) use($usuario) {
//				$m->from('system@expoindustri.com', 'ExpoIndustri');
//				$m->to('marcozs84@gmail.com', 'Marco Zeballos')
//					->subject('Ingreso: '.$usuario->username);
//			});
		}

		return redirect()->intended($this->redirectPath());
	}

	public function forgotPassword(Request $request) {

		$this->validate($request, [
			'forgotEmail' => 'required',
		]);


		$user = Usuario::where('username', trim($request['forgotEmail']))->first();

		if(!$user) {
			$response = [
				'forgotEmail' => [
					__('messages.user_not_found')
				]
			];
			return response()->make($response, 422);
		} else {
			$host = $_SERVER['SERVER_NAME'];
			$code = rand(100000,999999); //$user->confirmation_token;
			$agenda = $user->Owner->Agenda;
			$email = $user->email;
			$user->forgot_token = $code;
			$user->save();

			$content = __('registration.forgot_message', [
				'nombres_apellidos' => $agenda->nombres_apellidos,
//				'url_confirm' => "http://{$host}/resetPassword/{$email}/{$code}",
				'url_confirm' => "https://{$host}/resetPassword/{$code}",
			]);
			logger($content);

			$subject = __('registration.forgot_subject');

			Mail::send('frontend.email.template_light', [
				'titulo' => $subject,
				'content' => $content,
				'local' => session('visitor_location', 'en')
			], function ($m) use ( $user, $subject ) {
				$m->from('info@expoindustri.com', 'ExpoIndustri');
//			$m->to( 'marco.zeballos@expoindustri.com' );
				$m->to( $user->email );
				$m->bcc( 'marco.zeballos+forgot_password@expoindustri.com', 'Marco Zeballos' );
				$m->replyTo( 'no-reply@expoindustri.com', 'ExpoIndustri');
				$m->subject( $subject );
			});

//			if(env('APP_ENV') == 'local'){
//				Mail::send('frontend.email.template_light', [
//					'content' => $content,
//					'titulo' => $subject,
//					'local' => session('visitor_location', 'en')], function ($m) use($user, $subject) {
//					$m->from('system@expoindustri.com', 'ExpoIndustri');
//					$m->to($user->email);
//					$m->bcc('marcozs84@gmail.com', 'Marco Zeballos');
//					$m->subject($subject);
//				});
//			} else {
//				$headers = "From: ExpoIndustri <" . strip_tags('no-reply@expoindustri.com') . ">\r\n";
//				$headers .= "Reply-To: ExpoIndustri <". strip_tags('no-reply@expoindustri.com') . ">\r\n";
//				$headers .= "BCC: marcozs84@gmail.com\r\n";
//				$headers .= "MIME-Version: 1.0\r\n";
//				$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//				$ver = phpversion();
//				$headers .= "X-Mailer: PHP{$ver}\r\n";
//				$sent = mail($email, $subject, view('frontend.email.template_light', ['content' => $content, 'titulo' => $subject])->render(), $headers);
//
//				if(!$sent) {
//					logger('################## PASSWORD RECOVERY: ERROR EN EL ENVÍO DE CORREO');
//					$this->reportDev("Failed to send reset password mail to: {$email}");
//				}
//			}
		}

		$response = Array(
			'result' => true,
			'message' => __('messages.forgot_message_sent'),
			'data' => []
		);

		return response()->json($response);
	}

	public function resetPassword(Request $request, $token) {

		$changed = false;

		if($request->method() == 'POST'){
			$this->validate($request, [
				'password' => 'required|confirmed',
				'password_confirmation' => 'required',
			]);

//			$user = Usuario::where('username', $email)->first();
			$user = Usuario::where('forgot_token', $token)->first();
			if(!$user){
				$response = [
					'Usuario' => [
						__('messages.user_not_found')
					]
				];
//				return response()->make($response, 422);
				return redirect()->back()->withErrors($response);
			} else {
				$user->password = bcrypt(trim($request['password']));
				$user->save();
				Auth::guard('clientes')->login($user);
			}

			$changed = true;
		}

		return view('frontend.pages.resetPassword', [
			'titulo' => __('messages.forgot_password'),
			'mensaje' => 'Please write your new password',
			'pass_changed' => $changed,
			'token' => $token,
		]);
	}

}
