<?php

namespace App\Http\Controllers\AuthCliente;

use App\Models\Agenda;
use App\Models\Cliente;
use App\Models\Correo;
use App\Models\Galeria;
use App\Models\Proveedor;
use App\Models\ProveedorContacto;
use App\Models\Usuario;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/cuenta';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

		$validations = [
			'nombres' => 'required|string|max:255',
			'apellidos' => 'required|string|max:255',
			'empresa' => '',
			'nit' => '',
			'pais' => 'required',
			'telefono' => 'required',
			'email' => 'required|string|email|max:255|unique:Usuario',
			'password' => 'required|string|min:6|confirmed'
		];

		if(session('isProvider', 'none') != 'none' && session('isProvider') == 'true'){
//		if(isset($data['lang']) && in_array(trim($data['lang']), ['se', 'en'])){
			$validations['empresa'] = 'required|max:255';
			$validations['nit'] = 'required|max:255';
		}

		return Validator::make($data, $validations);
    }

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function register(Request $request)
	{
		$this->validator($request->all())->validate();

		event(new Registered($user = $this->create($request->all())));

		$this->guard()->login($user);

		if($request->ajax()){

			$this->registered($request, $user);

			$response = Array(
				'result' => true,
				'message' => 'Login succesful.',
				'data' => $this->guard()->user()
			);
			return response()->json($response);

		} else {
			return $this->registered($request, $user)
				?: redirect($this->redirectPath());
		}
	}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Usuario
     */
    protected function create(array $data)
    {

		DB::beginTransaction();

		$temp_token = time().'_'.str_random(16);

//		if(session('isProvider', 'none') != 'none' && session('isProvider') == 'true'){
			$prov = new Proveedor();
			$prov->nombre = $data['empresa'];
			$prov->nit = $data['nit'];
			$prov->save();

			$prov->Telefonos()->create([
				'numero' => $data['telefono'],
				'descripcion' => 'registro',
				'estado' => 1
			]);

			$galeria = $prov->Galerias()->save(new Galeria());

			$pc = new ProveedorContacto();
			$pc->idProveedor = $prov->id;
			$pc->idProveedorContactoEstado = 1;
			$pc->idProveedorContactoTipo = 0;
			$pc->save();

			$agenda = $pc->Agenda()->create([
				'nombres' => $data['nombres'],
				'apellidos' => $data['apellidos'],
				'pais' => $data['pais'],
			]);

			$agenda->Correos()->create([
				'correo' => $data['email'],
				'descripcion' => 'login',
				'estado' => 1
			]);

			$agenda->Telefonos()->create([
				'numero' => $data['telefono'],
				'descripcion' => 'registro',
				'estado' => 1
			]);

			$usuario = $pc->Usuario()->create([
				'username' => $data['email'],
				'email' => $data['email'],
				'password' => bcrypt($data['password']),
				'estado' => Usuario::ESTADO_ESPERANDO_CONFIRMACION,  // Awaiting confirmation
				'idTipo' => Usuario::TIPO_CLIENTE,
				'cargo' => 'Cliente',
				'confirmation_token' => $temp_token
			]);

			$pc->idUsuario = $usuario->id;
			$pc->idAgenda = $agenda->id;
			$pc->save();
//		} else {
			$cliente = Cliente::create();

			$cliente->idAgenda = $agenda->id;
			$cliente->idUsuario = $usuario->id;
			$cliente->save();

//			$agenda = $cliente->Agenda()->create([
//				'nombres' => $data['nombres'],
//				'apellidos' => $data['apellidos'],
//				'pais' => $data['pais'],
//			]);

//			$agenda->Correos()->create([
//				'correo' => $data['email'],
//				'descripcion' => 'login',
//				'estado' => 1
//			]);

//			$agenda->Telefonos()->create([
//				'numero' => $data['telefono'],
//				'descripcion' => 'registro',
//				'estado' => 1
//			]);

//			$usuario = $cliente->Usuario()->create([
//				'username' => $data['email'],
//				'email' => $data['email'],
//				'password' => bcrypt($data['password']),
//				'estado' => 3,  // Awaiting confirmation
//				'idTipo' => 0,
//				'confirmation_token' => $temp_token
//			]);
//		}

		$usuario = $cliente->Usuario;



//		if(trim($data['empresa']) != ''){
//			$prov = new Proveedor();
//			$prov->nombre = $data['empresa'];
//			$prov->nit = $data['nit'];
//			$prov->save();
//
//			$prov->Telefonos()->create([
//				'numero' => $data['telefono'],
//				'descripcion' => 'registro',
//				'estado' => 1
//			]);
//
//			$galeria = $prov->Galerias()->save(new Galeria());
//
//			$pc = new ProveedorContacto();
//			$pc->idProveedor = $prov->id;
//			$pc->idUsuario = $usuario->id;
//			$pc->idAgenda = $agenda->id;
//			$pc->idProveedorContactoEstado = 1;
//			$pc->idProveedorContactoTipo = 0;
//			$pc->save();
//		}

		DB::commit();

		return $usuario;

    }

	protected function guard() {
		return Auth::guard('clientes');
	}

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showRegistrationForm()
	{
		return view('frontend.pages.loginForm');
	}

	/**
	 * The user has been registered.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  mixed  $user
	 * @return mixed
	 */
	protected function registered(Request $request, $user) {

		$host = $_SERVER['SERVER_NAME'];
		$code = $user->confirmation_token;
		$agenda = $user->Owner->Agenda;
		$email = $user->email;

		// TODO: traducir el/los mensaje que se envía al correo al momento de registrarse

//		$content = <<<xxxx
//Estimado {$agenda->nombres_apellidos},<br><br>
//
//Queremos darle la mas cordial bienvenida a <b>ExpoIndustri</b>,
//para poder verificar que esta es su cuenta de correo
//por favor haga click en el siguiente enlace:
//<br><br>
//<a href="http://{$host}/confirm/{$email}/{$code}">http://{$host}/confirm/{$email}/{$code}</a>
//
//<br><br>
//Reciba un cordial saludo!<br>
//<b>Equipo de ExpoIndustri</b>
//
//<br><br>
//<hr>
//<span style="color:#666666;">Si por algun motivo usted no ha solicitado una membresía en nuestro sítio, por favor háganoslo saber para dar de baja su cuenta de correo utilizando el siguiente enlace:</span>
//<br><br>
//<a href="http://{$host}/reportar/{$email}/{$code}">http://{$host}/reportar/{$email}/{$code}</a>
//
//xxxx;

		$content = __('registration.register_message', [
			'nombres_apellidos' => $agenda->nombres_apellidos,
//			'url_confirm' => "http://{$host}/confirm/{$email}/{$code}",
//			'url_report' => "http://{$host}/reportar/{$email}/{$code}",
			'url_confirm' => "https://{$host}/confirm/{$code}",
			'url_report' => "https://{$host}/reportar/{$code}",
		]);
		logger($content);

		$subject = 'Bienvenido a ExpoIndustri';
		$subject = __('registration.register_subject');

		Mail::send('frontend.email.template_light', [
			'titulo' => $subject,
			'content' => $content,
			'local' => session('visitor_location', 'en')
		], function ($m) use($user, $subject) {
			$m->from('info@expoindustri.com', 'ExpoIndustri');
			$m->to($user->email);
//			$m->to('marcozs84@gmail.com');
			$m->bcc('marco.zeballos+registry@expoindustri.com', 'Marco Zeballos');
			$m->bcc('joakim.byren+registry@expoindustri.com', 'Joakim Byren');
			$m->bcc('claudia.byren+registry@expoindustri.com', 'Claudia Byren');
//			$m->bcc('joakimbyren@hotmail.com', 'Joakim Byren');
			$m->subject($subject);
		});

//		if(env('APP_ENV') == 'local'){
//			Mail::send('frontend.email.template_light', ['content' => $content, 'titulo' => $subject, 'local' => session('visitor_location', 'en')], function ($m) use($user, $subject) {
//				$m->from('system@expoindustri.com', 'ExpoIndustri');
//				$m->to($user->email);
//				$m->bcc('marcozs84@gmail.com', 'Marco Zeballos');
//				$m->bcc('joakimbyren@hotmail.com', 'Joakim Byren');
//				$m->subject($subject);
//			});
//		} else {
//			$headers = "From: ExpoIndustri <" . strip_tags('no-reply@expoindustri.com') . ">\r\n";
//			$headers .= "Reply-To: ExpoIndustri <". strip_tags('no-reply@expoindustri.com') . ">\r\n";
//			$headers .= "BCC: marcozs84@gmail.com, joakimbyren@hotmail.com\r\n";
//			$headers .= "MIME-Version: 1.0\r\n";
//			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//			$ver = phpversion();
//			$headers .= "X-Mailer: PHP{$ver}\r\n";
//			$sent = mail($email, $subject, view('frontend.email.template_light', ['content' => $content, 'titulo' => $subject])->render(), $headers);
//		}


//		if($sent){
//			logger("enviado");
//		} else {
//			logger("no enviado");
//		}


		return false;  // ES NECESARIO "FALSE" PARA QUE EL METODO "REGISTER" REDIRIJA A LA PAGINA DE LA CUENTA.
//
//		$response = Array(
//			'result' => true,
//			'message' => 'Registro exitoso s.',
//			'data' => []
//		);
//
//		return response()->json($response);
	}

	public function confirm($register_token = ''){

		$invalidToken = false;
		$titulo = '';
		$mensaje = '';

		//TODO: traducir los mensajes que se imprimen en la confirmación

		$lang = App::getLocale();

		if($register_token == ''){
			$invalidToken = true;
			$titulo = __('messages.register_invalid_token');
			$mensaje = __('messages.register_invalid_token_msg', ['linkContact' => route("public.{$lang}.contact")]);

		} else {
			$u = Usuario::where('confirmation_token', $register_token)
				->where('estado', Usuario::ESTADO_ESPERANDO_CONFIRMACION)
				->get();

			if($u->count() == 0){
				$invalidToken = true;
				$titulo = __('messages.register_invalid_token');
				$mensaje = __('messages.register_invalid_token_msg', ['linkContact' => route("public.{$lang}.contact")]);
			} else {
				if($u->count() > 1){
					$this->reportDev('Duplicated confirmation token: '.$register_token.' Email: '.$u->username);
					$invalidToken = true;
					$titulo = __('messages.register_confirmation_error');
					$mensaje = __('messages.register_confirmation_error_msg');
				} else {

					// ========== USUARIO CONFIRMADO =========

					$user = $u->first();
					$user->estado = 1;
					$user->confirmation_token = '';
					$user->save();

					Auth::guard('clientes')->login($user);

					// ----------

					$invalidToken = false;
					$titulo = __('messages.account_confirmed');
					$mensaje = __('messages.account_confirmed_msg');
				}
			}
		}

		return view('frontend.pages.confirm', [
			'invalidToken' => $invalidToken,
			'titulo' => $titulo,
			'mensaje' => $mensaje
		]);

	}

	public function reportar($register_token = ''){
		$invalidToken = false;
		$titulo = '';
		$mensaje = '';

		$contact_form = __('messages.contact_form');
		$contact_form = "<a href=\"javascript:;\" class=\"\" style=\"color:#333; text-decoration: underline;\">{$contact_form}</a>";
		if($register_token == ''){
			$invalidToken = true;
			$titulo = __('messages.register_invalid_token'); //'Token inválido';
			$mensaje = __('register_report_token_invalid', ['contact_form' => $contact_form]);

		} else {
			$u = Usuario::where('confirmation_token', $register_token)
				->get();
//			$u = Usuario::where('email', $email)
//				->first();

			if($u->estado == Usuario::ESTADO_ACTIVO) {
				// El usuario ya ha confirmado su cuenta anteriormente
				// dirigirlo a la pantalla para dase de baja.
				// confirmation_token = ''

				$invalidToken = true;
				$titulo = __('messages.register_report_user_confirmed_title');
				$mensaje = __('messages.register_report_user_confirmed', ['contact_form' => $contact_form]);

			} elseif($u->estado == Usuario::ESTADO_ESPERANDO_CONFIRMACION) {
				// El verdadero dueño de la cuenta quiere darse de baja.
				// confirmation_token = '29302934_abcd3230d'
				// actualizar confirmation_token = ''

				$invalidToken = true;
				$titulo = __('messages.register_report_unsubscribe_title');
				$mensaje = __('messages.register_report_unsubscribe');

				$u->estado = Usuario::ESTADO_REPORTADO;
				$u->confirmation_token = '';
				$u->save();

				$this->reportDev("Usuario reportado: {$u->email} ({$u->id}), es necesario darle de baja a petición del dueño de la cuenta de correo.");

			} elseif($u->estado == Usuario::ESTADO_INACTIVO ||
				$u->estado == Usuario::ESTADO_REPORTADO) {
				// No hacer nada, solo informar que el usuario ya ha sido inhabilitado.
				// confirmation_token = '29302934_abcd3230d' ó ''

				$invalidToken = true;
				$titulo = __('messages.disabled_account');
				$mensaje = __('messages.register_report_disabled_account_mail');
			}
		}

		return view('frontend.pages.confirm', [
			'invalidToken' => $invalidToken,
			'titulo' => $titulo,
			'mensaje' => $mensaje
		]);
	}

}
