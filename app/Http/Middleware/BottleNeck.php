<?php

namespace App\Http\Middleware;

use App\Models\Categoria;
use App\Models\Configuracion;
use App\Models\Lista;
use App\Models\ListaItem;
use App\Models\Marca;
use App\Models\Metatag;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use GeoIp2\Database\Reader;
use Illuminate\Support\Facades\Gate;

class BottleNeck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

		// ----------

		$email_pendiente = '';
		$usuarioLogged = null;

		$userEmpleado = Auth::guard('empleados')->user();
		$userCliente = Auth::guard('clientes')->user();
		$userProveedor = Auth::guard('proveedores')->user();
		if($userEmpleado){
			$usuarioLogged = $userEmpleado;
		} elseif ($userCliente){
			$usuarioLogged = $userCliente;
		} elseif($userProveedor){
			$usuarioLogged = $userProveedor;
		}

		if($usuarioLogged){
			view()->share('auth_nombres_apellidos', $usuarioLogged->Owner->Agenda->nombres_apellidos);
			view()->share('auth_apellidos_nombres', $usuarioLogged->Owner->Agenda->nombres_apellidos);
			view()->share('auth_username', $usuarioLogged->username);
			view()->share('auth_cargo', $usuarioLogged->cargo);
			if($usuarioLogged->estado == 3){
				$email_pendiente = $usuarioLogged->email;
			}
		} else {
			view()->share('auth_nombres_apellidos', '');
			view()->share('auth_nombres_apellidos', '');
			view()->share('auth_username', '');
			view()->share('auth_cargo', '');
		}

		// ---------- NAVIGATION
		$navigation = config('navigation');

		for($i = 0 ; $i < count($navigation) ; $i++){
			$navigation[$i] = $this->parseNavigation($navigation[$i]);
		}
		view()->share('navigation', $navigation);
		view()->share('currentNav', ($request->route()->getName()) ? $request->route()->getName() : '' );

		// ---------- FOOTER STATUS

		view()->share('headerStatus', 'normal');
		view()->share('footerStatus', 'normal');

		// ---------- GLOBAL VALUES
		view()->share('headerLanguageBar', true);
		view()->share('isAjaxRequest', $request->ajax());
		view()->share('global_paises', $this->getPaises());
		view()->share('global_paisesCodigo', $this->getCountryCodes());
		view()->share('email_pendiente', $email_pendiente);

		// ----------

//		view()->share('sidebarMinified', true);

		$this->localInfo(); // Mantener esta linea ANTES del listado de Categorias

		$categorias = Categoria::with('Traduccion')->where('nivel', 0)->get();
		$catList = $categorias->pluck('Traduccion.'.App::getLocale(), 'id')->toArray();
		view()->share('global_categorias', $catList);

		// ----------

//		$marcas = ListaItem::where('idLista', Lista::MARCA)->get()->pluck('texto', 'valor')->toArray();
		$marcas = Marca::all()->pluck('nombre', 'id')->toArray();
		view()->share('global_marcas', $marcas);
		// ----------

		$tipoCambioKr = (float) Configuracion::where('llave', 'tipo_cambio_sek')->first()->valor;
		view()->share('tipo_cambio_sek', $tipoCambioKr);

		// ----------



		// ----------
		//if($request->path() != '/'){

			logger("\r\r\r\r\n\n");
			if(Auth::guard('clientes')->user()){
				logger('==> '.Auth::guard('clientes')->user()->username);
			}

//			logger($request->route()->getActionName());
			logger($request->path());
			$urlParts = explode('\\', $request->route()->getActionName());
			$controller = array_pop($urlParts);
			$folder = array_pop($urlParts);
			logger(sprintf('[%s] %s\%s (%s)', $request->method(), $folder, $controller, $request->route()->getName()));


			if(in_array($request->method(), ['POST', 'PATCH', 'DELETE'])){
				if(!isset($request['draw'])){
					$ar = $request->toArray();
					foreach($ar as &$r){
						if(is_string($r)){
							if(strlen($r) > 200){
								$r = str_limit($r, 200);
							}
						}
					}
//					Log::info(print_r($request->toArray(), true));
					logger(print_r($ar, true));
				}
			}

			//if($request->route()->hasParameters()){
			//	Log::info(print_r($request->route()->parameters(), true));
			//}

			//Log::info("##############################################################################");
			//Log::info("==============================================================================");
			logger("-------------------------------------------------------------------");
//		}
//		$browser = $this->getVisitorBrowser();
//		logger('Browser: '.$browser);

		// ---------- METAS

		$metas = Metatag::where('llave', $request->route()->getName() )->get();
		$seo_metas =[];
		$seo_title = 'ExpoIndustri';
		if( $metas->count() > 0 ) {
			$title = $metas->where('name','title')->first();
			if( $title ) {
				$seo_title = $title->content;
			}
			$seo_metas = $metas->whereNotIn('name', ['title']);
		}
		view()->share('seo_title', $seo_title);
		view()->share('seo_metas', $seo_metas);


		// ---------- DETECT MOBILE JIVO CHAT

		$useragent=$_SERVER['HTTP_USER_AGENT'];

		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
			view()->share('jivoscript', '');
		} else {
			view()->share('jivoscript', '<script src="//code.jivosite.com/widget/hZoJlMrJaL" async></script>');
		}

        return $next($request);
    }

	private function parseNavigation($menu){



		if(isset($menu['childs'])){
			$result = false;
			$permissions = array_pluck($menu['childs'], 'permission');
			foreach ( $permissions as $permission ) {
				if ( Gate::allows( $permission ) ) {
					$result = true;
				}
			}

			if( $result == false ) {
				return false;
			}
		}

		if(isset($menu['permission'])) {

			$result = false;

			if ( is_array( $menu['permission'] ) ) {
				foreach ( $menu['permission'] as $permission ) {
					if ( Gate::allows( $permission ) ) {
						$result = true;
					}
				}
			} else {
				$permissions = explode( ',', $menu['permission'] );
				foreach ( $permissions as $permission ) {
					if ( Gate::allows( $permission ) ) {
						$result = true;
					}
				}
			}

			if( $result == false ) {
				return false;
			}

//			if(Gate::denies($menu['permission'])){
//				return false;
//			}
		}

		if(isset($menu['route'])){
			if($menu['route'] != 'javascript:;' && $menu['route'] != ''){
				$menu['href'] = route($menu['route']);
			}
		} elseif(isset($menu['url'])){
			if($menu['url'] != 'javascript:;' && $menu['url'] != ''){
				$menu['href'] = $menu['url'];
			}
		} else {
			$menu['href'] = '';
		}

		if(isset($menu['childs'])){
			if(count($menu['childs']) > 0){
				for($j = 0 ; $j < count($menu['childs']); $j++){
					$menu['childs'][$j] = $this->parseNavigation($menu['childs'][$j]);
				}
				//foreach($menu['childs'] as $child){
				//	$this->parseNavigation($child);
				//}
			}
//		} elseif (isset($menu['permission'])) {
//			logger("permission:");
//			logger($menu['permission']);
		}
		return $menu;
	}

	private function getPaises(){
		return [
			"af"                                  => "Afghanistan",
			"al"                                      => "Albania",
			"dz"                                      => "Algeria",
			"as"                               => "American Samoa",
			"ad"                                      => "Andorra",
			"ad"                                       => "Angola",
			"ai"                                     => "Anguilla",
			"aq"                                   => "Antarctica",
			"ag"                          => "Antigua and Barbuda",
			"ar"                                    => "Argentina",
			"am"                                      => "Armenia",
			"aw"                                        => "Aruba",
			"au"                                    => "Australia",
			"at"                                      => "Austria",
			"az"                                   => "Azerbaijan",
			"bs"                                      => "Bahamas",
			"bh"                                      => "Bahrain",
			"bd"                                   => "Bangladesh",
			"bb"                                     => "Barbados",
			"by"                                      => "Belarus",
			"be"                                      => "Belgium",
			"bz"                                       => "Belize",
			"bj"                                        => "Benin",
			"bm"                                      => "Bermuda",
			"bt"                                       => "Bhutan",
			"bo"                                      => "Bolivia",
			"ba"                       => "Bosnia and Herzegowina",
			"bw"                                     => "Botswana",
			"bv"                                => "Bouvet Island",
			"br"                                       => "Brazil",
			"io"               => "British Indian Ocean Territory",
			"bn"                            => "Brunei Darussalam",
			"bg"                                     => "Bulgaria",
			"bf"                                 => "Burkina Faso",
			"bi"                                      => "Burundi",
			"kh"                                     => "Cambodia",
			"cm"                                     => "Cameroon",
			"ca"                                       => "Canada",
			"cv"                                   => "Cabo Verde",
			"ky"                               => "Cayman Islands",
			"cf"                     => "Central African Republic",
			"td"                                         => "Chad",
			"cl"                                        => "Chile",
			"cn"                                        => "China",
			"cx"                             => "Christmas Island",
			"cc"                      => "Cocos (Keeling) Islands",
			"co"                                     => "Colombia",
			"km"                                      => "Comoros",
			"cg"                                        => "Congo",
			"cd"        => "Congo, the Democratic Republic of the",
			"ck"                                 => "Cook Islands",
			"cr"                                   => "Costa Rica",
//			"Cote d\'Ivoire"                                => "ci",
			"hr"                           => "Croatia (Hrvatska)",
			"cu"                                         => "Cuba",
			"cy"                                       => "Cyprus",
			"cz"                               => "Czech Republic",
			"dk"                                      => "Denmark",
			"dj"                                     => "Djibouti",
			"dm"                                     => "Dominica",
			"do"                           => "Dominican Republic",
			"tl"                                   => "East Timor",
			"ec"                                      => "Ecuador",
			"eg"                                        => "Egypt",
			"sv"                                  => "El Salvador",
			"gq"                            => "Equatorial Guinea",
			"er"                                      => "Eritrea",
			"ee"                                      => "Estonia",
			"et"                                     => "Ethiopia",
			"fk"                  => "Falkland Islands (Malvinas)",
			"fo"                                => "Faroe Islands",
			"fj"                                         => "Fiji",
			"fi"                                      => "Finland",
			"fr"                                       => "France",
			"gf"                                => "French Guiana",
			"pf"                             => "French Polynesia",
			"tf"                  => "French Southern Territories",
			"ga"                                        => "Gabon",
			"gm"                                       => "Gambia",
			"ge"                                      => "Georgia",
			"de"                                      => "Germany",
			"gh"                                        => "Ghana",
			"gi"                                    => "Gibraltar",
			"gr"                                       => "Greece",
			"gl"                                    => "Greenland",
			"gd"                                      => "Grenada",
			"gp"                                   => "Guadeloupe",
			"gu"                                         => "Guam",
			"gt"                                    => "Guatemala",
			"gn"                                       => "Guinea",
			"gw"                                => "Guinea-Bissau",
			"gy"                                       => "Guyana",
			"ht"                                        => "Haiti",
			"hm"                  => "Heard and Mc Donald Islands",
			"va"                => "Holy See (Vatican City State)",
			"hn"                                     => "Honduras",
			"hk"                                    => "Hong Kong",
			"hu"                                      => "Hungary",
			"is"                                      => "Iceland",
			"in"                                        => "India",
			"id"                                    => "Indonesia",
			"ir"                   => "Iran (Islamic Republic of)",
			"iq"                                         => "Iraq",
			"ie"                                      => "Ireland",
			"il"                                       => "Israel",
			"it"                                        => "Italy",
			"jm"                                      => "Jamaica",
			"jp"                                        => "Japan",
			"jo"                                       => "Jordan",
			"kz"                                   => "Kazakhstan",
			"ke"                                        => "Kenya",
			"ki"                                     => "Kiribati",
			"kp"       => "Korea, Democratic People\'s Republic of",
			"kr"                           => "Korea, Republic of",
			"kw"                                       => "Kuwait",
			"kg"                                   => "Kyrgyzstan",
			"lc"           => "Lao, People\'s Democratic Republic",
			"lv"                                       => "Latvia",
			"lb"                                      => "Lebanon",
			"ls"                                      => "Lesotho",
			"lr"                                      => "Liberia",
			"ly"                       => "Libyan Arab Jamahiriya",
			"li"                                => "Liechtenstein",
			"lt"                                    => "Lithuania",
			"lu"                                   => "Luxembourg",
			"mo"                                        => "Macao",
			"mk"   => "Macedonia, The Former Yugoslav Republic of",
			"mg"                                   => "Madagascar",
			"mw"                                       => "Malawi",
			"my"                                     => "Malaysia",
			"mv"                                     => "Maldives",
			"ml"                                         => "Mali",
			"mt"                                        => "Malta",
			"mh"                             => "Marshall Islands",
			"mq"                                   => "Martinique",
			"mr"                                   => "Mauritania",
			"mu"                                    => "Mauritius",
			"yt"                                      => "Mayotte",
			"mx"                                       => "Mexico",
			"fm"              => "Micronesia, Federated States of",
			"md"                         => "Moldova, Republic of",
			"mc"                                       => "Monaco",
			"mn"                                     => "Mongolia",
			"ms"                                   => "Montserrat",
			"ma"                                      => "Morocco",
			"mz"                                   => "Mozambique",
			"mm"                                      => "Myanmar",
			"na"                                      => "Namibia",
			"nr"                                        => "Nauru",
			"np"                                        => "Nepal",
			"nl"                                  => "Netherlands",
			"an"                         => "Netherlands Antilles",
			"nc"                                => "New Caledonia",
			"nz"                                  => "New Zealand",
			"ni"                                    => "Nicaragua",
			"ne"                                        => "Niger",
			"ng"                                      => "Nigeria",
			"nu"                                         => "Niue",
			"nf"                               => "Norfolk Island",
			"mp"                     => "Northern Mariana Islands",
			"no"                                       => "Norway",
			"om"                                         => "Oman",
			"pk"                                     => "Pakistan",
			"pw"                                        => "Palau",
			"pa"                                       => "Panama",
			"pg"                             => "Papua New Guinea",
			"py"                                     => "Paraguay",
			"pe"                                         => "Peru",
			"ph"                                  => "Philippines",
			"pn"                                     => "Pitcairn",
			"pl"                                       => "Poland",
			"pt"                                     => "Portugal",
			"pr"                                  => "Puerto Rico",
			"qa"                                        => "Qatar",
			"re"                                      => "Reunion",
			"ro"                                      => "Romania",
			"ru"                           => "Russian Federation",
			"rw"                                       => "Rwanda",
			"kn"                        => "Saint Kitts and Nevis",
			"lc"                                  => "Saint Lucia",
			"vc"             => "Saint Vincent and the Grenadines",
			"ws"                                        => "Samoa",
			"sm"                                   => "San Marino",
			"st"                        => "Sao Tome and Principe",
			"sa"                                 => "Saudi Arabia",
			"sn"                                      => "Senegal",
			"sc"                                   => "Seychelles",
			"sl"                                 => "Sierra Leone",
			"sg"                                    => "Singapore",
			"sk"                   => "Slovakia (Slovak Republic)",
			"si"                                     => "Slovenia",
			"sb"                              => "Solomon Islands",
			"so"                                      => "Somalia",
			"za"                                 => "South Africa",
			"gs" => "South Georgia and the South Sandwich Islands",
			"es"                                        => "Spain",
			"lk"                                    => "Sri Lanka",
			"sh"                                   => "St. Helena",
			"pm"                      => "St. Pierre and Miquelon",
			"sd"                                        => "Sudan",
			"sr"                                     => "Suriname",
			"sj"               => "Svalbard and Jan Mayen Islands",
			"sz"                                    => "Swaziland",
			"se"                                       => "Sweden",
			"ch"                                  => "Switzerland",
			"sy"                         => "Syrian Arab Republic",
			"tw"                    => "Taiwan, Province of China",
			"tj"                                   => "Tajikistan",
			"tz"                 => "Tanzania, United Republic of",
			"th"                                     => "Thailand",
			"tg"                                         => "Togo",
			"tk"                                      => "Tokelau",
			"to"                                        => "Tonga",
			"tt"                          => "Trinidad and Tobago",
			"tn"                                      => "Tunisia",
			"tr"                                       => "Turkey",
			"tm"                                 => "Turkmenistan",
			"tc"                     => "Turks and Caicos Islands",
			"tv"                                       => "Tuvalu",
			"ug"                                       => "Uganda",
			"ua"                                      => "Ukraine",
			"ae"                         => "United Arab Emirates",
			"gb"                               => "United Kingdom",
			"us"                                => "United States",
			"um"         => "United States Minor Outlying Islands",
			"uy"                                      => "Uruguay",
			"uz"                                   => "Uzbekistan",
			"vu"                                      => "Vanuatu",
			"ve"                                    => "Venezuela",
			"vn"                                      => "Vietnam",
			"vg"                     => "Virgin Islands (British)",
			"vi"                        => "Virgin Islands (U.S.)",
			"wf"                    => "Wallis and Futuna Islands",
			"eh"                               => "Western Sahara",
			"ye"                                        => "Yemen",
			"yu"                                       => "Serbia",
			"zm"                                       => "Zambia",
			"zw"                                     => "Zimbabwe"
		];
//		return [
//			"Afghanistan"                                  => "af",
//			"Albania"                                      => "al",
//			"Algeria"                                      => "dz",
//			"American Samoa"                               => "as",
//			"Andorra"                                      => "ad",
//			"Angola"                                       => "ad",
//			"Anguilla"                                     => "ai",
//			"Antarctica"                                   => "aq",
//			"Antigua and Barbuda"                          => "ag",
//			"Argentina"                                    => "ar",
//			"Armenia"                                      => "am",
//			"Aruba"                                        => "aw",
//			"Australia"                                    => "au",
//			"Austria"                                      => "at",
//			"Azerbaijan"                                   => "az",
//			"Bahamas"                                      => "bs",
//			"Bahrain"                                      => "bh",
//			"Bangladesh"                                   => "bd",
//			"Barbados"                                     => "bb",
//			"Belarus"                                      => "by",
//			"Belgium"                                      => "be",
//			"Belize"                                       => "bz",
//			"Benin"                                        => "bj",
//			"Bermuda"                                      => "bm",
//			"Bhutan"                                       => "bt",
//			"Bolivia"                                      => "bo",
//			"Bosnia and Herzegowina"                       => "ba",
//			"Botswana"                                     => "bw",
//			"Bouvet Island"                                => "bv",
//			"Brazil"                                       => "br",
//			"British Indian Ocean Territory"               => "io",
//			"Brunei Darussalam"                            => "bn",
//			"Bulgaria"                                     => "bg",
//			"Burkina Faso"                                 => "bf",
//			"Burundi"                                      => "bi",
//			"Cambodia"                                     => "kh",
//			"Cameroon"                                     => "cm",
//			"Canada"                                       => "ca",
//			"Cabo Verde"                                   => "cv",
//			"Cayman Islands"                               => "ky",
//			"Central African Republic"                     => "cf",
//			"Chad"                                         => "td",
//			"Chile"                                        => "cl",
//			"China"                                        => "cn",
//			"Christmas Island"                             => "cx",
//			"Cocos (Keeling) Islands"                      => "cc",
//			"Colombia"                                     => "co",
//			"Comoros"                                      => "km",
//			"Congo"                                        => "cg",
//			"Congo, the Democratic Republic of the"        => "cd",
//			"Cook Islands"                                 => "ck",
//			"Costa Rica"                                   => "cr",
////			"Cote d\'Ivoire"                                => "ci",
//			"Croatia (Hrvatska)"                           => "hr",
//			"Cuba"                                         => "cu",
//			"Cyprus"                                       => "cy",
//			"Czech Republic"                               => "cz",
//			"Denmark"                                      => "dk",
//			"Djibouti"                                     => "dj",
//			"Dominica"                                     => "dm",
//			"Dominican Republic"                           => "do",
//			"East Timor"                                   => "tl",
//			"Ecuador"                                      => "ec",
//			"Egypt"                                        => "eg",
//			"El Salvador"                                  => "sv",
//			"Equatorial Guinea"                            => "gq",
//			"Eritrea"                                      => "er",
//			"Estonia"                                      => "ee",
//			"Ethiopia"                                     => "et",
//			"Falkland Islands (Malvinas)"                  => "fk",
//			"Faroe Islands"                                => "fo",
//			"Fiji"                                         => "fj",
//			"Finland"                                      => "fi",
//			"France"                                       => "fr",
//			"French Guiana"                                => "gf",
//			"French Polynesia"                             => "pf",
//			"French Southern Territories"                  => "tf",
//			"Gabon"                                        => "ga",
//			"Gambia"                                       => "gm",
//			"Georgia"                                      => "ge",
//			"Germany"                                      => "de",
//			"Ghana"                                        => "gh",
//			"Gibraltar"                                    => "gi",
//			"Greece"                                       => "gr",
//			"Greenland"                                    => "gl",
//			"Grenada"                                      => "gd",
//			"Guadeloupe"                                   => "gp",
//			"Guam"                                         => "gu",
//			"Guatemala"                                    => "gt",
//			"Guinea"                                       => "gn",
//			"Guinea-Bissau"                                => "gw",
//			"Guyana"                                       => "gy",
//			"Haiti"                                        => "ht",
//			"Heard and Mc Donald Islands"                  => "hm",
//			"Holy See (Vatican City State)"                => "va",
//			"Honduras"                                     => "hn",
//			"Hong Kong"                                    => "hk",
//			"Hungary"                                      => "hu",
//			"Iceland"                                      => "is",
//			"India"                                        => "in",
//			"Indonesia"                                    => "id",
//			"Iran (Islamic Republic of)"                   => "ir",
//			"Iraq"                                         => "iq",
//			"Ireland"                                      => "ie",
//			"Israel"                                       => "il",
//			"Italy"                                        => "it",
//			"Jamaica"                                      => "jm",
//			"Japan"                                        => "jp",
//			"Jordan"                                       => "jo",
//			"Kazakhstan"                                   => "kz",
//			"Kenya"                                        => "ke",
//			"Kiribati"                                     => "ki",
//			"Korea, Democratic People\'s Republic of"       => "kp",
//			"Korea, Republic of"                           => "kr",
//			"Kuwait"                                       => "kw",
//			"Kyrgyzstan"                                   => "kg",
//			"Lao, People\'s Democratic Republic"            => "la",
//			"Latvia"                                       => "lv",
//			"Lebanon"                                      => "lb",
//			"Lesotho"                                      => "ls",
//			"Liberia"                                      => "lr",
//			"Libyan Arab Jamahiriya"                       => "ly",
//			"Liechtenstein"                                => "li",
//			"Lithuania"                                    => "lt",
//			"Luxembourg"                                   => "lu",
//			"Macao"                                        => "mo",
//			"Macedonia, The Former Yugoslav Republic of"   => "mk",
//			"Madagascar"                                   => "mg",
//			"Malawi"                                       => "mw",
//			"Malaysia"                                     => "my",
//			"Maldives"                                     => "mv",
//			"Mali"                                         => "ml",
//			"Malta"                                        => "mt",
//			"Marshall Islands"                             => "mh",
//			"Martinique"                                   => "mq",
//			"Mauritania"                                   => "mr",
//			"Mauritius"                                    => "mu",
//			"Mayotte"                                      => "yt",
//			"Mexico"                                       => "mx",
//			"Micronesia, Federated States of"              => "fm",
//			"Moldova, Republic of"                         => "md",
//			"Monaco"                                       => "mc",
//			"Mongolia"                                     => "mn",
//			"Montserrat"                                   => "ms",
//			"Morocco"                                      => "ma",
//			"Mozambique"                                   => "mz",
//			"Myanmar"                                      => "mm",
//			"Namibia"                                      => "na",
//			"Nauru"                                        => "nr",
//			"Nepal"                                        => "np",
//			"Netherlands"                                  => "nl",
//			"Netherlands Antilles"                         => "an",
//			"New Caledonia"                                => "nc",
//			"New Zealand"                                  => "nz",
//			"Nicaragua"                                    => "ni",
//			"Niger"                                        => "ne",
//			"Nigeria"                                      => "ng",
//			"Niue"                                         => "nu",
//			"Norfolk Island"                               => "nf",
//			"Northern Mariana Islands"                     => "mp",
//			"Norway"                                       => "no",
//			"Oman"                                         => "om",
//			"Pakistan"                                     => "pk",
//			"Palau"                                        => "pw",
//			"Panama"                                       => "pa",
//			"Papua New Guinea"                             => "pg",
//			"Paraguay"                                     => "py",
//			"Peru"                                         => "pe",
//			"Philippines"                                  => "ph",
//			"Pitcairn"                                     => "pn",
//			"Poland"                                       => "pl",
//			"Portugal"                                     => "pt",
//			"Puerto Rico"                                  => "pr",
//			"Qatar"                                        => "qa",
//			"Reunion"                                      => "re",
//			"Romania"                                      => "ro",
//			"Russian Federation"                           => "ru",
//			"Rwanda"                                       => "rw",
//			"Saint Kitts and Nevis"                        => "kn",
//			"Saint Lucia"                                  => "lc",
//			"Saint Vincent and the Grenadines"             => "vc",
//			"Samoa"                                        => "ws",
//			"San Marino"                                   => "sm",
//			"Sao Tome and Principe"                        => "st",
//			"Saudi Arabia"                                 => "sa",
//			"Senegal"                                      => "sn",
//			"Seychelles"                                   => "sc",
//			"Sierra Leone"                                 => "sl",
//			"Singapore"                                    => "sg",
//			"Slovakia (Slovak Republic)"                   => "sk",
//			"Slovenia"                                     => "si",
//			"Solomon Islands"                              => "sb",
//			"Somalia"                                      => "so",
//			"South Africa"                                 => "za",
//			"South Georgia and the South Sandwich Islands" => "gs",
//			"Spain"                                        => "es",
//			"Sri Lanka"                                    => "lk",
//			"St. Helena"                                   => "sh",
//			"St. Pierre and Miquelon"                      => "pm",
//			"Sudan"                                        => "sd",
//			"Suriname"                                     => "sr",
//			"Svalbard and Jan Mayen Islands"               => "sj",
//			"Swaziland"                                    => "sz",
//			"Sweden"                                       => "se",
//			"Switzerland"                                  => "ch",
//			"Syrian Arab Republic"                         => "sy",
//			"Taiwan, Province of China"                    => "tw",
//			"Tajikistan"                                   => "tj",
//			"Tanzania, United Republic of"                 => "tz",
//			"Thailand"                                     => "th",
//			"Togo"                                         => "tg",
//			"Tokelau"                                      => "tk",
//			"Tonga"                                        => "to",
//			"Trinidad and Tobago"                          => "tt",
//			"Tunisia"                                      => "tn",
//			"Turkey"                                       => "tr",
//			"Turkmenistan"                                 => "tm",
//			"Turks and Caicos Islands"                     => "tc",
//			"Tuvalu"                                       => "tv",
//			"Uganda"                                       => "ug",
//			"Ukraine"                                      => "ua",
//			"United Arab Emirates"                         => "ae",
//			"United Kingdom"                               => "gb",
//			"United States"                                => "us",
//			"United States Minor Outlying Islands"         => "um",
//			"Uruguay"                                      => "uy",
//			"Uzbekistan"                                   => "uz",
//			"Vanuatu"                                      => "vu",
//			"Venezuela"                                    => "ve",
//			"Vietnam"                                      => "vn",
//			"Virgin Islands (British)"                     => "vg",
//			"Virgin Islands (U.S.)"                        => "vi",
//			"Wallis and Futuna Islands"                    => "wf",
//			"Western Sahara"                               => "eh",
//			"Yemen"                                        => "ye",
//			"Serbia"                                       => "yu",
//			"Zambia"                                       => "zm",
//			"Zimbabwe"                                     => "zw"
//		];
	}

	private function getCountryCodes(){
		$countryArray = array(
			'ad' => '376',
			'ae' => '971',
			'af' => '93',
			'ag' => '1268',
			'ai' => '1264',
			'al' => '355',
			'am' => '374',
			'an' => '599',
			'ao' => '244',
			'aq' => '672',
			'ar' => '54',
			'as' => '1684',
			'at' => '43',
			'au' => '61',
			'aw' => '297',
			'az' => '994',
			'ba' => '387',
			'bb' => '1246',
			'bd' => '880',
			'be' => '32',
			'bf' => '226',
			'bg' => '359',
			'bh' => '973',
			'bi' => '257',
			'bj' => '229',
			'bl' => '590',
			'bm' => '1441',
			'bn' => '673',
			'bo' => '591',
			'br' => '55',
			'bs' => '1242',
			'bt' => '975',
			'bw' => '267',
			'by' => '375',
			'bz' => '501',
			'ca' => '1',
			'cc' => '61',
			'cd' => '243',
			'cf' => '236',
			'cg' => '242',
			'ch' => '41',
			'ci' => '225',
			'ck' => '682',
			'cl' => '56',
			'cm' => '237',
			'cn' => '86',
			'co' => '57',
			'cr' => '506',
			'cu' => '53',
			'cv' => '238',
			'cx' => '61',
			'cy' => '357',
			'cz' => '420',
			'de' => '49',
			'dj' => '253',
			'dk' => '45',
			'dm' => '1767',
			'do' => '1809',
			'dz' => '213',
			'ec' => '593',
			'ee' => '372',
			'eg' => '20',
			'er' => '291',
			'es' => '34',
			'et' => '251',
			'fi' => '358',
			'fj' => '679',
			'fk' => '500',
			'fm' => '691',
			'fo' => '298',
			'fr' => '33',
			'ga' => '241',
			'gb' => '44',
			'gd' => '1473',
			'ge' => '995',
			'gh' => '233',
			'gi' => '350',
			'gl' => '299',
			'gm' => '220',
			'gn' => '224',
			'gq' => '240',
			'gr' => '30',
			'gt' => '502',
			'gu' => '1671',
			'gw' => '245',
			'gy' => '592',
			'hk' => '852',
			'hn' => '504',
			'hr' => '385',
			'ht' => '509',
			'hu' => '36',
			'id' => '62',
			'ie' => '353',
			'il' => '972',
			'im' => '44',
			'in' => '91',
			'iq' => '964',
			'ir' => '98',
			'is' => '354',
			'it' => '39',
			'jm' => '1876',
			'jo' => '962',
			'jp' => '81',
			'ke' => '254',
			'kg' => '996',
			'kh' => '855',
			'ki' => '686',
			'km' => '269',
			'kn' => '1869',
			'kp' => '850',
			'kr' => '82',
			'kw' => '965',
			'ky' => '1345',
			'kz' => '7',
			'la' => '856',
			'lb' => '961',
			'lc' => '1758',
			'li' => '423',
			'lk' => '94',
			'lr' => '231',
			'ls' => '266',
			'lt' => '370',
			'lu' => '352',
			'lv' => '371',
			'ly' => '218',
			'ma' => '212',
			'mc' => '377',
			'md' => '373',
			'me' => '382',
			'mf' => '1599',
			'mg' => '261',
			'mh' => '692',
			'mk' => '389',
			'ml' => '223',
			'mm' => '95',
			'mn' => '976',
			'mo' => '853',
			'mp' => '1670',
			'mr' => '222',
			'ms' => '1664',
			'mt' => '356',
			'mu' => '230',
			'mv' => '960',
			'mw' => '265',
			'mx' => '52',
			'my' => '60',
			'mz' => '258',
			'na' => '264',
			'nc' => '687',
			'ne' => '227',
			'ng' => '234',
			'ni' => '505',
			'nl' => '31',
			'no' => '47',
			'np' => '977',
			'nr' => '674',
			'nu' => '683',
			'nz' => '64',
			'om' => '968',
			'pa' => '507',
			'pe' => '51',
			'pf' => '689',
			'pg' => '675',
			'ph' => '63',
			'pk' => '92',
			'pl' => '48',
			'pm' => '508',
			'pn' => '870',
			'pr' => '1',
			'pt' => '351',
			'pw' => '680',
			'py' => '595',
			'qa' => '974',
			'ro' => '40',
			'rs' => '381',
			'ru' => '7',
			'rw' => '250',
			'sa' => '966',
			'sb' => '677',
			'sc' => '248',
			'sd' => '249',
			'se' => '46',
			'sg' => '65',
			'sh' => '290',
			'si' => '386',
			'sk' => '421',
			'sl' => '232',
			'sm' => '378',
			'sn' => '221',
			'so' => '252',
			'sr' => '597',
			'st' => '239',
			'sv' => '503',
			'sy' => '963',
			'sz' => '268',
			'tc' => '1649',
			'td' => '235',
			'tg' => '228',
			'th' => '66',
			'tj' => '992',
			'tk' => '690',
			'tl' => '670',
			'tm' => '993',
			'tn' => '216',
			'to' => '676',
			'tr' => '90',
			'tt' => '1868',
			'tv' => '688',
			'tw' => '886',
			'tz' => '255',
			'ua' => '380',
			'ug' => '256',
			'us' => '1',
			'uy' => '598',
			'uz' => '998',
			'va' => '39',
			'vc' => '1784',
			've' => '58',
			'vg' => '1284',
			'vi' => '1340',
			'vn' => '84',
			'vu' => '678',
			'wf' => '681',
			'ws' => '685',
			'xk' => '381',
			'ye' => '967',
			'yt' => '262',
			'za' => '27',
			'zm' => '260',
			'zw' => '263',
		);

		return $countryArray;
	}

	private function localInfo(){
		$reader = new Reader(storage_path('app/GeoLite2-Country.mmdb'));

		// Replace "city" with the appropriate method for your database, e.g.,
		// "country

		if(env('APP_ENV') == 'local'){
			$record = $reader->country('128.101.101.101');
		} else {
			$ip = request()->ip();
			if($ip == '127.0.0.1'){
				$ip = '128.101.101.101';
			}
			$record = $reader->country($ip);
		}

//		logger($record->country->isoCode); // 'US'
//		logger($record->country->name); // 'United States'
//		logger($record->country->names['zh-CN']); // '美国'

//		logger($record->mostSpecificSubdivision->name); // 'Minnesota'
//		logger($record->mostSpecificSubdivision->isoCode); // 'MN'
//
//		logger($record->city->name); // 'Minneapolis'
//
//		logger($record->postal->code); // '55455'
//
//		logger($record->location->latitude); // 44.9733
//		logger($record->location->longitude); // -93.2323

		// ---------- LOCALIZATION || LANGUAGE

		$latinoamerica = ['bo', 'cl', 'pe', 'co', 'ar', 'br', 'cr', 'cu', 'sv', 'gt', 'ht', 'hn', 'mx', 'ni', 'pa', 'py', 'do', 'uy', 've'];
		$countryProv = ['se','dk','no','nl','de','be','fr','es','uk'];

		$countryAbr = strtolower($record->country->isoCode);
		if(env('APP_ENV') == 'local'){
			$countryAbr = 'se';

			if(preg_match('/admin.expo/', request()->url() ) ) {
				$countryAbr = 'es';
			}
		}

		if(preg_match('/expoindustri.se/', request()->url() ) ) {
			$countryAbr = 'se';
		}

		$countryAbrOrig = $countryAbr;
		$countryAbrOrigName = $record->country->name;

		view()->share('global_country_abr', $countryAbrOrig);
		view()->share('global_country_name', $countryAbrOrigName);


		session(['visitor_location' => $countryAbrOrig]);

		if(in_array($countryAbr, $countryProv)){
			view()->share('global_isProvider', true);
			session(['isProvider' => 'true']);
		} else {
			view()->share('global_isProvider', false);
			session(['isProvider' => 'false']);
		}

		if(in_array($countryAbr, $latinoamerica)){
			$countryAbr = 'es';
		} else {
			if($countryAbr != 'se'){
				$countryAbr = 'en';
			}
		}

		if($countryAbr == 'se') {
			if(session('current_currency', 'none') == 'none') {
				session(['current_currency' => 'sek']);
			}
		}

		//view()->share('global_localization', $countryAbr);
		view()->share('global_localization', session('lang', $countryAbr));
		view()->share('glc', session('lang', $countryAbr));

		// ----------
		$dtLocal = '';
		switch(session('lang', $countryAbr)){
			case 'es':
				$dtLocal = 'https://cdn.datatables.net/plug-ins/1.10.18/i18n/Spanish.json';
				break;
			case 'se':
				$dtLocal = 'https://cdn.datatables.net/plug-ins/1.10.18/i18n/Swedish.json';
				break;
			case 'en':
				$dtLocal = 'https://cdn.datatables.net/plug-ins/1.10.18/i18n/English.json';
				break;
			default:
				$dtLocal = 'https://cdn.datatables.net/plug-ins/1.10.18/i18n/English.json';

		}

		view()->share('global_dtlang', $dtLocal);
		// ----------

		App::setLocale(session('lang', $countryAbr));


		// ---------- RECAPTCHA KEY

		if(preg_match('/expoindustri.se/', request()->url() ) ) {
			$recaptchaKey_public = '6LfIPu0UAAAAAHpj_S4dWONwsl_2mSKS2ElDgIej';
		} else {
			$recaptchaKey_public = '6LdcWocUAAAAAKiSh4xFKlB6HrCw--LWOnZU17pK';
		}
		view()->share('global_recaptcha_public_key', $recaptchaKey_public);
	}

	public function getVisitorBrowser() {

		$name = 'NA';

		if(isset($_SERVER['HTTP_USER_AGENT'])) {
			$agent = $_SERVER['HTTP_USER_AGENT'];

			if (preg_match('/MSIE/i', $agent) && !preg_match('/Opera/i', $agent)) {
				$name = 'Internet Explorer';
			} elseif (preg_match('/Firefox/i', $agent)) {
				$name = 'Mozilla Firefox';
			} elseif (preg_match('/Chrome/i', $agent)) {
				$name = 'Google Chrome';
			} elseif (preg_match('/Safari/i', $agent)) {
				$name = 'Apple Safari';
			} elseif (preg_match('/Opera/i', $agent)) {
				$name = 'Opera';
			} elseif (preg_match('/Netscape/i', $agent)) {
				$name = 'Netscape';
			}
		}

		return $name;
	}
}
