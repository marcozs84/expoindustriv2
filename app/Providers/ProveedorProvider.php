<?php

namespace App\Providers;

use App\Models\ProveedorContacto;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class ProveedorProvider extends EloquentUserProvider {

	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array  $credentials
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByCredentials(array $credentials) {
		$proveedor = ProveedorContacto::whereHas('Usuario', function($q) use($credentials){
			$q->where('username', $credentials['email']);
//			$q->where('password', bcrypt($credentials['password']));
		})->first();

		if($proveedor){
			return $proveedor->Usuario;
		} else {
			return null;
		}
	}

	/**
	 * Validate a user against the given credentials.
	 *
	 * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
	 * @param  array  $credentials
	 * @return bool
	 */
	public function validateCredentials(Authenticatable $user, array $credentials) {
//		$found = ($credentials['email'] == $user->getAuthIdentifier() &&
//			bcrypt($credentials['password']) == $user->getAuthPassword());
		$found = ($credentials['email'] == $user->getAuthIdentifier() &&
			$this->hasher->check($credentials['password'], $user->getAuthPassword()));

		return $found;
	}


}
