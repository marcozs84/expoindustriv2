<?php

namespace App\Providers;

use App\Models\Permiso;
use App\Models\Usuario;
use App\Providers\ClienteProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('ClienteProvider', function ($app, array $config) {
        	return new ClienteProvider($app['hash'], $config['model']);
		});

        Auth::provider('EmpleadoProvider', function ($app, array $config) {
        	return new EmpleadoProvider($app['hash'], $config['model']);
		});

        Auth::provider('ProveedorProvider', function ($app, array $config) {
        	return new ProveedorProvider($app['hash'], $config['model']);
		});

		Gate::before(function ($user, $ability) {
			if ($user->isAdmin()) {
				return true;
			}
		});

//		if( Auth::guard('empleados')->check() ) {
			$permisos = $this->getPermisos();
			foreach($permisos as $permiso){
				Gate::define($permiso->nombre, function ($user) use($permiso){
					if($user->idTipo == Usuario::TIPO_ADMINISTRADOR)
						return true;

					//              $hasRole = $user->hasRole($permiso->Roles);
					//              logger('hasRole: '.$hasRole);
					//              return $hasRole;
					return $user->hasRole($permiso->Roles);
				});
			}
//		}

	}

	public function getPermisos(){
		return Permiso::with('Roles')->get();
	}
}
