<?php

namespace App\Providers;

use App\Models\Empleado;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class EmpleadoProvider extends EloquentUserProvider {

	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array  $credentials
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByCredentials(array $credentials) {
		$empleado = Empleado::whereHas('Usuario', function($q) use($credentials){
			$q->where('username', $credentials['email']);
//			$q->where('password', bcrypt($credentials['password']));
		})->first();

		if($empleado){
			return $empleado->Usuario;
		} else {
			return null;
		}
	}

	/**
	 * Validate a user against the given credentials.
	 *
	 * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
	 * @param  array  $credentials
	 * @return bool
	 */
	public function validateCredentials(Authenticatable $user, array $credentials) {
//		$found = ($credentials['email'] == $user->getAuthIdentifier() &&
//			bcrypt(trim($credentials['password'])) == $user->getAuthPassword());
//		$found = ($credentials['email'] == $user->getAuthIdentifier() &&
//			trim($credentials['password']) == $user->getAuthPassword());
		$found = ($credentials['email'] == $user->getAuthIdentifier() &&
			$this->hasher->check($credentials['password'], $user->getAuthPassword()));

		return $found;
	}


}
