<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MakeResource extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'make:resource {table : Name of the table to get attributes} {ControllerName?}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates a resource controller based on a table name';

	// ---------- CUSTOM

	protected $searchSelectFields = [];
	protected $tableName = '';
	protected $controllerName = '';
	protected $resourceName = '';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$tableName = $this->argument( 'table' );
		$controllerName = trim( $this->argument( 'ControllerName' ) );

		if ( $tableName == '' ) {
			$this->info( 'Nombre de la tabla es requerido' );
		}

		$schema = DB::getDoctrineSchemaManager();
		$tables = $schema->listTables();

		$found = false;
		$tableObj = null;
		foreach ( $tables as $tableRecord ) {
//			logger($tableRecord->getName());
			if ( $tableRecord->getName() == $tableName ) {
				$found = true;
				$tableObj = $tableRecord;
			}
//			echo "<i>".$table->getName() . " </i><b>columns:</b><br>";
//			foreach ($table->getColumns() as $column) {
//				echo ' - ' . $column->getName() . " - " . $column->getType()->getName() . "<br>";
//			}
		}

		if ( !$found ) {
			$this->error( 'Table was not found in DataBase' );
			return false;
		}

		$this->tableName 		= $tableName;
		$this->controllerName 	= $controllerName != '' ? $controllerName : ucfirst( $tableName );
		$this->resourceName 	= lcfirst( $tableName );

		$this->info( '===========================================' );
		$this->info( '* Controller: ' . $this->controllerName );

		$result = $this->parseResourceController( $tableName, $tableObj );

		if ( !$result ) {
			$this->info( 'Command terminated.' );
			$this->info( '===========================================' );
			return true;
		}

		$this->info( '-------------------------------------------' );

		$this->info( 'Add this to your Routes file under Resources section: ' );
		$this->info( "
	Route::post('{$this->resourceName}/ds', 					[	'uses' => 'Resources\\{$this->controllerName}Controller@ds', 					'as' => 'resource.{$this->resourceName}.ds'				]);
	Route::resource('{$this->resourceName}', 'Resources\\{$this->controllerName}Controller', [
		'as' => 'resource',
		'only' => [
			'store',
			'update',
			'destroy'
		]
	]);
		" );
		$this->info( '===========================================' );

		return true;

	}

	private function parseResourceController( $tableName, $tableObj ) {

		$stub_filename = "{$this->laravel->basePath()}/resources/views/stubs/stub_ResourceController.stub";
		$new_index_file = "{$this->laravel->basePath()}/app/Http/Controllers/Resources/{$this->controllerName}Controller.php";
		if ( File::exists( $new_index_file ) ) {
			$this->info( '' );
			$this->line( "File: app/Http/Controllers/Resources/{$this->controllerName}Controller.php already exists in location." );
			$this->info( '' );
			if ( !$this->confirm( "Do you want to replace the already existing Resource Controller? " ) ) {
				$this->info( 'Nothing was replaced.' );
				return false;
			} else {
				$this->info( 'Resource Controller REPLACED!' );
			}
		}
		$stub_index = fopen( $stub_filename, "r" );
		$html_index = fread( $stub_index, filesize( $stub_filename ) );
		fclose( $stub_index );

		// ----------

		$html_index = preg_replace( '/{{ table_name_cap }}/', $this->controllerName, $html_index );
		$html_index = preg_replace( '/{{ resource_name }}/', $this->resourceName, $html_index );

		// ---------- DataTable column names

		$validationCreate = '';
		$assignInsert = '';
		$assignUpdate = '';
		$columnList = [];

		$longestCol = 0;

		foreach ( $tableObj->getColumns() as $column ) {
			if ( strlen( $column->getName() ) > $longestCol ) {
				$longestCol = strlen( $column->getName() );
			}
			$columnList[] = $column->getName();
		}

		$longestCol += 2;
		foreach ( $tableObj->getColumns() as $column ) {
			$colRender = [];

			$colname = $column->getName();

			if ( in_array( $colname, [ 'id', 'created_at', 'updated_at', 'deleted_at' ] ) )
				continue;

//			$tabs = str_pad('', 6 - ceil(strlen($colname) / 4), "\t"); // Using TABS
			$tabs = str_pad( '', $longestCol - strlen( $colname ), " " ); // Using SPACES
//			$tabs2 = str_pad('', 9 - ceil((strlen($colname) + 13) / 4), "\t"); // Using TABS
			$tabs2 = str_pad( '', $longestCol - strlen( $colname ), " " ); // Using SPACES

			$ucolname = ucfirst( $column->getName() );
			if ( $colname != 'id' ) {

				if ( in_array( $column->getType()->getName(), [ 'integer', 'boolean' ] ) ) {
					$validationCreate .= "'{$colname}' {$tabs}=> 'numeric',\n\t\t\t";
					$assignInsert .= "\${$this->resourceName}->{$colname} {$tabs2}= (int) \$request['{$colname}'];\n\t\t";
//					$assignUpdate .= "if( isset(\$request['{$colname}']) ){\n\t\t\t\${$this->resourceName}->{$colname} = (int) \$request['{$colname}'];\n\t\t}\n\t\t";
					$assignUpdate .= "if( isset(\$request['{$colname}']) ){$tabs2}\${$this->resourceName}->{$colname} {$tabs2}= (int) \$request['{$colname}'];\n\t\t";

				} elseif ( in_array( $column->getType()->getName(), [ 'float', 'decimal' ] ) ) {
					$validationCreate .= "'{$colname}' {$tabs}=> 'numeric',\n\t\t\t";
					$assignInsert .= "\${$this->resourceName}->{$colname} {$tabs2}= \$request['{$colname}'];\n\t\t";
//					$assignUpdate .= "if( isset(\$request['{$colname}']) ){\n\t\t\t\${$this->resourceName}->{$colname} = \$request['{$colname}'];\n\t\t}\n\t\t";
					$assignUpdate .= "if( isset(\$request['{$colname}']) ){$tabs2}\${$this->resourceName}->{$colname} {$tabs2}= \$request['{$colname}'];\n\t\t";

				} elseif ( in_array( $column->getType()->getName(), [ 'date' ] ) ) {
					$validationCreate .= "'{$colname}' {$tabs}=> 'required',\n\t\t\t";
					$assignInsert .= "\${$this->resourceName}->{$colname} {$tabs2}= Carbon::createFromFormat('d/m/Y', \$request['{$colname}']);\n\t\t";
//					$assignUpdate .= "if( isset(\$request['{$colname}']) ){\n\t\t\t\${$this->resourceName}->{$colname} = Carbon::createFromFormat('d/m/Y', \$request['{$colname}']);\n\t\t}\n\t\t";
					$assignUpdate .= "if( isset(\$request['{$colname}']) ){$tabs2}\${$this->resourceName}->{$colname} {$tabs2}= Carbon::createFromFormat('d/m/Y', \$request['{$colname}']);\n\t\t";

				} elseif ( in_array( $column->getType()->getName(), [ 'datetime' ] ) ) {
					$validationCreate .= "'{$colname}' {$tabs}=> 'required',\n\t\t\t";
					$assignInsert .= "\${$this->resourceName}->{$colname} {$tabs2}= Carbon::createFromFormat('d/m/Y H:i:s', \$request['{$colname}']);\n\t\t";
//					$assignUpdate .= "if( isset(\$request['{$colname}']) ){\n\t\t\t\${$this->resourceName}->{$colname} = Carbon::createFromFormat('d/m/Y', \$request['{$colname}']);\n\t\t}\n\t\t";
					$assignUpdate .= "if( isset(\$request['{$colname}']) ){$tabs2}\${$this->resourceName}->{$colname} {$tabs2}= Carbon::createFromFormat('d/m/Y H:i:s', \$request['{$colname}']);\n\t\t";

				} else {
					$validationCreate .= "'{$colname}' {$tabs}=> '',\n\t\t\t";
					$assignInsert .= "\${$this->resourceName}->{$colname} {$tabs2}= trim(\$request['{$colname}']);\n\t\t";
//					$assignUpdate .= "if( isset(\$request['{$colname}']) ){\n\t\t\t\${$this->resourceName}->{$colname} = trim(\$request['{$colname}']);\n\t\t}\n\t\t";
					$assignUpdate .= "if( isset(\$request['{$colname}']) ){$tabs2}\${$this->resourceName}->{$colname} {$tabs2}= trim(\$request['{$colname}']);\n\t\t";
				}
			}
		}

		$html_index = preg_replace( '/{{ validation_create }}/', 	$validationCreate, 	$html_index );
		$html_index = preg_replace( '/{{ validation_update }}/', 	$validationCreate, 	$html_index );
		$html_index = preg_replace( '/{{ assign_insert }}/', 		$assignInsert, 		$html_index );
		$html_index = preg_replace( '/{{ assign_update }}/', 		$assignUpdate, 		$html_index );

		if ( in_array( 'created_at', $columnList ) ) {
			$orderColumn = 'created_at';
			$orderDirection = 'desc';
			$order_by_ds = "\$builder->orderBy('{$orderColumn}', '{$orderDirection}');";
		} elseif ( in_array( 'nombre', $columnList ) ) {
			$orderColumn = 'nombre';
			$orderDirection = 'asc';
			$order_by_ds = "\$builder->orderBy('{$orderColumn}', '{$orderDirection}');";
		} elseif ( in_array( 'nombres', $columnList ) ) {
			$orderColumn = 'nombres';
			$orderDirection = 'asc';
			$order_by_ds = "\$builder->orderBy('{$orderColumn}', '{$orderDirection}');";
		} else {
			$order_by_ds = '';
		}

		$html_index = preg_replace( '/{{ order_by_ds }}/', 		$order_by_ds, 		$html_index );

		// ----------

		if ( File::exists( $new_index_file ) )
			File::delete( $new_index_file );

		if ( !File::exists( "{$this->laravel->basePath()}/app/Http/Controllers/Resources" ) )
			$makeDir = File::makeDirectory( "{$this->laravel->basePath()}/app/Http/Controllers/Resources" );

		$myfile = fopen( $new_index_file, "w" );
		fwrite( $myfile, $html_index );

		$this->info( "Resource Controller: {$this->controllerName}Controller was generated succesfully" );
		return true;
	}

}
