<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class MakeCrud extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'make:crud {table : Name of the table to get attributes} {ControllerName?}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates a CRUD structure based on a table name';

	// ---------- CUSTOM

	protected $searchSelectFields = [];
	protected $tableName = '';
	protected $controllerName = '';
	protected $resourceName = '';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$tableName = $this->argument( 'table' );
		$controllerName = trim( $this->argument( 'ControllerName' ) );

		if ( $tableName == '' ) {
			$this->info( 'Nombre de la tabla es requerido' );
		}

		$schema = DB::getDoctrineSchemaManager();
		$tables = $schema->listTables();

		$found = false;
		$tableObj = null;
		foreach ( $tables as $tableRecord ) {
			logger( $tableRecord->getName() );
			if ( $tableRecord->getName() == $tableName ) {
				$found = true;
				$tableObj = $tableRecord;
			}
//			echo "<i>".$table->getName() . " </i><b>columns:</b><br>";
//			foreach ($table->getColumns() as $column) {
//				echo ' - ' . $column->getName() . " - " . $column->getType()->getName() . "<br>";
//			}
		}

		if ( !$found ) {
			$this->error( 'Table was not found in DataBase' );
			return false;
		}

		$this->tableName 		= $tableName;
		$this->controllerName 	= $controllerName != '' ? $controllerName : ucfirst( $tableName );
		$this->resourceName 	= lcfirst( $tableName );

		$this->info( '===========================================' );
		$this->info( '* Controller: ' . $this->controllerName );

		$result = $this->parseAdminController( $tableName, $tableObj );
		if ( !$result ) {
//			$this->info( 'Command terminated.' );
			$this->info( '-------------------------------------------' );
//			return true;
		}

		$result = $this->parseIndex( $tableName, $tableObj );
		if ( !$result ) {
//			$this->info( 'Command terminated.' );
			$this->info( '-------------------------------------------' );
//			return true;
		}

		$result = $this->parseCreate( $tableName, $tableObj );
		if ( !$result ) {
//			$this->info( 'Command terminated.' );
			$this->info( '-------------------------------------------' );
//			return true;
		}

		$result = $this->parseEdit( $tableName, $tableObj );
		if ( !$result ) {
//			$this->info( 'Command terminated.' );
			$this->info( '-------------------------------------------' );
//			return true;
		}

		$result = $this->parseShow( $tableName, $tableObj );
		if ( !$result ) {
//			$this->info( 'Command terminated.' );
			$this->info( '-------------------------------------------' );
//			return true;
		}

		$this->info( '-------------------------------------------' );

		$this->info( 'Add this to your Routes file under Admin section: ' );
		$this->info( "
	Route::resource('{$this->resourceName}', 'Admin\\{$this->controllerName}Controller', 		[
		'as' => 'admin',
		'only' => [
			'index',
			'show',
			'create',
			'edit',
		]
	]);
		" );

		$this->info( '===========================================' );

		return true;

	}

	private function parseAdminController( $tableName, $tableObj ) {

		$stub_filename 	= "{$this->laravel->basePath()}/resources/views/stubs/stub_AdminController.stub";
		$new_index_file = "{$this->laravel->basePath()}/app/Http/Controllers/Admin/{$this->controllerName}Controller.php";
		if ( File::exists( $new_index_file ) ) {
			$this->info( '' );
			$this->line( "File: app/Http/Controllers/Admin/{$this->controllerName}Controller.php already exists in location." );
			if ( !$this->confirm( "Do you want to replace the existing Admin Controller? " ) ) {
				$this->info( 'Nothing was replaced.' );
				return false;
			} else {
				$this->info( 'Resource Controller REPLACED!' );
			}
		}
		$stub_index = fopen( $stub_filename, "r" );
		$html_index = fread( $stub_index, filesize( $stub_filename ) );
		fclose( $stub_index );

		// ----------

		$html_index = preg_replace( '/{{ table_name_cap }}/', $this->controllerName, $html_index );
		$html_index = preg_replace( '/{{ resource_name }}/', $this->resourceName, $html_index );

		// ----------

		if ( File::exists( $new_index_file ) )
			File::delete( $new_index_file );

		if ( !File::exists( "{$this->laravel->basePath()}/app/Http/Controllers/Admin" ) )
			$makeDir = File::makeDirectory( "{$this->laravel->basePath()}/app/Http/Controllers/Admin" );

		$myfile = fopen( $new_index_file, "w" );
		fwrite( $myfile, $html_index );

		$this->info( "* Admin Controller \t--------------- OK" );
		return true;
	}

	private function parseIndex( $tableName, $tableObj ) {
		$stub_filename 	= "{$this->laravel->basePath()}/resources/views/stubs/stub_index.stub";
		$new_index_file = "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}/index.blade.php";
		if ( File::exists( $new_index_file ) ) {
			$this->info( '' );
			$this->line( "File: resources/views/admin/pages/{$this->resourceName}/index.blade.php already exists in location." );
			$this->info( '' );
			if ( !$this->confirm( "Do you want to replace the existing file? " ) ) {
				$this->info( 'Nothing was replaced.' );
				return false;
			} else {
				$this->info( 'Index file REPLACED!' );
			}
		}

		$title_uppder_plural = ucfirst( preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tableName ) ) . 's';

		$controllerName = ucfirst( $tableName );
		$resourceName = lcfirst( $tableName );

		$stub_index = fopen( $stub_filename, "r" );
		$html_index = fread( $stub_index, filesize( $stub_filename ) );
		fclose( $stub_index );

		$html_index = preg_replace( '/{{ title_upper_plural }}/', 	$title_uppder_plural, $html_index );
		$html_index = preg_replace( '/{{ resource_name }}/', 		$this->resourceName, $html_index );
		$html_index = preg_replace( '/{{ title_upper_singular }}/', $controllerName, $html_index );
		$html_index = preg_replace( '/{{ table_name_cap }}/', 		$controllerName, $html_index );

		// ---------- DataTable column names

		$colListing = '';
		$colRenderAll = [];
		$colRender = [];
		foreach ( $tableObj->getColumns() as $column ) {
			$colRender = [];

			$colname = $column->getName();

			if ( in_array( $colname, [ 'updated_at', 'deleted_at' ] ) )
				continue;

			$ucolname = ucfirst( $column->getName() );
			$colListing .= "\t\t\t\t\t{ data: '{$colname}', title: '{$ucolname}'},\n";

			array_push( $colRender, "// {$colname}" );
			array_push( $colRender, 'targets: {{ $counter++ }}' );

			if ( $colname == 'id' ) {
				array_push( $colRender, 'classname: \'text-center\'' );
				array_push( $colRender, 'width: \'30px\'' );
			} elseif ( $colname == 'created_at' ) {
				array_push( $colRender, 'classname: \'text-center\'' );
				array_push( $colRender, 'width: \'100px\'' );
			}

			logger( "campo: " . $column->getName() . " Tipo: " . $column->getType() . " Tipo: " . $column->getType()->getName() );

			if ( in_array( $colname, [ 'id', 'nombre' ] ) ) {
				array_push( $colRender, "render: function( data, type, row ) {\n\t\t\t\t\t\t\treturn '<a href=\"javascript:;\" onclick=\"{$this->resourceName}Show('+ row.id +')\">'+ data +'</i></a>';\n\t\t\t\t\t\t}" );
			} elseif ( in_array( $column->getType()->getName(), [ 'date' ] ) ) {
				array_push( $colRender, "render: function( data, type, row ) {\n\t\t\t\t\t\t\treturn format_date(data);\n\t\t\t\t\t\t}" );
			} elseif ( in_array( $column->getType()->getName(), [ 'datetime' ] ) ) {
				array_push( $colRender, "render: function( data, type, row ) {\n\t\t\t\t\t\t\treturn format_datetime(data);\n\t\t\t\t\t\t}" );
			} elseif ( in_array( $column->getType(), [ 'Date' ] ) ) {
				array_push( $colRender, "render: function( data, type, row ) {\n\t\t\t\t\t\t\treturn format_date(data);\n\t\t\t\t\t\t}" );
			} else {
				array_push( $colRender, "render: function( data, type, row ) {\n\t\t\t\t\t\t\treturn data;\n\t\t\t\t\t\t}" );
			}

			array_push( $colRenderAll, "\t\t\t\t\t\t" . implode( ",\n\t\t\t\t\t\t", $colRender ) );

		}

		$colDefinitions = implode( "\n\t\t\t\t\t},{\n", $colRenderAll );

		$html_index = preg_replace( '/{{ table_col_listing }}/', $colListing, $html_index );
		$html_index = preg_replace( '/{{ table_col_definitions }}/', $colDefinitions, $html_index );

		// ----------

		if ( File::exists( $new_index_file ) )
			File::delete( $new_index_file );

		if ( !File::exists( "{$this->laravel->basePath()}/resources/views/admin/pages/{$resourceName}" ) )
			$makeDir = File::makeDirectory( "{$this->laravel->basePath()}/resources/views/admin/pages/{$resourceName}" );

		$myfile = fopen( $new_index_file, "w" );
		fwrite( $myfile, $html_index );

		$this->info( "* Index page \t\t--------------- OK" );
		return true;
	}

	private function parseCreate( $tableName, $tableObj ) {

		$stub_filename = "{$this->laravel->basePath()}/resources/views/stubs/stub_create.stub";
		$new_index_file = "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}/create.blade.php";
		if ( File::exists( $new_index_file ) ) {
			$this->info( '' );
			$this->line( "File: resources/views/admin/pages/{$this->resourceName}/create.blade.php already exists in location." );
			$this->info( '' );
			if ( !$this->confirm( "Do you want to replace the existing file? " ) ) {
				$this->info( 'Nothing was replaced.' );
				return false;
			} else {
				$this->info( 'Create form REPLACED!' );
			}
		}

		$stub_index = fopen( $stub_filename, "r" );
		$html_index = fread( $stub_index, filesize( $stub_filename ) );
		fclose( $stub_index );

		$title_upper_separated = trim( ucfirst( preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tableName ) ) );
		$formName = "form{$this->controllerName}Create";

		$html_index = preg_replace( '/{{ title_upper_separated }}/', $title_upper_separated, $html_index );
		$html_index = preg_replace( '/{{ resource_name }}/', $this->resourceName, $html_index );
		$html_index = preg_replace( '/{{ controller_name }}/', $this->controllerName, $html_index );
		$html_index = preg_replace( '/{{ table_name_cap }}/', $this->controllerName, $html_index );
		$html_index = preg_replace( '/{{ form_name }}/', $formName, $html_index );

		// ---------- DataTable column names

		$formElements = '';
		$formAssignments = [];
		$libCss = [];
		$libJs = [];
		$datepickers = [];
		$datetimepickers = [];
		$searchElements = [];
		$searchElementsResource = [];
		foreach ( $tableObj->getColumns() as $column ) {
			if ( in_array( $column->getName(), [ 'id', 'created_at', 'updated_at', 'deleted_at' ] ) )
				continue;

			$colname 		= $column->getName();
			$ucolname 		= ucfirst( $column->getName() );
			$colnamespace 	= ucfirst( preg_replace( '/(?<!\ )[A-Z]/', ' $0', $colname ) );

			$tabs = str_pad( '', 4 - ceil( strlen( $colname ) / 4 ), "\t" );

//			$formAssignments[] = "{$colname}: {$tabs}$('#{$formName} #{$colname}').val()";
			$formAssignments[] = "{$colname}: {$tabs}{$formName}.find('#{$colname}').val()";

			if ( in_array( $column->getType()->getName(), [ 'string' ] ) ) {

				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::text('{$colname}', '', [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'text' ] ) ) {
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
                    <div class="col-8">
                        {!! Form::textarea('{$colname}', '', [
                            'id' => '{$colname}',
                            'placeholder' => '{$colnamespace}',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'integer' ] ) ) {

				if ( preg_match( '/^id[A-Z]+/', $colname ) ) {
					$searchElements[$colname] = "#$formName #$colname";
					$searchElementsResource[$colname] = lcfirst( substr( $colname, 2 ) );
					$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::select('{$colname}', [], '', [
							'id' => '{$colname}',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>

xxx;
					$formElements .= <<<xxx
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">{$colnamespace}</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('{$colname}', '', [--}}
							{{--'id' => '{$colname}',--}}
							{{--'placeholder' => '{$colnamespace}',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}

xxx;
				} else {
					$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::number('{$colname}', '', [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
				}

			} elseif ( in_array( $column->getType()->getName(), [ 'date' ] ) ) {
				$datepickers[] = "#$formName #$colname";
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('{$colname}', '', [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'datetime' ] ) ) {
				$datetimepickers[] = "#$formName #$colname";
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('{$colname}', '', [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			} else {
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::text('{$colname}', '', [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			}

		}

		if ( count( $datepickers ) > 0 || count( $datetimepickers ) > 0 ) {
			$libCss[] = '<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />';
			$libJs[]  = '<script src="/assets/plugins/moment/min/moment.min.js"></script>';
			$libJs[]  = '<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		}
		if ( count( $searchElements ) > 0 ) {
			$libCss[] = '<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />';
			$libJs[] = '<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>';
		}

		$libCss = array_unique( $libCss );
		$libJs = array_unique( $libJs );
		$formAssignmentsStr = implode( ",\n\t\t", $formAssignments );

		$datepicker_init = '';
		if ( count( $datepickers ) > 0 ) {
//			$datepickersStr = implode(', ', $datepickers );
			foreach ( $datepickers as $datepicker ) {
				$datepicker_init .= <<<xxx
	$('{$datepicker}').datetimepicker({
		format: 'DD/MM/YYYY'
	})
		.on('dp.change', function( e ){
		})
		.data("DateTimePicker").defaultDate(defaultDate);


xxx;

			}


		}
		if ( count( $datetimepickers ) > 0 ) {
//			$datepickersStr = implode(', ', $datetimepickers );
			foreach ( $datetimepickers as $datetimepicker ) {
				$datepicker_init .= <<<xxx
	$('{$datetimepicker}').datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss'
	})
		.on('dp.change', function(){
		})
		.data("DateTimePicker").defaultDate(defaultDate);
		
		
xxx;
			}


		}

		$searchElement_init = '';
		if ( count( $searchElements ) > 0 ) {
			foreach ( $searchElements as $colname => $searchElement ) {
				$searchResourceName = $searchElementsResource[$colname];
				$searchElement_init .= <<<xxx
	var {$colname}Search = Search.resource('{$searchElement}', {
		url: '{{ route('resource.{$searchResourceName}.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		}
	});


xxx;
			}

		}

		$datepickers_default_date = '';
		if ( count( $datepickers ) > 0 || count( $datetimepickers ) > 0 )
			$datepickers_default_date = 'var defaultDate = moment().hours(0).minutes(0).seconds(0);';

		$html_index = preg_replace( '/{{ libs_css }}/', 		implode( "\n", $libCss ), $html_index );
		$html_index = preg_replace( '/{{ libs_js }}/', 			implode( "\n", $libJs ), $html_index );
		$html_index = preg_replace( '/{{ form_elements }}/', 	$formElements, $html_index );
		$html_index = preg_replace( '/{{ form_assignments }}/', $formAssignmentsStr, $html_index );
		$html_index = preg_replace( '/{{ datepickers_default_date }}/', 	$datepickers_default_date, $html_index );
		$html_index = preg_replace( '/{{ datepickers_init }}/', $datepicker_init, $html_index );
		$html_index = preg_replace( '/{{ searchelements_init }}/', $searchElement_init, $html_index );

		// ----------

		if ( File::exists( $new_index_file ) )
			File::delete( $new_index_file );

		if ( !File::exists( "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}" ) )
			$makeDir = File::makeDirectory( "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}" );

		$myfile = fopen( $new_index_file, "w" );
		fwrite( $myfile, $html_index );

		$this->info( "* Create form \t\t--------------- OK" );
		return true;
	}

	private function parseEdit( $tableName, $tableObj ) {
		$stub_filename 	= "{$this->laravel->basePath()}/resources/views/stubs/stub_edit.stub";
		$new_index_file = "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}/edit.blade.php";
		if ( File::exists( $new_index_file ) ) {
			$this->info( '' );
			$this->line( "File: resources/views/admin/pages/{$this->resourceName}/edit.blade.php already exists in location." );
			$this->info( '' );
			if ( !$this->confirm( "Do you want to replace the existing file? " ) ) {
				$this->info( 'Nothing was replaced.' );
				return false;
			} else {
				$this->info( 'Edit form REPLACED!' );
			}
		}

		$stub_index = fopen( $stub_filename, "r" );
		$html_index = fread( $stub_index, filesize( $stub_filename ) );
		fclose( $stub_index );

		$title_upper_separated = trim( ucfirst( preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tableName ) ) );
		$formName = "form{$this->controllerName}Edit";

		$html_index = preg_replace( '/{{ title_upper_separated }}/', 	$title_upper_separated, $html_index );
		$html_index = preg_replace( '/{{ resource_name }}/', 			$this->resourceName, 	$html_index );
		$html_index = preg_replace( '/{{ controller_name }}/', 			$this->controllerName, 	$html_index );
		$html_index = preg_replace( '/{{ table_name_cap }}/', 			$this->controllerName, 	$html_index );
		$html_index = preg_replace( '/{{ form_name }}/', 				$formName, 				$html_index );

		// ---------- DataTable column names

		$formElements = '';
		$formAssignments = [];
		$libCss = [];
		$libJs = [];
		$datepickers = [];
		$datetimepickers = [];
		$searchElements = [];
		$searchElementsResource = [];
		foreach ( $tableObj->getColumns() as $column ) {
			if ( in_array( $column->getName(), [ 'id', 'created_at', 'updated_at', 'deleted_at' ] ) )
				continue;

			$colname = $column->getName();
			$ucolname = ucfirst( $column->getName() );
			$colnamespace = ucfirst( preg_replace( '/(?<!\ )[A-Z]/', ' $0', $colname ) );

			$tabs = str_pad( '', 4 - ceil( strlen( $colname ) / 4 ), "\t" );

//			$formAssignments[] = "{$colname}: {$tabs}$('#{$formName} #{$colname}').val()";
			$formAssignments[] = "{$colname}: {$tabs}{$formName}.find('#{$colname}').val()";

			if ( in_array( $column->getType()->getName(), [ 'string' ] ) ) {

				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::text('{$colname}', \${$this->resourceName}->{$colname}, [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'text' ] ) ) {
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
                    <div class="col-8">
                        {!! Form::textarea('{$colname}', \${$this->resourceName}->{$colname}, [
                            'id' => '{$colname}',
                            'placeholder' => '{$colnamespace}',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'integer' ] ) ) {

				if ( preg_match( '/^id[A-Z]+/', $colname ) ) {
					$searchElements[$colname] = "#$formName #$colname";
					$searchElementsResource[$colname] = lcfirst( substr( $colname, 2 ) );
					$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::select('{$colname}', [], '', [
							'id' => '{$colname}',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>

xxx;
					$formElements .= <<<xxx
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">{$colnamespace}</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('{$colname}', \${$this->resourceName}->{$colname}, [--}}
							{{--'id' => '{$colname}',--}}
							{{--'placeholder' => '{$colnamespace}',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}

xxx;
				} else {
					$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::number('{$colname}', \${$this->resourceName}->{$colname}, [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
				}
			} elseif ( in_array( $column->getType()->getName(), [ 'date' ] ) ) {
				$datepickers[] = "#$formName #$colname";
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('{$colname}', (\${$this->resourceName}->{$colname} ? \${$this->resourceName}->{$colname}->format('d/m/Y') : ''), [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'datetime' ] ) ) {
				$datetimepickers[] = "#$formName #$colname";
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('{$colname}', (\${$this->resourceName}->{$colname} ? \${$this->resourceName}->{$colname}->format('d/m/Y H:i:s') : ''), [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			} else {
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{!! Form::text('{$colname}', \${$this->resourceName}->{$colname}, [
							'id' => '{$colname}',
							'placeholder' => '{$colnamespace}',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

xxx;
			}

		}

		if ( count( $datepickers ) > 0 || count( $datetimepickers ) > 0 ) {
			$libCss[] = '<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />';
			$libJs[] = '<script src="/assets/plugins/moment/min/moment.min.js"></script>';
			$libJs[] = '<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>';
		}
		if ( count( $searchElements ) > 0 ) {
			$libCss[] = '<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />';
			$libJs[] = '<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>';
		}

		$libCss = array_unique( $libCss );
		$libJs = array_unique( $libJs );
		$formAssignmentsStr = implode( ",\n\t\t", $formAssignments );

		$datepicker_init = '';
		if ( count( $datepickers ) > 0 ) {
//			$datepickersStr = implode(', ', $datepickers );
			foreach ( $datepickers as $datepicker ) {
				$datepicker_init .= <<<xxx
	$('{$datepicker}').datetimepicker({
		format: 'DD/MM/YYYY'
	})
		.on('dp.change', function(){
		});


xxx;

			}
		}

		if ( count( $datetimepickers ) > 0 ) {
//			$datepickersStr = implode(', ', $datetimepickers );
			foreach ( $datetimepickers as $datetimepicker ) {
				$datepicker_init .= <<<xxx
	$('{$datetimepicker}').datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss'
	})
		.on('dp.change', function(){
		});


xxx;
			}

		}

		$searchElement_init = '';
		if ( count( $searchElements ) > 0 ) {
			foreach ( $searchElements as $colname => $searchElement ) {
				$searchResourceName = $searchElementsResource[$colname];
				$searchResourceNameUP = ucfirst( $searchElementsResource[$colname] );
				$searchElement_init .= <<<xxx
	var {$colname}Search = Search.resource('{$searchElement}', {
		url: '{{ route('resource.{$searchResourceName}.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		defaultValue: {
			value: {{ \${$this->resourceName}->{$colname} }},
			text: '{{ \${$this->resourceName}->{$searchResourceNameUP}->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault(\${$colname}Search, {{ \${$this->resourceName}->{$colname} }}, '{{ \${$this->resourceName}->{$searchResourceNameUP}->id }});


xxx;
			}

		}

		$html_index = preg_replace( '/{{ libs_css }}/', implode( "\n", $libCss ), $html_index );
		$html_index = preg_replace( '/{{ libs_js }}/', implode( "\n", $libJs ), $html_index );
		$html_index = preg_replace( '/{{ form_elements }}/', $formElements, $html_index );
		$html_index = preg_replace( '/{{ form_assignments }}/', $formAssignmentsStr, $html_index );
		$html_index = preg_replace( '/{{ datepickers_init }}/', $datepicker_init, $html_index );
		$html_index = preg_replace( '/{{ searchelements_init }}/', $searchElement_init, $html_index );

		// ----------

		if ( File::exists( $new_index_file ) )
			File::delete( $new_index_file );

		if ( !File::exists( "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}" ) )
			$makeDir = File::makeDirectory( "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}" );

		$myfile = fopen( $new_index_file, "w" );
		fwrite( $myfile, $html_index );

		$this->info( "* Edit form \t\t--------------- OK" );
		return true;
	}

	private function parseShow( $tableName, $tableObj ) {
		$stub_filename = "{$this->laravel->basePath()}/resources/views/stubs/stub_show.stub";
		$new_index_file = "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}/show.blade.php";
		if ( File::exists( $new_index_file ) ) {
			$this->info( '' );
			$this->line( "File: resources/views/admin/pages/{$this->resourceName}/show.blade.php already exists in location." );
			$this->info( '' );
			if ( !$this->confirm( "Do you want to replace the existing file? " ) ) {
				$this->info( 'Nothing was replaced.' );
				return false;
			} else {
				$this->info( 'Show form REPLACED!' );
			}
		}

		$stub_index = fopen( $stub_filename, "r" );
		$html_index = fread( $stub_index, filesize( $stub_filename ) );
		fclose( $stub_index );

		$title_upper_separated = trim( ucfirst( preg_replace( '/(?<!\ )[A-Z]/', ' $0', $tableName ) ) );
		$formName = "form{$this->controllerName}Show";

		$html_index = preg_replace( '/{{ title_upper_separated }}/', $title_upper_separated, $html_index );
		$html_index = preg_replace( '/{{ resource_name }}/', $this->resourceName, $html_index );
		$html_index = preg_replace( '/{{ controller_name }}/', $this->controllerName, $html_index );
		$html_index = preg_replace( '/{{ table_name_cap }}/', $this->controllerName, $html_index );
		$html_index = preg_replace( '/{{ form_name }}/', $formName, $html_index );

		// ---------- DataTable column names

		$formElements = '';
		$formAssignments = [];
		$libCss = [];
		$libJs = [];
		$datepickers = [];
		foreach ( $tableObj->getColumns() as $column ) {
			if ( in_array( $column->getName(), [ 'updated_at', 'deleted_at' ] ) )
				continue;

			$colname = $column->getName();
			$ucolname = ucfirst( $column->getName() );
			$colnamespace = ucfirst( preg_replace( '/(?<!\ )[A-Z]/', ' $0', $colname ) );

			$tabs = str_pad( '', 4 - ceil( strlen( $colname ) / 4 ), "\t" );

			$formAssignments[] = "{$colname}: {$tabs}$('#{$formName} #{$colname}').val()";

			if ( in_array( $column->getType()->getName(), [ 'string' ] ) ) {

				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{{ \${$this->resourceName}->{$colname} }}
					</div>
				</div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'text' ] ) ) {
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
                    <div class="col-8">
                       {!! \${$this->resourceName}->{$colname} !!}
                    </div>
                </div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'integer' ] ) ) {
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{{ \${$this->resourceName}->{$colname} }}
					</div>
				</div>

xxx;
			} elseif ( in_array( $column->getType()->getName(), [ 'date', 'datetime' ] ) ) {
				$libCss[] = '<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />';
				$libJs[] = '<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>';
				$libJs[] = '<script src="/assets/plugins/masked-input/masked-input.js"></script>';
				$datepickers[] = "#$formName #$colname";
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{{ (\${$this->resourceName}->{$colname} ? \${$this->resourceName}->{$colname}->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>

xxx;
			} else {
				$formElements .= <<<xxx
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">{$colnamespace}</label>
					<div class="col-8">
						{{ \${$this->resourceName}->{$colname} }}
					</div>
				</div>

xxx;
			}

		}

		$libCss = array_unique( $libCss );
		$libJs = array_unique( $libJs );

		$html_index = preg_replace( '/{{ libs_css }}/', implode( "\n", $libCss ), $html_index );
		$html_index = preg_replace( '/{{ libs_js }}/', implode( "\n", $libJs ), $html_index );
		$html_index = preg_replace( '/{{ form_elements }}/', $formElements, $html_index );

		// ----------

		if ( File::exists( $new_index_file ) )
			File::delete( $new_index_file );

		if ( !File::exists( "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}" ) )
			$makeDir = File::makeDirectory( "{$this->laravel->basePath()}/resources/views/admin/pages/{$this->resourceName}" );

		$myfile = fopen( $new_index_file, "w" );
		fwrite( $myfile, $html_index );

		$this->info( "* Show form \t\t--------------- OK" );
		return true;
	}
}
