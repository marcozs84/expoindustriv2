<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\OrdenDetalle
 *
 * @property int $id
 * @property int|null $idOrden
 * @property int|null $idProductoItem
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Orden|null $Orden
 * @property-read \App\Models\ProductoItem|null $ProductoItem
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrdenDetalle onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdenDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdenDetalle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdenDetalle whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdenDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdenDetalle whereIdOrden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrdenDetalle whereIdProductoItem($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrdenDetalle withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OrdenDetalle withoutTrashed()
 * @mixin \Eloquent
 */
class OrdenDetalle extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'OrdenDetalle';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idOrden',
		'idProductoItem',
		'precioVenta',
		'monedaVenta',
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Orden() {
		return $this->belongsTo('App\Models\Orden', 'idOrden');
	}

	public function ProductoItem() {
		return $this->belongsTo('App\Models\ProductoItem', 'idProductoItem');
	}

}