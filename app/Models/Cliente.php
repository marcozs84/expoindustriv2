<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Cliente
 *
 * @property int $id
 * @property int|null $idUsuario
 * @property int|null $idAgenda
 * @property int|null $idClienteTipo
 * @property int|null $idClienteEtado
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Agenda $Agenda
 * @property-read \App\Models\ListaItem $ClienteEstado
 * @property-read \App\Models\ListaItem $ClienteTipo
 * @property-read \App\Models\Usuario $Usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cliente onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereIdAgenda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereIdClienteEtado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereIdClienteTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereIdUsuario($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cliente withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cliente withoutTrashed()
 * @mixin \Eloquent
 */
class Cliente extends Model  {

	use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Cliente';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idUsuario',
		'idAgenda',
		'idClienteTipo',
		'idClienteEstado'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Agenda() {
//		return $this->morphOne('App\Models\Agenda', 'owner');
		return $this->belongsTo('App\Models\Agenda', 'idAgenda');
	}

	public function ClienteEstado() {
		return $this->belongsTo('App\Models\ListaItem');
	}

	public function ClienteTipo() {
		return $this->belongsTo('App\Models\ListaItem');
	}

	public function Usuario() {
//		return $this->morphOne('App\Models\Usuario', 'owner');
		return $this->belongsTo('App\Models\Usuario', 'idUsuario');
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting(function($cliente) { // before delete() method call this
			$cliente->Usuario->delete();
		});
	}

}