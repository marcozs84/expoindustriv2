<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticia extends Model {
	use SoftDeletes;

	protected $table = 'Noticia';

	protected $fillable = [
		'titulo_es',
		'resumen_es',
		'titulo_en',
		'resumen_en',
		'titulo_se',
		'resumen_se',
		'fuente',
		'fuenteUrl',
		'fechaPublicacion',
		'pais',
	];

	protected $dates = [
		'fechaPublicacion',
		'deleted_at',
	];
}
