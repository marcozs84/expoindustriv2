<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ActividadTipo
 *
 * @property int $id
 * @property int|null $idPadre
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $orden
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActividadTipo whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActividadTipo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActividadTipo whereIdPadre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActividadTipo whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ActividadTipo whereOrden($value)
 * @mixin \Eloquent
 */
class ActividadTipo extends Model {

	protected $table = 'ActividadTipo';

	public $timestamps = false;

	public function Padre() {
		return $this->belongsTo('App\Models\ActividadTipo', 'idPadre');
	}

}
