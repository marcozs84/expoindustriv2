<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PublicacionTransporte
 *
 * @property int $id
 * @property int|null $idPublicacion
 * @property int|null $idTransporte
 * @property float|null $precio
 * @property string|null $disponibilidad
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionTransporte whereDisponibilidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionTransporte whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionTransporte whereIdPublicacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionTransporte whereIdTransporte($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionTransporte wherePrecio($value)
 * @mixin \Eloquent
 */
class PublicacionTransporte extends Model {

	protected $table = 'PublicacionTransporte';
	protected $fillable = [
		'idPublicacion',
		'idTransporte',
		'precio',
		'disponibilidad',
	];

	public $timestamps = false;
}
