<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Cotizacion
 *
 * @property int $id
 * @property int|null $idAlmacen
 * @property int|null $idCliente
 * @property int|null $idCotizacionEsquema
 * @property int|null $idPublicacion
 * @property int|null $idProducto
 * @property int|null $revisionTecnica
 * @property float|null $revisionTecnicaPrecio
 * @property int|null $fotografiasExtra
 * @property float|null $fotografiasExtraPrecio
 * @property int|null $seguro
 * @property float|null $seguroPrecio
 * @property int|null $transporte
 * @property float|null $transportePrecio
 * @property float|null $destinoPrecio
 * @property array $definicion
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Almacen|null $Almacen
 * @property-read \App\Models\Cliente|null $Cliente
 * @property-read \App\Models\CotizacionEsquema|null $CotizacionEsquema
 * @property-read \App\Models\Orden $Orden
 * @property-read \App\Models\Producto|null $Producto
 * @property-read \App\Models\Publicacion|null $Publicacion
 * @property-read mixed $created_at_format
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cotizacion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereDefinicion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereDestinoPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereFotografiasExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereFotografiasExtraPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereIdAlmacen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereIdCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereIdCotizacionEsquema($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereIdProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereIdPublicacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereRevisionTecnica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereRevisionTecnicaPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereSeguro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereSeguroPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereTransporte($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereTransportePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cotizacion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cotizacion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cotizacion withoutTrashed()
 * @mixin \Eloquent
 */
class Cotizacion extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Cotizacion';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idAlmacen',
		'idCliente',
		'idCotizacionEsquema',
		'idPublicacion',
		'revisionTecnica',
		'revisionTecnicaPrecio',
		'fotografiasExtra',
		'fotografiasExtraPrecio',
		'seguro',
		'seguroPrecio',
		'transporte',
		'transportePrecio',
		'destinoPrecio',
		'definicion',
	];

    protected $dates = ['deleted_at'];

    protected $casts = [
		'definicion' => 'array'
    ];

    protected $appends = [
    	'created_at_format'
	];

	const ESTADO_ACTIVE = 1;
	const ESTADO_CLOSED = 2;
	const ESTADO_REJECTED = 3;
	const ESTADO_IN_PROGRESS = 4;
	const ESTADO_FINISHED = 5;

    // ---------- ATTRIBUTES

	public function getCreatedAtFormatAttribute(){
		return Carbon::parse($this->attributes['created_at'])->format('d.m.Y');
	}

    // ---------- RELATIONSHIPS

	public function Almacen() {
		return $this->belongsTo('App\Models\Almacen', 'idAlmacen');
	}

	public function Cliente() {
		return $this->belongsTo('App\Models\Cliente', 'idCliente');
	}

	public function CotizacionEsquema() {
		return $this->belongsTo('App\Models\CotizacionEsquema', 'idCotizacionEsquema');
	}

	public function Orden() {
		return $this->hasOne('App\Models\Orden', 'idCotizacion');
	}

	public function Producto() {
		return $this->belongsTo('App\Models\Producto', 'idProducto');
	}

	public function Publicacion() {
		return $this->belongsTo('App\Models\Publicacion', 'idPublicacion');
	}

}