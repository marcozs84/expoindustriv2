<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LookIp
 *
 * @property int $id
 * @property string|null $ip
 * @property string|null $pais
 * @property int|null $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookIp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookIp whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookIp wherePais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookIp whereStatus($value)
 * @mixin \Eloquent
 */
class LookIp extends Model {

	protected $table = 'LookIp';
	public $timestamps = false;
}
