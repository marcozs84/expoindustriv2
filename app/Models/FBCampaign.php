<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FBCampaign extends Model {

	use SoftDeletes;

	protected $table = 'FBCampaign';

	protected $fillable = [
		'idFBC',
		'fbclid',
	];

	protected $dates = [
		'deleted_at',
	];
}
