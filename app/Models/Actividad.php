<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Actividad
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property int|null $idActividadTipo
 * @property int|null $idCalendario
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property string|null $fechaInicio
 * @property string|null $fechaFin
 * @property string|null $linked
 * @property int|null $completado
 * @property array $extrainfo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Archivo[] $Archivos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comentario[] $Comentarios
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Galeria[] $Galerias
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Actividad onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereCompletado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereExtrainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereFechaFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereFechaInicio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereIdActividadTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereIdCalendario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereLinked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereTitulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Actividad whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Actividad withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Actividad withoutTrashed()
 * @mixin \Eloquent
 * @property-read \App\Models\Calendario|null $Calendario
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Usuario[] $Responsables
 */
class Actividad extends Model {

	use SoftDeletes;

	protected $table = 'Actividad';

	protected $fillable = [
		'idActividadTipo',
		'idCalendario',
		'titulo',
		'descripcion',
		'fechaInicio',
		'fechaFin',
		'linked',
		'completado',
		'extrainfo',
	];

	protected $dates = [
		'fechaInicio',
		'fechaFin',
		'deleted_at'
	];
	protected $casts = [
		'extrainfo' => 'array'
	];

	// ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	public function Archivos() {
		return $this->morphMany('App\Models\Archivo', 'owner');
	}

	public function Calendario() {
		return $this->belongsTo('App\Models\Calendario', 'idCalendario');
	}

	public function Comentarios() {
		return $this->morphMany('App\Models\Comentario', 'owner');
	}

	public function Galeria() {
		return $this->morphOne('App\Models\Galeria', 'owner');
	}

	public function Galerias() {
		return $this->morphMany('App\Models\Galeria', 'owner');
	}

	public function Responsables() {
		return $this->belongsToMany('App\Models\Usuario', 'ActividadUsuario', 'idActividad', 'idUsuario')->withPivot('created_at');
	}

	public function ActividadTipo() {
		return $this->belongsTo('App\Models\ActividadTipo', 'idActividadTipo');
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting(function($actividad) { // before delete() method call this

//			$actividad->Responsables()->delete();
			// do the rest of the cleanup...
		});

		static::restoring( function( $actividad ) {
//			$actividad->Responsables()->withTrashed()->restore();
		});
	}
}
