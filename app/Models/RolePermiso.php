<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RolePermiso
 *
 * @property int|null $idRole
 * @property int|null $idPermiso
 * @property-read \App\Models\Permiso|null $Permiso
 * @property-read \App\Models\Role|null $Role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RolePermiso whereIdPermiso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RolePermiso whereIdRole($value)
 * @mixin \Eloquent
 */
class RolePermiso extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'RolePermiso';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idRole',
		'idPermiso'
	];

    // ---------- RELATIONSHIPS

	public function Role() {
		return $this->belongsTo('App\Models\Role', 'idRole');
	}

	public function Permiso() {
		return $this->belongsTo('App\Models\Permiso', 'idPermiso');
	}

}