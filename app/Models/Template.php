<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Template
 *
 * @property int $id
 * @property int|null $idModulo
 * @property int|null $idTemplateTipo
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property string|null $asunto
 * @property string|null $mensaje
 * @property int|null $estado
 * @property string|null $extrainfo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereAsunto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereExtrainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereIdModulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereIdTemplateTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereMensaje($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Template whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Template withoutTrashed()
 * @mixin \Eloquent
 */
class Template extends Model {

	use SoftDeletes;

	protected $table = 'Template';

	protected $dates = ['deleted_at'];

	const TIPO_HTML = 1;
	const TIPO_PDF = 2;

	protected $fillable = [
		'idModulo',
		'idTemplateTipo',
		'nombre',
		'descripcion',
		'asunto',
		'mensaje',
		'estado',
		'extrainfo',
	];

	protected $casts = [
		'extrainfo' => 'array'
	];

	// ---------- RELATIONSHIPS

	public function Modulo() {
		return $this->belongsTo('App\Models\Modulo', 'idModulo');
	}

	public function TemplateTipo() {
		return $this->hasOne('App\Models\ListaItem', 'valor', 'idTemplateTipo')
			->where('idLista', Lista::TEMPLATE_TIPO);
	}

}
