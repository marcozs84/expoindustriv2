<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Modelo
 *
 * @property int $id
 * @property int|null $idMarca
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereIdMarca($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modelo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Modelo extends Model {

	protected $table = 'Modelo';

	protected $fillable = [
		'idMarca',
		'nombre',
		'descripcion'
	];

	// ---------- RELATIONSHIPS

	public function Marca() {
		return $this->belongsTo('App\Models\Marca', 'idMarca');
	}
}
