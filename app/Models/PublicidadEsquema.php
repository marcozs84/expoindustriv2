<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\PublicidadEsquema
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $idPublicidadEsquemaTipo
 * @property float|null $precio
 * @property int|null $idMoneda
 * @property int|null $ancho
 * @property int|null $alto
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\ListaItem|null $PublicidadEsquemaTipo
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PublicidadEsquema onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereAlto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereAncho($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereIdMoneda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereIdPublicidadEsquemaTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadEsquema wherePrecio($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PublicidadEsquema withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PublicidadEsquema withoutTrashed()
 * @mixin \Eloquent
 */
class PublicidadEsquema extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'PublicidadEsquema';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nombre',
		'descripcion',
		'idPublicidadEsquemaTipo',
		'precio',
		'idMoneda',
		'ancho',
		'alto'
	];

	protected $dates = ['deleted_at'];

	// ---------- RELATIONSHIPS

	public function PublicidadEsquemaTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idPublicidadEsquemaTipo');
	}

}