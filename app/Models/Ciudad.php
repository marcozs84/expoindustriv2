<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ciudad
 *
 * @property int $id
 * @property int|null $idPais
 * @property string|null $nombre
 * @property string|null $codigo
 * @property-read \App\Models\Pais|null $Pais
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ciudad onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ciudad whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ciudad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ciudad whereIdPais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ciudad whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ciudad withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ciudad withoutTrashed()
 * @mixin \Eloquent
 */
class Ciudad extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Ciudad';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Pais() {
		return $this->belongsTo('App\Models\Pais', 'idPais');
	}

}