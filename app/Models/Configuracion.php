<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Configuracion
 *
 * @property int $id
 * @property int|null $idUsuario
 * @property string|null $llave
 * @property string|null $valor
 * @property string|null $default
 * @property int|null $esSistema
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Usuario $Usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuracion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereEsSistema($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereIdUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereLlave($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Configuracion whereValor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuracion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuracion withoutTrashed()
 * @mixin \Eloquent
 */
class Configuracion extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Configuracion';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idUsuario',
		'llave',
		'valor',
		'default',
		'esSistema'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Usuario() {
		return $this->morphOne('App\Models\Usuario', 'owner');
	}
}