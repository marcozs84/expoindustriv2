<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Traduccion extends Model {


	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Traduccion';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'owner_type',
		'owner_id',
		'campo',
		'es',
		'en',
		'se',
	];

	protected $dates = ['deleted_at'];

	// ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}
}
