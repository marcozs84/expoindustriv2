<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductoCategoria
 *
 * @property int|null $idProducto
 * @property int|null $idCategoria
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Categoria[] $Categoria
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Producto[] $Producto
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCategoria whereIdCategoria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCategoria whereIdProducto($value)
 * @mixin \Eloquent
 */
class ProductoCategoria extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ProductoCategoria';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idProducto',
		'idCategoria'
	];

    // ---------- RELATIONSHIPS

	public function Categoria() {
		return $this->belongsToMany('App\Models\Categoria', 'idCategoria');
	}

	public function Producto() {
		return $this->belongsToMany('App\Models\Producto', 'idProducto');
	}

}