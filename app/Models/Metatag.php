<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

/**
 * App\Models\Metatag
 *
 * @property int $id
 * @property string|null $llave
 * @property string|null $name
 * @property string|null $es
 * @property string|null $en
 * @property string|null $se
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Metatag onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereLlave($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereSe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Metatag whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Metatag withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Metatag withoutTrashed()
 * @mixin \Eloquent
 */
class Metatag extends Model {

	use SoftDeletes;

	protected $table = 'Metatag';

	protected $fillable = [
		'llave',
		'name',
		'content',
		'se',
		'en',
		'es',
	];

	protected $dates = [
		'deleted_at',
	];

	protected $appends = [
		'content'
	];

	// ---------- ATTRIBUTES

	public function getContentAttribute(){
		$lang = App::getLocale();

		return $this->$lang;
	}
}
