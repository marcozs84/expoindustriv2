<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Comunicacion
 *
 * @property int $id
 * @property int|null $idComunicacionTipo
 * @property int|null $idPadre
 * @property string|null $remitente_type
 * @property int|null $remitente_id
 * @property string|null $destinatario_type
 * @property int|null $destinatario_id
 * @property string|null $nombre
 * @property string|null $email
 * @property string|null $asunto
 * @property string|null $mensaje
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Destinatario
 * @property-read \App\Models\Comunicacion|null $Padre
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Remitente
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comunicacion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereAsunto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereDestinatarioId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereDestinatarioType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereIdComunicacionTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereIdPadre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereMensaje($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereRemitenteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comunicacion whereRemitenteType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comunicacion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comunicacion withoutTrashed()
 * @mixin \Eloquent
 */
class Comunicacion extends Model  {

	use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Comunicacion';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nombre',
		'email',
		'asunto',
		'mensaje'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Padre() {
		return $this->belongsTo('App\Models\Comunicacion', 'idPadre');
	}

	public function Remitente() {
		return $this->morphTo();
	}

	public function Destinatario() {
		return $this->morphTo();
	}

}