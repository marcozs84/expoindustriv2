<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Factura
 *
 * @property int $id
 * @property int|null $idOrden
 * @property string|null $nombre
 * @property string|null $numero
 * @property string|null $serial
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Orden|null $Orden
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereIdOrden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereSerial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Factura whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Factura extends Model {
	protected $table = 'Factura';

	// ---------- RELATIONSHIPS

	public function Orden() {
		return $this->belongsTo('App\Models\Orden', 'idOrden');
	}
}
