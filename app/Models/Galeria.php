<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Galeria
 *
 * @property int $id
 * @property int|null $idGaleriaTipo
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\ListaItem|null $GaleriaTipo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Imagen[] $Imagenes
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Galeria onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereIdGaleriaTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Galeria whereOwnerType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Galeria withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Galeria withoutTrashed()
 * @mixin \Eloquent
 */
class Galeria extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Galeria';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idGaleriaTipo',
		'nombre',
		'descripcion',
		'estado'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function GaleriaTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'valor')->where('idLista', Lista::GALERIA_TIPO);
	}

	public function Imagenes() {
		return $this->hasMany('App\Models\Imagen', 'idGaleria');
	}

	public function Owner() {
		return $this->morphTo();
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting( function ( $galeria ) { // before delete() method call this
			$galeria->Imagenes()->delete();
//			if($galeria->Imagenes){a
//				foreach($galeria->Imagenes as $imagen) {
//					$imagen->delete();
//				}
//			}
			// do the rest of the cleanup...
		} );

		static::restoring(function($galeria) {
			$galeria->Imagenes()->withTrashed()->restore();
		});
	}

}