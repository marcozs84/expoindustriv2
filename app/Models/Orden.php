<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Orden
 *
 * @property int $id
 * @property int|null $idPadre
 * @property int|null $idCliente
 * @property int|null $idCotizacion
 * @property int|null $idUbicacionEntrega
 * @property int|null $idVendedor
 * @property int|null $idHorario
 * @property int|null $idOrdenTipo
 * @property int|null $idOrdenEstado
 * @property \Carbon\Carbon|null $fechaEntrega
 * @property float|null $precioTransporte
 * @property float|null $precioFacturado
 * @property string|null $nombreFactura
 * @property string|null $nit
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Cliente|null $Cliente
 * @property-read \App\Models\Cotizacion|null $Cotizacion
 * @property-read \App\Models\Empleado|null $Vendedor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrdenDetalle[] $OrdenDetalle
 * @property-read \App\Models\ListaItem|null $OrdenEstado
 * @property-read \App\Models\ListaItem|null $OrdenTipo
 * @property-read \App\Models\Orden|null $Padre
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductoItem[] $ProductoItems
 * @property-read \App\Models\Ubicacion|null $UbicacionEntrega
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Orden onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereFechaEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdCotizacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdVendedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdHorario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdOrdenEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdOrdenTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdPadre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereIdUbicacionEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereNit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereNombreFactura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden wherePrecioFacturado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden wherePrecioTransporte($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Orden whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Orden withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Orden withoutTrashed()
 * @mixin \Eloquent
 */
class Orden extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Orden';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idPadre',
		'idCliente',
		'idCotizacion',
		'idUbicacionEntrega',
		'idVendedor',
		'idHorario',
		'idOrdenTipo',
		'fechaEntrega',
		'precioTransporte',
		'precioFacturado',
		'nombreFactura',
		'nit'
	];

    protected $dates = [
    	'fechaEntrega',
		'deleted_at'
	];

    protected $appends = [
		'codename'
	];

    const ESTADO_ACTIVE = 1;
    const ESTADO_CLOSED = 2;
    const ESTADO_REJECTED = 3;
    const ESTADO_IN_PROGRESS = 4;
    const ESTADO_FINISHED = 5;


    // ---------- ATTRIBUTES

	public function getCodenameAttribute() {
		return 'ORD-'.str_pad($this->id, 5, '0', STR_PAD_LEFT);
	}

    // ---------- RELATIONSHIPS

	public function Cliente() {
		return $this->belongsTo('App\Models\Cliente', 'idCliente');
	}

	public function Cotizacion() {
		return $this->belongsTo('App\Models\Cotizacion', 'idCotizacion');
	}

	public function Vendedor() {
		return $this->belongsTo('App\Models\Usuario', 'idVendedor');
	}

	public function Facturas() {
		return $this->hasMany('App\Models\Factura', 'idOrden');
	}

	public function OrdenTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idOrdenTipo');
	}

	public function OrdenEstado() {
		return $this->belongsTo('App\Models\ListaItem', 'idOrdenEstado');
	}

	public function Padre() {
		return $this->belongsTo('App\Models\Orden', 'idPadre');
	}

	public function UbicacionEntrega() {
		return $this->belongsTo('App\Models\Ubicacion', 'idUbicacionEntrega');
	}

	public function ProductoItems() {
		return $this->belongsToMany('App\Models\ProductoItem', 'OrdenDetalle', 'idOrden', 'idProductoItem');
	}

	public function OrdenDetalles() {
		return $this->hasMany('App\Models\OrdenDetalle', 'idOrden');
	}

}