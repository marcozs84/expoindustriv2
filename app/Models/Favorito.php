<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Favorito
 *
 * @property int $id
 * @property int|null $idCliente
 * @property int|null $idPublicacion
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Cliente|null $Cliente
 * @property-read \App\Models\Producto|null $Producto
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Favorito onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorito whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorito whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorito whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorito whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorito whereIdCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Favorito whereIdPublicacion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Favorito withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Favorito withoutTrashed()
 * @mixin \Eloquent
 */
class Favorito extends Model  {

	use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Favorito';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Cliente() {
		return $this->belongsTo('App\Models\Cliente', 'idCliente');
	}

	public function Producto() {
		return $this->belongsTo('App\Models\Producto', 'idPublicacion');
	}

	public function Publicacion() {
		return $this->belongsTo('App\Models\Publicacion', 'idPublicacion');
	}

}