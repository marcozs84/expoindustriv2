<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Categoria
 *
 * @property int $id
 * @property int|null $idPadre
 * @property int|null $nivel
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $estado
 * @property int|null $visible
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Producto[] $Productos
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Categoria onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereIdPadre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereNivel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Categoria whereVisible($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Categoria withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Categoria withoutTrashed()
 * @mixin \Eloquent
 */
class Categoria extends Model  {

	use SoftDeletes;

	const INDUSTRIAL = 1;
	const TRANSPORTE = 13;
	const EQUIPO = 19;
	const AGRICOLA = 27;
	const MONTACARGA = 33;
	const OTROS = 37;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Categoria';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nivel',
		'nombre',
		'descripcion',
		'etado',
		'visible'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function SubCategorias() {
		return $this->hasMany('App\Models\Categoria', 'idPadre');
	}

	public function Padre() {
		return $this->belongsTo('App\Models\Categoria', 'idPadre');
	}

	public function Productos() {
		return $this->belongsToMany('App\Models\Producto', 'ProductoCategoria', 'idCategoria', 'idProducto');
	}

	public function Traduccion() {
		return $this->morphOne('App\Models\Traduccion', 'owner');
	}

}