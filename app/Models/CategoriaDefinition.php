<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CategoriaDefinition
 *
 * @property int $id
 * @property int|null $idCategoria
 * @property array $campos
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Categoria $Categoria
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriaDefinition whereCampos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriaDefinition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriaDefinition whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriaDefinition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriaDefinition whereIdCategoria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoriaDefinition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CategoriaDefinition extends Model {

	use SoftDeletes;

	protected $table = 'CategoriaDefinition';

	protected $fillable = [
		'idCategoria',
		'campos',
	];

	protected $casts = [
		'campos' => 'array'
	];

	protected $dates = [
		'deleted_at'
	];

	// ---------- RELATIONSHIPS
	
	public function Categoria() {
		return $this->belongsTo('App\Models\Categoria', 'idCategoria');
	}
}
