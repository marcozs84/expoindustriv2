<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Modulo
 *
 * @property int $id
 * @property string|null $nombre
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modulo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Modulo whereNombre($value)
 * @mixin \Eloquent
 */
class Modulo extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Modulo';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nombre'
	];

    public $timestamps = false;

    const MODULO_ANUNCIO = 1;
    const MODULO_PREORDEN = 2;
    const MODULO_ORDEN = 3;
    const MODULO_PUBLICIDAD = 4;
    const MODULO_BANNER = 5;
    const MODULO_CRM = 6;

}