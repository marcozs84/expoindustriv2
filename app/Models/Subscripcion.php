<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Subscripcion
 *
 * @property int $id
 * @property int|null $idSubscripcionTipo
 * @property int|null $idUsuario
 * @property string|null $fechaDesde
 * @property string|null $fechaHasta
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Subscripcion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereFechaDesde($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereFechaHasta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereIdSubscripcionTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereIdUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscripcion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Subscripcion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Subscripcion withoutTrashed()
 * @mixin \Eloquent
 */
class Subscripcion extends Model {

	use SoftDeletes;

    protected $table = 'Subscripcion';

	protected $dates = [
		'fechaDesde',
		'fechaHasta',
		'deleted_at'
	];

	protected $fillable = [
		'idSubscripcionTipo',
		'idUsuario',
		'fechaDesde',
		'fechaHasta',
	];

	// ---------- CONSTANTS

	const PLAN_BUSINESS = 1;
	const PLAN_PLUS = 2;

	// ---------- ATTRIBUTES

	public function getEstadoAttribute() {
		if(
			$this->fechaDesde <= Carbon::today() &&
			$this->fechaHasta >= Carbon::today()
		) {
			return 'Activo';
		} else {
			return 'Inactivo';
		}
	}

	// ---------- RELATIONSHIPS

	public function Tipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idSubscripcionTipo', 'valor')->where('idLista', Lista::SUBSCRIPCION_TIPO);
	}

	public function Usuario() {
		return $this->belongsTo('App\Models\Usuario', 'idUsuario');
	}

	public function Transacciones(){
		return $this->morphMany('App\Models\Transaccion', 'owner');
	}
}
