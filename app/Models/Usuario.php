<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Usuario
 *
 * @property int $id
 * @property int|null $idTipo
 * @property string|null $idStripe
 * @property string|null $idStripeTest
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string|null $cargo
 * @property int|null $estado
 * @property string|null $remember_token
 * @property string|null $forgot_token
 * @property string|null $confirmation_token
 * @property string|null $impersonation_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Cliente $Cliente
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Producto[] $Productos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Publicacion[] $Publicaciones
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $Roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subscripcion[] $Subscripciones
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Usuario onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereCargo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereConfirmationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereForgotToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereIdStripe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereIdStripeTest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereIdTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereImpersonationToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Usuario whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Usuario withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Usuario withoutTrashed()
 * @mixin \Eloquent
 */
class Usuario extends Authenticatable {

	use Notifiable;
	use SoftDeletes;

	const EMPLEADO = 1;
	const CLIENTE = 2;  // Tambien usado para ProveedorContacto -> todos los clientes también son ProveedorContacto

	const TIPO_CLIENTE = 1;
	const TIPO_PROVEEDOR = 2;
	const TIPO_EMPLEADO = 3;
	const TIPO_ADMINISTRADOR = 4;

	const ESTADO_ACTIVO = 1;
	const ESTADO_INACTIVO = 2;
	const ESTADO_ESPERANDO_CONFIRMACION = 3;
	const ESTADO_REPORTADO = 4;  // Suscripcion indeseaca (impersonation)

    protected $table = 'Usuario';

    protected $fillable = [
    	'idTipo',
		'idStripe',
		'username',
		'password',
		'email',
		'cargo',
		'estado',
		'confirmation_token',
		'impersonation_token',
	];

    protected $dates = ['deleted_at'];

	protected $hidden = [
		'password', 'remember_token',
		'idStripe', 'idStripeTest',
	];

	// ---------- CUSTOM

	private $permisos = [];
	private $oRoles = [];
	public $appends = [
		'nombres_apellidos'
	];

	// ---------- ATTRIBUTES

	public function getAuthIdentifierName(){
		return 'username';
	}

	public function getNombresApellidosAttribute() {
		return $this->Owner->Agenda->nombres_apellidos;
	}

    // ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	public function Roles() {
		return $this->belongsToMany('App\Models\Role', 'UsuarioRole', 'idUsuario', 'idRole');
	}

	public function Productos(){
		return $this->morphMany('App\Models\Producto', 'owner');
	}

	public function Publicaciones(){
		return $this->morphMany('App\Models\Publicacion', 'owner');
	}

	public function Cliente() {
		return $this->hasOne('App\Models\Cliente', 'idUsuario');
	}

	public function Subscripciones() {
		return $this->hasMany('App\Models\Subscripcion', 'idUsuario');
	}

	// ---------- CUSTOM

	public function getStripeId() {
		if(env('APP_ENV') == 'local'){
			return $this->idStripeTest;
		} else {
			return $this->idStripe;
		}
	}

	public function setStripeId($val) {
		if(env('APP_ENV') == 'local'){
			$this->idStripeTest = $val;
		} else {
			$this->idStripe = $val;
		}
	}

	public function isAdmin() {
		if($this->idTipo == Usuario::TIPO_ADMINISTRADOR) {
			return true;
		}
		return false;
//		self::load('Roles');
//		if(count($this->Roles->where('nombre', 'Administrador')) > 0){
//			return true;
//		} else {
//			return false;
//		}
	}

	public function hasRole($role){

		if(count($this->oRoles) == 0){
			$this->oRoles = $this->Roles;
//			logger("llenando oRoles");
		} else {
//			logger("oRoles LLENO");
		}

		if(is_string($role)){
			return $this->oRoles->contains('nombre', $role);
		}

		return !! $role->intersect($this->oRoles)->count();
	}

	public function Permisos(){
		$roles = $this->Roles;

		$this->permisos = [];
		foreach($roles as $role){

			$oPermisos = $role->RolePermisos; //()->with(['Permiso', 'Convenio'])->get();

			logger("oPermisos: ");
			logger($oPermisos);
			if(count($oPermisos) > 0){
				foreach($oPermisos as $rp){
//				$this->permisos[] = $permiso->nombre;
					if(!isset($this->permisos[$rp->Permiso->nombre])) {
						$this->permisos[$rp->Permiso->nombre] = [];
					}

					if($rp->Convenio){
						if(!isset($this->permisos[$rp->Permiso->nombre][$rp->Convenio->nombreCorto])) {
							$this->permisos[$rp->Permiso->nombre][] = $rp->Convenio->nombreCorto;
						}
					}
				}
			}
		}
//		$permisos = array_unique($permisos);

		return collect($this->permisos);
	}

}
