<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Destino
 *
 * @property int $id
 * @property int|null $idPais
 * @property string|null $nombre
 * @property float|null $precio
 * @property string|null $moneda
 * @property int|null $estado
 * @property-read \App\Models\Pais $Pais
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Publicacion[] $Publicacion
 * @property-read mixed $precio_publicacion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Destino whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Destino whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Destino whereIdPais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Destino whereMoneda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Destino whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Destino wherePrecio($value)
 * @mixin \Eloquent
 */
class Destino extends Model {

	protected $table = 'Destino';

	public $timestamps = false;

	public $appends = [
		'precio_publicacion',
	];

	protected $fillable = [
		'idPais',
		'nombre',
		'precio',
		'moneda',
		'estado'
	];

	// ---------- ATTRIBUTES

	public function getPrecioPublicacionAttribute(){
		$currencyFrom = 'usd';
		$currencyTo = session('current_currency', 'usd');

		if($currencyFrom == $currencyTo) {
			return $this->precio;
		} else {
			$llave = "tipo_cambio_{$currencyFrom}_{$currencyTo}";
			$exchange = Configuracion::where('llave', $llave)->first()->valor;
			return round($this->precio * $exchange, 2);
//			return number_format(($this->precio / $exchange), 2);
		}
	}

	// ---------- RELATIONSHIPS

	public function Publicacion(){
		return $this->belongsToMany('App\Models\Publicacion', 'PublicacionDestino', 'idDestino', 'idPublicacion');
	}
	
	public function Pais() {
		return $this->belongsTo('App\Models\Pais', 'idPais');
	}
}
