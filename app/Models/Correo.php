<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Correo
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $correo
 * @property string|null $descripcion
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Correo onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereCorreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Correo whereOwnerType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Correo withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Correo withoutTrashed()
 * @mixin \Eloquent
 */
class Correo extends Model  {

	use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Correo';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'correo',
		'descripcion',
		'estado'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}
}