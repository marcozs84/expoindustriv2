<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ConsultaExterna
 *
 * @property int $id
 * @property int|null $idTipoConsulta
 * @property string|null $nombres
 * @property string|null $apellidos
 * @property string|null $email
 * @property string|null $pais
 * @property string|null $telefono
 * @property string|null $asunto
 * @property string|null $descripcion
 * @property int|null $estado
 * @property int|null $idUsuarioRevisor
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Usuario|null $UsuarioRevisor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereApellidos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereAsunto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereIdTipoConsulta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereIdUsuarioRevisor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereNombres($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna wherePais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ConsultaExterna whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ConsultaExterna extends Model {

	use SoftDeletes;
	protected $table = 'ConsultaExterna';

	protected $fillable = [
		'idTipoConsulta',
		'nombres',
		'apellidos',
		'email',
		'pais',
		'telefono',
		'asunto',
		'descripcion',
		'idUsuarioRevisor',
		'extrainfo',
	];

	protected $casts = [
		'extrainfo' => 'array',
	];

	const ESTADO_NEW = 1;
	const ESTADO_UNDER_REVIEW = 2;
	const ESTADO_RESOLVED = 3;
	const ESTADO_CLOSED = 4;
	const ESTADO_REJECTED = 5;

	/**
	 * Mensajes enviados desde la página de contacto.
	 */
	const TIPO_CONTACT = 1;

	/**
	 * Mensajes enviados desde /services
	 * normalmente consultas sobre el servicio de Banners
	 */
	const TIPO_BANNER = 2;
	/**
	 *
	 * Mensajes enviados desde el buscador de la página principal
	 * "¿No encontró lo que buscaba?"
	 */
	const TIPO_SEARCH_ALERT = 3;
	/**
	 * Mensajes enviados desde /services
	 * normalmente consultas sobre el servicio de Inspeccion tédnica
	 */
	const TIPO_INSPECTION = 4;
	/**
	 * Mensajes enviados desde /services
	 * normalmente consultas sobre el servicio de Soporte Logístico
	 */
	const TIPO_SUPPORT = 5;
	/**
	 * Mensajes enviados desde /services
	 * normalmente consultas sobre el servicio de Tienda Online
	 */
	const TIPO_ISTORE = 6;
	/**
	 * Mensajes enviados desde la página de contacto que se consideran SPAM (robot)
	 */
	const TIPO_CONTACT_ROBOT = 7;

	/**
	 * Se utiliza cuando el precio está oculto en un producto
	 * habilita un formulario de contacto para un producto específico.
	 */
	const TIPO_PRODUCT_CONTACT_CONSULTANT = 8;

	/**
	 * Mensajes enviados desde Private->Account (Cuenta)
	 * normalmente solicitudes de Renovación de Plan (iStore u otro)
	 */
	const TIPO_SUBSCRIPCION_RENEWAL = 9;

	/**
	 * Ubicación: Mi Cuenta
	 * Requerimiento de automatización para extracción
	 * desde un sitio de terceros (WebScrapping)
	 */
	const TIPO_CLIENT_AUTOMATIC_IMPORT = 10;

	// ---------- RELATIONSHIPS

	public function UsuarioRevisor(){
		return $this->belongsTo('App\Models\Usuario', 'idUsuarioRevisor');
	}


}
