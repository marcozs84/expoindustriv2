<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UsuarioRole
 *
 * @property int|null $idUsuario
 * @property int|null $idRole
 * @property-read \App\Models\Role|null $Role
 * @property-read \App\Models\Usuario|null $Usuario
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsuarioRole whereIdRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsuarioRole whereIdUsuario($value)
 * @mixin \Eloquent
 */
class UsuarioRole extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'UsuarioRole';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idUsuario',
		'idRole'
	];

    // ---------- RELATIONSHIPS

	public function Usuario() {
		return $this->belongsTo('App\Models\Usuario', 'idUsuario');
	}

	public function Role() {
		return $this->belongsTo('App\Models\Role', 'idRole');
	}

}