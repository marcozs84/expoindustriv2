<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;

class Archivo extends Model {

	use SoftDeletes;

	const ESTADO_ACTIVO = 1;
	const ESTADO_PENDIENTE = 2;	// Descarga pendiente
	const ESTADO_FALLIDO = 3;

	protected $table = 'Archivo';

	protected $fillable = [
		'realname',
		'ruta',
		'filename',
		'mimetype',
		'descripcion',
		'estado',
		'extrainfo',
		'fuente',
	];

	protected $dates = [
		'deleted_at',
	];

	protected $casts = [
		'extrainfo' => 'array'
	];

	public $appends = [
		'created_at_literal'
	];

	// ---------- ATTRIBUTES

	public function getCreatedAtLiteralAttribute() {
		$created_at = $this->created_at;
		$meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		return $created_at->format('d').' '.$meses[ $created_at->format('n') ].' '.$created_at->format('Y');
	}

	// ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting( function ( $archivo ) { // before delete() method call this

			if ( $archivo->isForceDeleting() ) {
				File::delete( storage_path( $archivo->ruta ) );
			}

			// do the rest of the cleanup...
		} );

	}
}
