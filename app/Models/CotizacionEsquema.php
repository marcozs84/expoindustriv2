<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\CotizacionEsquema
 *
 * @property int $id
 * @property int|null $idCategoria
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property float|null $revisionTecnicaPrecio
 * @property float|null $fotografiasExtraPrecio
 * @property float|null $seguroPrecio
 * @property float|null $transporteCompartidoPrecio
 * @property float|null $transportePrivadoPrecio
 * @property float|null $transporteAbiertoPrecio
 * @property string|null $zonasPrecio
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CotizacionEsquema onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereFotografiasExtraPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereRevisionTecnicaPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereSeguroPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereTransporteAbiertoPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereTransporteCompartidoPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereTransportePrivadoPrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CotizacionEsquema whereZonasPrecio($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CotizacionEsquema withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CotizacionEsquema withoutTrashed()
 * @mixin \Eloquent
 */
class CotizacionEsquema extends Model  {

	use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'CotizacionEsquema';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'idCategoria',
		'nombre',
		'descripcion',
		'revisionTecnicaPrecio',
		'fotografiasExtraPrecio',
		'seguroPrecio',
		'transporteCompartidoPrecio',
		'transportePrivadoPrecio',
		'transporteAbiertoPrecio',
		'zonasPrecio',
	];

    protected $dates = ['deleted_at'];
    
    protected $casts = [
    	'zonasPrecio' => 'array'
    ];

}