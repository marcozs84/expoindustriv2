<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Publicacion
 *
 * @property int $id
 * @property int|null $idProducto
 * @property int|null $idTipoPago
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property \Carbon\Carbon|null $fechaInicio
 * @property \Carbon\Carbon|null $fechaFin
 * @property int|null $visitas
 * @property int|null $estado
 * @property string|null $token
 * @property int|null $dias
 * @property float|null $precio
 * @property string|null $moneda
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cotizacion[] $Cotizaciones
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Destino[] $Destinos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Favorito[] $Favoritos
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read \App\Models\Producto|null $Producto
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaccion[] $Transacciones
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transporte[] $Transportes
 * @property-read mixed $fecha_fin
 * @property-read mixed $fecha_inicio
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Publicacion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereDias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereFechaFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereFechaInicio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereIdProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereIdTipoPago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereMoneda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicacion whereVisitas($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Publicacion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Publicacion withoutTrashed()
 * @mixin \Eloquent
 */
class Publicacion extends Model {

	use SoftDeletes;

	protected $table = 'Publicacion';

	protected $fillable = [
		'idProducto',
		'fechaInicio',
		'fechaFin',
		'visitas',
		'estado',
		'token',
		'dias',
	];

	protected $dates = [
		'fechaInicio',
		'fechaFin',
		'deleted_at'
	];

//	protected $dateFormat = 'U';

//	protected $appends = [
//		'fecha_inicio',
//		'fecha_fin'
//	];

	/** ********************************************
	 * If adding ESTADOS remember to update:
	 * 		announcementInbox/index
	 *		publicacion/index
	 *		publicacion/create
	 *		publicacion/edit
	 */
	const ESTADO_DRAFT = 1;
	const ESTADO_AWAITING_APPROVAL = 2;
	const ESTADO_APPROVED = 3;
	const ESTADO_REJECTED = 4;
	const ESTADO_DISABLED = 5;

	/** ********************************************
	 * If adding PAGOS remember to update:
	 * 		announcementInbox/index
	 *		publicacion/index
	 *		publicacion/create
	 *		publicacion/edit
	 */
	const PAGO_NONE = 0;
	const PAGO_STRIPE = 1;
	const PAGO_FAKTURA = 2;
	const PAGO_SUBSCRIPTION = 3;
	/** ******************************************** */

	// ---------- ATTRIBUTES

//	public function getFechaInicioAttribute(){
////		return $this->fechaInicio;
//		if(isset($this->attributes['fechaInicio']) && $this->attributes['fechaInicio'] != '') {
//			return Carbon::parse($this->attributes['fechaInicio'])->format('d.m.Y');
//		} else {
//			return '';
//		}
//
//	}
//
//	public function getFechaFinAttribute(){
////		return $this->fechaInicio;
//		if(isset($this->attributes['fechaFin']) && $this->attributes['fechaFin'] != '') {
//			return Carbon::parse($this->attributes['fechaFin'])->format('d.m.Y');
//		} else {
//			return '';
//		}
//
//	}


	// ---------- RELATIONSHIPS

	public function Cotizaciones() {
		return $this->hasMany('App\Models\Cotizacion', 'idPublicacion');
	}

	public function Destinos() {
		return $this->belongsToMany('App\Models\Destino', 'PublicacionDestino', 'idPublicacion', 'idDestino')->withPivot('precio');
	}

	public function Estado() {
		return $this->hasOne('App\Models\ListaItem', 'valor', 'estado')->where('idLista', Lista::PUBLICACION_ESTADO);
	}

	public function Favoritos() {
		return $this->hasMany('App\Models\Favorito', 'idProducto');
	}

	public function Owner() {
		return $this->morphTo();
	}

	public function Producto() {
		return $this->belongsTo('App\Models\Producto', 'idProducto');
	}

	public function Transportes(){
		return $this->belongsToMany('App\Models\Transporte', 'PublicacionTransporte', 'idPublicacion', 'idTransporte')->withPivot('precio', 'disponibilidad');
	}

	public function Transacciones(){
		return $this->morphMany('App\Models\Transaccion', 'owner');
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting(function( $publicacion ) { // before delete() method call this
//			$publicacion->Producto()->delete();
//			if($publicacion->Producto){
//				$publicacion->Producto->delete();
//			}
			// do the rest of the cleanup...
		});
	}
}
