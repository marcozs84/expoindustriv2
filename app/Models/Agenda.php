<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Agenda
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property int|null $idEstadoCivil
 * @property string|null $nombres
 * @property string|null $apellidos
 * @property string|null $sexo
 * @property string|null $ci
 * @property string|null $pais
 * @property string|null $nit
 * @property string|null $nombreFactura
 * @property int|null $puntosAcumulados
 * @property \Carbon\Carbon|null $fechaNacimiento
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read mixed $apellidos_nombres
 * @property-read mixed $nombres_apellidos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda abrev()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agenda onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereApellidos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereCi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereFechaNacimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereIdEstadoCivil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereNit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereNombreFactura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereNombres($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda wherePuntosAcumulados($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agenda whereSexo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agenda withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Agenda withoutTrashed()
 * @mixin \Eloquent
 */
class Agenda extends Model {

	use SoftDeletes;

	const sexo_hombre = 'h';
	const sexo_mujer = 'm';

	protected $table = 'Agenda';

	protected $fillable = [
		'idEstadoCivil',
		'nombres',
		'apellidos',
		'sexo',
		'ci',
		'pais',
		'nit',
		'nombreFactura',
		'puntosAcumulados',
		'fechaNacimiento'
	];

	protected $dates = [
		'fechaNacimiento',
		'deleted_at'
	];

	protected $appends = [
		'nombres_apellidos',
		'apellidos_nombres'
	];

	public static $abrev = [
		'id',
		'nombres',
		'apellidos',
		'sexo'
	];

	// ---------- ATTRIBUTES

	public function getNombresApellidosAttribute() {
		return $this->nombres.' '.$this->apellidos;
	}

	public function getApellidosNombresAttribute() {
		return $this->apellidos.' '.$this->nombres;
	}

	// ---------- SCOPES

	public function scopeAbrev($q) {
		//return $q->select(self::$abrev);
		//$table = (new Agenda)->getTable();
		$table = self::getTable();
		$campos = [];
		foreach (self::$abrev as $field) {
			$campos[] = $table.'.'.$field;
		}
		$q->select($campos);
	}

    // ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	public function Correos() {
		return $this->morphMany('App\Models\Correo', 'owner');
	}

	public function Telefonos() {
		return $this->morphMany('App\Models\Telefono', 'owner');
	}
}
