<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Proveedor
 *
 * @property int $id
 * @property int|null $idPadre
 * @property int|null $idCiudad
 * @property string|null $nombre
 * @property string|null $nit
 * @property string|null $descripcion
 * @property int|null $idProveedorEstado
 * @property int|null $idProveedorTipo
 * @property string|null $urlSitio
 * @property string|null $urlFacebook
 * @property string|null $urlTwitter
 * @property string|null $urlGooglePlus
 * @property int|null $tieneStore
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Ciudad|null $Ciudad
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ubicacion[] $Direcciones
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Galeria[] $Galerias
 * @property-read \App\Models\Proveedor|null $Padre
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProveedorContacto[] $ProveedorContactos
 * @property-read \App\Models\ListaItem|null $ProveedorEstado
 * @property-read \App\Models\ListaItem|null $ProveedorTipo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Telefono[] $Telefonos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ubicacion[] $Ubicaciones
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereIdCiudad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereIdPadre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereIdProveedorEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereIdProveedorTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereNit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereTieneStore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereUrlFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereUrlGooglePlus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereUrlSitio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proveedor whereUrlTwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Proveedor withoutTrashed()
 * @mixin \Eloquent
 */
class Proveedor extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Proveedor';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idPadre',
		'idCiudad',
		'nombre',
		'nit',
		'descripcion',
		'idProveedorEstado',
		'idProveedorTipo',
		'urlSitio',
		'urlFacebook',
		'urlTwitter',
		'urlGooglePlus',
		'tieneStore',
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Ciudad() {
		return $this->belongsTo('App\Models\Ciudad', 'idCiudad');
	}

	public function Padre() {
		return $this->belongsTo('App\Models\Proveedor', 'idPadre');
	}

	public function ProveedorEstado() {
		return $this->belongsTo('App\Models\ListaItem', 'idProveedorEstado');
	}

	public function ProveedorTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idProveedorTipo');
	}

	public function ProveedorContactos() {
		return $this->hasMany('App\Models\ProveedorContacto', 'idProveedor');
	}

	public function Galerias(){
		return $this->morphMany('App\Models\Galeria', 'owner');
	}

	public function Telefonos(){
		return $this->morphMany('App\Models\Telefono', 'owner');
	}

	public function Traducciones(){
		return $this->morphMany('App\Models\Traduccion', 'owner');
	}

	public function Direcciones(){
		return $this->morphMany('App\Models\Ubicacion', 'owner');
	}

	public function Ubicaciones() {
		return $this->morphMany('App\Models\Ubicacion', 'owner');
	}
}