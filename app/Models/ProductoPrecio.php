<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ProductoPrecio
 *
 * @property int $id
 * @property int|null $idProducto
 * @property int|null $idProductoItem
 * @property float|null $precio
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Producto|null $Producto
 * @property-read \App\Models\ProductoItem|null $ProductoItem
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductoPrecio onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoPrecio whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoPrecio whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoPrecio whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoPrecio whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoPrecio whereIdProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoPrecio whereIdProductoItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoPrecio wherePrecio($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductoPrecio withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductoPrecio withoutTrashed()
 * @mixin \Eloquent
 */
class ProductoPrecio extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ProductoPrecio';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idProducto',
		'idProductoItem',
		'precio'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Producto() {
		return $this->belongsTo('App\Models\Producto', 'idProducto');
	}

	public function ProductoItem() {
		return $this->belongsTo('App\Models\ProductoItem', 'idProductoItem');
	}

}