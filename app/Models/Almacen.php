<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Almacen
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property string|null $deletec_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Almacen onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereDeletecAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Almacen whereOwnerType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Almacen withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Almacen withoutTrashed()
 * @mixin \Eloquent
 */
class Almacen extends Model {

	use SoftDeletes;

	protected $table = 'Almacen';

	protected $filllable = [
		'nombres',
		'descripcion'
	];

	protected $dates = ['deleted_at'];

	// ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}
}
