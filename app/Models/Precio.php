<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Precio
 *
 * @property int $id
 * @property int|null $owner_id
 * @property string|null $owner_type
 * @property int|null $idTipo
 * @property float|null $precio
 * @property string|null $moneda
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Precio onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereIdTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereMoneda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Precio whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Precio withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Precio withoutTrashed()
 * @mixin \Eloquent
 */
class Precio extends Model {

	use SoftDeletes;

	protected $table = 'Precio';

	protected $fillable = [
		'owner_id',
		'owner_type',
		'idTipo',
		'precio',
		'moneda',
		'estado',
	];

	protected $dates = [
		'deleted_at',
	];

	const TIPO_PRECIO_VENTA = 1;

	// ---------- ATTRIBUTES

	public function getPrecioLiteralAttribute() {
		return $this->precio . " " . strtoupper( $this->moneda );
	}
	// ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}
}
