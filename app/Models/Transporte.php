<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transporte
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property float|null $precio
 * @property int|null $esEspecial
 * @property int|null $estado
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Publicacion[] $Publicaciones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transporte whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transporte whereEsEspecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transporte whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transporte whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transporte whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transporte wherePrecio($value)
 * @mixin \Eloquent
 */
class Transporte extends Model {
    protected $table = 'Transporte';
    public $timestamps = false;

	const TIPO_CONTENEDOR_COMPARTIDO = 1;
	const TIPO_CONTENEDOR_EXCLUSIVO = 2;
	const TIPO_ENVIO_ABIERTO = 3;
	const TIPO_TERRESTRE = 4;
    // ---------- RELATIONSHIPS

	public function Publicaciones(){
		return $this->belongsToMany('App\Models\Publicacion', 'PublicacionTransporte', 'idTransporte', 'idPublicacion');
	}

	public function Traduccion() {
		return $this->morphMany('App\Models\Traduccion', 'owner');
	}
}
