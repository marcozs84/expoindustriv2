<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Newsletter
 *
 * @property int $id
 * @property string|null $email
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Newsletter onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Newsletter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Newsletter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Newsletter withoutTrashed()
 * @mixin \Eloquent
 */
class Newsletter extends Model {

	use SoftDeletes;

	protected $table = 'Newsletter';

	protected $fillable = [
		'email',
		'status',
	];

	protected $dates = [
		'deleted_at',
	];
}
