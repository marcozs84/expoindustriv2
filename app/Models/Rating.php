<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Rating
 *
 * @property int $id
 * @property int|null $idProducto
 * @property int|null $idAutor
 * @property string|null $nombre
 * @property string|null $email
 * @property int|null $rating
 * @property string|null $titulo
 * @property string|null $comentario
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Producto|null $Producto
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Rating onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereComentario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereIdAutor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereIdProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rating whereTitulo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Rating withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Rating withoutTrashed()
 * @mixin \Eloquent
 */
class Rating extends Model  {

	use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Rating';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idProducto',
		'idAutor',
		'nombre',
		'email',
		'rating',
		'titulo',
		'comentario'
	];

	protected $dates = ['deleted_at'];

	// ---------- RELATIONSHIPS

	public function Producto() {
		return $this->belongsTo('App\Models\Producto', 'idProducto');
	}

}