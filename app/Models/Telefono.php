<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Telefono
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $numero
 * @property string|null $descripcion
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Telefono whereOwnerType($value)
 * @mixin \Eloquent
 */
class Telefono extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Telefono';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'numero',
		'descripcion',
		'estado'
	];

	protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	public function __toString() {
		return $this->numero;
	}

}