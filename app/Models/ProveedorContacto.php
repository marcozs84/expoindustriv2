<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ProveedorContacto
 *
 * @property int $id
 * @property int|null $idProveedor
 * @property int|null $idUsuario
 * @property int|null $idAgenda
 * @property int|null $idProveedorContactoEstado
 * @property int|null $idProveedorContactoTipo
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Agenda $Agenda
 * @property-read \App\Models\Proveedor|null $Proveedor
 * @property-read \App\Models\ListaItem|null $ProveedorContactoEstado
 * @property-read \App\Models\ListaItem|null $ProveedorContactoTipo
 * @property-read \App\Models\Usuario $Usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProveedorContacto onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereIdAgenda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereIdProveedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereIdProveedorContactoEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereIdProveedorContactoTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProveedorContacto whereIdUsuario($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProveedorContacto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProveedorContacto withoutTrashed()
 * @mixin \Eloquent
 */
class ProveedorContacto extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ProveedorContacto';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idProveedor',
		'idUsuario',
		'idAgenda',
		'idProveedorContactoEstado',
		'idProveedorContactoTipo',
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Agenda() {
		return $this->morphOne('App\Models\Agenda', 'owner');
	}

	public function Proveedor() {
		return $this->belongsTo('App\Models\Proveedor', 'idProveedor');
	}

	public function ProveedorContactoEstado() {
		return $this->belongsTo('App\Models\ListaItem', 'idProveedorContactoEstado');
	}

	public function ProveedorContactoTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idProveedorContactoTipo');
	}

	public function Usuario() {
		return $this->morphOne('App\Models\Usuario', 'owner');
	}

}