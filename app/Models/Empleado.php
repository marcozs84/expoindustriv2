<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Empleado
 *
 * @property int $id
 * @property int|null $idUsuario
 * @property int|null $idAgenda
 * @property int|null $idEmpleadoTipo
 * @property float|null $salario
 * @property int|null $idEmpleadoEstado
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Agenda $Agenda
 * @property-read \App\Models\ListaItem|null $EmpleadoEstado
 * @property-read \App\Models\ListaItem|null $EmpleadoTipo
 * @property-read \App\Models\Usuario $Usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empleado onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereIdAgenda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereIdEmpleadoEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereIdEmpleadoTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereIdUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereSalario($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empleado withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empleado withoutTrashed()
 * @mixin \Eloquent
 */
class Empleado extends Model  {

	use SoftDeletes;

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Empleado';

	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'idUsuario',
		'idAgenda',
		'idEmpleadoTipo',
		'idEmpleadoEstado',
		'salario'
	];

	protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Agenda() {
		return $this->morphOne('App\Models\Agenda', 'owner');
	}

	public function EmpleadoEstado() {
		return $this->belongsTo('App\Models\ListaItem', 'idEmpleadoEstado');
	}

	public function EmpleadoTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idEmpleadoTipo');
	}

	public function Usuario() {
		return $this->morphOne('App\Models\Usuario', 'owner');
	}

}