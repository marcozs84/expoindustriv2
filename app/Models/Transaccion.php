<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Transaccion
 *
 * @property int $id
 * @property int|null $idUsuario
 * @property string|null $idStripeCustomer
 * @property string|null $idStripeTransaction
 * @property string|null $producto
 * @property float|null $monto
 * @property string|null $moneda
 * @property string|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Usuario|null $Usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Transaccion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereIdStripeCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereIdStripeTransaction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereIdUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereMoneda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaccion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Transaccion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Transaccion withoutTrashed()
 * @mixin \Eloquent
 */
class Transaccion extends Model {
	use SoftDeletes;

	protected $table = "Transaccion";

    protected $fillable = [
    	'idTransaccionTipo',
    	'idUsuario',
		'idStripeCustomer',
		'idStripeTransaction',
		'producto',
		'monto',
		'impuesto',
		'moneda',
		'estado',
		'procesada',
		'extrainfo',
	];

	protected $dates = ['deleted_at'];

	protected $casts = [
		'extrainfo' => 'array'
	];

	// ---------- CONSTANTS

	const ESTADO_SUCCEEDED = 1;
	const ESTADO_REJECTED = 2;
	const ESTADO_PROCESSED = 3;

	const TIPO_CARD = 1;
	const TIPO_FAKTURA = 2;
	const TIPO_CASH = 3;


	// ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	public function Usuario(){
		return $this->belongsTo('App\Models\Usuario', 'idUsuario');
	}

	public function Archivos() {
		return $this->morphMany('App\Models\Archivo', 'owner');
	}

	public function TransaccionTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idTransaccionTipo', 'valor')->where('idLista', Lista::TRANSACCION_TIPO);
	}

	public function TransaccionEstado() {
		return $this->belongsTo('App\Models\ListaItem', 'idTransaccionTipo', 'valor')->where('idLista', Lista::TRANSACCION_ESTADO);
	}

}
