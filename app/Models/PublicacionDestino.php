<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PublicacionDestino
 *
 * @property int $id
 * @property int|null $idPublicacion
 * @property int|null $idDestino
 * @property float|null $precio
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionDestino whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionDestino whereIdDestino($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionDestino whereIdPublicacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicacionDestino wherePrecio($value)
 * @mixin \Eloquent
 */
class PublicacionDestino extends Model {

	protected $table = 'PublicacionDestino';
}
