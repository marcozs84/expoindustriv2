<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Gasto
 *
 * @property int $id
 * @property int|null $idUsuario
 * @property int|null $idGastoTipo
 * @property float|null $monto
 * @property string|null $descripcion
 * @property string|null $extrainfo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Usuario|null $Usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Gasto onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereExtrainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereIdGastoTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereIdUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gasto whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Gasto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Gasto withoutTrashed()
 * @mixin \Eloquent
 */
class Gasto extends Model {

	use SoftDeletes;

	protected $table = 'Gasto';

	protected $fillable = [
		'idUsuario',
		'idGastoTipo',
		'fecha',
		'monto',
		'moneda',
		'descripcion',
		'extrainfo',
	];

	protected $dates = [
		'deleted_at',
		'fecha',
	];

	// ---------- RELATIONSHIPS

	public function Archivos() {
		return $this->morphMany('App\Models\Archivo', 'owner');
	}

	public function Usuario() {
		return $this->belongsTo('App\Models\Usuario', 'idUsuario');
	}

	public function GastoTipo() {
		return $this->hasOne('App\Models\ListaItem', 'valor', 'idGastoTipo')
			->where('idLista', Lista::GASTO_TIPO);
	}

}
