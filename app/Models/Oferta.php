<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Oferta
 *
 * @property int $id
 * @property int|null $idProductoItem
 * @property int|null $idVendedor
 * @property int|null $idCliente
 * @property int|null $idOrden
 * @property int|null $noOferta
 * @property float|null $monto
 * @property \Carbon\Carbon|null $fechaEnvio
 * @property int|null $tipoVisita
 * @property int|null $esClienteNuevo
 * @property string|null $telefono
 * @property string|null $email
 * @property string|null $lugar
 * @property int|null $estado
 * @property string|null $observaciones
 * @property array $extrainfo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Archivo[] $Archivos
 * @property-read \App\Models\Cliente|null $Cliente
 * @property-read \App\Models\ProductoItem|null $ProductoItem
 * @property-read \App\Models\Usuario|null $Vendedor
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Oferta onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereEsClienteNuevo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereExtrainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereFechaEnvio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereIdCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereIdOrden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereIdProductoItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereIdVendedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereLugar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereNoOferta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereTipoVisita($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Oferta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Oferta withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Oferta withoutTrashed()
 * @mixin \Eloquent
 */
class Oferta extends Model {

	use SoftDeletes;

	protected $table = 'Oferta';

	protected $fillable = [
		'idProductoItem',
		'idVendedor',
		'idCliente',
		'idOrden',
		'noOferta',
		'monto',
		'fechaEnvio',
		'tipoVisita',
		'esClienteNuevo',
		'telefono',
		'email',
		'lugar',
		'estado',
		'observaciones',
		'extrainfo',
	];

	protected $dates = [
		'deleted_at',
		'fechaEnvio',
	];

	protected $casts = [
		'extrainfo' => 'array',
	];

	// ---------- RELATIONSHIPS

	public function Archivos() {
		return $this->morphMany('App\Models\Archivo', 'owner');
	}

	public function Cliente() {
		return $this->belongsTo('App\Models\Cliente', 'idCliente');
	}

	public function ProductoItem() {
		return $this->belongsTo('App\Models\ProductoItem', 'idProductoItem');
	}

	public function Vendedor() {
		return $this->belongsTo('App\Models\Usuario', 'idVendedor');
	}

}
