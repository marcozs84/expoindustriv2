<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Calculadora
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property float|null $informe_tecnico
 * @property float|null $seguro_extra
 * @property float|null $tasa_tramite
 * @property float|null $contenedor_compartido
 * @property float|null $contenedor_exclusivo
 * @property float|null $envio_abierto
 * @property float|null $destino_1
 * @property float|null $destino_2
 * @property float|null $destino_3
 * @property float|null $destino_4
 * @property float|null $destino_5
 * @property float|null $destino_6
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereContenedorCompartido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereContenedorExclusivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereDestino1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereDestino2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereDestino3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereDestino4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereDestino5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereDestino6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereEnvioAbierto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereInformeTecnico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereSeguroExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereTasaTramite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calculadora whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Calculadora extends Model {
    protected $table = 'Calculadora';

    // ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}
}
