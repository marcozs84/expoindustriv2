<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Excepcion
 *
 * @property int $id
 * @property string|null $ip
 * @property string|null $pais
 * @property string|null $codigo
 * @property string|null $tipo
 * @property string|null $mensaje
 * @property string|null $url
 * @property string|null $route
 * @property string|null $archivo
 * @property int|null $linea
 * @property string|null $params
 * @property string|null $stackTrace
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereArchivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereLinea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereMensaje($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion wherePais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereStackTrace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Excepcion whereUrl($value)
 * @mixin \Eloquent
 */
class Excepcion extends Model {
    protected $table = 'Excepcion';
}
