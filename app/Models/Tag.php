<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tag
 *
 * @property int $id
 * @property int|null $idPadre
 * @property string|null $nombre
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read \App\Models\Traduccion $Traduccion
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Traduccion[] $Traducciones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereIdPadre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereNombre($value)
 * @mixin \Eloquent
 */
class Tag extends Model {

	protected $table = 'Tag';
	public $timestamps = false;

	// ---------- R E L A T I O N S H I P S

	public function Owner() {
		return $this->morphTo();
	}

	public function Traduccion() {
		return $this->morphOne('App\Models\Traduccion', 'owner');
	}

	public function Traducciones() {
		return $this->morphMany('App\Models\Traduccion', 'owner');
	}

	public function Padre() {
		return $this->belongsTo('App\Models\Tag', 'idPadre');
	}

	public function Hijos() {
		return $this->hasMany('App\Models\Tag', 'idPadre');
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting(function($tag) { // before delete() method call this

			if($tag->Traducciones->count() > 0){
				$tag->Traducciones()->delete();
			}
			// do the rest of the cleanup...
		});
	}
}
