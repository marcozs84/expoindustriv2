<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

/**
 * App\Models\Producto
 *
 * @property int $id
 * @property int|null $idMarca
 * @property int|null $idProductoTipo
 * @property int|null $idProductoEstado
 * @property int|null $idModelo
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $codigo
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $unidades
 * @property float|null $precioCompra
 * @property string|null $monedaCompra
 * @property float|null $precioMinimo
 * @property string|null $monedaMinimo
 * @property float|null $precioUnitario
 * @property string|null $monedaUnitario
 * @property float|null $precioEuropa
 * @property string|null $monedaEuropa
 * @property float|null $precioProveedor
 * @property string|null $monedaProveedor
 * @property bool $precioFijo
 * @property bool $precioSudamerica
 * @property float|null $precioFijoUsd
 * @property float|null $precioFijoEur
 * @property float|null $precioFijoSek
 * @property int|null $anio
 * @property int|null $esNuevo
 * @property int|null $visitas
 * @property string|null $paisFabricacion
 * @property string|null $consumo
 * @property string|null $peso
 * @property string|null $fabricacion
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Categoria[] $Categorias
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cotizacion[] $Cotizaciones
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Galeria[] $Galerias
 * @property-read \App\Models\Marca|null $Marca
 * @property-read \App\Models\Modelo|null $Modelo
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read \App\Models\ListaItem|null $ProductoEstado
 * @property-read \App\Models\ProductoItem $ProductoItem
 * @property-read \App\Models\ListaItem|null $ProductoTipo
 * @property-read \App\Models\Publicacion $Publicacion
 * @property-read mixed $precio_publicacion
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Producto onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereAnio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereConsumo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereEsNuevo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereFabricacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereIdMarca($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereIdModelo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereIdProductoEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereIdProductoTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereMonedaCompra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereMonedaEuropa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereMonedaMinimo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereMonedaProveedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereMonedaUnitario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePaisFabricacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePeso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioCompra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioEuropa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioFijo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioFijoEur($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioFijoSek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioFijoUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioMinimo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioProveedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioSudamerica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto wherePrecioUnitario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereUnidades($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Producto whereVisitas($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Producto withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Producto withoutTrashed()
 * @mixin \Eloquent
 */
class Producto extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Producto';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idMarca',
		'idProductoTipo',
		'idProductoEstado',
		'idModelo',
		'codigo',
		'nombre',
		'descripcion',
		'unidades',
		'precioCompra',
		'monedaCompra',
		'precioMinimo',
		'monedaMinimo',
		'precioUnitario',
		'monedaUnitario',
		'precioEuropa',
		'monedaEuropa',
		'precioProveedor',
		'monedaProveedor',
		'precioFijo',
		'precioFijoUsd',
		'precioFijoEur',
		'precioFijoSek',
		'precioSudamerica',
		'moneda',
		'anio',
		'esNuevo',
		'visitas',
		'paisFabricacion',
		'consumo',
		'peso',
		'fabricacion',
		'permalink',
		'fuente_import',
	];

	protected $dates = ['deleted_at'];

	protected $casts = [
		'precioFijo' => 'boolean',
		'precioSudamerica' => 'boolean',
	];

	protected $appends = [
		'nombre_alter',
		'precio_publicacion',
		'url_c',
		'url_cs',
		'url_mm',
		'url_csmmid',
	];

	const TIPO_MAQUINARIA = 1;
	const TIPO_ACCESORIO = 2;

	const ESTADO_ACTIVO = 1;
	const ESTADO_INACTIVO = 2;

	// ---------- ATTRIBUTES

	public function getPrecioPublicacionAttribute(){

		$currencyTo = session('current_currency', 'usd');

		if($this->precioFijo == 1) {
			$currencyToStr = ucwords($currencyTo);
			$precio = $this->attributes["precioFijo{$currencyToStr}"];
			return $precio;
		} else {

			$currencyFrom = trim($this->monedaProveedor);
			if( empty($currencyFrom) ) {
				return 0;
			}
			$isProvider = session('isProvider', 'false');

			if( $isProvider == 'true' && $this->precioEuropa != '' && $this->precioEuropa != 0 ) {
				$currencyFrom = trim( $this->monedaEuropa );
			}

			if($currencyFrom == $currencyTo) {
				if( $isProvider == 'true' && $this->precioEuropa != '' && $this->precioEuropa != 0 ) {
					return $this->precioEuropa;
				} else {
					return $this->precioProveedor;
				}

			} else {
				$llave = "tipo_cambio_{$currencyFrom}_{$currencyTo}";
				$exchange = Configuracion::where('llave', $llave)->first()->valor;

				if( $isProvider == 'true' && $this->precioEuropa != '' && $this->precioEuropa != 0 ) {
					$precio_fuente = $this->precioEuropa;
				} else {
					$precio_fuente = $this->precioProveedor;
				}

				return round($precio_fuente * $exchange, 2);
//				logger(number_format(($precio_fuente * $exchange), 2, '.', ''));
				return number_format(($precio_fuente * $exchange), 2);
			}
		}
	}

	public function getNombreAlterAttribute() {
		$nombre = '';
		if( trim($nombre) == '' && $this->idProductoTipo == $this::TIPO_ACCESORIO ) {
			$lang 	= App::getLocale();
			$trans = $this->Traducciones->where('campo', 'nombre')->first();
			if( $trans ) {
				$nombre = $trans->$lang;
			}
			if( trim($nombre) != '')
				return $nombre;
		}
		if( $this->Marca ) {
			$nombre = $this->Marca->nombre;
			$nombre .= ' ';
		}
		if( $this->Modelo ) {
			$nombre .= $this->Modelo->nombre;
		}
		if( trim($this->nombre) != '' ) {
			$nombre = $this->nombre;
		}

		return $nombre;
	}

	public function getNombreAlterMlAttribute() {
		$nombre = '';
		if( trim($nombre) == '' && $this->idProductoTipo == $this::TIPO_ACCESORIO ) {
			$lang 	= App::getLocale();
			$trans = $this->Traducciones->where('campo', 'nombre')->first();
			if( $trans ) {
				$nombre = $trans->$lang;
			}
			if( trim($nombre) != '')
				return $nombre;
		}
		if( $this->Marca ) {
			$nombre = $this->Marca->nombre;
			$nombre .= '<br>';						// ATTRIBUTE JUST FOR MULTILINE NAMING
		}
		if( $this->Modelo ) {
			$nombre .= $this->Modelo->nombre;
			if( trim( $this->Modelo->nombre ) == '' ) {
				$nombre .= '<br>';
			}
		} else {
			$nombre .= '<br>';
		}
		if( trim($this->nombre) != '' ) {
			$nombre = $this->nombre;
		}
		return $nombre;
	}

	/**
	 * Landing page URL: / Categoria
	 * @return string
	 */
	public function getUrlCAttribute() {
		if( count($this->Categorias) == 0 ) {
			return '';
		}
		$lang 	= App::getLocale();
		$cat 	= $this->Categorias->first();
		$catUrl = $cat->Padre->Traduccion->$lang;
		$cat 	= $this->cleanUrl($catUrl);

		return "/{$cat}";
	}

	/**
	 * Landing page URL: / Categoria / Subcategoria
	 * @return string
	 */
	public function getUrlCsAttribute() {
		if( count($this->Categorias) == 0 ) {
			return '';
		}
		$lang 		= App::getLocale();
		$cat 		= $this->Categorias->first();
		$catUrl 	= $cat->Padre->Traduccion->$lang;
		$subcatUrl	= $cat->Traduccion->$lang;

		$cat 		= $this->cleanUrl($catUrl);
		$subcatUrl	= $this->cleanUrl($subcatUrl);

		return "/{$cat}/{$subcatUrl}";
	}

	/**
	 * URL: / Marca / Modelo
	 * @return string
	 */
	public function getUrlMmAttribute() {
//		if( !$this->Marca ) {
//			return '';
//		}
		$marca 		= ( $this->Marca ) ? $this->Marca->nombre : '-';
		$modelo 	= ( $this->Modelo ) ? $this->Modelo->nombre : '-';

		$marca		= ( trim($marca) != '' ) ? $marca : '-' ;
		$modelo		= ( trim($modelo) != '' ) ? $modelo : '-' ;

		$marca 	= $this->cleanUrl($marca);
		$modelo = $this->cleanUrl($modelo);

		return "/{$marca}/{$modelo}";
	}

	/**
	 * Full URL: / Categoria / Subcategoria / Marca / Modelo
	 * @return string
	 */
	public function getUrlCsmmidAttribute() {
		if( count($this->Categorias) == 0 ) {
			return '';
		}
		$lang = App::getLocale();
		$cat = $this->Categorias->first();

		$catUrl 	= $cat->Padre->Traduccion->$lang;
		$subcatUrl 	= $cat->Traduccion->$lang;

		$marca 		= ( $this->Marca ) ? $this->Marca->nombre : '-' ;
		$modelo 	= ( $this->Modelo ) ? $this->Modelo->nombre : '-' ;

		$marca		= ( trim($marca) != '' ) ? $marca : '-' ;
		$modelo		= ( trim($modelo) != '' ) ? $modelo : '-' ;

		$cat 		= $this->cleanUrl($catUrl);
		$subcatUrl 	= $this->cleanUrl($subcatUrl);
		$marca 		= $this->cleanUrl($marca);
		$modelo 	= $this->cleanUrl($modelo);

		if( $this->Publicacion ) {
			$id = $this->Publicacion->id;
			return "/{$cat}/{$subcatUrl}/{$marca}/{$modelo}/{$id}";
		} else {
			return '';
		}

	}

	private function cleanUrl( $string ) {
		$string = strtolower( $string );
//		$string = urlencode( $string );

		// Ref.: https://stackoverflow.com/a/14114419/2367718
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}

	public function getKeywords() {
		//$pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] . ' ' . $pub->Producto->Categorias[0]->Traduccion[$global_localization] . ' ' .
		// $pub->Producto->Marca->nombre . ' ' . $pub->Producto->Modelo->nombre;
		$lang 	= App::getLocale();
		$keywords = '';
		if( $this->Categorias->count() > 0 ) {
			$keywords .= $this->Categorias[0]->Padre->Traduccion[$lang] . ' ';
			$keywords .= $this->Categorias[0]->Traduccion[$lang] . ' ';
		}

		if( $this->idProductoTipo != self::TIPO_ACCESORIO ) {
			if( $this->Marca ) {
				$keywords .= $this->Marca->nombre . ' ';
			}

			if( $this->Modelo ) {
				$keywords .= $this->Modelo->nombre . ' ';
			}
		}

		return $keywords;

	}

    // ---------- RELATIONSHIPS

	public function Calendarios() {
		return $this->morphMany('App\Models\Calendario', 'owner');
	}

	public function Categorias() {
		return $this->belongsToMany('App\Models\Categoria', 'ProductoCategoria', 'idProducto', 'idCategoria');
	}

	public function Cotizaciones() {
		return $this->hasMany('App\Models\Cotizacion', 'idProducto');
	}

	public function Marca() {
//		return $this->belongsTo('App\Models\ListaItem', 'idMarca')->where('idLista', Lista::MARCA);
		return $this->belongsTo('App\Models\Marca', 'idMarca');
	}

	public function Modelo() {
//		return $this->belongsTo('App\Models\ListaItem', 'idModelo')->where('idLista', Lista::MODELO);
		return $this->belongsTo('App\Models\Modelo', 'idModelo');
	}

	public function Owner() {
		return $this->morphTo();
	}

	public function ProductoEstado() {
		return $this->belongsTo('App\Models\ListaItem', 'idProductoEstado');
	}

	public function ProductoItem() {
		return $this->hasOne('App\Models\ProductoItem', 'idProducto');
	}

	public function ProductoItems() {
		return $this->hasMany('App\Models\ProductoItem', 'idProducto');
	}

	public function ProductoTipo() {
		return $this->belongsTo('App\Models\ListaItem', 'idProductoTipo');
	}

	public function Publicacion() {
		return $this->hasOne('App\Models\Publicacion', 'idProducto');
	}

	public function Galerias() {
		return $this->morphMany('App\Models\Galeria', 'owner');
	}

	public function Archivos() {
		return $this->morphMany('App\Models\Archivo', 'owner');
	}

	public function Tags() {
		return $this->belongsToMany('App\Models\Tag', 'ProductoTag', 'idProducto', 'idTag');
	}

	public function Traducciones(){
		//return $this->morphOne('App\Models\Traduccion', 'owner');
		return $this->morphMany('App\Models\Traduccion', 'owner');
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting(function($producto) { // before delete() method call this

			if( $producto->Cotizaciones->count() > 0 ) {
				return false;
			}

			$producto->ProductoItems()->delete();
			$producto->Publicacion()->delete();
			$producto->Galerias()->delete();
			$producto->Archivos()->delete();
			// do the rest of the cleanup...
		});

		static::restoring( function( $producto ) {
			$producto->ProductoItems()->withTrashed()->restore();
			$producto->Publicacion()->withTrashed()->delete();
			$producto->Galerias()->withTrashed()->delete();
			$producto->Archivos()->withTrashed()->delete();
		});
	}

}