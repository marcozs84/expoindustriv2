<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ProductoItem
 *
 * @property int $id
 * @property int|null $idProducto
 * @property int|null $idLote
 * @property int|null $idDisponibilidad
 * @property int|null $idTipoImportacion
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $descripcionPrimaria
 * @property string|null $descripcionDetalle
 * @property string|null $ean
 * @property float|null $precioDistribuidor
 * @property float|null $precioVenta
 * @property string|null $monedaVenta
 * @property float|null $precioPromocion
 * @property string|null $color
 * @property array $dimension
 * @property \Carbon\Carbon|null $fechaElaboracion
 * @property \Carbon\Carbon|null $fechaVencimiento
 * @property array $definicion
 * @property string|null $permalink
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Archivo[] $Adjuntos
 * @property-read \App\Models\Calculadora $Calculadora
 * @property-read \App\Models\ListaItem|null $Disponibilidad
 * @property-read \App\Models\Lote|null $Lote
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrdenDetalle[] $OrdenDetalle
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read \App\Models\Producto|null $Producto
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Traduccion[] $Traducciones
 * @property-read \App\Models\Ubicacion $Ubicacion
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductoItem onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereDefinicion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereDescripcionDetalle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereDescripcionPrimaria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereDimension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereEan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereFechaElaboracion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereFechaVencimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereIdDisponibilidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereIdLote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereIdProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereIdTipoImportacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereMonedaVenta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem wherePermalink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem wherePrecioDistribuidor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem wherePrecioPromocion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem wherePrecioVenta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductoItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductoItem withoutTrashed()
 * @mixin \Eloquent
 */
class ProductoItem extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ProductoItem';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idProducto',
    	'idTipoImportacion',
		'idLote',
		'nombre',
		'descripcionPrimaria',
		'descripcionDetalle',
		'ean',
		'sku',
		'precioDistribuidor',
		'precioVenta',
		'monedaVenta',
		'precioPromocion',
		'color',
		'dimension',
		'fechaElaboracion',
		'fechaVencimiento',
		'definicion',
		'permalink'
	];

    protected $dates = [
    	'fechaElaboracion',
    	'fechaVencimiento',
    	'deleted_at'
	];

    protected $casts = [
    	'definicion' => 'array',
		'dimension' => 'array'
	];


	const DISPONIBILIDAD_DISPONIBLE = 1;
	const DISPONIBILIDAD_RESERVADO = 2;
	const DISPONIBILIDAD_VENDIDO = 3;
	const DISPONIBILIDAD_OTRO = 4;

    // ---------- RELATIONSHIPS

	public function Adjuntos() {
		return $this->morphMany('App\Models\Archivo', 'owner');
	}

	public function Disponibilidad() {
		return $this->belongsTo('App\Models\ListaItem', 'idDisponibilidad', 'valor')->where('idLista', Lista::PRODUCTOITEM_DISPONIBILIDAD);
	}

	public function Producto() {
		return $this->belongsTo('App\Models\Producto', 'idProducto');
	}

	public function Lote() {
		return $this->belongsTo('App\Models\Lote', 'idLote');
	}

	public function OrdenDetalle() {
		return $this->hasMany('App\Models\OrdenDetalle', 'idProductoItem');
	}

	public function Ubicacion(){
		return $this->morphOne('App\Models\Ubicacion', 'owner');
	}

	public function Calculadora() {
		return $this->morphOne('App\Models\Calculadora', 'owner');
	}

	public function Traducciones(){
		//return $this->morphOne('App\Models\Traduccion', 'owner');
		return $this->morphMany('App\Models\Traduccion', 'owner');
	}

	public function Owner() {
		return $this->morphTo();
	}

	public function Precios() {
		return $this->morphMany('App\Models\Precio', 'owner');
	}

	public function getPrecioLiteralAttribute() {
		$currencyTo = session('current_currency', 'usd');
		$precio = $this->Precios->where('moneda', $currencyTo)->first();
		if( $precio ) {
			return $precio->precio . ' ' . $precio->moneda;
		} else {
			return '';
		}
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting( function ( $productoItem ) { // before delete() method call this

			if( $productoItem->OrdenDetalle->count() > 0 ) {
				return false;
			}
			$productoItem->Adjuntos()->delete();
			$productoItem->Calculadora()->delete();
			$productoItem->OrdenDetalle()->delete();
			$productoItem->Precios()->delete();
			$productoItem->Traducciones()->delete();
			$productoItem->Ubicacion()->delete();

			// do the rest of the cleanup...
		} );

		static::restoring( function( $productoItem ) {
			$productoItem->Adjuntos()->withTrashed()->restore();
			$productoItem->Calculadora()->withTrashed()->delete();
			$productoItem->OrdenDetalle()->withTrashed()->delete();
			$productoItem->Precios()->withTrashed()->restore();
			$productoItem->Traducciones()->withTrashed()->delete();
			$productoItem->Ubicacion()->withTrashed()->delete();
		});
	}

}