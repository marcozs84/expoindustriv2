<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PublicidadProducto
 *
 * @property int $id
 * @property int|null $idPublicidad
 * @property int|null $idProducto
 * @property-read \App\Models\Producto|null $Producto
 * @property-read \App\Models\Publicidad|null $Publicidad
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadProducto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadProducto whereIdProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PublicidadProducto whereIdPublicidad($value)
 * @mixin \Eloquent
 */
class PublicidadProducto extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'PublicidadProducto';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idPublicidad',
		'idProducto'
	];

    // ---------- RELATIONSHIPS

	public function Publicidad() {
		return $this->belongsTo('App\Models\Publicidad', 'idPublicidad');
	}

	public function Producto() {
		return $this->belongsTo('App\Models\Producto', 'idProducto');
	}

}