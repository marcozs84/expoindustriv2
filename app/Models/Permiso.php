<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Permiso
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $Roles
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permiso onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permiso whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permiso whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permiso whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permiso whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permiso whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permiso whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permiso whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permiso withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Permiso withoutTrashed()
 * @mixin \Eloquent
 */
class Permiso extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Permiso';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nombre',
		'descripcion',
		'estado'
	];

    protected $dates = ['deleted_at'];

    // ---------- ATTRIBUTES

	public function setDescripcionAttribute($value = '') {
		$this->attributes['descripcion'] = trim($value);
	}

    // ---------- RELATIONSHIPS

	public function Roles() {
		return $this->belongsToMany('App\Models\Role', 'RolePermiso', 'idPermiso', 'idRole');
	}

	// ---------- CUSTOM

	static  function getTree(){
		$ar = [];

		$permisos = self::all();

		foreach($permisos as $permiso){
			$nombre = $permiso->nombre;
			$parts = explode('.', $nombre);

			switch(count($parts)){
				case 0 :
					$ar[] = $permiso->nombre;
					break;
				case 1 :
					$ar[] = $parts[0];
					break;
				case 2 :
					$ar[$parts[0]][] = [
						'id' => $permiso->id,
						'nombre' => $parts[1],
						'descripcion' => ( $permiso->descripcion ? $permiso->descripcion : '' )
					];
					break;
				case 3 :
					$ar[$parts[0]][$parts[1]][] = [
						'id' => $permiso->id,
						'nombre' => $parts[2],
						'descripcion' => ( $permiso->descripcion ? $permiso->descripcion : '' )
					];
					break;
				case 4 :
					$ar[$parts[0]][$parts[1]][$parts[2]][] = [
						'id' => $permiso->id,
						'nombre' => $parts[3],
						'descripcion' => ( $permiso->descripcion ? $permiso->descripcion : '' )
					];
					break;
				case 5 :
					$ar[$parts[0]][$parts[1]][$parts[2]][$parts[3]][] = [
						'id' => $permiso->id,
						'nombre' => $parts[4],
						'descripcion' => ( $permiso->descripcion ? $permiso->descripcion : '' )
					];
					break;
				case 6 :
					$ar[$parts[0]][$parts[1]][$parts[2]][$parts[3]][$parts[4]][] = [
						'id' => $permiso->id,
						'nombre' => $parts[5],
						'descripcion' => ( $permiso->descripcion ? $permiso->descripcion : '' )
					];
					break;
			}
		}
		return $ar;
	}

}