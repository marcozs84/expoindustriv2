<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;

/**
 * App\Models\Imagen
 *
 * @property int $id
 * @property int|null $idGaleria
 * @property string|null $titulo
 * @property string|null $ruta
 * @property string|null $filename
 * @property string|null $mimetype
 * @property int|null $estado
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Galeria|null $Galeria
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imagen onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereIdGaleria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereMimetype($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereRuta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereTitulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Imagen whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imagen withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Imagen withoutTrashed()
 * @mixin \Eloquent
 */
class Imagen extends Model  {

	use SoftDeletes;

	const ESTADO_ACTIVO = 1;
	const ESTADO_PENDIENTE = 2;
	const ESTADO_FALLIDO = 3;
	const ESTADO_FALLIDO_RESIZE = 4;

	const DIMENSION_MAX_WIDTH = 1024;
	const DIMENSION_MAX_HEIGHT = 1024;
	const DIMENSION_MAX_WIDTH_THUMB = 305;
	const DIMENSION_MAX_HEIGHT_THUMB = 150;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Imagen';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idGaleria',
		'titulo',
		'ruta',
		'filename',
		'mimetype',
		'estado',
		'fuente',
	];

    protected $dates = ['deleted_at'];

    protected $appends = [
		'ruta_publica_banner',
		'ruta_publica_producto',
		'ruta_publica_producto_thumb',
	];

    // ---------- ATTRIBUTES

	public function getRutaPublicaBannerAttribute() {
		return "/public_assets/banner/{$this->filename}";
	}

	public function getRutaPublicaProductoAttribute() {
		$filename_res = str_replace('.', '_res.', $this->filename);
		$ruta_res = str_replace($this->filename, $filename_res, $this->ruta);
		if( File::exists( storage_path( $ruta_res ) ) ) {
			return str_replace('app/published', '/pubimgs', $ruta_res);
		} else {
			return str_replace('app/published', '/pubimgs', $this->ruta);
		}
	}

	public function getRutaPublicaProductoThumbAttribute() {
		$filename_res = str_replace('.', '_thumb.', $this->filename);
		$ruta_res = str_replace($this->filename, $filename_res, $this->ruta);
		if( File::exists( storage_path( $ruta_res ) ) ) {
			return str_replace('app/published', '/pubimgs', $ruta_res);
		} else {
			return str_replace('app/published', '/pubimgs', $this->ruta);
		}
	}

	public function getFilenameResizedAttribute() {
		$filename_res = str_replace('.', '_res.', $this->filename);
		return $filename_res;
	}

	public function getFilenameThumbAttribute() {
		return str_replace('.', '_thumb.', $this->filename);
	}

    // ---------- RELATIONSHIPS

	public function Galeria() {
		return $this->belongsTo('App\Models\Galeria', 'idGaleria');
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

		static::deleting(function($imagen) { // before delete() method call this
			if($imagen->isForceDeleting()) {
				File::delete(storage_path($imagen->ruta));
			}
			// do the rest of the cleanup...
		});


	}

	/**
	 * Ref.: https://stackoverflow.com/a/60630053/2367718
	 * @param $w
	 * @param $h
	 * @param bool $crop
	 * @param string $newname
	 * @param string $message
	 * @return bool|mixed
	 */
	public function resize( $w, $h, $crop = false, $newname = '', &$message = '' ) {

		$file = storage_path( $this->ruta );

		try {
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$ext = strtolower($ext);
			list($width, $height) = getimagesize( $file );
			// if the image is smaller we dont resize

			if ($w > $width && $h > $height) {
				$message = 'La imagen es menor a '.$w.', no se redimensionará.';
				return false;
			}
			$r = $width / $height;
			if ($crop) {
				if ($width > $height) {
					$width = ceil($width - ($width * abs($r - $w / $h)));
				} else {
					$height = ceil($height - ($height * abs($r - $w / $h)));
				}
				$newwidth = $w;
				$newheight = $h;
			} else {
				if ($w / $h > $r) {
					$newwidth = $h * $r;
					$newheight = $h;
				} else {
					$newheight = $w / $r;
					$newwidth = $w;
				}
			}
			$dst = imagecreatetruecolor($newwidth, $newheight);

			switch ($ext) {
				case 'jpg':
				case 'jpeg':
					$src = imagecreatefromjpeg($file);
					break;
				case 'png':
					$src = imagecreatefrompng($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
				case 'gif':
					$src = imagecreatefromgif($file);
					imagecolortransparent($dst, imagecolorallocatealpha($dst, 0, 0, 0, 127));
					imagealphablending($dst, false);
					imagesavealpha($dst, true);
					break;
//				case 'bmp':
//					$src = imagecreatefrombmp($file);
					break;
				default:
					$message = 'Unsupported image extension found: ' . $ext;
					return false;
			}
			$result = imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			if( $newname != '' ) {
				$filename = pathinfo( $file, PATHINFO_BASENAME);
				$file = str_replace($filename, $newname, $file);
				$this->filename = $newname;
				$this->ruta = $file;
				$this->save();
			}

			switch ($ext) {
				case 'bmp':
					imagewbmp($dst, $file);
					break;
				case 'gif':
					imagegif($dst, $file);
					break;
				case 'jpg':
				case 'jpeg':
					imagejpeg($dst, $file);
					break;
				case 'png':
					imagepng($dst, $file);
					break;
			}

			imagedestroy($dst);
			imagedestroy($src);

			return $file;

		} catch (Exception $err) {
			// LOG THE ERROR HERE
			$message = $err->getMessage();
			return false;
		}
	}

}