<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Calendario
 *
 * @property int $id
 * @property int|null $idCalendarioTipo
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property array $extrainfo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereExtrainfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereIdCalendarioTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendario whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Calendario extends Model {

	use SoftDeletes;

	protected $table = 'Calendario';

	protected $fillable = [
		'idCalendarioTipo',
		'extrainfo',
	];

	protected $dates = ['deleted_at'];
	protected $casts = [
		'extrainfo' => 'array'
	];

	const TIPO_HISTORIA = 1;

	// ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	public function Actividades() {
		return $this->hasMany('App\Models\Actividad', 'idCalendario');
	}
}
