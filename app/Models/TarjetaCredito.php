<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TarjetaCredito
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $tipo
 * @property string|null $descripcion
 * @property string|null $numero
 * @property string|null $codigo
 * @property string|null $vencimientoMes
 * @property string|null $vencimientoAnio
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TarjetaCredito onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereVencimientoAnio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TarjetaCredito whereVencimientoMes($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TarjetaCredito withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TarjetaCredito withoutTrashed()
 * @mixin \Eloquent
 */
class TarjetaCredito extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'TarjetaCredito';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'tipo',
		'descripcion',
		'numero',
		'codigo',
		'vencimientoMes',
		'vencimientoAnio'
	];

    protected $dates = ['deleted_at'];

    // ---------- RELATIONSHIPS

	public function Owner(){
		return $this->morphTo();
	}

}