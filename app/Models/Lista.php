<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Lista
 *
 * @property int $id
 * @property int|null $idPadre
 * @property string|null $nombre
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ListaItem[] $Items
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ListaItem[] $ListaItems
 * @property-read \App\Models\Lista|null $Padre
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lista whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lista whereIdPadre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lista whereNombre($value)
 * @mixin \Eloquent
 */
class Lista extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Lista';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idPadre',
		'nombre'
	];

    public $timestamps = false;

    // ---------- CONSTANTS

	const SEXO = 1;
	const CLIENTE_TIPO = 2;
	const CLIENTE_ESTADO = 3;
	const EMPLEADO_TIPO = 4;
	const EMPLEADO_ESTADO = 5;
	const ESTADO_CIVIL = 6;
	const GALERIA_TIPO = 7;
	const GALERIA_ESTADO = 8;
	const HORARIO = 9;
	const DIMENSION = 10;
	const RECURSO = 11;
	const MARCA = 12;
	const MODELO = 13;
	const ORDEN_TIPO = 14;
	const ORDEN_ESTADO = 15;
	const PRODUCTO_TIPO = 16;
	const PRODUCTO_ESTADO = 17;
	const PROVEEDOR_TIPO = 18;
	const PROVEEDOR_ESTADO = 19;
	const PROVEEDOR_CONTACTO_TIPO = 20;
	const PROVEEDOR_CONTACTO_ESTADO = 21;
	const PUBLICIDAD_ESTADO = 22;
	const PUBLICIDAD_ESQUEMA_TIPO = 23;
	const USUARIO_TIPO = 24;
	const USUARIO_ESTADO = 25;
	const MONEDA = 26;
	const PUBLICACION_ESTADO = 27;
	const CONSULTA_EXTERNA_ESTADO = 28;
	const BANNER_TIPO = 29;
	const TEMPLATE_TIPO = 30;
	const MODULO = 31;
	const PAGO_TIPO = 32;
	const PRODUCTOITEM_DISPONIBILIDAD = 33;
	const SUBSCRIPCION_TIPO = 34;
	const MONEDA_USO_INTERNO = 35;
	const GASTO_TIPO = 36;
	const TRANSACCION_TIPO = 37;
	const TRANSACCION_ESTADO = 38;
	const UBICACION_TIPO = 39;
	const CONSULTA_EXTERNA_TIPO = 40;


	// ---------- RELATIONSHIPS

	public function ListaItems() {
		return $this->hasMany('App\Models\ListaItem', 'idLista');
	}

	public function Items() {
		return $this->hasMany('App\Models\ListaItem', 'idLista');
	}

	public function Padre() {
		return $this->belongsTo('App\Models\Lista', 'idPadre');
	}

}