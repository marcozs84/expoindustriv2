<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Role
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $estado
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permiso[] $Permisos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RolePermiso[] $RolePermisos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Usuario[] $Usuarios
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Role whereNombre($value)
 * @mixin \Eloquent
 */
class Role extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Role';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nombre',
		'descripcion',
		'estado',
	];

    public $timestamps = false;

    CONST VENDEDOR = 1;

    // ---------- RELATIONSHIPS

	public function Permisos() {
		return $this->belongsToMany('App\Models\Permiso', 'RolePermiso', 'idRole', 'idPermiso');
	}

	public function Usuarios() {
		return $this->belongsToMany('App\Models\Usuario', 'UsuarioRole', 'idRole', 'idUsuario');
	}

	public function RolePermisos(){
		return $this->hasMany('App\Models\RolePermiso', 'idRole');
	}

}