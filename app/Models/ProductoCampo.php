<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductoCampo
 *
 * @property int $id
 * @property int $idCategoria
 * @property string|null $campo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Categoria $Categoria
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCampo whereCampo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCampo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCampo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCampo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCampo whereIdCategoria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductoCampo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductoCampo extends Model {

	protected $table = 'ProductoCampo';

	public function Categoria() {
		return $this->belongsTo('App\Models\Categoria', 'idCategoria');
	}
	
}
