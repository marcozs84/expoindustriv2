<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Ubicacion
 *
 * @property int $id
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property string|null $latitud
 * @property string|null $longitud
 * @property string|null $email
 * @property string|null $pais
 * @property string|null $ciudad
 * @property string|null $lugar
 * @property string|null $cpostal
 * @property string|null $direccion
 * @property string|null $descripcion
 * @property int|null $idUbicacionTipo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read \App\Models\ListaItem $UbicacionTipo
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereCiudad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereCpostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereIdUbicacionTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereLatitud($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereLongitud($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereLugar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion wherePais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Ubicacion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ubicacion withoutTrashed()
 * @mixin \Eloquent
 */
class Ubicacion extends Model  {

	use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Ubicacion';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'latitud',
		'longitud',
		'email',
		'pais',
		'ciudad',
		'lugar',
		'cpostal',
		'direccion',
		'descripcion',
		'direccion',
		'idUbicacionTipo'
	];

	const TIPO_PRIMARIA = 1;
	const TIPO_SECUNDARIA = 2;
	const TIPO_FACTURACION = 3;

	protected $dates = ['deleted_at'];

	// ---------- RELATIONSHIPS

	public function UbicacionTipo() {
		return $this->hasOne('App\Models\ListaItem', 'valor', 'idUbicacionTipo')->where('idLista', Lista::UBICACION_TIPO);
	}

	public function Owner() {
		return $this->morphTo();
	}

}