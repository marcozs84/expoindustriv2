<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Banner
 *
 * @property int $id
 * @property int|null $idBannerTipo
 * @property int|null $idProducto
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property string|null $ubicacion
 * @property \Carbon\Carbon|null $fechaInicio
 * @property \Carbon\Carbon|null $fechaFin
 * @property string|null $texto
 * @property float|null $precio
 * @property string|null $enlace
 * @property int|null $orden
 * @property string|null $onclick
 * @property string|null $visibilidad
 * @property int|null $estado
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Galeria $Galeria
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereEnlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereFechaFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereFechaInicio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereIdBannerTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereIdProducto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereOnclick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereOrden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereTexto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereTitulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereUbicacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Banner whereVisibilidad($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Banner withoutTrashed()
 * @mixin \Eloquent
 */
class Banner extends Model {

	use SoftDeletes;

	protected $table = 'Banner';

	const ESTADO_ACTIVO = 1;
	const ESTADO_INACTIVO = 0;

	const TIPO_IMAGEN = 1;
	const TIPO_PRODUCTO = 2;
	const TIPO_FONDO = 3;

	protected $fillable = [
		'idBannerTipo',
		'titulo',
		'descripcion',
		'ubicacion',
		'fechaInicio',
		'fechaFin',
		'texto',
		'precio',
		'enlace',
		'estado',
		'orden',
		'onclick',
		'visibilidad',
	];

	protected $dates = [
		'fechaInicio',
		'fechaFin',
		'deleted_at'
	];

	protected $casts = [
		'visibilidad' => 'array'
	];

	// ---------- RELATIONSHIPS

	public function Galeria() {
		return $this->morphOne('App\Models\Galeria', 'owner');
	}

	// ---------- EVENTS

	protected static function boot() {
		parent::boot();

//		static::deleting(function($banner) { // before delete() method call this
//			if($banner->Galeria){
//				$banner->Galeria->delete();
//			}
//			// do the rest of the cleanup...
//		});

		static::restoring(function($banner) {
			$ban_orden = Banner::where('idBannerTipo', $banner->idBannerTipo)->orderBy('orden', 'desc')->first();
			if($ban_orden){
				$banner->orden = $ban_orden->orden + 1;
			} else {
				$banner->orden = 0;
			}

//			if($banner->Galeria()->withTrashed()) {
//				$banner->Galeria()->withTrashed()->restore();
//			}
		});
	}

}
