<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Publicidad
 *
 * @property int $id
 * @property int|null $idPublicidadEsquema
 * @property string|null $owner_type
 * @property int|null $owner_id
 * @property \Carbon\Carbon|null $fechaInicio
 * @property string|null $fechafin
 * @property int|null $idPublicidadEstado
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @property-read \App\Models\PublicidadEsquema|null $PublicidadEsquema
 * @property-read \App\Models\ListaItem|null $PublicidadEstado
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Publicidad onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereFechaInicio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereFechafin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereIdPublicidadEsquema($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereIdPublicidadEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publicidad whereOwnerType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Publicidad withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Publicidad withoutTrashed()
 * @mixin \Eloquent
 */
class Publicidad extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Publicidad';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idPublicidadEsquema',
		'fechaInicio',
		'fechaFin',
		'idPublicidadEstado'
	];

    protected $dates = [
    	'fechaInicio',
		'fechaFin',
		'deleted_at'
	];

    // ---------- RELATIONSHIPS

	public function PublicidadEsquema() {
		return $this->belongsTo('App\Models\PublicidadEsquema', 'idPublicidadEsquema');
	}

	public function Owner() {
		return $this->morphTo();
	}

	public function PublicidadEstado() {
		return $this->belongsTo('App\Models\ListaItem', 'idPublicidadEstado');
	}

}