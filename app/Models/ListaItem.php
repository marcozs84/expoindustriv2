<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ListaItem
 *
 * @property int $id
 * @property int|null $idLista
 * @property string|null $texto
 * @property string|null $valor
 * @property int|null $orden
 * @property-read \App\Models\Lista|null $Lista
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ListaItem listado($idLista)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ListaItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ListaItem whereIdLista($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ListaItem whereOrden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ListaItem whereTexto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ListaItem whereValor($value)
 * @mixin \Eloquent
 */
class ListaItem extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ListaItem';
    public $timestamps = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idLista',
		'texto',
		'valor',
		'orden'
	];

    // ---------- SCOPES

	public function scopeListado($q, $idLista){
		return $q->where('idLista', $idLista);
	}

    // ---------- RELATIONSHIPS

	public function Lista() {
		return $this->belongsTo('App\Models\Lista', 'idLista');
	}

	public function Traduccion() {
		return $this->morphOne('App\Models\Traduccion', 'owner');
	}

}