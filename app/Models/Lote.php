<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Lote
 *
 * @property int $id
 * @property int|null $idProveedor
 * @property \Carbon\Carbon|null $fechaIngreso
 * @property float|null $precioProveedor
 * @property float|null $precioVenta
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Producto[] $Producto
 * @property-read \App\Models\Proveedor|null $Proveedor
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lote onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote whereFechaIngreso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote whereIdProveedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote wherePrecioProveedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lote wherePrecioVenta($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lote withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lote withoutTrashed()
 * @mixin \Eloquent
 */
class Lote extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Lote';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idProveedor',
		'fechaIngreso',
		'precioProveedor',
		'precioVenta'
	];

    protected $dates = [
    	'fechaIngreso',
    	'deleted_at'
	];

    // ---------- RELATIONSHIPS

	public function Producto() {
		return $this->hasMany('App\Models\Producto', 'idLote');
	}

	public function Proveedor() {
		return $this->belongsTo('App\Models\Proveedor', 'idProveedor');
	}

}