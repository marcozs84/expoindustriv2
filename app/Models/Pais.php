<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Pais
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $codigo
 * @property string|null $bandera
 * @property int|null $esDestino
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ciudad[] $Ciudades
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Destino[] $Destinos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereBandera($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereEsDestino($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pais whereNombre($value)
 * @mixin \Eloquent
 */
class Pais extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Pais';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nombre',
		'codigo',
		'bandera'
	];

	public $timestamps = false;

	// ---------- RELATIONSHIPS

	public function Ciudades() {
		return $this->hasMany('App\Models\Ciudad', 'idPais');
	}

	public function Destinos() {
		return $this->hasMany('App\Models\Destino', 'idPais');
	}

}