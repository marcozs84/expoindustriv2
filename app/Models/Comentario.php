<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Comentario
 *
 * @property int $id
 * @property string $owner_type
 * @property int|null $owner_id
 * @property string|null $asunto
 * @property string|null $mensaje
 * @property \Carbon\Carbon|null $created_at
 * @property string|null $edited_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $Owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comentario onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereAsunto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereEditedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereMensaje($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Comentario whereOwnerType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comentario withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Comentario withoutTrashed()
 * @mixin \Eloquent
 */
class Comentario extends Model  {

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Comentario';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'idUsuario',
    	'idPadre',
		'asunto',
		'mensaje',
	];

    protected $dates = ['deleted_at'];

	public $appends = [
		'created_at_literal'
	];

	// ---------- ATTRIBUTES

	public function getCreatedAtLiteralAttribute() {
		$created_at = $this->created_at;
		$meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		return $created_at->format('d').' '.$meses[ $created_at->format('n') ].' '.$created_at->format('Y');
	}

    // ---------- RELATIONSHIPS

	public function Owner() {
		return $this->morphTo();
	}

	public function Padre() {
		return $this->belongsTo('App\Mdels\Comentario', 'idPadre');
	}

	public function Usuario() {
		return $this->belongsTo('App\Models\Usuario', 'idUsuario');
	}

}