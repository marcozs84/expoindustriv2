<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Marca
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Marca whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Marca extends Model {

    protected $table = 'Marca';

    protected $fillable = [
    	'nombre',
		'descripcion'
	];

    // ---------- RELATIONSHIPS

	public function Modelos() {
		return $this->hasMany('App\Models\Modelo', 'idMarca');
	}

}
