<?php

namespace App\Exceptions;

use App\Models\Excepcion;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use GeoIp2\Database\Reader;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

		$url = '';
		$route = '';
		$action = '';
		$method = '';

		$errorCode = $exception->getCode();
		$archivo = $exception->getFile();
		$linea = $exception->getLine();
		$stack = $exception->getTraceAsString();
		$stack = preg_replace('/#/', '<br>#', $stack);
		//$stack2 = $exception->getTrace();
		//Log::info(print_r($stack2, true));
		//$stack = implode('<br>', $stack2);
		//$route = $request->route()->getName();
		//$action = $request->route()->getActionName();
		$params = '';
		if($request){
			$method = $request->method();
			if(strtoupper($method) == 'POST'){
//				$params = print_r($request->all(), true);
				$params = json_encode($request->all());
			}
		}

		// ---------- COUNTRY VISITOR
		$reader = new Reader(storage_path('app/GeoLite2-Country.mmdb'));

		// Replace "city" with the appropriate method for your database, e.g.,
		// "country

		$ip = 'Localhost';
		if(env('APP_ENV') == 'local'){
			$record = $reader->country('128.101.101.101');
		} else {
			$ip = request()->ip();
			if($ip == '127.0.0.1'){
				$ip = '128.101.101.101';
			}
			$record = $reader->country($ip);
		}

		$paisCode = $record->country->isoCode;
		$paisVisitor = $record->country->name;

//		logger($record->country->isoCode); // 'US'
//		logger($record->country->name); // 'United States'
		// ----------


		$url = $request->path();
		$fullUrl = $request->url();
		$fullUrl2 = $request->fullUrl();
		$mensajeError = $exception->getMessage();
		$mensaje = <<<xxx
Error {$errorCode} en https://www.expoindustri.com/{$url}

<table cellpadding="2" cellspacing="2">
<tr>
	<td style="width:100px; text-align:right; color:#ffffff !important;"><span>Referencia:</span></td>
	<td class="color:#ffffff !important;"><a href="__referencia__">__referencia__</a></td>
</tr>
<tr>
	<td style="width:100px; text-align:right; color:#ffffff !important;"><span>Action:</span></td>
	<td class="color:#ffffff !important;">{$action}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;"><p>Route:</p></td>
	<td class="color:#ffffff !important;">{$route}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">Pais:</td>
	<td class="color:#ffffff !important;">{$paisVisitor} ({$paisCode})</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">IP:</td>
	<td class="color:#ffffff !important;">{$ip}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">Method</td>
	<td class="color:#ffffff !important;">{$method}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">FullURL1:</td>
	<td class="color:#ffffff !important;">{$fullUrl}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">FullURL2:</td>
	<td class="color:#ffffff !important;">{$fullUrl2}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">Archivo:</td>
	<td class="color:#ffffff !important;">{$archivo}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">Linea:</td>
	<td class="color:#ffffff !important;">{$linea}</td>
</tr>
<tr>
	<td style="text-align:right; color:#ffffff !important;">Params:</td>
	<td class="color:#ffffff !important;">{$params}</td>
</tr>
<tr>
	<td colspan="2" style="color:#ffffff !important;">
	Error Message: <br>
	{$mensajeError}
	</td>
</tr>

</table>

xxx;

		/**
		<tr>
		<td colspan="2" style="color:#ffffff !important;">
		Stack Trace: <br>
		{$stack}
		</td>
		</tr>
		 */

		// ---------- Create Excepcion

		$e = new Excepcion();
		$e->ip = $ip;
		$e->pais = $paisCode;
//		$e->pais = $paisVisitor;
//		logger(get_class($exception));
//		if(get_class($exception) != 'AuthenticationException') {


		try {
			if(
				!$exception instanceof AuthenticationException &&
				!$exception instanceof TokenMismatchException &&
				!$exception instanceof ModelNotFoundException &&
				!$exception instanceof ErrorException
			) {
				//$e->codigo = $exception->getCode().' - '.$exception->getStatusCode();
			}
		} catch (Exception $e) {
			$e->codigo = 0;
		} finally {
			$e->codigo = 0;
		}

		if( strlen($fullUrl2) > 300 ) {
			$mensajeError = $fullUrl2.'<br><br><br>'.$mensajeError;
			$fullUrl2 = substr($fullUrl2, 0, 300);
		}

		$e->mensaje = $mensajeError;
		$e->url = $fullUrl2;
		$e->route = $route;
		$e->archivo = $archivo;
		$e->linea = $linea;
		$e->params = $params;

		// ----------

		$mail = [];
		$mail['to'] = 'marcozs84@gmail.com';
		$mail['from'] = 'info@expoindustri.com';
		$mail['subject'] = "Error en el sistema";
		$mail['mensaje'] = $mensaje;
		$cabeceras = 'From: info@expoindustri.com' . "\r\n" .
			'Reply-To: info@expoindustri.com' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();

		if($exception instanceof NotFoundHttpException){
//			logger("NOT FOUND");
//			logger($fullUrl2);
			$e->tipo = 'NotFoundHttpException';
			$e->stackTrace = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'Desconocido');
			$e->save();
		} else {

			$e->tipo = get_class($exception);
			$e->stackTrace = $stack;
			$e->save();

			$mensaje = str_replace('__referencia__', 'http://admin.expoindustri.com/excepcion/'.$e->id, $mensaje, $count);

			/**
			 * Messages sent to mail are suspended from now on.
			 * There are too many messages sent do my account and it is getting suspicious.
			 */
//			if(env('APP_ENV') == 'local'){
//				Mail::send('frontend.email.template_light', [
//					'content' => $mensaje,
//					'titulo' => $mail['subject']
//				], function ($m) use($mail) {
//					$m->from($mail['from'], 'ExpoIndustri');
//					$m->sender($mail['from'], 'Dev');
//					$m->replyTo($mail['from'], 'Dev');
//					$m->to($mail['to']);
////				$m->bcc('marcozs84@gmail.com', 'Marco Zeballos');
//					$m->subject($mail['subject']);
//				});
//			} else {
//				$headers = "From: " . strip_tags('info@expoindustri.com') . "\r\n";
//				$headers .= "Reply-To: ". strip_tags('info@expoindustri.com') . "\r\n";
////			$headers .= "BCC: marcozs84@gmail.com\r\n";
////			$headers .= "BCC: joakimbyren@hotmail.com\r\n";
//				$headers .= "MIME-Version: 1.0\r\n";
//				$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//				$sent = mail($mail['to'], $mail['subject'], view('frontend.email.template_dark', [
//					'content' => $mensaje,
//					'titulo' => $mail['subject']
//				])->render(), $headers);
//			}
		}


//		if(env('DB_CONNECTION') == '_PROD'){

//		}

		if (!config('app.debug')) {

			if($exception instanceof TokenMismatchException){
				$response = [
					'result' => true,
					'message' => 'TOKEN MISMATCH EXCEPTION'
				];
				return response()->make($response, 409);
			}

			if($exception instanceof ModelNotFoundException){
				return view('errors.generic', [
					'estado' => 404,
					'mensajeCorto' => 'Recurso no encontrado',
					'mensaje' => "No hay resultados para el modelo [ {$exception->getModel()} ].<br>El error fué reportado al departamento de tecnología."
				]);
				// ó
				//return response()->view('errors.generic', [
				//	'estado' => 404,
				//	'mensajeCorto' => 'Recurso no encontrado',
				//	'mensaje' => "No hay resultados para el modelo [ {$exception->getModel()} ].<br>El error fué reportado al departamento de tecnología."
				//], 404);
			}
			if($exception instanceof QueryException){

				$mensaje = "El error fué reportado al departamento de tecnología.";
				if(preg_match('/Integrity constraint violation/', $exception->getMessage()) == 1){
					$mensaje = "No se puede eliminar el registro, está siendo utilizado por otra entidad.";
					$response = [
						'General' => [
							$mensaje
						]
					];
					return response()->make($response, 422);
				}

				return view('errors.generic', [
					'estado' => 404,
					'mensajeCorto' => 'QUERY EXCEPTION (temp - review)',
					'mensaje' => $mensaje // $exception->getMessage() <- obtiene error sql
				]);
			}
		}

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {

		$guard = array_get($exception->guards(), 0);
		switch ($guard) {
			case 'clientes':
				$login = 'public.login';
				break;
			case 'proveedores':
				$login = 'provider.login';
				break;
			case 'empleados':
				$login = 'admin.login';
				break;
			default:
				$login = 'public.login';
				break;
		}


        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route($login));
    }
}
