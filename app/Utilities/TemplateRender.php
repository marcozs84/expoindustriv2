<?php

namespace App\Utilities;

//use App\Models\Actividad;
use App\Models\Template;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//use JpGraph\JpGraph;
//use Graph;

class TemplateRender {

	var $diaSemana = [
		0 => 'Domingo',
		1 => 'Lunes',
		2 => 'Martes',
		3 => 'Miercoles',
		4 => 'Jueves',
		5 => 'Viernes',
		6 => 'Sabado',
	];

	var $meses = [
		1 => 'Enero',
		2 => 'Febrero',
		3 => 'Marzo',
		4 => 'Abril',
		5 => 'Mayo',
		6 => 'Junio',
		7 => 'Julio',
		8 => 'Agosto',
		9 => 'Septiembre',
		10 => 'Octubre',
		11 => 'Noviembre',
		12 => 'Diciembre'
	];

	var $dia_semana_numeral = 0;
	var $dia_semana_literal = '';
	var $dia_mes_numeral = 0;
	var $mes_numeral = 0;
	var $mes_literal = '';

	var $anio_yy = 0;
	var $anio_yyyy = 0;

	public function __construct(){

		$this->dia_semana_numeral = Carbon::today()->dayOfWeek;
		$this->dia_semana_literal = $this->diaSemana[Carbon::today()->dayOfWeek];

		$this->dia_mes_numeral = Carbon::today()->day;

		$this->mes_numeral = Carbon::today()->month;
		$this->mes_literal = $this->meses[Carbon::today()->month];

		$this->anio_yy = Carbon::today()->format('y');
		$this->anio_yyyy = Carbon::today()->year;

	}

}