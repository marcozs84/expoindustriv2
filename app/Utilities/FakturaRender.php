<?php

namespace App\Utilities;

use App\Models\Template;

class FakturaRender extends TemplateRender {

	public function __construct() {
		parent::__construct();
	}

	public function render($idTemplate) {

		$template = Template::findOrFail($idTemplate);
		$html = $template->mensaje;

		//$html = preg_replace('/{{ alumno->mencion }}/', $mencion, $html);

		return $html;

	}

}