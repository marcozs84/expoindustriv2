/**
 * Created by MZ on 3/03/20.
 */

var Search = {

	resource: function ( selector, options ) {

		var searchElement = $( selector );
		searchElement.select2( {
			allowClear: true,
			minimumInputLength: 0,  // 1,
			placeholder: {
				id: "",
				placeholder: "Buscar"
			},
			ajax: {
				url: options.url,
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token': $( 'meta[name="csrf-token"]' ).attr( 'content' )
				},
				delay: 250,
				data: function ( params ) {

					data = {
						q: params.term, // search term
						page: params.page
					};

					if( options.data !== undefined ) {
						for (const property in options.data) {
							data[property] = options.data[property];
						}
					}

					return data;
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function ( data, params ) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},

			escapeMarkup: function ( markup ) {
				return markup;
			}, // let our custom formatter work

			templateResult: function ( data ) {
				if ( data.loading ) return data.text;
				if ( data.text ) return data.text;

				if ( options ) {
					return options.templateResult( data );
				}

				if ( data.nombre ) return data.nombre;
				if ( data.nombres ) return data.nombres;

				return '';

			},

			templateSelection: function formatRepoSelection( data ) {
				if ( data.placeholder ) return data.placeholder;
				if ( data.text ) return data.text;

				if ( options ) {
					return options.templateSelection( data );
				}

				if ( data.nombre ) return data.nombre;
				if ( data.nombres ) return data.nombres;

				return '';
			}
		} )
			.trigger( 'change' );

		if ( options.defaultValue ) {

			var option = new Option( options.defaultValue.text, options.defaultValue.value, true, true );
			searchElement.append( option );
			// searchElement.trigger( {
			// 	type: 'select2:select',
			// 	params: {
			// 		data: data
			// 	}
			// } );
		}

		return searchElement;
	},

	setDefault: function ( searchElement, value, text ) {
		var option = new Option( text, value, true, true );
		searchElement.append( option );
	}
};