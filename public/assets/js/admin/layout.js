$(document).ready(function() {
	App.init();

	if($.fn.dataTable){
		$.fn.dataTable.ext.errMode = 'none';
		//$.fn.dataTable.ext.errMode = 'throw';
		$.extend( true, $.fn.dataTable.defaults, {
			language: {
				url: 'https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
			},
			ajax: {
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler,
					// 205: ajaxTokenMismatch,
					// 206: ajaxTokenMismatch,
					409: ajaxTokenMismatch
				}
			}
		} );
	}

	if ($.fn.datepicker && $.fn.datepicker.defaults) {
		$.fn.datepicker.defaults.format = "dd/mm/yyyy";
	}

	$.fn.modal.Constructor.prototype.enforceFocus = function() {};

	if($.gritter){
		$.extend($.gritter.options, {
			// class_name: 'gritter-light', // for light notifications (can be added directly to $.gritter.add too)
			position: 'top-right', // possibilities: bottom-left, bottom-right, top-left, top-right
			// fade_in_speed: 100, // how fast notifications fade in (string or int)
			// fade_out_speed: 100, // how fast the notices fade out
			// time: 3000 // hang on the screen for...
		});
	}

});

function toast(titulo, mensaje, tipo, options){
	//	if (typeof(options) != 'undefined'){
	//		toastr.options = options;
	//	}
	//
	////	toastr.options.positionClass = 'toast-bottom-left';
	//	toastr[tipo](mensaje, titulo);

	var image = '';
	//if(tipo == 'success'){
	//	tipo = tipo+"_primary";
	//	tipo = 'info';
	//}
	if(!tipo){
		tipo = 'info';
	}
	if(tipo != ''){
		image = '/assets/img/admin/alert_'+tipo+'.gif';
	} else {
		image = '/assets/img/admin/alert_info.gif';
	}
	$.gritter.add({
		title: titulo,
		text: mensaje,
		sticky: false,
		image: image,
		//time: ''
		time: 3000
	});
}
function sweet(titulo, mensaje, tipo, options){
	if (typeof(options) != 'undefined'){
	}
	swal({
		title: titulo,
		text: mensaje,
		type: tipo,
		html: true
	});
}
function redirect(url){
	// window.location.replace(url);
	window.location.href = url;
}

function refreshPage() {
	location.reload();
}

function reloadPage() {
	refreshPage();
}

function ajaxErrorHandler(e){
	//var errors = JSON.parse(e.responseText);
	//console.log('Error 500: '+e.responseText);
	toast('Error!', 'Error interno del sistema, contacte al desarrollador.', 'error');
}

function ajaxValidationHandler(e){
	var errors = JSON.parse(e.responseText);
	var message = '';
	$('label a[data-toggle="tooltip"]').tooltip();
	// $('label a[data-toggle="tooltip"]').tooltip('hide');
	$('.validation-alert-icon').remove();
	for(var elem in errors) {
		// message += errors[elem].join('<br>') + '<br>';
		if(Array.isArray(errors[elem])){
			message = errors[elem].join('<br>') + '<br>';
		} else {
			message = errors[elem];
		}

		if( $('#'+elem).length > 0) {
			$('#'+elem).closest('.form-group').find('label').append(" <a href='javascript:;' class='validation-alert-icon' data-html='true' data-toggle='tooltip' data-title='"+ message +"'><i class='fa fa-exclamation-circle text-red'></i></a>");
			// $('#'+elem).closest('.form-group').addClass('bg-red-lighter');
		}
	}
	toast('Alerta', message, 'warning');
	$('label a[data-toggle="tooltip"]').each(function(e) {
		if($(this).is(":visible")) {
			// $(this).tooltip('show');
		}

	});
	return false;
}

function ajaxTokenMismatch(e){
	//var errors = JSON.parse(e.responseText);
	//console.log('Error 500: '+e.responseText);
	// console.log('ajaxTokenMismatch');
	toast('Error!', 'La sesión ha caducado por favor recargue la pagina.', 'error');

	var txt;
	var r = confirm("La sesión ha caducado, desea recargar la página ahora?.");
	if (r == true) {
		location.reload();
	}
	// alert('Su sesión ha caducado por favor recargue la pagina');
}

function mostrarAlerta(titulo, mensaje){
	console.log("ELIMINAR ESTA FUNCION -> mostrarAlerta, corregir o dejar de utilizar este layout");
	$.gritter.add({
		title: titulo,
		text: mensaje,
		sticky: false,
		position: 'bottom-right',
		time: ''
	});
}

function str_pad (str, max) {
	str = str.toString();
	return str.length < max ? str_pad("0" + str, max) : str;
}

function format_date(strDate, options){
	if ( strDate === undefined || strDate === null || strDate === '' ) return '';
	var html = '';
	if(strDate !== undefined && strDate !== '') {
		date = new Date(strDate.replace(/\s/, 'T'));
		month = str_pad(date.getMonth() + 1, 2);

		if( options !== undefined ) {
			var meses_l = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Noviembre', 'Diciembre'];
			var meses_s = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Nov', 'Dic'];
			if( options.format !== undefined ) {
				switch( options.format ) {
					case 'legible' :
						html = str_pad(date.getDate(), 2) + ' de ' + meses_l[ date.getMonth() ] + ' de ' + date.getFullYear();
						break;
					case 'legible_resumido' :
						html = str_pad(date.getDate(), 2) + ' ' + meses_s[ date.getMonth() ] + ' \'' + date.getFullYear().toString().substr(-2);
						break;
					default:
						html = str_pad(date.getDate(), 2) + '.' + month + '.' + date.getFullYear();
				}
			}
		} else {
			html = str_pad(date.getDate(), 2) + '.' + month + '.' + date.getFullYear();
		}
	}
	return html;
}

function format_datetime(strDate, options){
	if ( strDate === undefined || strDate === null || strDate === '' ) return '';
	var html = '';
	if(strDate !== undefined && strDate !== '') {
		date = new Date(strDate.replace(/\s/, 'T'));
		month = str_pad(date.getMonth() + 1, 2);

		if( options !== undefined ) {
			var meses_l = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Noviembre', 'Diciembre'];
			var meses_s = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Nov', 'Dic'];
			if( options.format !== undefined ) {
				switch( options.format ) {
					case 'legible' :
						html = str_pad(date.getDate(), 2) + ' de ' + meses_l[ date.getMonth() ] + ' de ' + date.getFullYear() +' '+ str_pad(date.getHours(), 2) + ':' + str_pad(date.getMinutes(), 2);
						break;
					case 'legible_resumido' :
						html = str_pad(date.getDate(), 2) + ' ' + meses_s[ date.getMonth() ] + ' \'' + date.getFullYear().toString().substr(-2) +' '+ str_pad(date.getHours(), 2) + ':' + str_pad(date.getMinutes(), 2);
						break;
					default:
						html = str_pad(date.getDate(), 2) + '.' + month + '.' + date.getFullYear() +' '+ str_pad(date.getHours(), 2) + ':' + str_pad(date.getMinutes(), 2);
				}
			}
		} else {
			html = str_pad(date.getDate(), 2) + '.' + month + '.' + date.getFullYear() +' '+ str_pad(date.getHours(), 2) + ':' + str_pad(date.getMinutes(), 2);
		}
	}
	return html;
}

function datetime_obj(strDate){
	if ( strDate === undefined || strDate === null || strDate === '' ) return '';
	var html = '';
	if(strDate !== undefined && strDate !== '') {
		date = new Date(strDate.replace(/\s/, 'T'));
		return date;
	}
	return new Date();
}

function toggleCollapse(id) {
	$(id).slideToggle();
	// $(id).toggleClass( 'hide' );
}

function get_trans(campo, translations, lang) {
	for(var i = 0 ; i < translations.length ; i++) {
		if( translations[i]['campo'] === campo ) {
			if( lang !== undefined ) {
				return translations[i][lang];
			}
			return translations[i];
		}
	}

	return '';
}