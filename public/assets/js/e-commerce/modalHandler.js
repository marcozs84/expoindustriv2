/**
 * Created by MZ on 6/9/17.
 */

$(document).on('show.bs.modal', '.modal', function () {
	var zIndex = 1040 + (10 * $('.modal:visible').length);
	$(this).css('z-index', zIndex);
	setTimeout(function() {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});

function setModalHandler(event, action){
	$(document).on(event, action);
}

var _modalIndexCounter = 0;
var _modalUrls = [];
var _modalHolder = [];
function openModal(url, titulo, callback, options){

	//options = {
	//	size: ' modal-lg | modal-sm | modal-xs'
	//};

	var modalSize = 'modal-md';
	//var modalSize = ' modal-message';

	if(options != undefined){
		if(options.size != undefined){
			modalSize = options.size;
		}
	}

	_modalIndexCounter++;
	var newId = 'modal_'+_modalIndexCounter;
	var newModal = $('#stub_modal_window').clone();
	newModal.prop('id', newId);

	if(modalSize == 'modal-message'){
		newModal.addClass(modalSize);
	} else {
		newModal.find('.modal-dialog').addClass(modalSize);
	}

	if(titulo != undefined && titulo != ''){
		newModal.find('.modal-title').html(titulo);
	}

	newModal.find('.modal_btnReload').attr('onclick', 'reloadModal("'+ _modalIndexCounter +'")');
	newModal.find('.modal_btnExternal').attr('onclick', 'openPopup("'+ url +'", 900)');
	newModal.find('.modal_btnNewTab').attr('href', url);
	newModal.find('.modal_btnNewTab').attr('target', '_blank');


	newModal.find('.modal-footer').addClass('hide');
	$('#modals_container').append(newModal);

	$('#'+newId+' .modal-body').html('Cargando...');

	_modalUrls[newId] = url;
	$('#'+newId+' .modal-body').load(url, function(response, status, xhr){
		if(callback != undefined){
			callback(newModal);
		}

		if(xhr.status == 500){
			response = response.replace('position: fixed;', '');
			$(this).html(response);
			$(this).closest('.modal-dialog').css({
//                    'height': '100%',
				'width': '90%'
			});
			$(this).find('.left-panel').css({'height':'800px'});
//                $(this).closest('.modal-content').css('height', '100%');

		}

		// MEJORAR ESTA FUNCION PARA CARGAR ARCHIVOS PDF
		// console.log(xhr.getResponseHeader("content-type"));
		// console.log(xhr.status)
		// if(xhr.getResponseHeader("content-type") == 'application/pdf'){
		// 	var iframe = $('<iframe>');
		// 	iframe.attr('src', url);
		// 	// $('#targetDiv').append(iframe);
		// 	$(this).html('');
		// 	$(this).append(iframe);
		// }


//            $(this).prepend('<input type="hidden" class="currentModalId" value="'+newId+'" />')
	});
	var modal = $('#'+newId).modal('show');
	//var modal = $('#'+newId).modal({
	//	backdrop: 'static'
	//});

	_modalHolder[newId] = modal;

	modal.on('hidden.bs.modal', function (e) {
		modal.remove();
		if($('#lightboxOverlay') != undefined){
			$('#lightboxOverlay').remove();
			$('#lightbox').remove();
		}
		$(document).trigger('modal:hidden');
	});

	return modal;
}

function dismissModal(modal){
	//modal.on('hidden.bs.modal', function (e) {
	//	modal.remove();
	//});
	modal.modal('hide');
}

function reloadModal(reference, newUrl, callback) {

	var idModal = 0;
	if(isNaN(reference)){
		idModal = $(reference).closest('div.modal').prop('id');
	} else {
		idModal = 'modal_'+reference;
	}


	// var modal = _modalHolder[modalId];
	// console.log(modal.prop('id'));
	// console.log(_modalUrls[modal.prop('id')]);
	// var idModal = modal.prop('id');

	var url = '';
	if(newUrl != undefined){
		url = newUrl;
	} else {
		url = _modalUrls[idModal];
	}

	$('#'+idModal+' .modal-body').html('Recargando...');

	$('#'+idModal+' .modal-body').load(url, function(response, status, xhr){
		if(callback != undefined){
			callback(modal);
		}
		if(xhr.status == 500){
			response = response.replace('position: fixed;', '');
			$(this).html(response);
			$(this).closest('.modal-dialog').css({
//                    'height': '100%',
				'width': '90%'
			});
			$(this).find('.left-panel').css({'height':'800px'});
//                $(this).closest('.modal-content').css('height', '100%');
		}

//            $(this).prepend('<input type="hidden" class="currentModalId" value="'+newId+'" />')
	});

}

/**
 * Función para abrir ventanas popup
 * Ref.: https://stackoverflow.com/a/16861050/2367718
 * @param url
 * @param w
 * @param h
 */
function openPopup(url, w, h){

	// Fixes dual-screen position                         Most browsers      Firefox
	var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	var top = ((height / 2) - (h / 2)) + dualScreenTop;

	// Porcion de codigo para insertar un parametro en URL
	// Ref.: https://stackoverflow.com/a/8737675/2367718
	var separator = (url.indexOf("?")===-1)?"?":"&";
	var newParam = separator + "pp=1";
	var newUrl = url.replace(newParam,"");
	newUrl += newParam;
	url = newUrl;
	// ---------------------------------------------------

	var newWindow = window.open(url, '_blank', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	// Puts focus on the newWindow
	if (window.focus) {
		newWindow.focus();
	}

	// ventana = window.open(url, '_blank', 'top=10,left=10,height=900,width=1500,resizable=1,scrollbars=1');
}

function ajaxGet(url, data, options){

	var silent = false;
	var dataType = 'application/x-www-form-urlencoded';
	var successHandler = function(){
		console.log("success");
	};
	var failureHandler = function(){
		console.log("failure");
	};
	var doneHandler = function(){
		console.log("done");
	};
	var validationHandler = ajaxValidationHandler;
	var errorHandler = ajaxErrorHandler;
	var tokenMismatch = ajaxTokenMismatch;
	if(options != undefined){
		if(options.silent != undefined){
			silent = options.silent;
		}
		if(options.onSuccess != undefined){
			successHandler = options.onSuccess;
		}
		if(options.onFailure != undefined){
			failureHandler = options.onFailure;
		}
		if(options.onValidation != undefined){
			validationHandler = options.onValidation;
		}
		if(options.onError != undefined){
			errorHandler = options.onError;
		}
		if(options.onTokenMismatch != undefined){
			tokenMismatch = options.onTokenMismatch;
		}
		if(options.onDone != undefined){
			doneHandler = options.onDone;
		}
	}
	$.ajax({
		url: url,
		method: 'GET',
		dataType: dataType, // commented, because doesn't load htmls plain text
		headers: {
			//'X-CSRF-Token' : '{!! csrf_token() !!}'
			'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content')
		},
		statusCode: {
			422: ajaxValidationHandler,
			500: ajaxErrorHandler,
			409: ajaxTokenMismatch
		}
	}).done(function(data){
		if(data.result == true){
			if(silent){
				console.log('Listo!: ' + data.message);
			} else {
				toast('Listo!', data.message, 'success');
			}
			if(successHandler != undefined){
				successHandler(data);
			}
		} else {
			toast('Listo!', data.message, 'warning');
			if(failureHandler != undefined){
				failureHandler(data);
			}
		}
		if(doneHandler != undefined){
			doneHandler(data);
		}
	});
}

function ajaxPost(url, data, options){

	var silent = false;
	var dataType = 'json';
	var successHandler = function(){
		console.log("success");
	};
	var failureHandler = function(){
		console.log("failure");
	};
	var doneHandler = function(){
		console.log("done");
	};
	// var validationHandler = ajaxValidationHandler;
	var validationHandler = function(){
		console.log("validation");
	};
	var errorHandler = ajaxErrorHandler;
	var tokenMismatch = ajaxTokenMismatch;
	if(options !== undefined){
		if(options.silent !== undefined){
			silent = options.silent;
		}
		if(options.dataType !== undefined){
			dataType = options.dataType;
			if(options.dataType === 'html_script'){
				dataType = 'application/x-www-form-urlencoded';
			}
		}
		// ------ EVENTS
		if(options.onSuccess !== undefined){
			successHandler = options.onSuccess;
		}
		if(options.onFailure !== undefined){
			failureHandler = options.onFailure;
		}
		if(options.onValidation !== undefined){
			validationHandler = options.onValidation;
		}
		if(options.onError !== undefined){
			errorHandler = options.onError;
		}
		if(options.onTokenMismatch !== undefined){
			tokenMismatch = options.onTokenMismatch;
		}
		if(options.onDone !== undefined){
			doneHandler = options.onDone;
		}
	}

	$.ajax({
		url: url,
		method: 'POST',
		data: data,
		dataType: dataType, //<-- required commented for html response
		// type:'post',
		// async:true,             // agregado para uploads
		// processData: false,     // agregado para uploads
		// contentType: false,     // agregado para uploads
		headers: {
			//'X-CSRF-Token' : '{!! csrf_token() !!}'
			'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content')
		},
		statusCode: {
			422: function(data){
				if(!(options.onValidationOverride !== undefined && options.onValidationOverride === true)){
					ajaxValidationHandler(data);
				}
				var data1 = JSON.parse(data.responseText);
				validationHandler(data1);
			},
			500: errorHandler,
			409: tokenMismatch
			// 205: ajaxTokenMismatch
		}
	}).done(function(data){
		if(data.result == true){
			if(silent){
				console.log('Listo!: ' + data.message);
			} else {
				toast('Listo!', data.message, 'success');
			}
			if(successHandler != undefined){
				successHandler(data);
			}
		} else {
			toast('Listo!', data.message, 'warning');
			if(failureHandler != undefined){
				failureHandler(data);
			}
		}
		if(doneHandler != undefined){
			doneHandler(data);
		}
	});
}

function ajaxPatch(url, data, onSuccess, onError, options){
	var silent = false;
	var validationHandler = ajaxValidationHandler;
	if(options != undefined){
		if(options.silent != undefined){
			silent = options.silent;
		}
		if(options.onValidation !== undefined){
			validationHandler = options.onValidation;
		}
	}
	$.ajax({
		url: url,
		method: 'PATCH',
		data: data,
		headers: {
			//'X-CSRF-Token' : '{!! csrf_token() !!}'
			'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content')
		},
		statusCode: {
			422: validationHandler,
			500: ajaxErrorHandler,
			409: ajaxTokenMismatch
		}
	}).done(function(data){
		if(data.result == true){
			if(silent){
				console.log('Listo!: ' + data.message);
			} else {
				toast('Listo!', data.message, 'success');
			}
			if(onSuccess != undefined){
				onSuccess(data);
			}
		} else {
			toast('Listo!', data.message, 'warning');
			if(onError != undefined){
				onError(data);
			}
		}
	});
}

function ajaxDelete(url, ids, onSuccess, onError, options){
	var silent = false;
	if(options != undefined){
		if(options.silent != undefined){
			silent = options.silent;
		}
	}
	$.ajax({
		url: url,
		method: 'DELETE',
		data: {
			ids: ids
		},
		headers: {
			//'X-CSRF-Token' : '{!! csrf_token() !!}'
			'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content')
		},
		statusCode: {
			422: ajaxValidationHandler,
			500: ajaxErrorHandler,
			409: ajaxTokenMismatch
		}
	}).done(function(data){
		//		var data = JSON.parse(data);
		if(data.result == true){
			if(silent){
				console.log('Listo!: ' + data.message);
			} else {
				toast('Listo!', data.message, 'success');
			}
			if(onSuccess != undefined){
				onSuccess(data);
			}
		} else {
			toast("Error!", data.message, "error");
			if(onError != undefined){
				onError(data);
			}
		}
	});
}

function ajaxUpload(url, data, onSuccess, onError, options){
	var silent = false;
	if(options != undefined){
		if(options.silent != undefined){
			silent = options.silent;
		}
	}
	$.ajax({
		url: url,
		method: 'POST',
		data: data,
		dataType:'json',
		// type:'post',
		async:true,             // agregado para uploads
		processData: false,     // agregado para uploads
		contentType: false,     // agregado para uploads
		headers: {
			//'X-CSRF-Token' : '{!! csrf_token() !!}'
			'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content')
		},
		statusCode: {
			422: ajaxValidationHandler,
			500: ajaxErrorHandler,
			409: ajaxTokenMismatch
		}
	}).done(function(data){
		if(data.result == true){
			if(silent){
				console.log('Listo!: ' + data.message);
			} else {
				toast('Listo!', data.message, 'success');
			}
			if(onSuccess != undefined){
				console.log(data)
				onSuccess(data);
			}
		} else {
			toast('Listo!', data.message, 'warning');
			if(onError != undefined){
				onError(data);
			}
		}
	});
}

function makeGetters(formID){
	var html = '';
	$('#'+formID).find('input').each(function(e){
		html += $(this).prop('id') + ": $('#"+formID+" #"+ $(this).prop('id') +"').val()," + "\n";
	});
	console.log(html);

	var html = '';
	$('#'+formID).find('select').each(function(e){
		html += $(this).prop('id') + ": $('#"+formID+" #"+ $(this).prop('id') +"').val()," + "\n";
	});
	console.log(html);

	var html1 = '';
	var html2 = '';
	$('#'+formID).find('textarea').each(function(e){
		html1 += $(this).prop('id') + ": $('#"+formID+" #"+ $(this).prop('id') +"').val()," + "\n";
		html2 += $(this).prop('id') + ": tinymce.get('" + $(this).prop('id') + "').getContent()," + "\n";
	});
	console.log(html1);
	console.log(html2);
}
