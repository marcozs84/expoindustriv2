@yield('css')

@yield('content')

@yield('libraries')

@yield('scripts')