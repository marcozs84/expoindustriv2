        <div class="error" style="min-height:300px;">
            <div class="error-code m-b-10">{{ $estado }} <i class="fa fa-warning"></i></div>
            <div class="error-content">
                <div class="error-message">{{ $mensajeCorto }}</div>
                <div class="error-desc m-b-20">
                    {!! $mensaje !!}
                </div>
                <div>
                    <a href="javascript:;" data-dismiss="modal" class="btn btn-success">Cerrar</a>
                </div>
            </div>
        </div>
        <!-- end error -->
