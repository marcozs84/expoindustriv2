<!-- begin #footer -->
<div id="footer" class="footer">
	Copyright &copy; {{ date('Y') }} ExpoIndustri Sweden AB - All Rights Reserved
</div>
<!-- end #footer -->
