<script src="/assets/js/bundle.js"></script>
<script src="/assets/js/theme/default.js"></script>
<script src="/assets/js/apps.min.js"></script>

<script src="/assets/js/admin/layout.js?v=3"></script>
<script src="/assets/js/admin/modalHandler.js?v=2"></script>
<script src="/assets/js/admin/searchHandler.js"></script>

<script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>


<script>
	$(document).ready(function() {
		App.init();
	});
</script>

@stack('scripts')