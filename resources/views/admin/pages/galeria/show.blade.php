@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Galeria</li>
	</ol>

	<h1 class="page-header">Nuevo Galeria</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Galeria</h4>
		</div>
		<div class="panel panel-body">
			<form id="formGaleriaShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $galeria->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Galeria Tipo</label>
					<div class="col-8">
						{{ $galeria->idGaleriaTipo }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{{ $galeria->owner_type }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{{ $galeria->owner_id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{{ $galeria->nombre }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                       {!! $galeria->descripcion !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{{ $galeria->estado }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{{ ($galeria->created_at ? $galeria->created_at->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>


					<div class="col-12">
						<h4>Galería de imágenes</h4>
						{{--<a href="javascript:;" onclick="show_thumbs()">Mostrar miniaturas</a><br>--}}
						<div id="gallery" class="gallery">
							<ul class="attached-document clearfix">
								@foreach($galeria->Imagenes as $imagen)
									<li class="fa-camera">
										<div class="document-file">
											<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-{{ $galeria->owner_id }}">
												<img src="{{ $imagen->ruta_publica_producto_thumb }}" alt="" />
											</a>
										</div>
										<div class="document-name">
											<a href="javascript:;">{{ str_replace( '/', ' / ', $imagen->ruta_publica_producto ) }}</a>

											<div class="thumbs_info hide" style="margin-top:10px;">
												@if(isset($img_paths[$imagen->id]))
													@foreach($img_paths[$imagen->id] as $path)
														@if(preg_match('/(_res|thumb)/', $path) )
															<a href="{{ $imagen->ruta_publica_producto_thumb }}" target="_blank">{{ $path }}</a>
															<a href="javascript:;" onclick="image_remove('{{ pathinfo($imagen->ruta, PATHINFO_DIRNAME) }}/{{ $path }}')" class=""><i class="fa fa-trash"></i></a><br>
														@else
															{{ $path }}<br>
														@endif
													@endforeach
												@endif
											</div>

										</div>
									</li>
								@endforeach
							</ul>
						</div>
					</div>

			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="galeriaEdit({{ $galeria->id }})">Edit</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="galeriaCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif


function galeriaCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formGaleriaShow:cancel');
	@else
		redirect('{{ route('admin.galeria.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

