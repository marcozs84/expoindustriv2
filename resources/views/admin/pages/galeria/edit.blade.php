@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.galeria.index') }}">Galeria</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $galeria->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Galeria</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Galeria</h4>
		</div>
		<div class="panel panel-body">
			<form id="formGaleriaEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Galeria Tipo</label>
					<div class="col-8">
						{!! Form::select('idGaleriaTipo', [], '', [
							'id' => 'idGaleriaTipo',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Galeria Tipo</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idGaleriaTipo', $galeria->idGaleriaTipo, [--}}
							{{--'id' => 'idGaleriaTipo',--}}
							{{--'placeholder' => 'Id Galeria Tipo',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', $galeria->owner_type, [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', $galeria->owner_id, [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', $galeria->nombre, [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                        {!! Form::textarea('descripcion', $galeria->descripcion, [
                            'id' => 'descripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::text('estado', $galeria->estado, [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="galeriaCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="galeriaSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){



	$idGaleriaTipoSearch = Search.resource('#formGaleriaEdit #idGaleriaTipo', {
		url: '{{ route('resource.galeriaTipo.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		defaultValue: {
			value: {{ $galeria->idGaleriaTipo }},
			text: '{{ $galeria->GaleriaTipo->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idGaleriaTipoSearch, {{ $galeria->idGaleriaTipo }}, '{{ $galeria->GaleriaTipo->id }});



}

$( "#formGaleriaEdit" ).submit(function( event ) {
	galeriaSave();
	event.preventDefault();
});

function galeriaSave(){
	var url = '{{ route('resource.galeria.update', [$galeria->id]) }}';
	ajaxPatch(url, {
		idGaleriaTipo: $('#formGaleriaEdit #idGaleriaTipo').val(),
		owner_type: 	$('#formGaleriaEdit #owner_type').val(),
		owner_id: 		$('#formGaleriaEdit #owner_id').val(),
		nombre: 		$('#formGaleriaEdit #nombre').val(),
		descripcion: 	$('#formGaleriaEdit #descripcion').val(),
		estado: 		$('#formGaleriaEdit #estado').val()
	}, function(){
		$(document).trigger('formGaleriaEdit:success');
	});
}

function galeriaCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formGaleriaEdit:cancel');
	@else
		redirect('{{ route('admin.galeria.index')  }}');
	@endif
}

</script>
@endpush

