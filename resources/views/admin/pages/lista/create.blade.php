@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.lista.index') }}">Lista</a></li>
		<li class="breadcrumb-item active">Nuevo/a lista</li>
	</ol>

	<h1 class="page-header">Nuevo Lista</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Lista</h4>
		</div>
		<div class="panel panel-body">
			<form id="formListaCreate" method="post" class="form-horizontal row">


				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-2">Nombre</label>
					<div class="col-10">
						{!! Form::text('nombre', '', [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Padre <br><small>no modificar</small></label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idPadre', 0, [--}}
							{{--'id' => 'idPadre',--}}
							{{--'placeholder' => 'Id Padre',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}

			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="listaCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="listaSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif

$( "#formListaCreate" ).submit(function( event ) {
	listaSave();
	event.preventDefault();
});

function listaSave(){
	var url = '{{ route('resource.lista.store') }}';
	ajaxPost(url, {
		idPadre: 		0, //$('#formListaCreate #idPadre').val(),
		nombre: 		$('#formListaCreate #nombre').val()
	}, function(){
		$(document).trigger('formListaCreate:success');
	});
}

function listaCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formListaCreate:cancel');
	@else
		redirect('{{ route('admin.lista.index')  }}');
	@endif
}

function formInit(){



}

</script>
@endpush

