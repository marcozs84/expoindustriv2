@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.lista.index') }}">Lista</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $lista->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Lista</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Lista</h4>
		</div>
		<div class="panel panel-body">
			<form id="formListaEdit" method="post" class="form-horizontal row">

				<table id="dtListaItems" class="table table-condensed table-striped"></table>

			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-info btn-sm" href="javascript:;" onclick="listaItemCreate_lista()">Agregar item</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="listaCancel()">Aceptar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>


<script>

var dtListaItems = null;

@if($isAjaxRequest)
    formInit();
	initDataTable();
@else
    $(function(){
    	formInit();
		initDataTable();
    });
@endif

$( "#formListaEdit" ).submit(function( event ) {
	listaSave();
	event.preventDefault();
});

function listaCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formListaEdit:cancel');
	@else
		redirect('{{ route('admin.lista.index')  }}');
	@endif
}

function listaItemCreate_lista(){
	var url = '{{ route('admin.listaItem.create_alt', [ 'idLista' => $lista->id ]) }}';
	var modal = openModal(url, 'Nuevo elemento de lista', null, { size:'modal-lg' });
	setModalHandler('formListaItemCreate:success', function(){
		$(document).trigger('formListaEdit_alt:success');
		dismissModal(modal);
		dtListaItems.ajax.reload();
	});
}

function listaItemEdit_lista(id){
	var url = '{{ route('admin.listaItem.edit_alt', ['idRecord']) }}';
	url = url.replace('idRecord', id);
	var modal = openModal(url, 'Editar ListaItem', null, { size:'modal-lg' });
	setModalHandler('formListaItemEdit:success', function(){
		$(document).trigger('formListaEdit_alt:success');
		dismissModal(modal);
		dtListaItems.ajax.reload(null, false);
	});
}

function listaItemDelete_lista(id){
	var ids = [id];

	swal({
		text: 'Está seguro que desea eliminar los registros seleccionados?',
		icon: 'warning',
		buttons: {
			cancel: 'No',
			ok: 'Si'
		},
		dangerMode: true
	}).then(function(response) {
		if (response === 'ok') {
			var url = '{{ route('resource.listaItem.destroy', [0]) }}';
			ajaxDelete(url, ids, function(){
				$(document).trigger('formListaEdit_alt:success');
				swal('Registros eliminados', { icon: "success" });
				dtListaItems.ajax.reload(null, false);
			});
		}
	});
}

function formInit(){

}

function initDataTable(){
	if (!$.fn.DataTable.isDataTable('#dtListaItems')) {
		dtListaItems = $('#dtListaItems').DataTable({
			paging: false,
			searching: false,
//				dom: 'Bfrtip',
			buttons: [
				{
					text: 'Nuevo',
					action: function(e, dt, node, config){
						alert('Button activated');
					}
				}
			],
			ajax: {
				url: '{{ route('resource.listaItem.ds') }}',
				dataSrc: 'data',
				method: 'POST',
				'headers': {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'
				},
				data: function (d){ // Procesa datos enviados
					 d.idLista = {{ $lista->id }};
					// ...
				},
				dataFilter: function(data){ // Procesa datos recibidos
					return data;
//	                    var data = JSON.parse(data);
//	                    for(var i = 0 ; i < data.data.length; i++){
//	                        // ...
//	                    }
//	                    return JSON.stringify( data );
				}
			},
			columns: [
				{ data: 'texto', title: 'Texto'},
				{ data: 'valor', title: 'Valor'},
				{ data: 'orden', title: 'Orden'},

				{ title: ''},
				// ...
			],
			columnDefs: [
					@php $counter = 0 @endphp
				{
					// texto,
					targets: {{ $counter++ }},
					render: function( data, type, row ) {
						return data;
					}
				},{
					// valor,
					targets: {{ $counter++ }},
					render: function( data, type, row ) {
						return data;
					}
				},{
					// orden,
					targets: {{ $counter++ }},
					render: function( data, type, row ) {
						return data;
					}
				}, {
					targets: {{ $counter++ }},
					className: 'text-center with-btn',
					width: '100px',
					render: function(data, type, row){
						var html = '';
						html += '<a href="javascript:;" onclick="listaItemEdit_lista('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
						html += '&nbsp;&nbsp;';
						html += '<a href="javascript:;" onclick="listaItemDelete_lista('+ row.id +')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>';
						return html;
					}
				}
			],
			fixedHeader: {
				header: true,
				headerOffset: $('#header').height()
			},
			bSort : false,
			bAutoWidth: false,
			processing: true,
			serverSide: true,
			responsive: true,
			fixedColumns: true
		}).on('draw.dt', function(){
			$('#idsChecker').on('change', function(e){
				$('.chkId').prop('checked', $(this).prop('checked'));
			});
		});
	} else {
		dtListaItems.ajax.reload();
	}
}

</script>
@endpush

