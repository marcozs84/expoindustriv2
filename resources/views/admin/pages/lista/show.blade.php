@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')


<style>
    .col-form-label, .row.form-group>.col-form-label {
        padding:0px;
        text-align:right;
    }
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Lista</li>
	</ol>

	<h1 class="page-header">Nuevo Lista</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Lista</h4>
		</div>
		<div class="panel panel-body">
			<form id="formListaShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $lista->id }}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Padre</label>--}}
					{{--<div class="col-8">--}}
						{{--{{ $lista->idPadre }}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{{ $lista->nombre }}
					</div>
				</div>

				<table class="table table-condensed table-stripped">
					<tr>
						<th class="text-center">Texto</th>
						<th class="text-center">Valor</th>
						<th class="text-center">Orden</th>
					</tr>
						@foreach($listaItems as $li )
							<tr>
								<td class="text-center">{{ $li->texto }}</td>
								<td class="text-center">{{ $li->valor }}</td>
								<td class="text-center">{{ $li->orden }}</td>
							</tr>
						@endforeach
				</table>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="listaCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif


function listaCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formListaShow:cancel');
	@else
		redirect('{{ route('admin.lista.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

