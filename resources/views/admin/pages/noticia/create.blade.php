@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.noticia.index') }}">Noticias</a></li>
		<li class="breadcrumb-item active">Nueva noticia</li>
	</ol>

	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Nueva Noticia</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nueva noticia</h4>
		</div>
		<div class="panel-body">
			<form id="formNoticiaCreate" method="get">

				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Titulo ES</label>
					<div class="col-md-9">
						{!! Form::text('titulo_es', '', [
							'id' => 'titulo_es',
							'placeholder' => 'Titulo ES',
							'class' => 'form-control form-control-sm m-b-5'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Resumen ES</label>
					<div class="col-md-9">
						{!! Form::textarea('resumen_es', '', [
							'id' => 'resumen_es',
							'placeholder' => 'Resumen ES',
							'class' => 'form-control m-b-5',
							'rows' => 3,
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Titulo EN</label>
					<div class="col-md-9">
						{!! Form::text('titulo_en', '', [
							'id' => 'titulo_en',
							'placeholder' => 'Titulo EN',
							'class' => 'form-control form-control-sm m-b-5'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Resumen EN</label>
					<div class="col-md-9">
						{!! Form::textarea('resumen_en', '', [
							'id' => 'resumen_en',
							'placeholder' => 'Resumen EN',
							'class' => 'form-control m-b-5',
							'rows' => 3,
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Titulo SE</label>
					<div class="col-md-9">
						{!! Form::text('titulo_se', '', [
							'id' => 'titulo_se',
							'placeholder' => 'Titulo SE',
							'class' => 'form-control form-control-sm m-b-5'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Resumen SE</label>
					<div class="col-md-9">
						{!! Form::textarea('resumen_se', '', [
							'id' => 'resumen_se',
							'placeholder' => 'Resumen SE',
							'class' => 'form-control m-b-5',
							'rows' => 3,
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Fuente</label>
					<div class="col-md-9">
						{!! Form::text('fuente', '', [
							'id' => 'fuente',
							'placeholder' => 'Fuente',
							'class' => 'form-control m-b-5',
							'rows' => 3,
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Fuente URL</label>
					<div class="col-md-9">
						{!! Form::textarea('fuenteUrl', '', [
							'id' => 'fuenteUrl',
							'placeholder' => 'Fuente',
							'class' => 'form-control m-b-5',
							'rows' => 3,
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Pais</label>
					<div class="col-md-9">
						{!! Form::select('pais', $global_paises, null, [
							'id' => 'pais',
							'placeholder' => '',
							'class' => 'form-control'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">Fecha publicacion</label>
					<div class="col-md-9">
						{!! Form::text('fechaPublicacion', '', [
							'id' => 'fechaPublicacion',
							'placeholder' => '',
							'class' => 'form-control'
						]) !!}
					</div>
				</div>


				<div class="hr-line-dashed"></div>

				<div class="form-group row m-b-10">
					<div class="col-lg-12 text-right">
						<a id="btnModeloCancelar" class="btn btn-white" data-dismiss="modal" href="javascript:;">Cancelar</a>
						<a id="btnModeloAceptar" class="btn btn-primary" href="javascript:guardarNoticia();" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script>

	@if($isAjaxRequest)
formInit();
	@else
$(function(){ formInit(); });
	@endif

	function formInit() {
		$('#fechaPublicacion').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
		}).on('changeDate', function(){
			console.log("lala");
//		buscar();
		});
		$("#fechaPublicacion").mask("99/99/9999");

		$('#fechaPublicacion').datepicker({
			todayHighlight: true
		});
	}

	function guardarNoticia(){
		var url = '{{ route('resource.noticia.store') }}';
		var data = {

			titulo_es : $('#formNoticiaCreate #titulo_es').val(),
			resumen_es : $('#formNoticiaCreate #resumen_es').val(),
			titulo_en : $('#formNoticiaCreate #titulo_en').val(),
			resumen_en : $('#formNoticiaCreate #resumen_en').val(),
			titulo_se : $('#formNoticiaCreate #titulo_se').val(),
			resumen_se : $('#formNoticiaCreate #resumen_se').val(),
			fuente : $('#formNoticiaCreate #fuente').val(),
			fuenteUrl : $('#formNoticiaCreate #fuenteUrl').val(),
			pais : $('#formNoticiaCreate #pais').val(),
			fechaPublicacion : $('#formNoticiaCreate #fechaPublicacion').val()
		};
		ajaxPost(url, data, function(data){
			@if($isAjaxRequest)
				$(document).trigger('formNoticiaCreate:aceptar');
			@else
				var url = '{{ route('admin.noticia.edit', ['idn']) }}';
				url = url.replace('idn', data.data.id);
				redirect(url);
			@endif
		});
	}

</script>
@endpush

