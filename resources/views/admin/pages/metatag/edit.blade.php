@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.metatag.index') }}">Metatag</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $metatag->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Metatag</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Metatag</h4>
		</div>
		<div class="panel panel-body">
			<form id="formMetatagEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Llave</label>
					<div class="col-8">
						{!! Form::text('llave', $metatag->llave, [
							'id' => 'llave',
							'placeholder' => 'Llave',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Name</label>
					<div class="col-8">
						{!! Form::select('name', [
							'keywords' => 'Keywords',
							'description' => 'Description',
							'title' => 'Title',
						], $metatag->name, [
							'id' => 'name',
							'placeholder' => 'Name',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Es</label>
                    <div class="col-8">
                        {!! Form::textarea('es', $metatag->es, [
                            'id' => 'es',
                            'placeholder' => 'Es',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">En</label>
                    <div class="col-8">
                        {!! Form::textarea('en', $metatag->en, [
                            'id' => 'en',
                            'placeholder' => 'En',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Se</label>
                    <div class="col-8">
                        {!! Form::textarea('se', $metatag->se, [
                            'id' => 'se',
                            'placeholder' => 'Se',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="metatagCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="metatagClone()" >Clone</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="metatagSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){





}

$( "#formMetatagEdit" ).submit(function( event ) {
	metatagSave();
	event.preventDefault();
});

function metatagSave(){
	var url = '{{ route('resource.metatag.update', [$metatag->id]) }}';
	ajaxPatch(url, {
		llave: 		$('#formMetatagEdit #llave').val(),
		name: 			$('#formMetatagEdit #name').val(),
		es: 			$('#formMetatagEdit #es').val(),
		en: 			$('#formMetatagEdit #en').val(),
		se: 			$('#formMetatagEdit #se').val()
	}, function(){
		$(document).trigger('formMetatagEdit:success');
	});
}

function metatagClone(){
	var url = '{{ route('resource.metatag.store') }}';
	ajaxPost(url, {
		llave: 		$('#formMetatagEdit #llave').val(),
		name: 			$('#formMetatagEdit #name').val(),
		es: 			$('#formMetatagEdit #es').val(),
		en: 			$('#formMetatagEdit #en').val(),
		se: 			$('#formMetatagEdit #se').val()
	}, function(){
		$(document).trigger('formMetatagEdit:success');
	});
}

function metatagCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formMetatagEdit:cancel');
	@else
		redirect('{{ route('admin.metatag.index')  }}');
	@endif
}

</script>
@endpush

