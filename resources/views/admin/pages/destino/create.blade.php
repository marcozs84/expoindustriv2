@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.destino.index') }}">Destino</a></li>
		<li class="breadcrumb-item active">Nuevo/a destino</li>
	</ol>

	<h1 class="page-header">Nuevo Destino</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Destino</h4>
		</div>
		<div class="panel panel-body">
			<form id="formDestinoCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Pais</label>
					<div class="col-8">
						{!! Form::select('idPais', $paises, '', [
							'id' => 'idPais',
							'placeholder' => 'País',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', '', [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio</label>
					<div class="col-8">
						{!! Form::text('precio', '', [
							'id' => 'precio',
							'placeholder' => 'Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Moneda</label>
					<div class="col-8">
						{!! Form::select('moneda', $monedas, '', [
							'id' => 'moneda',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::checkbox('estado', 1, true, [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="destinoCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="destinoSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif

$( "#formDestinoCreate" ).submit(function( event ) {
	destinoSave();
	event.preventDefault();
});

function destinoSave(){
	var url = '{{ route('resource.destino.store') }}';
	ajaxPost(url, {
		idPais: 		$('#formDestinoCreate #idPais').val(),
		nombre: 		$('#formDestinoCreate #nombre').val(),
		precio: 		$('#formDestinoCreate #precio').val(),
		moneda: 		$('#formDestinoCreate #moneda').val(),
		estado: 		( $('#formDestinoCreate #estado').prop('checked') ) ? 1 : 0
	}, function(){
		$(document).trigger('formDestinoCreate:success');
	});
}

function destinoCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formDestinoCreate:cancel');
	@else
		redirect('{{ route('admin.destino.index')  }}');
	@endif
}

function formInit(){



}

</script>
@endpush

