@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')


@push('css')
<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->

{{--<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />--}}
{{--<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />--}}
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
<style>
	.col-mz{
		word-wrap: break-word !important;
		white-space:pre-wrap !important;
		text-overflow: ellipsis !important;
	}

	.flot-tick-label {
		max-width:50px !important;
	}

</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Reporte Ventas</li>
	</ol>

	<h1 class="page-header">Reporte Ventas </h1>

	<div class="row">
		<!-- begin col-8 -->
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Estadísticas</h4>
				</div>
				<div class="panel-body p-b-0">
					<div class="row" id="searchForm">
						<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">
							<label for="periodoBusqueda">Periodo</label>
							<div style="width:100%;">
								<div id="periodoBusqueda" class="btn btn-default btn-block text-left f-s-12">
									<i class="fa fa-caret-down pull-right m-t-2"></i>
									<span></span>
								</div>
							</div>
						</div>

						<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">
							<label for="vendedorId" style="">Vendedor</label>
							{!! Form::select('vendedorId', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id'), null, [
								'id' => 'vendedorId',
								'class' => 'form-control multiple-select2',
								'style' => 'width:100%;',
								'multiple' => 'multiple'
							]) !!}
						</div>
						<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">
							<label for="vendedorId" style="">Agrupacion</label>
							{!! Form::select('agrupacion', [
								'grupo' => 'General',
								'individual' => 'Individual'
							], 'grupo', [
								'id' => 'agrupacion',
								'class' => 'form-control default-select2',
								'style' => 'width:100%;',
							]) !!}
						</div>
						{{--<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">--}}
						{{--<label for="estado" style="">Resolución</label>--}}
						{{--{!! Form::select('resolucion', [--}}
						{{--'diario' => 'Diario',--}}
						{{--'semanal' => 'Semanal'--}}
						{{--], null, [--}}
						{{--'id' => 'resolucion',--}}
						{{--'class' => 'form-control multiple-select2',--}}
						{{--'style' => 'width:100%;'--}}
						{{--]) !!}--}}
						{{--</div>--}}
						<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

							{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
							<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaCreate()"><i class="fa fa-plus"></i> Registrar venta</a>
							<a class="btn btn-primary btn-sm" href="javascript:;" onclick="reporteVentaEliminar()"><i class="fa fa-trash"></i> Eliminar</a>

							<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:;" onclick="buscar()"><i class="fa fa-search"></i> Buscar</a>
							<a class="btn btn-sm btn-primary pull-right" href="javascript:;" onclick="limpiar()">Limpiar</a>

							{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<!-- begin col-8 -->
		<div class="col-lg-8">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Estadísticas</h4>
				</div>
				<div class="panel-body">
					<div class="row flot-container">
						<div id="flot-chart" class="height-sm" style="width:100%; height:800px; "></div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-lg-4">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Tendencia</h4>
				</div>
				<div class="panel-body p-t-0" style="height: 330px; /*max-height: 330px;*/ overflow: scroll;">
					<div class="table-responsive">
						<table class="table table-valign-middle">
							<thead>
							<tr>
								<th>Vendedor</th>
								<th>Total</th>
								<th>Tendencia</th>
							</tr>
							</thead>
							<tbody id="tblVendedores">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Ordenes</h4>
				</div>
				<div class="panel-body">
					<table id="data-table" class="table table-striped table-bordered width-full table-condensed"></table>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

<script src="/assets/plugins/flot/dom-tools.js"></script>
<script src="/assets/plugins/flot/EventEmitter.js"></script>
{{--<script src="/assets/plugins/jquery_flot/jquery.flot.min.js"></script>--}}
{{--<script src="/assets/plugins/jquery_flot/jquery.flot.time.min.js"></script>--}}
{{--<script src="/assets/plugins/jquery_flot/jquery.flot.resize.js"></script>--}}
<script src="/assets/plugins/sparkline/jquery.sparkline.js"></script>
<script src="/assets/plugins/flot/flot.js"></script>
<script src="/assets/plugins/flot/flot.time.js"></script>
<script src="/assets/plugins/flot/flot.stack.js"></script>
<script src="/assets/plugins/flot/flot.categories.js"></script>



{{--<script src="/assets/js/demo/chart-flot.demo.js"></script>--}}

<!-- ================== END PAGE LEVEL JS ================== -->
<script>

	var datatable = null;
	var modal = null;

	var originalInicio = moment('2018/01/01', 'YYYY/M/D');
//	var originalInicio = moment().subtract(12, 'month').startOf('month'); //moment().subtract(30, 'day');
	//var originalInicio = moment().subtract(1, 'week');
	var originalFin = moment().endOf('month'); //moment();

	var fechaInicio = originalInicio;
	var fechaFin = originalFin;
	//var fechaInicio = '2018-11-01';
	//var fechaFin = '2018-11-30';

	var plot = null;
	var barPlot = null;
	var dataPlot = [];

	var standard    = '#CCCCFF',
		blue		= '#348fe2',
		blueLight	= '#5da5e8',
		blueDark	= '#1993E4',
		aqua		= '#49b6d6',
		aquaLight	= '#6dc5de',
		aquaDark	= '#3a92ab',
		green		= '#00acac',
		greenLight	= '#33bdbd',
		greenDark	= '#008a8a',
		orange		= '#f59c1a',
		orangeLight	= '#f7b048',
		orangeDark	= '#c47d15',
		dark		= '#2d353c',
		grey		= '#b6c2c9',
		purple		= '#727cb6',
		purpleLight	= '#8e96c5',
		purpleDark	= '#5b6392',
		red         = '#ff5b57';

	var colorArray = [
		'#CCCCFF',
		'#5b6392',
		'#ff5b57',
		'#348fe2',
		'#008a8a',
		'#5da5e8',
		'#f59c1a',
		'#1993E4',
		'#c47d15',
		'#49b6d6',
		'#f7b048',
		'#6dc5de',
		'#2d353c',
		'#3a92ab',
		'#00acac',
		'#33bdbd',
		'#b6c2c9',
		'#727cb6',
		'#8e96c5',
	];

	$(document).ready(function(){
		initSearchForm();
		initDataTable();
		initChart();
		getLineasData();
//	initSingleCharts();
	});

	function buscar() {
		getLineasData();
		datatable.ajax.reload();
	}

	function limpiar() {
		$('#vendedorId').val([]).trigger('change');
		fechaInicio = originalInicio;
		fechaFin = originalFin;
		$('#periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
		getLineasData();
	}

	function initSearchForm() {

		$('#vendedorId, #agrupacion').select2().on('select2:select', function (e) {
			buscar();
		}).on('select2:unselect', function (e) {
			buscar();
		});

		$('#periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
		$('#periodoBusqueda').daterangepicker({
			format: 'DD/MM/YYYY',
			startDate: originalInicio,
			endDate:originalFin,
			minDate: '01/01/2018',
			maxDate: '31/12/{{ date('Y') + 1 }}',
			dateLimit: { months: 36 },
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			linkedCalendars: false,
			ranges: {
				'Hoy día': [moment(), moment()],
				'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Esta semana': [moment().startOf('week'), moment().endOf('week')],
				'Semana anterior': [moment().subtract(1, 'weeks').startOf('week'), moment().subtract(1, 'weeks').endOf('week')],
				'Últimos 7 días': [moment().subtract(1, 'weeks'), moment()],
				'Últimas 2 semanas': [moment().subtract(2, 'weeks'), moment()],
				'Últimos 30 días': [moment().subtract(1, 'month'), moment()],
				'Este més': [moment().startOf('month'), moment().endOf('month')],
				'Més anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'Últimos 3 meses': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')],
				'Último semestre': [moment().subtract(6, 'month').startOf('month'), moment().endOf('month')],
				'Último año': [moment().subtract(12, 'month').startOf('month'), moment().endOf('month')]

			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' a ',
//			singleDatePicker: true,
			locale: {
				applyLabel: 'Aceptar',
				cancelLabel: 'Cancelar',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Personalizado',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#periodoBusqueda span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			fechaInicio = start;
			fechaFin = end;
//		getLineasData();
			buscar();
		});
	}

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 10,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.oferta.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.fechaInicio = fechaInicio.format('YYYY/MM/DD');
						d.fechaFin = fechaFin.format('YYYY/MM/DD');
						d.idVendedor = $('#vendedorId').val();
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
					{ data: 'idCliente', title: 'Cliente'},
					{ data: 'idCotizacion', title: 'Cotizacion'},
					{ data: 'idVendedor', title: 'Vendedor'},
					{ data: 'idHorario', title: 'IdHorario'},
					{ data: 'idOrdenTipo', title: 'IdOrdenTipo'},
					{ data: 'idOrdenEstado', title: 'IdOrdenEstado'},
					{ data: 'fechaEntrega', title: 'FechaEntrega'},
					{ data: 'precioTransporte', title: 'PrecioTransporte'},
					{ data: 'precioFacturado', title: 'PrecioFacturado'},
					{ data: 'nombreFactura', title: 'NombreFactura'},
					{ data: 'nit', title: 'Nit'},
					{ data: 'created_at', title: 'Created_at'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
				],
				columnDefs: [
						@php $counter = 0 @endphp
					{
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '30px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="ordenShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// idCliente,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							var html = '';
							html += '<a href="javascript:;" onclick="clienteShow('+ row.idCliente +')" >'+row.cliente.agenda.nombres_apellidos+'</a>';
							return html;
						}
					},{
						// idCotizacion,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							var html = '';
							if( row.idCotizacion > 0 ) {
								html += '<a href="javascript:;" onclick="cotizacionShow('+ row.idCotizacion +')" >Cotizacion</a>';
							}
							return html;
						}
					},{
						// idVendedor,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							var html = '';
							html += '<a href="javascript:;" onclick="usuarioShow('+ row.idVendedor +')" >'+row.vendedor.owner.agenda.nombres_apellidos+'</a>';
							return html;
						}
					},{
						// idHorario,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// idOrdenTipo,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// idOrdenEstado,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// fechaEntrega,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					},{
						// precioTransporte,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// precioFacturado,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// nombreFactura,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// nit,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// created_at,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '100px',
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';
							html += '<a href="javascript:;" onclick="ordenEdit('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
							return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
//		datatable.ajax.reload();
			buscar();
		}

	}

	function ordenShow(id){
		var url = '{{ route('admin.orden.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Orden', null, {
			'size': 'modal-lg'
		});
	}

	function ordenCreate(){
		var url = '{{ route('admin.orden.create') }}';
		var modal = openModal(url, 'Nuevo Orden', null, { size:'modal-lg' });
		setModalHandler('formOrdenCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function ordenEdit(id){
		var url = '{{ route('admin.orden.index') }}/'+id+'/edit';
		var modal = openModal(url, 'Editar Orden', null, { size:'modal-lg' });
		setModalHandler('formOrdenEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function ordenDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.orden.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

	function clienteShow(id){
		var url = '{{ route('admin.cliente.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Cliente', null, {
			'size': 'modal-lg'
		});
	}

	function cotizacionShow(id){
		var url = '{{ route('admin.cotizacion.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Cotizacion', null, {
			'size': 'modal-lg'
		});
	}

	function usuarioShow(id){
		var url = '{{ route('admin.usuario.show', ['idUsuario']) }}';
		url = url.replace('idUsuario', id);
		var modal = openModal(url, 'Usuario', null, {
			'size': 'modal-lg'
		});
	}

	// ---------- CHARTS

	function showTooltip(x, y, contents) {
		$('<div id="tooltip" class="flot-tooltip">' + contents + '</div>').css( {
			top: y - 45,
			left: x - 55
		}).appendTo("body").fadeIn(200);
	}

	function initChart(){

		if ($('#flot-chart').length !== 0) {
			var data1 = [
			];
			var data2 = [
			];
			var xLabel = [
			];

			var placeholder = document.getElementById('flot-chart');

			plot =  new Plot(placeholder, [], {
				xaxis: {  ticks:xLabel, tickDecimals: 0,
					reserveSpace: true, tickColor: COLOR_BLACK_TRANSPARENT_2 },
				yaxis: {  ticks: 10,
					reserveSpace: true, tickColor: COLOR_BLACK_TRANSPARENT_2, min: 0 },
				grid: {
					hoverable: true,
					clickable: true,
					tickColor: COLOR_BLACK_TRANSPARENT_2,
					borderWidth: 1,
					backgroundColor: COLOR_WHITE,
					borderColor: COLOR_BLACK_TRANSPARENT_2
				},
				legend: {
					labelBoxBorderColor: COLOR_BLACK_TRANSPARENT_2,
					margin: 10,
					noColumns: 1,
					show: true,
					labelFormatter: labelFormat,
				}
			});
			var previousPoint = null;

			plot.bind("plothover", function (event, pos, item) {

//			$("#x").text(pos.x.toFixed(2));
//			$("#y").text(pos.y.toFixed(2));
				if (item) {
					if (previousPoint !== item.dataIndex) {
						previousPoint = item.dataIndex;
						$("#tooltip").remove();
						var y = item.datapoint[1].toFixed(0);
						var content = item.series.label + ": <b>" + y + '</b><br>' + item.series.data[item.dataIndex][2];
						showTooltip(item.pageX, item.pageY, content);
					}
				} else {
					$("#tooltip").remove();
					previousPoint = null;
				}
				event.preventDefault();
			});

		}
	}

	function labelFormat(label, series){
		return '<input type="checkbox" value="" style="margin-left:5px;" onchange="labelChange(this)" checked/> &nbsp;<a class="linkSerie" href="javascript:;" onclick="filtrarSerie(this)">'+ label +'</a> ';
	}

	function filtrarSerie(e){
		$('.legend table input').each(function(k, v){
			$(v).prop('checked', false);
		})
		$(e).parent().find('input').prop('checked', true);
		labelChange();
	}

	function labelChange(e){
		var dsUpdate = [];
		$('.legend table input').each(function(k, v){
			if($(v).prop('checked')){
				if( dataPlot[k] ) {
					dataPlot[k].lines.show = true;
					dataPlot[k].points.show = true;
				}
			} else {
				if( dataPlot[k] ) {
					dataPlot[k].lines.show = false;
					dataPlot[k].points.show = false;
				}
			}

			plot.setData(dataPlot);
			plot.draw();
		})
	}

	function getLineasData(){

		var url = '{{ route('resource.flotchart.getOfertas') }}';
		var data = {
			fechaInicio: fechaInicio.format('YYYY/MM/DD'),
			fechaFin: fechaFin.format('YYYY/MM/DD'),
			idVendedor: $('#vendedorId').val(),
			agrupacion: $('#agrupacion').val(),
		};

		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
		];

		ajaxPost(url, data, function(data){

			data = data.data;
			values = [];
			var xvals = [];
			var noDivs = 8;
			var mod = 1;
			var tblVendedores = $('#tblVendedores');

			var inicio = data.begin;
			var fin = data.end;

			var range = fin - inicio;
			var piece = Math.ceil(range / noDivs);
			var dot = inicio;

			for(var i = 0 ; i < noDivs ; i++) {

				// if(i % mod === 0 || i === noDivs-1) {
				if(i % mod === 0) {

					var df = new Date(dot*1000);
					// var dateString = df.getDay() + '.' + df.getMonth() + '.' + df.getFullYear();
					var dateString = monthNames[df.getMonth()] + ' ' + df.getFullYear();

					xvals.push([dot, dateString]);
				} else {
					xvals.push([dot, '1']);
				}

				dot += piece;
			}
			df = new Date(fin*1000);
			dateString = monthNames[df.getMonth()] + ' ' + df.getFullYear();
			xvals.push([fin, dateString]);

			// ---------- BAR PLOT
			dataPlot = [];
			arSorted = [];
			colorsIndex = [];

			tblVendedores.html('');

			for(var i = 0 ; i < data.datos.length ; i++) {
				if(data.datos.length === 1) {
					colorLine = blueDark;
				} else {
					colorLine = getAColor();
				}

				dataPlot.push({
					color: colorLine,
					data: data.datos[i].datos,
					label: data.datos[i].nombre,
					lines: { show: true, fill:false, lineWidth: 2 },
					points: { show: true, radius: 2, fillColor: '#fff' },
					shadowSize: 0
				});

				// ----------

				arSorted.push({
					index: i,
					value: sumArray(data.datos[i].datos)
				});

				colorsIndex[data.datos[i].id] = colorLine;
			}

			plot.setData(dataPlot);
			//plot.getOptions().xaxis.ticks = xvals;
			plot.getOptions().xaxes[0].ticks = xvals;
			//console.log(plot.getOptions());
			plot.setupGrid();
			plot.draw();

			// ---------- SPARK LINES

			arSorted.sort(function (a, b) {
				return b.value - a.value; // traverse for oposite sorting
			});

			for(var i = 0 ; i < arSorted.length ; i++) {
				var tr = $('<tr>');
				var td = $('<td>').html(data.datos[arSorted[i].index].nombre);
				tr.append(td);
				var td = $('<td>').html(sumArray(data.datos[arSorted[i].index].datos));
//			var td = $('<td>').html(sumArray(data.datos[i].datos) + ' <span class="text-success"><i class="fa fa-arrow-up"></i></span>');
				tr.append(td);
				var td = $('<td>').html('<div id="sparkline-vendedor-' + data.datos[arSorted[i].index].id + '"></div>');
				tr.append(td);
				tblVendedores.append(tr);
			}

			for(var i = 0 ; i < data.datos.length ; i++) {
				colorLine = colorsIndex[data.datos[i].id];
				var values = data.datos[i].spark;
				sparkOptions.type = 'line';
				sparkOptions.height = '23px';
				sparkOptions.lineColor = colorLine;
				sparkOptions.highlightLineColor = colorLine;
				sparkOptions.highlightSpotColor = colorLine;

				var countWidth = $('#sparkline-vendedor-'+data.datos[i].id).width();
				if (countWidth >= 200) {
					sparkOptions.width = '200px';
				} else {
					sparkOptions.width = '100%';
				}

				$('#sparkline-vendedor-'+data.datos[i].id).sparkline(values, sparkOptions);
			}

		});
	}

	function sumArray(array){
		var sum = 0;
		for(i = 0; i < array.length; i++){
			sum += array[i][1];
		}
		return sum;
	}

	var colorIndex = 0;
	function getAColor() {
		var color = colorArray[colorIndex];
		colorIndex++;
		if(colorIndex >= colorArray.length) {
			colorIndex = 0;
		}
		return color;
	}

	function ofertaShow(id){
		var url = '{{ route('admin.oferta.show', ['idOferta']) }}';
		url = url.replace('idOferta', id);
		var modal = openModal(url, 'Oferta', null, {
			'size': 'modal-lg'
		});
	}

	// ----------

	var sparkOptions = {
		height: '50px',
		width: '100%',
		fillColor: 'transparent',
		lineWidth: 2,
		spotRadius: '4',
		highlightLineColor: COLOR_BLUE,
		highlightSpotColor: COLOR_BLUE,
		spotColor: false,
		minSpotColor: false,
		maxSpotColor: false
	};

	function initSingleCharts() {
		var value = [50,30,45,40];
		sparkOptions.type = 'line';
		sparkOptions.height = '23px';
		sparkOptions.lineColor = COLOR_RED;
		sparkOptions.highlightLineColor = COLOR_RED;
		sparkOptions.highlightSpotColor = COLOR_RED;

		var countWidth = $('#sparkline-unique-visitor').width();
		if (countWidth >= 200) {
			sparkOptions.width = '200px';
		} else {
			sparkOptions.width = '100%';
		}

		$('#sparkline-unique-visitor').sparkline(value, sparkOptions);
		sparkOptions.lineColor = COLOR_ORANGE;
		sparkOptions.highlightLineColor = COLOR_ORANGE;
		sparkOptions.highlightSpotColor = COLOR_ORANGE;
		$('#sparkline-bounce-rate').sparkline(value, sparkOptions);
		sparkOptions.lineColor = COLOR_GREEN;
		sparkOptions.highlightLineColor = COLOR_GREEN;
		sparkOptions.highlightSpotColor = COLOR_GREEN;
		$('#sparkline-total-page-views').sparkline(value, sparkOptions);
		sparkOptions.lineColor = COLOR_BLUE;
		sparkOptions.highlightLineColor = COLOR_BLUE;
		sparkOptions.highlightSpotColor = COLOR_BLUE;
		$('#sparkline-avg-time-on-site').sparkline(value, sparkOptions);
		sparkOptions.lineColor = COLOR_GREY;
		sparkOptions.highlightLineColor = COLOR_GREY;
		sparkOptions.highlightSpotColor = COLOR_GREY;
		$('#sparkline-new-visits').sparkline(value, sparkOptions);
		sparkOptions.lineColor = COLOR_BLACK;
		sparkOptions.highlightLineColor = COLOR_BLACK;
		sparkOptions.highlightSpotColor = COLOR_GREY;
		$('#sparkline-return-visitors').sparkline(value, sparkOptions);
	}

</script>
@endpush