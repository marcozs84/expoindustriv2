@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.imagen.index') }}">Imagen</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $imagen->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Imagen</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Imagen</h4>
		</div>
		<div class="panel panel-body">
			<form id="formImagenEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Galeria</label>
					<div class="col-8">
						{!! Form::select('idGaleria', [], '', [
							'id' => 'idGaleria',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Galeria</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idGaleria', $imagen->idGaleria, [--}}
							{{--'id' => 'idGaleria',--}}
							{{--'placeholder' => 'Id Galeria',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Titulo</label>
					<div class="col-8">
						{!! Form::text('titulo', $imagen->titulo, [
							'id' => 'titulo',
							'placeholder' => 'Titulo',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Ruta</label>
                    <div class="col-8">
                        {!! Form::textarea('ruta', $imagen->ruta, [
                            'id' => 'ruta',
                            'placeholder' => 'Ruta',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Filename</label>
					<div class="col-8">
						{!! Form::text('filename', $imagen->filename, [
							'id' => 'filename',
							'placeholder' => 'Filename',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Mimetype</label>
					<div class="col-8">
						{!! Form::text('mimetype', $imagen->mimetype, [
							'id' => 'mimetype',
							'placeholder' => 'Mimetype',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Background Color</label>
					<div class="col-8">
						{!! Form::text('backgroundColor', $imagen->backgroundColor, [
							'id' => 'backgroundColor',
							'placeholder' => 'Background Color',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::text('estado', $imagen->estado, [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fuente</label>
					<div class="col-8">
						{!! Form::text('fuente', $imagen->fuente, [
							'id' => 'fuente',
							'placeholder' => 'Fuente',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="imagenCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="imagenSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){



	$idGaleriaSearch = Search.resource('#formImagenEdit #idGaleria', {
		url: '{{ route('resource.galeria.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		defaultValue: {
			value: {{ $imagen->idGaleria }},
			text: '{{ $imagen->Galeria->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idGaleriaSearch, {{ $imagen->idGaleria }}, '{{ $imagen->Galeria->id }});



}

$( "#formImagenEdit" ).submit(function( event ) {
	imagenSave();
	event.preventDefault();
});

function imagenSave(){
	var url = '{{ route('resource.imagen.update', [$imagen->id]) }}';
	ajaxPatch(url, {
		idGaleria: 	$('#formImagenEdit #idGaleria').val(),
		titulo: 		$('#formImagenEdit #titulo').val(),
		ruta: 			$('#formImagenEdit #ruta').val(),
		filename: 		$('#formImagenEdit #filename').val(),
		mimetype: 		$('#formImagenEdit #mimetype').val(),
		backgroundColor: $('#formImagenEdit #backgroundColor').val(),
		estado: 		$('#formImagenEdit #estado').val(),
		fuente: 		$('#formImagenEdit #fuente').val()
	}, function(){
		$(document).trigger('formImagenEdit:success');
	});
}

function imagenCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formImagenEdit:cancel');
	@else
		redirect('{{ route('admin.imagen.index')  }}');
	@endif
}

</script>
@endpush

