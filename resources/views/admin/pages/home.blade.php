@extends(($isAjaxRequest == true) ? 'layouts.ajax' : 'admin.layouts.default')
{{--@extends('layouts.default')--}}

@section('title', 'Dashboard V1')

@push('css')
	<link href="/assets/plugins/jquery-jvectormap/jquery-jvectormap.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />

<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item active">Dashboard</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Dashboard <small>header small text goes here...</small></h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-red">
				<div class="stats-icon"><i class="fa fa-desktop"></i></div>
				<div class="stats-info">
					<h4>TOTAL VISITANTES</h4>
					<p>{{ $totalVisitors }}</p>
				</div>
				{{--<div class="stats-link">--}}
					{{--<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>--}}
				{{--</div>--}}
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-orange">
				<div class="stats-icon"><i class="fa fa-link"></i></div>
				<div class="stats-info">
					<h4>ANUNCIOS PUBLICADOS</h4>
					<p>{{ $totalAnuncios }}</p>
				</div>
				<div class="stats-link">
					<a href="{{ route('admin.announcementInbox.index') }}">Detalle <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-grey-darker">
				<div class="stats-icon"><i class="fa fa-users"></i></div>
				<div class="stats-info">
					<h4>PRODUCTOS EN STOCK</h4>
					<p>{{ $totalProductos }}</p>
				</div>
				<div class="stats-link">
					<a href="{{ route('admin.stock.index') }}">Detalle <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-black-lighter">
				<div class="stats-icon"><i class="fa fa-clock"></i></div>
				<div class="stats-info">
					<h4>TOTAL CLIENTES</h4>
					<p>{{ $totalClientes }}</p>
				</div>
				<div class="stats-link">
					<a href="{{ route('admin.agenda.index') }}">Detalle <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
	</div>
	<!-- end row -->
	<!-- begin row -->
	<div class="row">
		<!-- begin col-8 -->
		<div class="col-lg-8">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="index-1">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Anuncios publicados</h4>
				</div>
				<div class="panel-body">
					<div id="chart_publicaciones" class="height-sm"></div>
				</div>
			</div>
			<!-- end panel -->

			
			<!-- begin panel -->
			<div class="panel panel-inverse hide" data-sortable-id="index-4">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Quick Post</h4>
				</div>
				<div class="panel-toolbar">
					<div class="btn-group m-r-5">
						<a class="btn btn-white" href="javascript:;"><i class="fa fa-bold"></i></a>
						<a class="btn btn-white active" href="javascript:;"><i class="fa fa-italic"></i></a>
						<a class="btn btn-white" href="javascript:;"><i class="fa fa-underline"></i></a>
					</div>
					<div class="btn-group">
						<a class="btn btn-white" href="javascript:;"><i class="fa fa-align-left"></i></a>
						<a class="btn btn-white active" href="javascript:;"><i class="fa fa-align-center"></i></a>
						<a class="btn btn-white" href="javascript:;"><i class="fa fa-align-right"></i></a>
						<a class="btn btn-white" href="javascript:;"><i class="fa fa-align-justify"></i></a>
					</div>
				</div>
				<textarea class="form-control no-rounded-corner bg-silver" rows="14">Enter some comment.</textarea>
				<div class="panel-footer text-right">
					<a href="javascript:;" class="btn btn-white btn-sm">Cancel</a>
					<a href="javascript:;" class="btn btn-primary btn-sm m-l-5">Action</a>
				</div>
			</div>
			<!-- end panel -->
			
			<!-- begin panel -->
			<div class="panel panel-invers hide" data-sortable-id="index-5">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Message</h4>
				</div>
				<div class="panel-body">
					<div class="height-sm" data-scrollbar="true">
						<ul class="media-list media-list-with-divider media-messaging">
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-5.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<h5 class="media-heading">John Doe</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id nunc non eros fermentum vestibulum ut id felis. Nunc molestie libero eget urna aliquet, vitae laoreet felis ultricies. Fusce sit amet massa malesuada, tincidunt augue vitae, gravida felis.</p>
								</div>
							</li>
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-6.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<h5 class="media-heading">Terry Ng</h5>
									<p>Sed in ante vel ipsum tristique euismod posuere eget nulla. Quisque ante sem, scelerisque iaculis interdum quis, eleifend id mi. Fusce congue leo nec mauris malesuada, id scelerisque sapien ultricies.</p>
								</div>
							</li>
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-8.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<h5 class="media-heading">Fiona Log</h5>
									<p>Pellentesque dictum in tortor ac blandit. Nulla rutrum eu leo vulputate ornare. Nulla a semper mi, ac lacinia sapien. Sed volutpat ornare eros, vel semper sem sagittis in. Quisque risus ipsum, iaculis quis cursus eu, tristique sed nulla.</p>
								</div>
							</li>
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-7.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<h5 class="media-heading">John Doe</h5>
									<p>Morbi molestie lorem quis accumsan elementum. Morbi condimentum nisl iaculis, laoreet risus sed, porta neque. Proin mi leo, dapibus at ligula a, aliquam consectetur metus.</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="panel-footer">
					<form>
						<div class="input-group">
							<input type="text" class="form-control bg-silver" placeholder="Enter message" />
							<span class="input-group-append">
								<button class="btn btn-primary" type="button"><i class="fa fa-pencil-alt"></i></button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-8 -->
		<!-- begin col-4 -->
		<div class="col-lg-4">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="index-6">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Anuncios más visitados</h4>
				</div>
				<div class="panel-body p-t-0" style="height: 330px; overflow: scroll;">
					<div class="table-responsive">
						<table class="table table-valign-middle">
							<thead>
								<tr>	
									<th>Maquina</th>
									<th>Visitas</th>
									<th></th>
									{{--<th>Imágenes</th>--}}
								</tr>
							</thead>
							<tbody>

								@foreach($masVisitados as $pub)
									<tr>
										<td><a href="javascript:;" onclick="productoShow({{ $pub->Producto->id }})">{{ $pub->Producto->nombre_alter }}</a></td>
										<td>{{ $pub->visitas }} <span class="text-success">
												{{--<i class="fa fa-arrow-up"></i></span>--}}
										</td>
										<td><a href="{{ route('public.producto', [$pub->id]) }}" target="_blank">Ir al anuncio</a></td>
										{{--<td>--}}
											{{--@if($pub->Producto->Galerias->count() > 0)--}}
												{{--@foreach($pub->Producto->Galerias->first()->Imagenes as $imagen)--}}
													{{--<a href="/pubimgs/{{ $imagen->filename }}" data-lightbox="gallery-group-{{ $pub->id }}">--}}
														{{--<img src="/pubimgs/{{ $imagen->filename }}" alt="" style="max-width:30px;" />--}}
													{{--</a>--}}
												{{--@endforeach--}}
											{{--@endif--}}
										{{--</td>--}}
									</tr>
								@endforeach

								{{--<tr>--}}
									{{--<td><label class="label label-danger">Suscriptores</label></td>--}}
									{{--<td>13,203 <span class="text-success"><i class="fa fa-arrow-up"></i></span></td>--}}
									{{--<td><div id="sparkline-unique-visitor"></div></td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td><label class="label label-warning">Visitas</label></td>--}}
									{{--<td>28.2%</td>--}}
									{{--<td><div id="sparkline-bounce-rate"></div></td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td><label class="label label-success">Articulos visitados</label></td>--}}
									{{--<td>1,230,030</td>--}}
									{{--<td><div id="sparkline-total-page-views"></div></td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td><label class="label label-primary">Avg Time On Site</label></td>--}}
									{{--<td>00:03:45</td>--}}
									{{--<td><div id="sparkline-avg-time-on-site"></div></td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td><label class="label label-default">% New Visits</label></td>--}}
									{{--<td>40.5%</td>--}}
									{{--<td><div id="sparkline-new-visits"></div></td>--}}
								{{--</tr>--}}
								{{--<tr>--}}
									{{--<td><label class="label label-inverse">Return Visitors</label></td>--}}
									{{--<td>73.4%</td>--}}
									{{--<td><div id="sparkline-return-visitors"></div></td>--}}
								{{--</tr>--}}
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- end panel -->
			
			<!-- begin panel -->
			<div class="panel panel-inverse hide" data-sortable-id="index-7">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Visitors User Agent</h4>
				</div>
				<div class="panel-body">
					<div id="donut-chart" class="height-sm"></div>
				</div>
			</div>
			<!-- end panel -->
			
			<!-- begin panel -->
			<div class="panel panel-inverse hide" data-sortable-id="index-8">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Todo List</h4>
				</div>
				<div class="panel-body p-0">
					<ul class="todolist">
						<li class="active">
							<a href="javascript:;" class="todolist-container active" data-click="todolist">
								<div class="todolist-input"><i class="fa fa-square"></i></div>
								<div class="todolist-title">Donec vehicula pretium nisl, id lacinia nisl tincidunt id.</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" class="todolist-container" data-click="todolist">
								<div class="todolist-input"><i class="fa fa-square"></i></div>
								<div class="todolist-title">Duis a ullamcorper massa.</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" class="todolist-container" data-click="todolist">
								<div class="todolist-input"><i class="fa fa-square"></i></div>
								<div class="todolist-title">Phasellus bibendum, odio nec vestibulum ullamcorper.</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" class="todolist-container" data-click="todolist">
								<div class="todolist-input"><i class="fa fa-square"></i></div>
								<div class="todolist-title">Duis pharetra mi sit amet dictum congue.</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" class="todolist-container" data-click="todolist">
								<div class="todolist-input"><i class="fa fa-square"></i></div>
								<div class="todolist-title">Duis pharetra mi sit amet dictum congue.</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" class="todolist-container" data-click="todolist">
								<div class="todolist-input"><i class="fa fa-square"></i></div>
								<div class="todolist-title">Phasellus bibendum, odio nec vestibulum ullamcorper.</div>
							</a>
						</li>
						<li>
							<a href="javascript:;" class="todolist-container active" data-click="todolist">
								<div class="todolist-input"><i class="fa fa-square"></i></div>
								<div class="todolist-title">Donec vehicula pretium nisl, id lacinia nisl tincidunt id.</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- end panel -->
			
			<!-- begin panel -->
			<div class="panel panel-inverse hide" data-sortable-id="index-9">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">World Visitors</h4>
				</div>
				<div class="panel-body p-0">
					<div id="world-map" class="height-sm width-full"></div>
				</div>
			</div>
			<!-- end panel -->
			

		</div>
		<!-- end col-4 -->
	</div>
	<!-- end row -->

	<div class="row">
		<div class="col-lg-6">


			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="index-1">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">
						{{--Ordenes ejecutadas (Last 7 Days)--}}
						Ofertas registradas
					</h4>
				</div>
				<div class="panel-body">
					<div id="chart_ofertas" class="height-sm"></div>
				</div>
			</div>
			<!-- end panel -->

		</div>
		<div class="col-lg-6">

			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="index-1">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Nuevos clientes suscritos</h4>
				</div>
				<div class="panel-body">
					<div id="chart_subscribers" class="height-sm"></div>
				</div>
			</div>
			<!-- end panel -->

		</div>
	</div>

	<div class="row">
		<div class="col-lg-8">
			<!-- begin tabs -->
			<ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
				<li class="nav-item"><a href="#latest-post" data-toggle="tab" class="nav-link active"><i class="fa fa-camera fa-lg m-r-5"></i> <span class="d-none d-md-inline">Anuncios mas recientes (10)</span></a></li>
				<li class="nav-item"><a href="#purchase" data-toggle="tab" class="nav-link"><i class="fa fa-archive fa-lg m-r-5"></i> <span class="d-none d-md-inline">Purchase</span></a></li>
				<li class="nav-item"><a href="#email" data-toggle="tab" class="nav-link"><i class="fa fa-envelope fa-lg m-r-5"></i> <span class="d-none d-md-inline">Email</span></a></li>
			</ul>
			<div class="tab-content" data-sortable-id="index-3">
				<div class="tab-pane fade active show" id="latest-post">
					<div class="height-sm" data-scrollbar="true">

						@foreach($masRecientes as $pub)
							<div style="widows: 100%;" class="m-b-15">
								<h4 style="display:block; clear:both;"><a href="javascript:;" onclick="productoShow({{ $pub->Producto->id }})">{{ $pub->Producto->nombre_alter }} </a>
									<small><a href="{{ route('public.producto', [$pub->id]) }}" target="_blank">Ir al anuncio</a></small>
								</h4>

								@if($pub->Producto->Galerias->count() > 0)
									@foreach($pub->Producto->Galerias->first()->Imagenes as $imagen)
										{{--<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-{{ $pub->id }}">--}}
											{{--<img src="{{ $imagen->ruta_publica_producto_thumb }}" class="m-b-5" alt="" style="max-height: 80px;" />--}}
										{{--</a>--}}
									@endforeach
								@endif
							</div>
							<hr>
						@endforeach

						<ul class="media-list media-list-with-divider hide">
							<li class="media media-lg">
								<a href="javascript:;" class="pull-left">
								</a>
								<div class="media-body">
									<h4 class="media-heading">Somatec sks1330</h4>
									<div class="product-info">
										<!-- BEGIN product-info-list -->

										<ul class="fa-ul">
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Tipo</b>:
												Torno paralelo																	</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Estado</b>:
												Operativa																	</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Entre punto</b>:
												600
											</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Volteo</b>:
												315
											</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Escote</b>:
												No																	</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>País de fabricación</b>:
												Francia
											</li>
										</ul>
										<!-- END product-info-list -->

										<!-- END product-social -->
									</div>
								</div>
							</li>
							<li class="media media-lg">
								<a href="javascript:;" class="pull-left">
								</a>
								<div class="media-body">
									<h4 class="media-heading">Esab LHF</h4>
									<div class="product-info">
										<!-- BEGIN product-info-list -->

										<ul class="fa-ul">
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Tipo</b>:
												Torno paralelo																	</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Estado</b>:
												Operativa																	</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Entre punto</b>:
												600
											</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Volteo</b>:
												315
											</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>Escote</b>:
												No																	</li>
											<li>
												<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
												<b>País de fabricación</b>:
												Francia
											</li>
										</ul>
										<!-- END product-info-list -->

										<!-- END product-social -->
									</div>
								</div>
							</li>
							<li class="media media-lg">
								<a href="javascript:;" class="pull-left">
									<img class="media-object" src="/assets/img/gallery/gallery-7.jpg" alt="" />
								</a>
								<div class="media-body">
									<h4 class="media-heading">Maecenas eget turpis luctus, scelerisque arcu id, iaculis urna. Interdum et malesuada fames ac ante ipsum primis in faucibus.</h4>
									Morbi placerat est nec pharetra placerat. Ut laoreet nunc accumsan orci aliquam accumsan. Maecenas volutpat dolor vitae sapien ultricies fringilla. Suspendisse vitae orci sed nibh ultrices tristique. Aenean in ante eget urna semper imperdiet. Pellentesque sagittis a nulla at scelerisque. Nam augue nulla, accumsan quis nisi a, facilisis eleifend nulla. Praesent aliquet odio non imperdiet fringilla. Morbi a porta nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.
								</div>
							</li>
							<li class="media media-lg">
								<a href="javascript:;" class="pull-left">
									<img class="media-object" src="/assets/img/gallery/gallery-8.jpg" alt="" />
								</a>
								<div class="media-body">
									<h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec auctor accumsan rutrum.</h4>
									Fusce augue diam, vestibulum a mattis sit amet, vehicula eu ipsum. Vestibulum eu mi nec purus tempor consequat. Vestibulum porta non mi quis cursus. Fusce vulputate cursus magna, tincidunt sodales ipsum lobortis tincidunt. Mauris quis lorem ligula. Morbi placerat est nec pharetra placerat. Ut laoreet nunc accumsan orci aliquam accumsan. Maecenas volutpat dolor vitae sapien ultricies fringilla. Suspendisse vitae orci sed nibh ultrices tristique. Aenean in ante eget urna semper imperdiet. Pellentesque sagittis a nulla at scelerisque.
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="tab-pane fade" id="purchase">
					<div class="height-sm" data-scrollbar="true">
						<table class="table">
							<thead>
							<tr>
								<th>Date</th>
								<th class="hidden-sm">Product</th>
								<th></th>
								<th>Amount</th>
								<th>User</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>13/02/2013</td>
								<td class="hidden-sm">
									<a href="javascript:;">
									</a>
								</td>
								<td class="text-nowrap">
									<h6><a href="javascript:;">Nunc eleifend lorem eu velit eleifend, <br />eget faucibus nibh placerat.</a></h6>
								</td>
								<td>$349.00</td>
								<td class="text-nowrap"><a href="javascript:;">Derick Wong</a></td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane fade" id="email">
					<div class="height-sm" data-scrollbar="true">
						<ul class="media-list media-list-with-divider">
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-1.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<a href="javascript:;"><h4 class="media-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4></a>
									<p class="m-b-5">
										Aenean mollis arcu sed turpis accumsan dignissim. Etiam vel tortor at risus tristique convallis. Donec adipiscing euismod arcu id euismod. Suspendisse potenti. Aliquam lacinia sapien ac urna placerat, eu interdum mauris viverra.
									</p>
									<i class="text-muted">Received on 04/16/2013, 12.39pm</i>
								</div>
							</li>
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-2.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<a href="javascript:;"><h4 class="media-heading">Praesent et sem porta leo tempus tincidunt eleifend et arcu.</h4></a>
									<p class="m-b-5">
										Proin adipiscing dui nulla. Duis pharetra vel sem ac adipiscing. Vestibulum ut porta leo. Pellentesque orci neque, tempor ornare purus nec, fringilla venenatis elit. Duis at est non nisl dapibus lacinia.
									</p>
									<i class="text-muted">Received on 04/16/2013, 12.39pm</i>
								</div>
							</li>
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-3.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<a href="javascript:;"><h4 class="media-heading">Ut mi eros, varius nec mi vel, consectetur convallis diam.</h4></a>
									<p class="m-b-5">
										Ut mi eros, varius nec mi vel, consectetur convallis diam. Nullam eget hendrerit eros. Duis lacinia condimentum justo at ultrices. Phasellus sapien arcu, fringilla eu pulvinar id, mattis quis mauris.
									</p>
									<i class="text-muted">Received on 04/16/2013, 12.39pm</i>
								</div>
							</li>
							<li class="media media-sm">
								<a href="javascript:;" class="pull-left">
									<img src="/assets/img/user/user-4.jpg" alt="" class="media-object rounded-corner" />
								</a>
								<div class="media-body">
									<a href="javascript:;"><h4 class="media-heading">Aliquam nec dolor vel nisl dictum ullamcorper.</h4></a>
									<p class="m-b-5">
										Aliquam nec dolor vel nisl dictum ullamcorper. Duis vel magna enim. Aenean volutpat a dui vitae pulvinar. Nullam ligula mauris, dictum eu ullamcorper quis, lacinia nec mauris.
									</p>
									<i class="text-muted">Received on 04/16/2013, 12.39pm</i>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- end tabs -->
		</div>
		<div class="col-lg-4">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="index-10">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Calendar</h4>
				</div>
				<div class="panel-body">
					<div id="datepicker-inline" class="datepicker-full-width overflow-y-scroll position-relative"><div></div></div>
				</div>
			</div>
			<!-- end panel -->
		</div>
	</div>
@endsection

@push('scripts')
	<script src="/assets/plugins/flot/dom-tools.js"></script>
    <script src="/assets/plugins/flot/EventEmitter.js"></script>
	<script src="/assets/plugins/flot/flot.js"></script>
	<script src="/assets/plugins/flot/flot.time.js"></script>
	<script src="/assets/plugins/flot/flot.pie.js"></script>
	<script src="/assets/plugins/gritter/js/jquery.gritter.min.js"></script>
	<script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
	<script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="/assets/js/demo/dashboard-v1.js"></script>


<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script>

	var url_flotchart_ofertas           = '{{ route('resource.flotchart.getOfertas') }}';
	var url_flotchart_publicaciones     = '{{ route('resource.flotchart.getPublicaciones') }}';
	var url_flotchart_subscribers       = '{{ route('resource.flotchart.getSubscribers') }}';

	$(document).ready(function() {
		Dashboard.init();
	});

	function productoShow(id){
		var url = '{{ route('admin.producto.show', ['idProducto']) }}';
		url = url.replace('idProducto', id);
		var modal = openModal(url, 'Producto', null, {
			'size': 'modal-lg'
		});
	}

</script>
@endpush
