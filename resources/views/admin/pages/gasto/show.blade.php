@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<style>
	.files-holder {
		background-color:#d9e0e7;
		padding:15px;
		margin:5px;

		-ms-word-break: break-all;
		word-break: break-all;
		word-break: break-word;
	}
	.fileIcon {
		text-align:center;
		font-size:11px;
	}
	.fileIcon a {
		/*font-weight:bold;*/
		color:darkblue !important;
	}
	.fileIcon i {
		margin:auto;
		margin-bottom:10px;
	}
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Gastos</li>
	</ol>

	<h1 class="page-header">Nuevo Gasto</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Gasto</h4>
		</div>
		<div class="panel panel-body">
			<form id="formGastoEdit" method="post" class="form-horizontal">

				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-3">Vendedor</label>
					<div class="col-sm-8">
						{{ $gasto->Usuario->Owner->Agenda->nombres_apellidos }}
					</div>
				</div>

				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-3">Tipo</label>
					<div class="col-sm-8">
						{!! Form::select('idGastoTipo', $gasto_tipos, $gasto->idGastoTipo, [
							'id' => 'idGastoTipo',
							'class' => 'form-control default-select2',
							'autofocus',
							'disabled'
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-3">Monto</label>
					<div class="col-sm-8">
						{{ $gasto->monto }} {{ $gasto->moneda }}
					</div>
				</div>
				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-3">Fecha </label>
					<div class="col-sm-8">
						{{  $gasto->fecha->format('d/m/Y H:i:s') }}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Descripcion</label>
					<div class="col-md-10">
						{{ $gasto->descripcion }}
					</div>
				</div>

				<div class="form-group row m-b-15">

					<h4 class="col-md-12">Archivos adjuntos</h4>

					<div class="col-md-12">
						<div class="row files-holder">
							@foreach($gasto->Archivos as $archivo)

								<div class="col-md-2 fileIcon">
									@if(pathinfo($archivo->ruta, PATHINFO_EXTENSION) == 'pdf')
										<i class="fa fa-file-pdf fa-4x"></i>
									@else
										<i class="fa fa-file-alt fa-4x"></i>
									@endif
									<br>
									<a target="_blank" href="{{ route('resource.archivo.stream', [$archivo->id]) }}">{{ $archivo->realname }}</a>
								</div>
							@endforeach
						</div>
					</div>
				</div>

				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="gastoCancelar()">Cerrar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>

	@if($isAjaxRequest)
		formInit();
	@else
		$(function(){ formInit(); });
	@endif

$( "#formGastoEdit" ).submit(function( event ) {
		gastoGuardar();
		event.preventDefault();
	});


	function gastoCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formGastoEdit:cancelar');
		@else
			redirect('{{ route('admin.gasto.index')  }}');
		@endif
	}

	function formInit(){

	}

</script>
@endpush

