@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<style>
	.files-holder {
		background-color:#d9e0e7;
		padding:15px;
		margin:5px;

		-ms-word-break: break-all;
		word-break: break-all;
		word-break: break-word;
	}
	.fileIcon {
		text-align:center;
		font-size:11px;
	}
	.fileIcon a {
		/*font-weight:bold;*/
		color:darkblue !important;
	}
	.fileIcon i {
		margin:auto;
		margin-bottom:10px;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Gastos</li>
	</ol>

	<h1 class="page-header">Nuevo Gasto</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Gasto</h4>
		</div>
		<div class="panel panel-body">
			<div id="formGastoEdit" class="form-horizontal">

				@if($usuario->isAdmin())
					<div class="form-group col-md-12 row m-b-15">
						<label class="col-form-label col-md-4">Vendedor</label>
						<div class="col-sm-8">
							{!! Form::select('idVendedor', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id'), $gasto->idUsuario, [
								'id' => 'idVendedor',
								'class' => 'form-control default-select2',
								'autofocus'
							]) !!}
						</div>
					</div>
				@else
					<div class="form-group col-md-12 row m-b-15">
						<label class="col-form-label col-md-4">Vendedor</label>
						<div class="col-sm-8">
							{!! Form::text('noiditem', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id')->toArray()[$idSelectedVendedor], [
							'id' => 'noiditem',
							'class' => 'form-control',
							'disabled'
						]) !!}
						</div>
					</div>
				@endif



				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-4">Tipo</label>
					<div class="col-sm-8">
						{!! Form::select('idGastoTipo', $gasto_tipos, $gasto->idGastoTipo, [
							'id' => 'idGastoTipo',
							'class' => 'form-control default-select2',
							'autofocus'
						]) !!}
						@can('admin.gasto.adminTipos')
							<br><br>
							<a href="javascript:;" onclick="tipoGastoEdit()">Administrar tipos de gasto</a>
						@endcan
					</div>
				</div>
				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-4">Monto</label>
					<div class="col-sm-8">
						{!! Form::text('monto', $gasto->monto, [
							'id' => 'monto',
							'placeholder' => '0.00',
							'class' => 'form-control default-select2',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-4">Moneda</label>
					<div class="col-sm-8">
						{!! Form::select('moneda', $moneda, $gasto->moneda, [
							'id' => 'moneda',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 row m-b-15">
					<label class="col-form-label col-md-4">Fecha de gasto</label>
					<div class="col-sm-8">
						{!! Form::text('fecha', $gasto->fecha->format('d/m/Y'), [
							'id' => 'fecha',
							'placeholder' => '',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Descripcion</label>
					<div class="col-md-10">
						{!! Form::textarea('descripcion', $gasto->descripcion, [
							'id' => 'descripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>


				<h4 class="col-md-12">Archivos adjuntos</h4>

				<div class="col-md-12">
					<div class="row files-holder">
						@foreach($gasto->Archivos as $archivo)

							<div class="col-md-2 fileIcon">
								@if(pathinfo($archivo->ruta, PATHINFO_EXTENSION) == 'pdf')
									<i class="fa fa-file-pdf fa-4x"></i>
								@else
									<i class="fa fa-file-alt fa-4x"></i>
								@endif
								<br>
								<a target="_blank" href="{{ route('resource.archivo.stream', [$archivo->id]) }}">{{ $archivo->realname }}</a>
							</div>
						@endforeach
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Fotografía / Captura</label>
					<div class="col-md-12">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
						{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
						{{--<div class="form-group">--}}
						{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
						{{--</div>--}}
						{{--<div class="form-group">--}}
						{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
						{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.bannerSuperior.upload') }}" class="dropzone">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="sesna" value="{{ rand(1,1000) }}">
							<input type="hidden" name="gasto_id" id="gasto_id" value="{{ $gasto->id }}">
							<div class="dz-message" data-dz-message><span>
							{{--@lang('messages.dz_upload')--}}
									Adjuntar comprobante
						</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor aguarde hasta que el archivo termine de subir.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>

				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="gastoCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="gastoGuardar()" >Guardar</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>

	@if($isAjaxRequest)
		formInit();
	@else
		$(function(){ formInit(); });
	@endif

	var myDropzone = '';
	$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
		Dropzone.autoDiscover = false;
		setTimeout(function(){
			{{--$('div#dropzone').dropzone({--}}
			{{--url: '{{ route('admin.bannerSuperior.upload') }}'--}}
			{{--});--}}  // comented so it fixes the already attached dropzone

			myDropzone = new Dropzone(".dropzone", {
				url: "{{ route('admin.gasto.upload') }}",
				addRemoveLinks: true,
				autoProcessQueue: false,
				parallelUploads: 1,
				init: function() {

					this.on("queuecomplete", function(file) {
						$(document).trigger('formGastoEdit:aceptar');
					});
					this.on("removedfile", function(file) {
						//alert(file.name);
						console.log('Eliminado: ' + file.name);

						file.previewElement.remove();

						// Create the remove button
						var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

						//Capture the Dropzone instance as closure.
						var _this = this;

						// Listen to the click event
						//					removeButton.addEventListener();

						// Add the button to the file preview element.
						file.previewElement.appendChild(removeButton);
					});
				},
				success: function(file, response){
					console.log("success");
					console.log(file.name);
					console.log(response);
					myDropzone.processQueue();
//					$(document).trigger('formGastoCreate:aceptar');
				},
				removedfile: function(file){
					x = confirm('@lang('messages.confirm_removal')');
					if(!x) return false;
				}
			});
		}, 1000);

		$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
		$("[data-toggle=tooltip]").tooltip();

	});

$( "#formGastoEdit" ).submit(function( event ) {
		gastoGuardar();
		event.preventDefault();
	});

	function gastoGuardar(){
		var url = '{{ route('resource.gasto.update', [$gasto->id]) }}';
		ajaxPatch(url, {
			@if($usuario->isAdmin())
				idUsuario: $('#formGastoEdit #idVendedor').val(),
			@else
				idUsuario: {{ $gasto->idUsuario }},
			@endif
			idGastoTipo: $('#formGastoEdit #idGastoTipo').val(),
			fecha: $('#formGastoEdit #fecha').val(),
			monto: $('#formGastoEdit #monto').val(),
			moneda: $('#formGastoEdit #moneda').val(),
			descripcion: $('#formGastoEdit #descripcion').val()
		}, function(data){
//			$('#gasto_id').val(data.data.id);

			if(myDropzone.files.length === 0) {
				$(document).trigger('formGastoEdit:aceptar');
			} else {
				myDropzone.processQueue();
			}

		});
	}

	function gastoCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formGastoEdit:cancelar');
		@else
			redirect('{{ route('admin.gasto.index')  }}');
		@endif
	}

	function formInit(){
		$('#formGastoEdit #idVendedor, #formGastoEdit #idGastoTipo, #formGastoEdit #moneda').select2();

		$('#formGastoEdit #fecha').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			todayHighlight: true,
			orientation: 'bottom'
		})
			.on('changeDate', function(){

			});
		$("#formGastoEdit #fecha").mask("99/99/9999");
	}

</script>
@endpush

