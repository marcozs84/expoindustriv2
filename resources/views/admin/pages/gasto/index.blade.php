@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')


@push('css')
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
	
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
	<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
<style>
	.col-mz{
		/*max-width:150px !important;*/
		word-wrap: break-word !important;
		white-space:pre-wrap !important;
        text-overflow: ellipsis !important;
	}
	/*td {word-wrap: break-word !important;}*/
</style>
@endpush

@section('content')

<ol class="breadcrumb pull-right">
	<li class="breadcrumb-item "><a href="{{ route('admin.home') }}">Home</a></li>
	<li class="breadcrumb-item active">Gastos</li>
</ol>

<h1 class="page-header">Gastos <small></small></h1>

<div class="panel panel-inverse">
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		</div>
		<h4 class="panel-title">Búsqueda</h4>
	</div>
	<div id="formSearchGasto" class="panel-body p-b-0">
		<div class="row" id="searchForm">
			<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
				<label for="periodoBusqueda">Periodo</label>
				<div style="width:100%;">
					<div id="periodoBusqueda" class="btn btn-default btn-block text-left f-s-12">
						<i class="fa fa-caret-down pull-right m-t-2"></i>
						<span></span>
					</div>
				</div>
			</div>

			<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
				<label for="idUsuario" style="">Vendedor</label>
				{!! Form::select('idUsuario', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id'), null, [
					'id' => 'idUsuario',
					'class' => 'form-control multiple-select2',
					'style' => 'width:100%;',
					'multiple' => 'multiple'
				]) !!}
			</div>
			<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

				{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="gastoCrear()"><i class="fa fa-plus"></i> Crear</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="gastoEliminar()"><i class="fa fa-trash"></i> Eliminar</a>

				<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:;" onclick="buscar()"><i class="fa fa-search"></i> Buscar</a>
				<a class="btn btn-sm btn-primary pull-right" href="javascript:;" onclick="limpiar()">Limpiar</a>

				{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

			</div>
		</div>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Gastos</h4>
            </div>
            <div class="panel-body">

				<div class="form-group">


				</div>

                <table id="data-table" class="table table-striped table-bordered width-full table-condensed"></table>

            </div>
        </div>
    </div>
</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>--}}

<!-- ================== END PAGE LEVEL JS ================== -->
<script>

var datatable = null;
var modal = null;

var originalInicio = moment().subtract(1, 'month').startOf('month');
//var originalInicio = moment().subtract(1, 'week');
var originalFin = moment();

var fechaInicio = originalInicio;
var fechaFin = originalFin;

$(document).ready(function(){
	initSearchForm();
	initDataTable();
});

function initSearchForm() {

	$('#idUsuario').select2()
		.on('select2:select', function (e) {
			datatable.ajax.reload();
		})
		.on('select2:unselect', function (e) {
			datatable.ajax.reload();
		});

	$('#formSearchGasto #periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
	$('#formSearchGasto #periodoBusqueda').daterangepicker({
		format: 'DD/MM/YYYY',
		startDate: moment(),
		endDate: moment().add(6, 'days'),
		minDate: '01/01/2018',
		maxDate: '31/12/{{ date('Y') + 1 }}',
		dateLimit: { months: 36 },
		showDropdowns: true,
		showWeekNumbers: true,
		timePicker: false,
		timePickerIncrement: 1,
		timePicker12Hour: true,
		linkedCalendars: false,
		ranges: {
			'Hoy día': [moment(), moment()],
			'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Esta semana': [moment().startOf('week'), moment().endOf('week')],
			'Semana anterior': [moment().subtract(1, 'weeks').startOf('week'), moment().subtract(1, 'weeks').endOf('week')],
			'Últimos 7 días': [moment().subtract(1, 'weeks'), moment()],
			'Últimas 2 semanas': [moment().subtract(2, 'weeks'), moment()],
			'Últimos 30 días': [moment().subtract(1, 'month'), moment()],
			'Este més': [moment().startOf('month'), moment().endOf('month')],
			'Més anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			'Últimos 3 meses': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')]

		},
		opens: 'right',
		drops: 'down',
		buttonClasses: ['btn', 'btn-sm'],
		applyClass: 'btn-primary',
		cancelClass: 'btn-default',
		separator: ' a ',
//			singleDatePicker: true,
		locale: {
			applyLabel: 'Aceptar',
			cancelLabel: 'Cancelar',
			fromLabel: 'From',
			toLabel: 'To',
			customRangeLabel: 'Personalizado',
			daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			firstDay: 1
		}
	}, function(start, end, label) {
		$('#periodoBusqueda span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		fechaInicio = start;
		fechaFin = end;
		datatable.ajax.reload();
	});
}

function initDataTable(){
	if (!$.fn.DataTable.isDataTable('#data-table')) {
	    datatable = $('#data-table').DataTable({
	        lengthMenu: [5, 10, 20, 30],
	        pageLength: 20,
	        buttons: [
	            {
	                text: 'Nuevo',
	                action: function(e, dt, node, config){
	                    alert('Button activated');
	                }
	            }
	        ],
	        ajax: {
	            url: '{{ route('resource.gasto.ds') }}',
	            dataSrc: 'data',
				method: 'POST',
	            'headers': {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
	            data: function (d){ // Procesa datos enviados
//	                d.element = $('#element').val();
		            d.fechaInicio = fechaInicio.format('YYYY-M-D');
		            d.fechaFin = fechaFin.format('YYYY-M-D');
		            d.idUsuario = $('#idUsuario').val();
	                // ...
	            },
	            dataFilter: function(data){ // Procesa datos recibidos
	                return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
	            }
	        },
	        columns: [
				{ data: "id", title: 'Id' },
				{ data: "usuario.owner.agenda.nombres_apellidos", title: 'Usuario'},
				{ data: "monto", title: 'Monto'},
				{ title: 'Tipo'},
				{ data: "descripcion", title: 'Descripcion'},
				{ data: "fecha", title: 'Fecha'},
				{ title: ''},
				{ title: '<input type="checkbox" id="idsChecker" value="" />'},
	            // ...
	        ],
	        columnDefs: [
	            {
	                targets: 0,
	                className: 'text-center',
	                width: '20px',
	            },{
	                targets: 1,
			        width: '200px',
	                render: function(data, type, row){
	                    return '<a href="javascript:;" onclick="gastoShow('+row.id+')">'+ data +'</a>';
	                }
	            },{
	                targets: 2,
			        width: '50px',
	                render: function(data, type, row){
	                    return data + ' ' + ( row.moneda ? row.moneda : '' );
	                }
	            },{
	                targets: 3,
	                render: function(data, type, row){
	                    return row.gasto_tipo.texto ;
	                }
	            },{
	                targets: 4,
	                render: function(data, type, row){
	                    return ( row.archivos && row.archivos.length > 0 ? '<i class="fa fa-paperclip"></i>' : '' ) + ' ' +  data ;
	                }
	            },{
	                targets: 5,
			        width: '100px',
	                render: function(data, type, row){
	                    return format_datetime(data);
	                }
	            },{
	                targets: 6,
			        width: '30px',
	                className: 'with-btn',
	                render: function(data, type, row){
		                var html = '';
		                html += '<a href="javascript:;" onclick="gastoEditar('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
		                return html;
	                }
	            }, {
	                targets: 7,
	                className: 'text-center',
	                width: '80px',
	                render: function(data, type, row){
	                    return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
	                }
	            }
	        ],
	        fixedHeader: {
	            header: true,
	            headerOffset: $('#header').height()
	        },
	        bSort : false,
	        bAutoWidth: false,
	        processing: true,
	        serverSide: true,
	        responsive: true,
	        fixedColumns: true
	    }).on('draw.dt', function(){
            $('#idsChecker').on('change', function(e){
                $('.chkId').prop('checked', $(this).prop('checked'));
            });
	    });
	} else {
	    datatable.ajax.reload();
	}

}

function buscar() {
	datatable.ajax.reload();
}

function limpiar() {
	$('#idUsuario').val([]).trigger('change');
	fechaInicio = originalInicio;
	fechaFin = originalFin;
	$('#periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
	datatable.ajax.reload();
}

function gastoCrear(){
    var url = '{{ route('admin.gasto.create') }}';
    var modal = openModal(url, 'Nuevo Gasto', undefined, { size: 'modal-lg' });
    setModalHandler('formGastoCreate:aceptar', function(){
        dismissModal(modal);
        datatable.ajax.reload();
    });
}

function gastoEditar(id){
    var url = '{{ route('admin.gasto.edit', 'idGasto') }}';
    url = url.replace('idGasto', id);
    var modal = openModal(url, 'Editar Gasto', undefined, { size: 'modal-lg' });
    setModalHandler('formGastoEdit:aceptar', function(){
        dismissModal(modal);
        datatable.ajax.reload(null, false);
    });
}

function gastoShow(id){
	var url = '{{ route('admin.gasto.show', 'idGasto') }}';
	url = url.replace('idGasto', id);
    var modal = openModal(url, 'Gasto', undefined, { size: 'modal-lg' });
}

function gastoEliminar(){
	var ids = [];
    $('.chkId').each(function() {
        if($(this).prop('checked')){
            ids.push($(this).val());
        }
    });

	swal({
		text: 'Está seguro que desea eliminar los registros seleccionados?',
		icon: 'warning',
		buttons: {
			cancel: 'No',
			ok: 'Si'
		},
		dangerMode: true
	}).then(function(response) {
		if (response === 'ok') {
			var url = '{{ route('resource.gasto.destroy', [0]) }}';
			ajaxDelete(url, ids, function(){
				swal('Registros eliminados', { icon: "success" });
				datatable.ajax.reload(null, false);
			});
		}
	});


}

</script>
@endpush