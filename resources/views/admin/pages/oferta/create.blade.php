@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.oferta.index') }}">Ofertas</a></li>
		<li class="breadcrumb-item active">Registrar oferta</li>
	</ol>

	<h1 class="page-header">Registrar oferta</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Oferta</h4>
		</div>
		<div class="panel panel-body">
			<div id="formOfertaCreate" class="form-horizontal">

				<div class="row">
					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">No. Oferta</label>
						<div class="col-8">
							{!! Form::text('noOferta', $noOferta, [
								'id' => 'noOferta',
								'placeholder' => '',
								'class' => 'form-control',
								'disabled' => 'true',
								'autofocus'
							]) !!}
						</div>
					</div>

					@if($usuario->isAdmin())
						<div class="form-group col-md-6 col-sm-12 row m-b-15">
							<label class="col-form-label col-4">Vendedor</label>
							<div class="col-8">
								{!! Form::select('idVendedor', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id'), $idSelectedVendedor, [
									'id' => 'idVendedor',
									'class' => 'form-control default-select2',
									'onchange' => 'getNoOferta()',
									'autofocus'
								]) !!}
							</div>
						</div>
					@else
						<div class="form-group col-md-6 col-sm-12 row m-b-15">
							<label class="col-form-label col-4">Vendedor</label>
							<div class="col-8">
								{!! Form::text('idVendedor', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id')->toArray()[$idSelectedVendedor], [
								'id' => 'idVendedor',
								'placeholder' => '',
								'class' => 'form-control',
								'disabled'
							]) !!}
							</div>
						</div>
					@endif

					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Fecha de oferta</label>
						<div class="col-8">
							{!! Form::text('fechaOferta', '', [
								'id' => 'fechaOferta',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>
					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">No. Orden</label>
						<div class="col-8">
							{{--{!! Form::text('ofertaNombre', '', [--}}
								{{--'id' => 'ofertaNombre',--}}
								{{--'placeholder' => '',--}}
								{{--'class' => 'form-control',--}}
								{{--'autofocus'--}}
							{{--]) !!}--}}

							{!! Form::select('searchOrden', [], null, [
							'id' => 'searchOrden',
							'class' => 'default-select2 form-control',
						]); !!}

						</div>
					</div>

					<h4 class="col-md-12">Datos del cliente</h4>

					<div class="form-group col-md-6 col-sm-12 row m-b-15 group-cliente">
						<label class="col-form-label col-4">Empresa / Cliente</label>
						<div class="col-8">
							{!! Form::select('searchCliente', [], null, [
								'id' => 'searchCliente',
								'class' => 'form-control default-select2',
								'autofocus'
							]) !!}
							<a href="javascript:;" class="btn btn-primary" onclick="clienteCreate()" style="margin-top:5px;">Nuevo Cliente</a>
							<a href="javascript:;" class="btn btn-primary btnClienteEdit hide" onclick="clienteEdit()" style="margin-top:5px;">Editar Cliente</a>
						</div>
					</div>

					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Cliente nuevo</label>
						<div class="col-8">
							{!! Form::select('nuevoCliente', [
								0 => 'No',
								1 => 'Si',
								2 => 'Habitual',
							], 0, [
								'id' => 'nuevoCliente',
								'class' => 'form-control',
								//'disabled' => 'disabled',
							]) !!}
						</div>
					</div>

					<style>
						.tblProveedor tr th {
							text-align:center;
							font-size:14px;
						}
						.tblProveedor tr td {
							vertical-align: top;
							padding-left:10px;
						}
						.tblProveedor {
							width:100%
						}
						.tblLabel {
							font-weight:bold;
							text-align: right;
						}
					</style>

					<div class="col-md-12 hide p-b-10" id="pnlContacto">
						<div class="panel panel-info">
							<div class="panel-body bg-black-transparent-2 text-black" style="padding-bottom:10px;">
								<table class="tblProveedor">
									<tr>
										<th style="width:50%">Datos Contacto</th>
										<th style="width:50%">Datos Empresa</th>
									</tr>
									<tr>
										<td>
											<address>
												<strong class="pContacto"></strong><br />
												<a href="javascript:;" class="text-black"><span class="pCorreoContacto"></span></a>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos"></span>
											</address>
										</td>
										<td>
											<address>
												<strong class="pNombreProv">Twitter, Inc.</strong><br />
												<span class="pDireccion"></span>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos"></span>
											</address>
											<strong>Otros contactos:</strong><br>
											<span class="pContactos"></span>
										</td>
									</tr>
								</table>

							</div>
						</div>
					</div>

					<div class="form-group col-md-6 col-sm-12 row m-b-15 group-cliente">
						<label class="col-form-label col-4">Maquina</label>
						<div class="col-8">
							{!! Form::select('searchProducto', [], null, [
								'id' => 'searchProducto',
								'class' => 'form-control default-select2',
								'autofocus'
							]) !!}
						</div>
					</div>


					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Lugar</label>
						<div class="col-8">
							{!! Form::text('lugar', '', [
								'id' => 'lugar',
								'placeholder' => '',
								'class' => 'form-control',
								'autofocus'
							]) !!}
						</div>
					</div>
					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Teléfono</label>
						<div class="col-8">
							{!! Form::text('telefono', '', [
								'id' => 'telefono',
								'placeholder' => '',
								'class' => 'form-control',
								'autofocus'
							]) !!}
						</div>
					</div>
					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">E-mail</label>
						<div class="col-8">
							{!! Form::text('email', '', [
								'id' => 'email',
								'placeholder' => '',
								'class' => 'form-control',
								'autofocus'
							]) !!}
						</div>
					</div>
				</div>

				<div class="form-group col-12 row m-b-15">
					<div class="col-12">
						<b>Observaciones:</b><br>
						{!! Form::textarea('observaciones', '', [
							'id' => 'observaciones',
							'class' => 'form-control',
							'rows' => 3,
						]) !!}
					</div>
				</div>


				<div class="form-group col-12 row m-b-15">
					<label class="col-form-label col-md-3">Fotografía / Captura</label>
					<div class="col-md-12">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
						{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
						{{--<div class="form-group">--}}
						{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
						{{--</div>--}}
						{{--<div class="form-group">--}}
						{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
						{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.oferta.upload') }}" class="dropzone">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="sesna" value="{{ rand(1,1000) }}">
							<input type="hidden" name="oferta_id" id="oferta_id" value="0">
							<div class="dz-message" data-dz-message><span>
									{{--@lang('messages.dz_upload')--}}
									Adjuntar oferta
								</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor aguarde hasta que el archivo termine de subir.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>


				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-md-6 text-left">
						{{--<a class="btn btn-warning btn-sm" href="javascript:;" onclick="usuarioCreate()">Nuevo Cliente</a>--}}
					</div>
					<div class="col-md-6 text-right">
						<a class="btn btn-danger btn-sm" href="javascript:;" onclick="ofertaVenta()" >Marcar venta 🎉</a>
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="ofertaCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaGuardar()" >Guardar</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>

	@if($isAjaxRequest)
		formInit();
	@else
		$(function(){ formInit(); });
	@endif

	var myDropzone = '';
	$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
		Dropzone.autoDiscover = false;
		setTimeout(function(){
			{{--$('div#dropzone').dropzone({--}}
				{{--url: '{{ route('admin.bannerSuperior.upload') }}'--}}
			{{--});--}}  // comented so it fixes the already attached dropzone

			myDropzone = new Dropzone(".dropzone", {
				url: "{{ route('admin.oferta.upload') }}",
				addRemoveLinks: true,
				autoProcessQueue: false,
				parallelUploads: 1,
				init: function() {
					{{--@foreach($images as $image)--}}
						{{--var mockFile = {--}}
						{{--name: '{{ $image['filename'] }}',--}}
						{{--size: 123,--}}
						{{--};--}}
						{{--this.options.addedfile.call(this, mockFile);--}}
						{{--this.options.thumbnail.call(this, mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
						{{--this.emit('thumbnail', mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
						{{--this.emit('complete', mockFile);--}}
						{{--// Src.: https://stackoverflow.com/a/22719947;--}}
					{{--@endforeach--}}

					this.on("queuecomplete", function(file) {
						$(document).trigger('formOfertaCreate:aceptar');
					});
					this.on("removedfile", function(file) {
						//alert(file.name);
						console.log('Eliminado: ' + file.name);

						file.previewElement.remove();

						// Create the remove button
						var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

						//Capture the Dropzone instance as closure.
						var _this = this;

						// Listen to the click event
						//					removeButton.addEventListener();

						// Add the button to the file preview element.
						file.previewElement.appendChild(removeButton);
					});
				},
				success: function(file, response){
					console.log("success");
					console.log(file.name);
					console.log(response);
					myDropzone.processQueue();
//					$(document).trigger('formOfertaCreate:aceptar');
				},
				removedfile: function(file){
					x = confirm('@lang('messages.confirm_removal')');
					if(!x) return false;
//					removeImage(1, file.name);
//					for(var i = 0 ; i < file_up_names.length; i++){
//						if(file_up_names[i] === file.name){
//							ajaxPost('public.publishRemove', {
//								'archivo' : ''
//							});
//						}
//					}
				}
			});
		}, 1000);

		$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
		$("[data-toggle=tooltip]").tooltip();

	});

	$( "#formOfertaCreate" ).submit(function( event ) {
		ofertaGuardar();
		event.preventDefault();
	});

	function ofertaGuardar(){
		var url = '{{ route('resource.oferta.store') }}';
		ajaxPost(url, {
			@if($usuario->isAdmin())
			idVendedor: $('#formOfertaCreate #idVendedor').val(),
			@else
			idVendedor: {{ $idSelectedVendedor }},
			@endif
//			idVendedor: $('#formOfertaCreate #idVendedor').val(),
			fechaEnvio: $('#formOfertaCreate #fechaOferta').val(),
			idOrden: $('#formOfertaCreate #searchOrden').val(),
			idProductoItem: $('#formOfertaCreate #searchProducto').val(),
			idCliente: $('#formOfertaCreate #searchCliente').val(),
			esClienteNuevo: $('#formOfertaCreate #nuevoCliente').val(),
			lugar: $('#formOfertaCreate #lugar').val(),
			telefono: $('#formOfertaCreate #telefono').val(),
			email: $('#formOfertaCreate #email').val(),
			observaciones: $('#formOfertaCreate #observaciones').val(),
		}, function(data){

			$('#oferta_id').val(data.data.id);

			if(myDropzone.files.length === 0) {
				$(document).trigger('formOfertaCreate:aceptar');
			} else {
				myDropzone.processQueue();
			}
		});
	}

	function ofertaCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formOfertaCreate:cancelar');
		@else
			redirect('{{ route('admin.oferta.index')  }}');
		@endif
	}

	function getNoOferta() {
		var idVendedor = $('#formOfertaCreate #idVendedor').val();

		var url = '{{ route('admin.oferta.getNoOferta', ['idVendedor']) }}';
		url = url.replace('idVendedor', idVendedor);
		ajaxGet(url, function (data) {
			$('#formOfertaCreate #noOferta').val(data.data);
		})
	}

	function formInit(){

		@if($usuario->isAdmin())
			$('#formOfertaCreate #idVendedor').select2();
		@endif


		$('#formOfertaCreate #fechaOferta').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			todayHighlight: true,
		}).datepicker("setDate",'now').on('changeDate', function(){
			buscar();
		});
		$("#formOfertaCreate #fechaOferta").mask("99/99/9999");

		// ----------

		$('#formOfertaCreate #searchOrden').select2({
//        allowClear: true,
			placeholder: {
				id: "",
				placeholder: "Leave blank to ..."
			},
			ajax: {
				url: '{{ route('resource.orden.ds') }}',
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token' : '{!! csrf_token() !!}'
				},
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page
					};
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function (data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: function(repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if (repo.loading) return repo.text;
				var markup = "<div><b>" + repo.codename + '</b><br>' + repo.cliente.agenda.nombres_apellidos + "</div>";
				return markup;
			},
			templateSelection: function formatRepoSelection (repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if(repo.codename){
					return repo.codename + ' - ' + repo.cliente.agenda.nombres_apellidos;
				} else {
					return repo.text;
				}
				//return repo.nombre + ' ' + repo.apellidos || repo.text;
			},
//	    allowClear: true
		})
			.trigger('change');


		$('#formOfertaCreate #searchProducto').select2({
//        allowClear: true,
			placeholder: {
				id: "",
				placeholder: "Leave blank to ..."
			},
			ajax: {
				url: '{{ route('resource.productoItem.ds') }}',
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token' : '{!! csrf_token() !!}'
				},
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page
					};
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function (data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: function(repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if (repo.loading) return repo.text;

				var owner_pub = '';
				if(repo.producto) {
					owner_pub = repo.producto.owner.owner.agenda.nombres_apellidos;
					owner_pub += ' ( ' + repo.producto.owner.owner.proveedor.nombre + ')';
				}
				var markup = "<div><b>" + repo.producto.marca.nombre + ' ' + repo.producto.modelo.nombre + '</b><br>' + owner_pub + "</div>";
				return markup;
			},
			templateSelection: function formatRepoSelection (repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if(repo.producto){
					return repo.producto.marca.nombre + ' - ' + repo.producto.modelo.nombre;
				} else {
					return repo.text;
				}
				//return repo.nombre + ' ' + repo.apellidos || repo.text;
			},
//	    allowClear: true
		})
			.trigger('change');

		$('#formOfertaCreate #searchCliente').select2({
//        allowClear: true,
			placeholder: {
				id: "",
				placeholder: "Leave blank to ..."
			},
			ajax: {
{{--				url: '{{ route('resource.proveedorContacto.ds') }}',--}}
				url: '{{ route('resource.cliente.ds') }}',
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token' : '{!! csrf_token() !!}'
				},
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page,
						proveedor_data: 1 // Utilizado para que el 'ds' consultado retorne informacion más completa
					};
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function (data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: function(repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if (repo.loading) return repo.text;

				var companyName = '';
				if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
					companyName = '- - -';
				} else {
					companyName = repo.usuario.owner.proveedor.nombre;
				}
				var otrosContactos = [];
//				if(repo.proveedor.proveedor_contactos && repo.proveedor.proveedor_contactos.length > 0) {
//					$.each(repo.proveedor.proveedor_contactos, function(k,v) {
//						if(v.id !== repo.id) {
//							otrosContactos.push(v.agenda.nombres_apellidos);
//						}
//					});
//				}

				var markup = "<div>" + repo.agenda.nombres_apellidos + '<br><b>' + companyName + "</b><br>" + otrosContactos.join('<br> ') + "</div>";
//				var markup = "<div><b>" + repo.nombre + '</b><br>' + repo.descripcion + "</div>";
				return markup;
			},
			templateSelection: function formatRepoSelection (repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if(repo.usuario){
//					var telefonos = [];
//					if( repo.telefonos ) {
//						for(var i = 0 ; i < repo.telefonos.length ; i++) {
//							telefonos.push(repo.telefonos[i].numero);
//						}
//						$('#formOfertaCreate #lblTelefonos').html(telefonos.join(', '));
//					}
//					$('#formOfertaCreate .btnProveedorEdit').attr('onclick', 'proveedorEdit('+ repo.id +')');
//					$('#formOfertaCreate .btnProveedorEdit').removeClass('hide');

					var companyName = '';
					if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
						companyName = '- - -';
					} else {
						companyName = repo.usuario.owner.proveedor.nombre;
					}

					$('#formOfertaCreate #nuevoCliente option[value="0"]').attr('selected', 'selected');

					// ----------

					$('#formOfertaCreate .pNombreProv').html(companyName);

					var otrosContactos = [];
					if(repo.usuario.owner.proveedor.proveedor_contactos && repo.usuario.owner.proveedor.proveedor_contactos.length > 0) {
						$.each(repo.usuario.owner.proveedor.proveedor_contactos, function(k,v) {
							if(parseInt(v.id, 10) !== parseInt(repo.id, 10)) {
								otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
							}
						});
					}

					var telefonos = [];
					if(repo.usuario.owner.proveedor.telefonos && repo.usuario.owner.proveedor.telefonos.length > 0) {
						$.each(repo.usuario.owner.proveedor.telefonos, function(k,v) {
							if(v.id !== repo.id) {
								telefonos.push(v.numero);
							}
						});
					}

					var direcciones = [];
					if(repo.usuario.owner.proveedor.direcciones && repo.usuario.owner.proveedor.direcciones.length > 0) {
						$.each(repo.usuario.owner.proveedor.direcciones, function(k,v) {
							if(v.id !== repo.id) {
								direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
							}
						});
					}

					$('#formOfertaCreate .pContacto').html(repo.agenda.nombres_apellidos);
					$('#formOfertaCreate .pCorreoContacto').html(repo.usuario.username);
					$('#formOfertaCreate .pContactos').html(otrosContactos.join('<br> '));
					$('#formOfertaCreate .pCorreos').html(repo.usuario.owner.proveedor.correos);
					$('#formOfertaCreate .pTelefonos').html(telefonos.join(', '));
					$('#formOfertaCreate .pDireccion').html(direcciones.join('<br>'));

					$('#pnlContacto').removeClass('hide');

					// ----------

					return repo.agenda.nombres_apellidos + ' [' + companyName + ']';
				} else {
//					$('#formOfertaCreate .btnProveedorEdit').addClass('hide');
					return repo.text;
				}
				//return repo.nombre + ' ' + repo.apellidos || repo.text;
			},
//	    allowClear: true
		})
			.trigger('change');
	}

	function clienteCreate(){
		var url = '{{ route('admin.usuario.create', [ 'tipo' => 'cliente' ]) }}';
		var modal = openModal(url, 'Nuevo Cliente');
		setModalHandler('formUsuarioCreate:aceptar', function(event, data){
			var usuario = data;
			if(usuario.owner.proveedor.nombre === null) {
				usuario.owner.proveedor.nombre = '';
			}
			var companyName = '';
			if(usuario.owner.proveedor.nombre === '' || usuario.owner.proveedor.nombre === null) {
				companyName = '- - -';
			} else {
				companyName = usuario.owner.proveedor.nombre;
			}
			var option = $('<option>', {
				value: usuario.cliente.id,
				'selected' : 'selected'
			}).html(usuario.owner.agenda.nombres_apellidos + ' [' + companyName + ']');
			$('#formOfertaCreate #searchCliente').html('');
			$('#formOfertaCreate #searchCliente').append(option).trigger('change');

			$('#formOfertaCreate #nuevoCliente option[value="1"]').attr('selected', 'selected');

			$('#formOfertaCreate .pNombreProv').html(companyName);

			var otrosContactos = [];
			if(usuario.owner.proveedor.proveedor_contactos && usuario.owner.proveedor.proveedor_contactos.length > 0) {
				$.each(usuario.owner.proveedor.proveedor_contactos, function(k,v) {
					if(v.id !== usuario.id) {
						otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
					}
				});
			}

			var telefonos = [];
			if(usuario.owner.proveedor.telefonos && usuario.owner.proveedor.telefonos.length > 0) {
				$.each(usuario.owner.proveedor.telefonos, function(k,v) {
					if(v.id !== usuario.id) {
						telefonos.push(v.numero);
					}
				});
			}

			var direcciones = [];
			if(usuario.owner.proveedor.direcciones && usuario.owner.proveedor.direcciones.length > 0) {
				$.each(usuario.owner.proveedor.direcciones, function(k,v) {
					if(v.id !== usuario.id) {
						if( v.direccion != undefined ) {
							direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
						}
					}
				});
			}

			$('#formOfertaCreate .pContacto').html(usuario.owner.agenda.nombres_apellidos);
			$('#formOfertaCreate .pCorreoContacto').html(usuario.username);
			$('#formOfertaCreate .pContactos').html(otrosContactos.join('<br> '));
			$('#formOfertaCreate .pCorreos').html(usuario.owner.proveedor.correos);
			$('#formOfertaCreate .pTelefonos').html(telefonos.join(', '));
			$('#formOfertaCreate .pDireccion').html(direcciones.join('<br>'));

			$('#pnlContacto').removeClass('hide');

			dismissModal(modal);
		});
	}


</script>
@endpush

