@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />

<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="/home">Home</a></li>
		<li class="breadcrumb-item active">Ofertas</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Ofertas <small></small></h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Búsqueda</h4>
		</div>
		<div id="formSearchOferta" class="panel-body p-b-0">
			<div class="row" id="searchForm">
				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="periodoBusqueda">Periodo</label>
					<div style="width:100%;">
						<div id="periodoBusqueda" class="btn btn-default btn-block text-left f-s-12">
							<i class="fa fa-caret-down pull-right m-t-2"></i>
							<span></span>
						</div>
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="vendedorId" style="">Vendedor</label>
					{!! Form::select('vendedorId', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id'), null, [
						'id' => 'vendedorId',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>
				{{--<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">--}}
				{{--<label for="estado" style="">Resolución</label>--}}
				{{--{!! Form::select('resolucion', [--}}
				{{--'diario' => 'Diario',--}}
				{{--'semanal' => 'Semanal'--}}
				{{--], null, [--}}
				{{--'id' => 'resolucion',--}}
				{{--'class' => 'form-control multiple-select2',--}}
				{{--'style' => 'width:100%;'--}}
				{{--]) !!}--}}
				{{--</div>--}}
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaDelete()"><i class="fa fa-trash"></i> Eliminar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:buscar();"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:limpiar();">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Ofertas</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	var originalInicio = moment('2018/01/01', 'YYYY/M/D');
	var originalFin = moment();

	var fechaInicio = originalInicio;
	var fechaFin = originalFin;

	$(document).ready(function(){
		initSearchForm();
		initDataTable();
	});

	function initSearchForm() {

		$('#formSearchOferta #vendedorId').select2()
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});

		$('#formSearchOferta #periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
		$('#formSearchOferta #periodoBusqueda').daterangepicker({
			format: 'DD/MM/YYYY',
			startDate: originalInicio,
			endDate: originalFin.add(1, 'days'),
			minDate: '01/01/2018',
			maxDate: '31/12/{{ date('Y') + 1 }}',
			dateLimit: { months: 36 },
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			linkedCalendars: false,
			ranges: {
				'Hoy día': [moment(), moment()],
				'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Esta semana': [moment().startOf('week'), moment().endOf('week')],
				'Semana anterior': [moment().subtract(1, 'weeks').startOf('week'), moment().subtract(1, 'weeks').endOf('week')],
				'Últimos 7 días': [moment().subtract(1, 'weeks'), moment()],
				'Últimas 2 semanas': [moment().subtract(2, 'weeks'), moment()],
				'Últimos 30 días': [moment().subtract(1, 'month'), moment()],
				'Este més': [moment().startOf('month'), moment().endOf('month')],
				'Més anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'Últimos 3 meses': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')],
				'Último semestre': [moment().subtract(6, 'month').startOf('month'), moment().endOf('month')],
				'Último año': [moment().subtract(12, 'month').startOf('month'), moment().endOf('month')]

			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' a ',
//			singleDatePicker: true,
			locale: {
				applyLabel: 'Aceptar',
				cancelLabel: 'Cancelar',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Personalizado',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#periodoBusqueda span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			fechaInicio = start;
			fechaFin = end;
			datatable.ajax.reload();
		});
	}

	function buscar() {
		datatable.ajax.reload();
	}

	function limpiar() {
		$('#vendedorId').val([]).trigger('change');
		fechaInicio = originalInicio;
		fechaFin = originalFin;
		$('#periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
		datatable.ajax.reload();
	}


	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.oferta.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.fechaInicio = fechaInicio.format('YYYY/M/D');
						d.fechaFin = fechaFin.format('YYYY/M/D');
						d.idVendedor = $('#vendedorId').val();
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ data: "cliente.agenda.nombres_apellidos", title: 'Cliente'},
					{ data: "vendedor.owner.agenda.nombres_apellidos", title: 'Vendedor'},
					{ title: 'Producto'},
					{ title: 'Fecha'},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data ,type, row) {
							return '<a href="javascript:;" onclick="ofertaShow(' + row.id + ')">' + data  + '</a>'
						}
					},{
						targets: 2,
					}, {
						targets: 3,
						className: 'text-center',
						render: function(data, type, row){
							var nombre = row.producto_item.producto.marca.nombre + row.producto_item.producto.modelo.nombre;
							return nombre;
						}
					}, {
						targets: 4,
						className: 'text-center',
						render: function(data, type, row){
							return format_datetime(row.created_at);
						}
					}, {
						targets: 5,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function ofertaShow(id){
		var url = '{{ route('admin.oferta.show', ['idOferta']) }}';
		url = url.replace('idOferta', id);
		var modal = openModal(url, 'Oferta', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formOfertaShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}

	function updateEstado(id, e){
		var url = '{{ route('resource.oferta.update', [0]) }}';
		url = url.replace('0', id);
		var estado = undefined;
		if($(e).prop('checked')){
			estado = 1;
		} else {
			estado = 0;
		}

		var data = {
			estado: estado
		}
		ajaxPatch(url, data, function(data){
			if(data.data.estado == 1){
				$(e).prop('checked', true);
			} else {
				$(e).prop('checked', false);
			}
		}, function(data){
			if($(e).prop('checked')){
				$(e).prop('checked', false);
			} else {
				$(e).prop('checked', true);
			}
		})
	}

	function ofertaCreate(){
		var url = '{{ route('admin.oferta.create') }}';
		var modal = openModal(url, 'Nuevo Oferta', null, { size: 'modal-lg' });
		setModalHandler('formOfertaCreate:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function ofertaEdit(id){
		var url = '{{ route('admin.oferta.edit', 'idOferta') }}';
		url = url.replace('idOferta', id);
		var modal = openModal(url, 'Editar Oferta');
		setModalHandler('formOfertaEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function ofertaDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length == 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.oferta.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

</script>
@endpush