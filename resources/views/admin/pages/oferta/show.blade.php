@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.oferta.index') }}">Ofertas</a></li>
		<li class="breadcrumb-item active">Oferta</li>
	</ol>

	<h1 class="page-header">Oferta</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Oferta</h4>
		</div>
		<div class="panel panel-body">
			<div id="formOfertaShow" class="form-horizontal">

				<div class="row">
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">No. Oferta</label>
						<div class="col-sm-8">
							{{ $oferta->noOferta }}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Vendedor</label>
						<div class="col-sm-8">
							{{ $oferta->Vendedor->Owner->Agenda->nombres_apellidos }}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Fecha de oferta</label>
						<div class="col-sm-8">
							{{ $oferta->fechaEnvio }}
						</div>
					</div>
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">No. Orden</label>
						<div class="col-sm-8">

							@if($oferta->Orden)
								{{ $oferta->Orden->codename }}
							@else
								- Sin Orden relacionada -
							@endif
						</div>
					</div>

					<h4 class="col-md-12">Datos del cliente</h4>

					<style>
						.tblProveedor tr th {
							text-align:center;
							font-size:14px;
						}
						.tblProveedor tr td {
							vertical-align: top;
							padding-left:10px;
						}
						.tblProveedor {
							width:100%
						}
						.tblLabel {
							font-weight:bold;
							text-align: right;
						}
					</style>

					<table class="table table-condensed table-bordered tblProveedor">
						<tr class="active">
							<th style="width:50%">Datos Contacto</th>
							<th style="width:50%">Datos Empresa</th>
						</tr>
						<tr class="active">
							<td>
								<address>
									<strong class="pContacto">{{ $oferta->Cliente->Agenda->nombres_apellidos }}</strong><br />
									<a href="javascript:;" class="text-black"><span class="pCorreoContacto">{{ $oferta->Cliente->Usuario->username }}</span></a>
									<br>
									<strong>Telefonos:</strong> <span class="pTelefonos">{{ ($oferta->Cliente->Agenda->Telefonos->count() > 0) ? implode(', ', $oferta->Cliente->Agenda->Telefonos->pluck('numero')->toArray()) : '' }}</span>
								</address>
							</td>
							<td>
								<address>
									<strong class="pNombreProv">{{ $oferta->Cliente->Usuario->Owner->Proveedor->nombre }}</strong><br />
									<span class="pDireccion">{{ ($oferta->Cliente->Usuario->Owner->Proveedor->Direcciones->count() > 0) ? implode('<hr> ', $oferta->Cliente->Usuario->Owner->Proveedor->Direcciones->pluck('direccion')->toArray()) : '' }}</span>
									<br>
									<strong>Telefonos:</strong> <span class="pTelefonos">{{ ($oferta->Cliente->Usuario->Owner->Proveedor->Telefonos->count() > 0) ? implode('<hr> ', $oferta->Cliente->Usuario->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) : '' }}</span>
								</address>
								<strong>Otros contactos:</strong><br>
								<span class="pContactos">
												@foreach($oferta->Cliente->Usuario->Owner->Proveedor->ProveedorContactos as $pc)
										{{ $pc->Agenda->nombres_apellidos }} &lt; {{ $pc->Usuario->username }} &gt; <br>
									@endforeach
											</span>
							</td>
						</tr>
					</table>


					<div class="form-group col-md-6 row m-b-15 group-cliente">
						<label class="col-form-label col-md-4">Maquina</label>
						<div class="col-md-8">
							{{ $oferta->ProductoItem->Producto->Marca->nombre }} {{ $oferta->ProductoItem->Producto->Modelo->nombre }}
						</div>
					</div>


					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Lugar</label>
						<div class="col-sm-8">
							{{ $oferta->lugar }}
						</div>
					</div>
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Teléfono</label>
						<div class="col-sm-8">
							{{ $oferta->telefono }}
						</div>
					</div>
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">E-mail</label>
						<div class="col-sm-8">
							{{ $oferta->email }}
						</div>
					</div>
				</div>

				<div class="form-group col-md-12 row m-b-15">
					<div class="col-md-12">
						Observaciones:<br>
						{{ $oferta->observaciones }}
					</div>
				</div>

				<style>
					.files-holder {
						background-color:#d9e0e7;
						padding:15px;
						margin:5px;

						-ms-word-break: break-all;
						word-break: break-all;
						word-break: break-word;
					}
					.fileIcon {
						text-align:center;
						font-size:11px;
					}
					.fileIcon a {
						/*font-weight:bold;*/
						color:darkblue !important;
					}
					.fileIcon i {
						margin:auto;
						margin-bottom:10px;
					}
					.col-form-label, .row.form-group>.col-form-label {
						padding:0px;
					}
				</style>


				<div class="form-group row m-b-15">

					<h4 class="col-md-12">Archivos adjuntos</h4>

					<div class="col-md-12">
						<div class="row files-holder">
						@foreach($oferta->Archivos as $archivo)

							<div class="col-md-2 fileIcon">
								@if(pathinfo($archivo->ruta, PATHINFO_EXTENSION) == 'pdf')
									<i class="fa fa-file-pdf fa-4x"></i>
								@else
									<i class="fa fa-file-alt fa-4x"></i>
								@endif
								<br>
								<a target="_blank" href="{{ route('resource.archivo.stream', [$archivo->id]) }}">{{ $archivo->realname }}</a>
							</div>
						@endforeach
						</div>
					</div>
				</div>


				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-md-6 text-left">
					</div>
					<div class="col-md-6 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="ofertaCancelar()">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>

</script>
@endpush

