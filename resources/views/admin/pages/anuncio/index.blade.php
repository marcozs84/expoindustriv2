@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="/home">Home</a></li>
		<li class="breadcrumb-item active">Anuncios</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Anuncios <small></small></h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Filtros</h4>
		</div>
		<div id="formSearchOrden" class="panel-body p-b-0">
			<div class="row" id="searchForm">

				<div class="form-group col-lg-4 col-md-4 col-sm-6" style="vertical-align:top;">
					<label for="estados" style="">Estado</label>
					{!! Form::select('estados', [
						\App\Models\Publicacion::ESTADO_APPROVED => __('messages.approved'),
						\App\Models\Publicacion::ESTADO_DRAFT => __('messages.draft'),
						\App\Models\Publicacion::ESTADO_AWAITING_APPROVAL => __('messages.awaiting_approval'),
						\App\Models\Publicacion::ESTADO_REJECTED => __('messages.rejected'),
						\App\Models\Publicacion::ESTADO_DISABLED => __('messages.inactive'),
					], null, [
						'id' => 'estados',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
						]) !!}
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-6" style="vertical-align:top;">
					<label for="vendedorId" style="">Marcas</label>
					{!! Form::select('idMarcas', $marcas, null, [
						'id' => 'idMarcas',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
						]) !!}
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-6" style="vertical-align:top;">
					<label for="vendedorId" style="">Modelos</label>
					{!! Form::select('idModelos', $modelos, null, [
						'id' => 'idModelos',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
						]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
					{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaCreate()"><i class="fa fa-plus"></i> Crear</a>--}}
					{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaDelete()"><i class="fa fa-trash"></i> Eliminar</a>--}}


					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="anuncioCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="anuncioDelete()"><i class="fa fa-trash"></i> Eliminar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:buscar();"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:limpiar();">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Anuncios</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>--}}
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = {!! json_encode($publicacionEstados)  !!};

	$(document).ready(function(){
		initDataTable();
		initSearchForm();
	});

	function initSearchForm() {
		$('#estados, #idMarcas, #idModelos').select2()
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});
	}

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.anuncio.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
//	                d.element = $('#element').val();
						d.estados = $('#estados').val();
						d.idMarcas = $('#idMarcas').val();
						d.idModelos = $('#idModelos').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: '@lang('messages.product')'},
					{ title: 'Categoria'},
					{ title: 'Estado'},
					{ data: 'created_at', title: 'Fecha Creacion'},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
							var name = '';
							if(row.producto){
								name = row.producto.nombre_alter;
							} else {
								name =  '<b class="text-red">@lang('messages.no_product')!</b>';
							}
							return '<a href="javascript:;" onclick="anuncioShow('+row.id+')">'+name+'</a>'
						}
					},{
						targets: 2,
						render: function(data, type, row){
							var response = '';
							if(row.producto !== null){
								if(row.producto.categorias !== undefined){
									$.each(row.producto.categorias, function(key, value){
										response += value.padre.nombre + ' / ' + value.nombre;
									});
									return response;
								} else {
									return 'Sin categorias';
								}
							} else {
								return '';
							}
						}
					},{
						targets:3,
						render: function(data, type, row){
							return estados[row.estado];
						}
					}, {
						targets: 5,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function buscar() {
		datatable.ajax.reload();
	}

	function limpiar() {
		$('#estados, #idMarcas, #idModelos').val([]).trigger('change');
		datatable.ajax.reload();
	}

	function anuncioShow(id){
		var url = '{{ route('admin.anuncio.show', ['idAnuncio']) }}';
		url = url.replace('idAnuncio', id);
		var modal = openModal(url, 'Anuncio', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formAnuncioShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}

	function anuncioCreate(){
		var url = '{{ route('admin.anuncio.create') }}';
		var modal = openModal(url, 'Nuevo Anuncio', null, { size : 'modal-lg' });
		setModalHandler('formAnuncioCreate:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function anuncioEdit(id){
		var url = '{{ route('admin.anuncio.edit', ['idAnuncio']) }}';
		url = url.replace('idAnuncio', id);
		var modal = openModal(url, 'Editar Anuncio');
		setModalHandler('formAnuncioEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function anuncioDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length == 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '{{ route('resource.anuncio.destroy', [0]) }}';

		ajaxDelete(url, ids, function(){
			datatable.ajax.reload(null, false);
		});
	}

</script>
@endpush