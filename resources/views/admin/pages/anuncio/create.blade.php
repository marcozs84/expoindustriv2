@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.anuncio.index') }}">Anuncios</a></li>
		<li class="breadcrumb-item active">Nuevo/a publicacion</li>
	</ol>

	<h1 class="page-header">Nuevo Anuncio</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Publicacion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formPublicacionCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Producto</label>
					<div class="col-8">
						{!! Form::number('idProducto', '', [
							'id' => 'idProducto',
							'placeholder' => 'Id Producto',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Tipo Pago</label>
					<div class="col-8">
						{!! Form::text('idTipoPago', '', [
							'id' => 'idTipoPago',
							'placeholder' => 'Id Tipo Pago',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', '', [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', '', [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Inicio</label>
					<div class="col-8">
						{!! Form::text('fechaInicio', '', [
							'id' => 'fechaInicio',
							'placeholder' => 'Fecha Inicio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Fin</label>
					<div class="col-8">
						{!! Form::text('fechaFin', '', [
							'id' => 'fechaFin',
							'placeholder' => 'Fecha Fin',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Visitas</label>
					<div class="col-8">
						{!! Form::number('visitas', '', [
							'id' => 'visitas',
							'placeholder' => 'Visitas',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::text('estado', '', [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Token</label>
					<div class="col-8">
						{!! Form::text('token', '', [
							'id' => 'token',
							'placeholder' => 'Token',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Dias</label>
					<div class="col-8">
						{!! Form::number('dias', '', [
							'id' => 'dias',
							'placeholder' => 'Dias',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio</label>
					<div class="col-8">
						{!! Form::text('precio', '', [
							'id' => 'precio',
							'placeholder' => 'Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Moneda</label>
					<div class="col-8">
						{!! Form::text('moneda', '', [
							'id' => 'moneda',
							'placeholder' => 'Moneda',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{!! Form::text('created_at', '', [
							'id' => 'created_at',
							'placeholder' => 'Created_at',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="publicacionCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="publicacionSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

	@if($isAjaxRequest)
formInit();
	@else
$(function(){ formInit(); });
	@endif

$( "#formPublicacionCreate" ).submit(function( event ) {
		publicacionSave();
		event.preventDefault();
	});

	function publicacionSave(){
		var url = '{{ route('resource.publicacion.store') }}';
		ajaxPost(url, {
			idProducto: 	$('#formPublicacionCreate #idProducto').val(),
			idTipoPago: 	$('#formPublicacionCreate #idTipoPago').val(),
			owner_type: 	$('#formPublicacionCreate #owner_type').val(),
			owner_id: 		$('#formPublicacionCreate #owner_id').val(),
			fechaInicio: 	$('#formPublicacionCreate #fechaInicio').val(),
			fechaFin: 		$('#formPublicacionCreate #fechaFin').val(),
			visitas: 		$('#formPublicacionCreate #visitas').val(),
			estado: 		$('#formPublicacionCreate #estado').val(),
			token: 		$('#formPublicacionCreate #token').val(),
			dias: 			$('#formPublicacionCreate #dias').val(),
			precio: 		$('#formPublicacionCreate #precio').val(),
			moneda: 		$('#formPublicacionCreate #moneda').val(),
			created_at: 	$('#formPublicacionCreate #created_at').val()
		}, function(){
			$(document).trigger('formPublicacionCreate:success');
		});
	}

	function publicacionCancel(){
		@if($isAjaxRequest)
$(document).trigger('formPublicacionCreate:cancel');
		@else
redirect('{{ route('admin.publicacion.index')  }}');
		@endif
	}

	function formInit(){

		$('#formPublicacionCreate #fechaInicio, #formPublicacionCreate #fechaFin, #formPublicacionCreate #created_at').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			todayHighlight: true,
			orientation: 'bottom'
		})
			.datepicker("setDate",'now')
			.on('changeDate', function(){
			});
		$("#formPublicacionCreate #fechaInicio, #formPublicacionCreate #fechaFin, #formPublicacionCreate #created_at").mask("99/99/9999");

	}

</script>
@endpush

