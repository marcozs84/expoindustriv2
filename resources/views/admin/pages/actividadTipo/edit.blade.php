@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.actividadTipo.index') }}">Actividad Tipo</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $actividadTipo->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Actividad - Tipo</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando: {{ $actividadTipo->nombre }} ( {{ $actividadTipo->id }} )</h4>
		</div>
		<div class="panel panel-body">
			<form id="formActividadTipoEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Padre</label>
					<div class="col-8">
						{!! Form::select('idPadre', [], '', [
							'id' => 'idPadre',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-12 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Padre</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idPadre', $actividadTipo->idPadre, [--}}
							{{--'id' => 'idPadre',--}}
							{{--'placeholder' => 'Id Padre',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', $actividadTipo->nombre, [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                        {!! Form::textarea('descripcion', $actividadTipo->descripcion, [
                            'id' => 'descripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Orden</label>
					<div class="col-8">
						{!! Form::number('orden', $actividadTipo->orden, [
							'id' => 'orden',
							'placeholder' => 'Orden',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="actividadTipoCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="actividadTipoSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){



	$idPadreSearch = Search.resource('#formActividadTipoEdit #idPadre', {
		url: '{{ route('resource.actividadTipo.ds') }}',
		data: {
			soloGrupos : 1
		},
		templateResult: function( data ) {
			return data.nombre;
		},
		templateSelection: function( data ) {
			return data.nombre;
		},
		@if($actividadTipo->idPadre > 0)
		defaultValue: {
			value: {{ $actividadTipo->idPadre }},
			text: '{{ $actividadTipo->Padre->nombre }}'   // <- Must match format with templateSelection;
		}
		@endif
	});
	@if($actividadTipo->idPadre > 0)
	// Search.setDefault($idPadreSearch, {{ $actividadTipo->idPadre }}, '{{ $actividadTipo->Padre->nombre }});
	@endif



}

$( "#formActividadTipoEdit" ).submit(function( event ) {
	actividadTipoSave();
	event.preventDefault();
});

function actividadTipoSave(){
	var url = '{{ route('resource.actividadTipo.update', [$actividadTipo->id]) }}';
	var formActividadTipoEdit = $('#formActividadTipoEdit');
	ajaxPatch(url, {
		idPadre: 		formActividadTipoEdit.find('#idPadre').val(),
		nombre: 		formActividadTipoEdit.find('#nombre').val(),
		descripcion: 	formActividadTipoEdit.find('#descripcion').val(),
		orden: 		formActividadTipoEdit.find('#orden').val()
	}, function(){
		$(document).trigger('formActividadTipoEdit:success');
	});
}

function actividadTipoCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formActividadTipoEdit:cancel');
	@else
		redirect('{{ route('admin.actividadTipo.index')  }}');
	@endif
}

</script>
@endpush

