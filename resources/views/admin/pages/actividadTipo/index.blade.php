@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', ' Actividad Tipos')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"> Actividad Tipos</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"> Actividad Tipos <small></small></h1>
	<!-- end page-header -->


	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Búsqueda</h4>
		</div>
		<div id="formSearchGasto" class="panel-body p-b-0">
			<div class="row" id="searchForm">

				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="idPadreSearch" style="">Grupo</label>
					{!! Form::select('idPadreSearch', [], null, [
						'id' => 'idPadreSearch',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="actividadTipoCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="actividadTipoDelete()"><i class="fa fa-trash"></i> Eliminar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:;" onclick="buscar()"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:;" onclick="limpiar()">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title"> Actividad Tipos</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	$(document).ready(function(){
		var idPadreSearch = Search.resource('#idPadreSearch', {
			url: '{{ route('resource.actividadTipo.ds') }}',
			data: {
				soloGrupos : 1
			},
			templateResult: function( data ) {
				return data.nombre;
			},
			templateSelection: function( data ) {
				return data.nombre;
			}
		});

		idPadreSearch
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});

		initDataTable();
	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.actividadTipo.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.idPadre = $('#idPadreSearch').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//						var data = JSON.parse(data);
//						for(var i = 0 ; i < data.data.length; i++){
//							// ...
//						}
//						return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
					{ data: 'idPadre', title: 'IdPadre'},
					{ data: 'nombre', title: 'Nombre'},
					{ data: 'descripcion', title: 'Descripcion'},
					{ data: 'orden', title: 'Orden'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					@php $counter = 0 @endphp
					{
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '30px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="actividadTipoShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// idPadre,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// nombre,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="actividadTipoShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// descripcion,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// orden,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';
							html += '<a href="javascript:;" onclick="actividadTipoEdit('+ row.id +')" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Editar</a>';
							return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function buscar() {
		datatable.ajax.reload();
	}

	function actividadTipoShow(id){
		var url = '{{ route('admin.actividadTipo.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Actividad - Tipo', null, {
			'size': 'modal-lg'
		});
	}

	function actividadTipoCreate(){
		var url = '{{ route('admin.actividadTipo.create') }}';
		var modal = openModal(url, 'Nuevo Actividad - Tipo', null, { size:'modal-lg' });
		setModalHandler('formActividadTipoCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function actividadTipoEdit(id){
		var url = '{{ route('admin.actividadTipo.edit', 'idRecord') }}';
		url = url.replace('idRecord', id);
		var modal = openModal(url, 'Editar Actividad - Tipo', null, { size:'modal-lg' });
		setModalHandler('formActividadTipoEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function actividadTipoDelete( id ){
		var ids = [];

		if ( id !== undefined ) {
			ids = [ id ];
		} else {
			$('.chkId').each(function() {
				if($(this).prop('checked')){
					ids.push($(this).val());
				}
			});
			if(ids.length === 0){
				alert("Debe seleccionar al menos 1 registro.");
				return false;
			}
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.actividadTipo.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

</script>
@endpush