@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.actividadTipo.index') }}">Actividad Tipo</a></li>
		<li class="breadcrumb-item active">Nuevo/a actividadTipo</li>
	</ol>

	<h1 class="page-header">Nuevo Actividad - Tipo</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Actividad - Tipo</h4>
		</div>
		<div class="panel panel-body">
			<form id="formActividadTipoCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Padre</label>
					<div class="col-8">
						{!! Form::select('idPadre', [], '', [
							'id' => 'idPadre',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-12 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Padre</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idPadre', '', [--}}
							{{--'id' => 'idPadre',--}}
							{{--'placeholder' => 'Id Padre',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', '', [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                        {!! Form::textarea('descripcion', '', [
                            'id' => 'descripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Orden</label>
					<div class="col-8">
						{!! Form::number('orden', '', [
							'id' => 'orden',
							'placeholder' => 'Orden',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="actividadTipoCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="actividadTipoSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){





	var idPadreSearch = Search.resource('#formActividadTipoCreate #idPadre', {
		url: '{{ route('resource.actividadTipo.ds') }}',
		data: {
			soloGrupos : 1
		},
		templateResult: function( data ) {
			return data.nombre;
		},
		templateSelection: function( data ) {
			return data.nombre;
		}
	});

}

$( "#formActividadTipoCreate" ).submit(function( event ) {
	actividadTipoSave();
	event.preventDefault();
});

function actividadTipoSave(){
	var url = '{{ route('resource.actividadTipo.store') }}';
	var formActividadTipoCreate = $('#formActividadTipoCreate');
	ajaxPost(url, {
		idPadre: 		formActividadTipoCreate.find('#idPadre').val(),
		nombre: 		formActividadTipoCreate.find('#nombre').val(),
		descripcion: 	formActividadTipoCreate.find('#descripcion').val(),
		orden: 		formActividadTipoCreate.find('#orden').val()
	}, function(){
		$(document).trigger('formActividadTipoCreate:success');
	});
}

function actividadTipoCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formActividadTipoCreate:cancel');
	@else
		redirect('{{ route('admin.actividadTipo.index')  }}');
	@endif
}

</script>
@endpush

