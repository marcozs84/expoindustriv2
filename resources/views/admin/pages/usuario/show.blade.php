@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
{{-- #### SOLUCIONADO EL PROBLEMA DE CERRAR EL COMBOBOX AUMENTANDO EL MARGEN --}}
{{--<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />--}}
<!-- ================== END PAGE LEVEL STYLE ================== -->
<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}
</style>
@endpush

@section('content')

	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.usuario.index') }}">Usuarios</a></li>
		<li class="breadcrumb-item active">Editar usuario</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Editar Usuario <small></small></h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Datos agenda</h4>
				</div>
				<div class="panel-body">


							<form id="formUsuarioShow" class="form-horizontal">

								<style>
									.col-form-label {
										text-align:right !important;
									}
								</style>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-4">Nombre de usuario (email)</label>
									<div class="col-8">
										{{ $usuario->username }}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-4" style="padding: 10px 0px 10px 0px;">Tipo</label>
									<div class="col-8">
										{!! Form::select('idTipo', $tipos, $usuario->idTipo, [
											'id' => 'idTipo',
											'class' => 'form-control default-select2',
											'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-4">Nombres</label>
									<div class="col-8">
										{!! $usuario->Owner->Agenda->nombres !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-4">Apellidos</label>
									<div class="col-8">
										{!! $usuario->Owner->Agenda->apellidos !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-empleado">
									<label class="col-form-label col-4">Cargo</label>
									<div class="col-8">
										{!! $usuario->cargo !!}
									</div>
								</div>

								@if( get_class($usuario->Owner) == 'App\Models\ProveedorContacto' )
									<div class="form-group row m-b-15 group-cliente">
										<label class="col-form-label col-4">Empresa</label>
										<div class="col-8">
											{!! $usuario->Owner->Proveedor->nombre !!}
										</div>
									</div>
								@endif

								<div class="form-group row m-b-15 group-cliente">
									<label class="col-form-label col-4">Teléfono(s) Empresa</label>
									<div class="col-8" id="lblTelefonos"></div>
								</div>

								<div class="form-group row m-b-15 group-cliente">
									<label class="col-form-label col-4">NIT Empresa</label>  <!-- <br><small>Obligatorio si es Tipo "Cliente"<br>Separados por coma</small> -->
									<div class="col-8" id="lblNit"></div>
								</div>

								<div class="form-group row m-b-15 group-empleado group-cliente">
									<label class="col-form-label col-4">Teléfono(s)</label>  <!-- <br><small>Obligatorio si es Tipo "Cliente"<br>Separados por coma</small> -->
									<div class="col-8">
										{!! ($usuario->Owner->Agenda->Telefonos->count() > 0 ? implode(', ', $usuario->Owner->Agenda->Telefonos->pluck('numero')->toArray()) : '' ) !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente">
									<label class="col-form-label col-4">NIT<br><small>Obligatorio si es Tipo "Cliente"</small></label>
									<div class="col-8">
										{!! $usuario->Owner->Agenda->nit !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-4" style="padding: 10px 0px 10px 0px;">Estado</label>
									<div class="col-8">
										{!! Form::select('idEstado', [
												\App\Models\Usuario::ESTADO_ACTIVO => 'Activo',
												\App\Models\Usuario::ESTADO_INACTIVO => 'Inactivo',
												\App\Models\Usuario::ESTADO_ESPERANDO_CONFIRMACION => 'Esperando confirmacion',
												\App\Models\Usuario::ESTADO_REPORTADO => 'Reportado',
											], $usuario->estado, [
											'id' => 'idEstado',
											'class' => 'form-control default-select2',
											'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group row m-b-15">
									<label class="col-form-label col-4">Roles</label>
									<div class="col-8">
										@foreach($roles as $roleId => $roleNombre)
												@if($usuario->Roles->where('id', $roleId)->first())
													{{ $roleNombre }},
												@endif
										@endforeach
									</div>
								</div>
								<div class="form-group row m-b-15">
									<label class="col-form-label col-4">Permisos</label>
									<div class="col-8">
										@foreach($permisos as $permiso => $convenios)
											<span style="padding:3px; margin:3px; float: left;" class="bg-aqua-transparent-2">{{ $permiso }}</span>
										@endforeach
									</div>
								</div>

							</form>

							<div class="row">
								<div class="col-lg-12 text-right">
									<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;">Cerrar</a>
								</div>
							</div>


				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->

@stop

@push('scripts')
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/plugins/masked-input/masked-input.min.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/assets/plugins/parsley/dist/parsley.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>--}}
<!-- ================== END PAGE LEVEL JS ================== -->
<script>

	$.getScript('/assets/plugins/select2/dist/js/select2.min.js').done(function() {
		$('.multiple-select2, .default-select2').select2();
		formInit();
	});

	function formInit(){

		if( '{{ get_class($usuario->Owner) }}' === 'AppModelsProveedorContacto' ) {
			$('.group-cliente, .group-empleado').addClass('hide');
			$('.group-cliente').removeClass('hide');
		} else {
			$('.group-cliente, .group-empleado').addClass('hide');
			$('.group-empleado').removeClass('hide');
		}

		$('#password').val('');
		$('#password_confirmation').val('');

		@if( get_class($usuario->Owner) == 'App\Models\ProveedorContacto' )
			$('#formUsuarioShow #lblNit').html('{{ $usuario->Owner->Proveedor->nit }}');
			$('#formUsuarioShow #lblTelefonos').html('{{ ($usuario->Owner->Proveedor->Telefonos->count() > 0) ? implode(', ', $usuario->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) : '' }}');
			var option = $('<option>', {
				value: {{ $usuario->Owner->Proveedor->id }}
			}).html('{{ $usuario->Owner->Proveedor->nombre }}');
			$('#formUsuarioShow #searchProveedor').html('');
			$('#formUsuarioShow #searchProveedor').append(option);
			$('#formUsuarioShow .btnProveedorEdit').attr('onclick', 'proveedorEdit({{ $usuario->Owner->Proveedor->id }})');
			$('#formUsuarioShow .btnProveedorEdit').removeClass('hide');
		@else

		@endif

	}

	function usarEmail(email){
		$('#formUsuarioShow #username').val(email);
	}


</script>
@endpush