@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
	{{-- #### SOLUCIONADO EL PROBLEMA DE CERRAR EL COMBOBOX AUMENTANDO EL MARGEN --}}
	{{--<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />--}}
	<!-- ================== END PAGE LEVEL STYLE ================== -->
@endpush

@section('content')

	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="javascript:;">Admin</a></li>
		<li><a href="javascript:;">Usuarios</a></li>
		<li class="active">Crear</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Nuevo Usuario <small></small></h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Datos agendales</h4>
				</div>
				<div class="panel-body">

					<ul class="nav nav-pills nav-pills-inverse nav-justified nav-justified-mobile">
						<li class="nav-item"><a href="#usuario-tab-1" class="nav-link active" data-toggle="tab">Información</a></li>
						<li class="nav-item"><a href="#usuario-tab-2" class="nav-link" data-toggle="tab">Roles</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active show" id="usuario-tab-1">

							<form id="formUsuarioEdit" class="form-horizontal">

								<style>
									.col-form-label {
										text-align:right !important;
									}
								</style>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4">Nombre de usuario (email)</label>
									<div class="col-md-8">
										{!! Form::text('username', $usuario->username, [
											'id' => 'username',
											'class' => 'form-control',
											'placeholder' => 'E-mail',
											'autocomplete' => 'false',
											'autofocus'
										]) !!}
									</div>
								</div>
								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4">Contraseña</label>
									<div class="col-md-8">
										{!! Form::password('password', [
											'id' => 'password',
											'value' => '',
											'class' => 'form-control',
											'placeholder' => 'Contraseña',
											'autocomplete' => 'off'
										]) !!}
									</div>
								</div>
								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4">Confirmar contraseña</label>
									<div class="col-md-8">
										{!! Form::password('password_confirmation', [
											'id' => 'password_confirmation',
											'class' => 'form-control',
											'placeholder' => 'Confirmar contraseña',
											'autocomplete' => 'off'
										]) !!}
									</div>
								</div>
								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4"></label>
									<div class="col-md-8">
										<a href="http://passwordsgenerator.net/es/" target="_blank">Generador de contraseñas</a>
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4">Tipo</label>
									<div class="col-md-8">
										{!! Form::select('idTipo', $tipos, $usuario->idTipo, [
											'id' => 'idTipo',
											'class' => 'form-control default-select2',
											'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4">Nombres</label>
									<div class="col-md-8">
										{!! Form::text('nombres', $usuario->Owner->Agenda->nombres, [
											'id' => 'nombres',
											'class' => 'form-control',
											'placeholder' => 'Nombres',
										]) !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4">Apellidos</label>
									<div class="col-md-8">
										{!! Form::text('apellidos', $usuario->Owner->Agenda->apellidos, [
											'id' => 'apellidos',
											'class' => 'form-control',
											'placeholder' => 'Apellidos',
										]) !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-empleado">
									<label class="col-form-label col-md-4">Cargo</label>
									<div class="col-md-8">
										{!! Form::text('cargo', $usuario->cargo, [
											'id' => 'cargo',
											'class' => 'form-control',
											'placeholder' => 'Cargo',
										]) !!}
									</div>
								</div>

								<div style="background-color:#e5e5e5;" class="p-t-5 p-r-5">
									<div class="form-group row m-b-15 group-cliente">
										<label class="col-form-label col-md-4">Empresa<br><small>Obligatorio si es Tipo "Cliente"</small></label>
										<div class="col-md-8">
											{{--{!! Form::text('empresa', '', [--}}
											{{--'id' => 'empresa',--}}
											{{--'class' => 'form-control',--}}
											{{--'placeholder' => 'Empresa',--}}
											{{--]) !!}--}}

											{!! Form::select('searchProveedor', [], null, [
												'id' => 'searchProveedor',
												'class' => 'form-control default-select2',
												'autofocus'
											]) !!}
											<a href="javascript:;" class="btn btn-primary" onclick="proveedorCreate()" style="margin-top:5px;">Nuevo Proveedor</a>
											<a href="javascript:;" class="btn btn-primary btnProveedorEdit hide" onclick="proveedorEdit()" style="margin-top:5px;">Editar Proveedor</a>
										</div>
									</div>

									<div class="form-group row m-b-15 group-cliente">
										<label class="col-form-label col-md-4">Teléfono(s) Empresa</label>  <!-- <br><small>Obligatorio si es Tipo "Cliente"<br>Separados por coma</small> -->
										<div class="col-md-8 p-t-5" id="lblTelefonos"></div>
									</div>

									<div class="form-group row m-b-15 group-cliente">
										<label class="col-form-label col-md-4">NIT Empresa</label>  <!-- <br><small>Obligatorio si es Tipo "Cliente"<br>Separados por coma</small> -->
										<div class="col-md-8 p-t-5" id="lblNit"></div>
									</div>
								</div>

								<div class="form-group row m-b-15 group-empleado group-cliente">
									<label class="col-form-label col-md-4">Teléfono(s)</label>  <!-- <br><small>Obligatorio si es Tipo "Cliente"<br>Separados por coma</small> -->
									<div class="col-md-8 p-t-5">
										{!! Form::text('telefono',  ($usuario->Owner->Agenda->Telefonos->count() > 0) ? implode(', ', $usuario->Owner->Agenda->Telefonos->pluck('numero')->toArray()) : '', [
											'id' => 'telefono',
											'class' => 'form-control',
											'placeholder' => 'Teléfono',
										]) !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente">
									<label class="col-form-label col-md-4">NIT<br><small>Obligatorio si es Tipo "Cliente"</small></label>
									<div class="col-md-8">
										{!! Form::text('nit', $usuario->Owner->Agenda->nit, [
											'id' => 'nit',
											'class' => 'form-control',
											'placeholder' => 'NIT',
										]) !!}
									</div>
								</div>

								<div class="form-group row m-b-15 group-cliente group-empleado">
									<label class="col-form-label col-md-4">Estado</label>
									<div class="col-md-8">
										{!! Form::select('idEstado', [
												\App\Models\Usuario::ESTADO_ACTIVO => 'Activo',
												\App\Models\Usuario::ESTADO_INACTIVO => 'Inactivo',
												\App\Models\Usuario::ESTADO_ESPERANDO_CONFIRMACION => 'Esperando confirmacion',
												\App\Models\Usuario::ESTADO_REPORTADO => 'Reportado',
											], $usuario->estado, [
											'id' => 'idEstado',
											'class' => 'form-control default-select2',
											'autofocus'
										]) !!}
									</div>
								</div>

								<div class="form-group text-right group-cliente group-empleado">
									<div class="col-md-12">
										<a class="btn btn-white btn-sm btn-warning" href="javascript:;" onclick="agendaCrear()">Crear Agenda</a>
										<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" >Cerrar</a>
										<a class="btn btn-primary btn-sm" href="javascript:;" onclick="usuarioGuardar()" >Guardar</a>
									</div>
								</div>
							</form>




						</div>
						<div class="tab-pane fade" id="usuario-tab-2">

							<table class="table table-striped table-condensed">
								<tr>
									<td style="width:50%; padding:5px;" class="text-right"><b>Usuario:</b> </td>
									<td style="width:50%; padding:5px;">{{ $usuario->username }}</td>
								</tr>
							</table>

							<div class="row">
								<div class="col col-lg-6 col-md-6">
									<h5><i class="fa fa-chevron-right"></i> Resumen de roles actuales</h5>
									<table class="table table-striped table-bordered width-full table-condensed">
										<thead>
										<tr>
											<th>Role</th>
											<th class="text-center">Activo</th>
										</tr>
										</thead>
										<tbody>
										@foreach($roles as $roleId => $roleNombre)
											<tr>
												<td>
													@if($usuario->Roles->where('id', $roleId)->first())
														<a href="javascript:;" onclick="editarRole({{ $roleId }})">{{ $roleNombre }}</a></td>
												@else
													{{ $roleNombre }}
												@endif
												<td class="text-center">
													<input id="chkRole_{{ $roleId }}" type="checkbox" class="chkRole " value="{{ $roleId }}" onchange="asignarRoles({{ $roleId }})"
													       @if($usuario->Roles->where('id', $roleId)->first()) checked @endif/>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="col col-lg-6 col-md-6">
									<h5><i class="fa fa-chevron-right"></i> Resumen de permisos asignados</h5>
									<table id="arbolPermisos_usuario" class="table table-bordered width-full table-condensed">
										<thead>
										<tr>
											<th>Permiso</th>
											<th class="text-center">Convenios</th>
										</tr>
										</thead>
										<tbody>
										@foreach($permisos as $permiso => $convenios)
											<tr>
												<td>{{ $permiso }}</td>
												<td>
													{{ implode(', ', $convenios) }}
												</td>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 text-right">
									<a class="btn btn-white btn-sm" data-dismiss="modal" onclick="usuarioCancelar()" href="javascript:;">Cerrar</a>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->

@stop

@push('scripts')
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/plugins/masked-input/masked-input.min.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/assets/plugins/parsley/dist/parsley.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>--}}
<!-- ================== END PAGE LEVEL JS ================== -->
<script>

	$.getScript('/assets/plugins/select2/dist/js/select2.min.js').done(function() {
		$('.multiple-select2, .default-select2').select2();
		formInit();
	});

			{{--@if($isAjaxRequest)--}}
			{{--formInit();--}}
			{{--@else--}}
			{{--$(function(){ formInit(); });--}}
			{{--@endif--}}

	var form_user = null;
	var form_agenda_nueva = null;
	var form_agenda_existente = null;
	var buscar_agenda = null;
	function formInit(){

		if( '{{ get_class($usuario->Owner) }}' === 'AppModelsProveedorContacto' ) {
			$('.group-cliente, .group-empleado').addClass('hide');
			$('.group-cliente').removeClass('hide');
		} else {
			$('.group-cliente, .group-empleado').addClass('hide');
			$('.group-empleado').removeClass('hide');
		}

		$('#password').val('');
		$('#password_confirmation').val('');

		@if( get_class($usuario->Owner) == 'App\Models\ProveedorContacto' )
			$('#formUsuarioEdit #empresa').val('{{ $usuario->Owner->Proveedor->nombre }}');
			$('#formUsuarioEdit #nit').val('{{ $usuario->Owner->Agenda->nit }}');
			$('#formUsuarioEdit #lblNit').html('{{ $usuario->Owner->Proveedor->nit }}');
			$('#formUsuarioEdit #lblTelefonos').html('{{ ($usuario->Owner->Proveedor->Telefonos->count() > 0) ? implode(', ', $usuario->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) : '' }}');
			var option = $('<option>', {
				value: {{ $usuario->Owner->Proveedor->id }}
			}).html('{{ $usuario->Owner->Proveedor->nombre }}');
			$('#formUsuarioEdit #searchProveedor').html('');
			$('#formUsuarioEdit #searchProveedor').append(option);
			$('#formUsuarioEdit .btnProveedorEdit').attr('onclick', 'proveedorEdit({{ $usuario->Owner->Proveedor->id }})');
			$('#formUsuarioEdit .btnProveedorEdit').removeClass('hide');
		@else

		@endif

		// ----------

		$('#formUsuarioEdit #searchProveedor').select2({
//        allowClear: true,
			placeholder: {
				id: "",
				placeholder: "Leave blank to ..."
			},
			ajax: {
				url: '{{ route('resource.proveedor.ds') }}',
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token' : '{!! csrf_token() !!}'
				},
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page
					};
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function (data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: function(repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if (repo.loading) return repo.text;

				var proveedoresHtml = '';
				for(var i = 0 ; i < repo.proveedor_contactos.length ; i++) {
					proveedoresHtml += repo.proveedor_contactos[i].agenda.nombres_apellidos + "<br>";
				}

				var markup = "<div><b>" + repo.nombre + '</b><br>' + proveedoresHtml + "</div>";
//				var markup = "<div><b>" + repo.nombre + '</b><br>' + repo.descripcion + "</div>";
				return markup;
			},
			templateSelection: function formatRepoSelection (repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if(repo.nombre){
					var telefonos = [];
					if( repo.telefonos ) {
						for(var i = 0 ; i < repo.telefonos.length ; i++) {
							telefonos.push(repo.telefonos[i].numero);
						}
						$('#formUsuarioEdit #lblTelefonos').html(telefonos.join(', '));
					}
					$('#formUsuarioEdit #lblNit').html(repo.nit);
					$('#formUsuarioEdit .btnProveedorEdit').attr('onclick', 'proveedorEdit('+ repo.id +')');
					$('#formUsuarioEdit .btnProveedorEdit').removeClass('hide');

					// ----------

					return repo.nombre;
				} else {
					console.log("returning test");
					return repo.text;
				}
				//return repo.nombre + ' ' + repo.apellidos || repo.text;
			},
//	    allowClear: true
		})
			.trigger('change');

	}

	function usarEmail(email){
		$('#formUsuarioEdit #username').val(email);
	}

	function abrirUsuarioExistente(id){

		$('#buscar_agenda').remove();

		var url = '{{ route('admin.usuario.index') }}/'+id+'/edit';
		var modal = openModal(url, 'Editar Usuario');
		setModalHandler('formUsuarioEdit:aceptar', function(){
			dismissModal(modal);
		});
		$(document).trigger('formUsuarioEdit:cancelar');
	}

	function verificarTieneUsuario(){

		var url = '{{ route('admin.usuario.verificarTieneUsuario') }}';
		var data = {
			idAgenda: $('#buscar_agenda').val(),
			idUsuario: {{ $usuario->id }}
		};

		ajaxPost(url, data, function(response){
			$('#infoUsuario').removeClass('hide');
			$('#linkUsuario').attr('href', 'javascript:;');
			$('#linkUsuario').attr('onclick', 'abrirUsuarioExistente('+response.data.idUsuario+')');

		}, function(response){
			$('#email_alternativos').addClass('hide');
			$('#email_disponibles').html('');

			var email1 = '';
			var email2 = '';

			if(response.data.email1){
				email1 = response.data.email1.trim();
			}
			if(response.data.email2){
				email2 = response.data.email2.trim();
			}

			if(email1 != '' && email2 != ''){
				// se listan los correos disponibles para que el usuario escoja el correcto
				$('#email_alternativos').removeClass('hide');
				var li1 = $('<li></li>').html('<a href="javascript:usarEmail(\''+email1+'\');">'+email1+'</a>');
				var li2 = $('<li></li>').html('<a href="javascript:usarEmail(\''+email2+'\');">'+email2+'</a>');

				$('#email_disponibles').append(li1);
				$('#email_disponibles').append(li2);
				$('#username').val('');
			} else if(email1 != ''){
				usarEmail(email1);
			} else if(email2 != ''){
				usarEmail(email2);
			} else {
				mostrarAlerta('La agenda seleccionada no tiene una cuenta de correo asociada, por favor provea una')
			}
		});
	}

	function usuarioGuardar(){

		var url = '{{ route('admin.usuario.update', [$usuario->id]) }}';
		var data = {
			username: $('#formUsuarioEdit #username').val().trim(),
			password: $('#formUsuarioEdit #password').val(),
			password_confirmation: $('#formUsuarioEdit #password_confirmation').val(),

			nombres: $('#formUsuarioEdit #nombres').val(),
			apellidos: $('#formUsuarioEdit #apellidos').val(),
			cargo: $('#formUsuarioEdit #cargo').val(),

//			empresa: $('#formUsuarioEdit #empresa').val(),
			empresa: $('#formUsuarioEdit #searchProveedor').val(),
			telefono: $('#formUsuarioEdit #telefono').val(),
			nit: $('#formUsuarioEdit #nit').val(),

//			idRole: $('#idRole').val(),  // NO EDITABLE EN ESTA INSTANCIA, ESTA EN EL TAB DE AL LADO
			idTipo: $('#idTipo').val(),
			idEstado: $('#idEstado').val(),

		};

		ajaxPatch(url, data, function(data){
			$(document).trigger('formUsuarioEdit:aceptar');
		});
	}

	function agendaCrear(){
		alert('Temporalmente deshabilitado, funcion para crear agendas desactivada.');
		return false;
		var url = '';
		var modal = openModal(url, 'Nueva Agenda');
		setModalHandler('formUsuarioEdit:aceptar', function(){
			dismissModal(modal);
		});
	}

	function asignarRoles(){
		var ids = [];
		$('.chkRole').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});

		var url = '{{ route('admin.usuario.asignarRoles') }}';
		var data = {
			idUsuario: {{ $usuario->id }},
			idRoles: ids
		}

		$('.chkRole').each(function() {
			$(this).prop('checked', false);
		});

		ajaxPost(url, data, function(data){
			console.log(data.data);
			$.each(data.data, function(k, v){
				console.log(v);
				$('#chkRole_'+v).prop('checked', true);
			});
		});
	}

	function usuarioCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formUsuarioEdit:cancelar');
		@else
			redirect('{{ route('admin.usuario.index')  }}');
		@endif
	}

	function editarRole(id){
		if(id === undefined){
			var id = $('#searchRoles').val();
		}

		var url = '{{ route('admin.role.index') }}/'+id+'/edit';
		var modal = openModal(url, 'Editar Role', null, { size: 'modal-lg' });
		setModalHandler('formRoleEdit:cancelar', function(){

			@if($isAjaxRequest)
			reloadModal('#formPermisoEdit');
			@endif

			dismissModal(modal);
		});
	}
	//function editRole

	function proveedorCreate(){
		var url = '{{ route('admin.proveedor.create') }}';
		var modal = openModal(url, 'Nuevo Proveedor');
		setModalHandler('formProveedorCreate:aceptar', function(event, data){
			var proveedor = data;
			var option = $('<option>', {
				value: proveedor.id
			}).html(proveedor.nombre);
			$('#formUsuarioEdit #searchProveedor').html('');
			$('#formUsuarioEdit #searchProveedor').append(option);
			$('#formUsuarioEdit #searchProveedor').append(option);

			var telefonos = [];
			if( proveedor.telefonos ) {
				for(var i = 0 ; i < proveedor.telefonos.length ; i++) {
					telefonos.push(proveedor.telefonos[i].numero);
				}
				$('#formUsuarioEdit #lblTelefonos').html(telefonos.join(', '));
			}
			$('#formUsuarioEdit #lblNit').html(proveedor.nit);

			dismissModal(modal);
		});
	}

	function proveedorEdit(id){
		@if( get_class($usuario->Owner) == 'App\Models\ProveedorContacto' )
			var url = '{{ route('admin.proveedor.edit', ['idProveedor']) }}';
			url = url.replace('idProveedor', id);
			var modal = openModal(url, 'Editar Proveedor');
			setModalHandler('formProveedorEdit:aceptar', function(event, data){
				var proveedor = data;
				var option = $('<option>', {
					value: proveedor.id
				}).html(proveedor.nombre);
				$('#formUsuarioEdit #searchProveedor').html('');
				$('#formUsuarioEdit #searchProveedor').append(option);
				$('#formUsuarioEdit #searchProveedor').append(option);

				var telefonos = [];
				if( proveedor.telefonos ) {
					for(var i = 0 ; i < proveedor.telefonos.length ; i++) {
						telefonos.push(proveedor.telefonos[i].numero);
					}
					$('#formUsuarioEdit #lblTelefonos').html(telefonos.join(', '));
				}
				$('#formUsuarioEdit #lblNit').html(proveedor.nit);

				dismissModal(modal);
			});
		@else
			console.log("do nothing");
			alert('Funcion habilitada sólo para Clientes');
		@endif
	}

</script>
@endpush