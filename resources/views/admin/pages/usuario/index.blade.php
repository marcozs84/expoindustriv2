@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="/home">Home</a></li>
		<li class="breadcrumb-item active">Usuarios</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Usuarios <small></small></h1>
	<!-- end page-header -->


	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Búsqueda</h4>
		</div>
		<div id="formSearchGasto" class="panel-body p-b-0">
			<div class="row" id="searchForm">

				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="idUsuario" style="">Tipo</label>
					{!! Form::select('usuarioTipo', [
						\App\Models\Usuario::TIPO_ADMINISTRADOR => 'ADMINISTRADOR',
						\App\Models\Usuario::TIPO_EMPLEADO => 'EMPLEADO',
						\App\Models\Usuario::TIPO_CLIENTE => 'CLIENTE',
					], null, [
						'id' => 'usuarioTipo',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>

				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="idUsuario" style="">Estado</label>
					{!! Form::select('usuarioEstado', $estados, [
						1
					], [
						'id' => 'usuarioEstado',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="usuarioCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="usuarioDelete()"><i class="fa fa-trash"></i> Eliminar</a>
					<a class="btn btn-warning btn-sm" href="javascript:;" onclick="usuarioRestore()"><i class="fa fa-recycle"></i> Restaurar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:;" onclick="buscar()"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:;" onclick="limpiar()">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Usuarios</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	@if($isAjaxRequest)
		$(function(){ initDataTable(); });
	@else
		initDataTable();
	@endif

	function initDataTable(){

		$('#usuarioEstado, #usuarioTipo').select2()
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});

		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.usuario.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.estado = $('#usuarioEstado').val();
						d.idTipo = $('#usuarioTipo').val();
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ data: 'username', title: 'Usuario'},
					{ data: 'cargo', title: 'Cargo'},
					{ data: 'idTipo', title: 'Tipo'},
					{ data: 'estado', title: 'Estado'},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
							return '<a href="javascript:;" onclick="usuarioEdit('+row.id+')">'+ data +'</a>';
//							return row.pais.nombre;
						}
					},{
						targets: 2,
					}, {
						targets: 3,
						className: 'text-center',
						width: '170px',
						render: function(data, type, row){
							if(parseInt(row.idTipo) === {{ \App\Models\Usuario::TIPO_ADMINISTRADOR }}){
								return '<i class="fa fa-user"></i> ADMINISTRADOR';
							} else if(parseInt(row.idTipo) === {{ \App\Models\Usuario::TIPO_EMPLEADO }}){
								return 'Funcionario';
							} else if(parseInt(row.idTipo) === {{ \App\Models\Usuario::TIPO_CLIENTE }}){
								return 'Cliente';
							} else {
								return data;
							}

							return '';
						}
					}, {
						targets: 4,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){

							if( row.deleted_at !== null ) {
								return '<a href="javascript:;" data-toggle="tooltip" data-title="Eliminado"><i class="fa fa-ban text-red"></i></a>';
							}

							if(row.estado === {{ \App\Models\Usuario::ESTADO_ACTIVO }}){
								return '<a href="javascript:;" data-toggle="tooltip" data-title="Activo"><i class="fa fa-check"></i></a>';
							} else if(row.estado === {{ \App\Models\Usuario::ESTADO_INACTIVO }}){
								return '<a href="javascript:;" data-toggle="tooltip" data-title="Inactivo"><i class="fa fa-exclamation-triangle text-red"></i></a>';
							} else if(row.estado === {{ \App\Models\Usuario::ESTADO_ESPERANDO_CONFIRMACION }}){
								return '<a href="javascript:;" data-toggle="tooltip" data-title="Esperando confirmacion"><i class="fa fa-envelope"></i></a>';
							} else if(row.estado === {{ \App\Models\Usuario::ESTADO_REPORTADO }}){
								return '<a href="javascript:;" data-toggle="tooltip" data-title="Reportado"><i class="fa fa-bomb text-red"></i></a>';
							}

							return '';

//							return '<input id="chk_'+ row.id +'" type="checkbox" class="chkActivo" name="id[]" value="'+row.id+'" '+ checked +' onclick="updateEstado('+row.id+', this)" />';
						}
					}, {
						targets: 5,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
				$('[data-toggle="tooltip"]').tooltip()
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function usuarioShow(id){
		var url = '{{ route('admin.usuario.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Usuario', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formUsuarioShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}

	{{--function updateEstado(id, e){--}}
		{{--var url = '{{ route('resource.usuario.update', [0]) }}';--}}
		{{--url = url.replace('0', id);--}}
		{{--var estado = undefined;--}}
		{{--if($(e).prop('checked')){--}}
			{{--estado = 1;--}}
		{{--} else {--}}
			{{--estado = 0;--}}
		{{--}--}}

		{{--var data = {--}}
			{{--estado: estado--}}
		{{--}--}}
		{{--ajaxPatch(url, data, function(data){--}}
			{{--if(data.data.estado == 1){--}}
				{{--$(e).prop('checked', true);--}}
			{{--} else {--}}
				{{--$(e).prop('checked', false);--}}
			{{--}--}}
		{{--}, function(data){--}}
			{{--if($(e).prop('checked')){--}}
				{{--$(e).prop('checked', false);--}}
			{{--} else {--}}
				{{--$(e).prop('checked', true);--}}
			{{--}--}}
		{{--})--}}
	{{--}--}}

	function usuarioCreate(){
		var url = '{{ route('admin.usuario.create') }}';
		var modal = openModal(url, 'Nuevo Usuario');
		setModalHandler('formUsuarioCreate:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function usuarioEdit(id){
		var url = '{{ route('admin.usuario.index') }}/'+id+'/edit';
		var modal = openModal(url, 'Editar Usuario', null, { size:'modal-lg' });
		setModalHandler('formUsuarioEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function usuarioDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '{{ route('resource.usuario.destroy', [0]) }}';

		ajaxDelete(url, ids, function(){
			datatable.ajax.reload(null, false);
		});
	}

	function usuarioRestore(){
		console.log("restoring");
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '{{ route('resource.usuario.restore', [0]) }}';

		ajaxPost(url, { ids:ids }, function(){
			datatable.ajax.reload(null, false);
		});
	}

</script>
@endpush