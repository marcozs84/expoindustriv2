@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
	{{-- #### SOLUCIONADO EL PROBLEMA DE CERRAR EL COMBOBOX AUMENTANDO EL MARGEN --}}
	{{--<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />--}}
	<!-- ================== END PAGE LEVEL STYLE ================== -->
	<style>
		.col-mz{
			/*max-width:150px !important;*/
			word-wrap: break-word !important;
			white-space:pre-wrap !important;
			text-overflow: ellipsis !important;
		}
		/*td {word-wrap: break-word !important;}*/
	</style>
@endpush

@section('content')

	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="javascript:;">Admin</a></li>
		<li><a href="javascript:;">Usuarios</a></li>
		<li class="active">Crear</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Nuevo Usuario <small></small></h1>
	<!-- end page-header -->

	<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
		<div class="col-md-12">
			<!-- begin panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Datos personales</h4>
				</div>
				<div class="panel-body">

					<form id="formUsuarioCreate" class="form-horizontal">

						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4">Nombre de usuario (email)</label>
							<div class="col-md-8">
								{!! Form::text('username', '---', [
									'id' => 'username',
									'class' => 'form-control',
									'placeholder' => 'Email/username',
									'autocomplete' => 'off',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4">Contraseña</label>
							<div class="col-md-8">
								{!! Form::password('password', [
									'id' => 'password',
									'value' => '',
									'class' => 'form-control',
									'placeholder' => 'Contraseña',
									'autocomplete' => 'off'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4">Confirmar contraseña</label>
							<div class="col-md-8">
								{!! Form::password('password_confirmation', [
									'id' => 'password_confirmation',
									'class' => 'form-control',
									'placeholder' => 'Confirmar contraseña',
									'autocomplete' => 'off'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4"></label>
							<div class="col-md-8">
								<a href="http://passwordsgenerator.net/es/" target="_blank">Generador de contraseñas</a>
							</div>
						</div>
						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4">Tipo</label>
							<div class="col-md-8">
								@if($idTipo === null)
									{!! Form::select('idTipo', [
											\App\Models\Usuario::TIPO_EMPLEADO => 'Empleado',
											\App\Models\Usuario::TIPO_ADMINISTRADOR => 'Administrador',
											\App\Models\Usuario::TIPO_CLIENTE => 'Cliente',
											//\App\Models\Usuario::TIPO_PROVEEDOR => 'Proveedor (use Cliente instead)',
										], $idTipo, [
										'id' => 'idTipo',
										'class' => 'form-control default-select2',
										'autofocus',
										'onchange' => 'accesoChange(this)'
									]) !!}
								@else
									{!! Form::select('idTipo', [
											\App\Models\Usuario::TIPO_EMPLEADO => 'Empleado',
											\App\Models\Usuario::TIPO_ADMINISTRADOR => 'Administrador',
											\App\Models\Usuario::TIPO_CLIENTE => 'Cliente',
											//\App\Models\Usuario::TIPO_PROVEEDOR => 'Proveedor (use Cliente instead)',
										], $idTipo, [
										'id' => 'idTipo',
										'class' => 'form-control default-select2',
										'autofocus',
										'onchange' => 'accesoChange(this)',
										'disabled'
									]) !!}
								@endif
							</div>
						</div>
						<div class="form-group row m-b-15 group-empleado">
							<label class="col-form-label col-md-4">Role inicial</label>
							<div class="col-md-8">
								{!! Form::select('idRole', $roles, null, [
									'id' => 'idRole',
									'class' => 'form-control default-select2',
									'autofocus'
								]) !!}
							</div>
						</div>

						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4">Nombres</label>
							<div class="col-md-8">
								{!! Form::text('nombres', '', [
									'id' => 'nombres',
									'class' => 'form-control',
									'placeholder' => 'Nombres',
								]) !!}
							</div>
						</div>

						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4">Apellidos</label>
							<div class="col-md-8">
								{!! Form::text('apellidos', '', [
									'id' => 'apellidos',
									'class' => 'form-control',
									'placeholder' => 'Apellidos',
								]) !!}
							</div>
						</div>

						<div class="form-group row m-b-15 group-empleado">
							<label class="col-form-label col-md-4">Cargo</label>
							<div class="col-md-8">
								{!! Form::text('cargo', '', [
									'id' => 'cargo',
									'class' => 'form-control',
									'placeholder' => 'Cargo',
								]) !!}
							</div>
						</div>

						<div class="form-group row m-b-15 group-cliente">
							<label class="col-form-label col-md-4">Empresa<br><small>Obligatorio si es Tipo "Cliente"</small></label>
							<div class="col-md-8">
								{{--{!! Form::text('empresa', '', [--}}
									{{--'id' => 'empresa',--}}
									{{--'class' => 'form-control',--}}
									{{--'placeholder' => 'Empresa',--}}
								{{--]) !!}--}}
								{!! Form::select('searchProveedor', [], null, [
									'id' => 'searchProveedor',
									'class' => 'form-control default-select2',
									'autofocus'
								]) !!}
								<a href="javascript:;" class="btn btn-primary" onclick="proveedorCreate()" style="margin-top:5px;">Nuevo Proveedor</a>
								<a href="javascript:;" class="btn btn-primary btnProveedorEdit hide" onclick="proveedorEdit()" style="margin-top:5px;">Editar Proveedor</a>
							</div>
						</div>

						<div class="form-group row m-b-15 group-cliente">
							<label class="col-form-label col-md-4">Teléfono(s) Empresa</label>  <!-- <br><small>Obligatorio si es Tipo "Cliente"<br>Separados por coma</small> -->
							<div class="col-md-8 p-t-5" id="lblTelefonos">
								{{--{!! Form::text('telefono', '', [--}}
									{{--'id' => 'telefono',--}}
									{{--'class' => 'form-control',--}}
									{{--'placeholder' => 'Teléfono',--}}
								{{--]) !!}--}}
							</div>
						</div>

						<div class="form-group row m-b-15 group-empleado group-cliente">
							<label class="col-form-label col-md-4">Teléfono(s)</label>  <!-- <br><small>Obligatorio si es Tipo "Cliente"<br>Separados por coma</small> -->
							<div class="col-md-8 p-t-5">
								{!! Form::text('telefono', '', [
									'id' => 'telefono',
									'class' => 'form-control',
									'placeholder' => 'Teléfono',
								]) !!}
							</div>
						</div>

						<div class="form-group row m-b-15 group-cliente">
							<label class="col-form-label col-md-4">NIT<br><small>Obligatorio si es Tipo "Cliente"</small></label>
							<div class="col-md-8">
								{!! Form::text('nit', '', [
									'id' => 'nit',
									'class' => 'form-control',
									'placeholder' => 'NIT',
								]) !!}
							</div>
						</div>

						<div class="form-group row m-b-15 group-cliente group-empleado">
							<label class="col-form-label col-md-4">Estado</label>
							<div class="col-md-8">
								{!! Form::select('idEstado', [
										\App\Models\Usuario::ESTADO_ACTIVO => 'Activo',
										\App\Models\Usuario::ESTADO_INACTIVO => 'Inactivo',
										\App\Models\Usuario::ESTADO_ESPERANDO_CONFIRMACION => 'Esperando confirmacion',
										\App\Models\Usuario::ESTADO_REPORTADO => 'Reportado',
									], null, [
									'id' => 'idEstado',
									'class' => 'form-control default-select2',
									'autofocus'
								]) !!}
							</div>
						</div>


						<div class="form-group text-right group-cliente group-empleado">
							<div class="col-md-12">
								<a class="btn btn-white btn-sm btn-warning" href="javascript:;" onclick="agendaCrear()">Crear Agenda</a>
								<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" >Cerrar</a>
								<a class="btn btn-primary btn-sm" href="javascript:;" onclick="usuarioGuardar()" >Guardar</a>
							</div>
						</div>

					</form>
				</div>
			</div>
			<!-- end panel -->
		</div>
		<!-- end col-12 -->
	</div>
	<!-- end row -->

@stop

@push('scripts')
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/plugins/masked-input/masked-input.min.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/default_assets/plugins/parsley/dist/parsley.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>--}}
<!-- ================== END PAGE LEVEL JS ================== -->

<script>

	$.getScript('/assets/plugins/select2/dist/js/select2.min.js').done(function() {
//		$('.multiple-select2, .default-select2').select2();  // Commented, because overrides parent modal windows select2 instances.
		$('#idTipo').on('select2:select', function(e) {
			accesoChange();
		});
		formInit();
	});

	{{--@if($isAjaxRequest)--}}
		{{--formInit();--}}
	{{--@else--}}
		{{--$(function(){ formInit(); });--}}
	{{--@endif--}}

	var form_user = null;
	var form_agenda_nueva = null;
	var form_agenda_existente = null;

	function formInit(){

		if(parseInt($('#idTipo').val()) === {{ \App\Models\Usuario::TIPO_CLIENTE }}) {
			$('.group-cliente, .group-empleado').addClass('hide');
			$('.group-cliente').removeClass('hide');
		} else if (parseInt($('#idTipo').val()) === {{ \App\Models\Usuario::TIPO_EMPLEADO }} || $('#idTipo').val() === {{ \App\Models\Usuario::TIPO_ADMINISTRADOR }}) {
			$('.group-cliente, .group-empleado').addClass('hide');
			$('.group-empleado').removeClass('hide');
		}

		$('#formUsuarioCreate #searchProveedor').select2({
//        allowClear: true,
			placeholder: {
				id: "",
				placeholder: "Leave blank to ..."
			},
			ajax: {
				url: '{{ route('resource.proveedor.ds') }}',
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token' : '{!! csrf_token() !!}'
				},
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page
					};
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function (data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: function(repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if (repo.loading) return repo.text;

				var proveedoresHtml = '';
				for(var i = 0 ; i < repo.proveedor_contactos.length ; i++) {
					proveedoresHtml += repo.proveedor_contactos[i].agenda.nombres_apellidos + "<br>";
				}

				var markup = "<div><b>" + repo.nombre + '</b><br>' + proveedoresHtml + "</div>";
//				var markup = "<div><b>" + repo.nombre + '</b><br>' + repo.descripcion + "</div>";
				return markup;
			},
			templateSelection: function formatRepoSelection (repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if(repo.nombre){
					var telefonos = [];
					if( repo.telefonos ) {
						for(var i = 0 ; i < repo.telefonos.length ; i++) {
							telefonos.push(repo.telefonos[i].numero);
						}
						$('#formUsuarioCreate #lblTelefonos').html(telefonos.join(', '));
					}
					$('#formUsuarioCreate .btnProveedorEdit').attr('onclick', 'proveedorEdit('+ repo.id +')');
					$('#formUsuarioCreate .btnProveedorEdit').removeClass('hide');

					// ----------

					return repo.nombre;
				} else {
					console.log("returning test");
					return repo.text;
				}
				//return repo.nombre + ' ' + repo.apellidos || repo.text;
			},
//	    allowClear: true
		})
			.trigger('change');

	};

	function usarEmail(email){
		$('#username').val(email);
	}

	function abrirUsuarioExistente(id){
		var url = '{{ route('admin.usuario.index') }}/'+id+'/edit';
		var modal = openModal(url, 'Editar Usuario');
		setModalHandler('formUsuarioEdit:aceptar', function(){
			dismissModal(modal);
		});
		$(document).trigger('formUsuarioCreate:cancelar');
	}

	function usuarioGuardar(){

		var url = '{{ route('admin.usuario.store') }}';
		var data = {
			username: $('#formUsuarioCreate #username').val().trim(),
			password: $('#formUsuarioCreate #password').val(),
			password_confirmation: $('#formUsuarioCreate #password_confirmation').val(),

			nombres: $('#formUsuarioCreate #nombres').val(),
			apellidos: $('#formUsuarioCreate #apellidos').val(),
			cargo: $('#formUsuarioCreate #cargo').val(),

//			empresa: $('#empresa').val(),
			empresa: $('#formUsuarioCreate #searchProveedor').val(),
			telefono: $('#formUsuarioCreate #telefono').val(),
			nit: $('#formUsuarioCreate #nit').val(),

			idRole: $('#formUsuarioCreate #idRole').val(),
			idTipo: $('#formUsuarioCreate #idTipo').val(),
			idEstado: $('#formUsuarioCreate #idEstado').val(),
		};

		ajaxPost(url, data, function(data){
			$(document).trigger('formUsuarioCreate:aceptar', data.data);
		});
	}

	function accesoChange() {
		if(parseInt($('#formUsuarioCreate #idTipo').val()) === {{ \App\Models\Usuario::TIPO_CLIENTE }}) {
			$('#formUsuarioCreate .group-cliente, .group-empleado').addClass('hide');
			$('#formUsuarioCreate .group-cliente').removeClass('hide');
		} else if (parseInt($('#formUsuarioCreate #idTipo').val()) === {{ \App\Models\Usuario::TIPO_EMPLEADO }} || parseInt($('#idTipo').val()) === {{ \App\Models\Usuario::TIPO_ADMINISTRADOR }}) {
			$('#formUsuarioCreate .group-cliente, .group-empleado').addClass('hide');
			$('#formUsuarioCreate .group-empleado').removeClass('hide');
		}
	}

	function agendaCrear(){
		alert('Temporalmente deshabilitado, funcion para crear agendas desactivada.');
		return false;
		var url = '';
		var modal = openModal(url, 'Nueva Agenda');
		setModalHandler('formAgendaCreate:aceptar', function(){
			dismissModal(modal);
		});
	}

	function proveedorCreate(){
		var url = '{{ route('admin.proveedor.create') }}';
		var modal = openModal(url, 'Nuevo Proveedor');
		setModalHandler('formProveedorCreate:aceptar', function(event, data){
			var proveedor = data;
			var option = $('<option>', {
				value: proveedor.id
			}).html(proveedor.nombre);
			$('#formUsuarioCreate #searchProveedor').html('');
			$('#formUsuarioCreate #searchProveedor').append(option);
//			$('#formUsuarioCreate #searchProveedor').append(option);

			var telefonos = [];
			if( proveedor.telefonos ) {
				for(var i = 0 ; i < proveedor.telefonos.length ; i++) {
					telefonos.push(proveedor.telefonos[i].numero);
				}
				$('#formUsuarioCreate #lblTelefonos').html(telefonos.join(', '));
			}

			dismissModal(modal);
		});
	}

	function proveedorEdit(id){
		var url = '{{ route('admin.proveedor.edit', ['idProveedor']) }}';
		url = url.replace('idProveedor', id);
		var modal = openModal(url, 'Editar Proveedor');
		setModalHandler('formProveedorEdit:aceptar', function(event, data){
			var proveedor = data;
			var option = $('<option>', {
				value: proveedor.id
			}).html(proveedor.nombre);
			$('#formUsuarioCreate #searchProveedor').html('');
			$('#formUsuarioCreate #searchProveedor').append(option);
			$('#formUsuarioCreate #searchProveedor').append(option);

			var telefonos = [];
			if( proveedor.telefonos ) {
				for(var i = 0 ; i < proveedor.telefonos.length ; i++) {
					telefonos.push(proveedor.telefonos[i].numero);
				}
				$('#formUsuarioCreate #lblTelefonos').html(telefonos.join(', '));
			}

			dismissModal(modal);
		});
	}

</script>
@endpush