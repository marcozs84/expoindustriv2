@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.modelo.index') }}">Modelo</a></li>
		<li class="breadcrumb-item active">Nuevo/a modelo</li>
	</ol>

	<h1 class="page-header">Nuevo Modelo</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Modelo</h4>
		</div>
		<div class="panel panel-body">
			<form id="formModeloCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Marca</label>
					<div class="col-8">
						{!! Form::select('idMarca', [], '', [
							'id' => 'idMarca',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Marca</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idMarca', '', [--}}
							{{--'id' => 'idMarca',--}}
							{{--'placeholder' => 'Id Marca',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', '', [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                        {!! Form::textarea('descripcion', '', [
                            'id' => 'descripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="modeloCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="modeloSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){





	$idMarcaSearch = Search.resource('#formModeloCreate #idMarca', {
		url: '{{ route('resource.marca.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		}
	});



}

$( "#formModeloCreate" ).submit(function( event ) {
	modeloSave();
	event.preventDefault();
});

function modeloSave(){
	var url = '{{ route('resource.modelo.store') }}';
	ajaxPost(url, {
		idMarca: 		$('#formModeloCreate #idMarca').val(),
		nombre: 		$('#formModeloCreate #nombre').val(),
		descripcion: 	$('#formModeloCreate #descripcion').val()
	}, function(){
		$(document).trigger('formModeloCreate:success');
	});
}

function modeloCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formModeloCreate:cancel');
	@else
		redirect('{{ route('admin.modelo.index')  }}');
	@endif
}

</script>
@endpush

