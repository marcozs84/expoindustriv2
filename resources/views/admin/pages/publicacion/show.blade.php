@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
	.progressBar {
		width:60%;
		height:20px;
		margin:auto;
	}
	.progressBar .bar {
		width:100%;
		border:1px solid #666666;
		background-color:#e5e5e5;
		height:18px;
		margin:0px;
		padding:0px;
	}
	.progressBar .caret {
		margin:0;
		padding:0;
		margin-top:-7px;
		padding-top:5px;
		width:0;
		background:none;
		border:none;
		border-right:3px solid #ff0000;
		height:25px;
		padding-right:5px;
		font-weight:700;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Anuncios</li>
	</ol>

	<h1 class="page-header">Anuncio</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Anuncio</h4>
		</div>
		<div class="panel panel-body">
			<form id="formAnuncioCreate" method="get" class="form-horizontal">
				<h5>Anuncio ( ID: {{ $pub->id }} )</h5>
				<table class="table table-condensed table-striped">
					<tr>
						<td class="text-right">Desde</td>
						<td><b>{{ $pub->fechaInicio->format('d M, Y') }}</b></td>
						<td class="text-right">Hasta</td>
						<td><b>{{ $pub->fechaFin->format('d M, Y') }}</b></td>
					</tr>
					<tr>
						<td colspan="4">
							@php
								$total = $pub->fechaFin->diffInDays($pub->fechaInicio);
								$actual = \Carbon\Carbon::today()->diffInDays($pub->fechaInicio);
								$progress = ( ( $actual * 100 ) / $total );
							@endphp
							<div class="progressBar">
								<div class="bar">
									<div class="caret text-right" style="width:{{ $progress }}%;">{{ round($progress) }} %</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text-right">Duración</td>
						<td><b>{{ $pub->fechaFin->diffInMonths($pub->fechaInicio) }} Months</b> ( {{ $pub->fechaFin->diffInDays($pub->fechaInicio) }} Days )</td>
						<td class="text-right">Faltan</td>
						<td><b>{{ $pub->fechaFin->diffInMonths(\Carbon\Carbon::today()) }} Months</b> ( {{ $pub->fechaFin->diffInDays(\Carbon\Carbon::today()) }} Days )</td>
					</tr>
					<tr>
						<td class="text-right">Estado</td>
						<td>{{ __('messages.'.$pub->Estado->texto) }}</td>
						<td class="text-right">Precio</td>
						<td>{{ $pub->precio }} {{ strtoupper($pub->moneda) }}</td>
					</tr>
				</table>

				<h5>Producto ( ID: {{ $pub->idProducto }} )</h5>
				@if($pub->Producto)

					<table class="table table-condensed table-striped">
						<tr>
							<td class="text-right">Marca</td>
							<td>
								@if($pub->Producto->Modelo)
									{{ $pub->Producto->Marca->nombre }}
								@endif
								( {{ $pub->Producto->idMarca }} )
							</td>
							<td class="text-right">idModelo</td>
							<td>{{ $pub->Producto->idModelo }}
								@if($pub->Producto->Modelo)
									({{ $pub->Producto->Modelo->nombre }})
								@endif
							</td>
						</tr>
						<tr>
							<td class="text-right">idProductoTipo</td>
							<td>{{ $pub->Producto->idProductoTipo }}</td>
							<td class="text-right">idProductoEstado</td>
							<td>{{ $pub->Producto->idProductoEstado }}</td>
						</tr>
						<tr>
							<td class="text-right">owner_type</td>
							<td>{{ $pub->Producto->owner_type }}</td>
							<td class="text-right">owner_id</td>
							<td>{{ $pub->Producto->owner_id }}</td>
						</tr>
						<tr>
							<td class="text-right">codigo</td>
							<td>{{ $pub->Producto->codigo }}</td>
							<td class="text-right">nombre</td>
							<td>{{ $pub->Producto->nombre }}</td>
						</tr>
						<tr>
							<td class="text-right">precioUnitario</td>
							<td>{{ $pub->Producto->precioUnitario }}</td>
							<td class="text-right">monedaUnitario</td>
							<td>{{ $pub->Producto->monedaUnitario }}</td>
						</tr>
						<tr>
							<td class="text-right">precioProveedor</td>
							<td>{{ $pub->Producto->precioProveedor }}</td>
							<td class="text-right">monedaProveedor</td>
							<td>{{ $pub->Producto->monedaProveedor }}</td>
						</tr>
					</table>


					<h5>ProductoItem</h5>
					@if($pub->Producto->ProductoItem)

						<table class="table table-condensed table-striped">
							<tr>
								<td class="text-right">definicion</td>
								<td>
									@if($pub->Producto->ProductoItem->definicion)
										<table class="table table-condensed">

											@foreach($pub->Producto->ProductoItem->definicion as $key => $value)
												<tr>
													<td style="width:30%;">{{ str_replace('_', ' ', $key) }}</td>
													<td>
														@if(!is_array($value) )
															{{ $value }} <br>
														@else
															<br>
															@foreach($value as $item)
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $item }} <br>
															@endforeach
														@endif
													</td>
												</tr>
											@endforeach

										</table>
									@endif
								</td>
								<td class="text-right">dimension</td>
								<td style="width:30%">
									@if($pub->Producto->ProductoItem->dimension)
										@foreach($pub->Producto->ProductoItem->dimension as $key => $value)
											{{ $key }} => {{ $value }} <br>
										@endforeach
									@endif
								</td>
							</tr>
						</table>

						<h5>Ubicacion</h5>
						@if($pub->Producto->ProductoItem->Ubicacion)

							<table class="table table-condensed table-striped">
								<tr>
									<td class="text-right">pais</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->pais }}</td>
									<td class="text-right">ciudad</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->ciudad }}</td>
								</tr>
								<tr>
									<td class="text-right">codigo postal (cpostal)</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->cpostal }}</td>
									<td class="text-right">direccion</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->direccion }}</td>
								</tr>
							</table>
						@else
							<p>No tiene Ubicacion relacionada</p>
						@endif
					@else
						<p>No tiene ProductoItem relacionado</p>
					@endif

					<h5>Galerias</h5>
					@if($pub->Producto->Galerias)

						<table class="table table-condensed table-striped">
							@foreach($pub->Producto->Galerias as $gal)
								<tr>
									<td class="text-right">idGaleriaTipo</td>
									<td>{{ $gal->idGaleriaTipo }}</td>
									<td class="text-right">Imagenes</td>
									<td>{{ $gal->Imagenes->count() }}</td>
								</tr>
								<tr>
									<td colspan="2" class="text-right">
										@foreach($gal->Imagenes as $img)
											<img src="{{ $img->ruta_publica_producto_thumb }}" alt="" class="img-fluid" style="max-width:100px;">
										@endforeach
									</td>
								</tr>
							@endforeach
						</table>
					@else
						<p>No tiene ProductoItem relacionado</p>
					@endif

				@else
					<p>No tiene producto relacionado</p>
				@endif

			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-danger btn-sm" data-dismiss="modal" href="javascript:;" onclick="publicacionEdit({{ $pub->id }})">Edit</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="publicacionSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('libraries')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
@endpush

@push('scripts')
	<script>

		function formInit(){
		}
		formInit();

	</script>
@endpush

