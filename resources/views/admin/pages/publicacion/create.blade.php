@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.publicacion.index') }}">Publicacion</a></li>
		<li class="breadcrumb-item active">Nuevo/a publicacion</li>
	</ol>

	<h1 class="page-header">Nuevo Publicacion</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Publicacion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formPublicacionCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Inicio</label>
					<div class="col-8">
						{!! Form::text('fechaInicio', '', [
							'id' => 'fechaInicio',
							'placeholder' => 'Fecha Inicio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Fin</label>
					<div class="col-8">
						{!! Form::text('fechaFin', '', [
							'id' => 'fechaFin',
							'placeholder' => 'Fecha Fin',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-12 row m-b-15 group-cliente">
					<label class="col-form-label col-4">Maquina</label>
					<div class="col-8">
						{!! Form::select('searchProducto', [], null, [
							'id' => 'searchProducto',
							'class' => 'form-control default-select2',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Tipo de Pago</label>
					<div class="col-8">
						{!! Form::select('idTipoPago', [
							\App\Models\Publicacion::PAGO_NONE => \App\Models\Publicacion::PAGO_NONE.' - Ninguno',
							\App\Models\Publicacion::PAGO_STRIPE => \App\Models\Publicacion::PAGO_STRIPE . ' - Stripe',
							\App\Models\Publicacion::PAGO_FAKTURA => \App\Models\Publicacion::PAGO_FAKTURA . ' - Faktura',
							\App\Models\Publicacion::PAGO_SUBSCRIPTION => \App\Models\Publicacion::PAGO_SUBSCRIPTION . ' - Subscripcion',
						], null, [
							'id' => 'idTipoPago',
							'class' => 'form-control select2-default',
						]) !!}
					</div>
				</div>

				<div class="form-group col-md-6 row m-b-15 group-cliente">
					<label class="col-form-label col-md-4" style="padding-top:0px;">Empresa / Cliente</label>
					<div class="col-md-8">
						{!! Form::select('searchCliente', [], null, [
							'id' => 'searchCliente',
							'class' => 'form-control default-select2',
							'autofocus'
						]) !!}
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::select('estado', [
							\App\Models\Publicacion::ESTADO_DRAFT => \App\Models\Publicacion::ESTADO_DRAFT.' - Borrador',
							\App\Models\Publicacion::ESTADO_AWAITING_APPROVAL => \App\Models\Publicacion::ESTADO_AWAITING_APPROVAL.' - Esperando aprobación',
							\App\Models\Publicacion::ESTADO_APPROVED=> \App\Models\Publicacion::ESTADO_APPROVED.' - Aprobado',
							\App\Models\Publicacion::ESTADO_REJECTED=> \App\Models\Publicacion::ESTADO_REJECTED.' - Rechazado',
							\App\Models\Publicacion::ESTADO_DISABLED=> \App\Models\Publicacion::ESTADO_DISABLED.' - Inactivo',
						], \App\Models\Publicacion::ESTADO_AWAITING_APPROVAL, [
							'id' => 'estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Dias</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('dias', '', [--}}
							{{--'id' => 'dias',--}}
							{{--'placeholder' => 'Dias',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Precio</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::text('precio', '', [--}}
							{{--'id' => 'precio',--}}
							{{--'placeholder' => 'Precio',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Moneda</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::text('moneda', '', [--}}
							{{--'id' => 'moneda',--}}
							{{--'placeholder' => 'Moneda',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="publicacionCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="publicacionSave(false)" >Guardar</a>
					<a class="btn btn-danger btn-sm" href="javascript:;" onclick="publicacionSave(true)" >Guardar e ir al anuncio</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif

var form = $('#formPublicacionCreate');
form.submit(function( event ) {
	publicacionSave();
	event.preventDefault();
});

var idUsuarioPropietario = 0;

function publicacionSave(redirectTo){
	var url = '{{ route('resource.publicacion.store') }}';

	ajaxPost(url, {
		idProducto: 	form.find('#searchProducto').val(),
		idTipoPago: 	form.find('#idTipoPago').val(),
		idUsuario: 	    idUsuarioPropietario, //form.find('#searchCliente').val(),
		fechaInicio: 	form.find('#fechaInicio').val(),
		fechaFin: 		form.find('#fechaFin').val(),
		estado: 		form.find('#estado').val(),
	}, function( data ){
		$(document).trigger('formPublicacionCreate:success');
		if( redirectTo === true ) {
			var url = '{{ route('admin.announcementInbox.show', 'idAnnouncement') }}';
			url = url.replace('idAnnouncement', data.data.id);
			redirect(url);
		}
	});
}

function publicacionCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formPublicacionCreate:cancel');
	@else
		redirect('{{ route('admin.publicacion.index')  }}');
	@endif
}

function formInit(){

	var form = $('#formPublicacionCreate');

	form.find('#idTipoPago').select2();
	form.find('#estado').select2();

	form.find('#fechaInicio').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		todayHighlight: true,
		orientation: 'bottom'
	})
		.datepicker("setDate",'now')
		.on('changeDate', function(){
		});

	form.find('#fechaFin').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		todayHighlight: true,
		orientation: 'bottom'
	})
		.datepicker("setDate",'{{ \Carbon\Carbon::today()->addMonths(1)->format('d/m/Y') }}')
		.on('changeDate', function(){
		});
	$("#formPublicacionCreate #fechaInicio, #formPublicacionCreate #fechaFin, #formPublicacionCreate #created_at").mask("99/99/9999");

	form.find('#searchProducto').select2({
//        allowClear: true,
		placeholder: {
			id: "",
			placeholder: "Leave blank to ..."
		},
		ajax: {
			url: '{{ route('resource.productoItem.ds') }}',
			dataType: 'json',
			method: 'POST',
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			delay: 250,
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page
				};
			},
			statusCode: {
				422: ajaxValidationHandler,
				500: ajaxErrorHandler
			},
			processResults: function (data, params) {
				// parse the results into the format expected by Select2
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data, except to indicate that infinite
				// scrolling can be used
				params.page = data.current_page || 1;

				return {
					results: data.data,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: function(repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if (repo.loading) return repo.text;

			var owner_pub = '';
			if(repo.producto) {
				owner_pub = repo.producto.owner.owner.agenda.nombres_apellidos;
				owner_pub += ' ( ' + repo.producto.owner.owner.proveedor.nombre + ')';
			}
			var markup = "<div><b>" + repo.producto.marca.nombre + ' ' + repo.producto.modelo.nombre + '</b><br>' + owner_pub + "</div>";
			return markup;
		},
		templateSelection: function formatRepoSelection (repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if(repo.producto){
				return repo.producto.marca.nombre + ' - ' + repo.producto.modelo.nombre;
			} else {
				return repo.text;
			}
			//return repo.nombre + ' ' + repo.apellidos || repo.text;
		},
//	    allowClear: true
	})
		.trigger('change');

	// ----------
	// Add here the Default User/Client  (8) Bengtmachines

	form.find('#searchCliente').select2({
//        allowClear: true,
		placeholder: {
			id: "",
			placeholder: "Leave blank to ..."
		},
		ajax: {
			{{--				url: '{{ route('resource.proveedorContacto.ds') }}',--}}
			url: '{{ route('resource.cliente.ds') }}',
			dataType: 'json',
			method: 'POST',
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			delay: 250,
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page,
					proveedor_data: 1 // Utilizado para que el 'ds' consultado retorne informacion más completa
				};
			},
			statusCode: {
				422: ajaxValidationHandler,
				500: ajaxErrorHandler
			},
			processResults: function (data, params) {
				// parse the results into the format expected by Select2
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data, except to indicate that infinite
				// scrolling can be used
				params.page = data.current_page || 1;

				return {
					results: data.data,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: function(repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if (repo.loading) return repo.text;

			var companyName = '';
			if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
				companyName = '- - -';
			} else {
				companyName = repo.usuario.owner.proveedor.nombre;
			}
			var otrosContactos = [];

			var markup = "<div>" + repo.agenda.nombres_apellidos + '<br><b>' + companyName + "</b><br>" + otrosContactos.join('<br> ') + "</div>";
			return markup;
		},
		templateSelection: function formatRepoSelection (repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if(repo.usuario){

				idUsuarioPropietario = repo.idUsuario;

				var companyName = '';
				if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
					companyName = '- - -';
				} else {
					companyName = repo.usuario.owner.proveedor.nombre;
				}

				// ----------

				var otrosContactos = [];
				if(repo.usuario.owner.proveedor.proveedor_contactos && repo.usuario.owner.proveedor.proveedor_contactos.length > 0) {
					$.each(repo.usuario.owner.proveedor.proveedor_contactos, function(k,v) {
						if(parseInt(v.id, 10) !== parseInt(repo.id, 10)) {
							otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
						}
					});
				}

				var telefonos = [];
				if(repo.usuario.owner.proveedor.telefonos && repo.usuario.owner.proveedor.telefonos.length > 0) {
					$.each(repo.usuario.owner.proveedor.telefonos, function(k,v) {
						if(v.id !== repo.id) {
							telefonos.push(v.numero);
						}
					});
				}

				var direcciones = [];
				if(repo.usuario.owner.proveedor.direcciones && repo.usuario.owner.proveedor.direcciones.length > 0) {
					$.each(repo.usuario.owner.proveedor.direcciones, function(k,v) {
						if(v.id !== repo.id) {
							direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
						}
					});
				}

				// ----------

				return repo.agenda.nombres_apellidos + ' [' + companyName + ']';
			} else {
				idUsuarioPropietario = 0;
				return repo.text;
			}
			//return repo.nombre + ' ' + repo.apellidos || repo.text;
		},
//	    allowClear: true
	})
		.trigger('change');
}

</script>
@endpush

