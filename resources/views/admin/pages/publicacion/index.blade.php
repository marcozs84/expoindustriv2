@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', ' Publicaciones')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"> Publicaciones</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"> Publicaciones <small></small></h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Filtros</h4>
		</div>
		<div id="formSearchOrden" class="panel-body p-b-0">
			<div class="row" id="searchForm">

				<div class="form-group col-lg-4 col-md-4 col-sm-6" style="vertical-align:top;">
					<label for="estados" style="">Estado</label>
					{!! Form::select('estados', [
						\App\Models\Publicacion::ESTADO_APPROVED => __('messages.approved'),
						\App\Models\Publicacion::ESTADO_DRAFT => __('messages.draft'),
						\App\Models\Publicacion::ESTADO_AWAITING_APPROVAL => __('messages.awaiting_approval'),
						\App\Models\Publicacion::ESTADO_REJECTED => __('messages.rejected'),
						\App\Models\Publicacion::ESTADO_DISABLED => __('messages.inactive'),
					], null, [
						'id' => 'estados',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
						]) !!}
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-6" style="vertical-align:top;">
					<label for="vendedorId" style="">Marcas</label>
					{!! Form::select('idMarcas', $marcas, null, [
						'id' => 'idMarcas',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
						]) !!}
				</div>
				<div class="form-group col-lg-4 col-md-4 col-sm-6" style="vertical-align:top;">
					<label for="vendedorId" style="">Modelos</label>
					{!! Form::select('idModelos', $modelos, null, [
						'id' => 'idModelos',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
						]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
					{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaCreate()"><i class="fa fa-plus"></i> Crear</a>--}}
					{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaDelete()"><i class="fa fa-trash"></i> Eliminar</a>--}}

					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="publicacionCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="publicacionDelete()"><i class="fa fa-trash"></i> Eliminar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:buscar();"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:limpiar();">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title"> Publicaciones</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>--}}
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = {!! json_encode($publicacionEstados)  !!};

	$(document).ready(function(){
		initDataTable();
		initSearchForm();
	});

	function initSearchForm() {
		$('#estados, #idMarcas, #idModelos').select2()
			.on('select2:select', function (e) {
				buscar();
			})
			.on('select2:unselect', function (e) {
				buscar();
			});
	}


	function buscar() {
		datatable.ajax.reload();
	}

	function limpiar() {
		$('#estados, #idMarcas, #idModelos').val([]).trigger('change');
		buscar();
	}

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.publicacion.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.estados = $('#estados').val();
						d.idMarcas = $('#idMarcas').val();
						d.idModelos = $('#idModelos').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                    var data = JSON.parse(data);
//	                    for(var i = 0 ; i < data.data.length; i++){
//	                        // ...
//	                    }
//	                    return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
					{ data: 'Producto', title: 'Producto'},
					{ title: 'Categoria'},
					{ title: 'Estado'},
					{ data: 'visitas', title: 'Visitas'},
					{ data: 'created_at', title: 'Fecha Creacion'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
				    @php $counter = 0 @endphp
				    {
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '20px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="publicacionShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// idProducto,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							var name = '';
							if(row.producto){
								if(row.producto.idMarca > 0){
									name += row.producto.marca.nombre;
									name += ' ';
								} else {
									{{--name += '<b>@lang('messages.no_brand')</b>';--}}
								}
								name += ' ';
								if(row.producto.idModelo > 0){
									name += row.producto.modelo.nombre;
								} else {
									{{--name += '<b>@lang('messages.no_model')</b>';--}}
								}
								if( name === '' ) {
//						name = get_trans('nombre', row.producto.producto_item.traducciones, 'en');
									name = row.producto.nombre_alter;
								}
								if(name === '') {
									name = '<span class="text-danger" style="font-weight:bold;">SIN MARCA Y MODELO</span>'
								}
//								return name;
							} else {
								name =  '<b class="text-red">@lang('messages.no_product')!</b>';
							}
							return '<a href="javascript:;" onclick="productoShow('+row.producto.id+')">'+name+'</a>'
						}
					},{
						// categoria,
						targets: {{ $counter++ }},
						render: function(data, type, row){
							var response = '';
							if(row.producto !== null){
								if(row.producto.categorias !== undefined){
									$.each(row.producto.categorias, function(key, value){
										response += value.padre.nombre + ' / ' + value.nombre;
									});
									return response;
								} else {
									return 'Sin categorias';
								}
							} else {
								return '';
							}
						}
					},{
						// estado,
						targets: {{ $counter++ }},
						render: function(data, type, row){
							return estados[row.estado];
						}
					},{
						// visitas,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// created_at,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '100px',
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';
							html += '<div class="btn-group">'+
								'<a onclick="publicacionEdit('+ row.id +')" class="btn btn-primary btn-sm width-90 text-white" data-toggle="tooltip" data-title="Editar Anuncio"><i class="fa fa-shopping-cart"></i> Editar</a>'+
								'<a href="javascript:;" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown"><span class="caret"></span></a>'+
								'<div class="dropdown-menu dropdown-menu-right">'+
								'<a href="javascript:;" onclick="productoEdit('+ row.producto.id +')" class="dropdown-item" data-toggle="tooltip" data-title="Editar Producto"><i class="fa fa-truck"></i> Editar Producto</a>'+
								'<a href="javascript:;" onclick="productoEdit('+ row.id +')" class="dropdown-item" data-toggle="tooltip" data-title="Editar Anuncio"><i class="fa fa-shopping-cart"></i> Editar Anuncio</a>'+
								'</div>'+
								'</div>';
                            return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
				    }
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function publicacionShow(id){
		var url = '{{ route('admin.publicacion.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Publicacion', null, {
			'size': 'modal-lg'
		});
	}

	function publicacionCreate(){
		var url = '{{ route('admin.publicacion.create') }}';
		var modal = openModal(url, 'Nueva Publicacion', null, { size:'modal-lg' });
		setModalHandler('formPublicacionCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function publicacionEdit(id){
		var url = '{{ route('admin.publicacion.edit', 'idRecord') }}';
		url = url.replace('idRecord', id);
		var modal = openModal(url, 'Editar Publicacion', null, { size:'modal-lg' });
		setModalHandler('formPublicacionEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function publicacionDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.publicacion.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}


	function productoShow(id){
		var url = '{{ route('admin.producto.show', ['idProducto']) }}';
		url = url.replace('idProducto', id);
		var modal = openModal(url, 'Producto', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formProductoShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}

	function productoEdit(id){
		var url = '{{ route('admin.producto.edit', ['idProducto']) }}';
		url = url.replace('idProducto', id);
		var modal = openModal(url, 'Editar Producto', null, { size: 'modal-80' });
		setModalHandler('formProductoEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}


</script>
@endpush