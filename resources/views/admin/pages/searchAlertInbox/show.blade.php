@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Inbox')

@push('css')
<link href="/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.css" rel="stylesheet" />
@endpush

@section('content')

<style>
.tblLabel{
	text-align:right;
	font-weight:normal;
	vertical-align: top !important;
}
.tblValue{
	text-align:left;
	font-weight:bold;
	vertical-align: middle !important;
}
.tblValue .input-group{
	margin-bottom:0px !important;
}
</style>
<!-- begin wrapper -->
<div id="message_{{ $mod->id }}" class="wrapper">
	<h3 class="m-t-0 m-b-15 f-w-500">{{ $mod->nombres }} {{ $mod->apellidos }}
		<span class="badge

		@if($mod->estado == 1)
			badge-secondary
		@elseif($mod->estado == 2)
			badge-yellow
		@elseif($mod->estado == 3)
			badge-primary
		@elseif($mod->estado == 4)
			badge-danger
		@elseif($mod->estado == 5)
			badge-danger
		@endif
		" style="font-size:13px;">@lang('messages.'.$ceEstados[$mod->estado])</span>
	</h3>
	<ul class="media-list underline m-b-15 p-b-15">
		<li class="media media-sm clearfix">
			{{--<a href="javascript:;" class="pull-left">--}}
				{{--<img class="media-object rounded-corner" alt="" src="../assets/img/user/user-12.jpg" />--}}
			{{--</a>--}}
			<div class="media-body">
				<div class="email-from text-inverse f-s-14 f-w-600 m-b-3">
					Cliente: {{ $mod->nombres }} {{ $mod->apellidos }}
				</div>
				<div class="m-b-3"><i class="fa fa-clock fa-fw"></i> {{ $mod->created_at->toFormattedDateString() }} {{ $mod->created_at->format('H:i') }} - ( {{ $mod->created_at->diffForHumans(\Carbon\Carbon::now()) }} )</div>
				{{--<div class="m-b-3"><i class="fa fa-check-square fa-fw"></i> {{ $mod->Producto->Categorias[0]->Padre->nombre }} / {{ $mod->Producto->Categorias[0]->nombre }}</div>--}}

				{{--<div class="email-to">To: nguoksiong@live.co.uk</div>--}}
			</div>
		</li>
	</ul>

	<div class="row">
		<div class="col col-md-12">
			<h4>Datos de la consulta</h4>
			<table class="table table-condensed table-striped">
				<tr>
					<td class="tblLabel">Nombres y apellidos</td>
					<td class="tblValue"><span style="font-weight: bold; font-size:15px;">{{ $mod->nombres }} {{ $mod->apellidos }}</span></td>
				</tr>
				<tr>
					<td class="tblLabel">E-mail</td>
					<td class="tblValue">{{ $mod->email }}</td>
				</tr>
				<tr>
					<td class="tblLabel">País</td>
					<td class="tblValue">{{ $global_paises[$mod->pais] }} ({{ $mod->pais }})</td>
				</tr>
				<tr>
					<td class="tblLabel">Telephone</td>
					<td class="tblValue">{{ $mod->telefono }}</td>
				</tr>
				@if($mod->idTipoConsulta == \App\Models\ConsultaExterna::TIPO_PRODUCT_CONTACT_CONSULTANT)
				<tr>
					<td class="tblLabel">Publicacion</td>
					<td class="tblValue">
						<a href="{{ route('admin.announcementInbox.show', [$mod->extrainfo['idPub']]) }}">Ir al anuncio [{{ $mod->extrainfo['idPub'] }}]</a>
						@php
							$pub = \App\Models\Publicacion::find($mod->extrainfo['idPub']);

						@endphp
						<br>
						<h3>{{ $pub->Producto->nombre_alter }}</h3>
						<br>
						@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)
							@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)
								<div data-p="170.00">
									<img data-u="image" src="{{ $imagen->ruta_publica_producto_thumb }}" style="max-width:100px; max-height:100px;" />
								</div>
							@endforeach
						@endif
					</td>
				</tr>
				@endif
				<tr>
					<td class="tblLabel">Consulta</td>
					<td class="tblValue">
						<textarea name="desc" id="desc" style="width:100%; height:200px;" disabled>{{ $mod->descripcion }}</textarea>
					</td>
				</tr>
			</table>
		</div>
	</div>




	<h5 style="margin-bottom:20px;">Respuesta rápida</h5>

	<b>Asunto:</b>
	{!! Form::text('asunto', 'Answer to your commets sent through our website', [
		'id' => 'asunto',
		'placeholder' => '',
		'class' => 'form-control',
		'style' => 'margin-bottom:20px; background-color:#ffffff;'
	]) !!}
	<form action="javascript:;" name="wysihtml5" method="POST">
		<textarea class="textarea form-control" id="wysihtml5" placeholder="Enter text ..." rows="12">Estimado Sr(a). {{ $mod->nombres }} {{ $mod->apellidos }},
			<br><br>A tiempo de saludarle cordialmente hacemos llegar a su persona la información solicitada lorem ipsum dolor sit amet consectetur dip adipisicing set.
			<br><br>Atentamente, el equipo de <b>ExpoIndustri</b>.
		</textarea>
	</form>
	<div class="row">
		<div class="col-md-12 text-right">
			{{--<a href="javascript:;" class="btn btn-primary" onclick="guardarCalculadora()">Guardar calculadora</a>--}}
			<br><br>
			{{--<a href="javascript:;" onclick="guardar({{ $mod->id }}, 'rejected')" class="btn btn-danger"><i class="fa fa-thumbs-down"></i> Rechazar</a>--}}
			<a href="javascript:;" onclick="enviarMensajeLibre()" class="btn btn-primary"><i class="fa fa-send"></i> Enviar</a>
			{{--<a href="javascript:;" onclick="guardar({{ $mod->id }}, 'approved')" class="btn btn-inverse"><i class="fa fa-thumbs-up"></i> Guardar y Aprobar</a>--}}
		</div>
	</div>

</div>
<!-- end wrapper -->
@endsection

@push('scripts')

<script src="/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="/assets/js/demo/form-wysiwyg.demo.js"></script>

<script>

	$(document).ready(function() {
		FormWysihtml5.init();
	});

$.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyALPqjAs5i_1KqcyQ_YMIEtxKblkrDIHaY').done(function() {

});

$(document).ready(function() {

});

function enviarMensajeLibre(){
	var url = '{{ route('admin.searchAlertInbox.enviarMensajeLibre', [$mod->id]) }}';
	var data = {
		asunto:$('#asunto').val(),
		mensaje:$('#wysihtml5').val(),
		idSearchAlert: {{ $mod->id }}
	};

	ajaxPost(url, data, function(data){
		swal('Envio exitoso!', '', 'success');
	});
}


</script>
@endpush