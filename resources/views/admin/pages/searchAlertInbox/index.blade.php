@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Inbox')

@push('css')
<style>
	.list-email .email-time {
		width:120px;
	}
</style>
@endpush

@section('content')
	<!-- begin vertical-box -->
	<div class="vertical-box with-grid inbox">
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column width-200 bg-silver hidden-xs">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper bg-silver text-center">
					{{--<a href="email_compose.html" class="btn btn-inverse p-l-40 p-r-40 btn-sm">--}}
						{{--Crear nuevo--}}
					{{--</a>--}}
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin wrapper -->
								<div class="wrapper p-0">
									<div class="nav-title"><b>FOLDERS</b></div>
									<ul class="nav nav-inbox">
										<li class="active"><a href="javascript:;" onclick="regresar()"><i class="fa fa-inbox fa-fw m-r-5"></i> Inbox <span class="badge pull-right">{{ $counterQuotesNew }}</span></a></li>
										<li><a href="javascript:;"><i class="fa fa-thumbs-up fa-fw m-r-5"></i> Aprobados</a></li>
										<li><a href="javascript:;"><i class="fa fa-thumbs-down fa-fw m-r-5"></i> Rechazados</a></li>
										<li><a href="javascript:;"><i class="fa fa-search fa-fw m-r-5"></i> Todos</a></li>
										<li><a href="javascript:;"><i class="fa fa-trash fa-fw m-r-5"></i> Eliminados</a></li>
									</ul>
									<div class="nav-title"><b>TIPOS DE CONTACTO</b></div>
									<ul class="nav nav-inbox">
										@foreach($ceTipos as $value => $name )
											<li><a href="javascript:;" onclick="filterTipo('{{ $value }}')">
													{{--<i class="fa fa-fw f-s-10 m-r-0 fa-circle text-primary"></i>--}}
													{{ $name }}
												</a></li>
										@endforeach

										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-primary"></i> Designer & Employer</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-success"></i> Staff</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-warning"></i> Sponsorer</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-danger"></i> Client</a></li>--}}
									</ul>
								</div>
								<!-- end wrapper -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column bg-white">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper bg-silver-lighter">
					<!-- begin btn-toolbar -->
					<div id="controlsMessage" class="clearfix hide">
						<div class="pull-left">
							<div class="btn-group m-r-5">
								{{--<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-reply f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Reply</span></a>--}}
								<a href="javascript:;" onclick="regresar()" class="btn btn-white btn-sm"><i class="fa fa-arrow-left f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Regresar</span></a>
							</div>
							<div class="btn-group m-r-5">
								<a href="javascript:;" class="btn btn-white btn-sm btnDeleteMessageDetail"><i class="fa fa-trash f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Delete</span></a>
								{{--<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-archive f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Archive</span></a>--}}
							</div>
						</div>
						<div class="pull-right">
							<div class="btn-group">
								<a href="email_inbox.html" class="btn btn-white btn-sm disabled"><i class="fa fa-arrow-up f-s-14 t-plus-1"></i></a>
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-arrow-down f-s-14 t-plus-1"></i></a>
							</div>
							<div class="btn-group m-l-5">
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-times f-s-14 t-plus-1"></i></a>
							</div>
						</div>
					</div>
					<div id="controsListing" class="btn-toolbar ">
						<div class="btn-group m-r-5">
							<a href="javascript:;" class="p-t-5 pull-left m-r-3 m-t-2" data-click="email-select-all">
								<i class="far fa-square fa-fw text-muted f-s-16 l-minus-2"></i>
							</a>
						</div>
						<!-- begin btn-group -->
						<div class="btn-group dropdown m-r-5">
							<button class="btn btn-white btn-sm" data-toggle="dropdown">
								View All <span class="caret m-l-3"></span>
							</button>
							<ul class="dropdown-menu text-left text-sm">
								<li class="active"><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> All</a></li>
								<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> consultor1@expoindustri.com</a></li>
								<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> consultor2@expoindustri.com</a></li>
								{{--<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Unread</a></li>--}}
								{{--<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Contacts</a></li>--}}
								{{--<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Groups</a></li>--}}
								{{--<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Newsletters</a></li>--}}
								{{--<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Social updates</a></li>--}}
								{{--<li><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Everything else</a></li>--}}
							</ul>
						</div>

						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group m-r-5">
							<button class="btn btn-sm btn-white" onclick="reloadListing()"><i class="fa fa-redo f-s-14 t-plus-1"></i></button>
						</div>
						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group">
							{{--<button class="btn btn-sm btn-white hide" data-email-action="delete"><i class="fa t-plus-1 fa-thumbs-up f-s-14 m-r-3"></i> <span class="hidden-xs">Aprobar</span></button>--}}
							{{--<button class="btn btn-sm btn-white hide" data-email-action="archive"><i class="fa t-plus-1 fa-thumbs-down f-s-14 m-r-3"></i> <span class="hidden-xs">Rechazar</span></button>--}}
							<button class="btn btn-sm btn-white hide" data-email-action="archive" onclick="deleteMessages()"><i class="fa t-plus-1 fa-trash f-s-14 m-r-3"></i> <span class="hidden-xs">Eliminar</span></button>
						</div>
						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group ml-auto">
							<button class="btn btn-white btn-sm btnPrev">
								<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
							</button>
							<button class="btn btn-white btn-sm btnNext">
								<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
							</button>
						</div>
						<!-- end btn-group -->
					</div>
					<!-- end btn-toolbar -->
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin list-email -->
								<div id="listingHolder" class="listingHolder m-0 p-0">
									<ul id="messagesHolder" class="list-group list-group-lg no-radius list-email">
									</ul>
								</div>
								<!-- end list-email -->
								<div id="detailHolder" class="detailHolder m-0 p-0 hide">
								</div>
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
				<!-- begin wrapper -->
				<div class="wrapper bg-silver-lighter clearfix">
					<div class="btn-group pull-right">
						<button class="btn btn-white btn-sm btnPrev">
							<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
						</button>
						<button class="btn btn-white btn-sm btnNext">
							<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
						</button>
					</div>
					<div class="m-t-5 text-inverse f-w-600">{{ $counterConsultaExternas }} messages</div>
				</div>
				<!-- end wrapper -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
	</div>
	<!-- end vertical-box -->
<div class="hide">
<li class="list-group-item unread" id="stub_messageItem">
	<div class="email-checkbox">
		<label>
			<i class="far fa-square"></i>
			<input type="checkbox" data-checked="email-checkbox" />
		</label>
	</div>
	<a href="javascript:;" class="email-user bg-gradient-blue">
		<span class="text-white">F</span>
	</a>
	<div class="email-info">
		<a href="javascript:;">
			<span class="email-time"></span>
			<span class="email-sender"></span>
			<span class="email-title"></span>
			<span class="email-desc"></span>
		</a>
	</div>
</li>
</div>
@endsection

@push('scripts')
<script src="/assets/js/demo/email-inbox.demo.js"></script>
<script>
$(document).ready(function() {
	InboxV2.init();

	reloadListing();

});

var maxPage = 0;
var urlDs = '{{ route('resource.searchAlert.ds') }}';

var msgTipo = '';
function filterTipo(tipo) {
	msgTipo = tipo;
	reloadListing();
}

function reloadListing(page){

	var urlItem = '{{ route('admin.searchAlertInbox.show', [0]) }}';
	var data = {};

	if(msgTipo != '') {
		data['filterTipo'] = msgTipo;
	}

	if(page !== undefined && page !== '') {
		urlDs = page;
	} else {

	}

	console.log(urlDs);
	var mh = $('#messagesHolder');
	mh.html('<span class="p-10">Loading...</span>');
	var loadMsg = 0;
	var badge = '';

	ajaxPost(urlDs, data, function(data){
		var dt = data.data;
		mh.html('');

		$('.btnPrev').hide();
		$('.btnNext').hide();

		if(data.prev_page_url !== null) {
			$('.btnPrev').show();
			$('.btnPrev').each(function(e) {
				$(this).attr('onclick', 'reloadListing("' + data.prev_page_url +'")')
			});
		}
		if(data.next_page_url !== null) {
			$('.btnNext').show();
			$('.btnNext').each(function(e) {
				$(this).attr('onclick', 'reloadListing("' + data.next_page_url +'")');
			});
		}

		var counter = 0;
		$.each(dt, function(key, row){
			counter++;
			loadMsg = row.id;
			//row = row.publicacion;
			var item = $('#stub_messageItem').clone();
			item.removeAttr('id');
			urlShow = urlItem.replace('0', row.id);
			item.find('.email-user, .email-info a').prop('href', urlShow);
			item.find('.email-user, .email-info a').attr('onclick', 'loadDetail(event, '+row.id+')');
			item.find('.email-checkbox input').prop('id', row.id).val(row.id);
			item.find('.email-user span').html(row.nombres.substring(0, 1));
			item.find('.email-info .email-time').html(row.created_at);
//			item.find('.email-info .email-sender').html(counter + ": " + row.id + ": " + row.email);
			item.find('.email-info .email-sender').html(row.email);

			if(row.estado == {{ \App\Models\ConsultaExterna::ESTADO_NEW }}){
				badge = '';
			} else if(row.estado === {{ \App\Models\ConsultaExterna::ESTADO_UNDER_REVIEW }}){
				badge = '<span class="badge badge-warning">Revisado: '+ row.usuario_revisor.username +'</span>';
			} else if(row.estado === {{ \App\Models\ConsultaExterna::ESTADO_REJECTED }}){
				badge = '<span class="badge badge-danger">Revisado: '+ row.usuario_revisor.username +'</span>';
			} else if(row.estado === {{ \App\Models\ConsultaExterna::ESTADO_RESOLVED }}){
				badge = '<span class="badge badge-yellow">Revisado: '+ row.usuario_revisor.username +'</span>';
			}


			var badge = '';
			row.idTipoConsulta = parseInt(row.idTipoConsulta, 10);
			console.log(row.idTipoConsulta);
			switch(row.idTipoConsulta){
				case {{ \App\Models\ConsultaExterna::TIPO_CONTACT_ROBOT }}:
					badge = ' <span class="badge badge-danger">SPAM</span>';
					break;
				case {{ \App\Models\ConsultaExterna::TIPO_PRODUCT_CONTACT_CONSULTANT }}:
					badge = ' <span class="badge badge-info">CONTACTO PRODUCTO</span>';
					break;
				default:
					badge = '';
					break;
			}
			item.find('.email-info .email-title').html(row.nombres+' '+row.apellidos);
			item.find('.email-info .email-desc').html(badge);
			mh.append(item);
		});
		if(loadMsg !== 0){
//			loadDetail(null, loadMsg);
		} else {
			console.log(" NOT LOADING DETAIL ");
		}
	}, undefined, { silent: true });
}

function loadDetail(e, id){
	if(e !== null){
		e.preventDefault();
	}
	var url = '{{ route('admin.searchAlertInbox.show', [0]) }}';
	url = url.replace('0', id);
	var holder = $('#detailHolder');
	var msg = $('<div>').prop('id', 'msg_' + id);
	holder.html(msg);
	$('.btnDeleteMessageDetail').attr('onclick', 'deleteMessages('+ id +')');
	msg.load(url, function(response, status, xhr){
		$('#controlsMessage').removeClass('hide');
		$('#controsListing').addClass('hide');
		$('#detailHolder').removeClass('hide');
		$('#listingHolder').addClass('hide');
	});

	return false;
}

function regresar(){
	$('#controlsMessage').addClass('hide');
	$('#controsListing').removeClass('hide');
	$('#detailHolder').addClass('hide');
	$('#listingHolder').removeClass('hide');
}

function deleteMessages(id) {

	var ids = [];

	if(id !== undefined) {
		ids = [];
		ids.push(id);
		$('#' + id).prop('checked', 'checked');
	} else {
		$('.email-checkbox input').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});

		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
	}

	swal({
		text: 'Desea eliminar el/los mensajes seleccionados?',
		icon: 'warning',
		buttons: {
			cancel: '@lang('form.no')',
			ok: '@lang('form.yes')'
		},
		dangerMode: true
	}).then(function(response) {

		console.log(response);

		if (response === 'ok') {
			if(id !== undefined) {
				regresar();
			}
			console.log(ids);

			var url = '{{ route('resource.searchAlert.destroy', [0]) }}';

			ajaxDelete(url, ids, function(){
				swal('Mensaje eliminado', { icon: "success" });

				var targetEmailList = '[data-checked=email-checkbox]:checked';
				if ($(targetEmailList).length !== 0) {
					$(targetEmailList).closest('li').slideToggle(function() {
						$(this).remove();
//					handleEmailActionButtonStatus();
						if ($('.list-email > li').length === 0) {
							$('.list-email').html('<li class="p-15 text-center"><div class="p-20"><i class="fa fa-trash fa-5x text-silver"></i></div> This folder is empty</li>');
						}
					});
				}
			});
		}
	});
}
</script>
@endpush