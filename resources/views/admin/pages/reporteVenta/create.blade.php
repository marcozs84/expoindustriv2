@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.permiso.index') }}">Permisos</li>
	</ol>

	<h1 class="page-header">Nuevo Permiso</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Permiso</h4>
		</div>
		<div class="panel panel-body">
			<div id="formPermisoCreate" class="form-horizontal">

				<div class="row">
					<div class="col col-md-6">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">No. Oferta</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Fecha de oferta</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">No. Orden</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Empresa</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Cliente nuevo</label>
							<div class="col-sm-10">
								{!! Form::select('permisoNombre', ['si', 'no'], 0, [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
					</div>
					<div class="col col-md-6">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Lugar</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Nombre de contacto</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Teléfono</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">E-mail</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', '', [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
					</div>
				</div>





				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Fuente</label>
					<div class="col-md-12">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
						{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
						{{--<div class="form-group">--}}
						{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
						{{--</div>--}}
						{{--<div class="form-group">--}}
						{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
						{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.bannerSuperior.upload') }}" class="dropzone">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="sesna" value="{{ rand(1,1000) }}">
							<input type="hidden" name="banner_id" id="banner_id" value="0">
							<div class="dz-message" data-dz-message><span>
									{{--@lang('messages.dz_upload')--}}
									Adjuntar oferta
								</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor aguarde hasta que el archivo termine de subir.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>


                <div class="hr-line-dashed"></div>
                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="permisoCancelar()">Cerrar</a>
                        <a class="btn btn-primary btn-sm" href="javascript:;" onclick="permisoGuardar()" >Guardar</a>
                    </div>
                </div>
            </div>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>
$( "#formPermisoCreate" ).submit(function( event ) {
	permisoGuardar();
	event.preventDefault();
});

function permisoGuardar(){
	var url = '{{ route('resource.permiso.store') }}';
	ajaxPost(url, {
		nombre: $('#permisoNombre').val(),
		descripcion: $('#permisoDescripcion').val(),
		idConvenio: $('#permisoConvenio').val()
    }, function(){
		$(document).trigger('formPermisoCreate:aceptar');
	});
}

function permisoCancelar(){
	@if($isAjaxRequest)
		$(document).trigger('formPermisoCreate:cancelar');
	@else
		redirect('{{ route('admin.permiso.index')  }}');
	@endif
}

function formInit(){
	$('#permisoNombre').focus();
}
formInit();

var myDropzone = '';
$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
	setTimeout(function(){
		$('div#dropzone').dropzone({
			url: '{{ route('admin.bannerSuperior.upload') }}'
		});

		myDropzone = new Dropzone(".dropzone", {
			url: "{{ route('admin.bannerSuperior.upload') }}",
			addRemoveLinks: true,
			autoProcessQueue: false,
			parallelUploads: 1,
			init: function(){
				{{--@foreach($images as $image)--}}
						{{--var mockFile = {--}}
						{{--name: '{{ $image['filename'] }}',--}}
						{{--size: 123,--}}
						{{--};--}}
						{{--this.options.addedfile.call(this, mockFile);--}}
						{{--this.options.thumbnail.call(this, mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
						{{--this.emit('thumbnail', mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
						{{--this.emit('complete', mockFile);--}}
						{{--// Src.: https://stackoverflow.com/a/22719947;--}}
						{{--@endforeach--}}

					this.on("removedfile", function(file) {
					//alert(file.name);
					console.log('Eliminado: ' + file.name);

					file.previewElement.remove();

					// Create the remove button
					var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

					//Capture the Dropzone instance as closure.
					var _this = this;

					// Listen to the click event
					//					removeButton.addEventListener();

					// Add the button to the file preview element.
					file.previewElement.appendChild(removeButton);

				});
			},
			success: function(file, response){
				console.log("success");
				console.log(file.name);
				console.log(response);
				$(document).trigger('formBannerSuperiorCreate:aceptar');
			},
			removedfile: function(file){
				x = confirm('@lang('messages.confirm_removal')');
				if(!x) return false;
				removeImage(1, file.name);
//					for(var i = 0 ; i < file_up_names.length; i++){
//						if(file_up_names[i] === file.name){
//							ajaxPost('public.publishRemove', {
//								'archivo' : ''
//							});
//						}
//					}
			}
		});
	}, 1000);

	$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
	$("[data-toggle=tooltip]").tooltip();

});


</script>
@endpush

