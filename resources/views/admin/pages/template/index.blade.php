@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Templates')

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />

	<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Clientes</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Listado de templates</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Templates</h4>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">
					<label for="exampleInputEmail1" style="display:block;">Modulos</label>
						{!! Form::select('modulo', $modulos, null, [
                            'id' => 'selModulo',
                            'class' => 'multiple-select2 form-control',
                            'multiple' => 'multiple',
                            'style' => 'width:100%;'
                        ]); !!}
				</div>
				<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">
					<label for="exampleInputEmail1" style="display:block;">Tipo</label>
						{!! Form::select('tipo', $tipos, null, [
                            'id' => 'selTipo',
                            'class' => 'multiple-select2 form-control',
                            'multiple' => 'multiple',
                            'style' => 'width:100%;'
                        ]); !!}
				</div>
				<div class="form-group col-md-4 col-sm-4 col-lg-4" style="vertical-align:top;">
					<label for="exampleInputEmail1" style="display:block;">Estado</label>
						{!! Form::select('estado2', [], null, [
                            'id' => 'selEstado2',
                            'class' => 'multiple-select2 form-control',
                            'multiple' => 'multiple',
                            'style' => 'width:100%;'
                        ]); !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					<a class="btn btn-sm btn-primary" href="javascript:;" onclick="templateCrear()"><i class="fa fa-plus"></i> Nuevo Template</a>
					<a class="btn btn-sm btn-primary" href="javascript:;" onclick="templateEliminar()"><i class="fa fa-trash"></i> Eliminar Template(s)</a>

                    {{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}
                    <a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:buscar();"><i class="fa fa-search"></i> Buscar</a>
                    <a class="btn btn-sm btn-primary pull-right" href="javascript:limpiar();">Limpiar</a>
                </div>
			</div>

			<table id="dtTemplates" class="table table-striped table-bordered width-full table-condensed"></table>
		</div>
	</div>

	<div class="modal fade" id="modalNuevoTemplate">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Nuevo Template</h4>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<a href="javascript:;" class="btn btn-sm btn-success">Action</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

<style>
	.mzWordWrap{
		/*max-width:150px !important;*/
		word-wrap: break-word !important;
		white-space:pre-wrap !important;
        text-overflow: ellipsis !important;
	}
</style>
<script>
var datatable = null;
$(document).ready(function(){
	initView();
});

function initView(){

	$(".multiple-select2").select2();
	$(".multiple-select2").on("select2:select", function (e) {
        buscar();
    });

	$(".multiple-select2").on("select2:unselect", function (e) {
        buscar();
    });

	if ($('#dtTemplates').length !== 0) {
	    datatable = $('#dtTemplates').DataTable({
	        lengthMenu: [5, 10, 20, 30],
	        pageLength: 20,
	        ajax: {
	            url: '{{ route('resource.template.ds') }}',
				method: 'POST',
	            dataSrc: 'data',
	            'headers': {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
                statusCode: {
                    422: ajaxValidationHandler,
                    500: ajaxErrorHandler
                },
	            data: function (d){ // Procesa datos enviados
	                d.convenio = $('#selConvenio').val();
	                d.modulo = $('#selModulo').val();
	                d.tipo = $('#selTipo').val();
	                // ...
	            },
	            dataFilter: function(data){ // Procesa datos recibidos
	                // return data;
	                var data = JSON.parse(data);
	                for(var i = 0 ; i < data.data.length; i++){
	                    // ...
	                }
	                return JSON.stringify( data );
	            }
	        },
	        columns: [
	            {data: 'id', title: 'ID'},
	            {data: 'nombre', title: 'Nombre'},
	            {data: 'descripcion', title: 'Descripcion'},
	            {data: 'template_tipo.nombre', title: 'Tipo'},
	            {data: 'modulo.nombre', title: 'Modulo'},
	            {data: 'convenio.nombreCorto', title: 'Convenio'},
	            {data: 'estado', title: 'Estado'},
	            {title: ''},
	            // ...
	        ],
	        columnDefs: [
	            {
	                targets: 0,
	                class: 'mzWordWrap',
	                width: '15px'
	            },{
	                targets: 1,
	                class: 'mzWordWrap',
                    render: function(data, type, row){
                        var html = '<a href="javascript:;" onclick="templateEditar('+ row['id'] +')">'+data+'</a>';
                        {{--html += ' [ <a target="_blank" href="{{ route('admin.template.index') }}/'+ row['id'] +'/editor_pdf">Editor</a> ]';--}}

                        return html;
                    }
	            },{
	                targets: 2,
	                class: 'mzWordWrap',
	            },{
	                targets: 3,
	                class: 'text-center',
	            },{
	                targets: 4,
	                class: 'text-center',
	            },{
	                targets: 5,
	                class: 'text-center',
	            },{
	                targets: 6,
	                class: 'text-center',
			        render: function(data, type, row){
	                	if(data == 0){
	                		return 'Inactivo';
		                } else if(data == 1){
			                return 'Activo';
		                }
			        }
	            },{
	                targets: 7,
	                width: '10px',
	                class: 'text-center',
	                render: function(data, type, row){
	                    return '<a target="_blank" href="{{ route('admin.template.index') }}/'+ row['id'] +'/editor_pdf"><i class="fa fa-edit"></i></a>';
	                }
	            },{
	                targets: 8,
	                width: '10px',
	                class: 'text-center',
	                render: function(data, type, row){
	                    return '<input type="checkbox" class="chkId" value="'+ row['id'] +'" />';
	                }
	            }
	        ],
	        fixedHeader: {
	            header: true,
	            headerOffset: $('#header').height()
	        },
	        bSort : false,
	        bAutoWidth: false,
	        processing: true,
	        serverSide: true,
	        responsive: true,
	        fixedColumns: true
	    });
	}
}

function templateCrear(){
    var url = '{{ route('admin.template.create') }}';
    var modal = openModal(url, 'Nuevo template');
    setModalHandler('formTemplateCreate:aceptar', function(){
        dismissModal(modal);
        datatable.ajax.reload();
    });
}

function templateEditar(id){
    var url = '{{ route('admin.template.index') }}/'+id+'/edit';
    var modal = openModal(url, 'Editar template');
    setModalHandler('formTemplateEdit:aceptar', function(){
        dismissModal(modal);
        datatable.ajax.reload(null, false);
    });
}

function templateEliminar(){
	var ids = [];
    $('.chkId').each(function() {
        if($(this).prop('checked')){
            ids.push($(this).val());
        }
    });

    var url = '{{ route('resource.template.destroy', [0]) }}';

    ajaxDelete(url, ids, function(){
        datatable.ajax.reload(null, false);
    });
}

function buscar(){
	datatable.ajax.reload();
}

function limpiar(){
	$("#selConvenio").val([]).trigger('change');
	$("#selModulo").val([]).trigger('change');
	$("#selTipo").val([]).trigger('change');
	datatable.ajax.reload();
}
</script>
@endpush

