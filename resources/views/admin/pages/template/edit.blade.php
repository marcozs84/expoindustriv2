@extends(($isAjaxRequest) ? 'layouts.ajax_layout' : 'layouts.sac_default')

@section('css')
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li><a href="{{ route('admin.template.index') }}">Templates</a></li>
		<li class="active">Nuevo template</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Editar Template</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Nuevo template</h4>
        </div>
        <div class="panel-body">
            <form id="formTemplateEdit" method="get">

	            <div class="form-group row m-b-10">
	                <label class="col-form-label col-md-3">Modulo</label>
                    <div class="col-md-9">
                        {!! Form::select('selModulo', $modulos, $template->idModulo, [
                            'id' => 'selModulo',
                            'placeholder' => '',
                            'class' => 'form-control'
                        ]) !!}

                    </div>
                </div>
                <div class="form-group row m-b-10">
	                <label class="col-form-label col-md-3">Tipo</label>
                    <div class="col-md-9">
                        {!! Form::select('selTemplateTipo', $templateTipos, $template->idTemplateTipo, [
                            'id' => 'selTemplateTipo',
                            'placeholder' => '',
                            'class' => 'form-control'
                        ]) !!}
                    </div>
                </div>
	            <div class="form-group row m-b-10">
		            <label class="col-form-label col-md-3">Nombre</label>
		            <div class="col-md-9">
			            {!! Form::text('nombre', $template->nombre, [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control form-control-sm m-b-5'
						]) !!}
		            </div>
	            </div>
	            <div class="form-group row m-b-10">
		            <label class="col-form-label col-md-3">Descripcion</label>
		            <div class="col-md-9">
			            {!! Form::textarea('descripcion', $template->descripcion, [
							'id' => 'descripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control m-b-5',
							'rows' => 3,
						]) !!}
		            </div>
	            </div>
	            <hr class="hr-line-dashed">
	            <div class="form-group row m-b-10">
		            <label class="col-form-label col-md-3">Asunto</label>
		            <div class="col-md-9">
			            {!! Form::text('asunto', $template->asunto, [
							'id' => 'asunto',
							'placeholder' => 'Asunto',
							'class' => 'form-control form-control-sm m-b-5'
						]) !!}
		            </div>
	            </div>
	            <div class="form-group row m-b-10">
		            <label class="col-form-label col-md-3">Mensaje</label>
		            <div class="col-md-9">
			            {!! Form::textarea('mensaje', $template->mensaje, [
							'id' => 'mensaje',
							'placeholder' => 'Mensaje',
							'class' => 'form-control m-b-5',
							'rows' => 10,
						]) !!}
		            </div>
	            </div>
	            <div class="form-group row m-b-10">
		            <label class="col-form-label col-md-3">Estado</label>
		            <div class="col-md-9">
			            {!! Form::select('selEstado', [1 => 'Activo', 0 => 'Inactivo'], $template->estado, [
							'id' => 'selEstado',
							'placeholder' => '',
							'class' => 'form-control'
						]) !!}
		            </div>
	            </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row m-b-10">
                    <div class="col-lg-12 text-right">
                        <a class="btn btn-success" href="{{ route('admin.template.editor_pdf', [$template->id]) }}"><i class="fa fa-file-pdf-o"></i> Editor PDF</a>
                        <a id="btnModeloCancelar" class="btn btn-white" data-dismiss="modal" href="javascript:;">Cancelar</a>
                        <a id="btnModeloAceptar" class="btn btn-primary" href="javascript:guardarTemplate();" >Guardar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop

@push('scripts')
<script>

function guardarTemplate(){
	var url = '{{ route('resource.template.update', [$template->id]) }}';
	var data = {
        idModulo: $('#formTemplateEdit #selModulo').val(),
        idTemplateTipo: $('#formTemplateEdit #selTemplateTipo').val(),
        nombre: $('#formTemplateEdit #nombre').val(),
        descripcion: $('#formTemplateEdit #descripcion').val(),
        asunto: $('#formTemplateEdit #asunto').val(),
        mensaje: $('#formTemplateEdit #mensaje').val(),
		estado: $('#formTemplateEdit #selEstado').val(),
    };
	ajaxPatch(url, data, function(data){
		@if($isAjaxRequest)
            $(document).trigger('formTemplateEdit:aceptar');
        @else
            redirect('{{ route('admin.template.index') }}/'+data.object.id+'/edit');
        @endif
	});
}

</script>
@endpush

