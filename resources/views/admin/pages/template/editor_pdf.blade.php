@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Templates')

@push('css')
<link rel="stylesheet" href="/assets/plugins/codemirror/lib/codemirror2.css">
<link rel="stylesheet" href="/assets/plugins/codemirror/theme/elegant.css">
<style type="text/css">
.CodeMirror {
	/*font-family: Arial, monospace;*/
	font-size: 14px;
	border: 1px solid #eee;
}
.CodeMirror-scroll {
	max-width:100% !important;
}
.CodeMirror-sizer {
	min-width:inherit !improtant;
}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right @if($isAjaxRequest) hide @endif">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.template.index') }}">Templates</a></li>
		<li class="breadcrumb-item active">Editor PDF</li>
	</ol>
	<!-- begin page-header -->
	<h1 class="page-header">Editor de Template</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
        <div class="panel-heading @if($isAjaxRequest) hide @endif">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Editor de template</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6" style="overflow:hidden;">

                    <table class="table table-striped table-condensed table-detail" style="width:100%; overflow:scroll;">
		                <tr>
		                    <td class="text-right f-w-500">Nombre</td>
		                    <td>
		                        <b>{{ $template->nombre }}</b>
		                    </td>
		                </tr>
		                <tr>
		                    <td class="text-right f-w-500">Descripcion</td>
		                    <td>
		                        {{ $template->descripcion }}
		                    </td>
		                </tr>

		                <tr>
		                    <td class="width-100 text-right f-w-500">Modulo</td>
		                    <td>
		                        <b>{{ $template->Modulo->nombre }}</b>
		                    </td>
		                </tr>
		                <tr>
		                    <td class="text-right f-w-500">Tipo</td>
		                    <td>
		                        {{ $template->TemplateTipo->texto }}
		                    </td>
		                </tr>

		                <div class="hr-line-dashed"></div>
		                <tr>
		                    <td class="text-right f-w-500">Asunto</td>
		                    <td>
		                        {{ $template->asunto }}
		                    </td>
		                </tr>
		                <tr>
		                    <td colspan="2" style="padding:0px;">

			                    <ul class="nav nav-tabs nav-tabs-inverse nav-justified m-t-10">
				                    <li class="nav-items">
					                    <a href="#default-tab-1" data-toggle="tab" class="nav-link active">
						                    <span class="d-sm-none">Contenido</span>
						                    <span class="d-sm-block d-none">Contenido</span>
					                    </a>
				                    </li>
				                    <li class="nav-items">
					                    <a href="#default-tab-2" data-toggle="tab" class="nav-link">
						                    <span class="d-sm-none">Header & Footer</span>
						                    <span class="d-sm-block d-none">Header & Footer</span>
					                    </a>
				                    </li>
				                    <li class="">
					                    <a href="#default-tab-3" data-toggle="tab" class="nav-link">
						                    <span class="d-sm-none">Configuracion de página</span>
						                    <span class="d-sm-block d-none">Configuracion de página</span>
					                    </a>
				                    </li>
			                    </ul>

								<div class="tab-content p-0">
									<div class="tab-pane fade active show" id="default-tab-1">
										<textarea id="mensaje" name="mensaje" style="height:800px; width:100%; font-size:15px;">{{ $template->mensaje }}</textarea>
				                        
				                        <br>
				                        <input type="checkbox" id="chkIrFinal" /> Auto-Scroll
									</div>
									<div class="tab-pane fade" id="default-tab-2">
				                        <form id="formHeaderEdit" method="post" class="form-horizontal">
				                            <h4>Header</h4>
				                            <textarea id="header_mensaje" name="header_mensaje" class="form-control" rows="12">{{ isset($header['mensaje']) ? $header['mensaje'] : '' }}</textarea>
				                            <h4>Footer</h4>
				                            <textarea id="footer_mensaje" name="footer_mensaje" class="form-control" rows="12">{{ isset($footer['mensaje']) ? $footer['mensaje'] : '' }}</textarea>
				                        </form>
									</div>
									<div class="tab-pane fade" id="default-tab-3">
										<table class="table table-striped table-condensed">

							                <tr>
							                    <td class="text-right f-w-500">Margenes (px)</td>
							                    <td>
							                        <table cellspacing="5" style="margin-top:5px; width:100%; text-align:right;">
							                            <tr>
							                                <td>Arriba: </td>
							                                <td>{!! Form::text('m_t', $margenes['t'], ['id' => 'm_t', 'class' => 'form-control', 'placeholder' => 'Arriba', 'style' => 'width:100px;']) !!}</td>
							                                <td>&nbsp;&nbsp;</td>
							                                <td>Abajo: </td>
							                                <td>{!! Form::text('m_b',  $margenes['b'], ['id' => 'm_b', 'class' => 'form-control', 'placeholder' => 'Abajo', 'style' => 'width:100px;']) !!}</td>
							                            </tr>
							                            <tr>
							                                <td>Izquierda: </td>
							                                <td>{!! Form::text('m_l',  $margenes['l'], ['id' => 'm_l', 'class' => 'form-control', 'placeholder' => 'Izquierda', 'style' => 'width:100px;']) !!}</td>
							                                <td>&nbsp;&nbsp;</td>
							                                <td>Derecha: </td>
							                                <td>{!! Form::text('m_r',  $margenes['r'], ['id' => 'm_r', 'class' => 'form-control', 'placeholder' => 'Derecha', 'style' => 'width:100px;']) !!}</td>
							                            </tr>
							                        </table>

							                    </td>
							                </tr>
							                <tr>
							                    <td class="text-right f-w-500">Imagen de fondo</td>
							                    <td>
							                        {!! Form::select('bg', $imagenes, $bg, ['id' => 'bg', 'class' => 'default-select2 form-control']); !!}
							                        <table cellspacing="5" style="margin-top:5px; width:100%; text-align:right;">
							                            <tr>
							                                <td>Ancho: </td>
							                                <td>{!! Form::text('bg_w', $bg_w, ['id' => 'bg_w', 'class' => 'form-control', 'placeholder' => 'Ancho', 'style' => 'width:100px;']) !!}</td>
							                                <td>&nbsp;&nbsp;</td>
							                                <td>Alto: </td>
							                                <td>{!! Form::text('bg_h', $bg_h, ['id' => 'bg_h', 'class' => 'form-control', 'placeholder' => 'Alto', 'style' => 'width:100px;']) !!}</td>
							                            </tr>
							                            <tr>
							                                <td>Pos X: </td>
							                                <td>{!! Form::text('bg_x', $bg_x, ['id' => 'bg_x', 'class' => 'form-control', 'placeholder' => 'X', 'style' => 'width:100px;']) !!}</td>
							                                <td>&nbsp;&nbsp;</td>
							                                <td>Pos Y: </td>
							                                <td>{!! Form::text('bg_y', $bg_y, ['id' => 'bg_y', 'class' => 'form-control', 'placeholder' => 'Y', 'style' => 'width:100px;']) !!}</td>
							                            </tr>
							                            <tr>
							                                <td>Páginas: </td>
							                                <td>{!! Form::text('bg_pags', implode(', ', $bg_pags), ['id' => 'bg_pags', 'class' => 'form-control', 'placeholder' => 'Páginas', 'style' => 'width:100px;']) !!}</td>
							                                <td colspan="3" style="padding:3px; text-align:left;">Páginas en que la imagen de fondo será visible. Ejm: 1, 3, 4. <br> '0' paga aplicar a todas las páginas.</td>
							                            </tr>
							                        </table>

							                    </td>
							                </tr>
										</table>
									</div>
								</div>

		                    </td>
		                </tr>

		                <tr>
		                    <td colspan="2">
								<div class="form-group">
				                    <div class="col-lg-12 text-right">
				                        <a id="btnModeloCancelar" class="btn btn-white" data-dismiss="modal" href="{{ route('admin.template.edit', [$template->id]) }}">volver</a>
				                        <a id="btnModeloAceptar" class="btn btn-primary" href="javascript:guardarTemplate();" >Guardar</a>
				                    </div>
				                </div>
		                    </td>
		                </tr>
		            </table>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6" style="background-color:#ccc; height:880px;">

                    <iframe id="framePDF" src="{{ route('admin.template.getPDF', [$template->id]) }}" frameborder="0" style="width:100%; height:100%;"></iframe>

                </div>
            </div>
        </div>
    </div>

@stop

@push('scripts')
	<script src="/assets/plugins/codemirror/lib/codemirror.js"></script>
	<script src="/assets/plugins/codemirror/mode/xml/xml.js"></script>
	<script src="/assets/plugins/codemirror/mode/javascript/javascript.js"></script>
	<script src="/assets/plugins/codemirror/mode/css/css.js"></script>
	<script src="/assets/plugins/codemirror/mode/htmlmixed/htmlmixed.js"></script>
	<script src="/assets/plugins/codemirror/addon/hint/html-hint.js"></script>
	<script src="/assets/plugins/codemirror/addon/hint/css-hint.js"></script>
	<script src="/assets/plugins/codemirror/addon/hint/javascript-hint.js"></script>



<!-- 	           <script src="http://portal-local.illumant.com/node_modules/codemirror/mode/xml/xml.js"></script>
            <script src="http://portal-local.illumant.com/node_modules/codemirror/mode/javascript/javascript.js"></script>
            <script src="http://portal-local.illumant.com/node_modules/codemirror/mode/css/css.js"></script>
            <script src="http://portal-local.illumant.com/node_modules/codemirror/mode/htmlmixed/htmlmixed.js"></script>
            <script src="http://portal-local.illumant.com/node_modules/codemirror/addon/hint/html-hint.js"></script>
            <script src="http://portal-local.illumant.com/node_modules/codemirror/addon/hint/css-hint.js"></script>
            <script src="http://portal-local.illumant.com/node_modules/codemirror/addon/hint/javascript-hint.js"></script> -->
<script>
$(function(){
	initView();
});

var myCodeMirror = null;

function initView(){
//	$(".default-select2").select2();

	console.log($('#mensaje').height());
	$('#mensaje').height( $('.table-detail').height() - 50 );
	console.log($('#mensaje').height());

//	var myCodeMirror = CodeMirror(document.getElementById('mensaje'));
	myCodeMirror = CodeMirror.fromTextArea(document.getElementById('mensaje'), {
//		mode: 'htmlmixed',
		// tabSize: 4,
		// indentWithTabs: true,
		// theme: 'elegant',
		lineNumbers: true,
        mode: 'htmlmixed',
        tabSize: 4,
        indentUnit: 4,
        indentWithTabs: true,
        height: 'auto',
        width: 'auto',
	});

	myCodeMirror.setSize('100%', '500px');
	$('.CodeMirror-sizer').css({ minWidth: 'inherit' });
	$('.CodeMirror-scroll').css({ maxWidth: '100% !important' });
}

$( "#formTemplateEdit" ).submit(function( event ) {
	guardarTemplate();
	event.preventDefault();
});

$(document).keydown(function(event) {
        // If Control or Command key is pressed and the S key is pressed
        // run save function. 83 is the key code for S.
        if((event.ctrlKey || event.metaKey) && event.which == 83) {
            // Save Function
            guardarTemplate();
            event.preventDefault();
            return false;
        };
    }
);

function guardarTemplate(){
	$.ajax({
		url: '{{ route('resource.template.update', [$template->id]) }}',
		method: 'PATCH',
		data: {
			mensaje: myCodeMirror.getValue(), //$('#mensaje').val(),
			bg: $('#bg').val(),
			bg_w: $('#bg_w').val(),
			bg_h: $('#bg_h').val(),
			bg_x: $('#bg_x').val(),
			bg_y: $('#bg_y').val(),
			bg_pags: $('#bg_pags').val(),
			header_mensaje: $('#header_mensaje').val(),
			footer_mensaje: $('#footer_mensaje').val(),
			margenes: {
				t: $('#m_t').val(),
				b: $('#m_b').val(),
				l: $('#m_l').val(),
				r: $('#m_r').val()
			}
		},
		headers: {
			'X-CSRF-Token' : '{!! csrf_token() !!}'
		},
		statusCode: {
			422: ajaxValidationHandler,
			500: ajaxErrorHandler
		}
	}).done(function(data){
//		var data = JSON.parse(data);
		if(data.result == true){
			toast('Listo!', data.message, 'success');
			reloadPDF();
		} else {
			toast('Alerta!', data.message, 'warning');
		}
	});
}

function reloadPDF(){
	var url = '{{ route('admin.template.getPDF', [$template->id]) }}';
	$('#framePDF').attr('src', url);
}

$('#framePDF').ready(function(){
	console.log("pdf listo");
});

</script>
@endpush

