@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Templates')

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />

	<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Clientes</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Listado de templates</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Email Templates</h4>
		</div>
		<div class="panel-body">


			<h2>Email templates</h2>

			@php
				$files = \Illuminate\Support\Facades\File::files(base_path('resources/views/frontend/email/'));
				$list = [];
			@endphp


			<table class="table table-striped">
				<tr>
					<td>
						@foreach( $files as $file )
							@php
								$name = str_replace('.blade.php', '', basename($file));
								$list[$name] = $name;
							@endphp
						@endforeach

						{!! Form::select('template_name', $list, null, [
							'id' => 'template_name',
							'class' => 'multiple-select2 form-control',
							'style' => 'width:100%;'
						]) !!}
					</td>
					<td>
						{!! Form::text('email', '', [
							'id' => 'email',
							'class' => 'form-control',
							'placeholder' => 'Email',
						]) !!}
					</td>
					<td>
						<a href="javascript:;" onclick="enviarTemplate()" class="btn btn-primary btn-sm"><i class="fa fa-envelope"></i>&nbsp; Enviar correo</a>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Asunto:
						{!! Form::text('message_subject', '', [
							'id' => 'message_subject',
							'class' => 'form-control',
						]) !!}
						<br>
						Texto:
						{!! Form::textarea('message_text', '', [
							'id' => 'message_text',
							'class' => 'form-control',
						]) !!}
					</td>
				</tr>
			</table>


		</div>
	</div>

@stop

@push('scripts')

	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

<style>
	.mzWordWrap{
		/*max-width:150px !important;*/
		word-wrap: break-word !important;
		white-space:pre-wrap !important;
        text-overflow: ellipsis !important;
	}
</style>
<script>
var datatable = null;
$(document).ready(function(){
	initView();
});

function initView(){

	$(".multiple-select2").select2();

}

function enviarTemplate(){

	var url = '{{ route('admin.template.sendEmail') }}';
	var data = {
		template_name: $('#template_name').val(),
		email: $('#email').val(),
		message: $('#message_text').val(),
		subject: $('#message_subject').val(),
	};
	ajaxPost(url, data, function(data){
		$(document).trigger('formTemplateSendEmail:aceptar');
		console.log(data);
	});

}
</script>
@endpush

