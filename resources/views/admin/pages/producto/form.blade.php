@php
	$grp = 0;
	$openGroup = false;
@endphp
<div class="row">
@foreach($primaryFields as $element)

	@if($element['type'] == 'titulo1')
		@include('admin.formElements.titulo1', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'titulo2')
		@include('admin.formElements.titulo2', [
			'e' => $element,
			'group' => $grp
		])
	@endif

	@if($element['type'] == 'text')

		@if($element['key'] == 'pais_de_fabricacion')
			@include('admin.formElements.select_country', [
				'e' => $element,
			])
		@else
			@include('admin.formElements.input', [
				'e' => $element,
			])
		@endif
	@endif

	@if($element['type'] == 'number')
		@include('admin.formElements.number', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'boolean')
		@include('admin.formElements.boolean', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'multiline')
		@include('admin.formElements.text', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'select')
		@include('admin.formElements.select', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'multiselect')
		@include('admin.formElements.multiselect', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'percent10')
		@include('admin.formElements.percent10', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'range_year')
		@include('admin.formElements.rangeYear', [
			'e' => $element,
		])
	@endif
@endforeach

</div>

	@php
		$grp = 1;
		$openGroup = false;
	@endphp
	@foreach($secondaryFields as $element)

		@if($element['type'] == 'titulo1')
			@include('admin.formElements.titulo1', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'titulo2')
			@if($openGroup == true)
				@php echo '</div>'; @endphp
			@endif

			@include('admin.formElements.titulo2', [
				'e' => $element,
				'group' => $grp
			])
			<div id="group_{{ $grp++ }}" class="__hide row" style="display: none;">
				@php
					$openGroup = true;
				@endphp
				@endif

				@if($element['type'] == 'text')
					@if($element['key'] == 'pais_de_fabricacion')
						@include('admin.formElements.select_country', [
							'e' => $element,
						])
					@else
						@include('admin.formElements.input', [
							'e' => $element,
						])
					@endif
				@endif

				@if($element['type'] == 'number')
					@include('admin.formElements.number', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'boolean')
					@include('admin.formElements.boolean', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'multiline')
					@include('admin.formElements.text', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'select')
					@include('admin.formElements.select', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'multiselect')
					@include('admin.formElements.multiselect', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'percent10')
					@include('admin.formElements.percent10', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'range_year')
					@include('admin.formElements.rangeYear', [
						'e' => $element,
					])
				@endif
				@endforeach