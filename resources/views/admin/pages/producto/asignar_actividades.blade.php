@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
{{--<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />--}}
{{--<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />--}}
{{--<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />--}}
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

{{--<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />--}}
{{--<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />--}}

<style>
	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}
</style>


<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}

	.producto_tipos {}
	.producto_tipos > tbody > tr > td {
		padding:5px;
		padding-top:10px;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.producto.index') }}">Productos</a></li>
		<li class="breadcrumb-item active">Producto - Asignar tareas</li>
	</ol>

	<h1 class="page-header page-header-producto-edit">Producto - Asignar tareas</h1>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">

			<table class="m-b-10 width-full">
				<tbody>
				<tr>
					<td style="vertical-align:middle; width:80px; background-color:#ffffff;">
						<img src="{{ $producto->Galerias[0]->Imagenes[0]->ruta_publica_producto_thumb }}" alt="IMG" style="max-width:80px; max-height:80px;">
					</td>
					<td>
						<h3>{{ $producto->nombre_alter }}</h3>
						<br>
						<table id="product_assign_table" class="table table-condensed table-striped">
						@if( $producto->Calendarios->count() > 0 )
							@foreach( $producto->Calendarios[0]->Actividades as $actividad)
								<tr class="asignacion_{{ $actividad->id }}">
									<td>
										<b>{{ $actividad->ActividadTipo->nombre }}</b>
										<br>
										<small>{{ $actividad->created_at->format('d.m.Y H:i:s') }}</small>
									</td>
									<td>
										@foreach( $actividad->Responsables as $responsable )

											@php
												$numero = '';
												if( $responsable->Owner->Agenda->Telefonos && $responsable->Owner->Agenda->Telefonos->count() > 0 ) {
													$numero = $responsable->Owner->Agenda->Telefonos[0]->numero;
												}
												$iniciales = '';
												if( $responsable->Owner && $responsable->Owner->Agenda ) {
													$iniciales = strtoupper( substr( ($responsable->Owner->Agenda->nombres) ? $responsable->Owner->Agenda->nombres : '', 0, 1) ) . strtoupper( substr( ($responsable->Owner->Agenda->apellidos) ? $responsable->Owner->Agenda->apellidos : '', 0, 1) );
												}
												if( $numero ) {
													$numero_class = 'btn-warning';
													$numero_title = '';
												} else {
													$numero_class = 'btn-danger';
													$numero_title = 'No tiene número de whatsapp definido';
												}
											@endphp
											<a href="https://api.whatsapp.com/send?phone={{ $numero }}&text={{ rawurlencode( __('whatsapp.actividad_history_add_images') . ' ' ) }}{{ route('admin.producto.historia', [ $producto->id ]) }}" target="_blank" class="btn {{ $numero_class }} m-b-5" data-title="{{ $numero_title }}" data-toggle="tooltip">
												<i class="fab fa-whatsapp fa-lg"></i> {{ $responsable->username }}
											</a>
											<br>
										@endforeach
									</td>
									<td>
										<a href="javascript:;" onclick="actividadDeleteAsignacion({{ $actividad->id }})" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>
									</td>
								</tr>
							@endforeach
						@endif
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Asignación de actividades</h4>
		</div>
		<div class="panel panel-body">
			<form id="formActividadAsignacionCreate" method="post" class="form-horizontal row">

				{{--<div class="row" style="margin:0px;">--}}
				{{--@foreach( $actividadTipos as $actividadTipo )--}}
					{{--<div class="form-group col-md-4 col-sm-4 row m-b-15">--}}
						{{--<div class="col-md-12">--}}
							{{--<b>{{ $actividadTipo->nombre }}</b>--}}
							{{--<br>--}}
							{{--{{ $actividadTipo->descripcion }}--}}
						{{--</div>--}}
					{{--</div>--}}

					{{--<div class="form-group col-md-4 col-sm-4 row m-b-15">--}}
						{{--<label for="idResponsable" class="col-form-label col-12" style="text-align:left !important;">Responsable(s)</label>--}}
						{{--<div class="col-12">--}}
							{{--{!! Form::select('idResponsable_' . $actividadTipo->id, [], '', [--}}
								{{--'id' => 'idResponsable_' . $actividadTipo->id,--}}
								{{--'class' => 'form-control multiple-select2',--}}
								{{--'data-idat' => $actividadTipo->id,--}}
								{{--'multiple' => 'multiple'--}}
							{{--]) !!}--}}
						{{--</div>--}}
					{{--</div>--}}

					{{--<div id="wa-buttons-{{ $actividadTipo->id }}" class="col-md-4 col-sm-4 ">--}}
						{{--<a href="javascript:;" class="btn btn-warning btn-md"><i class="fab fa-whatsapp fa-lg"></i></a>--}}
					{{--</div>--}}
				{{--@endforeach--}}
				{{--</div>--}}

				<table class="table">
					<tr>
						<th style="width:35%">Tarea</th>
						<th style="width:35%">Responsable(s)</th>
						<th style="width:30%">Mensaje Whatsapp</th>
					</tr>
					@foreach( $actividadTipos as $actividadTipo )
						<tr>
							<td>
								<b>{{ $actividadTipo->nombre }}</b>
								<br>
								{{ $actividadTipo->descripcion }}
							</td>
							@if( $idProducto > 0 )
								<td>
									{!! Form::select('idResponsable_' . $actividadTipo->id, [],'', [
										'id' => 'idResponsable_' . $actividadTipo->id,
										'class' => 'form-control multiple-select2',
										'data-idat' => $actividadTipo->id,
										'data-idp' => $actividadTipo->id,
										'multiple' => 'multiple'
									]) !!}
								</td>
							@else
								<td>
									{!! Form::select('idResponsable_' . $actividadTipo->id, [], '', [
										'id' => 'idResponsable_' . $actividadTipo->id,
										'class' => 'form-control multiple-select2',
										'data-idat' => $actividadTipo->id,
										'data-idp' => $actividadTipo->id,
										'multiple' => 'multiple'
									]) !!}
								</td>

							@endif
							<td>
								<div id="wa-buttons-{{ $actividadTipo->id }}" class="">
									{{--<a href="javascript:;" class="btn btn-warning btn-md"><i class="fab fa-whatsapp fa-lg"></i></a>--}}
									@if( $idProducto > 0 && $producto->Calendarios->count() > 0 && $producto->Calendarios[0]->Actividades->where('idActividadTipo', $actividadTipo->id)->count() > 0)
										<div class="input-group m-b-5">
											<span class="input-group-prepend"><span style="width:75px;" class="input-group-text btn btn-gray">Interno</span></span>
											<input type="text" class="form-control copylink_interno_{{ $actividadTipo->id }}" value="@lang('whatsapp.actividad_history_add_images') {{ route('admin.producto.historia', [ $producto->id ]) }}" />
											<span class="input-group-append">
												<a href="javascript:;" onclick="copylink('copylink_interno_{{ $actividadTipo->id }}')" type="button" class="btn btn-white"><i class="fa fa-copy"></i></a>
												<a href="{{ route('admin.producto.historia', [ $producto->id ]) }}" target="_blank" type="button" class="btn btn-white"><i class="fa fa-external-link-alt"></i></a>
											</span>
										</div>
										<div class="input-group m-b-5">
											<span class="input-group-prepend"><span style="width:75px;" class="input-group-text btn btn-primary">Cliente</span></span>
											<input type="text" class="form-control copylink_cliente_{{ $actividadTipo->id }}" value="Puede hacer seguimiento del estado de su máquina en el siguiente enlace: {{ route('public.seguimiento.index', [ $producto->id ]) }}" />
											<span class="input-group-append">
												<a href="javascript:;" onclick="copylink('copylink_cliente_{{ $actividadTipo->id }}')" type="button" class="btn btn-primary"><i class="fa fa-copy"></i></a>
												<a href="{{ route('public.seguimiento.index', [ $producto->id ]) }}" target="_blank" type="button" class="btn btn-primary"><i class="fa fa-external-link-alt"></i></a>
											</span>
										</div>
									@else
										<a href="javascript:;" id="generar_link_{{ $actividadTipo->id }}" onclick="asignacionGenerar({{ $actividadTipo->id }})" class="btn btn-warning btn-md">Generar link </a>
									@endif

								</div>
							</td>
						</tr>
						{{--<div class="form-group col-md-4 col-sm-4 row m-b-15">--}}
							{{--<div class="col-md-12">--}}

							{{--</div>--}}
						{{--</div>--}}

						{{--<div class="form-group col-md-4 col-sm-4 row m-b-15">--}}
							{{--<label for="idResponsable" class="col-form-label col-12" style="text-align:left !important;">Responsable(s)</label>--}}
							{{--<div class="col-12">--}}
								{{----}}
							{{--</div>--}}
						{{--</div>--}}

						{{--<div id="wa-buttons-{{ $actividadTipo->id }}" class="col-md-4 col-sm-4 ">--}}
							{{--<a href="javascript:;" class="btn btn-warning btn-md"><i class="fab fa-whatsapp fa-lg"></i></a>--}}
						{{--</div>--}}
					@endforeach
				</table>

				{{--<div class="form-group col-md-12 col-sm-12 row m-b-15">--}}
					{{--<label for="idActividadTipos" class="col-form-label col-4">Actividad(es)</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::select('idActividadTipos', [], '', [--}}
							{{--'id' => 'idActividadTipos',--}}
							{{--'class' => 'form-control default-select2',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}

				{{--<div class="form-group col-md-12 col-sm-12 row m-b-15">--}}
					{{--<label for="idResponsable" class="col-form-label col-4">Responsable(s)</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::select('idResponsable', [], '', [--}}
							{{--'id' => 'idResponsable',--}}
							{{--'class' => 'form-control multiple-select2',--}}
							{{--'multiple' => 'multiple'--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}

				{{--<div class="form-group col-md-12 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Información adicional</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::textarea('descripcion', '', [--}}
							{{--'id' => 'descripcion',--}}
							{{--'placeholder' => 'Descripcion',--}}
							{{--'class' => 'form-control',--}}
							{{--'rows' => 6--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;">Cerrar</a>
					{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="asignacionGuardar()" >Guardar</a>--}}
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

{{--<script src="/assets/plugins/moment/min/moment.min.js"></script>--}}
{{--<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>--}}
{{--<script src="/assets/plugins/masked-input/masked-input.js"></script>--}}
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>--}}
{{--<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>--}}

{{--<script src="/assets/plugins/ckeditor/ckeditor.js"></script>--}}
<script>

// Moved to Index, jquery-ui must be loaded before bootstrap
// Required HERE for marcas autocomplete to work properly, still don't know what helps to have it in index.
//$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
//	formInit();
//});

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){

	$('[data-toggle="tooltip"]').tooltip();

	producto_ids = [];
	producto_data = [];
	@if( $idProducto > 0 )
		producto_ids = [{{ $idProducto }}];
		producto_data[{{ $idProducto }}] = {!! json_encode( $producto->toArray() ) !!};
	@else
		producto_ids = producto_batch_ids;
		producto_data = producto_batch_data;
	@endif

	console.log(producto_ids);
	console.log(producto_data);

	// ----------

	var responsables_str = '{{ implode(', ', array_map(function( $v ) { return '#idResponsable_' . $v; }, $actividadTipos->pluck('id')->toArray())) }}';
	console.log(responsables_str);

//	var idResponsableSearch = Search.resource('#idResponsable', {
	var idResponsableSearch = Search.resource('#idResponsable, ' + responsables_str, {
		url: '{{ route('resource.usuario.ds') }}',
		data: {
			idTipo: [ {{ \App\Models\Usuario::TIPO_ADMINISTRADOR }}, {{ \App\Models\Usuario::TIPO_EMPLEADO }} ]
		},
		templateResult: function( data ) {
			var numero = '';
			if( data.owner.agenda.telefonos && data.owner.agenda.telefonos.length > 0 ) {
				numero = data.owner.agenda.telefonos[0].numero;
			}
			return data.owner.agenda.nombres_apellidos + '<br><b>' + numero + '</b> &lt;' + data.username + '&gt;';
		},
		templateSelection: function( data ) {
			return data.owner.agenda.nombres_apellidos;
		}
	}).on('select2:select', function( e ) {
		var data = e.params.data;
		console.log(data);
//		console.log($(e.target).val());

		var idActividadTipo = parseInt( $(e.target).data('idat'), 10);
		var numero = '';
		if( data.owner.agenda.telefonos && data.owner.agenda.telefonos.length > 0 ) {
			numero = data.owner.agenda.telefonos[0].numero;
		}

		var iniciales = '';
		if( data.owner && data.owner.agenda ) {
			iniciales = data.owner.agenda.nombres[0].toUpperCase() || '' + data.owner.agenda.apellidos[0].toUpperCase() || '';
		}

		urlSA = '{{ route('resource.producto.setActividades') }}';
		dataSA = {
			'idProductos' : producto_ids,
			'idResponsables' : [ data.id ],
			'idActividadTipos' : [ idActividadTipo ],
		};

		ajaxPost(urlSA, dataSA, function( response ) {
			if( numero ) {
				numero_class = 'btn-warning';
				numero_title = '';
			} else {
				numero_class = 'btn-danger';
				numero_title = 'No tiene número de whatsapp definido';
			}
			{{--var a = $('<a>', {--}}
				{{--href: 'https://api.whatsapp.com/send?phone=' + numero + '&text={{ rawurlencode( __('whatsapp.actividad_history_add_images') . ' ' ) }}https://www.expoindustri.com/seguimiento/' + response.data[0].id,--}}
				{{-- TELEGRAM: REF.: https://apperlas.com/enlace-personalizado-en-telegram/ --}}
						{{--href: 'https://t.me/ALIAS?phone=' + data.owner.agenda.telefonos[0].numero + '&text={{ rawurlencode( __('messages.whatsapp_more_info') . ' ' ) }}https://www.expoindustri.com/seguimiento/123456',--}}
				{{--'id': 'wa_' + idActividadTipo + '_' + data.id,--}}
				{{--'class': 'btn '+numero_class+' btn-md m-l-10',--}}
				{{--'title': numero_title,--}}
				{{--'data-toggle': 'tooltip'--}}
			{{--}).html('<i class="fab fa-whatsapp fa-lg"></i> ' + iniciales);--}}

			{{--$('#wa-buttons-' + $(e.target).data('idat')).append( a );--}}

			// ----------

			if( $('.asignacion_' + response.data[0].id ).length === 0 ) {
				var tr = $('<tr>', {
					'class' : 'asignacion_' + response.data[0].id,
				});
				var td = $('<td>');
				td.html('<b>' + response.data[0].actividad_tipo.nombre + '</b><br><small>'+ format_datetime(response.data[0].created_at) +'</small>');
				tr.append( td );

				td = $('<td>');
				var link_historia = '{{ route('admin.producto.historia', [ $producto->id ]) }}';
				td.html('<a href="https://api.whatsapp.com/send?phone=' + numero + '&text={{ rawurlencode( __('whatsapp.actividad_history_add_images') . ' ' ) }}' + link_historia + '" target="_blank" class="btn '+ numero_class +' m-b-5" data-title="'+ numero_title +'" data-toggle="tooltip"><i class="fab fa-whatsapp fa-lg"></i> '+ data.username +'</a><br>');
				tr.append( td );

				td = $('<td>');
				td.html('<a href="javascript:;" onclick="actividadDeleteAsignacion('+ response.data[0].id +')" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>');
				tr.append( td );

				$('#product_assign_table').append( tr );
			} else {

				td = $('.asignacion_' + response.data[0].id + ' td')[1];
				td = $(td);

//				td = $('<td>');
//				td.html('<b>' + response.data[0].actividad_tipo.nombre + '</b><br><small>'+ format_datetime(response.data[0].created_at) +'</small>');
//				tr.append( td );
				var link_historia = '{{ route('admin.producto.historia', [ $producto->id ]) }}';
				a = $('<a>', {
					'href' : 'https://api.whatsapp.com/send?phone=' + numero + '&text={{ rawurlencode( __('whatsapp.actividad_history_add_images') . ' ' ) }}'+ link_historia ,
					'target' : '_blank',
					'class' : 'btn '+ numero_class +' m-b-5',
					'data-title' : numero_title,
					'data-toggle' : 'tooltip'
				}).html( '<i class="fab fa-whatsapp fa-lg"></i> '+ data.username );

				{{--td.append('<a href="https://api.whatsapp.com/send?phone=' + numero + '&text={{ rawurlencode( __('whatsapp.actividad_history_add_images') . ' ' ) }}https://www.expoindustri.com/seguimiento/' + response.data[0].id + '" target="_blank" class="btn '+ numero_class +' m-b-5" data-title="'+ numero_title +'" data-toggle="tooltip"><i class="fab fa-whatsapp fa-lg"></i> '+ data.username +'</a><br>');--}}
				td.append(a);
				td.append('<br>');

//				td = $('<td>');
//				td.html('<a href="javascript:;" onclick="actividadDeleteAsignacion('+ response.data[0].id +')" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>');
//				tr.append( td );
			}
		} );

	}).on('select2:unselect', function( e ) {
		var data = e.params.data;
		$('#wa_' + $(e.target).data('idat') + '_' + data.id).remove();
//		console.log(data);
//		console.log($(e.target).val());
	});

	var idActividadTipoSearch = Search.resource('#idActividadTipos', {
		url: '{{ route('resource.actividadTipo.ds') }}',
		data: {
			soloHijos: 1
		},
		templateResult: function( data ) {
			return data.nombre;
		},
		templateSelection: function( data ) {
			return data.nombre;
		}
	});


//	for(var i = 0 ; i < producto_ids.length ; i++) {
//
//		var tr = $( '<tr>' );
//		var td = $( '<td>', {
//			style: 'vertical-align:middle; width:80px; background-color:#ffffff;'
//		} );
//		var img = $( '<img>', {
//			'src': producto_data[producto_ids[i]].galerias[0].imagenes[0].ruta_publica_producto_thumb,
//			'style': 'max-width:80px; max-height:80px;'
//		} );
//		td.append( img );
//		tr.append( td );
//
//		td = $( '<td>', {
//			style: 'vertical-align:middle;'
//		} ).html( producto_data[producto_ids[i]].nombre_alter );
//		tr.append( td );
//
//		var product_assign_table = $( '#product_assign_table' ).find('tbody');
//		product_assign_table.append( tr );
//	}

}

function asignacionGenerar( idActividadTipo ) {
	var url = '{{ route('resource.producto.setActividades') }}';

	var formActividadAsignacionCreate = $('#formActividadAsignacionCreate');
	console.log(producto_ids);

	@if( $idProducto > 0 )
		producto_ids = [{{ $idProducto }}];
	@else
		producto_ids = producto_batch_ids;
	@endif

	var data = {
		idProductos         : producto_ids, //formActividadAsignacionCreate.find('#idResponsable').val(),
		idResponsables      : [], //formActividadAsignacionCreate.find('#idResponsable').val(),
		idActividadTipos    : [ idActividadTipo ], //[ formActividadAsignacionCreate.find('#idActividadTipos').val() ],
		descripcion         : '', //formActividadAsignacionCreate.find('#descripcion').val()
	};
	ajaxPost(url, data, function( response ) {

		$('#generar_link_' + response.data[0].idActividadTipo).remove();

		var link_historia = '{{ route('admin.producto.historia', [ $producto->id ]) }}';
		var link_seguimiento = '{{ route('public.seguimiento.index', [ $producto->id ]) }}';
		$('#wa-buttons-' + response.data[0].idActividadTipo)
			.append( '<div class="input-group m-b-5"><span class="input-group-prepend"><span style="width:75px;" class="input-group-text btn btn-gray">Interno</span></span><input type="text" class="form-control copylink_interno_'+response.data[0].idActividadTipo+'" value="@lang('whatsapp.actividad_history_add_images') '+link_historia+'" /><span class="input-group-append"><a href="javascript:;" onclick="copylink(\'copylink_interno_'+response.data[0].idActividadTipo+'\')" type="button" class="btn btn-white dropdown-toggle no-caret"><i class="fa fa-copy"></i></a><a href="'+link_historia+'" target="_blank" type="button" class="btn btn-white"><i class="fa fa-external-link-alt"></i></a></span></div>' )
			.append( '<div class="input-group m-b-5"><span class="input-group-prepend"><span style="width:75px;" class="input-group-text btn btn-primary">Cliente</span></span><input type="text" class="form-control copylink_cliente_'+response.data[0].idActividadTipo+'" value="Puede hacer seguimiento del estado de su máquina en el siguiente enlace: '+link_seguimiento+'" /><span class="input-group-append"><a href="javascript:;" onclick="copylink(\'copylink_cliente_'+response.data[0].idActividadTipo+'\')" type="button" class="btn btn-white"><i class="fa fa-copy"></i></a><a href="'+link_seguimiento+'" target="_blank" type="button" class="btn btn-primary"><i class="fa fa-external-link-alt"></i></a></span></div>' );

		if( $('.asignacion_' + response.data[0].id ).length === 0 ) {
			var tr = $('<tr>', {
				'class' : 'asignacion_' + response.data[0].id,
			});
			var td = $('<td>');
			td.html('<b>' + response.data[0].actividad_tipo.nombre + '</b><br><small>'+ format_datetime(response.data[0].created_at) +'</small>');
			tr.append( td );

			td = $('<td>');
			{{--td.html('<a href="https://api.whatsapp.com/send?phone=' + numero + '&text={{ rawurlencode( __('whatsapp.actividad_history_add_images') . ' ' ) }}https://www.expoindustri.com/seguimiento/' + response.data[0].id + '" target="_blank" class="btn '+ numero_class +' m-b-5" data-title="'+ numero_title +'" data-toggle="tooltip"><i class="fab fa-whatsapp fa-lg"></i> '+ data.username +'</a><br>');--}}
			tr.append( td );

			td = $('<td>');
			td.html('<a href="javascript:;" onclick="actividadDeleteAsignacion('+ response.data[0].id +')" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>');
			tr.append( td );

			$('#product_assign_table').append( tr );
		}

//		$(document).trigger('formAsignacionActividad:success', response.data);
	});
}

function actividadDeleteAsignacion( id ){
	var ids = [];
	ids = [ id ];

	swal({
		text: 'Está seguro que desea eliminar los registros seleccionados?',
		icon: 'warning',
		buttons: {
			cancel: 'No',
			ok: 'Si'
		},
		dangerMode: true
	}).then(function(response) {
		if (response === 'ok') {
			var url = '{{ route('resource.actividad.destroy', [0]) }}';
			ajaxDelete(url, ids, function(){
//				swal('Registros eliminados', { icon: "success" });
				$('.asignacion_' + id ).remove();
			});
		}
	});
}

function copylink( classname ) {
	var dummy = $('.' + classname).select();
	document.execCommand('copy')
}

</script>
@endpush

