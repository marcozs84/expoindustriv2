@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li><a href="{{ route('admin.producto.index') }}">Productos</a></li>
		<li class="active">Nuevo Producto Item</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Nuevo Producto Item</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo ProductoItem</h4>
		</div>
		<div class="panel-body">
			<form id="formProductoItemPrecioCreate" >

                <div class="form-group row m-b-10">
	                <label class="col-form-label col-md-3">USD</label>
                    <div class="col-md-9">
	                    {!! Form::number('usd', $usd , [
                            'id' => 'usd',
                            'placeholder' => 'USD',
                            'class' => 'form-control form-control-sm m-b-5'
                        ]) !!}
                    </div>
                </div>

                <div class="form-group row m-b-10">
	                <label class="col-form-label col-md-3">SEK</label>
                    <div class="col-md-9">
	                    {!! Form::number('sek', $sek , [
                            'id' => 'sek',
                            'placeholder' => 'SEK',
                            'class' => 'form-control form-control-sm m-b-5'
                        ]) !!}
                    </div>
                </div>

                <div class="form-group row m-b-10">
	                <label class="col-form-label col-md-3">CLP</label>
                    <div class="col-md-9">
	                    {!! Form::number('clp', $clp , [
                            'id' => 'clp',
                            'placeholder' => 'CLP',
                            'class' => 'form-control form-control-sm m-b-5'
                        ]) !!}
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group row m-b-15">
                    <div class="col-lg-12 text-right">
                        <a class="btn btn-white" data-dismiss="modal" href="javascript:;">Cancelar</a>
                        <a class="btn btn-primary" href="javascript:;" onclick="guardarProductoItemPrecio()" >Guardar</a>
                    </div>
                </div>
            </form>
		</div>
	</div>

@stop

@push('scripts')
<script>
$(function(){
	initView();
});

function initView(){
//	$(".default-select2").select2();
}

function guardarProductoItemPrecio(){

	console.log("guardando");

{{--	var url = '{{ route('admin.producto.store_item', [ $producto->id ]) }}';--}}
	var url = '{{ route( 'resource.precio.store' ) }}';
	var form = $('#formProductoItemPrecioCreate');
	var idTipo = {{ \App\Models\Precio::TIPO_PRECIO_VENTA }};
	var data = {
		precio: form.find('#usd').val(),
		moneda: 'usd',
		idTipo: idTipo,
		estado: 1,
		modelo_type: 'producto_item',
		modelo_id: {{ $productoItem->id }}
    };
	ajaxPost(url, data, function(data){
		data = {
			precio: form.find('#sek').val(),
			moneda: 'sek',
			idTipo: idTipo,
			estado: 1,
			modelo_type: 'producto_item',
			modelo_id: {{ $productoItem->id }}
		};
		ajaxPost(url, data, function(data){
			data = {
				precio: form.find('#clp').val(),
				moneda: 'clp',
				idTipo: idTipo,
				estado: 1,
				modelo_type: 'producto_item',
				modelo_id: {{ $productoItem->id }}
			};
			ajaxPost(url, data, function(data){
				$(document).trigger('formProductoItemPrecioCreate:success');
			});
		});
	});
}

</script>
@endpush

