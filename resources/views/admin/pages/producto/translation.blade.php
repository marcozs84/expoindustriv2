@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Traduccion</li>
	</ol>

	<h1 class="page-header">Traducción</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Traduccion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTranslation" method="get" class="form-horizontal">
				<table class="table table-condensed table-striped">
					<tr>
						<th class="text-center">Texto Original</th>
						<th class="text-center">Español</th>
						<th class="text-center">Sueco</th>
						<th class="text-center">Inglés</th>
					</tr>
					<tr>
						<td class="text-center" style="vertical-align:middle;">@lang('form.'.$key)</td>
						<td style="vertical-align:middle;">
							{!! Form::text('espaniol', $espaniol, ['id' => 'espaniol', 'class' => 'form-control']) !!}
						</td>
						<td style="vertical-align:middle;">
							{!! Form::text('sueco', $sueco, ['id' => 'sueco', 'class' => 'form-control']) !!}
						</td>
						<td style="vertical-align:middle;">
							{!! Form::text('ingles', $ingles, ['id' => 'ingles', 'class' => 'form-control']) !!}
						</td>
					</tr>
				</table>
				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="translationCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="translationSave()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@section('libraries')
@stop

@push('scripts')
	<script>

		$( "#formTranslation" ).submit(function( event ) {
			translationSave();
			event.preventDefault();
		});

		function translationSave(){
			var url = '{{ route('admin.producto.translation', [ $prodId, $key ]) }}';
			ajaxPost(url, {
				es: $('#espaniol').val(),
				se: $('#sueco').val(),
				en: $('#ingles').val()
			}, function(){
				$(document).trigger('formTranslation:aceptar');
			});
		}

		function translationCancel(){
			$(document).trigger('formTranslation:cancelar');
		}

		function formInit(){
		}

	</script>
@endpush

