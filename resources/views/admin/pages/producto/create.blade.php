@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<style>
	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}
</style>


<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.producto.index') }}">Productos</a></li>
		<li class="breadcrumb-item active">Registrar producto</li>
	</ol>

	<h1 class="page-header">Registrar producto</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Producto</h4>
		</div>
		<div class="panel panel-body">
			<div id="formProductoCreate" class="form-horizontal">

				<div class="row">

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Marca</label>
						<div class="col-sm-8">
							{!! Form::text('marca', '', [
								'id' => 'marca',
								'placeholder' => '',
								'class' => 'form-control',
								'onblur' => 'getModelos(this)',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Modelo</label>
						<div class="col-sm-8">
							{!! Form::text('modelo', '', [
								'id' => 'modelo',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Categoria</label>
						<div class="col-sm-8">
							{!! Form::select('categoria', $categorias->pluck('traduccion.'.App::getLocale(), 'id'), [], [
							    'id' => 'categoria',
							    'placeholder' => __('messages.select_category'),
							    'class' => 'form-control',
							    'onchange' => 'changeSubs(this)',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Sub-categoria</label>
						<div class="col-sm-8">
							{!! Form::select('subcategoria', [], [], [
							    'id' => 'subcategoria',
							    'class' => 'form-control',
							    'placeholder' => __('messages.select_category'),
							    'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Tags</label>
						<div class="col-sm-8">
							{!! Form::select('tags', $grouped_tags, [], [
								'id' => 'tags',
								'class' => 'form-control multiple-select2',
								'multiple' => 'multiple',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Estado</label>
						<div class="col-sm-8">
							{!! Form::select('esNuevo', [
								'' => 'No definido',
								1 => 'Nuevo',
								0 => 'Usado',
							], null, [
								'id' => 'esNuevo',
								'class' => 'form-control default-select2',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Tipo (maquinaria/accesorio)</label>
						<div class="col-sm-8">
							{!! Form::select('idProductoTipo', [
								'' => 'No definido',
								1 => 'Maquinaria',
								2 => 'Accesorio',
							], null, [
								'id' => 'idProductoTipo',
								'class' => 'form-control default-select2',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

				</div>

				<h4>Ubicación</h4>

				<div class="row">
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">País</label>
						<div class="col-sm-8">
							{!! Form::select('pais', $global_paises, null, [
							    'id' => 'pais',
							    'class' => 'form-control',
							    'placeholder' => 'País',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Ciudad</label>
						<div class="col-sm-8">
							{!! Form::text('ciudad', '', [
								'id' => 'ciudad',
								'placeholder' => 'Ciudad',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Código postal</label>
						<div class="col-sm-8">
							{!! Form::text('codigo_postal', '', [
								'id' => 'codigo_postal',
								'placeholder' => 'Código postal',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Direccion</label>
						<div class="col-sm-8">
							{!! Form::textarea('direccion', null, [
							'id' => 'direccion',
							'class' => 'form-control',
							'rows' => 4,
							'placeholder' => 'Direccion',
						]) !!}
						</div>
					</div>

				</div>

				<h4>Dimensiones</h4>

				<div class="row">

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Longitud (mm)</label>
						<div class="col-sm-8">
							{!! Form::text('longitud', '', [
								'id' => 'longitud',
								'placeholder' => 'Longitud (mm)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Ancho (mm)</label>
						<div class="col-sm-8">
							{!! Form::text('ancho', '', [
								'id' => 'ancho',
								'placeholder' => 'Ancho (mm)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Alto (mm)</label>
						<div class="col-sm-8">
							{!! Form::text('alto', '', [
								'id' => 'alto',
								'placeholder' => 'Alto (mm)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Peso bruto (kg)</label>
						<div class="col-sm-8">
							{!! Form::text('peso_bruto_fix', '', [
								'id' => 'peso_bruto_fix',
								'placeholder' => 'Peso bruto (kg)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Paletizado</label>
						<div class="col-sm-8">
							{!! Form::checkbox('paletizado', 1, false, [
								'id' => 'paletizado',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

				</div>

				<h4>Datos del cliente / propietario</h4>

				<div class="row">

					@if($idUsuario == 0)
					<div class="form-group col-md-6 row m-b-15 group-cliente">
						<label class="col-form-label col-md-4">Empresa / Cliente</label>
						<div class="col-md-8">
							{!! Form::select('searchCliente', [], null, [
								'id' => 'searchCliente',
								'class' => 'form-control default-select2',
								'autofocus'
							]) !!}
							<a href="javascript:;" class="btn btn-primary" onclick="clienteCreate()" style="margin-top:5px;">Nuevo Cliente</a>
							<a href="javascript:;" class="btn btn-primary btnClienteEdit hide" onclick="clienteEdit()" style="margin-top:5px;">Editar Cliente</a>
						</div>
					</div>

					<div class="col-md-6 p-b-10 hide" id="pnlContacto">
						<div class="panel panel-info">
							<div class="panel-body bg-black-transparent-2 text-black" style="padding-bottom:10px;">
								<table class="tblProveedor">
									<tr>
										<th style="width:50%">Datos Contacto</th>
										<th style="width:50%">Datos Empresa</th>
									</tr>
									<tr>
										<td>
											<address>
												<strong class="pContacto"></strong><br />
												<a href="javascript:;" class="text-black"><span class="pCorreoContacto"></span></a>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos"></span>
											</address>
										</td>
										<td>
											<address>
												<strong class="pNombreProv">Twitter, Inc.</strong><br />
												<span class="pDireccion"></span>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos"></span>
											</address>
											<strong>Otros contactos:</strong><br>
											<span class="pContactos"></span>
										</td>
									</tr>
								</table>

							</div>
						</div>
					</div>
					@else
						<div class="col-md-12 p-b-10" id="pnlContacto">
							<div class="panel panel-info">
								<div class="panel-body bg-black-transparent-2 text-black" style="padding-bottom:10px;">
									<table class="tblProveedor">
										<tr>
											<th style="width:50%">Datos Contacto</th>
											<th style="width:50%">Datos Empresa</th>
										</tr>
										<tr>
											<td>
												<address>
													<strong class="pContacto">{{ $usuario->Owner->Agenda->nombres_apellidos }}</strong><br />
													<a href="javascript:;" class="text-black"><span class="pCorreoContacto">{{ $usuario->username }}</span></a>
													<br>
													<strong>Telefonos:</strong> <span class="pTelefonos">
													@if($usuario->Owner->Proveedor->Telefonos)
															{{ implode(', ', $usuario->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) }}
														@endif
												</span>
												</address>
											</td>
											<td>
												<address>
													<strong class="pNombreProv">{{ ($usuario->Owner->Proveedor->nombre == '') ? '- - - ' : $usuario->Owner->Proveedor->nombre }}</strong><br />
													<span class="pDireccion">
													@if($usuario->Owner->Proveedor->Direcciones)
															{!! implode('<br> ', $usuario->Owner->Proveedor->Direcciones->pluck('direccion')->toArray()) !!}
														@endif
												</span>
													<br>
													<strong>Telefonos:</strong> <span class="pTelefonos">
													@if($usuario->Owner->Proveedor->Telefonos)
															{{ implode(', ', $usuario->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) }}
														@endif
												</span>
												</address>
												{{--<strong>Otros contactos:</strong><br>--}}
												{{--<span class="pContactos"></span>--}}
											</td>
										</tr>
									</table>

								</div>
							</div>
						</div>
					@endif

				</div>

				<div class="row">

					{{--<div class="form-group col-md-12 row m-b-15">--}}
						{{--<label class="col-form-label col-md-4">Precio minimo</label>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::text('precioMinimo', '', [--}}
								{{--'id' => 'precioMinimo',--}}
								{{--'placeholder' => 'Precio minimo',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::select('currencyMinimo', $moneda, '', [--}}
								{{--'id' => 'currencyMinimo',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
					{{--</div>--}}

					<div class="form-group col-md-12 row m-b-15">
						<label class="col-form-label col-md-4">Precio proveedor</label>
						<div class="col-sm-4">
							{!! Form::text('precio', '', [
								'id' => 'precio',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
						<div class="col-sm-4">
							{!! Form::select('currency', $moneda, ($global_country_abr == 'se') ? 'sek' : '', [
								'id' => 'currency',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					{{--<div class="form-group col-md-12 row m-b-15">--}}
						{{--<label class="col-form-label col-md-4">Precio venta</label>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::text('precioVenta', '', [--}}
								{{--'id' => 'precioVenta',--}}
								{{--'placeholder' => '',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::select('currencyVenta', $moneda, ($global_country_abr == 'se') ? 'sek' : '', [--}}
								{{--'id' => 'currencyVenta',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
					{{--</div>--}}

					<div class="form-group col-md-12 row m-b-15">
						<label class="col-form-label col-md-4">Precio Europa</label>
						<div class="col-sm-4">
							{!! Form::text('precioEuropa', '', [
								'id' => 'precioEuropa',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
						<div class="col-sm-4">
							{!! Form::select('currencyEuropa', $moneda, ($global_country_abr == 'se') ? 'sek' : '', [
								'id' => 'currencyEuropa',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Solo Sudamérica</label>
						<div class="col-sm-8">
							{!! Form::checkbox('priceSouthamerica', 1, false, [
								'id' => 'priceSouthamerica',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>


					{{--<div class="form-group col-md-6 row m-b-15">--}}
						{{--<label class="col-form-label col-md-4">Fecha de producto</label>--}}
						{{--<div class="col-sm-8">--}}
							{{--{!! Form::text('fechaProducto', '', [--}}
								{{--'id' => 'fechaProducto',--}}
								{{--'placeholder' => '',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
					{{--</div>--}}
				</div>

				<h4>Detalles de la maquina <small><a href="javascript:;" onclick="toggleCollapse('#formHolder')">Ver / Ocultar detalles</a></small></h4>

				<div id="formHolder" class="/*hide*/" style="margin-left:10px;"></div>

				<hr>

				<h4>Galería de imágenes</h4>
				<div class="form-group row m-b-15">
					{{--<label class="col-form-label col-md-3">Fotografías / Captura</label>--}}
					<div class="col-md-12">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
						{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
						{{--<div class="form-group">--}}
						{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
						{{--</div>--}}
						{{--<div class="form-group">--}}
						{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
						{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.bannerSuperior.upload') }}" class="dropzone">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="idProducto" id="idProducto" value="0">
							<div class="dz-message" data-dz-message><span>
									{{--@lang('messages.dz_upload')--}}
									Adjuntar producto
								</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor no cierre la ventana hasta que se suban todos los archivos.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>


				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-md-6 text-left">
						{{--<a class="btn btn-warning btn-sm" href="javascript:;" onclick="usuarioCreate()">Nuevo Cliente</a>--}}
					</div>
					<div class="col-md-6 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="productoCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="productoGuardar()" >Guardar</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>

// Moved to Index, jquery-ui must be loaded before bootstrap
// Required HERE for marcas autocomplete to work properly, still don't know what helps to have it in index.
$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
	formInit();
});

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

var myDropzone = '';
$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
	Dropzone.autoDiscover = false;
	setTimeout(function(){
		{{--$('div#dropzone').dropzone({--}}
		{{--url: '{{ route('admin.bannerSuperior.upload') }}'--}}
		{{--});--}}  // comented so it fixes the already attached dropzone

		myDropzone = new Dropzone(".dropzone", {
			url: "{{ route('admin.producto.upload') }}",
			addRemoveLinks: true,
			autoProcessQueue: false,
			parallelUploads: 1,
			init: function() {
				{{--@foreach($images as $image)--}}
				{{--var mockFile = {--}}
				{{--name: '{{ $image['filename'] }}',--}}
				{{--size: 123,--}}
				{{--};--}}
				{{--this.options.addedfile.call(this, mockFile);--}}
				{{--this.options.thumbnail.call(this, mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
				{{--this.emit('thumbnail', mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
				{{--this.emit('complete', mockFile);--}}
				{{--// Src.: https://stackoverflow.com/a/22719947;--}}
				{{--@endforeach--}}

				this.on("queuecomplete", function(file) {
					$(document).trigger('formProductoCreate:aceptar');
				});
				this.on("removedfile", function(file) {
					//alert(file.name);
					console.log('Eliminado: ' + file.name);

					file.previewElement.remove();

					// Create the remove button
					var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

					//Capture the Dropzone instance as closure.
					var _this = this;

					// Listen to the click event
					//					removeButton.addEventListener();

					// Add the button to the file preview element.
					file.previewElement.appendChild(removeButton);
				});
			},
			success: function(file, response){
				console.log("success");
				console.log(file.name);
				console.log(response);
				myDropzone.processQueue();
//					$(document).trigger('formProductoCreate:aceptar');
			},
			removedfile: function(file){
				x = confirm('@lang('messages.confirm_removal')');
				if(!x) return false;
//					removeImage(1, file.name);
//					for(var i = 0 ; i < file_up_names.length; i++){
//						if(file_up_names[i] === file.name){
//							ajaxPost('public.publishRemove', {
//								'archivo' : ''
//							});
//						}
//					}
			}
		});
	}, 1000);

	$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));

});

$( "#formProductoCreate" ).submit(function( event ) {
	productoGuardar();
	event.preventDefault();
});

var idUsuarioPropietario = {{ $idUsuario }};

function formInit(){

	$("[data-toggle=tooltip]").tooltip();

	var availableTags = {!! json_encode($marcas) !!};
	$('#formProductoCreate #marca').autocomplete({
		source: availableTags
	});

	$('#formProductoCreate #pais').select2();
	$('#formProductoCreate #tags').select2();

	// ----------

	$('#formProductoCreate #searchCliente').select2({
        allowClear: true,
		placeholder: {
			id: "",
			placeholder: "Leave blank to ..."
		},
		ajax: {
			{{--				url: '{{ route('resource.proveedorContacto.ds') }}',--}}
			url: '{{ route('resource.cliente.ds') }}',
			dataType: 'json',
			method: 'POST',
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			delay: 250,
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page,
					proveedor_data: 1 // Utilizado para que el 'ds' consultado retorne informacion más completa
				};
			},
			statusCode: {
				422: ajaxValidationHandler,
				500: ajaxErrorHandler
			},
			processResults: function (data, params) {
				// parse the results into the format expected by Select2
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data, except to indicate that infinite
				// scrolling can be used
				params.page = data.current_page || 1;

				return {
					results: data.data,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: function(repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if (repo.loading) return repo.text;

			var companyName = '';
			if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
				companyName = '- - -';
			} else {
				companyName = repo.usuario.owner.proveedor.nombre;
			}
			var otrosContactos = [];

			var markup = "<div>" + repo.agenda.nombres_apellidos + '<br><b>' + companyName + "</b><br>" + otrosContactos.join('<br> ') + "</div>";
			return markup;
		},
		templateSelection: function formatRepoSelection (repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if(repo.usuario){

				idUsuarioPropietario = repo.idUsuario;

				var companyName = '';
				if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
					companyName = '- - -';
				} else {
					companyName = repo.usuario.owner.proveedor.nombre;
				}

				$('#formProductoCreate #nuevoCliente option[value="0"]').attr('selected', 'selected');

				// ----------

				$('#formProductoCreate .pNombreProv').html(companyName);

				var otrosContactos = [];
				if(repo.usuario.owner.proveedor.proveedor_contactos && repo.usuario.owner.proveedor.proveedor_contactos.length > 0) {
					$.each(repo.usuario.owner.proveedor.proveedor_contactos, function(k,v) {
						if(parseInt(v.id, 10) !== parseInt(repo.id, 10)) {
							otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
						}
					});
				}

				var telefonos = [];
				if(repo.usuario.owner.proveedor.telefonos && repo.usuario.owner.proveedor.telefonos.length > 0) {
					$.each(repo.usuario.owner.proveedor.telefonos, function(k,v) {
						if(v.id !== repo.id) {
							telefonos.push(v.numero);
						}
					});
				}

				var direcciones = [];
				if(repo.usuario.owner.proveedor.direcciones && repo.usuario.owner.proveedor.direcciones.length > 0) {
					$.each(repo.usuario.owner.proveedor.direcciones, function(k,v) {
						if(v.id !== repo.id) {
							direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
						}
					});
				}

				$('#formProductoCreate .pContacto').html(repo.agenda.nombres_apellidos);
				$('#formProductoCreate .pCorreoContacto').html(repo.usuario.username);
				$('#formProductoCreate .pContactos').html(otrosContactos.join('<br> '));
				$('#formProductoCreate .pCorreos').html(repo.usuario.owner.proveedor.correos);
				$('#formProductoCreate .pTelefonos').html(telefonos.join(', '));
				$('#formProductoCreate .pDireccion').html(direcciones.join('<br>'));

				$('#pnlContacto').removeClass('hide');

				// ----------

				return repo.agenda.nombres_apellidos + ' [' + companyName + ']';
			} else {
				idUsuarioPropietario = 0;
//				$('#formProductoCreate .btnProveedorEdit').addClass('hide');
				return repo.text;
			}
			//return repo.nombre + ' ' + repo.apellidos || repo.text;
		},
//	    allowClear: true
	})
		.trigger('change');
}

function productoGuardar(){
	var url = '{{ route('admin.producto.store') }}';
	var form = $('#formProductoCreate');

	var data = {
		marca:          form.find('#marca').val(),
		modelo:         form.find('#modelo').val(),
		idCategoria:    form.find('#subcategoria').val(),

		pais:           form.find('#pais').val(),
		ciudad:         form.find('#ciudad').val(),
		codigo_postal:  form.find('#codigo_postal').val(),
		direccion:      form.find('#direccion').val(),

		longitud:       form.find('#longitud').val(),
		ancho:          form.find('#ancho').val(),
		alto:           form.find('#alto').val(),
		peso_bruto_fix: form.find('#peso_bruto_fix').val(),
		paletizado: (   form.find('#paletizado').prop('checked') ) ? 1 : 0 ,

		idUsuario:      idUsuarioPropietario, //$('#searchCliente').val(),
		precio:         form.find('#precio').val(),
		currency:       form.find('#currency').val(),
//		precioVenta:    form.find('#precioVenta').val(),
//		currencyVenta:  form.find('#currencyVenta').val(),
//		precioMinimo:   form.find('#precioMinimo').val(),
//		currencyMinimo: form.find('#currencyMinimo').val(),
		precioEuropa:   form.find('#precioEuropa').val(),
		currencyEuropa: form.find('#currencyEuropa').val(),
		priceSouthamerica:  (form.find('#priceSouthamerica').prop('checked')) ? 1 : 0,

		tags:           form.find('#tags').val(),
		esNuevo:        form.find('#esNuevo').val(),
		idProductoTipo: form.find('#idProductoTipo').val(),

		multiselects: getKeys()
	};

	$.each(campos, function(k,v) {
//		console.log(v.key);
		data[v.key] = $('#'+v.key).val();
	});

	console.log(data);

	ajaxPost(url, data, function(data){

		$('#idProducto').val(data.data.id);

		if(myDropzone.files.length === 0) {
			$(document).trigger('formProductoCreate:aceptar');
		} else {
			myDropzone.processQueue();
		}
	});
}

function productoCancelar(){
	@if($isAjaxRequest)
		$(document).trigger('formProductoCreate:cancelar');
	@else
		redirect('{{ route('admin.producto.index')  }}');
	@endif
}

function clienteCreate(){
	var url = '{{ route('admin.usuario.create', [ 'tipo' => 'cliente' ]) }}';
	var modal = openModal(url, 'Nuevo Cliente');
	setModalHandler('formUsuarioCreate:aceptar', function(event, data){
		var usuario = data;
		if(usuario.owner.proveedor.nombre === null) {
			usuario.owner.proveedor.nombre = '';
		}
		var companyName = '';
		if(usuario.owner.proveedor.nombre === '' || usuario.owner.proveedor.nombre === null) {
			companyName = '- - -';
		} else {
			companyName = usuario.owner.proveedor.nombre;
		}
		var option = $('<option>', {
			value: usuario.cliente.id,
			'selected' : 'selected'
		}).html(usuario.owner.agenda.nombres_apellidos + ' [' + companyName + ']');
		$('#formProductoCreate #searchCliente').html('');
		$('#formProductoCreate #searchCliente').append(option).trigger('change');

		$('#formProductoCreate #nuevoCliente option[value="1"]').attr('selected', 'selected');

		$('#formProductoCreate .pNombreProv').html(companyName);

		var otrosContactos = [];
		if(usuario.owner.proveedor.proveedor_contactos && usuario.owner.proveedor.proveedor_contactos.length > 0) {
			$.each(usuario.owner.proveedor.proveedor_contactos, function(k,v) {
				if(v.id !== usuario.id) {
					otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
				}
			});
		}

		var telefonos = [];
		if(usuario.owner.proveedor.telefonos && usuario.owner.proveedor.telefonos.length > 0) {
			$.each(usuario.owner.proveedor.telefonos, function(k,v) {
				if(v.id !== usuario.id) {
					telefonos.push(v.numero);
				}
			});
		}

		var direcciones = [];
		if(usuario.owner.proveedor.direcciones && usuario.owner.proveedor.direcciones.length > 0) {
			$.each(usuario.owner.proveedor.direcciones, function(k,v) {
				if(v.id !== usuario.id) {
					direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
				}
			});
		}

		$('#formProductoCreate .pContacto').html(usuario.owner.agenda.nombres_apellidos);
		$('#formProductoCreate .pCorreoContacto').html(usuario.username);
		$('#formProductoCreate .pContactos').html(otrosContactos.join('<br> '));
		$('#formProductoCreate .pCorreos').html(usuario.owner.proveedor.correos);
		$('#formProductoCreate .pTelefonos').html(telefonos.join(', '));
		$('#formProductoCreate .pDireccion').html(direcciones.join('<br>'));

		$('#pnlContacto').removeClass('hide');

		dismissModal(modal);
	});
}

function getModelos(e){
	var marca = $(e).val();
	var url = '{{ route('admin.producto.getModelos') }}';
	var data = {
		marca : marca
	};

	$('#formProductoCreate #modelo').val('');

	ajaxPost(url, data, function(data){
		if(data.data.length > 0){
			$('#formProductoCreate #modelo').autocomplete({
				source: data.data
			});
		}
	});
}

var cats = {!! json_encode($categorias) !!};
var campos = '';

function changeSubs(e){
	cat = $(e).val();

	$('#formProductoCreate #subcategoria').html('');

	$.each(cats, function(k,v){

		console.log(v.id);
		if(v.id === parseInt(cat, 10)){
			subs = v.subcategorias;
			$('#subcategoria').append($('<option>', {
				'value': '',
				'selected': 'selected',
			}).html('@lang("messages.select_subcategory")'));
			$.each(subs, function(k1, v1){
				$('#subcategoria').append($('<option>', {
					'value': v1.id
				}).html(v1.nombre));

				{{--}).html(v1.traduccion.{{ App::getLocale() }}));--}}
			});
		}
	});
}

function loadForm() {
	var idSubCat = $('#subcategoria').val();

	if( !(idSubCat > 0) ) {
		return false;
	}

	$('#formProductoCreate #formHolder').html('');

	var url = '{{ route('admin.producto.getForm', [ 'idCategoria' ]) }}';
	url = url.replace('idCategoria', idSubCat);
	$('#formHolder').load(url);

	// ----------

	var url = '{{ route('admin.producto.getCampos', ['idCategoria' ]) }}';
	url = url.replace('idCategoria', idSubCat);

	ajaxGet(url, function(data) {
		campos = data.data;
	});
}

// ---------- MULTISELECT JS SCRIPTS
var hashOptions = {};
function initMultiselect(){

	$('.mselect').on('change', function(e){

	});
}

function registerOption(e){


	var id = $(e).attr('id');

	if(hashOptions[id] === undefined){
		hashOptions[id] = [];
	}

	hashOptions[id].push({
		key : $('#'+id).val(),
		text : $('#'+id + ' option:selected').text()
	});

	updateOptions(id);

}

function removeOption(id, op){
	console.log("buscando: ");
	var opIndex = -1;
	$.each(hashOptions[id], function(k, v){
		if(hashOptions[id][k].key == op) {
			opIndex = k;
		}
	});

	if(opIndex >= 0){
		hashOptions[id].splice(opIndex, 1);
	}

	updateOptions(id);
}

function updateOptions(id){

	$('#'+id).val('');
	$('#'+id + ' option').removeClass('hide');

	// ----------

	$('#mEle_'+id).html('');
	var ul = $('<ul>');

	$.each(hashOptions[id], function(k, v){

		$('#'+id + " option[value='"+v.key+"']").addClass('hide');

		// ----------

		var a = $('<a>', {
			'class': 'pull-right bclose',
			href: 'javascript:;',
			onclick: 'removeOption("'+id+'", "'+v.key+'")'
		}).html('<i class="fa fa-trash-alt"></i>');

		var li = $('<li>');
		li
			.append(v.text)
			.append(a);
		ul.append(li);
	});

	$('#mEle_'+id).append(ul);
}

function getKeys(){
	var optSelected = {};

	$.each(hashOptions, function(k,v){
		if(optSelected[k] === undefined){
			optSelected[k] = [];
		}

		$.each(hashOptions[k], function(i, l){
			optSelected[k].push(l.key);
		})
	});

	return optSelected;
}

// ---------- TEST ROUTINES

//loadForm(19);
</script>
@endpush

