@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />

<style>
	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}

	/* SOLO PARA SHOW */
	.form-group .col-sm-8 {
		padding-top:7px;
	}
	.form-group label {
		padding-top:3px;
	}

</style>


<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}


	.valores td {
		text-align:center;
		color: #000000;
		font-weight: bold;
	}
	.labels td {
		text-align:center;
		background-color: #EDEDED;
		/*padding: 3px !important;*/
	}

	.attached-document>li .document-name {
		white-space: none;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.producto.index') }}">Productos</a></li>
		<li class="breadcrumb-item active">Registrar producto</li>
	</ol>

	<h1 class="page-header">Registrar producto</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Resumen informativo</h4>
		</div>
		<div class="panel panel-body">
			<div id="formProductoShow" class="form-horizontal">

				<table class="table table-condensed table-bordered">
					<tr class="labels active">
						<td class="labels">Marca</td>
						<td class="labels">Modelo</td>
						<td class="labels">Categoria</td>
						<td class="labels">SubCategoria</td>
						<td class="labels">Disponibilidad</td>
					</tr>
					<tr class="valores">
						<td class="valores">{{ $producto->Marca->nombre }}</td>
						<td class="valores">{{ $producto->Modelo->nombre }}</td>
						<td class="valores">{{ $producto->Categorias->first()->Padre->nombre }}</td>
						<td class="valores">{{ $producto->Categorias->first()->nombre }}</td>
						<td class="valores  @if( $producto->ProductoItem->idDisponibilidad == \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO) text-white bg-danger @endif">{{ $producto->ProductoItem->Disponibilidad->texto }}</td>
					</tr>
				</table>

				{{--<div class="row">--}}

					{{--<div class="form-group col-md-6 row m-b-0">--}}
						{{--<label class="col-form-label col-md-4">Marca</label>--}}
						{{--<div class="col-sm-8">--}}
							{{--{{ $producto->Marca->nombre }}--}}
						{{--</div>--}}
					{{--</div>--}}

					{{--<div class="form-group col-md-6 row m-b-0">--}}
						{{--<label class="col-form-label col-md-4">Modelo</label>--}}
						{{--<div class="col-sm-8">--}}
							{{--{{ $producto->Modelo->nombre }}--}}
						{{--</div>--}}
					{{--</div>--}}

					{{--<div class="form-group col-md-6 row m-b-0">--}}
						{{--<label class="col-form-label col-md-4">Categoria</label>--}}
						{{--<div class="col-sm-8">--}}
							{{--{{ $producto->Categorias->first()->Padre->nombre }}--}}
						{{--</div>--}}
					{{--</div>--}}

					{{--<div class="form-group col-md-6 row m-b-0">--}}
						{{--<label class="col-form-label col-md-4">Sub-categoria</label>--}}
						{{--<div class="col-sm-8">--}}
							{{--{{ $producto->Categorias->first()->nombre }}--}}
						{{--</div>--}}
					{{--</div>--}}

				{{--</div>--}}

				<h4>Ubicación</h4>

				<div class="row">
					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">País</label>
						<div class="col-sm-8">
							@if(isset($producto->ProductoItem->Ubicacion) && $producto->ProductoItem->Ubicacion->pais != '')
								{{ $global_paises[$producto->ProductoItem->Ubicacion->pais] }}
							@endif
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Ciudad</label>
						<div class="col-sm-8">
							@if(isset($producto->ProductoItem->Ubicacion) )
								{{ $producto->ProductoItem->Ubicacion->ciudad }}
							@endif
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Código postal</label>
						<div class="col-sm-8">
							@if(isset($producto->ProductoItem->Ubicacion) )
								{{ $producto->ProductoItem->Ubicacion->cpostal }}
							@endif
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Direccion</label>
						<div class="col-sm-8">
							@if(isset($producto->ProductoItem->Ubicacion) )
								{{ $producto->ProductoItem->Ubicacion->direccion }}
							@endif
						</div>
					</div>

				</div>

				<h4>Dimensiones</h4>

				<div class="row">

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Longitud (mm)</label>
						<div class="col-sm-8">
							{{ $producto->ProductoItem->dimension['lng'] }}

						</div>
					</div>

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Ancho (mm)</label>
						<div class="col-sm-8">
							{{ $producto->ProductoItem->dimension['wdt'] }}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Alto (mm)</label>
						<div class="col-sm-8">
							{{ $producto->ProductoItem->dimension['hgt'] }}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Peso bruto (kg)</label>
						<div class="col-sm-8">
							{{ $producto->ProductoItem->dimension['wgt'] }}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-0">
						<label class="col-form-label col-md-4">Paletizado</label>
						<div class="col-sm-8">
							{{--{!! Form::checkbox('paletizado', 1, ($producto->ProductoItem->dimension['pal'] == 1) ? true : false , [--}}
								{{--'id' => 'paletizado',--}}
								{{--'class' => 'form-control',--}}
								{{--'disabled' => 'disabled',--}}
							{{--]) !!}--}}

							{{ ( ($producto->ProductoItem->dimension['pal'] == 1) ? 'Si' : 'No' ) }}
						</div>
					</div>

				</div>

				<h4>Datos del cliente / propietario</h4>

				<table class="table table-condensed table-bordered tblProveedor">
					<tr class="active">
						<th style="width:50%">Datos Contacto</th>
						<th style="width:50%">Datos Empresa</th>
					</tr>
					<tr class="">
						<td>
							<address>
								<strong class="pContacto">{{ $producto->Owner->Owner->Agenda->nombres_apellidos }}</strong><br />
								<a href="javascript:;" class="text-black"><span class="pCorreoContacto">{{ $producto->Owner->username }}</span></a>
								<br>
								<strong>Telefonos:</strong> <span class="pTelefonos">
													@if($producto->Owner->Owner->Proveedor->Telefonos)
										{{ implode(', ', $producto->Owner->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) }}
									@endif
												</span>
							</address>
						</td>
						<td>
							<address>
								<strong class="pNombreProv">{{ ($producto->Owner->Owner->Proveedor->nombre == '') ? '- - - ' : $producto->Owner->Owner->Proveedor->nombre }}</strong><br />
								<span class="pDireccion">
													@if($producto->Owner->Owner->Proveedor->Direcciones)
										{!! implode('<br> ', $producto->Owner->Owner->Proveedor->Direcciones->pluck('direccion')->toArray()) !!}
									@endif
												</span>
								<br>
								<strong>Telefonos:</strong> <span class="pTelefonos">
													@if($producto->Owner->Owner->Proveedor->Telefonos)
										{{ implode(', ', $producto->Owner->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) }}
									@endif
												</span>
							</address>
							{{--<strong>Otros contactos:</strong><br>--}}
							{{--<span class="pContactos"></span>--}}
						</td>
					</tr>
				</table>

				<h4>Precios</h4>

				<table class="table table-condensed table-bordered">
					<tr class="labels active">
						<td class="labels">Precio Europa</td>
						<td class="labels">Precio Proveedor</td>
						<td class="labels">Precio Venta</td>
						<td class="labels">Solo Sudamérica</td>
					</tr>
					<tr class="valores">
						<td class="valores"><b>{{ $producto->precioEuropa }} {{ strtoupper($producto->monedaEuropa) }}</b></td>
						<td class="valores"><b>{{ $producto->precioProveedor }} {{ strtoupper($producto->monedaProveedor) }}</b></td>
						<td class="valores"><b>{{ $producto->precioUnitario }} {{ strtoupper($producto->monedaUnitario) }}</b></td>
						<td class="valores"><b>{{ ( ($producto->precioSudamerica == 1) ? 'Si' : 'No' ) }}</b></td>
					</tr>
				</table>

				<h4>Detalles de la maquina <small><a href="javascript:;" onclick="toggleCollapse('#formHolder')">Ver / Ocultar detalles</a></small></h4>
				<br>
				<div id="formHolder"` style="display:none;">
					<h5>Información primaria</h5>
					<div class="row">
						@foreach($primaryData as $data)
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-6 tblLabel">{!! $data['text'] !!}</div>
									<div class="col-md-6 tblValue">
										@if(in_array($data['type'], ['select', 'boolean']))
											@lang('form.'.$data['value'])
										@elseif(in_array($data['type'], ['multiselect']))
											<ul class="p-0">
												@foreach($data['value'] as $val)
													<li>
														@lang('form.'.$val)
													</li>
												@endforeach
											</ul>
										@elseif($data['type'] == 'range_year')
											{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
										@elseif($data['type'] == 'text')
											<a href="javascript:;" onclick="translateField('{{ $data['key'] }}')">{{ $data['value'] }}</a>
										@elseif($data['type'] == 'multiline')
											<a href="javascript:;" onclick="translateFieldML('{{ $data['key'] }}')">{{ $data['value'] }}</a>
										@elseif($data['type'] == 'number')
											{{ $data['value'] }}
										@else
											@lang('form.'.$data['value'])
										@endif
										{{--({{ $data['type'] }})--}}
									</div>
								</div>
							</div>
						@endforeach
					</div>

					<br>
					<h5>Información adicional</h5>
					@if(count($secondaryData) > 0)
						<div class="row">
							@foreach($secondaryData as $data)
								<div class="col-md-4">
									<div class="row">
										<div class="col-md-6 tblLabel">{!! $data['text'] !!} </div>
										<div class="col-md-6 tblValue">
											@if(in_array($data['type'], ['select', 'boolean']))
												@lang('form.'.$data['value'])
											@elseif(in_array($data['type'], ['multiselect']))
												<ul class="p-0">
													@foreach($data['value'] as $val)
														<li>
															@lang('form.'.$val)
														</li>
													@endforeach
												</ul>
											@elseif($data['type'] == 'range_year')
												{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
											@elseif($data['type'] == 'text')
												<a href="javascript:;" onclick="translateField('{{ $data['key'] }}')">{{ $data['value'] }}</a>
											@elseif($data['type'] == 'multiline')
												<a href="javascript:;" onclick="translateFieldML('{{ $data['key'] }}')">{{ $data['value'] }}</a>
											@elseif($data['type'] == 'number')
												{{ $data['value'] }}
											@else
												@lang('form.'.$data['value'])
											@endif
											{{--({{ $data['type'] }})--}}
										</div>
									</div>
								</div>
							@endforeach
						</div>
					@else
						<div class="row">
							<div class="col-md-12"><p>Sin información secundaria </p></div>
						</div>
					@endif
				</div>

				<div id="formHolder" class="/*hide*/"></div>

				<hr>

				<h4>Galería de imágenes</h4>
				<a href="javascript:;" onclick="show_thumbs()">Mostrar miniaturas</a><br>
				<a href="javascript:;" class="thumbs_info hide" onclick="images_move({{ $producto->id }})">Mover - </a><a href="javascript:;" class="thumbs_info hide" onclick="images_resize({{ $producto->id }})">Redimensionar</a>
				<div id="gallery" class="gallery">
					@foreach($producto->Galerias as $galeria)
						<ul class="attached-document clearfix">
							@foreach($galeria->Imagenes as $imagen)
								<li class="fa-camera">
									<div class="document-file">
										<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-{{ $producto->id }}">
											<img src="{{ $imagen->ruta_publica_producto_thumb }}" alt="" />
										</a>
									</div>
									<div class="document-name">
										<a href="javascript:;">{{ str_replace( '/', ' / ', $imagen->ruta_publica_producto ) }}</a>

										<div class="thumbs_info hide" style="margin-top:10px;">
											@if(isset($img_paths[$imagen->id]))
												@foreach($img_paths[$imagen->id] as $path)
													@if(preg_match('/(_res|thumb)/', $path) )
														<a href="{{ $imagen->ruta_publica_producto_thumb }}" target="_blank">{{ $path }}</a>
														<a href="javascript:;" onclick="image_remove('{{ pathinfo($imagen->ruta, PATHINFO_DIRNAME) }}/{{ $path }}')" class=""><i class="fa fa-trash"></i></a><br>
													@else
														{{ $path }}<br>
													@endif
												@endforeach
											@endif
										</div>

									</div>
								</li>
							@endforeach
						</ul>
					@endforeach
				</div>


				<hr class="hr-line-dashed">
				<div class="form-group row">
					<div class="col-md-6 text-left">
						<a class="btn btn-danger btn-sm" href="javascript:;" onclick="generarOrden({{ $producto->id }})" >Registrar Orden de compra</a>
					</div>
					<div class="col-md-6 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="productoCancelar()">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script>

//	$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
//		formInit();
//	});

			{{--@if($isAjaxRequest)--}}
			{{--formInit();--}}
			{{--@else--}}
			{{--$(function(){ formInit(); });--}}
			{{--@endif--}}

	var idUsuarioPropietario = 0;


	function productoCancelar(){
		@if($isAjaxRequest)
$(document).trigger('formProductoShow:cancelar');
		@else
redirect('{{ route('admin.producto.index')  }}');
		@endif
	}

	function formInit(){

		// ----------

	}

	function generarOrden(id) {

		var url = '{{ route('admin.orden.create', ['idProducto']) }}';
		url = url.replace('idProducto', id);
		var modal = openModal(url, 'Registrar Orden', null, { size: 'modal-lg' });
		setModalHandler('formOrdenCreate:success', function( event, data ){

			swal( 'Orden registrada exitosamente.', {
				title: 'Listo!',
				icon: "success",
				buttons: {
					goOrden: {
						text: 'Ir a la orden',
						value: 'goOrden',
						visible: true,
						className: 'btn btn-danger',
					},
					ok: {
						text: 'Cerrar',
						value: 'ok',
						visible: true,
						className: 'btn btn-primary',
					},
				}
			}).then(function(value) {

				switch (value) {
					case "goOrden":
						var url = '{{ route('admin.ordenInbox.show', ['idOrden']) }}';
						url = url.replace('idOrden', data.id);
						redirect(url);
						break;

					default:
						dismissModal(modal);
						datatable.ajax.reload();
				}
			});
		});
	}

	function images_move( idProducto ) {

		var url = '{{ route('admin.producto.images_move', ['idProducto']) }}';
		url = url.replace('idProducto', idProducto);

		var data = {};

		ajaxPost( url, data, function( response ) {
			console.log(response);
		})
	}

	function images_resize( idProducto ) {

		var url = '{{ route('admin.producto.images_resize', ['idProducto']) }}';
		url = url.replace('idProducto', idProducto);

		var data = {};

		ajaxPost( url, data, function( response ) {
			console.log(response);
		})
	}

	function image_remove(path) {
		console.log(path);
		var url = '{{ route('admin.producto.images_remove', [ $producto->id ]) }}';

		var data = {
			path:path
		};

		ajaxPost( url, data, function( response ) {
			console.log(response);
		})
	}

	function show_thumbs() {
		$('.thumbs_info').toggleClass('hide');
	}

	function translateField(key){
		var url = '{{ route('admin.producto.translation', [ $producto->id, 'campo' ]) }}';
		url = url.replace('campo', key);
		var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
		setModalHandler('formTranslation:aceptar', function () {
			dismissModal(modal);
		});
	}

	function translateFieldML(key){
		var url = '{{ route('admin.producto.translationML', [ $producto->id, 'campo' ]) }}';
		url = url.replace('campo', key);
		var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
		setModalHandler('formTranslation:aceptar', function () {
			dismissModal(modal);
		});
	}
</script>
@endpush

