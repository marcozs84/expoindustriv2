@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />

<style>
	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}
</style>


<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.producto.index') }}">Productos</a></li>
		<li class="breadcrumb-item active">Editar producto</li>
	</ol>

	<h1 class="page-header">Editar producto</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn producto_h_navigation" style="margin-top: -4px;">
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>--}}
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>--}}
				<a href="javascript:;" class="btn btn-warning btn-sm m-r-5 btn_anterior"><i class="fa fa-chevron-left"></i> Anterior</a>
				<a href="javascript:;" class="btn btn-danger btn-sm m-r-5" data-dismiss="modal" onclick="productoCancelar()"><i class="fa fa-times"></i> Cerrar</a>
				<a href="javascript:;" class="btn btn-primary btn-sm m-r-5" onclick="productoGuardar(this)" ><i class="fa fa-save"></i> Guardar</a>
				<a href="javascript:;" class="btn btn-warning btn-sm btn_siguiente">Siguiente <i class="fa fa-chevron-right"></i></a>
			</div>
			<h4 class="panel-title">Editar Producto - {{ ($producto->Marca) ? $producto->Marca->nombre : '' }} {{ ($producto->Modelo) ? $producto->Modelo->nombre : '' }} ( {{ $producto->id }} )</h4>
		</div>
		<div class="panel panel-body">

			{{--<div class="m-b-20 m-t-20">--}}
				{{--<a href="javascript:;" class="btn btn-warning btn-sm pull-right btn_siguiente">Siguiente <i class="fa fa-chevron-right"></i></a>--}}
				{{--<a href="javascript:;" class="btn btn-primary btn-sm pull-right m-r-5" onclick="productoGuardar(this)" >Guardar</a>--}}
				{{--<a href="javascript:;" class="btn btn-white btn-sm pull-right m-r-5" data-dismiss="modal" onclick="productoCancelar()">Cerrar</a>--}}
				{{--<a href="javascript:;" class="btn btn-warning btn-sm pull-right m-r-5 btn_anterior"><i class="fa fa-chevron-left"></i> Anterior</a>--}}
			{{--</div>--}}
			<div class="container">
				<h4>Galería de imágenes</h4>
				<a href="javascript:;" onclick="show_thumbs()">Mas detalles</a><br>
				<a href="javascript:;" class="thumbs_info hide" onclick="images_move({{ $producto->id }})">Mover - </a><a href="javascript:;" class="thumbs_info hide" onclick="images_resize({{ $producto->id }})">Redimensionar</a>
				<div id="gallery" class="gallery row">
					@foreach($producto->Galerias as $galeria)
						<ul class="attached-document clearfix">
							@foreach($galeria->Imagenes as $imagen)
								<li class="fa-camera" id="prod_img_{{ $imagen->id }}">
									<div class="document-file">
										<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-{{ $producto->id }}">
											<img src="{{ $imagen->ruta_publica_producto_thumb }}" alt="" />
										</a>
									</div>
									<div class="document-name">
										<a href="javascript:;">{{ str_replace( '/', ' / ', $imagen->ruta_publica_producto ) }}</a>
										<div class="thumbs_info hide" style="margin-top:10px;">
											@if(isset($img_paths[$imagen->id]))
												@foreach($img_paths[$imagen->id] as $path)
													@if(preg_match('/(_res|thumb)/', $path) )
														<a href="{{ $imagen->ruta_publica_producto_thumb }}" target="_blank">{{ $path }}</a><br>
													@else
														{{ $path }}<br>
													@endif
												@endforeach
											@endif
											{{--<a href="javascript:;" onclick="imagenImportar({{ $imagen->id }})" class="btn_import_{{ $imagen->id }}"><i class="fa fa-cloud-download-alt"></i></a>--}}
											<a href="javascript:;" onclick="imagenEliminar({{ $imagen->id }})" class="" title="{{ $imagen->id }}"><i class="fa fa-trash"></i></a>
										</div>
									</div>

								</li>
							@endforeach
						</ul>
					@endforeach
				</div>
			</div>

			<div id="formProductoEdit" class="form-horizontal">

				<div class="row">

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Marca</label>
						<div class="col-sm-8">
							{!! Form::text('marca', $producto->Marca->nombre, [
								'id' => 'marca',
								'placeholder' => '',
								'class' => 'form-control',
								'onblur' => 'getModelos(this)',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Modelo</label>
						<div class="col-sm-8">
							{!! Form::text('modelo', $producto->Modelo->nombre, [
								'id' => 'modelo',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Categoria</label>
						<div class="col-sm-8">
							{!! Form::text('categoria', $producto->Categorias->first()->Padre->nombre, [
							    'id' => 'categoria',
							    'class' => 'form-control',
							    'disabled' => 'disabled'
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Sub-categoria</label>
						<div class="col-sm-8">
							{!! Form::text('subcategoria', $producto->Categorias->first()->nombre, [
							    'id' => 'subcategoria',
							    'class' => 'form-control',
							    'disabled' => 'disabled'
							]) !!}
						</div>
					</div>

					@if( $producto->ProductoItem->idDisponibilidad != \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO)

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Disponibilidad</label>
						<div class="col-sm-8">
							{!! Form::select('disponibilidad', $disponibilidad, $producto->ProductoItem->idDisponibilidad, [
								'id' => 'disponibilidad',
								'class' => 'form-control pull-left',
								//'placeholder' => 'Disponibilidad',
							]); !!}
						</div>
					</div>

					@else

						<div class="form-group col-md-6 row m-b-15">
							<label class="col-form-label col-md-4 p-t-5">Disponibilidad</label>
							<div class="col-sm-8 p-t-5">
								{{ $producto->ProductoItem->Disponibilidad->texto }}
							</div>
						</div>

					@endif

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Tags</label>
						<div class="col-sm-8">
							{!! Form::select('tags', $grouped_tags, $prod_tags, [
								'id' => 'tags',
								'class' => 'form-control multiple-select2',
								'multiple' => 'multiple',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Estado</label>
						<div class="col-sm-8">
							{!! Form::select('esNuevo', [
								'' => 'No definido',
								1 => 'Nuevo',
								0 => 'Usado',
							], $producto->esNuevo, [
								'id' => 'esNuevo',
								'class' => 'form-control default-select2',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Tipo (maquinaria/accesorio)</label>
						<div class="col-sm-8">
							{!! Form::select('idProductoTipo', [
								'' => 'No definido',
								1 => 'Maquinaria',
								2 => 'Accesorio',
							], $producto->idProductoTipo, [
								'id' => 'idProductoTipo',
								'class' => 'form-control default-select2',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

				</div>

				<h4>Ubicación</h4>

				<div class="row">
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">País</label>
						<div class="col-sm-8">
							{!! Form::select('pais', $global_paises, $producto->ProductoItem->Ubicacion->pais, [
							    'id' => 'pais',
							    'class' => 'form-control',
							    'placeholder' => 'País',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Ciudad</label>
						<div class="col-sm-8">
							{!! Form::text('ciudad', $producto->ProductoItem->Ubicacion->ciudad, [
								'id' => 'ciudad',
								'placeholder' => 'Ciudad',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Código postal</label>
						<div class="col-sm-8">
							{!! Form::text('codigo_postal', $producto->ProductoItem->Ubicacion->cpostal, [
								'id' => 'codigo_postal',
								'placeholder' => 'Código postal',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Direccion</label>
						<div class="col-sm-8">
							{!! Form::textarea('direccion', $producto->ProductoItem->Ubicacion->direccion, [
							'id' => 'direccion',
							'class' => 'form-control',
							'rows' => 4,
							'placeholder' => 'Direccion',
						]) !!}
						</div>
					</div>

				</div>

				<h4>Dimensiones</h4>

				<div class="row">

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Longitud (mm)</label>
						<div class="col-sm-8">
							{!! Form::text('longitud', $producto->ProductoItem->dimension['lng'], [
								'id' => 'longitud',
								'placeholder' => 'Longitud (mm)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Ancho (mm)</label>
						<div class="col-sm-8">
							{!! Form::text('ancho', $producto->ProductoItem->dimension['wdt'], [
								'id' => 'ancho',
								'placeholder' => 'Ancho (mm)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Alto (mm)</label>
						<div class="col-sm-8">
							{!! Form::text('alto', $producto->ProductoItem->dimension['hgt'], [
								'id' => 'alto',
								'placeholder' => 'Alto (mm)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Peso bruto (kg)</label>
						<div class="col-sm-8">
							{!! Form::text('peso_bruto_fix', $producto->ProductoItem->dimension['wgt'], [
								'id' => 'peso_bruto_fix',
								'placeholder' => 'Peso bruto (kg)',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4 col-sm-4 col-sx-8">Paletizado</label>
						<div class="col-sm-8 col-xs-8">
							{!! Form::checkbox('paletizado', 1, $producto->ProductoItem->dimension['pal'], [
								'id' => 'paletizado',
								'class' => '/*form-control*/',
							]) !!}
						</div>
					</div>

				</div>

				<h4>Datos del cliente / propietario</h4>

				<div class="row">

					<div class="form-group col-md-6 row m-b-15 group-cliente">
						<label class="col-form-label col-md-4">Empresa / Cliente</label>
						<div class="col-md-8">
							{!! Form::select('searchCliente', [], null, [
								'id' => 'searchCliente',
								'class' => 'form-control default-select2',
								//'autofocus'
							]) !!}
							<a href="javascript:;" class="btn btn-primary" onclick="clienteCreate()" style="margin-top:5px;">Nuevo Cliente</a>
							<a href="javascript:;" class="btn btn-primary btnClienteEdit hide" onclick="clienteEdit()" style="margin-top:5px;">Editar Cliente</a>
						</div>
					</div>

					<div class="col-md-6 p-b-10" id="pnlContacto">
						<div class="panel panel-info">
							<div class="panel-body bg-black-transparent-2 text-black" style="padding-bottom:10px;">
								<table class="tblProveedor">
									<tr>
										<th style="width:50%">Datos Contacto</th>
										<th style="width:50%">Datos Empresa</th>
									</tr>
									<tr>
										<td>
											<address>
												<strong class="pContacto">{{ $producto->Owner->Owner->Agenda->nombres_apellidos }}</strong><br />
												<a href="javascript:;" class="text-black"><span class="pCorreoContacto">{{ $producto->Owner->username }}</span></a>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos">
													@if($producto->Owner->Owner->Proveedor->Telefonos)
														{{ implode(', ', $producto->Owner->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) }}
													@endif
												</span>
											</address>
										</td>
										<td>
											<address>
												<strong class="pNombreProv">{{ ($producto->Owner->Owner->Proveedor->nombre == '') ? '- - - ' : $producto->Owner->Owner->Proveedor->nombre }}</strong><br />
												<span class="pDireccion">
													@if($producto->Owner->Owner->Proveedor->Direcciones)
														{!! implode('<br> ', $producto->Owner->Owner->Proveedor->Direcciones->pluck('direccion')->toArray()) !!}
													@endif
												</span>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos">
													@if($producto->Owner->Owner->Proveedor->Telefonos)
														{{ implode(', ', $producto->Owner->Owner->Proveedor->Telefonos->pluck('numero')->toArray()) }}
													@endif
												</span>
											</address>
											{{--<strong>Otros contactos:</strong><br>--}}
											{{--<span class="pContactos"></span>--}}
										</td>
									</tr>
								</table>

							</div>
						</div>
					</div>

				</div>

				<h4>Precios</h4>


				@if( $producto->precioFijoUsd != '' || $producto->precioFijoEur != '' || $producto->precioFijoSek != '' )

					<table class="table table-condensed table-bordered">
						<tr class="labels active">
							<td class="labels" colspan="3">Precio fijado en el anuncio <small>(Precio visible en el anuncio al público)</small></td>
						</tr>
						<tr class="valores">
							<td class="valores text-center"><b>{{ $producto->precioFijoUsd }} USD</b></td>
							<td class="valores text-center"><b>{{ $producto->precioFijoEur }} EUR</b></td>
							<td class="valores text-center"><b>{{ $producto->precioFijoSek }} SEK</b></td>
						</tr>
					</table>

				@endif

				@can('admin.producto.editPrecio')

				<div class="row">

					{{--<div class="form-group col-md-12 row m-b-15">--}}
						{{--<label class="col-form-label col-md-4">Precio venta</label>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::text('precioVenta', $producto->precioUnitario, [--}}
								{{--'id' => 'precioVenta',--}}
								{{--'placeholder' => '',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::select('currencyVenta', $moneda, $producto->monedaUnitario, [--}}
								{{--'id' => 'currencyVenta',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
					{{--</div>--}}

					<div class="form-group col-md-12 row m-b-15">
						<label class="col-form-label col-md-4">Precio proveedor</label>
						<div class="col-sm-4">
							{!! Form::text('precioProveedor', $producto->precioProveedor, [
								'id' => 'precioProveedor',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
						<div class="col-sm-4">
							{!! Form::select('currencyProveedor', $moneda, $producto->monedaProveedor, [
								'id' => 'currencyProveedor',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					{{--<div class="form-group col-md-12 row m-b-15">--}}
						{{--<label class="col-form-label col-md-4">Precio minimo</label>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::text('precioMinimo', $producto->precioMinimo, [--}}
								{{--'id' => 'precioMinimo',--}}
								{{--'placeholder' => '',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
						{{--<div class="col-sm-4">--}}
							{{--{!! Form::select('currencyMinimo', $moneda, $producto->monedaMinimo, [--}}
								{{--'id' => 'currencyMinimo',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
					{{--</div>--}}

					<div class="form-group col-md-12 row m-b-15">
						<label class="col-form-label col-md-4">Precio Europa</label>
						<div class="col-sm-4">
							{!! Form::text('precioEuropa', $producto->precioEuropa, [
								'id' => 'precioEuropa',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
						<div class="col-sm-4">
							{!! Form::select('currencyEuropa', $moneda, $producto->monedaEuropa, [
								'id' => 'currencyEuropa',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Solo Sudamérica</label>
						<div class="col-sm-8">
							{!! Form::checkbox('priceSouthamerica', 1, $producto->precioSudamerica, [
								'id' => 'priceSouthamerica',
								'class' => '/*form-control*/',
								'style' => 'height:inherig',
							]) !!}
						</div>
					</div>


					{{--<div class="form-group col-md-6 row m-b-15">--}}
						{{--<label class="col-form-label col-md-4">Fecha de producto</label>--}}
						{{--<div class="col-sm-8">--}}
							{{--{!! Form::text('fechaProducto', '', [--}}
								{{--'id' => 'fechaProducto',--}}
								{{--'placeholder' => '',--}}
								{{--'class' => 'form-control',--}}
							{{--]) !!}--}}
						{{--</div>--}}
					{{--</div>--}}
				</div>

				@else

					<table class="table table-condensed table-bordered">
						<tr class="labels active">
							<td class="labels text-center">Precio Venta</td>
							<td class="labels text-center">Precio Proveedor</td>
							<td class="labels text-center">Precio Minimo</td>
							<td class="labels text-center">Solo Sudamérica</td>
						</tr>
						<tr class="valores">
							<td class="valores text-center"><b>@if($producto->precioUnitario != '') {{ $producto->precioUnitario }} {{ strtoupper($producto->monedaUnitario) }} @endif</b></td>
							<td class="valores text-center"><b>{{ $producto->precioProveedor }} {{ strtoupper($producto->monedaProveedor) }}</b></td>
							<td class="valores text-center"><b>{{ $producto->precioMinimo }} {{ strtoupper($producto->monedaMinimo) }}</b></td>
							<td class="valores text-center"><b>{{ ( ($producto->precioSudamerica == 1) ? 'Si' : 'No' ) }}</b></td>
						</tr>
					</table>

				@endcan

				<h4>Detalles de la maquina <small><a href="javascript:;" onclick="toggleCollapse('#formHolder')">Ver / Ocultar detalles</a></small></h4>

				<div id="formHolder" class="/*hide*/" style="margin-left:10px;"></div>

				<hr>

				{{-- GALERIA DE IMAGENES VA AQUÍ --}}

				<div class="form-group row m-b-15">
					{{--<label class="col-form-label col-md-3">Fotografías / Captura</label>--}}
					<div class="col-md-12">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
						{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
						{{--<div class="form-group">--}}
						{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
						{{--</div>--}}
						{{--<div class="form-group">--}}
						{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
						{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.bannerSuperior.upload') }}" class="dropzone" id="dz_producto_edit_imagen">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="idProducto" id="idProducto" value="{{ $producto->id }}">
							<div class="dz-message" data-dz-message><span>
									{{--@lang('messages.dz_upload')--}}
									Adjuntar producto
								</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor no cierre la ventana hasta que se suban todos los archivos.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>


				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-md-6 text-left">
						{{--<a class="btn btn-warning btn-sm" href="javascript:;" onclick="usuarioCreate()">Nuevo Cliente</a>--}}
					</div>
					<div class="col-md-6 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="productoCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="productoGuardar(this)" >Guardar</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="hide">
		<ul>
			<li class="fa-camera" id="stub_li_imagen">
				<div class="document-file">
					<a href="#" data-lightbox="gallery-group-prodid">
						<img src="" alt="" />
					</a>
				</div>
				<div class="document-name">
					<a href="javascript:;"></a>
					<br>
					<a href="javascript:;" class="linkdelete" onclick="imagenEliminar()" style="margin: auto; text-align:center;">Eliminar</a>
				</div>
			</li>
		</ul>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script>

// Moved to Index, jquery-ui must be loaded before bootstrap
//$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
//	formInit();
//});

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

var myDropzone = '';
$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
	Dropzone.autoDiscover = false;
	setTimeout(function(){
		{{--$('div#dropzone').dropzone({--}}
		{{--url: '{{ route('admin.bannerSuperior.upload') }}'--}}
		{{--});--}}  // comented so it fixes the already attached dropzone

		myDropzone = new Dropzone("#dz_producto_edit_imagen", {
			url: "{{ route('admin.producto.upload') }}",
			addRemoveLinks: true,
			autoProcessQueue: true,
			parallelUploads: 1,
			init: function() {
				{{--@foreach($images as $image)--}}
				{{--var mockFile = {--}}
				{{--name: '{{ $image['filename'] }}',--}}
				{{--size: 123,--}}
				{{--};--}}
				{{--this.options.addedfile.call(this, mockFile);--}}
				{{--this.options.thumbnail.call(this, mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
				{{--this.emit('thumbnail', mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
				{{--this.emit('complete', mockFile);--}}
				{{--// Src.: https://stackoverflow.com/a/22719947;--}}
				{{--@endforeach--}}

				this.on("queuecomplete", function(file) {
//					$(document).trigger('formProductoEdit:aceptar');
				});
				this.on("removedfile", function(file) {
					//alert(file.name);
					console.log('Eliminado: ' + file.name);

					file.previewElement.remove();

					// Create the remove button
					var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

					//Capture the Dropzone instance as closure.
					var _this = this;

					// Listen to the click event
					//					removeButton.addEventListener();

					// Add the button to the file preview element.
					file.previewElement.appendChild(removeButton);
				});
			},
			success: function(file, response){
				console.log("success");
				console.log(file.name);
				console.log(file);
				console.log(response);

				$(file.previewElement).remove();

				if(response.result === true) {

					imagen = response.data;

					var li = $('#stub_li_imagen').clone();
					li.attr('id', 'prod_img_'+imagen.id);
					li.find('.document-file > a').attr('href', imagen.ruta_publica_producto);
					li.find('.document-file > a').attr('data-lightbox', 'gallery-group-{{ $producto->id }}');
					li.find('.document-file > a > img').prop('src', imagen.ruta_publica_producto);
					li.find('.document-name > a').html(imagen.filename);
					li.find('.document-name a.linkdelete').attr('onclick', 'imagenEliminar('+ imagen.id +')');
					li.find('.document-name a.linkdelete').html('Eliminar');

					$('.attached-document').append(li);
				}

				myDropzone.processQueue();
//					$(document).trigger('formProductoEdit:aceptar');
			},
			removedfile: function(file){
				x = confirm('@lang('messages.confirm_removal')');
				if(!x) return false;
//					removeImage(1, file.name);
//					for(var i = 0 ; i < file_up_names.length; i++){
//						if(file_up_names[i] === file.name){
//							ajaxPost('public.publishRemove', {
//								'archivo' : ''
//							});
//						}
//					}
			}
		});
	}, 1000);

	$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));

});

$( "#formProductoEdit" ).submit(function( event ) {
	productoGuardar();
	event.preventDefault();
});

var idUsuarioPropietario = {{ $producto->Owner->id }}; // idUsuario
var campos = {!! json_encode($campos) !!};
var availableTags = {!! json_encode($marcas) !!};

function formInit(){

	$("[data-toggle=tooltip]").tooltip();

	// ---------- ANTERIOR Y SIGUIENTE

	if( typeof producto_list !== 'undefined' && producto_list.length > 0 ) {

		$('.producto_h_navigation').removeClass('hide');

		if( producto_list_index > 0 ) {
			$('.btn_anterior').removeClass('hide');
		} else {
			$('.btn_anterior').addClass('hide');
		}
		if( producto_list_index < producto_list.length-1 ) {
			$('.btn_siguiente').removeClass('hide');
		} else {
			$('.btn_siguiente').addClass('hide');
		}

		$('.btn_siguiente').attr('onclick', 'reloadOn( ' + ( producto_list_index + 1) + ' )');
		$('.btn_anterior').attr('onclick', 'reloadOn( ' + ( producto_list_index - 1) + ' )');
	} else {
		$('.producto_h_navigation').addClass('hide');
	}
	// ----------

	$('#formProductoEdit #marca').autocomplete({
		source: availableTags
	});

	@if( $producto->ProductoItem->idDisponibilidad != \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO)

	$('#formProductoEdit #disponibilidad').select2().on('select2:select', function (e) {
		var data = e.params.data;
		console.log(data);

		if ( parseInt(data.id, 10) === {{ \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO }}) {
			swal({
				text: 'Desea registrar una orden?',
				icon: 'warning',
				buttons: {
					cancel: 'No',
					ok: 'Si'
				},
				dangerMode: true
			}).then(function(response) {
				if (response === 'ok') {
					registrarOrden({{ $producto->id }});
				} else {
					$('#formProductoEdit #disponibilidad').val({{ $producto->ProductoItem->idDisponibilidad }});
					$('#formProductoEdit #disponibilidad').trigger('change');
				}
			});

			$('#formProductoEdit #disponibilidad').val({{ $producto->ProductoItem->idDisponibilidad }});
			$('#formProductoEdit #disponibilidad').trigger('change');
		}
	});

	@endif


	$('#formProductoEdit #pais').select2();
	$('#formProductoEdit #tags').select2();
	$('#formProductoEdit #tags').on("select2:unselecting", function(e) {
		$(this).data('state', 'unselected');

		var data = e.params.args.data;
		var index = tagsSelected.indexOf( data.id );
		if (index > -1) {
			tagsSelected.splice(index, 1);
			$('#tagchk_' + data.id).prop('checked', false);
		}
	}).on("select2:opening", function(e) {
		var $self = $(this);
		if ($(this).data('state') === 'unselected') {
			$(this).removeData('state');
		} else {
			modalTags();
		}
		e.preventDefault();
		$self.select2('close');
	});

	// ----------

	$('#formProductoEdit #searchCliente').select2({
//        allowClear: true,
		placeholder: {
			id: "",
			placeholder: "Leave blank to ..."
		},
		ajax: {
			{{--				url: '{{ route('resource.proveedorContacto.ds') }}',--}}
			url: '{{ route('resource.cliente.ds') }}',
			dataType: 'json',
			method: 'POST',
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			delay: 250,
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page,
					proveedor_data: 1 // Utilizado para que el 'ds' consultado retorne informacion más completa
				};
			},
			statusCode: {
				422: ajaxValidationHandler,
				500: ajaxErrorHandler
			},
			processResults: function (data, params) {
				// parse the results into the format expected by Select2
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data, except to indicate that infinite
				// scrolling can be used
				params.page = data.current_page || 1;

				return {
					results: data.data,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: function(repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if (repo.loading) return repo.text;

			var companyName = '';
			if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
				companyName = '- - -';
			} else {
				companyName = repo.usuario.owner.proveedor.nombre;
			}
			var otrosContactos = [];

			var markup = "<div>" + repo.agenda.nombres_apellidos + '<br><b>' + companyName + "</b><br>" + otrosContactos.join('<br> ') + "</div>";
			return markup;
		},
		templateSelection: function formatRepoSelection (repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if(repo.usuario){

				idUsuarioPropietario = repo.idUsuario;

				var companyName = '';
				if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
					companyName = '- - -';
				} else {
					companyName = repo.usuario.owner.proveedor.nombre;
				}

				$('#formProductoEdit #nuevoCliente option[value="0"]').attr('selected', 'selected');

				// ----------

				$('#formProductoEdit .pNombreProv').html(companyName);

				var otrosContactos = [];
				if(repo.usuario.owner.proveedor.proveedor_contactos && repo.usuario.owner.proveedor.proveedor_contactos.length > 0) {
					$.each(repo.usuario.owner.proveedor.proveedor_contactos, function(k,v) {
						if(parseInt(v.id, 10) !== parseInt(repo.id, 10)) {
							otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
						}
					});
				}

				var telefonos = [];
				if(repo.usuario.owner.proveedor.telefonos && repo.usuario.owner.proveedor.telefonos.length > 0) {
					$.each(repo.usuario.owner.proveedor.telefonos, function(k,v) {
						if(v.id !== repo.id) {
							telefonos.push(v.numero);
						}
					});
				}

				var direcciones = [];
				if(repo.usuario.owner.proveedor.direcciones && repo.usuario.owner.proveedor.direcciones.length > 0) {
					$.each(repo.usuario.owner.proveedor.direcciones, function(k,v) {
						if(v.id !== repo.id) {
							direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
						}
					});
				}

				$('#formProductoEdit .pContacto').html(repo.agenda.nombres_apellidos);
				$('#formProductoEdit .pCorreoContacto').html(repo.usuario.username);
				$('#formProductoEdit .pContactos').html(otrosContactos.join('<br> '));
				$('#formProductoEdit .pCorreos').html(repo.usuario.owner.proveedor.correos);
				$('#formProductoEdit .pTelefonos').html(telefonos.join(', '));
				$('#formProductoEdit .pDireccion').html(direcciones.join('<br>'));

				$('#pnlContacto').removeClass('hide');

				// ----------

				return repo.agenda.nombres_apellidos + ' [' + companyName + ']';
			} else {
				idUsuarioPropietario = {{ $producto->Owner->id }};
//				$('#formProductoEdit .btnProveedorEdit').addClass('hide');
				return repo.text;
			}
			//return repo.nombre + ' ' + repo.apellidos || repo.text;
		},
//	    allowClear: true
	})
		.trigger('change');
}

function productoGuardar(elem){
	$(elem).html('Guardando...');
	var url = '{{ route('admin.producto.update', [ $producto->id ]) }}';

	var form = $('#formProductoEdit');

	var data = {
		marca: form.find('#marca').val(),
		modelo: form.find('#modelo').val(),
		@if( $producto->ProductoItem->idDisponibilidad != \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO)
		idDisponibilidad: $('#disponibilidad').val(),
		@else
		idDisponibilidad: {{ $producto->ProductoItem->idDisponibilidad }},
		@endif

//		idCategoria: form.find('#subcategoria').val(),
//
		pais:           form.find('#pais').val(),
		ciudad:         form.find('#ciudad').val(),
		codigo_postal:  form.find('#codigo_postal').val(),
		direccion:      form.find('#direccion').val(),
//
		longitud:       form.find('#longitud').val(),
		ancho:          form.find('#ancho').val(),
		alto:           form.find('#alto').val(),
		peso_bruto_fix: form.find('#peso_bruto_fix').val(),
		paletizado: ( form.find('#paletizado').prop('checked') ) ? 1 : 0 ,
//
		idUsuario: idUsuarioPropietario, //$('#searchCliente').val(),
		@can('admin.producto.editPrecio')
		precioProveedor:    form.find('#precioProveedor').val(),
		currencyProveedor:  form.find('#currencyProveedor').val(),
//		precioVenta:        form.find('#precioVenta').val(),
//		currencyVenta:      form.find('#currencyVenta').val(),
//		precioMinimo:       form.find('#precioMinimo').val(),
//		currencyMinimo:     form.find('#currencyMinimo').val(),
		precioEuropa:       form.find('#precioEuropa').val(),
		currencyEuropa:     form.find('#currencyEuropa').val(),
		priceSouthamerica: (form.find('#priceSouthamerica').prop('checked')) ? 1 : 0,
		@endcan

		tags:               form.find('#tags').val(),
		esNuevo:            form.find('#esNuevo').val(),
		idProductoTipo:     form.find('#idProductoTipo').val(),
//
		multiselects: getKeys()
	};

	$.each(campos, function(k,v) {
//		console.log(v.key);
		data[v.key] = $('#'+v.key).val();
	});

	console.log(data);

	ajaxPatch(url, data, function(data){

//		$('#idProducto').val(data.data.id);

		if(myDropzone.files.length === 0) {
			$(document).trigger('formProductoEdit:aceptar');
		} else {
			myDropzone.processQueue();
		}
	}, null, {
		onDone: function() {
			$(elem).html('Guardar');
		}
	});
}

function productoCancelar(){
	@if($isAjaxRequest)
		$(document).trigger('formProductoEdit:cancelar');
	@else
		redirect('{{ route('admin.producto.index')  }}');
	@endif
}

function clienteCreate(){
	var url = '{{ route('admin.usuario.create', [ 'tipo' => 'cliente' ]) }}';
	var modal = openModal(url, 'Nuevo Cliente');
	setModalHandler('formProductoEdit:aceptar', function(event, data){
		var usuario = data;
		if(usuario.owner.proveedor.nombre === null) {
			usuario.owner.proveedor.nombre = '';
		}
		var companyName = '';
		if(usuario.owner.proveedor.nombre === '' || usuario.owner.proveedor.nombre === null) {
			companyName = '- - -';
		} else {
			companyName = usuario.owner.proveedor.nombre;
		}
		var option = $('<option>', {
			value: usuario.cliente.id,
			'selected' : 'selected'
		}).html(usuario.owner.agenda.nombres_apellidos + ' [' + companyName + ']');
		$('#formProductoEdit #searchCliente').html('');
		$('#formProductoEdit #searchCliente').append(option).trigger('change');

		$('#formProductoEdit #nuevoCliente option[value="1"]').attr('selected', 'selected');

		$('#formProductoEdit .pNombreProv').html(companyName);

		var otrosContactos = [];
		if(usuario.owner.proveedor.proveedor_contactos && usuario.owner.proveedor.proveedor_contactos.length > 0) {
			$.each(usuario.owner.proveedor.proveedor_contactos, function(k,v) {
				if(v.id !== usuario.id) {
					otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
				}
			});
		}

		var telefonos = [];
		if(usuario.owner.proveedor.telefonos && usuario.owner.proveedor.telefonos.length > 0) {
			$.each(usuario.owner.proveedor.telefonos, function(k,v) {
				if(v.id !== usuario.id) {
					telefonos.push(v.numero);
				}
			});
		}

		var direcciones = [];
		if(usuario.owner.proveedor.direcciones && usuario.owner.proveedor.direcciones.length > 0) {
			$.each(usuario.owner.proveedor.direcciones, function(k,v) {
				if(v.id !== usuario.id) {
					direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
				}
			});
		}

		$('#formProductoEdit .pContacto').html(usuario.owner.agenda.nombres_apellidos);
		$('#formProductoEdit .pCorreoContacto').html(usuario.username);
		$('#formProductoEdit .pContactos').html(otrosContactos.join('<br> '));
		$('#formProductoEdit .pCorreos').html(usuario.owner.proveedor.correos);
		$('#formProductoEdit .pTelefonos').html(telefonos.join(', '));
		$('#formProductoEdit .pDireccion').html(direcciones.join('<br>'));

		$('#pnlContacto').removeClass('hide');

		dismissModal(modal);
	});
}

function getModelos(e){
	var marca = $(e).val();
	var url = '{{ route('admin.producto.getModelos') }}';
	var data = {
		marca : marca
	};

	$('#formProductoEdit #modelo').val('');

	ajaxPost(url, data, function(data){
		if(data.data.length > 0){
			$('#formProductoEdit #modelo').autocomplete({
				source: data.data
			});
		}
	});
}

var cats = {!! json_encode($categorias) !!};

function changeSubs(e){
	cat = $(e).val();

	$('#subcategoria').html('');

	$.each(cats, function(k,v){

		console.log(v.id);
		if(v.id === parseInt(cat, 10)){
			subs = v.subcategorias;
			$('#subcategoria').append($('<option>', {
				'value': '',
				'selected': 'selected',
			}).html('@lang("messages.select_subcategory")'));
			$.each(subs, function(k1, v1){
				$('#subcategoria').append($('<option>', {
					'value': v1.id
				}).html(v1.nombre));

				{{--}).html(v1.traduccion.{{ App::getLocale() }}));--}}
			});
		}
	});
}

function loadForm() {
	var idSubCat = {{ $producto->Categorias->first()->id }}; //$('#subcategoria').val();

	if( !(idSubCat > 0) ) {
		return false;
	}

	$('#formHolder').html('');

	var url = '{{ route('admin.producto.getForm', [ 'idCategoria', $producto->id ]) }}';
	url = url.replace('idCategoria', idSubCat);
	$('#formHolder').load(url);

	// ----------

	var url = '{{ route('admin.producto.getCampos', ['idCategoria' ]) }}';
	url = url.replace('idCategoria', idSubCat);

	ajaxGet(url, function(data) {
		campos = data.data;
	}, null, { silent:1 });
}

function imagenEliminar(id) {

	swal({
		text: 'Está seguro que desea eliminar esta imágen de la galería?',
		icon: 'warning',
		buttons: {
			cancel: 'No',
			ok: 'Si'
		},
		dangerMode: true
	}).then(function(response) {
		if (response === 'ok') {
			var url = '{{ route('resource.imagen.destroy', [0]) }}';
			var ids = [id];
			ajaxDelete(url, ids, function(){
				swal('Imágen eliminada', { icon: "success" });
				$('#prod_img_'+id).remove();
			});
		}
	});

}

function images_move( idProducto ) {

	var url = '{{ route('admin.producto.images_move', ['idProducto']) }}';
	url = url.replace('idProducto', idProducto);

	var data = {};

	ajaxPost( url, data, function( response ) {
		console.log(response);
	})
}

function images_resize( idProducto ) {

	var url = '{{ route('admin.producto.images_resize', ['idProducto']) }}';
	url = url.replace('idProducto', idProducto);

	var data = {};

	ajaxPost( url, data, function( response ) {
		console.log(response);
	})
}
// ---------- MULTISELECT JS SCRIPTS
var hashOptions = {};
function initMultiselect(){

	$('.mselect').on('change', function(e){

	});
}

function registerOption(e){


	var id = $(e).attr('id');

	if(hashOptions[id] === undefined){
		hashOptions[id] = [];
	}

	hashOptions[id].push({
		key : $('#'+id).val(),
		text : $('#'+id + ' option:selected').text()
	});

	updateOptions(id);

}

function removeOption(id, op){
	console.log("buscando: ");
	var opIndex = -1;
	$.each(hashOptions[id], function(k, v){
		if(hashOptions[id][k].key == op) {
			opIndex = k;
		}
	});

	if(opIndex >= 0){
		hashOptions[id].splice(opIndex, 1);
	}

	updateOptions(id);
}

function updateOptions(id){

	$('#'+id).val('');
	$('#'+id + ' option').removeClass('hide');

	// ----------

	$('#mEle_'+id).html('');
	var ul = $('<ul>');

	$('#'+id + " option").removeClass('hide');

	$.each(hashOptions[id], function(k, v){

		$('#'+id + " option[value='"+v.key+"']").addClass('hide');

		// ----------

		var a = $('<a>', {
			'class': 'pull-right bclose',
			href: 'javascript:;',
			onclick: 'removeOption("'+id+'", "'+v.key+'")'
		}).html('<i class="fa fa-trash-alt"></i>');

		var li = $('<li>');
		li
			.append(v.text)
			.append(a);
		ul.append(li);
	});

	$('#mEle_'+id).append(ul);
}

function getKeys(){
	var optSelected = {};

	$.each(hashOptions, function(k,v){
		if(optSelected[k] === undefined){
			optSelected[k] = [];
		}

		$.each(hashOptions[k], function(i, l){
			optSelected[k].push(l.key);
		})
	});

	return optSelected;
}

loadForm();


function registrarOrden(id) {
	var url = '{{ route('admin.orden.create', ['idProducto']) }}';
	url = url.replace('idProducto', id);
	var modal = openModal(url, 'Registrar Orden', null, { size: 'modal-lg' });
	setModalHandler('formOrdenCreate:success', function( event, data ){

		$('#formProductoEdit #disponibilidad').val({{ \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO }});
		$('#formProductoEdit #disponibilidad').trigger('change');

		swal( 'Orden registrada exitosamente.', {
			title: 'Listo!',
			icon: "success",
			buttons: {
				goOrden: {
					text: 'Ir a la orden',
					value: 'goOrden',
					visible: true,
					className: 'btn btn-danger',
				},
				ok: {
					text: 'Cerrar',
					value: 'ok',
					visible: true,
					className: 'btn btn-primary',
				},
			}
		}).then(function(value) {

			switch (value) {
				case "goOrden":
					var url = '{{ route('admin.ordenInbox.show', ['idOrden']) }}';
					url = url.replace('idOrden', data.id);
					redirect(url);
					break;

				default:
					dismissModal(modal);
					datatable.ajax.reload();
			}
		});
	});
}

function translateField(key){
	var url = '{{ route('admin.producto.translation', [ $producto->id, 'campo' ]) }}';
	url = url.replace('campo', key);
	var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
	setModalHandler('formTranslation:aceptar', function () {
		dismissModal(modal);
	});
}

function translateFieldML(key){
	var url = '{{ route('admin.producto.translationML', [ $producto->id, 'campo' ]) }}';
	url = url.replace('campo', key);
	var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
	setModalHandler('formTranslation:aceptar', function () {
		dismissModal(modal);
	});
}
// ---------- TEST ROUTINES

function show_thumbs() {
	$('.thumbs_info').toggleClass('hide');
}

function modalTags(){
	$('#formProductoEdit #tags').select2('close');
//	$('#modalSearchTags').modal();

	var url = '{{ route('admin.tag.selector', [ $producto->id ]) }}';
	var modal = openModal(url, 'Tags', null, {
		'size': 'modal-80'
	});
	setModalHandler('formTagSelector:aceptar', function( event, response){
		dismissModal(modal);
		tagsSelected = response.selectedtags;

		$('#formProductoEdit #tags').val( tagsSelected ).trigger('change');
	});

}

var tagsSelected = [];
function addTag(e) {
	tagsSelected = [];
	$('.tag_selector').each( function(k,v) {
		if( $(v).prop('checked') === true ) {
			tagsSelected.push( $(v).val() );
		}
	});
}

function reloadOn( index ) {
	producto_list_index = index;
	idProducto = producto_list[ producto_list_index ];
	var modal = $('.btn_siguiente').closest('.modal');
	var url = '{{ route('admin.producto.edit', ['idProducto']) }}';
	url = url.replace('idProducto', idProducto);
	reloadModal(modal, url);
}

//loadForm(19);
</script>
@endpush

