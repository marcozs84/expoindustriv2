@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li><a href="{{ route('admin.producto.index') }}">Productos</a></li>
		<li class="active">Nuevo Producto Item</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Nuevo Producto Item</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo ProductoItem</h4>
		</div>
		<div class="panel-body">
			<form id="formProductoItemCreate" >

                <div class="form-group row m-b-10">
	                <label class="col-form-label col-md-3">Nombre</label>
                    <div class="col-md-9">
	                    {!! Form::text('nombre', null, [
                            'id' => 'nombre',
                            'placeholder' => 'Nombre',
                            'class' => 'form-control form-control-sm m-b-5'
                        ]) !!}
                    </div>
                </div>
                <div class="form-group row m-b-10">
	                <label class="col-form-label col-md-3">Descripcion</label>
                    <div class="col-md-9">
	                    {!! Form::textarea('descripcionDetalle', null, [
                            'id' => 'descripcionDetalle',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control m-b-5',
                            'rows' => 3,
                        ]) !!}
                    </div>
                </div>
				<div class="form-group row m-b-10">
					<label class="col-form-label col-md-3">SKU</label>
					<div class="col-md-9">
						{!! Form::text('sku', null, [
							'id' => 'sku',
							'placeholder' => 'SKU',
							'class' => 'form-control form-control-sm m-b-5'
						]) !!}
					</div>
				</div>
                <div class="hr-line-dashed"></div>
                <div class="form-group row m-b-15">
                    <div class="col-lg-12 text-right">
                        <a class="btn btn-white" data-dismiss="modal" href="javascript:;">Cancelar</a>
                        <a class="btn btn-primary" href="javascript:;" onclick="guardarProductoItem()" >Guardar</a>
                    </div>
                </div>
            </form>
		</div>
	</div>

@stop

@push('scripts')
<script>
$(function(){
	initView();
});

function initView(){
//	$(".default-select2").select2();
}

function guardarProductoItem(){

	console.log("guardando");

{{--	var url = '{{ route('admin.producto.store_item', [ $producto->id ]) }}';--}}
	var url = '{{ route( 'resource.productoItem.store' ) }}';
	var form = $('#formProductoItemCreate');
	var data = {
		idProducto            : {{ $producto->id }},
		idLote                : 0,
		idDisponibilidad      : 1,
		idTipoImportacion     : 1,
		nombre                : form.find('#nombre').val(),
		descripcionPrimaria   : '', //form.find('#descripcionPrimaria'),
		descripcionDetalle    : form.find('#descripcionDetalle').val(),
		ean                   : '',
		sku                   : form.find('#sku').val(),
		precioDistribuidor    : 0, //form.find('#precioDistribuidor'),
		precioVenta           : 0, //form.find('#precioVenta'),
		monedaVenta           : '', //form.find('#monedaVenta'),
		precioPromocion       : 0, //form.find('#precioPromocion'),
		color                 : '', //form.find('#color'),
		dimension             : '', //form.find('#dimension'),
		fechaElaboracion      : '', //form.find('#fechaElaboracion'),
		fechaVencimiento      : '', //form.find('#fechaVencimiento'),
		definicion            : '', //form.find('#definicion'),
		permalink             : '', //form.find('#permalink'),
		make_item_trans       : 1
    };
	ajaxPost(url, data, function(data){
		@if($isAjaxRequest)
			$(document).trigger('formProductoItemCreate:success');
		@else
			redirect('{{ route('admin.producto.index') }}');
		@endif
	});

}

</script>
@endpush

