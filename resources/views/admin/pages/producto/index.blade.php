@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')

<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="/home">Home</a></li>
		<li class="breadcrumb-item active">Productos</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Productos <small></small></h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Búsqueda</h4>
		</div>
		<div id="formSearchGasto" class="panel-body p-b-0">
			<div class="row" id="searchForm">

				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="idUsuario" style="">Disponibilidad</label>
					{!! Form::select('idDisponibilidad', $disponibilidad, [
						\App\Models\ProductoItem::DISPONIBILIDAD_DISPONIBLE,
						\App\Models\ProductoItem::DISPONIBILIDAD_RESERVADO
					], [
						'id' => 'idDisponibilidad',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="productoCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="productoDelete()"><i class="fa fa-trash"></i> Eliminar</a>
					<a class="btn btn-warning btn-sm" href="javascript:;" onclick="productoAsignarTareas()"><i class="fa fa-trash"></i> Asignar tareas</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:;" onclick="buscar()"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:;" onclick="limpiar()">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">

			<div class="btn-group pull-right m-l-10">
				<button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> </button>
				<ul class="dropdown-menu dropdown-menu-right " role="menu">
					<li><a href="javascript:;" onclick="mostrarSeleccionados()">Mostrar elementos seleccionados</a></li>
					<li class="divider"></li>
					<li><a href="javascript:;" onclick="productoAsignarTareas()">Asignar tareas</a></li>
				</ul>
			</div>
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Productos</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>--}}

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;

	var producto_list = [];
	var producto_list_index = 0;

	var producto_batch_ids = [];
	var producto_batch_data = [];

	$(document).ready(function(){
		initDataTable();
	});

	function buscar() {
		datatable.ajax.reload();
	}

	function limpiar() {
		$('#idDisponibilidad').val([]).trigger('change');
		datatable.search('').draw();
//		datatable.ajax.reload();
	}

	function initDataTable(){

		$('#idDisponibilidad').select2()
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});

		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.producto.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
//						d.idUsuarios = [8];
						d.idDisponibilidad = $('#idDisponibilidad').val();
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(response){ // Procesa datos recibidos
//						return response;
						var data = JSON.parse(response);
						producto_list = [];
						for(var i = 0 ; i < data.data.length; i++){
							producto_list.push(data.data[i].id);
						}
						return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: 'Producto'},
					{ title: 'Categoria'},
					{ data:  'visitas', title: 'Visitas'},
					{ title: 'Precio Europa'},
					{ title: 'Precio Proveedor'},
					{ title: 'Tipo'},       //'Precio Minimo'},
					{ title: 'Nuevo'},  // 'Disponibilidad'},
					{ data: 'created_at', title: 'Fecha Creacion'},
					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
							var name = '';
							if(row){
								name = row.nombre_alter;
//								return name;
							} else {
								name =  '<b class="text-red">@lang('messages.no_product')!</b>';
							}
							return '<a href="javascript:;" onclick="productoShow('+row.id+')">'+name+'</a>';
						}
					},{
						targets: 2,
						render: function(data, type, row){
							var response = '';
							if(row.producto !== null){
								if(row.categorias !== undefined){
									$.each(row.categorias, function(key, value){
										response += value.padre.nombre + ' / ' + value.nombre;
									});
									return response;
								} else {
									return 'Sin categorias';
								}
							} else {
								return '';
							}
						}
					},{
						targets:3,
						className: "text-right",
						render: function(data, type, row){
							if( row.publicacion ) {
								return row.publicacion.visitas;
							}
							return '';
						}
					},{
						targets:4,
						className: "text-right",
						render: function(data, type, row){
							if(row.precioEuropa !== null) {
								return row.precioEuropa + " " + row.monedaEuropa.toUpperCase();
							} else {
								return '';
							}

						}
					},{
						targets:5,
						className: "text-right",
						render: function(data, type, row){
							return row.precioProveedor + " " + row.monedaProveedor.toUpperCase();
						}
					},{
						targets:6,
						className: "text-right",
						render: function(data, type, row){
//							if(row.precioMinimo !== null) {
//								return row.precioMinimo + " " + row.monedaMinimo.toUpperCase();
//							} else {
//								return '';
//							}

							var html = '';
							if( parseInt(row.idProductoTipo, 10) === {{ \App\Models\Producto::TIPO_MAQUINARIA }}) {
								html = '<span class="label label-yellow">Maquinaria</span>';
							} else if( parseInt(row.idProductoTipo, 10) === {{ \App\Models\Producto::TIPO_ACCESORIO }}) {
								html = '<span class="label label-success">Accesorio</span>';
							} else {
								html = 'No definido';
							}

							return html;
						}
					},{
						targets:7,
						className: "with-btn-group",
						width: '70px',
						render: function(data, type, row){
							{{--var html = '';--}}
							{{--html += row.producto_item.disponibilidad.texto;--}}

							{{--if ( row.producto_item.idDisponibilidad == {{ \App\Models\ProductoItem::DISPONIBILIDAD_RESERVADO }})--}}
								{{--html += ' <i class="fa fa-clock text-primary"></i> ';--}}

							{{--return html;--}}
							var html = '';
							if( row.esNuevo == 1 ) {
								html = '<span class="label label-primary">NUEVO</span>';
							} else if( row.esNuevo == 0 ) {
								html = '<span class="label label-secondary">USADO</span>';
							} else {
								html = 'NO DEFINIDO';
							}
							return html;
						}
					},{
						targets:8,
						className: "text-right",
						width: '100px',
						render: function(data, type, row){
							return format_datetime(data);
						}
					},{
						targets:9,
						className: "with-btn-group",
						width: '70px',
						render: function(data, type, row){
							var html = '';

							html += '<div class="btn-group">'+
								'<a onclick="productoEdit('+ row.id +')" title="productoEdit( '+ row.id +' )" class="btn btn-primary btn-sm width-90 text-white"><i class="fa fa-edit"></i> Editar</a>'+
								'<a href="javascript:;" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown"><span class="caret"></span></a>'+
								'<div class="dropdown-menu dropdown-menu-right">';

							if( row.publicacion ) {
//								html += '<a href="javascript:;" title="productoEdit( '+ row.id +' )" onclick="productoEdit('+ row.id +')" class="btn btn-sm btn-primary width-90"><i class="fa fa-edit"></i> Editar</a>';
//							} else {

								var urlAnuncio = '{{ route('admin.announcementInbox.show', [ 'idAnnouncement' ]) }}';
								urlAnuncio = urlAnuncio.replace('idAnnouncement', row.publicacion.id);

								var urlAnuncioPublico = '{{ route('public.producto', [ 'idAnnouncement' ]) }}';
								urlAnuncioPublico = urlAnuncioPublico.replace('idAnnouncement', row.publicacion.id);

								html += '<a href="'+ urlAnuncio +'" class="dropdown-item" target="_blank" data-toggle="tooltip" data-title="Ir al anuncio"><i class="fa fa-arrow-right"></i><i class="fa fa-shopping-cart"></i> Ir al anuncio</a>'+
										'<a href="'+ urlAnuncioPublico +'" class="dropdown-item" target="_blank" data-toggle="tooltip" data-title="Ir al anuncio público"><i class="fa fa-arrow-right"></i><i class="fa fa-shopping-cart"></i> Ver anuncio público</a>'+
										'<div class="divider"></div>';
							}

							html += '<a href="javascript:;" onclick="productoAsignarTareas('+ row.id +')" class="dropdown-item" data-toggle="tooltip" data-title="Asignar actividades">Asignar actividades</a>'+
								'</div>'+
								'</div>';

							return html;
						}
					}, {
						targets: 10,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							var checked = '';
							if( producto_batch_ids.indexOf( parseInt(row.id, 10) ) >= 0 ) {
								checked = ' checked="checked" ';
							}
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" onclick="persistCheckbox(this)" '+checked+' />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
				$('[data-toggle="tooltip"]').tooltip();
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function productoShow(id){
		var url = '{{ route('admin.producto.show', ['idProducto']) }}';
		url = url.replace('idProducto', id);
		var modal = openModal(url, 'Producto', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formProductoShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}

	function productoCreate(){
		var url = '{{ route('admin.producto.create') }}';
		var modal = openModal(url, 'Nuevo Producto', null, { size: 'modal-lg' });
		setModalHandler('formProductoCreate:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function productoEdit(id){

		producto_list_index = producto_list.indexOf( id );

		var url = '{{ route('admin.producto.edit', ['idProducto']) }}';
		url = url.replace('idProducto', id);
		var modal = openModal(url, 'Editar Producto', null, { size: 'modal-80' });
		setModalHandler('formProductoEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function productoDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.producto.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
					$('#idsChecker').prop('checked', false);
				});
			}
		});
	}

	function persistCheckbox( e ) {
		var position = producto_batch_ids.indexOf( parseInt(e.value, 10 ) );
		if( e.checked ) {
			if( position > -1 ) {
			} else {
				producto_batch_ids.push( parseInt(e.value, 10 ));
				var tr = $(e).closest('tr');
				producto_batch_data[ parseInt(e.value, 10 ) ] = datatable.row( tr ).data();
			}
		} else {
			if( position > -1 ) {
				producto_batch_ids.splice(position, 1);
				delete producto_batch_data[ parseInt(e.value, 10 ) ];
			}
		}
	}

	function productoAsignarTareas( id ) {

		var url = '';
		if( id !== undefined ) {
			url = '{{ route('admin.producto.asignarActividades', [ 'idProducto' ]) }}';
			url = url.replace('idProducto', id);
		} else {
			url = '{{ route('admin.producto.asignarActividadesMass') }}';
		}

		var modal = openModal(url, 'Asignación de tareas', null, { 'size' : 'modal-80'});
		setModalHandler('formAsignacionActividad:success', function( event, data ){
			dismissModal(modal);
//			datatable.ajax.reload(null, true);
		});

	}

	function mostrarSeleccionados() {
//		$('#data-table_filter input').val( producto_batch_ids.join(',') );
//		datatable.ajax.reload();

		datatable.search( producto_batch_ids.join(',') ).draw();
	}

</script>
@endpush