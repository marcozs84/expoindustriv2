@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"><a href="{{ route('admin.proveedor.index') }}">Proveedores</a></li>
		<li class="breadcrumb-item active">Actualizar Proveedor</li>
	</ol>

	<h1 class="page-header">Actualizar Proveedor</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizar Proveedor</h4>
		</div>
		<div class="panel panel-body">
			<form id="formProveedorEdit" method="get" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Nombre</label>
					<div class="col-sm-9">
						{!! Form::text('nombre', $proveedor->nombre, [
							'id' => 'nombre',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Telefono</label>
					<div class="col-sm-9">
						{!! Form::text('telefono', implode(', ', $proveedor->Telefonos->pluck('numero')->toArray()), [
							'id' => 'telefono',
							'placeholder' => '',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15 group-empleado">
					<label class="col-form-label col-md-3">Padre</label>
					<div class="col-md-9">
						{!! Form::select('idPadre', $proveedores, $proveedor->idPadre, [
							'id' => 'idPadre',
							'class' => 'form-control default-select2',
							'placeholder' => 'Ninguno'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">NIT</label>
					<div class="col-sm-9">
						{!! Form::text('nit', $proveedor->nit, [
							'id' => 'nit',
							'placeholder' => '',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="form-group row m-b-15 group-empleado">
					<label class="col-form-label col-md-3">Ciudad</label>
					<div class="col-md-9">
						{!! Form::select('idCiudad', $ciudades, $proveedor->idCiudad, [
							'id' => 'idCiudad',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15 group-empleado">
					<label class="col-form-label col-md-3">Estado</label>
					<div class="col-md-9">
						{!! Form::select('idEstado', [
								'1' => 'Activo',
								'0' => 'Inactivo',
							], $proveedor->idProveedorEstado, [
							'id' => 'idEstado',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Url Sitio Web</label>
					<div class="col-sm-9">
						{!! Form::text('urlSitio', $proveedor->urlSitio, [
							'id' => 'urlSitio',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Facebook</label>
					<div class="col-sm-9">
						{!! Form::text('urlFacebook', $proveedor->urlFacebook, [
							'id' => 'urlFacebook',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Twitter</label>
					<div class="col-sm-9">
						{!! Form::text('urlTwitter', $proveedor->urlTwitter, [
							'id' => 'urlTwitter',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				{{--<div class="form-group row m-b-15">--}}
				{{--<label class="col-form-label col-md-3">Tiene Store</label>--}}
				{{--<div class="col-sm-10">9}}
					{{--{!! Form::text('proveedorTieneStore', '', [--}}
				{{--'id' => 'proveedorTieneStore',--}}
				{{--'placeholder' => '',--}}
				{{--'class' => 'form-control',--}}
				{{--'autofocus'--}}
				{{--]) !!}--}}
				{{--</div>--}}
				{{--</div>--}}
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Dirección</label>
					<div class="col-md-9">
						{!! Form::textarea('direccion', $proveedorDireccion, [
							'id' => 'direccion',
							'placeholder' => 'Dirección',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Descripcion</label>
					<div class="col-md-9">
						{!! Form::textarea('descripcion', $proveedor->descripcion, [
							'id' => 'descripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="proveedorCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="proveedorGuardar()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>
	$( "#formProveedorEdit" ).submit(function( event ) {
		proveedorGuardar();
		event.preventDefault();
	});

	function proveedorGuardar(){
		var url = '{{ route('resource.proveedor.update', [$proveedor->id]) }}';
		ajaxPatch(url, {
			idPadre:    $('#formProveedorEdit #idPadre').val(),
			idCiudad:   $('#formProveedorEdit #idCiudad').val(),
			nombre:     $('#formProveedorEdit #nombre').val(),
			telefono:     $('#formProveedorEdit #telefono').val(),
			nit:        $('#formProveedorEdit #nit').val(),
			idProveedorEstado:   $('#formProveedorEdit #idEstado').val(),
			urlSitio:   $('#formProveedorEdit #urlSitio').val(),
			urlFacebook:$('#formProveedorEdit #urlFacebook').val(),
			urlTwitter: $('#formProveedorEdit #urlTwitter').val(),
//			tieneStore: $('#formProveedorEdit #proveedorTieneStore').val(),
			direccion:$('#formProveedorEdit #direccion').val(),
			descripcion:$('#formProveedorEdit #descripcion').val(),
		}, function(data){
			$(document).trigger('formProveedorEdit:aceptar', data.data );
		});
	}

	function proveedorCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formProveedorEdit:cancelar');
		@else
			redirect('{{ route('admin.proveedor.index')  }}');
		@endif
	}

	function formInit(){
		$('#formProveedorEdit #nombre').focus();
	}
	formInit();

</script>
@endpush

