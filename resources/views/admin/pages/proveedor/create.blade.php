@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Proveedores</li>
	</ol>

	<h1 class="page-header">Nuevo Proveedor</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Proveedor</h4>
		</div>
		<div class="panel panel-body">
			<form id="formProveedorCreate" method="get" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Nombre de la empresa</label>
					<div class="col-sm-9">
						{!! Form::text('nombre', '', [
							'id' => 'nombre',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Telefono</label>
					<div class="col-sm-9">
						{!! Form::text('telefono', '', [
							'id' => 'telefono',
							'placeholder' => '',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15 group-empleado">
					<label class="col-form-label col-md-3">Padre</label>
					<div class="col-md-9">
						{!! Form::select('idPadre', $proveedores, null, [
							'id' => 'idPadre',
							'class' => 'form-control default-select2',
							'placeholder' => 'Ninguno'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">NIT</label>
					<div class="col-sm-9">
						{!! Form::text('nit', '', [
							'id' => 'nit',
							'placeholder' => '',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="form-group row m-b-15 group-empleado">
					<label class="col-form-label col-md-3">Ciudad</label>
					<div class="col-md-9">
						{!! Form::select('idCiudad', $ciudades, null, [
							'id' => 'idCiudad',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15 group-empleado">
					<label class="col-form-label col-md-3">Estado</label>
					<div class="col-md-9">
						{!! Form::select('idEstado', [
								'1' => 'Activo',
								'0' => 'Inactivo',
							], null, [
							'id' => 'idEstado',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Url Sitio Web</label>
					<div class="col-sm-9">
						{!! Form::text('urlSitio', '', [
							'id' => 'urlSitio',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Facebook</label>
					<div class="col-sm-9">
						{!! Form::text('urlFacebook', '', [
							'id' => 'urlFacebook',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Twitter</label>
					<div class="col-sm-9">
						{!! Form::text('urlTwitter', '', [
							'id' => 'urlTwitter',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				{{--<div class="form-group row m-b-15">--}}
					{{--<label class="col-form-label col-md-3">Tiene Store</label>--}}
					{{--<div class="col-sm-10">9}}
						{{--{!! Form::text('proveedorTieneStore', '', [--}}
							{{--'id' => 'proveedorTieneStore',--}}
							{{--'placeholder' => '',--}}
							{{--'class' => 'form-control',--}}
							{{--'autofocus'--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Dirección</label>
					<div class="col-md-9">
						{!! Form::textarea('direccion', '', [
							'id' => 'direccion',
							'placeholder' => 'Dirección',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Descripcion</label>
					<div class="col-md-9">
						{!! Form::textarea('descripcion', '', [
							'id' => 'descripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="proveedorCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="proveedorGuardar()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>
	$( "#formProveedorCreate" ).submit(function( event ) {
		proveedorGuardar();
		event.preventDefault();
	});

	function proveedorGuardar(){
		var url = '{{ route('resource.proveedor.store') }}';
		ajaxPost(url, {
			idPadre:    $('#formProveedorCreate #idPadre').val(),
			idCiudad:   $('#formProveedorCreate #idCiudad').val(),
			nombre:     $('#formProveedorCreate #nombre').val(),
			telefono:     $('#formProveedorCreate #telefono').val(),
			nit:        $('#formProveedorCreate #nit').val(),
			idProveedorEstado:   $('#formProveedorCreate #idEstado').val(),
			urlSitio:   $('#formProveedorCreate #urlSitio').val(),
			urlFacebook:$('#formProveedorCreate #urlFacebook').val(),
			urlTwitter: $('#formProveedorCreate #urlTwitter').val(),
//			tieneStore: $('#formProveedorCreate #proveedorTieneStore').val(),
			direccion:$('#formProveedorCreate #direccion').val(),
			descripcion:$('#formProveedorCreate #descripcion').val(),
		}, function(data){
			$(document).trigger('formProveedorCreate:aceptar', data.data );
		});
	}

	function proveedorCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formProveedorCreate:cancelar');
		@else
			redirect('{{ route('admin.proveedor.index')  }}');
		@endif
	}

	function formInit(){
		$('#formProveedorCreate #nombre').focus();
	}
	formInit();

</script>
@endpush

