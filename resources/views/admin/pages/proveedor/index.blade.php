@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Proveedores</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Proveedores <small></small></h1>
	<!-- end page-header -->

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Proveedores</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="proveedorCreate()"><i class="fa fa-plus"></i> Crear</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="proveedorDelete()"><i class="fa fa-trash"></i> Eliminar</a>
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	$(document).ready(function(){
		initDataTable();
	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.proveedor.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						var data2 = JSON.parse(data);
//						console.table(data2.data);
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ data: 'nombre', title: 'Nombre'},
					{ title: 'Contactos'},
					{ title: 'Telefonos'},
					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
//							return '<a href="javascript:;" onclick="proveedorShow('+row.id+')">'+row.agenda.nombres_apellidos+'</a>';
							var url = '{{ route('admin.proveedor.show', [0]) }}';
							url = url.replace('0', row.id);
							try{
								return '<a href="'+url+'">'+row.nombre+'</a>';
							} catch(err) {
								return err;
							}

						}
					},{
						targets: 2,
						render: function(data, type, row){
							var html = '';
							$.each(row.proveedor_contactos, function(k, v) {
								html += v.agenda.nombres_apellidos + ' &lt;'+ v.usuario.username +'&gt;' + "<br>";
							});
//							if(row.proveedor_contactos[0].usuario){
//								return row.proveedor_contactos[0].usuario.username;
//							} else {
//								return '';
//							}
							return html;

						}
					},{
						targets: 3,
						render: function(data, type, row){
							var html = '';
							$.each(row.telefonos, function(k, v) {
								html += v.numero + ' ('+ v.descripcion +')' + "<br>";
							});
//							if(row.proveedor_contactos[0].usuario){
//								return row.proveedor_contactos[0].usuario.username;
//							} else {
//								return '';
//							}
							return html;

						}
					},{
						targets: 4,
						render: function(data, type, row){
//							return '<a href="javascript:;" onclick="proveedorShow('+row.id+')">'+row.agenda.nombres_apellidos+'</a>';
							try{
								return '<a href="javascript:;" onclick="proveedorEdit('+ row.id +')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';
							} catch(err) {
								return err;
							}

						}
					}, {
						targets: 5,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function proveedorShow(id){
		var url = '{{ route('admin.proveedor.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Proveedor', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formProveedorShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}


	function proveedorCreate(){
		var url = '{{ route('admin.proveedor.create') }}';
		var modal = openModal(url, 'Nuevo Proveedor');
		setModalHandler('formProveedorCreate:aceptar', function(event, data){
			var proveedor = data.proveedor;
			var option = $('<option>', {
				value: proveedor.id
			}).html(proveedor.nombre);
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function proveedorEdit(id){
		var url = '{{ route('admin.proveedor.index') }}/'+id+'/edit';
		var modal = openModal(url, 'Editar Proveedor');
		setModalHandler('formProveedorEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function proveedorDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length == 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '{{ route('resource.proveedor.destroy', [0]) }}';

		ajaxDelete(url, ids, function(){
			datatable.ajax.reload(null, false);
		});
	}

</script>
@endpush