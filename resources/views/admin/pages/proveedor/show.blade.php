@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@stop

@push('css')
<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.proveedor.index') }}">Clientes</a></li>
		<li class="breadcrumb-item active">{{ $obj->nombre }}</li>
	</ol>

	<h1 class="page-header">Proveedor</h1>

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Información de Proveedor (empresa)</h4>
				</div>
				<div class="panel panel-body">

					<table class="table table-condensed table-striped">
						<tr>
							<td class="text-right">Nombre</td>
							<td>{{ $obj->nombre }}</td>
						</tr>
						<tr>
							<td class="text-right">Padre</td>
							<td>
								@if($obj->idPadre != 0)
									{{ $obj->Padre->nombre }}
								@endif
							</td>
						</tr>
						<tr>
							<td class="text-right">Telefono</td>
							<td>{{ implode(', ', $obj->Telefonos->pluck('numero')->toArray()) }}</td>
						</tr>
						<tr>
							<td class="text-right">VAT</td>
							<td>{{ $obj->nit }}</td>
						</tr>
						<tr>
							<td class="text-right">Sitio Web</td>
							<td>{{ $obj->urlSitio }}</td>
						</tr>
						<tr>
							<td class="text-right">Facebook</td>
							<td>{{ $obj->urlFacebook }}</td>
						</tr>
						<tr>
							<td class="text-right">Twitter</td>
							<td>{{ $obj->urlTwitter }}</td>
						</tr>
						<tr>
							<td class="text-right">Descripción</td>
							<td>

								<div class="row">
									<div class="col-4">
										<h4>SE</h4>
										<span id="descripcion_se">{{ $descripcion_se }}</span>
									</div>
									<div class="col-4">
										<h4>ES</h4>
										<span id="descripcion_es">{{ $descripcion_es }}</span>
									</div>
									<div class="col-4">
										<h4>EN</h4>
										<span id="descripcion_en">{{ $descripcion_en }}</span>
									</div>
								</div>

								<div class="text-center m-t-30">
									<a href="javascript:;" onclick="translateFieldML()" class="btn btn-primary">Traducir</a>
								</div>

							</td>
						</tr>
					</table>
					<br>

				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">

			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Contactos</h4>
				</div>
				<div class="panel panel-body">

					<div class="row">
						@foreach($obj->ProveedorContactos as $contacto)
							<div class="col-md-6">

								<div class="panel panel-primary">
									<div class="panel-heading f-w-700">{{ $contacto->Agenda->nombres_apellidos }}</div>
									<div class="panel-body bg-blue text-white">
										@foreach($contacto->Agenda->Correos as $correo)
											{{ $correo->correo }} ({{ ucfirst($correo->descripcion) }})<br>
										@endforeach
										Telf.:
										@foreach($contacto->Agenda->Telefonos as $telefono)
											{{ $telefono->numero }} ({{ ucfirst($telefono->descripcion) }})<br>
										@endforeach
										@if(isset($contacto->Agenda->pais) && isset($global_paises[$contacto->Agenda->pais]))
											<b>{{ $global_paises[$contacto->Agenda->pais] }} ({{ strtoupper($contacto->Agenda->pais) }})</b>
										@endif
											<br><br>
											<a href="javscript:;" onclick="usuarioEdit({{ $contacto->Usuario->id }})" class="btn btn-default btn-icon btn-circle btn-md" data-toggle="tooltip" data-title="Editar"><i class="fa fa-edit"></i></a>
									</div>
								</div>

							</div>
						@endforeach
					</div>
				</div>
			</div>


		</div>

		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Pagos</h4>
				</div>
				<div class="panel panel-body">
					{{--<h5>Pagos</h5>--}}
					@if(isset($linkStripe) && $linkStripe != '')
						<a href="{{ $linkStripe }}" target="_blank" class="btn btn-primary" style="vertical-align: middle;"><i class="fa fa-address-card "></i> Ir al perfil de pagos en Stripe</a>
						<br><br>

						<h5>Pagos realizados hasta la fecha</h5>

						<table class="table table-condensed table-striped">
							<tr>
								<th>Description</th>
								<th>Monto</th>
							</tr>
							@foreach($pagos['data'] as $pago)
								<tr>
									<td>{{ $pago['description'] }}</td>
									<td>{{ $pago['amount'] }} {{ $pago['currency'] }}</td>
								</tr>
							@endforeach

						</table>
					@else
						<p>No tiene perfil en Stripe</p>
					@endif
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">



			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Direcciones</h4>
				</div>
				<div class="panel panel-body">
					<b>Direccion Primaria</b>
					<br>
					{{ nl2br($ubicacionPrimaria->direccion) }}
					<br><br>
					<b>Direccion secundarias</b>
					<table class="table table-condensed">
					@foreach($ubicacionesSecundarias as $ubicacion)
						<tr>
							<td>{!! nl2br($ubicacion->direccion) !!}</td>
							<td><a href="javascript:;" class="btn btn-sm" onclick="removeAddress({{ $ubicacion->id }})"><i class="fa fa-trash"></i></a></td>
						</tr>
					@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col col-md-12">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Galeria de imágenes</h4>
				</div>
				<div class="panel panel-body">
					@forelse($obj->Galerias as $galeria)
						<div id="gallery_{{ $galeria->id }}" class="gallery isotope" style="display:table;">

							@forelse($galeria->Imagenes as $imagen)
								<div class="image gallery-group-1 isotope-item" style="display:table-cell;">
									<div class="image-inner">
										<a href="/company_logo/{{ $imagen->filename }}" data-lightbox="gallery-group-1">
											<img src="/company_logo/{{ $imagen->filename }}" alt="" style="height:auto; max-width:150px;" />
										</a>
									</div>
									<div class="image-info">
										<h5 class="title"><a href="javascript:;" onclick="removeLogo({{ $imagen->id }})"><i class="fa fa-trash"></i> Eliminar</a></h5>
									</div>
								</div>
							@empty
								No hay imagenes

							@endforelse

						</div>

					@empty
						No hay galerias
					@endforelse
				</div>
			</div>



		</div>
	</div>

@stop

@section('libraries')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

	<script>
		$(document).ready(function() {
			Gallery.init();
		});
	</script>

@stop

@push('scripts')

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>

	<script>

		function formInit(){

			var container = $('#gallery');
			var dividerValue = calculateDivider();
			var containerWidth = $(container).width();
			var columnWidth = containerWidth / dividerValue;
			$(container).isotope({
				resizable: true,
				masonry: {
					columnWidth: columnWidth
				}
			});

			$(window).smartresize(function() {
				var dividerValue = calculateDivider();
				var containerWidth = $(container).width();
				var columnWidth = containerWidth / dividerValue;
				$(container).isotope({
					masonry: {
						columnWidth: columnWidth
					}
				});
			});
		}
		formInit();


		function removeLogo(id) {
			var url = '{{ route('resource.imagen.destroy', [0]) }}';
			url = url.replace(0, id);

			ajaxDelete(url, {}, function (data) {
				refreshPage();
			})
		}

		function removeAddress(id) {
			var url = '{{ route('resource.ubicacion.destroy', [0]) }}';
			ajaxDelete(url, { id: id }, function(data) {
				swal('Ubicación eliminada correctamente.').then(function() {
					refreshPage();
				});
			}, null, {});
		}

		function habilitarStore(e){
			var url = '{{ route('resource.proveedor.update', [$obj->id]) }}';
			var data = {
				'tieneStore' : e.value,
			};

			ajaxPatch(url, data, function(data) {
				swal('iStore actualizado');
			});
		}

		function usuarioEdit(id) {
			var url = '{{ route('admin.usuario.edit', [ 'idUsuario' ]) }}';
			url = url.replace('idUsuario', id);
			var modal = openModal(url, 'Editar Usuario');
			setModalHandler('#formUsuarioCreate:aceptar', function(event, data){
				console.log(data);
				dismissModal(modal);
			});
		}

		function calculateDivider() {
			var dividerValue = 4;
			if ($(this).width() <= 480) {
				dividerValue = 1;
			} else if ($(this).width() <= 767) {
				dividerValue = 2;
			} else if ($(this).width() <= 980) {
				dividerValue = 3;
			}
			return dividerValue;
		}

		function translateFieldML(key){
			var url = '{{ route('admin.traduccion.multiline', [ 'owner' => 'proveedor', 'owner_id' => $obj->id, 'campo' => 'descripcion' ]) }}';
			url = url.replace('key', key);
			var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
			setModalHandler('formTraduccion:success', function ( event, response ) {
				$('#descripcion_es').html( response.es );
				$('#descripcion_se').html( response.se );
				$('#descripcion_en').html( response.en );
				dismissModal(modal);
			});
		}

	</script>
@endpush

