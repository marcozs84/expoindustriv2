@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Inbox')

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb hidden-print pull-right">
		<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
		<li class="breadcrumb-item active">Invoice</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header hidden-print">Faktura <small></small></h1>
	<!-- end page-header -->

	<!-- begin invoice -->
	<div class="invoice">
		<!-- begin invoice-company -->
		<div class="invoice-company text-inverse f-w-600">
			<span class="pull-right hidden-print">
			<a href="javascript:;" class="btn btn-sm btn-white m-b-10 p-l-5"><i class="fa fa-file-pdf t-plus-1 text-danger fa-fw fa-lg"></i> Export as PDF</a>
			<a href="javascript:;" onclick="window.print()" class="btn btn-sm btn-white m-b-10 p-l-5"><i class="fa fa-print t-plus-1 fa-fw fa-lg"></i> Print</a>
			</span>
			{{ $orden->Cliente->Agenda->nombres_apellidos }}  <b>&lt;{{ $orden->Cliente->Usuario->username }}></b>
		</div>
		<!-- end invoice-company -->
		<!-- begin invoice-header -->
		<div class="invoice-header">
			<div class="invoice-from">
				<small>from</small>
				<address class="m-t-5 m-b-5">
					<strong class="text-inverse">ExpoIndustri S/A</strong><br />
					Street Address<br />
					City, Zip Code<br />
					Phone: (123) 456-7890<br />
					Fax: (123) 456-7890
				</address>
			</div>
			<div class="invoice-to">
				<small>to</small>
				<address class="m-t-5 m-b-5">
					<strong class="text-inverse">{{ $orden->Cliente->Agenda->nombres_apellidos }}</strong><br />
					Street Address<br />
					Nit: {{ $orden->Cliente->Agenda->nit }}<br />
					@if($orden->Cliente->Agenda->Pais != '')
						Country: {{ $global_paises[$orden->Cliente->Agenda->Pais] }}<br />
					@endif
					@if($orden->Cliente->Agenda->Telefonos->count() > 0)
						Phone: {{ $orden->Cliente->Agenda->Telefonos[0]->numero }}<br />
					@endif
				</address>
			</div>
			<div class="invoice-date">
				<small>Factura / {{ date('F') }} period</small>
				<div class="date text-inverse m-t-5">{{ date('F d,Y') }}</div>
				<div class="invoice-detail">
					#{{ $factura->serial }}<br />
					&lt;Services Product&gt;
				</div>
			</div>
		</div>
		<!-- end invoice-header -->
		<!-- begin invoice-content -->
		<div class="invoice-content">
			<!-- begin table-responsive -->
			<div class="table-responsive">
				<table class="table table-invoice">
					<thead>
					<tr>
						<th>Descripción del servicio</th>
						{{--<th class="text-center" width="10%">RATE</th>--}}
						{{--<th class="text-center" width="10%">HOURS</th>--}}
						<th class="text-right" width="20%">LINE TOTAL</th>
					</tr>
					</thead>
					<tbody>

					@if($cot && isset($cot->definicion['calc']['informe_tecnico']))
						<tr>
							<td>
								<span class="text-inverse">Informe Técnico</span><br />
								<small>(Ya se pagó al registrar la orden)</small>
							</td>
							{{--<td class="text-center">$50.00</td>--}}
							{{--<td class="text-center">50</td>--}}
							<td class="text-right" style="text-decoration: line-through;">${{ $cot->definicion['calc']['informe_tecnico'] }}</td>
						</tr>
					@endif
					@if($cot && isset($cot->definicion['calc']['seguro_extra']))
						<tr>
							<td>
								<span class="text-inverse">Seguro de maquinaria</span><br />
								<small></small>
							</td>
							{{--<td class="text-center">$50.00</td>--}}
							{{--<td class="text-center">50</td>--}}
							<td class="text-right">${{ $cot->definicion['calc']['seguro_extra'] }}</td>
						</tr>
					@endif
					@if($cot && isset($cot->definicion['transporte']))
						<tr>
							<td>
								<span class="text-inverse">Transporte</span><br />
								<small>{{ \App\Models\Transporte::find($cot->definicion['transporte']['id'])->nombre }}</small>
							</td>
							{{--<td class="text-center">$50.00</td>--}}
							{{--<td class="text-center">50</td>--}}
							<td class="text-right">${{ $cot->definicion['transporte']['precio'] }}</td>
						</tr>
					@endif
					@if($cot && isset($cot->definicion['destino']))
						<tr>
							<td>
								<span class="text-inverse">Destino</span><br />
								<small>({{ \App\Models\Destino::find($cot->definicion['destino']['id'])->Pais->nombre }}:
									{!! \App\Models\Destino::find($cot->definicion['destino']['id'])->nombre !!})</small>
							</td>
							{{--<td class="text-center">$50.00</td>--}}
							{{--<td class="text-center">50</td>--}}
							<td class="text-right">${{ $cot->definicion['destino']['precio'] }}</td>
						</tr>
					@endif
					</tbody>
				</table>
			</div>
			<!-- end table-responsive -->
			<!-- begin invoice-price -->
			<div class="invoice-price">
				<div class="invoice-price-left">
					<div class="invoice-price-row">
						<div class="sub-price">
							<small>SUBTOTAL</small>
							<span class="text-inverse">${{ ( $cot ? $cot->definicion['precio_final'] : 0 ) }}</span>
						</div>
						<div class="sub-price">
							<i class="fa fa-plus text-muted"></i>
						</div>
						<div class="sub-price">
							<small>PAYPAL FEE (5.4%)</small>
							<span class="text-inverse">$108.00</span>
						</div>
					</div>
				</div>
				<div class="invoice-price-right">
					<small>TOTAL</small> <span class="f-w-600">${{ ( $cot ? $cot->definicion['precio_final'] : 0 ) }}</span>
				</div>
			</div>
			<!-- end invoice-price -->
		</div>
		<!-- end invoice-content -->
		<!-- begin invoice-note -->
		<div class="invoice-note">
			* Make all cheques payable to [Your Company Name]<br />
			* Payment is due within 30 days<br />
			* If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]
		</div>
		<!-- end invoice-note -->
		<!-- begin invoice-footer -->
		<div class="invoice-footer">
			<p class="text-center m-b-5 f-w-600">
				THANK YOU FOR YOUR BUSINESS
			</p>
			<p class="text-center">
				<span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
				<span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
				<span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
			</p>
		</div>
		<!-- end invoice-footer -->
	</div>
	<!-- end invoice -->
@endsection

@push('scripts')

@endpush