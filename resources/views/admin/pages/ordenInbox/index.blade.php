@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Inbox')

@push('css')
<style>
	.list-email .email-time {
		width:120px;
	}
</style>
@endpush

@section('content')
	<!-- begin vertical-box -->
	<div class="vertical-box with-grid inbox">
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column width-200 bg-silver hidden-xs">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper bg-silver text-center">
					<a href="javascript:;" onclick="ordenCreate()" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
						Nuevo
					</a>
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin wrapper -->
								<div class="wrapper p-0">
									<div class="nav-title"><b>FOLDERS</b></div>
									<ul class="nav nav-inbox">
										<li class="active"><a href="javascript:;" onclick="regresar()"><i class="fa fa-inbox fa-fw m-r-5"></i> Inbox <span class="badge pull-right">{{ $counterOrders }}</span></a></li>
										<li><a href="javascript:;"><i class="fa fa-thumbs-up fa-fw m-r-5"></i> Aprobados</a></li>
										<li><a href="javascript:;"><i class="fa fa-thumbs-down fa-fw m-r-5"></i> Rechazados</a></li>
										<li><a href="javascript:;"><i class="fa fa-search fa-fw m-r-5"></i> Todos</a></li>
										<li><a href="javascript:;"><i class="fa fa-trash fa-fw m-r-5"></i> Eliminados</a></li>
									</ul>
									{{--<div class="nav-title"><b>LABEL</b></div>--}}
									{{--<ul class="nav nav-inbox">--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-inverse"></i> Admin</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-primary"></i> Designer & Employer</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-success"></i> Staff</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-warning"></i> Sponsorer</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-danger"></i> Client</a></li>--}}
									{{--</ul>--}}
								</div>
								<!-- end wrapper -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column bg-white">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper bg-silver-lighter">
					<!-- begin btn-toolbar -->
					<div id="controlsMessage" class="clearfix hide">
						<div class="pull-left">
							<div class="btn-group m-r-5">
								{{--<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-reply f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Reply</span></a>--}}
								<a href="javascript:;" onclick="regresar()" class="btn btn-white btn-sm"><i class="fa fa-arrow-left f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Regresar</span></a>
							</div>
							<div class="btn-group m-r-5">
								<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-trash f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Delete</span></a>
								<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-archive f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Archive</span></a>
							</div>
						</div>
						<div class="pull-right">
							<div class="btn-group">
								<a href="email_inbox.html" class="btn btn-white btn-sm disabled"><i class="fa fa-arrow-up f-s-14 t-plus-1"></i></a>
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-arrow-down f-s-14 t-plus-1"></i></a>
							</div>
							<div class="btn-group m-l-5">
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-times f-s-14 t-plus-1"></i></a>
							</div>
						</div>
					</div>
					<div id="controsListing" class="btn-toolbar ">
						<div class="btn-group m-r-5">
							<a href="javascript:;" class="p-t-5 pull-left m-r-3 m-t-2" data-click="email-select-all">
								<i class="far fa-square fa-fw text-muted f-s-16 l-minus-2"></i>
							</a>
						</div>
						<!-- begin btn-group -->
						<div class="btn-group dropdown m-r-5">
							<a href="javascript:;" class="btn btn-white btn-sm" data-toggle="dropdown">
								<span class="lblVendedorNombre">Ver todos</span> <span class="caret m-l-3"></span>
							</a>
							<ul class="dropdown-menu text-left text-sm">
								<li class="active"><a href="javascript:;" onclick="reloadListing(1)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Todos</a></li>
								@foreach($vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id') as $id => $nombre)
									<li><a href="javascript:;" onclick="setVendedor({{ $id }}, '{{ $nombre }}')"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> {{ $nombre }}</a></li>
								@endforeach
							</ul>
						</div>

						<div class="btn-group dropdown m-r-5">
							<button class="btn btn-white btn-sm" data-toggle="dropdown">
								Mensajes por página ( <span id="lblPageSize"></span> ) <span class="caret m-l-3"></span>
							</button>
							<ul class="dropdown-menu text-left text-sm">
								<li class="active"><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> All</a></li>
								<li><a href="javascript:;" onclick="setPageLength(10)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 10 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(30)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 30 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(50)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 50 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(100)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 100 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(200)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 200 </a></li>
							</ul>
						</div>

						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group m-r-5">
							<a href="javascript:;" onclick="reloadListing()" class="btn btn-sm btn-white"><i class="fa fa-redo f-s-14 t-plus-1"></i></a>
						</div>
						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group">
							<button class="btn btn-sm btn-white hide" data-email-action="delete"><i class="fa t-plus-1 fa-thumbs-up f-s-14 m-r-3"></i> <span class="hidden-xs">Aprobar</span></button>
							<button class="btn btn-sm btn-white hide" data-email-action="archive"><i class="fa t-plus-1 fa-thumbs-down f-s-14 m-r-3"></i> <span class="hidden-xs">Rechazar</span></button>
							<button class="btn btn-sm btn-white hide" data-email-action="archive"><i class="fa t-plus-1 fa-trash f-s-14 m-r-3"></i> <span class="hidden-xs">Eliminar</span></button>
						</div>
						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group ml-auto">
							<a href="javascript:;" class="btn btn-white btn-sm btnPrevList">
								<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
							</a>
							<a href="javascript:;" class="btn btn-white btn-sm btnNextList">
								<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
							</a>
						</div>
						<!-- end btn-group -->
					</div>
					<!-- end btn-toolbar -->
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin list-email -->
								<div id="listingHolder" class="listingHolder m-0 p-0">
									<ul id="messagesHolder" class="list-group list-group-lg no-radius list-email">
									</ul>
								</div>
								<!-- end list-email -->
								<div id="detailHolder" class="detailHolder m-0 p-0 hide">
								</div>
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
				<!-- begin wrapper -->
				<div class="wrapper bg-silver-lighter clearfix">
					<div class="btn-group pull-right">
						<a href="javascript:;" class="btn btn-white btn-sm btnPrevList">
							<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
						</a>
						<a href="javascript:;" class="btn btn-white btn-sm btnNextList">
							<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
						</a>
					</div>
					<div class="m-t-5 text-inverse f-w-600 msgCounter">1,232 messages</div>
				</div>
				<!-- end wrapper -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
	</div>
	<!-- end vertical-box -->
<div class="hide">
<li class="list-group-item unread" id="stub_messageItem">
	<div class="email-checkbox">
		<label>
			<i class="far fa-square"></i>
			<input type="checkbox" data-checked="email-checkbox" />
		</label>
	</div>
	<a href="javascript:;" class="email-user bg-gradient-blue">
		<span class="text-white">F</span>
	</a>
	<div class="email-info">
		<a href="javascript:;">
			<span class="email-time">Today</span>
			<span class="email-sender">Facebook Blueprint</span>
			<span class="email-title">Newly released courses, holiday marketing tips, how-to video, and more!</span>
			<span class="email-desc">Sed scelerisque dui lacus, quis pellentesque lorem tincidunt rhoncus. Nulla accumsan elit pharetra, lacinia turpis nec, varius erat.</span>
		</a>
	</div>
</li>
</div>
@endsection

@push('scripts')
<script src="/assets/js/demo/email-inbox.demo.js"></script>
<script>

var idMessage = {{ $idMessage }};
var inboxPageLength = 10;
var currentPage = 1;

$(document).ready(function() {
	InboxV2.init();

	reloadListing();

	if(idMessage > 0){
		loadDetail(idMessage);
	}

});

function setPageLength(size) {
	window.localStorage.setItem('inboxPageLength', size);
	reloadListing();
}

function setVendedor(idVendedor, nombre) {
	reloadListing(1, idVendedor);
	$('.lblVendedorNombre').html(nombre);
}

function reloadListing(page, idVendedor){
	var url = '{{ route('resource.orden.ds') }}';
	var urlItem = '{{ route('admin.ordenInbox.show', [0]) }}';

	var ls = window.localStorage.getItem('inboxPageLength');
	if( ls !== null ) {
		inboxPageLength = ls;
		$('#lblPageSize').html(ls);
	}

	var data = {
		length: inboxPageLength
	};
	if(page !== undefined) {
		data['page'] = page;
	} else {
		data['page'] = currentPage;
	}
	if(idVendedor !== undefined) {
		data['idVendedor'] = idVendedor;
	}
	var mh = $('#messagesHolder');
	mh.html('');
	var loadMsg = 0;
	ajaxPost(url, data, function(data){
		var dt = data.data;

		$('.msgCounter').html(data.from + ' - ' + data.to + ' de ' + data.total + ' Anuncios');

		currentPage = data.current_page;
		if(data.current_page > 1) {
			$('.btnPrevList').show().attr('onclick', 'reloadListing('+ (parseInt(data.current_page, 10) - 1) +')');
		} else {
			$('.btnPrevList').hide();
		}
		if(data.current_page === data.last_page) {
			$('.btnNextList').hide();
		} else {
			$('.btnNextList').show().attr('onclick', 'reloadListing('+ (parseInt(data.current_page, 10) + 1) +')');
		}

		$.each(dt, function(key, row){
			loadMsg = row.id;
			//row = row.publicacion;
			var item = $('#stub_messageItem').clone();
			item.removeAttr('id');
			urlShow = urlItem.replace('0', row.id);
			item.find('.email-user, .email-info a').prop('href', urlShow);
			item.find('.email-user, .email-info a').attr('onclick', 'loadDetail('+row.id+')');
			item.find('.email-checkbox input').prop('id', row.id);
			item.find('.email-user span').html(row.cliente.agenda.nombres_apellidos.substring(0, 1));
			item.find('.email-info .email-time').html(format_datetime(row.created_at));
//			item.find('.email-info .email-sender').html(row.cliente.usuario.username);
			item.find('.email-info .email-sender').html(row.cliente.agenda.nombres_apellidos);
			if( ! row.producto_items[0].producto ) {
				item.find('.email-info .email-title').addClass('text-danger').html('Producto eliminado');
			} else {
				item.find('.email-info .email-title').html(row.producto_items[0].producto.marca.nombre+' '+row.producto_items[0].producto.modelo.nombre);
			}

			item.find('.email-info .email-desc').html(row.producto_items[0].ubicacion.direccion);
			mh.append(item);
		});
		if(loadMsg !== 0){
//			loadDetail(loadMsg);
		} else {
			console.log(" NOT LOADING DETAIL ");
		}
	}, null, { silent: true });
}

function loadDetail(id){
	var url = '{{ route('admin.ordenInbox.show', [0]) }}';
	url = url.replace('0', id);
	var holder = $('#detailHolder');
	var msg = $('<div>').prop('id', 'msg_' + id);
	holder.html(msg);
	msg.load(url, function(response, status, xhr){
		$('#controlsMessage').removeClass('hide');
		$('#controsListing').addClass('hide');
		$('#detailHolder').removeClass('hide');
		$('#listingHolder').addClass('hide');
	});

	return false;
}

function loadFactura(e, id){
	if(e !== null){
		e.preventDefault();
	}
	var url = '{{ route('admin.ordenInbox.factura', [0]) }}';
	url = url.replace('0', id);
	var holder = $('#detailHolder');
	var msg = $('<div>').prop('id', 'msg_' + id);
	holder.html(msg);
	msg.load(url, function(response, status, xhr){
		$('#controlsMessage').removeClass('hide');
		$('#controsListing').addClass('hide');
		$('#detailHolder').removeClass('hide');
		$('#listingHolder').addClass('hide');
	});

	return false;
}

function regresar(){
	$('#controlsMessage').addClass('hide');
	$('#controsListing').removeClass('hide');
	$('#detailHolder').addClass('hide');
	$('#listingHolder').removeClass('hide');
}

function ordenCreate(){
	var url = '{{ route('admin.orden.create') }}';
	var modal = openModal(url, 'Registrar Orden', null, { size:'modal-lg' });
	setModalHandler('formOrdenCreate:success', function( event, data ){

		swal( 'Orden registrada exitosamente.', {
			title: 'Listo!',
			icon: "success",
			buttons: {
				goOrden: {
					text: 'Ir a la orden',
					value: 'goOrden',
					visible: true,
					className: 'btn btn-danger',
				},
				ok: {
					text: 'Aceptar',
					value: 'ok',
					visible: true,
					className: 'btn btn-primary',
				},
			}
		}).then(function(value) {

			switch (value) {
				case "goOrden":
					var url = '{{ route('admin.ordenInbox.show', ['idOrden']) }}';
					url = url.replace('idOrden', data.id);
					redirect(url);
					break;

				default:
					dismissModal(modal);
					reloadListing();
			}
		});
	});
}
</script>
@endpush