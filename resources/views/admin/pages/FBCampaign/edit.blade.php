@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.FBCampaign.index') }}">F B Campaign</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $FBCampaign->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando F B Campaign</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando F B Campaign</h4>
		</div>
		<div class="panel panel-body">
			<form id="formFBCampaignEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id F B C</label>
					<div class="col-8">
						{!! Form::select('idFBC', [], '', [
							'id' => 'idFBC',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id F B C</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idFBC', $FBCampaign->idFBC, [--}}
							{{--'id' => 'idFBC',--}}
							{{--'placeholder' => 'Id F B C',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fbclid</label>
					<div class="col-8">
						{!! Form::text('fbclid', $FBCampaign->fbclid, [
							'id' => 'fbclid',
							'placeholder' => 'Fbclid',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="FBCampaignCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="FBCampaignSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){



	$idFBCSearch = Search.resource('#formFBCampaignEdit #idFBC', {
		url: '{{ route('resource.fBC.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		defaultValue: {
			value: {{ $FBCampaign->idFBC }},
			text: '{{ $FBCampaign->FBC->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idFBCSearch, {{ $FBCampaign->idFBC }}, '{{ $FBCampaign->FBC->id }});



}

$( "#formFBCampaignEdit" ).submit(function( event ) {
	FBCampaignSave();
	event.preventDefault();
});

function FBCampaignSave(){
	var url = '{{ route('resource.FBCampaign.update', [$FBCampaign->id]) }}';
	ajaxPatch(url, {
		idFBC: 		$('#formFBCampaignEdit #idFBC').val(),
		fbclid: 		$('#formFBCampaignEdit #fbclid').val()
	}, function(){
		$(document).trigger('formFBCampaignEdit:success');
	});
}

function FBCampaignCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formFBCampaignEdit:cancel');
	@else
		redirect('{{ route('admin.FBCampaign.index')  }}');
	@endif
}

</script>
@endpush

