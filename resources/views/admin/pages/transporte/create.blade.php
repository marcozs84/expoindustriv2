@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.transporte.index') }}">Transporte</a></li>
		<li class="breadcrumb-item active">Nuevo/a transporte</li>
	</ol>

	<h1 class="page-header">Nuevo Transporte</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Transporte</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTransporteCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', '', [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
					<div class="col-8">
						{!! Form::text('descripcion', '', [
							'id' => 'descripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio</label>
					<div class="col-8">
						{!! Form::text('precio', '', [
							'id' => 'precio',
							'placeholder' => 'Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Es Especial</label>
					<div class="col-8">
						{!! Form::text('esEspecial', '', [
							'id' => 'esEspecial',
							'placeholder' => 'Es Especial',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::text('estado', '', [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transporteCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="transporteSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){







}

$( "#formTransporteCreate" ).submit(function( event ) {
	transporteSave();
	event.preventDefault();
});

function transporteSave(){
	var url = '{{ route('resource.transporte.store') }}';
	ajaxPost(url, {
		nombre: 		$('#formTransporteCreate #nombre').val(),
		descripcion: 	$('#formTransporteCreate #descripcion').val(),
		precio: 		$('#formTransporteCreate #precio').val(),
		esEspecial: 	$('#formTransporteCreate #esEspecial').val(),
		estado: 		$('#formTransporteCreate #estado').val()
	}, function(){
		$(document).trigger('formTransporteCreate:success');
	});
}

function transporteCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTransporteCreate:cancel');
	@else
		redirect('{{ route('admin.transporte.index')  }}');
	@endif
}

</script>
@endpush

