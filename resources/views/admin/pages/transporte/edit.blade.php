@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.transporte.index') }}">Transporte</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $transporte->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Transporte</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Transporte</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTransporteEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', $transporte->nombre, [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
					<div class="col-8">
						{!! Form::text('descripcion', $transporte->descripcion, [
							'id' => 'descripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio</label>
					<div class="col-8">
						{!! Form::text('precio', $transporte->precio, [
							'id' => 'precio',
							'placeholder' => 'Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Es Especial</label>
					<div class="col-8">
						{!! Form::text('esEspecial', $transporte->esEspecial, [
							'id' => 'esEspecial',
							'placeholder' => 'Es Especial',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::text('estado', $transporte->estado, [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<h4 style="clear:both; width:100%;">Traducciones</h4>
				<table class="table table-condensed table-striped">
					<tr>
						<th class="text-center">Texto</th>
						<th class="text-center">Expañol</th>
						<th class="text-center">Sueco</th>
						<th class="text-center">Inglés</th>
					</tr>
					<tr>
						<td class="text-center">Nombre</td>
						<td>
							{!! Form::text('nombre_es', $nombre_es, ['id' => 'nombre_es', 'class' => 'form-control']) !!}
						</td>
						<td>
							{!! Form::text('nombre_se', $nombre_se, ['id' => 'nombre_se', 'class' => 'form-control']) !!}
						</td>
						<td>
							{!! Form::text('nombre_en', $nombre_en, ['id' => 'nombre_en', 'class' => 'form-control']) !!}
						</td>
					</tr>
					<tr>
						<td class="text-center">Descripcion</td>
						<td>
							{!! Form::text('desc_es', $desc_es, ['id' => 'desc_es', 'class' => 'form-control']) !!}
						</td>
						<td>
							{!! Form::text('desc_se', $desc_se, ['id' => 'desc_se', 'class' => 'form-control']) !!}
						</td>
						<td>
							{!! Form::text('desc_en', $desc_en, ['id' => 'desc_en', 'class' => 'form-control']) !!}
						</td>
					</tr>
				</table>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transporteCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="transporteSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){





}

$( "#formTransporteEdit" ).submit(function( event ) {
	transporteSave();
	event.preventDefault();
});

function transporteSave(){
	var url = '{{ route('resource.transporte.update', [$transporte->id]) }}';
	ajaxPatch(url, {
		nombre: 		$('#formTransporteEdit #nombre').val(),
		descripcion: 	$('#formTransporteEdit #descripcion').val(),
		precio: 		$('#formTransporteEdit #precio').val(),
		esEspecial: 	$('#formTransporteEdit #esEspecial').val(),
		estado: 		$('#formTransporteEdit #estado').val(),
		nombre_es: 		$('#formTransporteEdit #nombre_es').val(),
		nombre_se: 		$('#formTransporteEdit #nombre_se').val(),
		nombre_en: 		$('#formTransporteEdit #nombre_en').val(),
		desc_es: 		$('#formTransporteEdit #desc_es').val(),
		desc_se: 		$('#formTransporteEdit #desc_se').val(),
		desc_en: 		$('#formTransporteEdit #desc_en').val(),
	}, function(){
		$(document).trigger('formTransporteEdit:success');
	});
}

function transporteCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTransporteEdit:cancel');
	@else
		redirect('{{ route('admin.transporte.index')  }}');
	@endif
}

</script>
@endpush

