@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')


<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Transporte</li>
	</ol>

	<h1 class="page-header">Nuevo Transporte</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Transporte</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTransporteShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $transporte->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{{ $transporte->nombre }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
					<div class="col-8">
						{{ $transporte->descripcion }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio</label>
					<div class="col-8">
						{{ $transporte->precio }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Es Especial</label>
					<div class="col-8">
						{{ $transporte->esEspecial }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{{ $transporte->estado }}
					</div>
				</div>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transporteEdit({{ $transporte->id }})">Edit</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transporteCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif


function transporteCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTransporteShow:cancel');
	@else
		redirect('{{ route('admin.transporte.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

