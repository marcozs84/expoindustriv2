@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.marca.index') }}">Marca</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $marca->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Marca</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Marca</h4>
		</div>
		<div class="panel panel-body">
			<form id="formMarcaEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{!! Form::text('nombre', $marca->nombre, [
							'id' => 'nombre',
							'placeholder' => 'Nombre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-12 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                        {!! Form::textarea('descripcion', $marca->descripcion, [
                            'id' => 'descripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="marcaCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="marcaSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){





}

$( "#formMarcaEdit" ).submit(function( event ) {
	marcaSave();
	event.preventDefault();
});

function marcaSave(){
	var url = '{{ route('resource.marca.update', [$marca->id]) }}';
	ajaxPatch(url, {
		nombre: 		$('#formMarcaEdit #nombre').val(),
		descripcion: 	$('#formMarcaEdit #descripcion').val()
	}, function(){
		$(document).trigger('formMarcaEdit:success');
	});
}

function marcaCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formMarcaEdit:cancel');
	@else
		redirect('{{ route('admin.marca.index')  }}');
	@endif
}

</script>
@endpush

