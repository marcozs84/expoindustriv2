@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Marca</li>
	</ol>

	<h1 class="page-header">Nuevo Marca</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Marca</h4>
		</div>
		<div class="panel panel-body">
			<form id="formMarcaShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $marca->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre</label>
					<div class="col-8">
						{{ $marca->nombre }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                       {!! $marca->descripcion !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{{ ($marca->created_at ? $marca->created_at->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>

				<table class="table table-striped table-condensed">
					@foreach( $marca->Modelos as $modelo )
					<tr>
						<td>
							<a href="javascript:;" onclick="modeloShow({{ $modelo->id }})">{{ $modelo->nombre }}</a>
						</td>
					</tr>
					@endforeach
				</table>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="marcaEdit({{ $marca->id }})">Edit</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="marcaCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif


function marcaCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formMarcaShow:cancel');
	@else
		redirect('{{ route('admin.marca.index')  }}');
	@endif
}

function formInit(){

}

function modeloShow(id){
	var url = '{{ route('admin.modelo.show', [0]) }}';
	url = url.replace(0, id);
	var modal = openModal(url, 'Modelo', null, {
		'size': 'modal-lg'
	});
}

function modeloEdit(id){
	var url = '{{ route('admin.modelo.edit', 'idRecord') }}';
	url = url.replace('idRecord', id);
	var modal = openModal(url, 'Editar Modelo', null, { size:'modal-lg' });
	setModalHandler('formModeloEdit:success', function(){
		dismissModal(modal);
		datatable.ajax.reload(null, false);
	});
}


</script>
@endpush

