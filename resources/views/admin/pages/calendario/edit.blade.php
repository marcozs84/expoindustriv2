@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.calendario.index') }}">Calendario</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $calendario->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Calendario</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Calendario</h4>
		</div>
		<div class="panel panel-body">
			<form id="formCalendarioEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Calendario Tipo</label>
					<div class="col-8">
						{!! Form::select('idCalendarioTipo', [], '', [
							'id' => 'idCalendarioTipo',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Calendario Tipo</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idCalendarioTipo', $calendario->idCalendarioTipo, [--}}
							{{--'id' => 'idCalendarioTipo',--}}
							{{--'placeholder' => 'Id Calendario Tipo',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', $calendario->owner_type, [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', $calendario->owner_id, [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Extrainfo</label>
                    <div class="col-8">
                        {!! Form::textarea('extrainfo', $calendario->extrainfo, [
                            'id' => 'extrainfo',
                            'placeholder' => 'Extrainfo',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="calendarioCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="calendarioSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){



	$idCalendarioTipoSearch = Search.resource('#formCalendarioEdit #idCalendarioTipo', {
		url: '{{ route('resource.calendarioTipo.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.text;
		},
		defaultValue: {
			value: {{ $calendario->idCalendarioTipo }},
			text: '{{ $calendario->CalendarioTipo->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idCalendarioTipoSearch, {{ $calendario->idCalendarioTipo }}, '{{ $calendario->CalendarioTipo->id }});



}

$( "#formCalendarioEdit" ).submit(function( event ) {
	calendarioSave();
	event.preventDefault();
});

function calendarioSave(){
	var url = '{{ route('resource.calendario.update', [$calendario->id]) }}';
	var formCalendarioEdit = $('#formCalendarioEdit');
	ajaxPatch(url, {
		idCalendarioTipo: formCalendarioEdit.find('#idCalendarioTipo').val(),
		owner_type: 	formCalendarioEdit.find('#owner_type').val(),
		owner_id: 		formCalendarioEdit.find('#owner_id').val(),
		extrainfo: 	formCalendarioEdit.find('#extrainfo').val()
	}, function(){
		$(document).trigger('formCalendarioEdit:success');
	});
}

function calendarioCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formCalendarioEdit:cancel');
	@else
		redirect('{{ route('admin.calendario.index')  }}');
	@endif
}

</script>
@endpush

