@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Calendario</li>
	</ol>

	<h1 class="page-header">Nuevo Calendario</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Calendario</h4>
		</div>
		<div class="panel panel-body">
			<form id="formCalendarioShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $calendario->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Calendario Tipo</label>
					<div class="col-8">
						{{ $calendario->idCalendarioTipo }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{{ $calendario->owner_type }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{{ $calendario->owner_id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Extrainfo</label>
                    <div class="col-8">
                       {!! $calendario->extrainfo !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{{ ($calendario->created_at ? $calendario->created_at->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="calendarioEdit({{ $calendario->id }})">Edit</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="calendarioCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif


function calendarioCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formCalendarioShow:cancel');
	@else
		redirect('{{ route('admin.calendario.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

