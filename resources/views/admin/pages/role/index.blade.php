@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')


@push('css')
	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />

	<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
<style>
	.col-mz{
		/*max-width:150px !important;*/
		word-wrap: break-word !important;
		white-space:pre-wrap !important;
        text-overflow: ellipsis !important;
	}
	/*td {word-wrap: break-word !important;}*/
</style>
@endpush

@section('content')

<ol class="breadcrumb pull-right">
	<li><a href="javascript:;">Home</a></li>
	<li><a href="/admin">Admin</a></li>
	<li class="active">Roles</li>
</ol>

<h1 class="page-header">Roles <small></small></h1>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Roles</h4>
            </div>
            <div class="panel-body">

	            <div class="row">
		            <div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

			            <a class="btn btn-sm btn-primary" href="javascript:;" onclick="roleCrear()"><i class="fa fa-plus"></i> Nuevo Role</a>
			            <a class="btn btn-sm btn-primary" href="javascript:;" onclick="roleEliminar()"><i class="fa fa-trash"></i> Eliminar Role(s)</a>

		            </div>
	            </div>

                <table id="data-table" class="table table-striped table-bordered width-full table-condensed"></table>

            </div>
        </div>
    </div>
</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<!-- ================== END PAGE LEVEL JS ================== -->
<script>

var datatable = null;
var modal = null;

@if($isAjaxRequest)
formInit();
@else
$(function(){ formInit(); });
@endif

function formInit(){
	$('.multiple-select2, .default-select2').select2();

	initDataTable();
}

function buscar(){
	datatable.ajax.reload();
}

function limpiar(){
	$('#convenios').val([]).trigger('change');
	datatable.ajax.reload();
}

function initDataTable(){
	if (!$.fn.DataTable.isDataTable('#data-table')) {
	    datatable = $('#data-table').DataTable({
	        lengthMenu: [5, 10, 20, 30, 50, 100, 150],
	        pageLength: 20,
	        ajax: {
	            url: '{{ route('resource.role.ds') }}',
	            dataSrc: 'data',
				method: 'POST',
	            'headers': {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
	            data: function (d){
		            d.nombre = $('#nombre').val();
		            d.convenios = $('#convenios').val();
	            },
	            dataFilter: function(data){ // Procesa datos recibidos
	                return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
	            }
	        },
	        columns: [
				{ data: "id", title: 'Id' },
				{ data: "nombre", title: 'Llave'},
				{ data: "descripcion", title: 'Descripcion'},
				{ data: "estado", title: 'Estado'},
				{ title: '<input type="checkbox" id="idsChecker" value="" />'},
	            // ...
	        ],
	        columnDefs: [
	            {
	                targets: 0,
	                className: 'text-center',
	                width: '30px',
	            },{
	                targets: 1,
	                render: function(data, type, row){
	                    return '<a href="javascript:roleEditar('+row.id+');">'+ data +'</a>';
	                }
	            },{
	                targets: 3,
	                render: function(data, type, row){
	                    return (data == 1) ? '<i class="fa fa-lg fa-check-circle text-orange"></i> Activo' : '<i class="fa fa-lg fa-times-circle text-red"></i> Inactivo';
	                }
	            }, {
	                targets: 4,
	                className: 'text-center',
	                width: '20px',
	                render: function(data, type, row){
	                    return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
	                }
	            }
	        ],
	        fixedHeader: {
	            header: true,
	            headerOffset: $('#header').height()
	        },
	        bSort : false,
	        bAutoWidth: false,
	        processing: true,
	        serverSide: true,
	        responsive: true,
	        fixedColumns: true
	    }).on('draw.dt', function(){
            $('#idsChecker').on('change', function(e){
                $('.chkId').prop('checked', $(this).prop('checked'));
            });
	    });
	} else {
	    datatable.ajax.reload();
	}

}

function roleCrear(){
    var url = '{{ route('admin.role.create') }}';
    var modal = openModal(url, 'Nuevo Role');
    setModalHandler('formRoleCreate:aceptar', function(){
        dismissModal(modal);
        datatable.ajax.reload();
    });
}

function roleEditar(id){
    var url = '{{ route('admin.role.index') }}/'+id+'/edit';
    var modal = openModal(url, 'Editar Role', null, { size: 'modal-lg' });
    setModalHandler('formRoleEdit:aceptar', function(){
        dismissModal(modal);
        datatable.ajax.reload(null, false);
    });
}

function roleEliminar(){
	var ids = [];
    $('.chkId').each(function() {
        if($(this).prop('checked')){
            ids.push($(this).val());
        }
    });

    var url = '{{ route('resource.role.destroy', [0]) }}';

    ajaxDelete(url, ids, function(){
        datatable.ajax.reload(null, false);
    });
}

</script>
@endpush