@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.role.index') }}">Roles</li>
	</ol>

	<h1 class="page-header">Nuevo Role</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Role</h4>
		</div>
		<div class="panel panel-body">
			<form id="formRoleCreate" method="get" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Nombre</label>
                    <div class="col-sm-10">
                        {!! Form::text('roleNombre', '', [
                            'id' => 'roleNombre',
                            'placeholder' => '',
                            'class' => 'form-control',
                            'autofocus'
                        ]) !!}
                    </div>
                </div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Descripcion</label>
                    <div class="col-md-10">
                        {!! Form::textarea('roleDescripcion', '', [
                            'id' => 'roleDescripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Estado</label>
                    <div class="col-md-10">
                        {!! Form::select('roleEstado', [1 => 'Activo', 0 => 'Inactivo'], null, [
                            'id' => 'roleEstado',
                            'class' => 'default-select2 form-control',
                        ]); !!}
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
				<div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="roleCancelar()">Cerrar</a>
                        <a class="btn btn-primary btn-sm" href="javascript:;" onclick="roleGuardar()" >Guardar</a>
                    </div>
                </div>
            </form>
		</div>
	</div>

@stop

@push('scripts')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>
$.getScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js').done(function() {
	$('.multiple-select2, .default-select2').select2();
});

$( "#formRoleCreate" ).submit(function( event ) {
	roleGuardar();
	event.preventDefault();
});

function roleGuardar(){
	var url = '{{ route('resource.role.store') }}';
	ajaxPost(url, {
		nombre: $('#roleNombre').val(),
		descripcion: $('#roleDescripcion').val(),
		estado: $('#roleEstado').val()
    }, function(){
		$(document).trigger('formRoleCreate:aceptar');
	});
}

function roleCancelar(){
	@if($isAjaxRequest)
		$(document).trigger('formRoleCreate:cancelar');
	@else
		redirect('{{ route('admin.role.index')  }}');
	@endif
}

</script>
@endpush

