@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.role.index') }}">Roles</li>
	</ol>

	<h1 class="page-header">Editar role</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Editar role</h4>
		</div>
		<div class="panel panel-body">

		<ul class="nav nav-pills nav-tabs-inverse nav-justified nav-justified-mobile">
		    <li class="nav-item"><a href="#role-tab-1" data-toggle="tab" class="nav-link active"><i class="fa fa-info-circle"></i> Detalles</a></li>
		    <li class="nav-item"><a href="#role-tab-2" data-toggle="tab" class="nav-link"><i class="fa fa-list-alt"></i> Permisos</a></li>
		</ul>
		<div class="tab-content p-0 m-b-0">
		    <div class="tab-pane fade active show p-t-10" id="role-tab-1">
				<form id="formRoleEdit" method="get" class="form-horizontal">
					<div class="form-group row m-b-15">
	                    <label class="col-form-label col-md-2">Nombre</label>
	                    <div class="col-sm-10">
	                        {!! Form::text('roleNombre', $role->nombre, [
	                            'id' => 'roleNombre',
	                            'placeholder' => '',
	                            'class' => 'form-control',
	                            'autofocus'
	                        ]) !!}
	                    </div>
	                </div>
	                {{--<div class="form-group row m-b-15">--}}
	                    {{--<label class="col-form-label col-md-2">Convenio</label>--}}
	                    {{--<div class="col-md-10">--}}
	                        {{--{!! Form::select('roleConvenio', $convenios, $role->idConvenio, [--}}
	                            {{--'id' => 'roleConvenio',--}}
	                            {{--'placeholder' => 'Convenio',--}}
	                            {{--'class' => 'default-select2 form-control',--}}
	                        {{--]); !!}--}}
	                    {{--</div>--}}
	                {{--</div>--}}
	                <div class="form-group row m-b-15">
	                    <label class="col-form-label col-md-2">Descripcion</label>
	                    <div class="col-md-10">
	                        {!! Form::textarea('roleDescripcion', $role->descripcion, [
	                            'id' => 'roleDescripcion',
	                            'placeholder' => 'Descripcion',
	                            'class' => 'form-control',
	                            'rows' => 6
	                        ]) !!}
	                    </div>
	                </div>
	                <div class="form-group row m-b-15">
	                    <label class="col-form-label col-md-2">Estado</label>
	                    <div class="col-md-10">
	                        {!! Form::select('roleEstado', [1 => 'Activo', 0 => 'Inactivo'], $role->estado, [
	                            'id' => 'roleEstado',
	                            'class' => 'default-select2 form-control',
	                        ]); !!}
	                    </div>
	                </div>
	                <div class="hr-line-dashed"></div>
	                <div class="form-group row">
	                    <div class="col-lg-12 text-right">
	                        <a class="btn btn-white btn-sm" data-dismiss="modal" onclick="roleCancelar()" href="javascript:;">Cancelar</a>
	                        <a class="btn btn-primary btn-sm" href="javascript:;" onclick="roleGuardar()" >Guardar</a>
	                    </div>
	                </div>
	            </form>
		    </div>
		    <div class="tab-pane fade" style="/*max-height:500px; overflow:auto;*/" id="role-tab-2">

			    <div class="row">
				    <div class="col col-lg-6 col-md-6 col-sm-6">
					    <h5><i class="fa fa-chevron-right"></i> Resumen de permisos actuales</h5>
					    <table id="tbRolePermisos" class="table table-striped table-bordered width-full table-condensed">
						    <thead>
						    <tr>
							    <th>Permiso</th>
							    <th>Descripcion</th>
							    <th></th>
						    </tr>
						    </thead>
						    <tbody>
						    @foreach($asignaciones as $idPermiso => $value)
							    <tr>
								    <td>{{ $role->Permisos->where('id', $idPermiso)->first()->nombre }}</td>
								    <td>
								    {{--@if(count($value) > 0 )--}}
									    {{--@foreach($value as $id)--}}
										    {{--{{ $convenios[$id].' ' }}--}}
									    {{--@endforeach--}}
								    {{--@endif--}}
									    {{ $role->Permisos->where('id', $idPermiso)->first()->descripcion }}
								    </td>
								    <td>
									    <a href="javascript:;" onclick="guardarRolePermiso({{ $role->id }}, {{ $idPermiso }}, 0)"><i class="fa fa-times"></i></a>
								    </td>
							    </tr>
						    @endforeach
						    </tbody>
					    </table>
				    </div>
				    <div class="col col-lg-6 col-md-6 col-sm-6">
					    <h5><i class="fa fa-chevron-right"></i> Listado de permisos disponibles</h5>
					    <table id="arbolPermisos" class="table table-bordered width-full table-condensed">
						    <thead>
						    <tr>
							    <th>Permiso</th>
							    <th colspan="2">Convenios</th>
						    </tr>
						    </thead>
						    <tbody>
						    </tbody>
					    </table>
				    </div>

				    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
					    <a class="btn btn-white btn-sm" data-dismiss="modal" onclick="roleCancelar()" href="javascript:;">Cerrar</a>
				    </div>
			    </div>


		    </div>
		</div>


		</div>
	</div>

@stop

@push('scripts')
<script>
	var convenios = {!! json_encode($convenios) !!};
$.getScript('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js').done(function() {
	$('.multiple-select2').select2();
	$('[data-toggle="tooltip"]').tooltip();
	$('.multiple-select2').on("select2:select", function (e) {
		var idRole = $('#'+this.id).data('idrole');
		var idPermiso = $('#'+this.id).data('idpermiso');
		console.log(this.id);
		console.log(idRole, idPermiso);
		$('#chk_'+idRole+'_'+idPermiso).prop('checked', true);
		guardarRolePermiso(idRole, idPermiso);
    });
	$('.multiple-select2').on("select2:unselect", function (e) {
		var idRole = $('#'+this.id).data('idrole');
		var idPermiso = $('#'+this.id).data('idpermiso');
		$('#chk_'+idRole+'_'+idPermiso).prop('checked', true);
		guardarRolePermiso(idRole, idPermiso);
    });
});

$('#roleNombre').focus();
$( "#formRoleEdit" ).submit(function( event ) {
	roleGuardar();
	event.preventDefault();
});

function roleGuardar(){
	var url = '{{ route('resource.role.update', [$role->id]) }}';
	ajaxPatch(url, {
		nombre: $('#roleNombre').val(),
		descripcion: $('#roleDescripcion').val(),
		estado: $('#roleEstado').val(),
//		idConvenio: $('#roleConvenio').val()
    }, function(){
		$(document).trigger('formRoleEdit:aceptar');
	});
}

function roleCancelar(){
	@if($isAjaxRequest)
		$(document).trigger('formRoleEdit:cancelar');
	@else
		redirect('{{ route('admin.role.index')  }}');
	@endif
}

function guardarRolePermiso(idRole, idPermiso, status){
	var url = '{{ route('resource.role.guardarRolePermiso') }}';
	var idConvenios = $('#conv_'+idRole+'_'+idPermiso).val();
	if(status !== undefined){
		$('#chk_'+idRole+'_'+idPermiso).prop('checked', (status === 1) ? true : false );
	} else {
		status = ($('#chk_'+idRole+'_'+idPermiso).prop('checked')) ? 1 : 0;
	}

	if(status === 0){
		$('#conv_'+idRole+'_'+idPermiso).val([]).trigger('change');
	}

	var arConvenios = [];
	$.each(convenios, function(k,v){
		arConvenios[k] = v;
	});
	ajaxPost(url, {
		status: status,
		idRole: idRole,
		idPermiso: idPermiso,
		idConvenios: idConvenios
	}, function(data){
		var permisos = data.data;
		var tb = $('#tbRolePermisos tbody');
		tb.html('');

		for(var i = 0; i < permisos.length ; i++){
			var tr = $('<tr>');
			var td1 = $('<td>');
			var td2 = $('<td>');
			var td3 = $('<td>');

			td1.html(permisos[i].nombre);
			td3.html('<a href="javascript:;" onclick="guardarRolePermiso({{ $role->id }}, '+permisos[i].id+', 0)"><i class="fa fa-times"></i></a>');
			if(permisos[i].pivot.idConvenio !== undefined){
				if($('#per_'+permisos[i].id).length){
					$('#per_'+permisos[i].id).append(', '+arConvenios[permisos[i].pivot.idConvenio]);
				} else {
					td2.attr('id', 'per_'+permisos[i].id);
					td2.html(arConvenios[permisos[i].pivot.idConvenio]);
					tr.append(td1);
					tr.append(td2);
					tr.append(td3);
					tb.append(tr);
				}
			} else {
				td2.html(permisos[i].descripcion);
				tr.append(td1);
				tr.append(td2);
				tr.append(td3);
				tb.append(tr);
			}
		}
	});
}

var selConvenios = $('<select>');
var arbolTb = $('#arbolPermisos tbody');
var asignaciones = {!! json_encode($asignaciones) !!}

function generarArbolPermisos(){

	selConvenios.addClass('multiple-select2');
	selConvenios.prop('multiple', 'multiple');
	$.each(convenios, function(k,v){
		var option = $('<option>');
		option.prop('value', k);
		option.html(v);
		selConvenios.append(option);
	});

	var arbolJson = {!! json_encode($arbol) !!};
	$.each(arbolJson, function(k,v){
		arbolTr(k,v, '', '', 10);
	});

	$.each(asignaciones, function(k, v){
		$('#chk_{{ $role->id }}_'+k).prop('checked', true);
		$('#conv_{{ $role->id }}_'+k).val(v).trigger('change');
	});
};

function arbolTr(k,v, genealogia, parent, indent){

	var tr = $('<tr>');
	if(genealogia !== ''){
		tr.addClass(genealogia);
		tr.addClass('hide');
	}

	genealogia += 'cs_'+k+'_';

	tr.addClass(parent);

	if(v.id !== undefined){
		var td = $('<td>'); // ---------- td
		td.css('padding-left', indent+'px');
		td.html(v.nombre);
		if( v.descripcion !== '' ) {
			td.append(' <a href="javascript:;" data-toggle="tooltip" data-title="'+ v.descripcion +'"><i class="fa fa-info-circle"></i></a>');
		}
		td.css('vertical-align', 'middle');
//		td.css('background-color', '#f0f3f5');
		tr.append(td);
		var td = $('<td>'); // ---------- td
		td.css('width', '30px');
		td.append('<input id="chk_{{ $role->id }}_'+v.id+'" type="checkbox" value=""  onchange="guardarRolePermiso({{ $role->id }},'+v.id+')">');
//		td.css('background-color', '#f0f3f5');
		tr.append(td);
		var td = $('<td>'); // ---------- td

		nsel = selConvenios.clone();
		nsel.prop('id', 'conv_{{ $role->id }}_'+v.id);
		nsel.data('idrole', {{ $role->id }});
		nsel.data('idpermiso', v.id);
		td.css('padding', '3px');
        td.append(nsel);
//		td.css('background-color', '#f0f3f5');
        tr.append(td);
        arbolTb.append(tr);
	} else {
		var i = $('<i>').addClass('fa fa-caret-right fa-sm text-black m-r-3');
		var a = $('<a>').prop('href', 'javascript:;').addClass(''/*'btn btn-primary btn-xs'*/).attr('onclick', 'toggleBranch("'+genealogia+'", this)');
		a.append(i);
		a.append(' ');
		a.append(k);
		var td = $('<td>'); // ---------- td
		td.css('padding-left', indent+'px');
		td.prop('colspan', 3);
		td.append(a);
		td.css('background-color', '#f0f3f5');
		tr.append(td);
		arbolTb.append(tr);

		$.each(v, function(k1,v1){
			arbolTr(k1,v1, genealogia , 'cs_'+k, indent+15);
		});
	}
}

generarArbolPermisos();

function toggleBranch(branch, e){
	var todosCerrados = true;
	$('.'+branch).each(function(e){
		if(!$(this).hasClass('hide')){
			todosCerrados = false;
		}
	});

	if(todosCerrados){
		$('.'+branch).removeClass('hide');
		$(e).find('i').removeClass('fa-caret-right');
		$(e).find('i').addClass('fa-caret-down');
	} else {
		$('.'+branch).addClass('hide');
		$(e).find('i').addClass('fa-caret-right');
        $(e).find('i').removeClass('fa-caret-down');
		$('#arbolPermisos tr').each(function(e){
			if($(this).attr('class') !== undefined){
				var regex = new RegExp(branch, 'g');
				var clase = $(this).attr('class');
				var result = clase.match(regex);
				if(result !== null){
					$(this).addClass('hide');
					$(this).find('i').each(function(e){
						$(this).addClass('fa-caret-right');
				        $(this).removeClass('fa-caret-down');
					});
				}
			}
		});
	}
}
</script>
@endpush

