@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Traduccion</li>
	</ol>

	<h1 class="page-header">Traducción</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Traduccion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTraduccion" method="get" class="form-horizontal">
				<h5 style="text-align:center;">{{ $campo }}</h5>
				<table class="table table-condensed table-striped">
					<tr>
						<th class="text-center bg-info text-white" style="text-transform: UPPERCASE;">Sueco</th>
						<th class="text-center bg-warning text-white" style="text-transform: UPPERCASE;">Español</th>
						<th class="text-center bg-primary text-white" style="text-transform: UPPERCASE;">Inglés</th>
					</tr>
					<tr>
						@php
							$prefix = 'ck_'.random_int(1,1000);
						@endphp
						<td>
							<div class="text-right">
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('se', 'es')"><i class="fa fa-arrow-right"></i> Español </a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('se', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
							{!! Form::textarea('se', $se, ['id' => $prefix.'se', 'style' => 'height:500px;', 'class' => 'form-control']) !!}
							<div class="text-right">
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('se', 'es')"><i class="fa fa-arrow-right"></i> Español</a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('se', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
						</td>
						<td>
							<div class="text-center">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('es', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('es', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
							{!! Form::textarea('es', $es, ['id' => $prefix.'es', 'style' => 'height:500px;', 'class' => 'form-control']) !!}
							<div class="text-center">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('es', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('es', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
						</td>
						<td>
							<div class="text-left">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('en', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('en', 'es')">Español <i class="fa fa-arrow-left"></i></a>
							</div>
							{!! Form::textarea('en', $en, ['id' => $prefix.'en', 'style' => 'height:500px;', 'class' => 'form-control']) !!}
							<div class="text-left">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('en', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('en', 'es')">Español <i class="fa fa-arrow-left"></i></a>
							</div>
						</td>
					</tr>
				</table>
				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="translationCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="translationSave()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@section('libraries')
	@if( $ck_enabled )
		<script src="/assets/plugins/ckeditor/ckeditor.js"></script>
	@endif
@stop

@push('scripts')
	<script>

		function formTraduccionInit() {
			@if( $ck_enabled )

//				CKEDITOR.editorConfig = function( config ){
//					// Define changes to default configuration here. For example:
//					// config.language = 'fr';
//					// config.uiColor = '#AADC6E';
//					config.height = '800px';
//				};
				CKEDITOR.replace( '{{ $prefix }}se', { height:'500px' } );
				CKEDITOR.replace( '{{ $prefix }}en', { height:'500px' } );
				CKEDITOR.replace( '{{ $prefix }}es', { height:'500px' } );
			@endif
		}

		$( "#formTraduccion" ).submit( function ( event ) {
			translationSave();
			event.preventDefault();
		});

		function translationSave(){
			var url = '{{ route('admin.traduccion.multiline', [$owner, $owner_id, $campo]) }}';
			ajaxPost(url, {
				@if( $ck_enabled )
				es: CKEDITOR.instances.{{ $prefix }}es.getData(),
				se: CKEDITOR.instances.{{ $prefix }}se.getData(),
				en: CKEDITOR.instances.{{ $prefix }}en.getData(),
				@else
				es: $('#es').val(),
				se: $('#se').val(),
				en: $('#en').val(),
				@endif

			}, function( response ){

				$('#es').val( response.data.es );
				$('#se').val( response.data.se );
				$('#en').val( response.data.en );

				$(document).trigger('formTraduccion:success', response.data);
			});
		}

		function translationCancel(){
			$(document).trigger('formTraduccion:cancel');
		}

		formTraduccionInit();

		function gtranslate( from, to ) {

			var oldClass = $('.btn_' + to + ' i').attr('class');
			var oldAction = $('.btn_' + to ).attr('onclick');

			$('.btn_' + to + ' i').attr('class', 'fas fa-cog fa-spin');
			$('.btn_' + to ).attr('onclick', '');

			// ----------
			var url = '{{ route('admin.traduccion.gtranslate') }}';
			var data = {
				'idTraduccion' : {{ $traduccion->id }},
				'from' : from,
				'to' : to,
				'text' : ''
			};

			@if( $ck_enabled )
				{{--data.text = eval('CKEDITOR.instances.{{ $prefix }}'+ from +'.getData()');--}}
				data.text = CKEDITOR.instances['{{ $prefix }}' + from ].getData();
			@else
				data.text = $('#' + from).val();
			@endif

			ajaxPost(url, data, function( response ){

				$('.btn_' + to + ' i').attr('class', oldClass);
				$('.btn_' + to ).attr('onclick', oldAction);

				@if( $ck_enabled )
					CKEDITOR.instances['{{ $prefix }}' + to ].setData( response.data.text );
				@else
					$('#' + to).val( response.data.text );
				@endif

//				$(document).trigger('formTraduccion:success', response.data);
			});
		}

	</script>
@endpush

