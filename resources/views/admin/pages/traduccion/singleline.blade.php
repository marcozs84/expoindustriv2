@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Traduccion</li>
	</ol>

	<h1 class="page-header">Traducción</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Traduccion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTraduccion" method="get" class="form-horizontal">
				<h5 style="text-align:center;">{{ $campo }}</h5>
				<table class="table table-condensed table-striped">
					<tr>
						<th class="text-center">Sueco</th>
						<th class="text-center">Español</th>
						<th class="text-center">Inglés</th>
					</tr>
					<tr>
						<td class="text-center">{{ $campo }}</td>
						<td>
							<div class="text-right">
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('se', 'es')"><i class="fa fa-arrow-right"></i> Español </a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('se', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
							{!! Form::text('se', $se, ['id' => 'se', 'class' => 'form-control']) !!}
							<div class="text-right">
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('se', 'es')"><i class="fa fa-arrow-right"></i> Español</a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('se', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
						</td>
						<td>
							<div class="text-center">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('es', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('es', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
							{!! Form::text('es', $es, ['id' => 'es', 'class' => 'form-control']) !!}
							<div class="text-center">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('es', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_en btn-primary m-b-5 m-t-5" onclick="gtranslate('es', 'en')"><i class="fa fa-arrow-right"></i> Inglés</a>
							</div>
						</td>
						<td>
							<div class="text-left">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('en', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('en', 'es')">Español <i class="fa fa-arrow-left"></i></a>
							</div>
							{!! Form::text('en', $en, ['id' => 'en', 'class' => 'form-control']) !!}
							<div class="text-left">
								<a href="javascript:;" class="btn btn-sm btn_se btn-info m-b-5 m-t-5" onclick="gtranslate('en', 'se')">Sueco <i class="fa fa-arrow-left"></i></a>
								<a href="javascript:;" class="btn btn-sm btn_es btn-warning m-b-5 m-t-5" onclick="gtranslate('en', 'es')">Español <i class="fa fa-arrow-left"></i></a>
							</div>
						</td>
					</tr>
				</table>
				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="translationCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="translationSave()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@section('libraries')
@stop

@push('scripts')
	<script>

		$( "#formTraduccion" ).submit(function( event ) {
			translationSave();
			event.preventDefault();
		});

		function translationSave(){
			var url = '{{ route('admin.traduccion.singleline', [$owner, $owner_id, $campo]) }}';
			ajaxPost(url, {
				es: $('#es').val(),
				se: $('#se').val(),
				en: $('#en').val()
			}, function( response ){

				$('#es').val( response.data.es );
				$('#se').val( response.data.se );
				$('#en').val( response.data.en );

				$(document).trigger('formTraduccion:success', response.data);
			});
		}

		function translationCancel(){
			$(document).trigger('formTraduccion:cancel');
		}

		function gtranslate( from, to ) {

			var oldClass = $('.btn_' + to + ' i').attr('class');
			var oldAction = $('.btn_' + to ).attr('onclick');

			$('.btn_' + to + ' i').attr('class', 'fas fa-cog fa-spin');
			$('.btn_' + to ).attr('onclick', '');

			// ----------
			var url = '{{ route('admin.traduccion.gtranslate') }}';
			var data = {
				'idTraduccion' : {{ $traduccion->id }},
				'from' : from,
				'to' : to,
				'text' : ''
			};

			data.text = $('#' + from).val();

			ajaxPost(url, data, function( response ){

				$('.btn_' + to + ' i').attr('class', oldClass);
				$('.btn_' + to ).attr('onclick', oldAction);

				$('#' + to).val( response.data.text );

				//				$(document).trigger('formTraduccion:success', response.data);
			});
		}

	</script>
@endpush

