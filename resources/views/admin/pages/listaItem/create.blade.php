@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.listaItem.index') }}">Lista Item</a></li>
		<li class="breadcrumb-item active">Nuevo/a listaItem</li>
	</ol>

	<h1 class="page-header">Nuevo Lista Item</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Lista Item</h4>
		</div>
		<div class="panel panel-body">
			<form id="formListaItemCreate" method="post" class="form-horizontal row">


				<div class="form-group col-md-6 col-sm-12 row m-b-15" >
					<label class="col-form-label col-4">Id Lista</label>
					<div class="col-8">
						@if( $idLista > 0 )
							<p style="padding-top:7px;">{{ $lista->nombre }}</p>
						@else
							{!! Form::number('idLista', '', [
								'id' => 'idLista',
								'placeholder' => 'Id Lista',
								'class' => 'form-control',
							]) !!}
						@endif
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Orden</label>
					<div class="col-8">
						{!! Form::number('orden', $orden, [
							'id' => 'orden',
							'placeholder' => 'Orden',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Texto</label>
					<div class="col-8">
						{!! Form::text('texto', '', [
							'id' => 'texto',
							'placeholder' => 'Texto',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Valor</label>
					<div class="col-8">
						{!! Form::text('valor', $valor, [
							'id' => 'valor',
							'placeholder' => 'Valor',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

			</form>

			<table id="dtOtherListaItems" class="table table-condensed-table-striped"></table>

			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="listaItemCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="listaItemSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>



<script>

var dtOtherListaItems = null;

@if($isAjaxRequest)
    formInit();
//	initDataTable();
@else
    $(function(){
    	formInit();
		initDataTable();
    });
@endif

$( "#formListaItemCreate" ).submit(function( event ) {
	listaItemSave();
	event.preventDefault();
});

function listaItemSave(){
	var url = '{{ route('resource.listaItem.store') }}';
	ajaxPost(url, {
		idLista: 	@if( $idLista > 0 ) {{ $idLista }}, // @endif $('#formListaItemCreate #idLista').val(),
		texto: 		$('#formListaItemCreate #texto').val(),
		valor: 		$('#formListaItemCreate #valor').val(),
		orden: 		$('#formListaItemCreate #orden').val()
	}, function(){
		$(document).trigger('formListaItemCreate:success');
	});
}

function listaItemCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formListaItemCreate:cancel');
	@else
		redirect('{{ route('admin.listaItem.index')  }}');
	@endif
}

function formInit(){



}

function listaItemEdit(id){
	var url = '{{ route('admin.listaItem.edit', ['idRecord']) }}';
	url = url.replace('idRecord', id);
	var modal = openModal(url, 'Editar ListaItem', null, { size:'modal-lg' });
	setModalHandler('formListaItemEdit:success', function(){
		dismissModal(modal);
		dtOtherListaItems.ajax.reload(null, false);
	});
}

function initDataTable(){
	if (!$.fn.DataTable.isDataTable('#dtOtherListaItems')) {
		dtOtherListaItems = $('#dtOtherListaItems').DataTable({
			paging: false,
			searching: false,
//				dom: 'Bfrtip',
			buttons: [
				{
					text: 'Nuevo',
					action: function(e, dt, node, config){
						alert('Button activated');
					}
				}
			],
			ajax: {
				url: '{{ route('resource.listaItem.ds') }}',
				dataSrc: 'data',
				method: 'POST',
				'headers': {
					'X-CSRF-TOKEN': '{{ csrf_token() }}'
				},
				data: function (d){ // Procesa datos enviados
					d.idLista = {{ $idLista }};
					// ...
				},
				dataFilter: function(data){ // Procesa datos recibidos
					return data;
//	                    var data = JSON.parse(data);
//	                    for(var i = 0 ; i < data.data.length; i++){
//	                        // ...
//	                    }
//	                    return JSON.stringify( data );
				}
			},
			columns: [
				{ data: 'texto', title: 'Texto'},
				{ data: 'valor', title: 'Valor'},
				{ data: 'orden', title: 'Orden'},
				{ title: ''},
				// ...
			],
			columnDefs: [
					@php $counter = 0 @endphp
				{
					// texto,
					targets: {{ $counter++ }},
					render: function( data, type, row ) {
						return data;
					}
				},{
					// valor,
					targets: {{ $counter++ }},
					render: function( data, type, row ) {
						return data;
					}
				},{
					// orden,
					targets: {{ $counter++ }},
					render: function( data, type, row ) {
						return data;
					}
				}, {
					targets: {{ $counter++ }},
					className: 'text-center with-btn',
					width: '20px',
					render: function(data, type, row){
						var html = '';
						html += '<a href="javascript:;" onclick="listaItemEdit('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
						return html;
					}
				}
			],
			fixedHeader: {
				header: true,
				headerOffset: $('#header').height()
			},
			bSort : false,
			bAutoWidth: false,
			processing: true,
			serverSide: true,
			responsive: true,
			fixedColumns: true
		}).on('draw.dt', function(){
		});
	} else {
		dtOtherListaItems.ajax.reload();
	}
}

</script>
@endpush

