@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.listaItem.index') }}">Lista Item</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $listaItem->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Lista Item</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Lista Item</h4>
		</div>
		<div class="panel panel-body">
			<form id="formListaItemEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-4 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Texto</label>
					<div class="col-8">
						{!! Form::text('texto', $listaItem->texto, [
							'id' => 'texto',
							'placeholder' => 'Texto',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-4 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Valor</label>
					<div class="col-8">
						{!! Form::text('valor', $listaItem->valor, [
							'id' => 'valor',
							'placeholder' => 'Valor',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-4 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Orden</label>
					<div class="col-8">
						{!! Form::number('orden', $listaItem->orden, [
							'id' => 'orden',
							'placeholder' => 'Orden',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="listaItemCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="listaItemSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif

$( "#formListaItemEdit" ).submit(function( event ) {
	listaItemSave();
	event.preventDefault();
});

function listaItemSave(){
	var url = '{{ route('resource.listaItem.update', [$listaItem->id]) }}';
	ajaxPatch(url, {
		texto: 		$('#formListaItemEdit #texto').val(),
		valor: 		$('#formListaItemEdit #valor').val(),
		orden: 		$('#formListaItemEdit #orden').val()
	}, function(){
		$(document).trigger('formListaItemEdit:success');
	});
}

function listaItemCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formListaItemEdit:cancel');
	@else
		redirect('{{ route('admin.listaItem.index')  }}');
	@endif
}

function formInit(){



}

</script>
@endpush

