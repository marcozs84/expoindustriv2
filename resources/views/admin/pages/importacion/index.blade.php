@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />

<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}
	.datepicker.dropdown-menu {
		z-index:20;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.importacion.index') }}">Importacion</a></li>
		<li class="breadcrumb-item active">Registrar importacion</li>
	</ol>

	<h1 class="page-header">Importar maquinarias</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Importacion</h4>
		</div>
		<div class="panel panel-body">
			<div id="formImportacionCreate" class="form-horizontal">

				<div class="row">

					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Fecha de inicio anuncio</label>
						<div class="col-8">
							{!! Form::text('fechaInicioAnuncio', '', [
								'id' => 'fechaInicioAnuncio',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Fecha de finalización</label>
						<div class="col-8">
							{!! Form::text('fechaFinAnuncio', '', [
								'id' => 'fechaFinAnuncio',
								'placeholder' => '',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 col-sm-12 row m-b-15 group-cliente">
						<label class="col-form-label col-4">Empresa / Cliente</label>
						<div class="col-8">
							{!! Form::select('searchCliente_import', [], null, [
								'id' => 'searchCliente_import',
								'class' => 'form-control default-select2',
								'autofocus'
							]) !!}
							<a href="javascript:;" class="btn btn-primary" onclick="clienteCreate()" style="margin-top:5px;">Nuevo Cliente</a>
							<a href="javascript:;" class="btn btn-primary btnClienteEdit hide" onclick="clienteEdit()" style="margin-top:5px;">Editar Cliente</a>
						</div>
					</div>

					<div class="form-group col-md-6 col-sm-12 row m-b-15 group-cliente">
						<label class="col-form-label col-4">Idioma</label>
						<div class="col-8">
							{!! Form::select('idioma', [
							0 => 'Idioma',
							'es' => 'Español',
							'se' => 'Sueco',
							'en' => 'Inglés',
							], null, [
								'id' => 'idioma',
								'class' => 'form-control default-select2',
								'onchange' => 'idioma_change()',
								'autofocus'
							]) !!}
						</div>
					</div>

					<div class="col-md-12 hide p-b-10" id="pnlContacto">
						<div class="panel panel-info" style="margin-bottom:0px;">
							<div class="panel-body ">
								<table class="table table-condensed table-bordered tblProveedor">
									<tbody>
									<tr class="active">
										<th style="width:50%">Datos Contacto</th>
										<th style="width:50%">Datos Empresa</th>
									</tr>
									<tr>
										<td>
											<address>
												<strong class="pContacto"></strong><br />
												<a href="javascript:;" class="text-black"><span class="pCorreoContacto"></span></a>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos"></span>
											</address>
										</td>
										<td>
											<address>
												<strong class="pNombreProv">Twitter, Inc.</strong><br />
												<span class="pDireccion"></span>
												<br>
												<strong>Telefonos:</strong> <span class="pTelefonos"></span>
											</address>
											<strong>Otros contactos:</strong><br>
											<span class="pContactos"></span>
										</td>
									</tr>
									</tbody>
								</table>

							</div>
						</div>
					</div>

				</div>


				<h4>Datos del producto</h4>

				<div class="row">
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Estado</label>
						<div class="col-sm-8">
							{!! Form::select('esNuevo', [
								'' => 'No definido',
								1 => 'Nuevo',
								0 => 'Usado',
							], null, [
								'id' => 'esNuevo',
								'class' => 'form-control default-select2',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Tipo (maquinaria/accesorio)</label>
						<div class="col-sm-8">
							{!! Form::select('idProductoTipo', [
								'' => 'No definido',
								1 => 'Maquinaria',
								2 => 'Accesorio',
							], null, [
								'id' => 'idProductoTipo',
								'class' => 'form-control default-select2',
								//'onchange' => 'loadForm()',
							]) !!}
						</div>
					</div>


				</div>

				<h4>Ubicación</h4>

				<div class="row">
					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">País</label>
						<div class="col-sm-8">
							{!! Form::select('pais', $global_paises, null, [
								'id' => 'pais',
								'class' => 'form-control',
								'placeholder' => 'País',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Ciudad</label>
						<div class="col-sm-8">
							{!! Form::text('ciudad', '', [
								'id' => 'ciudad',
								'placeholder' => 'Ciudad',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Código postal</label>
						<div class="col-sm-8">
							{!! Form::text('codigo_postal', '', [
								'id' => 'codigo_postal',
								'placeholder' => 'Código postal',
								'class' => 'form-control',
							]) !!}
						</div>
					</div>

					<div class="form-group col-md-6 row m-b-15">
						<label class="col-form-label col-md-4">Direccion</label>
						<div class="col-sm-8">
							{!! Form::textarea('direccion', null, [
							'id' => 'direccion',
							'class' => 'form-control',
							'rows' => 4,
							'placeholder' => 'Direccion',
						]) !!}
						</div>
					</div>

				</div>

				<div class="form-group col-12 row m-b-15">
					<label class="col-form-label col-md-3">Datos para importar</label>
					<div class="col-md-12">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
						{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
						{{--<div class="form-group">--}}
						{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
						{{--</div>--}}
						{{--<div class="form-group">--}}
						{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
						{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.importacion.upload') }}" class="dropzone" id="dz_import_element">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="sesna" value="{{ rand(1,1000) }}">
							<div class="dz-message" data-dz-message><span>
									{{--@lang('messages.dz_upload')--}}
									Adjuntar datos
								</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor aguarde hasta que el archivo termine de subir.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>


				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-md-6 text-left">
						{{--<a class="btn btn-warning btn-sm" href="javascript:;" onclick="usuarioCreate()">Nuevo Cliente</a>--}}
					</div>
					<div class="col-md-6 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="importacionCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="importacionGuardar()" >Guardar</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<h4 class="panel-title">Elementos importados</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="anuncioCreate()"><i class="fa fa-plus"></i> Crear</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="anuncioDelete()"><i class="fa fa-trash"></i> Eliminar</a>
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script>

	@if($isAjaxRequest)
		formInit();
	@else
		$(function(){ formInit(); });
	@endif

	var datatable = null;
	var modal = null;
	var estados = {!! json_encode($publicacionEstados)  !!};
	var idUsuarioPropietario = 0;

	var failures = [];

	var dz_import = '';
	$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
		Dropzone.autoDiscover = false;
		setTimeout(function(){
			{{--$('div#dz_import_element').dropzone({--}}
				{{--url: '{{ route('admin.bannerSuperior.upload') }}'--}}
			{{--});--}}  // comented so it fixes the already attached dropzone

			dz_import = new Dropzone("#dz_import_element", {
				url: "{{ route('admin.importacion.upload') }}",
				addRemoveLinks: true,
				timeout: 180000,
				autoProcessQueue: false,
				parallelUploads: 1,
				init: function() {

					this.on("queuecomplete", function(file) {
						$(document).trigger('formImportacionCreate:aceptar');
						datatable.ajax.reload();
//						this.removeAllFiles();
					});
					this.on("complete", function(file) {
						file.previewElement.remove();
//						dz_import.removeFile(file);
					});
					this.on("removedfile", function(file) {
						//alert(file.name);
						console.log('Eliminado: ' + file.name);

						file.previewElement.remove();

						// Create the remove button
						var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

						//Capture the Dropzone instance as closure.
						var _this = this;

						// Listen to the click event
						//					removeButton.addEventListener();

						// Add the button to the file preview element.
						file.previewElement.appendChild(removeButton);
					});
					this.on("sending", function(file, xhr, formData) {
						var formImportacionCreate = $('#formImportacionCreate');
						formData.append('idUsuario',    idUsuarioPropietario );
						formData.append('esNuevo',      formImportacionCreate.find('#esNuevo').val() );
						formData.append('idProductoTipo',formImportacionCreate.find('#idProductoTipo').val() );
						formData.append('fechaInicio',  formImportacionCreate.find('#fechaInicioAnuncio').val() );
						formData.append('fechaFin',     formImportacionCreate.find('#fechaFinAnuncio').val()    );
						formData.append('idioma',       formImportacionCreate.find('#idioma').val()             );
						formData.append('pais',         formImportacionCreate.find('#pais').val()               );
						formData.append('ciudad',       formImportacionCreate.find('#ciudad').val()             );
						formData.append('cpostal',      formImportacionCreate.find('#codigo_postal').val()      );
						formData.append('direccion',    formImportacionCreate.find('#direccion').val()          );
//						console.log(formData)
					});
				},
				success: function(file, response){
					console.log("success: " + file.name);
//					console.log(response);
					if( parseInt(response.code, 10) === 422 ) {
						alert(response.error);
					} else {
						dz_import.processQueue();
					}

//					$(document).trigger('formImportacionCreate:aceptar');
				},
				error: function(file, response){
					console.log("failure: " + file.name);
					failures.push(file.name);
					console.log(response);
					if( parseInt(response.code, 10) === 422 ) {
						console.log(response.error);
						dz_import.processQueue();
					}

//					$(document).trigger('formImportacionCreate:aceptar');
				},
				removedfile: function(file){
					{{--x = confirm('@lang('messages.confirm_removal')');--}}
					{{--if(!x) return false;--}}
//					removeImage(1, file.name);
//					for(var i = 0 ; i < file_up_names.length; i++){
//						if(file_up_names[i] === file.name){
//							ajaxPost('public.publishRemove', {
//								'archivo' : ''
//							});
//						}
//					}
				}
			});
		}, 1000);

		$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
		$("[data-toggle=tooltip]").tooltip();

	});

	$( "#formImportacionCreate" ).submit(function( event ) {
		importacionGuardar();
		event.preventDefault();
	});

	function importacionGuardar(){

		var formImportacionCreate = $('#formImportacionCreate');
		if( formImportacionCreate.find('#searchCliente_import').val() === 0 || formImportacionCreate.find('#searchCliente_import').val() === undefined || formImportacionCreate.find('#searchCliente_import').val() === null ) {
			alert("Debe seleccionar un cliente");
			return false;
		}

		if( parseInt(formImportacionCreate.find('#idioma').val(), 10) === 0 ) {
			alert("Debe seleccionar un idioma");
			return false;
		}

//		if( formImportacionCreate.find('#pais').val() === '' || formImportacionCreate.find('#ciudad').val() === '' || formImportacionCreate.find('#codigo_postal').val() === '' || formImportacionCreate.find('#direccion').val() === '' ) {
//			alert("Debe rellenar la ubicación de la máquina.");
//			return false;
//		}

		if(dz_import.files.length === 0) {
			alert('Seleccione un archivo para importar.')
			return false;
//			$(document).trigger('formImportacionCreate:success');
		} else {
			dz_import.processQueue();
		}
	}

	function importacionCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formImportacionCreate:cancelar');
		@else
			redirect('{{ route('admin.importacion.index')  }}');
		@endif
	}

	function formInit(){

		initDataTable();

		var formImportacionCreate = $('#formImportacionCreate');

		formImportacionCreate.find('#fechaInicioAnuncio').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			todayHighlight: true,
		}).datepicker("setDate",'now').on('changeDate', function(){
//			buscar();
		});
		formImportacionCreate.find("#fechaInicioAnuncio").mask("99/99/9999");

		formImportacionCreate.find('#fechaFinAnuncio').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			todayHighlight: true,
		}).datepicker("setDate", moment().add(1, 'year').format('DD-MM-YYYY') ).on('changeDate', function(){
//			buscar();
		});
		formImportacionCreate.find("#fechaFinAnuncio").mask("99/99/9999");

		// ----------

		formImportacionCreate.find('#searchCliente_import').select2({
//        allowClear: true,
			placeholder: {
				id: "",
				placeholder: "Leave blank to ..."
			},
			ajax: {
{{--				url: '{{ route('resource.proveedorContacto.ds') }}',--}}
				url: '{{ route('resource.cliente.ds') }}',
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token' : '{!! csrf_token() !!}'
				},
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page,
						proveedor_data: 1 // Utilizado para que el 'ds' consultado retorne informacion más completa
					};
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function (data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: function(repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if (repo.loading) return repo.text;

				if(repo.usuario) {
					idUsuarioPropietario = repo.idUsuario;
				}

				var companyName = '';
				if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
					companyName = '- - -';
				} else {
					companyName = repo.usuario.owner.proveedor.nombre;
				}
				var otrosContactos = [];
//				if(repo.proveedor.proveedor_contactos && repo.proveedor.proveedor_contactos.length > 0) {
//					$.each(repo.proveedor.proveedor_contactos, function(k,v) {
//						if(v.id !== repo.id) {
//							otrosContactos.push(v.agenda.nombres_apellidos);
//						}
//					});
//				}

				var markup = "<div>" + repo.agenda.nombres_apellidos + '<br><b>' + companyName + "</b><br>" + otrosContactos.join('<br> ') + "</div>";
//				var markup = "<div><b>" + repo.nombre + '</b><br>' + repo.descripcion + "</div>";
				return markup;
			},
			templateSelection: function formatRepoSelection (repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if(repo.usuario){
//					var telefonos = [];
//					if( repo.telefonos ) {
//						for(var i = 0 ; i < repo.telefonos.length ; i++) {
//							telefonos.push(repo.telefonos[i].numero);
//						}
//						$('#formImportacionCreate #lblTelefonos').html(telefonos.join(', '));
//					}
//					$('#formImportacionCreate .btnProveedorEdit').attr('onclick', 'proveedorEdit('+ repo.id +')');
//					$('#formImportacionCreate .btnProveedorEdit').removeClass('hide');

					var companyName = '';
					if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
						companyName = '- - -';
					} else {
						companyName = repo.usuario.owner.proveedor.nombre;
					}

					$('#formImportacionCreate #nuevoCliente option[value="0"]').attr('selected', 'selected');

					// ----------

					$('#formImportacionCreate .pNombreProv').html(companyName);

					var otrosContactos = [];
					if(repo.usuario.owner.proveedor.proveedor_contactos && repo.usuario.owner.proveedor.proveedor_contactos.length > 0) {
						$.each(repo.usuario.owner.proveedor.proveedor_contactos, function(k,v) {
							if(parseInt(v.id, 10) !== parseInt(repo.id, 10)) {
								otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
							}
						});
					}

					var telefonos = [];
					if(repo.usuario.owner.proveedor.telefonos && repo.usuario.owner.proveedor.telefonos.length > 0) {
						$.each(repo.usuario.owner.proveedor.telefonos, function(k,v) {
							if(v.id !== repo.id) {
								telefonos.push(v.numero);
							}
						});
					}

					var direcciones = [];
					if(repo.usuario.owner.proveedor.direcciones && repo.usuario.owner.proveedor.direcciones.length > 0) {
						$.each(repo.usuario.owner.proveedor.direcciones, function(k,v) {
							if(v.id !== repo.id) {

								if( v.direccion == undefined ) {
									v.direccion = '';
								}

								direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
							}
						});
					}

					$('#formImportacionCreate .pContacto').html(repo.agenda.nombres_apellidos);
					$('#formImportacionCreate .pCorreoContacto').html(repo.usuario.username);
					$('#formImportacionCreate .pContactos').html(otrosContactos.join('<br> '));
					$('#formImportacionCreate .pCorreos').html(repo.usuario.owner.proveedor.correos);
					$('#formImportacionCreate .pTelefonos').html(telefonos.join(', '));
					$('#formImportacionCreate .pDireccion').html(direcciones.join('<br>'));

					$('#pnlContacto').removeClass('hide');

					// ----------

					return repo.agenda.nombres_apellidos + ' [' + companyName + ']';
				} else {
//					$('#formImportacionCreate .btnProveedorEdit').addClass('hide');
					return repo.text;
				}
				//return repo.nombre + ' ' + repo.apellidos || repo.text;
			},
//	    allowClear: true
		})
			.trigger('change');
	}

	function clienteCreate(){
		var url = '{{ route('admin.usuario.create', [ 'tipo' => 'cliente' ]) }}';
		var modal = openModal(url, 'Nuevo Cliente');
		setModalHandler('formUsuarioCreate:aceptar', function(event, data){
			var usuario = data;
			if(usuario.owner.proveedor.nombre === null) {
				usuario.owner.proveedor.nombre = '';
			}
			var companyName = '';
			if(usuario.owner.proveedor.nombre === '' || usuario.owner.proveedor.nombre === null) {
				companyName = '- - -';
			} else {
				companyName = usuario.owner.proveedor.nombre;
			}
			var option = $('<option>', {
				value: usuario.cliente.id,
				'selected' : 'selected'
			}).html(usuario.owner.agenda.nombres_apellidos + ' [' + companyName + ']');
			$('#formImportacionCreate #searchCliente_import').html('');
			$('#formImportacionCreate #searchCliente_import').append(option).trigger('change');

			$('#formImportacionCreate #nuevoCliente option[value="1"]').attr('selected', 'selected');

			$('#formImportacionCreate .pNombreProv').html(companyName);

			var otrosContactos = [];
			if(usuario.owner.proveedor.proveedor_contactos && usuario.owner.proveedor.proveedor_contactos.length > 0) {
				$.each(usuario.owner.proveedor.proveedor_contactos, function(k,v) {
					if(v.id !== usuario.id) {
						otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
					}
				});
			}

			var telefonos = [];
			if(usuario.owner.proveedor.telefonos && usuario.owner.proveedor.telefonos.length > 0) {
				$.each(usuario.owner.proveedor.telefonos, function(k,v) {
					if(v.id !== usuario.id) {
						telefonos.push(v.numero);
					}
				});
			}

			var direcciones = [];
			if(usuario.owner.proveedor.direcciones && usuario.owner.proveedor.direcciones.length > 0) {
				$.each(usuario.owner.proveedor.direcciones, function(k,v) {
					if(v.id !== usuario.id) {
						if( v.direccion != undefined ) {
							direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
						}
					}
				});
			}

			$('#formImportacionCreate').find('.pContacto').html(usuario.owner.agenda.nombres_apellidos);
			$('#formImportacionCreate').find('.pCorreoContacto').html(usuario.username);
			$('#formImportacionCreate').find('.pContactos').html(otrosContactos.join('<br> '));
			$('#formImportacionCreate').find('.pCorreos').html(usuario.owner.proveedor.correos);
			$('#formImportacionCreate').find('.pTelefonos').html(telefonos.join(', '));
			$('#formImportacionCreate').find('.pDireccion').html(direcciones.join('<br>'));

			$('#pnlContacto').removeClass('hide');

			dismissModal(modal);
		});
	}

	function idioma_change() {
		datatable.ajax.reload();
	}

	function productoEdit(id) {
		var url = '{{ route('admin.producto.edit', ['idProducto']) }}';
		url = url.replace('idProducto', id);
		var modal = openModal(url, 'Editar Producto', null, {
			'size' : 'modal-80'
		});
		setModalHandler('formProductoEdit:aceptar', function( event, data) {
		});
	}

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.anuncio.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.idTipoImportacion = 1;
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: '@lang('messages.product')'},
					{ title: 'Categoria'},
					{ title: 'Estado'},
					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
							var name = '';
							if(row.producto){
								if(row.producto.idMarca > 0){
									name += row.producto.marca.nombre;
									name += ' ';
								} else {
									{{--name += '<b>@lang('messages.no_brand')</b>';--}}
								}

								if(row.producto.idModelo > 0){
									name += row.producto.modelo.nombre;
								} else {
									{{--name += '<b>@lang('messages.no_model')</b>';--}}
								}

								if( name === '' ) {
									var formImportacionCreate = $('#formImportacionCreate');
									var selLang = formImportacionCreate.find('#idioma').val();
									if(selLang === '0') {
										selLang = 'se';
									}
									var trans = get_trans('nombre', row.producto.traducciones, selLang);
									return trans;
								}
//								return name;
							} else {
								name =  '<b class="text-red">@lang('messages.no_product')!</b>';
							}
							return '<a href="javascript:;" onclick="anuncioShow('+row.id+')">'+name+'</a>';
						}
					},{
						targets: 2,
						render: function(data, type, row){
							var response = '';
							if(row.producto !== null){
								if(row.producto.categorias !== undefined){
									$.each(row.producto.categorias, function(key, value){
										response += ''; //value.padre.nombre + ' / ' + value.nombre;
									});
									return response;
								} else {
									return 'Sin categorias';
								}
							} else {
								return '';
							}
						}
					},{
						targets:3,
						render: function(data, type, row){
							return estados[row.estado];
						}
					},{
						targets:4,
						render: function(data, type, row){
							var html = '';

							html += '<a href="javascript:;" onclick="productoEdit('+ row.producto.id +')"><i class="fa fa-edit"></i></a>';
							return html;
						}
					}, {
						targets: 5,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}


</script>
@endpush

