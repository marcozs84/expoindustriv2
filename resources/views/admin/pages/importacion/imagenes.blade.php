@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', ' Imagenes')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<style>
	.imgimport img {
		max-width:100px;
	}
</style>
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"> Imagenes</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"> Imagenes <small></small></h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Filtros</h4>
		</div>
		<div id="formSearchOrden" class="panel-body p-b-0">
			<div class="row" id="searchForm">

				<div class="form-group col-lg-4 col-md-4 col-sm-6" style="vertical-align:top;">
					<label for="estados" style="">Estado</label>
					{!! Form::select('estados', [
						\App\Models\Imagen::ESTADO_ACTIVO => 'Activo',
						\App\Models\Imagen::ESTADO_PENDIENTE => 'Pendiente de importación',
						\App\Models\Imagen::ESTADO_FALLIDO => 'Importacion Fallida',
						\App\Models\Imagen::ESTADO_FALLIDO_RESIZE => 'Redimensión fallida',
					], [\App\Models\Imagen::ESTADO_PENDIENTE], [
						'id' => 'estados',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
						]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="imagenCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="imagenDelete()"><i class="fa fa-trash"></i> Eliminar</a>
					<a class="btn btn-info btn-sm" href="javascript:;" onclick="play_import()"><i class="fa fa-play"></i> Auto Import</a>
					<a class="btn btn-danger btn-sm" href="javascript:;" onclick="stop_import()"><i class="fa fa-stop"></i> Detener</a>

					{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:buscar();"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:limpiar();">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title"> Imagenes</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>--}}
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	var img_ids = [];
	var autoimport = false;

	$(document).ready(function(){
		initDataTable();
		initSearchForm();
	});

	function initSearchForm() {
		$('#estados').select2()
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});
	}

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.imagen.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						// d.element = $('#element').val();
						d.estado = $('#estados').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						var dataJson = JSON.parse(data);

						img_ids = [];
						$.each(dataJson.data, function(k, v) {
							img_ids.push( v.id );
						});

						console.log(img_ids);

						return data;
//						var data = JSON.parse(data);
//						for(var i = 0 ; i < data.data.length; i++){
//							// ...
//						}
//						return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
					{ data: 'idGaleria', title: 'IdGaleria'},
//					{ data: 'titulo', title: 'Titulo'},
//					{ data: 'ruta', title: 'Ruta'},
					{ data: 'filename', title: 'Filename'},
//					{ data: 'mimetype', title: 'Mimetype'},
					{ title: 'Preview'},
//					{ data: 'backgroundColor', title: 'BackgroundColor'},
					{ data: 'estado', title: 'Estado'},
					{ data: 'fuente', title: 'Fuente'},
					{ data: 'created_at', title: 'Created_at'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
						@php $counter = 0 @endphp
					{
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '30px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="imagenShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// idGaleria,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
						{{--},{--}}
						{{--// titulo,--}}
						{{--targets: {{ $counter++ }},--}}
						{{--render: function( data, type, row ) {--}}
						{{--return data;--}}
						{{--}--}}
						{{--},{--}}
						{{--// ruta,--}}
						{{--targets: {{ $counter++ }},--}}
						{{--render: function( data, type, row ) {--}}
						{{--return data;--}}
						{{--}--}}
					},{
						// filename,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return '<a href="javascript:;" data-tooltip="'+ row.ruta +'" title="'+ row.ruta +'">'+ data +'</a>';
						}
					{{--},{--}}
						{{--// mimetype,--}}
						{{--targets: {{ $counter++ }},--}}
						{{--render: function( data, type, row ) {--}}
							{{--return data;--}}
						{{--}--}}
					},{
						// preview,
						targets: {{ $counter++ }},
						className: 'text-center ',
						render: function( data, type, row ) {
							var img = '';
							if( parseInt(row.estado, 10) === 1) {
								img = '<img src="'+ row.ruta_publica_producto_thumb +'">';
							}
							return '<div class="imgimport imgimport_'+ row.id +'">'+ img +'</div>';
						}
					{{--},{--}}
						{{--// backgroundColor,--}}
						{{--targets: {{ $counter++ }},--}}
						{{--className: 'text-center ',--}}
						{{--render: function( data, type, row ) {--}}
							{{--return data;--}}
						{{--}--}}
					},{
						// estado,
						targets: {{ $counter++ }},
						className: 'text-center ',
						render: function( data, type, row ) {
							return data;
						}
					},{
						// fuente,
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '100px',
						render: function( data, type, row ) {
							var html = '';
							html = '<a class="btn btn-xs btn-primary" href="'+ row.fuente +'" target="_blank" data-tooltip="'+ row.fuente +'" title="'+ row.fuente +'"><i class="fa fa-link"></i></a> ';
							html += '<a class="btn btn-xs btn-danger btn_import_'+ row.id +'" href="javascript:;" data-tooltip="'+ row.fuente +'" title="Importar desde: '+ row.fuente +'" onclick="img_importar('+ row.id +')"><i class="fa fa-cloud-download-alt"></i></a>';
							return html;
						}
					},{
						// created_at,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '100px',
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';
							html += '<a href="javascript:;" onclick="imagenEdit('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
							return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){

				if( autoimport ) {
					autoimport_exec();
				}

				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function imagenShow(id){
		var url = '{{ route('admin.imagen.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Imagen', null, {
			'size': 'modal-lg'
		});
	}

	function imagenCreate(){
		var url = '{{ route('admin.imagen.create') }}';
		var modal = openModal(url, 'Nuevo Imagen', null, { size:'modal-lg' });
		setModalHandler('formImagenCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function imagenEdit(id){
		var url = '{{ route('admin.imagen.edit', 'idRecord') }}';
		url = url.replace('idRecord', id);
		var modal = openModal(url, 'Editar Imagen', null, { size:'modal-lg' });
		setModalHandler('formImagenEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function imagenDelete( id ){
		var ids = [];

		if ( id !== undefined ) {
			ids = [ id ];
		} else {
			$('.chkId').each(function() {
				if($(this).prop('checked')){
					ids.push($(this).val());
				}
			});
			if(ids.length === 0){
				alert("Debe seleccionar al menos 1 registro.");
				return false;
			}
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.imagen.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

	function img_importar( id ) {

		console.log("Importando: " + id);

		var url = '{{ route('admin.importacion.imp_imagen') }}';

		console.log('.btn_import_' + id);
		var btn_import = $('.btn_import_' + id);

		$('html, body').animate({
			scrollTop: btn_import.offset().top - 100
		}, 1000);

		var old_onclick = btn_import.attr('onclick');
		var old_html = btn_import.html();
		btn_import.attr('onclick', '');
		btn_import.html('<i class="fa fa-spin fa-circle-notch"></i>');

		ajaxPost(url, {
			'id_imagen': id
		}, function( response ) {
			if( response.result === 1 ) {
				var img = $('<img>', {
					'src': response.data.ruta_publica_producto_thumb
				});
				$('.imgimport_' + id).append(img);
				btn_import.html( '<i class="fa fa-check"></i>' );
				btn_import.removeClass( 'btn-danger' );
				btn_import.addClass( 'btn-primary' );

				if( autoimport ) {
					index_import++;
					autoimport_exec();
//					setTimeout( function() {
//						autoimport_exec();
//					}, 1000);

				}
			} else {
				$( '.imgimport_' + id ).parent().append( 'Failure: ' + response.message );
				btn_import.attr( 'onclick', old_onclick );
				btn_import.html( old_html );
			}
		}, function(date) {
			autoimport = false;
			alert("Importacion fallida: " + id);
		}, {
			onDone: function( response ) {
				btn_import.attr( 'onclick', old_onclick );
				btn_import.html( old_html );
			}
		});
	}

	var index_import = 0;
	function play_import() {
		autoimport = true;
		autoimport_exec();
	}
	function stop_import() {
		autoimport = false;
	}
	function autoimport_exec() {

		if( index_import === img_ids.length ) {
			index_import = 0;
			img_ids = [];
			datatable.ajax.reload();
			return true;
//			table.page( 'next' ).draw( 'page' );
		}

		if( !img_ids[ index_import ] ) {
			alert(" no hay mas elementos " + index_import);
			return false;
		}

		img_importar( img_ids[ index_import ] );
	}

</script>
@endpush