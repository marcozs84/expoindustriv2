@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Anuncios</li>
	</ol>

	<h1 class="page-header">Mapa de los formularios</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Anuncio</h4>
		</div>
		<div class="panel panel-body">
			<form id="formAnuncioCreate" method="get" class="form-horizontal">

				@foreach($formularios as $form)
					<br>
					<h5 style="font-size:18px;">{{ $form->Categoria->nombre }}</h5>
					<table class="table table-condensed table-striped">
						@foreach($form->campos as $campo)
							@if($campo['type'] == 'titulo1')
								<tr>
									<td style="font-size:14px;"><b>{{ $campo['text'] }}</b></td>
									<td></td>
								</tr>
							@endif
							@if($campo['type'] == 'titulo2')
								<tr>
									<td><b>{{ $campo['text'] }}</b></td>
									<td></td>
								</tr>
							@endif
							@if(!in_array($campo['type'], ['titulo1', 'titulo2']))
								<tr>
									<td>{{ $campo['text'] }}</td>
									<td>{{ $campo['key'] }}</td>
									<td>
										@if($campo['type'] == 'input')
											Texto
										@elseif($campo['type'] == 'text')
											Text
										@elseif($campo['type'] == 'multiline')
											Multilinea
										@elseif($campo['type'] == 'boolean')
											Boolean ( Si | No )
										@else
											{{ $campo['type'] }}
										@endif

									</td>
								</tr>
							@endif
						@endforeach
					</table>
				@endforeach
			</form>
		</div>
	</div>

@stop

@section('libraries')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

@stop

@section('scripts')
	<script>

		function formInit(){
		}
		formInit();

	</script>
@stop

