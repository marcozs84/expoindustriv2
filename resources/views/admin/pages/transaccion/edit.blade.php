@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.transaccion.index') }}">Transaccion</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $transaccion->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Transaccion #{{ $transaccion->id }}</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Transaccion #{{ $transaccion->id }}</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTransaccionEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', $transaccion->owner_type, [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', $transaccion->owner_id, [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Transaccion Tipo</label>
					<div class="col-8">
						{!! Form::number('idTransaccionTipo', $transaccion->idTransaccionTipo, [
							'id' => 'idTransaccionTipo',
							'placeholder' => 'Id Transaccion Tipo',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Usuario</label>
					<div class="col-8">
						{!! Form::number('idUsuario', $transaccion->idUsuario, [
							'id' => 'idUsuario',
							'placeholder' => 'Id Usuario',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Stripe Customer</label>
					<div class="col-8">
						{!! Form::text('idStripeCustomer', $transaccion->idStripeCustomer, [
							'id' => 'idStripeCustomer',
							'placeholder' => 'Id Stripe Customer',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Stripe Transaction</label>
					<div class="col-8">
						{!! Form::text('idStripeTransaction', $transaccion->idStripeTransaction, [
							'id' => 'idStripeTransaction',
							'placeholder' => 'Id Stripe Transaction',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Producto</label>
					<div class="col-8">
						{!! Form::text('producto', $transaccion->producto, [
							'id' => 'producto',
							'placeholder' => 'Producto',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Monto</label>
					<div class="col-8">
						{!! Form::text('monto', $transaccion->monto, [
							'id' => 'monto',
							'placeholder' => 'Monto',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Moneda</label>
					<div class="col-8">
						{!! Form::text('moneda', $transaccion->moneda, [
							'id' => 'moneda',
							'placeholder' => 'Moneda',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::text('estado', $transaccion->estado, [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Extrainfo</label>
                    <div class="col-8">
                        {!! Form::textarea('extrainfo', $transaccion->extrainfo, [
                            'id' => 'extrainfo',
                            'placeholder' => 'Extrainfo',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{!! Form::text('created_at', ($transaccion->created_at ? $transaccion->created_at->format('d/m/Y') : ''), [
							'id' => 'created_at',
							'placeholder' => 'Created_at',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transaccionCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="transaccionSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

$( "#formTransaccionEdit" ).submit(function( event ) {
	transaccionSave();
	event.preventDefault();
});

function transaccionSave(){
	var url = '{{ route('resource.transaccion.update', [$transaccion->id]) }}';
	ajaxPatch(url, {
		owner_type: 	$('#formTransaccionEdit #owner_type').val(),
		owner_id: 		$('#formTransaccionEdit #owner_id').val(),
		idTransaccionTipo: $('#formTransaccionEdit #idTransaccionTipo').val(),
		idUsuario: 	$('#formTransaccionEdit #idUsuario').val(),
		idStripeCustomer: $('#formTransaccionEdit #idStripeCustomer').val(),
		idStripeTransaction: $('#formTransaccionEdit #idStripeTransaction').val(),
		producto: 		$('#formTransaccionEdit #producto').val(),
		monto: 		$('#formTransaccionEdit #monto').val(),
		moneda: 		$('#formTransaccionEdit #moneda').val(),
		estado: 		$('#formTransaccionEdit #estado').val(),
		extrainfo: 	$('#formTransaccionEdit #extrainfo').val(),
		created_at: 	$('#formTransaccionEdit #created_at').val()
	}, function(){
		$(document).trigger('formTransaccionEdit:success');
	});
}

function transaccionCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTransaccionEdit:cancel');
	@else
		redirect('{{ route('admin.transaccion.index')  }}');
	@endif
}

function formInit(){

	$('#formTransaccionEdit #created_at').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		todayHighlight: true,
		orientation: 'bottom'
	})
		.on('changeDate', function(){
		});
	$("#formTransaccionEdit #created_at").mask("99/99/9999");

}

</script>
@endpush

