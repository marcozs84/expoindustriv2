@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />

<style>
	.label-show {
		padding-top:7px !important;
	}
</style>
<style>
	.files-holder {
		background-color:#d9e0e7;
		padding:15px;
		margin:5px;

		-ms-word-break: break-all;
		word-break: break-all;
		word-break: break-word;
	}
	.fileIcon {
		text-align:center;
		font-size:11px;

	}
	.fileIcon img {
		max-width:180px;
		max-height:80px;

	}
	.fileIcon a {
		/*font-weight:bold;*/
		/*padding: 5px 10px;*/
		/*text-align: center;*/
		/*white-space: nowrap;*/
		/*overflow: hidden;*/
		/*text-overflow: ellipsis;*/
		/*background: #f1f3f4;*/
	}
	.fileIcon i {
		margin:auto;
		margin-bottom:10px;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.transaccion.index') }}">Transaccion</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $transaccion->id }}</li>
	</ol>

	<h1 class="page-header">Procesando Transaccion #{{ $transaccion->id }}</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Procesando Transaccion #{{ $transaccion->id }}</h4>
		</div>
		<div class="panel panel-body">
			<div id="formTransaccionEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Propietario</label>
					<div class="col-8 label-show">
						{{ $transaccion->owner_type }} ({{ $transaccion->owner_id }})
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Tipo Transaccion</label>
					<div class="col-8 label-show">
						@if($transaccion->TransaccionTipo)
							{{ $transaccion->TransaccionTipo->texto }}
						@endif
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Usuario</label>
					<div class="col-8 label-show">

						<a href="javascript:;" onclick="clienteShow({{ $transaccion->Usuario->Owner->id }})">{{ $transaccion->Usuario->Owner->Agenda->nombres_apellidos }}  <b>&lt;{{ $transaccion->Usuario->username }}&gt;</b></a>
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Stripe Customer</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::text('idStripeCustomer', $transaccion->idStripeCustomer, [--}}
							{{--'id' => 'idStripeCustomer',--}}
							{{--'placeholder' => 'Id Stripe Customer',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Stripe Transaction</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::text('idStripeTransaction', $transaccion->idStripeTransaction, [--}}
							{{--'id' => 'idStripeTransaction',--}}
							{{--'placeholder' => 'Id Stripe Transaction',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Producto</label>
					<div class="col-8 label-show">
						{{ $transaccion->producto }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Monto</label>
					<div class="col-8 label-show">
						{{ $transaccion->monto }} {{ $transaccion->moneda }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8 label-show">
						@if($transaccion->TransaccionEstado)
							{{ $transaccion->TransaccionEstado->texto }}
						@endif

					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Extrainfo</label>--}}
                    {{--<div class="col-8">--}}
                        {{--{!! Form::textarea('extrainfo', $transaccion->extrainfo, [--}}
                            {{--'id' => 'extrainfo',--}}
                            {{--'placeholder' => 'Extrainfo',--}}
                            {{--'class' => 'form-control',--}}
                            {{--'rows' => 6--}}
                        {{--]) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Procesada</label>
					<div class="col-8">
						{!! Form::select('procesada', [
							1 => 'Si',
							0 => 'No'
						], $transaccion->procesada, [
							'id' => 'procesada',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha de creacion</label>
					<div class="col-8 label-show">
						{{ ($transaccion->created_at ? $transaccion->created_at->format('d/m/Y') : '') }}
					</div>
				</div>

				<h4 class="col-12">Archivos adjuntos</h4>

				<div id="gallery" class="gallery" style="margin-left:10px;">
					<ul class="attached-document clearfix">
						@foreach($transaccion->Archivos as $archivo)
							<li @if(in_array(pathinfo($archivo->ruta, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'png', 'gif'])) class="fa-camera" @endif>
								<div class="document-file">

										@if(in_array(pathinfo($archivo->ruta, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'png', 'gif']))
										<a href="{{ route('resource.archivo.stream', [$archivo->id]) }}" data-lightbox="gallery-group-1">
											<img src="{{ route('resource.archivo.stream', [$archivo->id]) }}" alt="" />
										</a>
										@elseif(pathinfo($archivo->ruta, PATHINFO_EXTENSION) == 'pdf')
										<a href="{{ route('resource.archivo.stream', [$archivo->id]) }}" target="_blank">
											<i class="fa fa-file-pdf fa-4x" style="font-size:40px;"></i>
										</a>
										@else
										<a href="{{ route('resource.archivo.stream', [$archivo->id]) }}" target="_blank">
											<i class="fa fa-file-alt fa-4x" style="font-size:40px;"></i>
										</a>
										@endif

								</div>
								<div class="document-name"><a target="_blank" href="{{ route('resource.archivo.stream', [$archivo->id]) }}" data-lightbox="gallery-group-{{ $archivo->id }}">{{ $archivo->realname }}</a></div>
							</li>
						@endforeach

					</ul>
				</div>

				{{--<div class="col-md-12">--}}
					{{--<div class="row files-holder">--}}
						{{--@foreach($transaccion->Archivos as $archivo)--}}

							{{--<div class="col-md-2 fileIcon">--}}
								{{--@if(pathinfo($archivo->ruta, PATHINFO_EXTENSION) == 'pdf')--}}
									{{--<i class="fa fa-file-pdf fa-4x"></i>--}}
								{{--@elseif(in_array(pathinfo($archivo->ruta, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'png', 'gif']))--}}
									{{--<img src="{{ route('resource.archivo.stream', [$archivo->id]) }}" alt="" />--}}
								{{--@else--}}
									{{--<i class="fa fa-file-alt fa-4x"></i>--}}
								{{--@endif--}}
								{{--<br>--}}
								{{--<a target="_blank" href="{{ route('resource.archivo.stream', [$archivo->id]) }}">{{ $archivo->realname }}</a>--}}
							{{--</div>--}}
						{{--@endforeach--}}
					{{--</div>--}}
				{{--</div>--}}

				<div class="form-group col-12 m-b-15">
					<label class="col-form-label " style="text-align:left;">Fotografía / Captura</label>
					<div class="col-md-12">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
						{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
						{{--<div class="form-group">--}}
						{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
						{{--</div>--}}
						{{--<div class="form-group">--}}
						{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
						{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.uploader.upload') }}" class="dropzone" style="width:100%;">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="sesna" value="{{ rand(1,1000) }}">
							<input type="hidden" name="object" id="object" value="Transaccion">
							<input type="hidden" name="object_id" id="object_id" value="{{ $transaccion->id }}">
							<div class="dz-message" data-dz-message><span>
							{{--@lang('messages.dz_upload')--}}
									Adjuntar comprobante
						</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor aguarde hasta que el archivo termine de subir.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>


			</div>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transaccionCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="transaccionSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

var myDropzone = '';
$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
	Dropzone.autoDiscover = false;
	setTimeout(function(){
		{{--$('div#dropzone').dropzone({--}}
		{{--url: '{{ route('admin.bannerSuperior.upload') }}'--}}
		{{--});--}}  // comented so it fixes the already attached dropzone

		myDropzone = new Dropzone(".dropzone", {
			url: "{{ route('admin.uploader.upload') }}",
			addRemoveLinks: true,
			autoProcessQueue: true,
			parallelUploads: 1,
			init: function() {

				this.on("queuecomplete", function(file) {
					$(document).trigger('formTransaccionProcess:success');
				});
				this.on("removedfile", function(file) {
					//alert(file.name);
					console.log('Eliminado: ' + file.name);

					file.previewElement.remove();

					// Create the remove button
					var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

					//Capture the Dropzone instance as closure.
					var _this = this;

					// Listen to the click event
					//					removeButton.addEventListener();

					// Add the button to the file preview element.
					file.previewElement.appendChild(removeButton);
				});
			},
			success: function(file, response){
				console.log("success");
				console.log(file.name);
				console.log(response);
				myDropzone.processQueue();
//					$(document).trigger('formTransaccionProcess:success');
			},
			removedfile: function(file){
				x = confirm('@lang('messages.confirm_removal')');
				if(!x) return false;
			}
		});
	}, 1000);

	$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
	$("[data-toggle=tooltip]").tooltip();

});


$( "#formTransaccionEdit" ).submit(function( event ) {
	transaccionSave();
	event.preventDefault();
});

function transaccionSave(){
	var url = '{{ route('resource.transaccion.update', [$transaccion->id]) }}';
	ajaxPatch(url, {
		procesada: 		$('#formTransaccionEdit #procesada').val(),
	}, function(){
		$(document).trigger('formTransaccionProcess:success');
	});
}

function transaccionCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTransaccionProcess:cancel');
	@else
		redirect('{{ route('admin.transaccion.index')  }}');
	@endif
}

function formInit(){

	// ----------

	$('#formTransaccionEdit #created_at').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		todayHighlight: true,
		orientation: 'bottom'
	})
		.on('changeDate', function(){
		});
	$("#formTransaccionEdit #created_at").mask("99/99/9999");

}

function clienteShow(id){
	var url = '{{ route('admin.agenda.show', ['idProveedorContacto']) }}';
	url = url.replace('idProveedorContacto', id);
	var modal = openModal(url, 'Tarjeta de identificación', null, {
		'size': 'modal-80'
	});
}

</script>
@endpush

