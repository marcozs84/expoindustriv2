@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Transaccion</li>
	</ol>

	<h1 class="page-header">Vista previa transaccion #{{ $transaccion->id }}</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Vista previa transaccion #{{ $transaccion->id }}</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTransaccionShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $transaccion->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{{ $transaccion->owner_type }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{{ $transaccion->owner_id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Transaccion Tipo</label>
					<div class="col-8">
						{{ $transaccion->idTransaccionTipo }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Usuario</label>
					<div class="col-8">
						{{ $transaccion->idUsuario }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Stripe Customer</label>
					<div class="col-8">
						{{ $transaccion->idStripeCustomer }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Stripe Transaction</label>
					<div class="col-8">
						{{ $transaccion->idStripeTransaction }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Producto</label>
					<div class="col-8">
						{{ $transaccion->producto }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Monto</label>
					<div class="col-8">
						{{ $transaccion->monto }} {{ strtoupper($transaccion->moneda) }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Impuesto</label>
					<div class="col-8">
						{{ $transaccion->impuesto }} {{ strtoupper($transaccion->moneda) }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Total</label>
					<div class="col-8">
						<strong>{{ ( $transaccion->monto + $transaccion->impuesto ) }} {{ strtoupper($transaccion->moneda) }}</strong>
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Moneda</label>
					<div class="col-8">
						{{ $transaccion->moneda }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{{ $transaccion->estado }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Extrainfo</label>
                    <div class="col-8">
                       {!! $transaccion->extrainfo !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{{ ($transaccion->created_at ? $transaccion->created_at->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-danger btn-sm" data-dismiss="modal" href="javascript:;" onclick="transaccionEdit({{ $transaccion->id }})">Editar</a>
					<a class="btn btn-primary btn-sm" data-dismiss="modal" href="javascript:;" onclick="transaccionProcess({{ $transaccion->id }})"><i class="fa fa-upload"></i>  Procesar</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transaccionCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif


function transaccionProcess(id){
	var url = '{{ route('admin.transaccion.process', 'idRecord') }}';
	url = url.replace('idRecord', id);
	var modal = openModal(url, 'Procesar Transaccion', null, { size:'modal-lg' });
	setModalHandler('formTransaccionProcess:success', function(){
//			dismissModal(modal);
		datatable.ajax.reload(null, false);
	});
}

function transaccionCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTransaccionShow:cancel');
	@else
		redirect('{{ route('admin.transaccion.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

