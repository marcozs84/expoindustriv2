@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', ' Transaccions')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"> Transacciones</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"> Transacciones <small></small></h1>
	<!-- end page-header -->

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title"> Transacciones</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="transaccionCreate()"><i class="fa fa-plus"></i> Crear</a>--}}
				{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="transaccionDelete()"><i class="fa fa-trash"></i> Eliminar</a>--}}
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	$(document).ready(function(){
		initDataTable();
	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.transaccion.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						// d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//						var data = JSON.parse(data);
//						for(var i = 0 ; i < data.data.length; i++){
//							// ...
//						}
//						return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
					{ data: 'idTransaccionTipo', title: 'Tipo'},
					{ title: 'Propietario'},
					{ data: 'producto', title: 'Producto'},
					{ data: 'monto', title: 'Monto'},
					{ data: 'impuesto', title: 'Impuesto'},
					{ title: 'Total'},
					{ data: 'procesada', title: 'Procesada'},
					{ data: 'estado', title: 'Estado'},
					{ data: 'created_at', title: 'Fecha'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					@php $counter = 0 @endphp
					{
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '30px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="transaccionShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// idTransaccionTipo,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							if( row.transaccion_tipo ) {
								return row.transaccion_tipo.texto;
							}
							return data;
						}
					},{
						// idUsuario,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="clienteShow(' + row.usuario.owner.id + ')">'+row.usuario.owner.agenda.nombres_apellidos+'</a>'
//							return data;
						}
					},{
						// producto,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// monto,
						targets: {{ $counter++ }},
						className: 'text-right',
						render: function( data, type, row ) {
							return data + ' ' + row.moneda.toUpperCase();
						}
					},{
						// impuesto,
						targets: {{ $counter++ }},
						className: 'text-right',
						render: function( data, type, row ) {
							if( data !== null ) {
								return data + ' ' + row.moneda.toUpperCase();
							}
							return '';
						}
					},{
						// total,
						targets: {{ $counter++ }},
						className: 'text-right',
						width: '80px',
						render: function( data, type, row ) {
							if( row.impuesto === null ) {
								row.impuesto = 0;
							}
							return '<strong>'+ ( parseFloat(row.monto) + parseFloat(row.impuesto) ) + ' ' + row.moneda.toUpperCase() +'</strong>';
						}
					},{
						// procesada,
						targets: {{ $counter++ }},
						className: 'text-center',
						render: function( data, type, row ) {
							var html = '';
							if (data === 1 ) {
								html = '<i class="fa fa-check-circle text-primary"></i>';
							} else {
								html = '<i class="fa fa-times"></i>';
							}
							return html;
						}
					},{
						// estado,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							if( row.transaccion_estado ) {
								return row.transaccion_estado.texto;
							}
							return data;
						}
					},{
						// created_at,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '100px',
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';

							html += '<div class="btn-group">'+
								'<a href="javascript:;" onclick="transaccionProcess('+ row.id +')" class="btn btn-primary btn-sm width-90 text-white"><i class="fa fa-upload"></i> Procesar</a>'+
								'<a href="javascript:;" class="btn btn-info btn-sm dropdown-toggle width-30 no-caret" data-toggle="dropdown"><span class="caret"></span></a>'+
								'<div class="dropdown-menu dropdown-menu-right">'+
								'<a href="javascript:;" onclick="transaccionEdit('+ row.id +')" class="dropdown-item" data-toggle="tooltip" data-title="Editar"><i class="fa fa-edit"></i> Editar</a>'+
								'</div>'+
								'</div>';

							return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function transaccionShow(id){
		var url = '{{ route('admin.transaccion.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Transaccion', null, {
			'size': 'modal-lg'
		});
	}

	function transaccionCreate(){
		var url = '{{ route('admin.transaccion.create') }}';
		var modal = openModal(url, 'Nuevo Transaccion', null, { size:'modal-lg' });
		setModalHandler('formTransaccionCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function transaccionEdit(id){
		var url = '{{ route('admin.transaccion.edit', 'idRecord') }}';
		url = url.replace('idRecord', id);
		var modal = openModal(url, 'Editar Transaccion', null, { size:'modal-lg' });
		setModalHandler('formTransaccionEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function transaccionProcess(id){
		var url = '{{ route('admin.transaccion.process', 'idRecord') }}';
		url = url.replace('idRecord', id);
		var modal = openModal(url, 'Procesar Transaccion', null, { size:'modal-lg' });
		setModalHandler('formTransaccionProcess:success', function(){
//			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function transaccionDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.transaccion.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

	function clienteShow(id){
		var url = '{{ route('admin.agenda.show', ['idProveedorContacto']) }}';
		url = url.replace('idProveedorContacto', id);
		var modal = openModal(url, 'Tarjeta de identificación', null, {
			'size': 'modal-80'
		});
	}

</script>
@endpush