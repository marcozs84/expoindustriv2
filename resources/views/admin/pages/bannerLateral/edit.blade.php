@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="{{ route('admin.home') }}">Admin</a></li>
		<li class="{{ route('admin.banner.index') }}">Banners</li>
	</ol>

	<h1 class="page-header">Editar Banner</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Banner</h4>
		</div>
		<div class="panel panel-body">
			<form id="formAnuncioCreate" method="get" class="form-horizontal">

				<h4>Informacion del banner</h4>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Titulo</label>
					<div class="col-md-9">
						{!! Form::text('bannerTitulo', $modelo->titulo, [
							'id' => 'bannerTitulo',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Tipo</label>
					<div class="col-md-9">
						{!! Form::select('bannerTipo', [
							'1' => 'Imagen',
							'2' => 'Producto',
						], $modelo->idBannerTipo, [
							'id' => 'bannerTipo',
							'placeholder' => 'Tipo (Imagen o producto)',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Ubicacion</label>
					<div class="col-md-9">
						{!! Form::select('bannerUbicacion', [
							'hi1' => 'Home: Izquierdo, debajo de buscador',
							'bd1' => 'Buscar: columna derecha',
							'bd2' => 'Buscar: columna derecha',
							'bd3' => 'Buscar: columna derecha',
						], $modelo->ubicacion, [
							'id' => 'bannerUbicacion',
							'placeholder' => 'Ubicacion',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Descripcion</label>
					<div class="col-md-9">
						{!! Form::textarea('bannerDescripcion', $modelo->descripcion, [
							'id' => 'bannerDescripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>


				{{--<div class="form-group row m-b-15">--}}
				{{--<label class="col-md-3 col-form-label">Periodo de vigencia</label>--}}
				{{--<div class="col-md-9">--}}
				{{--<div class="input-group date" id="bannerPeriodo_group">--}}
				{{--{!! Form::text('bannerPeriodo', '', [--}}
				{{--'id' => 'bannerPeriodo',--}}
				{{--'placeholder' => 'dd/mm/yyyy al dd/mm/yyyy',--}}
				{{--'class' => 'form-control'--}}
				{{--]) !!}--}}
				{{--<span class="input-group-append">--}}
				{{--<span class="input-group-text"><i class="fa fa-calendar"></i></span>--}}
				{{--</span>--}}
				{{--</div>--}}
				{{--</div>--}}
				{{--</div>--}}

				<div class="form-group row m-b-15">
					<label class="col-md-3 col-form-label">Periodo de vigencia</label>
					<div class="col-md-9">
						<div id="bannerPeriodo" class="btn btn-default btn-block text-left f-s-12">
							<i class="fa fa-caret-down pull-right m-t-2"></i>
							<span></span>
						</div>
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-md-3 col-form-label">Activo</label>
					<div class="col-md-9">
						{!! Form::checkbox('bannerEstado', $modelo->estado, [
							'id' => 'bannerEstado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<h4>Contenido</h4>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Fuente</label>
					<div class="col-md-9">
						<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >
							<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>
							<div class="form-group">
								<input type="file" class="form-control" name="archivo" id="archivo">
							</div>
							<div class="form-group">
								<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>
							</div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor aguarde hasta que el archivo termine de subir.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Texto</label>
					<div class="col-md-9">
						{!! Form::textarea('bannerTexto', $modelo->texto, [
							'id' => 'bannerTexto',
							'placeholder' => 'Texto',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Precio</label>
					<div class="col-md-9">
						{!! Form::textarea('bannerPrecio', $modelo->precio, [
							'id' => 'bannerPrecio',
							'placeholder' => 'Precio',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Enlace</label>
					<div class="col-md-9">
						{!! Form::textarea('bannerEnlace', $modelo->enlace, [
							'id' => 'bannerEnlace',
							'placeholder' => 'Enlace',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>

				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="anuncioCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="anuncioSave()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
	$( "#formAnuncioCreate" ).submit(function( event ) {
		anuncioSave();
		event.preventDefault();
	});

	var fechaInicio = '{{ $modelo->fechaInicio->format('d/m/Y') }}';
	var fechaFin = '{{ $modelo->fechaFin->format('d/m/Y') }}';

	function anuncioSave(){
		var url = '{{ route('resource.anuncio.update', [$modelo->id]) }}';
		ajaxPost(url, {
			titulo: $('#bannerTitulo').val(),
			idBannerTipo: $('#bannerTipo').val(),
			ubicacion: $('#bannerUbicacion').val(),
			descripcion: $('#bannerDescripcion').val(),
			fechaInicio: fechaInicio,
			fechaFin: fechaFin,
			texto: $('#bannerTexto').val(),
			precio: $('#bannerPrecio').val(),
			enlace: $('#bannerEnlace').val(),
			estado: $('#bannerEstado').prop('checked') ? 1 : 0,
		}, function(){
			$(document).trigger('formAnuncioCreate:aceptar');
		});
	}

	function anuncioCancel(){
		@if($isAjaxRequest)
			$(document).trigger('formAnuncioCreate:cancelar');
		@else
			redirect('{{ route('admin.anuncio.index')  }}');
		@endif
	}

	function formInit(){
		$('#anuncioNombre').focus();
//			$('#bannerPeriodo_group').datetimepicker();
		{{--$('#bannerPeriodo_group').daterangepicker({--}}
			{{--opens: 'right',--}}
			{{--format: 'DD/MM/YYYY',--}}
			{{--separator: ' al ',--}}
{{--//			startDate: moment().subtract('days', 29),--}}
			{{--startDate: moment(),--}}
			{{--endDate: moment(),--}}
			{{--minDate: '01/01/2018',--}}
			{{--maxDate: '12/31/{{ date('Y') + 1 }}',--}}
		{{--},--}}
		{{--function (start, end) {--}}
			{{--$('#bannerPeriodo_group input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));--}}
		{{--});--}}

		$('#bannerPeriodo span').html({{ $modelo->fechaInicio->format('m d, Y') }} + ' - ' + moment().format('m d, Y'));
		$('#bannerPeriodo').daterangepicker({
			format: 'DD/MM/YYYY',
			startDate: '{{ $modelo->fechaInicio->format('d/m/Y') }}',
			endDate: '{{ $modelo->fechaFin->format('d/m/Y') }}',
			minDate: '01/01/2017',
			maxDate: '31/12{{ date('Y') }}',
			dateLimit: { days: 90 },
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Hoy día': [moment(), moment()],
//				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'1 semana': [moment(), moment().add(6, 'days')],
				'2 semanas': [moment(), moment().add(13, 'days')],
				'1 mes': [moment(), moment().add(31, 'days')],
				'Este més': [moment().startOf('month'), moment().endOf('month')],
				'Ultimo més': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' a ',
//			singleDatePicker: true,
			locale: {
				applyLabel: 'Aceptar',
				cancelLabel: 'Cancelar',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Personalizado',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#bannerPeriodo span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			fechaInicio = start.format('DD/MM/YYYY');
			fechaFin = end.format('DD/MM/YYYY');
		});
	}

	formInit();

	// ----------

	function subirArchivo(){

		$('#msgSubiendo').removeClass('hide');
		$('#msgArchivoSubido').addClass('hide');

		$.ajax({
			url:'{{ route('admin.banner.upload') }}',
			method: 'POST',
			data: new FormData($("#upload_form")[0]),
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			statusCode: {
				422: function(e){
					$('#msgSubiendo').addClass('hide');
					ajaxValidationHandler(e);
				},
				500: ajaxErrorHandler
			},
			dataType:'json',
			type:'post',
			async:true,
			processData: false,
			contentType: false,
		}).done(function(data){
//		var data = JSON.parse(data);

			$('#msgSubiendo').addClass('hide');

			if(data.result == true){
				toast('Listo!', data.message, 'success');

				$('#msgArchivoSubido').removeClass('hide');
				$('#msgArchivoSubido a').prop('href', 'certificado/'+data.object.idRequerimiento+'/descargar');

				datatable.ajax.reload();
			} else {
				toast('Alerta!', data.message, 'warning');
			}
		});

	}


</script>
@endpush

