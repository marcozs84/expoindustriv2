@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />

<style type="text/css">

	.container img {
		height:80%;
	}

	@media (min-width: 1199px) {
		.carousel_container iframe{
			min-height: 450px;
		}
	}

	@media (max-width: 1199px) {
		.carousel_container iframe{
			min-height: 450px;
		}
	}
	@media (max-width: 1077px){
		.carousel_container iframe{
			min-height: 300px;
		}
	}

	@media (max-width: 991px) {
		.carousel_container iframe{
			min-height: 300px;
		}
	}

	@media (max-width: 768px) {
		.carousel_container iframe{
			min-height: 300px;
		}
	}

	@media (max-width: 767px) {

		.carousel_container iframe{
			min-height: 300px;
		}
	}

	@media (max-width: 480px) {
		.carousel_container iframe{
			min-height: 300px;
		}

	}

	@media (max-width: 400px) {
		.carousel_container iframe{
			min-height: 300px;
		}
	}

</style>
<meta content="width=device-width, initial-scale=1" name="viewport" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Banners</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Banners <small></small></h1>
	<!-- end page-header -->

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Carousel</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="carousel_container">
				<iframe src="{{ route('public.carousel') }}" frameborder="0" scrolling="no" style="width:100%; margin:0px; padding:0px;"></iframe>
			</div>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Banners</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="bannerCreate()"><i class="fa fa-plus"></i> Crear</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="bannerDelete()"><i class="fa fa-trash"></i> Eliminar</a>
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;

	$(document).ready(function(){
		initDataTable();
		$('.carousel').carousel()

	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.banner.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: 'Descripcion'},
					{ title: 'Categoria'},
					{ title: 'Estado'},
					{ data: 'created_at', title: 'Fecha Creacion'},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
							var name = '';
							if(row.producto){
								if(row.producto.marca === undefined){
									name += '<b>@lang('messages.no_brand')</b>';
								} else {
									name += row.producto.marca.nombre;
								}
								name += ' ';
								if(row.producto.modelo === undefined){
									name += '<b>@lang('messages.no_model')</b>';
								} else {
									name += row.producto.modelo.nombre;
								}
//								return name;
							} else {
								name =  '<b class="text-red">@lang('messages.no_product')!</b>';
							}
							return '<a href="javascript:;" onclick="bannerShow('+row.id+')">'+name+'</a>'
						}
					},{
						targets: 2,
						render: function(data, type, row){
							var response = '';
							if(row.producto !== null){
								if(row.producto.categorias !== undefined){
									$.each(row.producto.categorias, function(key, value){
										response += value.padre.nombre + ' / ' + value.nombre;
									});
									return response;
								} else {
									return 'Sin categorias';
								}
							} else {
								return '';
							}
						}
					},{
						targets:3,
						render: function(data, type, row){
							return estados[row.estado];
						}
					}, {
						targets: 5,
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function bannerShow(id){
		var url = '{{ route('admin.bannerSuperior.show', ['idBanner']) }}';
		url = url.replace('idBanner', id);
		var modal = openModal(url, 'Banner', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formBannerShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}

	function bannerCreate(){
		var url = '{{ route('admin.bannerSuperior.create') }}';
		var modal = openModal(url, 'Nuevo Banner', undefined, { size: 'modal-lg' });
		setModalHandler('formBannerCreate:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function bannerEdit(id){
		var url = '{{ route('admin.bannerSuperior.edit', ['idBanner']) }}';
		url = url.replace('idBanner', id);
		var modal = openModal(url, 'Editar Banner');
		setModalHandler('formBannerEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function bannerDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length == 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '{{ route('resource.banner.destroy', [0]) }}';

		ajaxDelete(url, ids, function(){
			datatable.ajax.reload(null, false);
		});
	}

</script>
@endpush