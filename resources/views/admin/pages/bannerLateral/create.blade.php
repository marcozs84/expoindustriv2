@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
	<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
	.dropzone .dz-preview .dz-image{
		border-radius:7px !important;
	}
	.dropzone .dz-preview .dz-image img{
		max-width:120px;
	}

	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}

	h5 {
		clear:both;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="{{ route('admin.home') }}">Home</a></li>
		<li><a href="{{ route('admin.bannerSuperior.index') }}">Banners</a></li>
		<li class="active">Nuevo Banners</li>
	</ol>

	<h1 class="page-header">Nuevo Banner</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Banner</h4>
		</div>
		<div class="panel panel-body">
			{{--<form id="formBannerCreate" method="get" class="form-horizontal">--}}

				<h4>Informacion del banner</h4>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Titulo</label>
					<div class="col-md-9">
						{!! Form::text('bannerTitulo', '', [
							'id' => 'bannerTitulo',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Tipo</label>
					<div class="col-md-9">
						{!! Form::select('bannerTipo', [
							'1' => 'Imagen',
							'2' => 'Producto',
						], null, [
							'id' => 'bannerTipo',
							'placeholder' => 'Tipo (Imagen o producto)',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Ubicacion</label>
					<div class="col-md-9">
						{!! Form::select('bannerUbicacion', [
							'bp1' => 'Home: Principal superior',
							'hi1' => 'Home: Izquierdo, debajo de buscador',
							'bd1' => 'Buscar: columna derecha 1',
							'bd2' => 'Buscar: columna derecha 2',
							'bd3' => 'Buscar: columna derecha 3',
						], null, [
							'id' => 'bannerUbicacion',
							'placeholder' => 'Ubicacion',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Descripcion</label>
					<div class="col-md-9">
						{!! Form::textarea('bannerDescripcion', '', [
							'id' => 'bannerDescripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>


				{{--<div class="form-group row m-b-15">--}}
				{{--<label class="col-md-3 col-form-label">Periodo de vigencia</label>--}}
				{{--<div class="col-md-9">--}}
				{{--<div class="input-group date" id="bannerPeriodo_group">--}}
				{{--{!! Form::text('bannerPeriodo', '', [--}}
				{{--'id' => 'bannerPeriodo',--}}
				{{--'placeholder' => 'dd/mm/yyyy al dd/mm/yyyy',--}}
				{{--'class' => 'form-control'--}}
				{{--]) !!}--}}
				{{--<span class="input-group-append">--}}
				{{--<span class="input-group-text"><i class="fa fa-calendar"></i></span>--}}
				{{--</span>--}}
				{{--</div>--}}
				{{--</div>--}}
				{{--</div>--}}

				<div class="form-group row m-b-15">
					<label class="col-md-3 col-form-label">Periodo de vigencia</label>
					<div class="col-md-9">
						<div id="bannerPeriodo" class="btn btn-default btn-block text-left f-s-12">
							<i class="fa fa-caret-down pull-right m-t-2"></i>
							<span></span>
						</div>
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-md-3 col-form-label">Activo</label>
					<div class="col-md-9">
						{!! Form::checkbox('bannerEstado', true, [
							'id' => 'bannerEstado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<h4>Contenido</h4>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Fuente</label>
					<div class="col-md-9">
						{{--<form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="" >--}}
							{{--<input type="hidden" id="randBanner" name="randBanner" value="{{ rand(1,99999) }}"/>--}}
							{{--<div class="form-group">--}}
								{{--<input type="file" class="form-control" name="archivo" id="archivo">--}}
							{{--</div>--}}
							{{--<div class="form-group">--}}
								{{--<a id="btnSubirArchivo" href="javascript:subirArchivo();" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Subir</a>--}}
							{{--</div>--}}
						{{--</form>--}}

						<form action="{{ route('admin.bannerSuperior.upload') }}" class="dropzone">
							<input type="hidden" name="_token" value="" id="dropToken">
							<input type="hidden" name="sesna" value="{{ $rand }}">
							<div class="dz-message" data-dz-message><span>@lang('messages.dz_upload')</span></div>
						</form>

						<div class="form-group hide" id="msgSubiendo">
							Subiendo... <i class="fa fa-refresh fa-spin"></i><br>
							Por favor aguarde hasta que el archivo termine de subir.
						</div>
						<div class="form-group hide" id="msgArchivoSubido">
							Para visualizar el archivo subido haga
							<a target="_blank" href=""><i class="fa fa-download"></i> click aquí.</a>
						</div>
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Texto</label>
					<div class="col-md-9">
						{!! Form::textarea('bannerTexto', '', [
							'id' => 'bannerTexto',
							'placeholder' => 'Texto',
							'class' => 'form-control',
							'rows' => 3
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Precio</label>
					<div class="col-md-9">
						{!! Form::text('bannerPrecio', '', [
							'id' => 'bannerPrecio',
							'placeholder' => 'Precio',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Enlace</label>
					<div class="col-md-9">
						{!! Form::textarea('bannerEnlace', '', [
							'id' => 'bannerEnlace',
							'placeholder' => 'Enlace',
							'class' => 'form-control',
							'rows' => 3
						]) !!}
					</div>
				</div>

				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="bannerCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="bannerSave()" >Guardar</a>
					</div>
				</div>
			{{--</form>--}}
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>

	$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
		setTimeout(function(){
			$('div#dropzone').dropzone({
				url: '{{ route('admin.bannerSuperior.upload') }}'
			});

			var myDropzone = new Dropzone(".dropzone", {
				url: "{{ route('admin.bannerSuperior.upload') }}",
				addRemoveLinks: true,
				autoProcessQueue: true,
				parallelUploads: 1,
				init: function(){
					{{--@foreach($images as $image)--}}
					{{--var mockFile = {--}}
							{{--name: '{{ $image['filename'] }}',--}}
							{{--size: 123,--}}
						{{--};--}}
					{{--this.options.addedfile.call(this, mockFile);--}}
					{{--this.options.thumbnail.call(this, mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
					{{--this.emit('thumbnail', mockFile, '/tmpimgs/{{ $image['filename'] }}');--}}
					{{--this.emit('complete', mockFile);--}}
					{{--// Src.: https://stackoverflow.com/a/22719947;--}}
					{{--@endforeach--}}

					this.on("removedfile", function(file) {
						//alert(file.name);
						console.log('Eliminado: ' + file.name);

						file.previewElement.remove();

						// Create the remove button
						var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");

						//Capture the Dropzone instance as closure.
						var _this = this;

						// Listen to the click event
						//					removeButton.addEventListener();

						// Add the button to the file preview element.
						file.previewElement.appendChild(removeButton);

					});
				},
				success: function(file, response){
					console.log("success");
					console.log(file.name);
					console.log(response);
				},
				removedfile: function(file){
					x = confirm('@lang('messages.confirm_removal')');
					if(!x) return false;
					removeImage(1, file.name);
//					for(var i = 0 ; i < file_up_names.length; i++){
//						if(file_up_names[i] === file.name){
//							ajaxPost('public.publishRemove', {
//								'archivo' : ''
//							});
//						}
//					}
				}
			});
		}, 1000);

		$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
		$("[data-toggle=tooltip]").tooltip();

	});

	$( "#formBannerCreate" ).submit(function( event ) {
		bannerSave();
		event.preventDefault();
	});

	var fechaInicio = '';
	var fechaFin = '';

	function bannerSave(){
		var url = '{{ route('resource.banner.store') }}';
		ajaxPost(url, {
			titulo: $('#bannerTitulo').val(),
			idBannerTipo: $('#bannerTipo').val(),
			ubicacion: $('#bannerUbicacion').val(),
			descripcion: $('#bannerDescripcion').val(),
			fechaInicio: fechaInicio,
			fechaFin: fechaFin,
			texto: $('#bannerTexto').val(),
			precio: $('#bannerPrecio').val(),
			enlace: $('#bannerEnlace').val(),
			estado: $('#bannerEstado').prop('checked') ? 1 : 0,
		}, function(){
			$(document).trigger('formBannerCreate:aceptar');
		});
	}

	function bannerCancel(){
		@if($isAjaxRequest)
			$(document).trigger('formBannerCreate:cancelar');
		@else
			redirect('{{ route('admin.bannerSuperior.index')  }}');
		@endif
	}

	function formInit(){
		$('#bannerNombre').focus();

		$('#bannerPeriodo span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
		$('#bannerPeriodo').daterangepicker({
			format: 'DD/MM/YYYY',
			startDate: moment(),
			endDate: moment().subtract(6, 'days'),
			minDate: '01/01/2018',
			maxDate: '31/12{{ date('Y') }}',
			dateLimit: { days: 90 },
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Hoy día': [moment(), moment()],
//				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'1 semana': [moment(), moment().add(6, 'days')],
				'2 semanas': [moment(), moment().add(13, 'days')],
				'1 mes': [moment(), moment().add(31, 'days')],
				'Este més': [moment().startOf('month'), moment().endOf('month')],
				'Ultimo més': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' a ',
//			singleDatePicker: true,
			locale: {
				applyLabel: 'Aceptar',
				cancelLabel: 'Cancelar',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Personalizado',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#bannerPeriodo span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			fechaInicio = start.format('DD/MM/YYYY');
			fechaFin = end.format('DD/MM/YYYY');
		});
	}

	formInit();

	// ----------

	function subirArchivo(){

		$('#msgSubiendo').removeClass('hide');
		$('#msgArchivoSubido').addClass('hide');

		$.ajax({
			url:'{{ route('admin.bannerSuperior.upload') }}',
			method: 'POST',
			data: new FormData($("#upload_form")[0]),
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			statusCode: {
				422: function(e){
					$('#msgSubiendo').addClass('hide');
					ajaxValidationHandler(e);
				},
				500: ajaxErrorHandler
			},
			dataType:'json',
			type:'post',
			async:true,
			processData: false,
			contentType: false,
		}).done(function(data){
//		var data = JSON.parse(data);

			$('#msgSubiendo').addClass('hide');

			if(data.result == true){
				toast('Listo!', data.message, 'success');

				$('#msgArchivoSubido').removeClass('hide');
				$('#msgArchivoSubido a').prop('href', 'certificado/'+data.object.idRequerimiento+'/descargar');

				datatable.ajax.reload();
			} else {
				toast('Alerta!', data.message, 'warning');
			}
		});

	}


</script>
@endpush

