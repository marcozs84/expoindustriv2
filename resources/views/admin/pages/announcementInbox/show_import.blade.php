@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Inbox')

@push('css')
<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

<style>
.tblLabel{
	text-align:right;
	font-weight:normal;
	vertical-align: middle !important;
}
.tblValue{
	text-align:left;
	font-weight:bold;
	vertical-align: middle !important;
}
.tblValue .input-group{
	margin-bottom:0px !important;
}

label {
	margin-top:10px;
}

.input-group-prepend .input-group-text {
		font-weight:bold;
	}
</style>
<!-- begin wrapper -->
<div id="message_{{ $pub->id }}" class="wrapper">
	<h3 class="m-t-0 m-b-15 f-w-500">{{ $pub->Producto->nombre_alter }}
		<span class="badge

		@if($pub->estado == 1)
			badge-secondary
		@elseif($pub->estado == 2)
			badge-yellow
		@elseif($pub->estado == 3)
			badge-primary
		@elseif($pub->estado == 4)
			badge-danger
		@elseif($pub->estado == 5)
			badge-warning
		@endif
		" style="font-size:13px;">@lang('messages.'.$pubEstados[$pub->estado])</span>

		<span class="pull-right">Cod: {{ str_pad($pub->id, 5, '0',STR_PAD_LEFT) }}</span>
	</h3>
	<ul class="media-list underline m-b-15 p-b-15">
		<li class="media media-sm clearfix">
			{{--<a href="javascript:;" class="pull-left">--}}
				{{--<img class="media-object rounded-corner" alt="" src="../assets/img/user/user-12.jpg" />--}}
			{{--</a>--}}
			<div class="media-body">
				<div class="email-from text-inverse f-s-14 f-w-600 m-b-3">
					Cliente: <a href="javascript:;" onclick="agendaShow({{ $pub->Owner->Owner->id }})">{{ $pub->Owner->Owner->Agenda->nombres_apellidos }}  <b>&lt;{{ $pub->Owner->username }}&gt;</b></a>
				</div>
				<div class="m-b-3">Publicado: <i class="fa fa-clock fa-fw"></i> {{ $pub->created_at->toFormattedDateString() }} {{ $pub->created_at->format('H:i') }} - ( {{ $pub->created_at->diffForHumans(\Carbon\Carbon::now()) }} )</div>
				<div class="m-b-3">Vence: <i class="fa fa-clock fa-fw"></i> {{ $pub->created_at->addDays($pub->dias)->toFormattedDateString() }} {{ $pub->created_at->addDays($pub->dias)->format('H:i') }} - ( {{ $pub->created_at->addDays($pub->dias)->diffForHumans(\Carbon\Carbon::now()) }} )</div>
				<div class="m-b-3">Dias contratados: {{ $pub->dias }} días</div>
				<div class="m-b-3">{{-- <i class="fa fa-check-square fa-fw"></i> --}}
					@if( $pub->Producto->Categorias->count() > 0 )
						{{ $pub->Producto->Categorias[0]->Padre->nombre }} / {{ $pub->Producto->Categorias[0]->nombre }}
					@else
						<h5><span class="text-danger" style="font-weight: bold;">No tiene Categoría ni sub categoría!!</span></h5>
					@endif
					<br>
					{{ $pub->Producto->nombre_alter }}
					<br>
					<a href="javascript:;" onclick="productoEdit()">Editar Producto</a> |
					<a href="{{ $pub->Producto->ProductoItem->permalink }}" target="_blank">Ir al anuncio original</a> |
					<a href="https://www.expoindustri.com{{ $pub->Producto->url_csmmid }}" target="_blank">Anuncio en portal</a> |
				</div>

				{{--<div class="email-to">To: nguoksiong@live.co.uk</div>--}}
			</div>
		</li>
	</ul>

	<h5>Imágenes <small><a href="javascript:;" onclick="toggleCollapse('#gallery')">Ver todas las imágenes</a></small></h5>
	<div id="gallery" class="gallery m-b-20" style="height:120px; overflow:hidden;">
	@forelse($pub->Producto->Galerias as $galeria)
		<ul class="attached-document clearfix">
			@foreach($galeria->Imagenes as $imagen)
				<li class="fa-camera">
					<div class="document-file">
						<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">
							<img src="{{ $imagen->ruta_publica_producto_thumb }}" alt="" />
						</a>
					</div>
					<div class="document-name"><a href="javascript:;">{{ $imagen->filename }}</a></div>
				</li>
			@endforeach
		</ul>
	@empty
		<div class="container"><p>No hay imágenes disponibles</p></div>
	@endforelse
	</div>


	{{--<ul class="nav nav-pills mb-2">--}}
		{{--<li class="nav-item">--}}
			{{--<a href="#nav-pills-tab-1" data-toggle="tab" class="nav-link active">--}}
				{{--<span class="d-sm-none">Anuncio</span>--}}
				{{--<span class="d-sm-block d-none">Información del anuncio</span>--}}
			{{--</a>--}}
		{{--</li>--}}
		{{--<li class="nav-item">--}}
			{{--<a href="#nav-pills-tab-2" data-toggle="tab" class="nav-link">--}}
				{{--<span class="d-sm-none">Máquina</span>--}}
				{{--<span class="d-sm-block d-none">Información de la máquina</span>--}}
			{{--</a>--}}
		{{--</li>--}}
	{{--</ul>--}}

	{{--<div class="tab-content">--}}
		{{--<!-- begin tab-pane -->--}}
		{{--<div class="tab-pane fade active show" id="nav-pills-tab-1">--}}
			{{--<h3 class="m-t-10">Información del anuncio</h3>--}}
			{{--<p>--}}
				{{--Lorem ipsum dolor sit amet, consectetur adipiscing elit.--}}
				{{--Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor,--}}
				{{--est diam sagittis orci, a ornare nisi quam elementum tortor.--}}
				{{--Proin interdum ante porta est convallis dapibus dictum in nibh.--}}
				{{--Aenean quis massa congue metus mollis fermentum eget et tellus.--}}
				{{--Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien,--}}
				{{--nec eleifend orci eros id lectus.--}}
			{{--</p>--}}
		{{--</div>--}}
		{{--<!-- end tab-pane -->--}}
		{{--<!-- begin tab-pane -->--}}
		{{--<div class="tab-pane fade" id="nav-pills-tab-2">--}}
			{{--<h3 class="m-t-10">Información de la máquina</h3>--}}
			{{--<p>--}}
				{{--Lorem ipsum dolor sit amet, consectetur adipiscing elit.--}}
				{{--Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor,--}}
				{{--est diam sagittis orci, a ornare nisi quam elementum tortor.--}}
				{{--Proin interdum ante porta est convallis dapibus dictum in nibh.--}}
				{{--Aenean quis massa congue metus mollis fermentum eget et tellus.--}}
				{{--Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien,--}}
				{{--nec eleifend orci eros id lectus.--}}
			{{--</p>--}}
		{{--</div>--}}
		{{--<!-- end tab-pane -->--}}
	{{--</div>--}}

	@if( $pub->Producto->ProductoItem->idTipoImportacion == 1 )

		@php
			$nombre = $pub->Producto->Traducciones->where('campo', 'nombre')->first();
			$nombre_se = $nombre_en = $nombre_es = '';
			if($nombre) {
				$nombre_se     = $nombre->se;
				$nombre_en     = $nombre->en;
				$nombre_es     = $nombre->es;
			}

			$resumen = $pub->Producto->Traducciones->where('campo', 'resumen')->first();
			$resumen_se = $resumen_en = $resumen_es = '';
			if($resumen) {
				$resumen_se     = $resumen->se;
				$resumen_en     = $resumen->en;
				$resumen_es     = $resumen->es;
			}

			$descripcion = $pub->Producto->Traducciones->where('campo', 'descripcion')->first();
			$descripcion_se = $descripcion_en = $descripcion_es = '';
			if($descripcion) {
				$descripcion_se     = $descripcion->se;
				$descripcion_en     = $descripcion->en;
				$descripcion_es     = $descripcion->es;
			}
		@endphp

		<h5>Nombre</h5>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-condensed table-striped">
					<tr>
						<td style="width:33%;">{{ $nombre_se }}</td>
						<td style="width:33%;">{{ $nombre_en }}</td>
						<td style="width:33%;">{{ $nombre_es }}</td>
					</tr>
				</table>
			</div>
		</div>

		<h5>Resumen</h5>
		<div class="row m-t-10">
			<div class="col-md-12">
				<table class="table table-condensed table-striped">
					<tr>
						<td style="width:33%;">{!! $resumen_se !!}</td>
						<td style="width:33%;">{!! $resumen_en !!}</td>
						<td style="width:33%;">{!! $resumen_es !!}</td>
					</tr>
				</table>
			</div>
		</div>

		<h5>Descripcion</h5>
		<div class="row m-t-10">
			<div class="col-md-12">
				<table class="table table-condensed table-striped">
					<tr>
						<td style="width:33%;">{!! $descripcion_se !!}</td>
						<td style="width:33%;">{!! $descripcion_en !!}</td>
						<td style="width:33%;">{!! $descripcion_es !!}</td>
					</tr>
				</table>
			</div>
		</div>
	@endif

	<h5>Información primaria</h5>
	@if(count($primaryData) > 0)
	<div class="row">
		@foreach($primaryData as $data)
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-6 tblLabel">{!! $data['text'] !!}</div>
					<div class="col-md-6 tblValue">
						@if(in_array($data['type'], ['select', 'boolean']))
							@lang('form.'.$data['value'])
						@elseif(in_array($data['type'], ['multiselect']))
							<ul class="p-0">
								@foreach($data['value'] as $val)
									<li>
										@lang('form.'.$val)
									</li>
								@endforeach
							</ul>
						@elseif($data['type'] == 'range_year')
							{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
						@elseif($data['type'] == 'text')
							<a href="javascript:;" onclick="translateField('{{ $data['key'] }}')">{{ $data['value'] }}</a>
						@elseif($data['type'] == 'multiline')
							<a href="javascript:;" onclick="translateFieldML('{{ $data['key'] }}')">{{ $data['value'] }}</a>
						@elseif($data['type'] == 'number')
							{{ $data['value'] }}
						@else
							@lang('form.'.$data['value'])
						@endif
						{{--({{ $data['type'] }})--}}
					</div>
				</div>
			</div>
		@endforeach
	</div>
	@else
		<div class="row">
			<div class="col-md-12"><p>Sin información primaria </p></div>
		</div>
	@endif

	<br>
	<h5>Información adicional</h5>
	@if(count($secondaryData) > 0)
		<div class="row">
			@foreach($secondaryData as $data)
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-6 tblLabel">{!! $data['text'] !!} </div>
						<div class="col-md-6 tblValue">
							@if(in_array($data['type'], ['select', 'boolean']))
								@lang('form.'.$data['value'])
							@elseif(in_array($data['type'], ['multiselect']))
								<ul class="p-0">
									@foreach($data['value'] as $val)
										<li>
											@lang('form.'.$val)
										</li>
									@endforeach
								</ul>
							@elseif($data['type'] == 'range_year')
								{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
							@elseif($data['type'] == 'text')
								<a href="javascript:;" onclick="translateField('{{ $data['key'] }}')">{{ $data['value'] }}</a>
							@elseif($data['type'] == 'multiline')
								<a href="javascript:;" onclick="translateFieldML('{{ $data['key'] }}')">{{ $data['value'] }}</a>
							@elseif($data['type'] == 'number')
								{{ $data['value'] }}
							@else
								@lang('form.'.$data['value'])
							@endif
							{{--({{ $data['type'] }})--}}
						</div>
					</div>
				</div>
			@endforeach
		</div>
	@else
		<div class="row">
			<div class="col-md-12"><p>Sin información secundaria </p></div>
		</div>
	@endif

	<br>
	<h5>@lang('messages.payments')</h5>
	<table class="table table-condensed table-striped">
		<thead>
		<tr>
			<th>Id</th>
			<th>@lang('messages.description')</th>
			<th>@lang('messages.amount')</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		@foreach($pub->transacciones as $transaccion)
			<tr>
				<td>{{ $transaccion->id }}</td>
				<td>{{ $transaccion->producto }}</td>
				<td>{{ number_format(($transaccion->monto / 100), 2) }} {{ $transaccion->moneda }}</td>
				<td>
					<a target="_blank" href="https://dashboard.stripe.com/test/payments/{{ $transaccion->idStripeTransaction }}">Ver pago</a>
				</td>
			</tr>
		@endforeach

		</tbody>
	</table>

	<br>
	<h5>@lang('messages.location')</h5>
	<table class="table table-condensed-xtra table-striped">
		<tr>
			<td class="tblLabel">@lang('messages.country')</td>
			<td class="tblValue">{{ $global_paises[$pub->Producto->ProductoItem->Ubicacion->pais] }} ({{ strtoupper($pub->Producto->ProductoItem->Ubicacion->pais) }})</td>
			<td class="tblLabel">@lang('messages.city')</td>
			<td class="tblValue">{!! $pub->Producto->ProductoItem->Ubicacion->ciudad !!}</td>
		</tr>
		<tr>
			<td class="tblLabel" style="vertical-align:middle;">@lang('messages.postal_code')</td>
			<td class="tblValue" style="vertical-align:middle;">{!! $pub->Producto->ProductoItem->Ubicacion->cpostal !!}</td>
			<td class="tblLabel" style="vertical-align:middle;">@lang('messages.address')</td>
			<td class="tblValue" style="vertical-align:middle;">{!! $pub->Producto->ProductoItem->Ubicacion->direccion !!}</td>
		</tr>
		{{--<tr>--}}
			{{--<td colspan="4">--}}
				{{--<style>--}}
					{{--#publish_map {--}}
						{{--height: 300px;--}}
						{{--width: 100%;--}}
					{{--}--}}
				{{--</style>--}}
				{{--<div id="publish_map"></div>--}}
			{{--</td>--}}
		{{--</tr>--}}
	</table>

	<br>
	<h5>@lang('messages.dimentions')</h5>
	<table class="table table-condensed-xtra table-striped">
		<tr>
			<td class="tblLabel">@lang('messages.longitude')</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['lng'] }}</td>
			<td class="tblLabel">@lang('messages.width')</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['wdt'] }}</td>
		</tr>
		<tr>
			<td class="tblLabel">@lang('messages.height')</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['hgt'] }}</td>
			<td class="tblLabel">@lang('messages.weight')</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['wgt'] }}</td>
		</tr>
		<tr>
			<td class="tblLabel">Pallet</td>
			<td class="tblValue">{{ ($pub->Producto->ProductoItem->dimension['pal'] == 1) ? 'Si': 'No' }}</td>
		</tr>
	</table>

	<br>
	<h4>Precio a publicar</h4>
	<table class="table table-condensed table-striped">
		<tr>
			<td class="tblLabel">Precio Proveedor</td>
			<td class="tblValue">{{ $pub->Producto->precioProveedor }} {{ strtoupper($pub->Producto->monedaProveedor) }}</td>
			{{--<td class="tblLabel">Precio Minimo</td>--}}
			{{--<td class="tblValue">{{ $pub->Producto->precioMinimo }} {{ strtoupper($pub->Producto->monedaMinimo) }}</td>--}}


			<td class="tblLabel"><label for="chkPrecioSudamerica">Visible solo en Sudamérica</label></td>
			<td class="tblValue">
				<input id="chkPrecioSudamerica" type="checkbox" @if($pub->Producto->precioSudamerica == 1) checked  @endif >
				<label for="chkPrecioSudamerica"></label>
			</td>
		</tr>
		<tr>

			<td class="tblLabel">Precio Europa</td>
			<td class="tblValue">{{ $pub->Producto->precioEuropa }} {{ strtoupper($pub->Producto->monedaEuropa) }}</td>
			{{--<td class="tblLabel">Precio Establecido</td>--}}

			<td class="tblLabel"><label for="chkPrecioFijo">Precio fijado por el consultor</label></td>
			<td class="tblValue">
				<input id="chkPrecioFijo" type="checkbox" @if($pub->Producto->precioFijo == 1) checked  @endif >
				{{--<label for="chkPrecioFijo">Precio fijado por el consultor</label>--}}
			</td>
		</tr>
	</table>

	<div id="preciosFijos" class="@if( $pub->Producto->precioFijo != 1 ) hide  @endif">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="input-group m-b-10">
							<div class="input-group-prepend"><span class="input-group-text">USD</span></div>
							<input type="number" value="{{ $pub->Producto->precioFijoUsd }}" id="precio_usd" name="precio_usd" class="form-control" placeholder="0">
							<div class="input-group-append"><span class="input-group-text">.00</span></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="input-group m-b-10">
							<div class="input-group-prepend"><span class="input-group-text">EUR</span></div>
							<input type="number" value="{{ $pub->Producto->precioFijoEur }}" id="precio_eur" name="precio_eur" class="form-control" placeholder="0">
							<div class="input-group-append"><span class="input-group-text">.00</span></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="input-group m-b-10">
							<div class="input-group-prepend"><span class="input-group-text">SEK</span></div>
							<input type="number" value="{{ $pub->Producto->precioFijoSek }}" id="precio_sek" name="precio_sek" class="form-control" placeholder="0">
							<div class="input-group-append"><span class="input-group-text">.00</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<h4>Calculadora</h4>
	<div class="row">
		<div class="col-md-12">
			<p>Todos los precios de la calculadora están expresados en USD.</p>
		</div>
	</div>
	<h5>Servicios adicionales</h5>
	<table class="table table-condensed table-striped">
		<tr>
			<td class="tblLabel">Informe técnico</td>
			<td class="tblValue">{!! Form::number('informe_tecnico', $calc->informe_tecnico, ['id' => 'informe_tecnico', 'class' => 'form-control', 'placeholder' => '']) !!}</td>
			<td class="tblLabel">Seguro extra</td>
			<td class="tblValue">{!! Form::number('seguro_extra', $calc->seguro_extra, ['id' => 'seguro_extra', 'class' => 'form-control', 'placeholder' => '']) !!}</td>
			<td class="tblLabel">Tasa de tramitación</td>
			<td class="tblValue">{!! Form::number('tasa_tramite', $calc->tasa_tramite, ['id' => 'tasa_tramite', 'class' => 'form-control', 'placeholder' => '']) !!}</td>
		</tr>
	</table>
	<h5>Transporte</h5>
	{{--<table class="table table-condensed table-striped">--}}
		{{--<tr>--}}
			{{--<td class="tblLabel">Contenedor compartido</td>--}}
			{{--<td class="tblValue">{!! Form::text('contenedor_compartido', $calc->contenedor_compartido, ['id' => 'contenedor_compartido', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}
			{{--<td class="tblLabel">Contenedor exclusivo</td>--}}
			{{--<td class="tblValue">{!! Form::text('contenedor_exclusivo', $calc->contenedor_exclusivo, ['id' => 'contenedor_exclusivo', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}
			{{--<td class="tblLabel">Envío abierto</td>--}}
			{{--<td class="tblValue">{!! Form::text('envio_abierto', $calc->envio_abierto, ['id' => 'envio_abierto', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}
		{{--</tr>--}}
	{{--</table>--}}
	<table class="table table-condensed table-striped">
		<tr>
			@php
				$dIdx = 0;
			@endphp
			@foreach($transportes as $transporte)
				@php $dIdx++  @endphp
				<td class="tblLabel">
					{{ $transporte->id }}) {{ $transporte->nombre }}

				</td>
				<td class="tblValue">
					<a href="javascript:;" onclick="openTransporteDisponibilidad({{ $pub->id }}, {{ $transporte->id }})">
						<i class="fa fa-eye"></i>
					</a>

					@if($transporte->estado == 0)
						<a href="javascript:;" data-toggle="tooltip" title="Deshabilitado de forma global"><i class="fa fa-info-circle fa-lg text-red"></i></a>
					@else
						<input id="chkTrans_{{ $transporte->id }}" type="checkbox"
{{--						       @if(count($transportePub) == 0 || isset($transportePub[$transporte->id])) checked @endif--}}
						       @if($transporte->estado == 0) disabled @endif
						>
					@endif
					{!! Form::number('txtTrans_'.$transporte->id, isset($transportePub[$transporte->id]) ? $transportePub[$transporte->id] : '', [
						'id' => 'txtTrans_'.$transporte->id,
						'class' => 'form-control',
						'style' => 'width:80%; display:inline;',
						'placeholder' => 'USD'
					]) !!}
				</td>
			@if($dIdx % 2 == 0)
		</tr>
		<tr>
			@endif
			@endforeach
		</tr>
	</table>
	<h5>Destinos</h5>
	{{--<table class="table table-condensed table-striped">--}}
		{{--<tr>--}}
			{{--<td class="tblLabel">Aduana interior Cochabamba&nbsp;&nbsp;--}}
				{{--<input type="checkbox" checked></td>--}}
			{{--<td class="tblValue">--}}
				{{--{!! Form::text('destino_1', $calc->destino_1, ['id' => 'destino_1', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}
			{{--<td class="tblLabel">Aduana interior La Paz&nbsp;&nbsp;--}}
				{{--<input type="checkbox" checked></td>--}}
			{{--<td class="tblValue">{!! Form::text('destino_2', $calc->destino_2, ['id' => 'destino_2', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}

		{{--</tr>--}}
		{{--<tr>--}}
			{{--<td class="tblLabel">Aduana interior Oruro&nbsp;&nbsp;--}}
				{{--<input type="checkbox" checked></td>--}}
			{{--<td class="tblValue">{!! Form::text('destino_3', $calc->destino_3, ['id' => 'destino_3', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}
			{{--<td class="tblLabel">Aduana interior Santa Cruz&nbsp;&nbsp;--}}
				{{--<input type="checkbox" disabled>--}}
				{{--<a href="javascript:;" data-toggle="tooltip" title="Deshabilitado de forma global"><i class="fa fa-info-circle fa-lg"></i></a>--}}

			{{--</td>--}}
			{{--<td class="tblValue">{!! Form::text('destino_4', $calc->destino_4, ['id' => 'destino_4', 'class' => 'form-control', 'placeholder' => '0', 'disabled' => 'disabled']) !!}</td>--}}

		{{--</tr>--}}
		{{--<tr>--}}
			{{--<td class="tblLabel">Zona franca de Iquique&nbsp;&nbsp;--}}
				{{--<input type="checkbox" checked></td>--}}
			{{--<td class="tblValue">{!! Form::text('destino_5', $calc->destino_5, ['id' => 'destino_5', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}
			{{--<td class="tblLabel">Zona franca de TacnaCruz&nbsp;&nbsp;--}}
				{{--<input type="checkbox" checked></td>--}}
			{{--<td class="tblValue">{!! Form::text('destino_6', $calc->destino_6, ['id' => 'destino_6', 'class' => 'form-control', 'placeholder' => '0']) !!}</td>--}}
		{{--</tr>--}}
	{{--</table>--}}
	<table class="table table-condensed table-striped">
		<tr>
		@php
			$dIdx = 0;
		@endphp
		@foreach($destinos as $destino)
			@php $dIdx++  @endphp
			<td class="tblLabel">
				{{ $destino->id }}) {{ $destino->nombre }}<br>
				<input id="chkDest_{{ $destino->id }}" type="checkbox"
{{--				        @if(count($destinosPub) == 0 || isset($destinosPub[$destino->id])) checked @endif--}}
						@if($destino->estado == 0) disabled @endif
				>
				@if($destino->estado == 0)
					<a href="javascript:;" data-toggle="tooltip" title="Deshabilitado de forma global"><i class="fa fa-info-circle fa-lg text-red"></i></a>
				@endif
			</td>
			<td class="tblValue">
				{!! Form::number('txtDest_'.$destino->id, isset($destinosPub[$destino->id]) ? $destinosPub[$destino->id] : '', ['id' => 'txtDest_'.$destino->id, 'class' => 'form-control', 'placeholder' => '']) !!}
			</td>
			@if($dIdx % 2 == 0)
				</tr>
				<tr>
			@endif
		@endforeach
		</tr>
	</table>

	<div class="row">
		<div class="col-md-3 text-right">
			{!! Form::select('disponibilidad', $disponibilidad, null, [
				'id' => 'disponibilidad',
				'class' => 'form-control pull-left',
				'style' => 'width:150px;'
			]); !!}
		</div>
		<div class="col-md-9 text-right">
			{{--<a href="javascript:;" class="btn btn-primary" onclick="guardarCalculadora()">Guardar calculadora</a>--}}
			{{--<br><br>--}}

			<a href="javascript:;" onclick="guardar('rejected')" class="btn btn-danger"><i class="fa fa-thumbs-down"></i> Rechazar</a>
			<a href="javascript:;" onclick="guardar('disabled')" class="btn btn-warning"><i class="fa fa-thumbs-down"></i> Desactivar</a>
			<a href="javascript:;" onclick="guardar('none')" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</a>
			<a href="javascript:;" onclick="guardar('approved')" class="btn btn-inverse"><i class="fa fa-thumbs-up"></i> Guardar y Aprobar</a>
		</div>
	</div>

</div>
<!-- end wrapper -->
@endsection

@push('scripts')
<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

{{--<script src="/assets/plugins/ckeditor/ckeditor.js"></script>--}}
{{--<script src="/assets/js/demo/gallery.demo.js"></script>--}}
<script>
	$(document).ready(function() {
//		Gallery.init();

		var container = $('#gallery');
		var dividerValue = calculateDivider();
		var containerWidth = $(container).width();
		var columnWidth = containerWidth / dividerValue;
		$(container).isotope({
			resizable: true,
			masonry: {
				columnWidth: columnWidth
			}
		});

		$(window).smartresize(function() {
			var dividerValue = calculateDivider();
			var containerWidth = $(container).width();
			var columnWidth = containerWidth / dividerValue;
			$(container).isotope({
				masonry: {
					columnWidth: columnWidth
				}
			});
		});

		$('#disponibilidad').val({{ $pub->Producto->ProductoItem->idDisponibilidad }});



		// ----------


//		var config1 = {
//			height:'400',
//			startupOutlineBlocks:true,
//			scayt_autoStartup:true,
//			toolbar_Full:[
//				{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
//				{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
//				'/',
//				{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
//				{ name: 'insert', items : [ 'Image','HorizontalRule' ] },
//				{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
//				{ name: 'colors', items : [ 'TextColor','BGColor' ] },
//				{ name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] },
//				{ name: 'document', items : [ 'Source' ] }
//			]
//		}
//
//		$('#descripcion_se').ckeditor(config1);
	});

	function calculateDivider() {
		var dividerValue = 4;
		if ($(this).width() <= 480) {
			dividerValue = 1;
		} else if ($(this).width() <= 767) {
			dividerValue = 2;
		} else if ($(this).width() <= 980) {
			dividerValue = 3;
		}
		return dividerValue;
	}

	$('#chkPrecioFijo').change(function() {
		if(this.checked) {
//			var returnVal = confirm("Are you sure?");
//			$(this).prop("checked", returnVal);
			$('#preciosFijos').removeClass('hide');
		} else {
			$('#preciosFijos').addClass('hide');
		}


	});

</script>
@endpush


@push('scripts')
<script>

//$.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyALPqjAs5i_1KqcyQ_YMIEtxKblkrDIHaY').done(function() {
//
//	initPublishMap();
//	//buscarEnMapa();
//});

$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();

	@if( $producto->ProductoItem->idDisponibilidad != \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO)

	$('#disponibilidad').select2().on('select2:select', function (e) {
		var data = e.params.data;
		console.log(data);

		if ( parseInt(data.id, 10) === {{ \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO }}) {
			swal({
				text: 'Desea registrar una orden?',
				icon: 'warning',
				buttons: {
					cancel: 'No',
					ok: 'Si'
				},
				dangerMode: true
			}).then(function(response) {
				if (response === 'ok') {
					registrarOrden({{ $producto->id }});
				} else {
					$('#disponibilidad').val({{ $producto->ProductoItem->idDisponibilidad }});
					$('#disponibilidad').trigger('change');
				}
			});

			$('#disponibilidad').val({{ $producto->ProductoItem->idDisponibilidad }});
			$('#disponibilidad').trigger('change');
		}
	});

	@endif

});

function registrarOrden(id) {
	var url = '{{ route('admin.orden.create', ['idProducto']) }}';
	url = url.replace('idProducto', id);
	var modal = openModal(url, 'Registrar Orden', null, { size: 'modal-lg' });
	setModalHandler('formOrdenCreate:success', function( event, data ){

		$('#disponibilidad').val({{ \App\Models\ProductoItem::DISPONIBILIDAD_VENDIDO }});
		$('#disponibilidad').trigger('change');

		swal( 'Orden registrada exitosamente.', {
			title: 'Listo!',
			icon: "success",
			buttons: {
				goOrden: {
					text: 'Ir a la orden',
					value: 'goOrden',
					visible: true,
					className: 'btn btn-danger',
				},
				ok: {
					text: 'Cerrar',
					value: 'ok',
					visible: true,
					className: 'btn btn-primary',
				},
			}
		}).then(function(value) {

			switch (value) {
				case "goOrden":
					var url = '{{ route('admin.ordenInbox.show', ['idOrden']) }}';
					url = url.replace('idOrden', data.id);
					redirect(url);
					break;

				default:
					dismissModal(modal);
					datatable.ajax.reload();
			}
		});
	});
}

function guardarAnuncio(){
	var url = '{{ route('admin.announcementInbox.update', [$pub->id]) }}';
	var data = {
		precio_final: $('#precio_final').val(),
		precioFijo: ($('#chkPrecioFijo').prop('checked')) ? 1 : 0,
	};

}

function guardarCalculadora(){

	var url = '{{ route('admin.announcementInbox.updateCalculadora', [$pub->id]) }}';
	var data = {
		informe_tecnico: $('#informe_tecnico').val(),
		seguro_extra: $('#seguro_extra').val(),
		tasa_tramite: $('#tasa_tramite').val(),
		contenedor_compartido: $('#contenedor_compartido').val(),
		contenedor_exclusivo: $('#contenedor_exclusivo').val(),
		envio_abierto: $('#envio_abierto').val(),
		@foreach($destinos as $destino)
			destino_{{ $destino->id }}: $('#txtDest_{{ $destino->id }}').val(),
		@endforeach
		@foreach($destinos as $destino)
			chk_{{ $destino->id }}: ($('#chkDest_{{ $destino->id }}').prop('checked')) ? 1 : 0,
		@endforeach
	};

	ajaxPatch(url, data, function(data){
		console.log(data);
	});
}

function guardar(estado){

	// ----------

	if($('#chkPrecioFijo').prop('checked')) {
		var valid = true;
		if($('#precio_usd').val() <= 0) {
			valid = false;
		}
		if($('#precio_eur').val() <= 0) {
			valid = false;
		}
		if($('#precio_sek').val() <= 0) {
			valid = false;
		}
		if(valid === false){
			swal("Debe proveer los precios fijados en USD, EUR y SEK.  Caso contrario por favor desactive la casilla de 'Precio Fijo'");
			return false;
		}
	}

	// ----------

	var url = '{{ route('admin.announcementInbox.updateCalculadora', [$pub->id]) }}';
	var data = {
		informe_tecnico: $('#informe_tecnico').val(),
		seguro_extra: $('#seguro_extra').val(),
		contenedor_compartido: $('#contenedor_compartido').val(),
		contenedor_exclusivo: $('#contenedor_exclusivo').val(),
		envio_abierto: $('#envio_abierto').val(),
		tasa_tramite: $('#tasa_tramite').val(),
//		destino_1: $('#destino_1').val(),
//		destino_2: $('#destino_2').val(),
//		destino_3: $('#destino_3').val(),
//		destino_4: $('#destino_4').val(),
//		destino_5: $('#destino_5').val(),
//		destino_6: $('#destino_6').val(),
		@foreach($destinos as $destino)
		destino_{{ $destino->id }}: $('#txtDest_{{ $destino->id }}').val(),
		@endforeach
		@foreach($destinos as $destino)
		chkDest_{{ $destino->id }}: ($('#chkDest_{{ $destino->id }}').prop('checked')) ? 1 : 0,
		@endforeach
		@foreach($transportes as $transporte)
		transporte_{{ $transporte->id }}: $('#txtTrans_{{ $transporte->id }}').val(),
		@endforeach
		@foreach($transportes as $transporte)
		chkTrans_{{ $transporte->id }}: ($('#chkTrans_{{ $transporte->id }}').prop('checked')) ? 1 : 0,
		@endforeach
		precioFijo: ($('#chkPrecioFijo').prop('checked')) ? 1 : 0,
	};

	ajaxPatch(url, data, function(data){
		var url = '{{ route('admin.announcementInbox.update', [$pub->id]) }}';
		var data = {
//			precio_final: $('#precio_final').val(),
			precio_usd: $('#precio_usd').val(),
			precio_eur: $('#precio_eur').val(),
			precio_sek: $('#precio_sek').val(),

			precioFijo: ($('#chkPrecioFijo').prop('checked')) ? 1 : 0,
			precioSudamerica: ($('#chkPrecioSudamerica').prop('checked')) ? 1 : 0,
			estado: estado,
			disponibilidad: $('#disponibilidad').val(),
		};

		ajaxPatch(url, data, function(data){
			console.log(data);
		});
	});


}

{{--var map = null;--}}
{{--var geocoder = null;--}}
{{--var marker = null;--}}
{{--function initPublishMap() {--}}
	{{--var uluru = {lat: -25.363, lng: 131.044};--}}
	{{--map = new google.maps.Map(document.getElementById('publish_map'), {--}}
		{{--scrollwheel: false,--}}
		{{--zoom: 4,--}}
		{{--center: uluru,--}}
{{--//			query: 'Bolivia'--}}
	{{--});--}}

	{{--geocoder = new google.maps.Geocoder();--}}

	{{--var address = '{{ $pub->Producto->ProductoItem->Ubicacion->direccion }}';--}}
	{{--geocodeAddress(geocoder, map, address);--}}

{{--//	google.maps.event.addListener(map, 'click', function(event) {--}}
{{--//		placeMarker(event.latLng, map);--}}
{{--//	});--}}

{{--//		var marker = new google.maps.Marker({--}}
{{--//			position: uluru,--}}
{{--//			map: map--}}
{{--//		});--}}
{{--}--}}

function placeMarker(location, map) {
	if (marker === null){
		marker = new google.maps.Marker({
			position: location,
			map: map
		});
	} else {
		marker.setPosition(location);
	}

//		marker = new google.maps.Marker({
//			position: location,
//			map: map
//		});
	map.panTo(location);
}
function geocodeAddress(geocoder, resultsMap, address) {
//		var address = 'Bolivia';
//		var address = $("#pais option:selected").text();
	geocoder.geocode({'address': address}, function(results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: resultsMap,
					position: results[0].geometry.location
				});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}

function buscarEnMapa(){
	var address = '{{ $pub->Producto->ProductoItem->Ubicacion->direccion }}';
	map.setZoom(15);
	geocodeAddress(geocoder, map, address);
}

// ----------

function clienteShow(id){
	var url = '{{ route('admin.cliente.show', ['idUsuario']) }}';
	url = url.replace('idUsuario', id);
	var modal = openModal(url, 'Cliente', null, {
		'size': 'modal-lg'
	});
}


function agendaShow(id){
	var url = '{{ route('admin.agenda.show', [0]) }}';
	url = url.replace(0, id);
	var modal = openModal(url, 'Tarjeta de identificación', null, {
		'size': 'modal-80'
	});
//		setModalHandler('formClienteShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
}

// ----------

function translateField(key){

	var url = '{{ route('admin.producto.translation', [ $pub->Producto->id, 'campo' ]) }}';
	url = url.replace('campo', key);
	var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
	setModalHandler('formTranslation:aceptar', function () {
		dismissModal(modal);
		loadDetail({{$pub->id}});
//		datatable.ajax.reload();
	});
}

function translateFieldML(key){

	var url = '{{ route('admin.producto.translationML', [ $pub->Producto->id, 'campo' ]) }}';
	url = url.replace('campo', key);
	var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
	setModalHandler('formTranslation:aceptar', function () {
		dismissModal(modal);
		loadDetail({{$pub->id}});
//		datatable.ajax.reload();
	});
}

function openTransporteDisponibilidad(idPublicacion, idTransporte) {
	var url = '{{ route('admin.announcementInbox.transporteDisponibilidad', ['idPublicacion', 'idTransporte' ]) }}';
	url = url.replace('idPublicacion', idPublicacion);
	url = url.replace('idTransporte', idTransporte);
	var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
	setModalHandler('formTransporteDisponibilidad:success', function () {
		dismissModal(modal);
		{{--loadDetail({{$pub->id}});--}}
	});
}

function productoEdit(mirror){
	if( mirror === undefined ) {
		mirror = 0;
	}
	var url = '{{ route('admin.producto.edit', [ $pub->Producto->id, 'mirror' => 'getMirror' ]) }}';
	url = url.replace('getMirror', mirror);
	var modal = openModal(url, 'Editar Producto', null, { size: 'modal-80' });
	setModalHandler('formProductoEdit:aceptar', function(){
		dismissModal(modal);
	});
}


</script>
@endpush