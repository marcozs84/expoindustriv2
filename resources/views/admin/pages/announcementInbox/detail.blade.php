<!-- begin wrapper -->
<div id="message_{{ $msg->id }}" class="wrapper">
	<h3 class="m-t-0 m-b-15 f-w-500">Bootstrap v4.0 is coming soon</h3>
	<ul class="media-list underline m-b-15 p-b-15">
		<li class="media media-sm clearfix">
			<a href="javascript:;" class="pull-left">
				<img class="media-object rounded-corner" alt="" src="../assets/img/user/user-12.jpg" />
			</a>
			<div class="media-body">
				<div class="email-from text-inverse f-s-14 f-w-600 m-b-3">
					from support@wrapbootstrap.com
				</div>
				<div class="m-b-3"><i class="fa fa-clock fa-fw"></i> Today, 8:30 AM</div>
				<div class="email-to">
					To: nguoksiong@live.co.uk
				</div>
			</div>
		</li>
	</ul>
	<ul class="attached-document clearfix">
		<li class="fa-file">
			<div class="document-file">
				<a href="javascript:;">
					<i class="fa fa-file-pdf"></i>
				</a>
			</div>
			<div class="document-name"><a href="javascript:;">flight_ticket.pdf</a></div>
		</li>
		<li class="fa-camera">
			<div class="document-file">
				<a href="javascript:;">
					<img src="../assets/img/gallery/gallery-11.jpg" alt="" />
				</a>
			</div>
			<div class="document-name"><a href="javascript:;">front_end_mockup.jpg</a></div>
		</li>
	</ul>

	<p class="f-s-12 text-inverse p-t-10">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vel auctor nisi, vel auctor orci. <br />
		Aenean in pretium odio, ut lacinia tellus. Nam sed sem ac enim porttitor vestibulum vitae at erat.
	</p>
	<p class="f-s-12 text-inverse">
		Curabitur auctor non orci a molestie. Nunc non justo quis orci viverra pretium id ut est. <br />
		Nullam vitae dolor id enim consequat fermentum. Ut vel nibh tellus. <br />
		Duis finibus ante et augue fringilla, vitae scelerisque tortor pretium. <br />
		Phasellus quis eros erat. Nam sed justo libero.
	</p>
	<p class="f-s-12 text-inverse">
		Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.<br />
		Sed tempus dapibus libero ac commodo.
	</p>
	<br />
	<br />
	<p class="f-s-12 text-inverse">
		Best Regards,<br />
		Sean.<br /><br />
		Information Technology Department,<br />
		Senior Front End Designer<br />
	</p>
</div>
<!-- end wrapper -->