@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Inbox')

@push('css')
<style>
	.list-email .email-time {
		width:120px;
	}
</style>
@endpush

@section('content')
	<!-- begin vertical-box -->
	<div class="vertical-box with-grid inbox">
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column width-200 bg-silver hidden-xs">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper bg-silver text-center">
					<a href="javascript:;" onclick="publicacionCreate()" class="btn btn-inverse p-l-40 p-r-40 btn-sm">
						Crear nuevo
					</a>
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin wrapper -->
								<div class="wrapper p-0">
									<div class="nav-title"><b>FOLDERS</b></div>
									<ul class="nav nav-inbox">
										<li class="btn_filter active"><a href="javascript:;" onclick="regresar()"><i class="fa fa-inbox fa-fw m-r-5"></i> Inbox <span class="badge pull-right">52</span></a></li>
										<li class="btn_filter btn_filter_{{ \App\Models\Publicacion::ESTADO_APPROVED }}"><a href="javascript:;" onclick="filterEstado('{{ \App\Models\Publicacion::ESTADO_APPROVED }}')"><i class="fa fa-thumbs-up fa-fw m-r-5"></i> Aprobados</a></li>
										<li class="btn_filter btn_filter_{{ \App\Models\Publicacion::ESTADO_REJECTED }}"><a href="javascript:;" onclick="filterEstado('{{ \App\Models\Publicacion::ESTADO_REJECTED }}')"><i class="fa fa-thumbs-down fa-fw m-r-5"></i> Rechazados</a></li>
										<li class="btn_filter btn_filter_{{ \App\Models\Publicacion::ESTADO_AWAITING_APPROVAL }}"><a href="javascript:;" onclick="filterEstado('{{ \App\Models\Publicacion::ESTADO_AWAITING_APPROVAL }}')"><i class="fa fa-check-square fa-fw m-r-5"></i> Esperando aprobación</a></li>
										<li class="btn_filter btn_filter_{{ \App\Models\Publicacion::ESTADO_DRAFT }}"><a href="javascript:;" onclick="filterEstado('{{ \App\Models\Publicacion::ESTADO_DRAFT }}')"><i class="fa fa-eraser fa-fw m-r-5"></i> Borradores</a></li>
										<li class="btn_filter"><a href="javascript:;"><i class="fa fa-search fa-fw m-r-5"></i> Todos</a></li>
										<li class="btn_filter"><a href="javascript:;"><i class="fa fa-trash fa-fw m-r-5"></i> Eliminados</a></li>
									</ul>
									{{--<div class="nav-title"><b>LABEL</b></div>--}}
									{{--<ul class="nav nav-inbox">--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-inverse"></i> Admin</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-primary"></i> Designer & Employer</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-success"></i> Staff</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-warning"></i> Sponsorer</a></li>--}}
										{{--<li><a href="javascript:;"><i class="fa fa-fw f-s-10 m-r-5 fa-circle text-danger"></i> Client</a></li>--}}
									{{--</ul>--}}
								</div>
								<!-- end wrapper -->
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
		<!-- begin vertical-box-column -->
		<div class="vertical-box-column bg-white">
			<!-- begin vertical-box -->
			<div class="vertical-box">
				<!-- begin wrapper -->
				<div class="wrapper bg-silver-lighter">
					<!-- begin btn-toolbar -->
					<div id="controlsMessage" class="clearfix hide">
						<div class="pull-left">
							<div class="btn-group m-r-5">
								{{--<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-reply f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Reply</span></a>--}}
								<a href="javascript:;" onclick="regresar()" class="btn btn-white btn-sm"><i class="fa fa-arrow-left f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Regresar</span></a>
							</div>
							<div class="btn-group m-r-5">
								<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-trash f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Delete</span></a>
								<a href="javascript:;" class="btn btn-white btn-sm"><i class="fa fa-archive f-s-14 m-r-3 m-r-xs-0 t-plus-1"></i> <span class="hidden-xs">Archive</span></a>
							</div>

							<div class="btn-group m-r-5">
								<a href="javascript:;" id="btnLang_es" onclick="changeLanguagePub('es')" class="btn btn-white chLan btn-sm"><span class="hidden-xs">ESP</span></a>
								<a href="javascript:;" id="btnLang_en" onclick="changeLanguagePub('en')" class="btn btn-white chLan btn-sm"><span class="hidden-xs">ENG</span></a>
								<a href="javascript:;" id="btnLang_se" onclick="changeLanguagePub('se')" class="btn btn-white chLan btn-sm"><span class="hidden-xs">SWE</span></a>
							</div>

							<div class="btn-group m-r-5">
								<a href="javascript:;" id="btnComparar" onclick="mostrarAnuncioOriginal('es')" class="btn btn-white chLan btn-sm"><span class="hidden-xs">Comparar con original</span></a>
							</div>
						</div>
						<div class="pull-right">
							<div class="btn-group">
								<a href="email_inbox.html" class="btn btn-white btn-sm disabled"><i class="fa fa-arrow-up f-s-14 t-plus-1"></i></a>
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-arrow-down f-s-14 t-plus-1"></i></a>
							</div>
							<div class="btn-group m-l-5">
								<a href="email_inbox.html" class="btn btn-white btn-sm"><i class="fa fa-times f-s-14 t-plus-1"></i></a>
							</div>
						</div>
					</div>
					<div id="controsListing" class="btn-toolbar ">
						<div class="btn-group m-r-5">
							<a href="javascript:;" class="p-t-5 pull-left m-r-3 m-t-2" data-click="email-select-all">
								<i class="far fa-square fa-fw text-muted f-s-16 l-minus-2"></i>
							</a>
						</div>
						<!-- begin btn-group -->
						<div class="btn-group dropdown m-r-5">
							<button class="btn btn-white btn-sm" data-toggle="dropdown">
								Ver todos <span class="caret m-l-3"></span>
							</button>
							<ul class="dropdown-menu text-left text-sm">
								<li class="active"><a href="javascript:;" onclick="reloadListing(1)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> Todos</a></li>
								@foreach($vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id') as $id => $nombre)
									<li><a href="javascript:;" onclick="reloadListing(1, {{ $id }})"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> {{ $nombre }}</a></li>
								@endforeach
							</ul>
						</div>

						<div class="btn-group dropdown m-r-5">
							<button class="btn btn-white btn-sm" data-toggle="dropdown">
								Mensajes por página ( <span id="lblPageSize"></span> ) <span class="caret m-l-3"></span>
							</button>
							<ul class="dropdown-menu text-left text-sm">
								<li class="active"><a href="javascript:;"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> All</a></li>
								<li><a href="javascript:;" onclick="setPageLength(10)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 10 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(30)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 30 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(50)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 50 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(100)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 100 </a></li>
								<li><a href="javascript:;" onclick="setPageLength(200)"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i> 200 </a></li>
							</ul>
						</div>

						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group m-r-5">
							<a href="javascript:;" onclick="reloadListing()" class="btn btn-sm btn-white"><i class="fa fa-redo f-s-14 t-plus-1"></i></a>
						</div>
						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group m-r-5">
							<a href="javascript:;" class="btn btn-sm btn-white hide" data-email-action="approve" onclick="estadoAnnouncements({{ \App\Models\Publicacion::ESTADO_APPROVED }})"><i class="fa t-plus-1 fa-thumbs-up f-s-14 m-r-3"></i> <span class="hidden-xs">Aprobar</span></a>
							<a href="javascript:;" class="btn btn-sm btn-white hide" data-email-action="archive" onclick="estadoAnnouncements({{ \App\Models\Publicacion::ESTADO_REJECTED }})"><i class="fa t-plus-1 fa-thumbs-down f-s-14 m-r-3"></i> <span class="hidden-xs">Rechazar</span></a>
							<a href="javascript:;" class="btn btn-sm btn-white hide" data-email-action="delete" onclick="estadoAnnouncements({{ \App\Models\Publicacion::ESTADO_DRAFT }})"><i class="fa t-plus-1 fa-eraser f-s-14 m-r-3"></i> <span class="hidden-xs">Borrador</span></a>
						</div>
						<div class="btn-group">
							<a href="javascript:;" class="btn btn-sm btn-white hide" data-email-action="delete" onclick="deleteAnnouncements()"><i class="fa t-plus-1 fa-trash f-s-14 m-r-3"></i> <span class="hidden-xs">Eliminar</span></a>
						</div>
						<!-- end btn-group -->
						<!-- begin btn-group -->
						<div class="btn-group ml-auto">
							<a href="javascript:;" class="btn btn-white btn-sm btnPrevList">
								<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
							</a>
							<a href="javascript:;" class="btn btn-white btn-sm btnNextList">
								<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
							</a>
						</div>
						<!-- end btn-group -->
					</div>
					<!-- end btn-toolbar -->
				</div>
				<!-- end wrapper -->
				<!-- begin vertical-box-row -->
				<div class="vertical-box-row">
					<!-- begin vertical-box-cell -->
					<div class="vertical-box-cell">
						<!-- begin vertical-box-inner-cell -->
						<div class="vertical-box-inner-cell">
							<!-- begin scrollbar -->
							<div data-scrollbar="true" data-height="100%">
								<!-- begin list-email -->
								<div id="listingHolder" class="listingHolder m-0 p-0">
									<ul id="messagesHolder" class="list-group list-group-lg no-radius list-email">
									</ul>
								</div>
								<!-- end list-email -->
								<div id="detailHolder" class="detailHolder m-0 p-0 hide">
								</div>
							</div>
							<!-- end scrollbar -->
						</div>
						<!-- end vertical-box-inner-cell -->
					</div>
					<!-- end vertical-box-cell -->
				</div>
				<!-- end vertical-box-row -->
				<!-- begin wrapper -->
				<div class="wrapper bg-silver-lighter clearfix">
					<div class="btn-group pull-right">
						<a href="javascript:;" class="btn btn-white btn-sm btnPrevList">
							<i class="fa fa-chevron-left f-s-14 t-plus-1"></i>
						</a>
						<a href="javascript:;" class="btn btn-white btn-sm btnNextList">
							<i class="fa fa-chevron-right f-s-14 t-plus-1"></i>
						</a>
					</div>
					<div class="m-t-5 text-inverse f-w-600 msgCounter">1,232 messages</div>
				</div>
				<!-- end wrapper -->
			</div>
			<!-- end vertical-box -->
		</div>
		<!-- end vertical-box-column -->
	</div>
	<!-- end vertical-box -->
<div class="hide">
<li class="list-group-item unread" id="stub_messageItem">
	<div class="email-checkbox">
		<label>
			<i class="far fa-square"></i>
			<input type="checkbox" data-checked="email-checkbox" />
		</label>
	</div>
	<a href="javascript:;" class="email-user bg-gradient-blue">
		<span class="text-white">F</span>
	</a>
	<div class="email-info">
		<a href="javascript:;">
			<span class="email-time">Today</span>
			<span class="email-sender">Facebook Blueprint</span>
			<span class="email-title">Newly released courses, holiday marketing tips, how-to video, and more!</span>
			<span class="email-desc">Sed scelerisque dui lacus, quis pellentesque lorem tincidunt rhoncus. Nulla accumsan elit pharetra, lacinia turpis nec, varius erat.</span>
		</a>
	</div>
</li>
</div>
@endsection

@push('scripts')
<script src="/assets/js/demo/email-inbox.demo.js?id=11"></script>
<script>

var idMessage = {{ $idMessage }};
var lastIdMessage = 0;
var inboxPageLength = 10;
var currentPage = 1;
$(document).ready(function() {
	InboxV2.init();

	reloadListing();

	if(idMessage > 0){
		loadDetail(idMessage);
	}

	$('#btnLang_{{ $lang }}').addClass('active');
});

var pubEstados = {!! json_encode($pubEstados) !!};

var idEstado = 0;
function filterEstado(estado) {
	idEstado = estado;
	$('.btn_filter').removeClass('active');
	$('.btn_filter_' + estado).addClass('active');
	reloadListing();
}

function publicacionCreate(){
	var url = '{{ route('admin.publicacion.create') }}';
	var modal = openModal(url, 'Nueva Publicacion', null, { size:'modal-lg' });
	setModalHandler('formPublicacionCreate:success', function(){
		dismissModal(modal);
	});
}

function setPageLength(size) {
	window.localStorage.setItem('inboxPageLength', size);
	reloadListing();
}

function reloadListing(page, idVendedor){
	var url = '{{ route('resource.anuncio.ds') }}';
	var urlItem = '{{ route('admin.announcementInbox.show', [0]) }}';

	var ls = window.localStorage.getItem('inboxPageLength');
	if( ls !== null ) {
		inboxPageLength = ls;
		$('#lblPageSize').html(ls);
	}

	var data = {
		length: inboxPageLength
	};
	if(page !== undefined) {
		data['page'] = page;
	} else {
		data['page'] = currentPage;
	}
	if(idVendedor !== undefined) {
		data['idVendedor'] = idVendedor;
	}
	if( idEstado > 0 ) {
		data['estados'] = idEstado;
	}
	var mh = $('#messagesHolder');
	mh.html('');
	var loadMsg = 0;
	var urlShow = '';
	ajaxPost(url, data, function(data){
		var dt = data.data;

		$('.msgCounter').html(data.from + ' - ' + data.to + ' de ' + data.total + ' Anuncios');

		currentPage = data.current_page;
		if(data.current_page > 1) {
			$('.btnPrevList').show().attr('onclick', 'reloadListing('+ (parseInt(data.current_page, 10) - 1) +')');
		} else {
			$('.btnPrevList').hide();
		}
		if(data.current_page === data.last_page) {
			$('.btnNextList').hide();
		} else {
			$('.btnNextList').show().attr('onclick', 'reloadListing('+ (parseInt(data.current_page, 10) + 1) +')');
		}

		var errores = '';
		$.each(dt, function(key, row){
			try {
				loadMsg = row.id;
				var item = $('#stub_messageItem').clone();
				item.removeAttr('id');
				urlShow = urlItem.replace('idPublicacion', row.id);
//			item.find('.email-user, .email-info a').prop('href', urlShow);
				item.find('.email-user, .email-info a').prop('href', 'javascript:;');
				item.find('.email-user, .email-info a').attr('onclick', 'loadDetail('+row.id+')');
				item.find('.email-checkbox input').prop('id', row.id);
				item.find('.email-checkbox input').val(row.id);
				item.find('.email-user span').html(row.owner.owner.agenda.nombres_apellidos.substring(0, 1));

				item.find('.email-info .email-time').html(format_datetime(row.created_at));
				item.find('.email-info .email-sender').html(row.owner.email);
//			item.find('.email-info .email-title').html(row.producto.marca.nombre+' '+row.producto.modelo.nombre);
//			item.find('.email-info .email-title').html( '[ '+ row.id +' ] - ' + row.producto.marca.nombre+' '+row.producto.modelo.nombre);

				// ----------
				var name = '';
				if(row.producto){
					if(row.producto.idMarca > 0){
						name += row.producto.marca.nombre;
						name += ' ';
					} else {
						{{--name += '<b>@lang('messages.no_brand')</b>';--}}
					}

					if(row.producto.idModelo > 0){
						name += row.producto.modelo.nombre;
					} else {
						{{--name += '<b>@lang('messages.no_model')</b>';--}}
					}

					if( name === '' ) {
//						name = get_trans('nombre', row.producto.producto_item.traducciones, 'en');
						name = row.producto.nombre_alter;
					}

					if(name === '') {
						name = '<span class="text-danger" style="font-weight:bold;">SIN MARCA Y MODELO</span>'
					}

					name += ' Precio: ' + row.producto.precioProveedor + " " + row.producto.monedaProveedor;

					item.find('.email-info .email-title').html( '[ '+ row.id +' ] - ' + name );
//								return name;
				} else {
					name =  '<b class="text-red">@lang('messages.no_product')!</b>';
				}
				// ----------

				var badge = '';
				row.estado = parseInt(row.estado, 10);
				switch(row.estado){
					case 1:
						badge = 'badge-secondary';
						break;
					case 2:
						badge = 'badge-yellow';
						break;
					case 3:
						badge = 'badge-primary';
						break;
					case 4:
						badge = 'badge-danger';
						break;
					case 5:
						badge = 'badge-warning';
						break;
				}

				var tpg = '';
				var tpgt = '';
				row.idTipoPago = parseInt(row.idTipoPago, 10);
				switch(row.idTipoPago){
					case {{ \App\Models\Publicacion::PAGO_STRIPE }}:
						tpg = 'badge-primary';
						tpgt = 'Stripe';
						break;
					case {{ \App\Models\Publicacion::PAGO_FAKTURA }}:
						tpg = 'badge-warning';
						tpgt = 'Faktura';
						break;
					case {{ \App\Models\Publicacion::PAGO_SUBSCRIPTION }}:
						tpg = 'badge-info';
						tpgt = 'Subscripcion';
						break;
					default:
						tpg = 'badge-secondary';
						tpgt = 'None';
						break;
				}

				item.find('.email-info .email-desc').html('<span class="badge ' + badge + '">' + pubEstados[row.estado] +"</span>");
//				item.find('.email-info .email-desc').append(' | ' + row.producto.producto_item.ubicacion.direccion);
				item.find('.email-info .email-desc').append('&nbsp;&nbsp;<span class="badge ' + tpg + '">' + tpgt +"</span>");

				mh.append(item);
			} catch( err ) {
				errores += '[' + row.id + ']: ' + err + "\n";
			}
		});

		if(errores !== '') {
			swal({
				html: true,
				text: "No se pudo desplegar los siguientes anuncios: \n" + errores,
			});
		}

		if(loadMsg !== 0){
//			loadDetail(null, loadMsg);
		} else {
			console.log(" NOT LOADING DETAIL ");
		}
	}, null, { silent: true });
}

function loadDetail(id, lang){
//	if(e !== null){
//		e.preventDefault();
//	}

	lastIdMessage = id;

	window.history.pushState("", "", '/announcementInbox/'+id);

	if(lang !== undefined){
		var url = '{{ route('admin.announcementInbox.show', ['idPub', 'lang']) }}';
		url = url.replace('idPub', id);
		url = url.replace('lang', lang);
	} else {
		var url = '{{ route('admin.announcementInbox.show', ['idPub']) }}';
		url = url.replace('idPub', id);
	}
	var holder = $('#detailHolder');
	var msg = $('<div>').prop('id', 'msg_' + id);
	holder.html(msg);
	msg.load(url, function(response, status, xhr){
		$('#controlsMessage').removeClass('hide');
		$('#controsListing').addClass('hide');
		$('#detailHolder').removeClass('hide');
		$('#listingHolder').addClass('hide');
	});

	return false;
}

function regresar(){
	window.history.pushState("", "", '/announcementInbox');
	$('#controlsMessage').addClass('hide');
	$('#controsListing').removeClass('hide');
	$('#detailHolder').addClass('hide');
	$('#listingHolder').removeClass('hide');
	reloadListing(1);
}

function changeLanguagePub(lang){
	loadDetail(lastIdMessage, lang);
	$('.chLan').removeClass('active');
	$('#btnLang_'+lang).addClass('active');
}

function deleteAnnouncements() {

	var ids = [];
	$('.email-checkbox input').each(function() {
		if($(this).prop('checked')){
			ids.push($(this).val());
		}
	});
	if(ids.length === 0){
		alert("Debe seleccionar al menos 1 registro.");
		return false;
	}

	swal({
		text: '@lang('messages.wanna_remove_announcement')',
		icon: 'warning',
		buttons: {
			cancel: '@lang('form.no')',
			ok: '@lang('form.yes')'
		},
		dangerMode: true
	}).then(function(response) {

		if (response === 'ok') {
			console.log( "mande a borrar" );
			console.log( ids );

			var url = '{{ route('admin.announcementInbox.destroy', [0]) }}';

			ajaxDelete( url, ids, function () {
				swal( '@lang('messages.announcement_removed')', {icon: "success"} );

				var targetEmailList = '[data-checked=email-checkbox]:checked';
				if ( $( targetEmailList ).length !== 0 ) {
					$( targetEmailList ).closest( 'li' ).slideToggle( function () {
						$( this ).remove();
//					handleEmailActionButtonStatus();
						if ( $( '.list-email > li' ).length === 0 ) {
							$( '.list-email' ).html( '<li class="p-15 text-center"><div class="p-20"><i class="fa fa-trash fa-5x text-silver"></i></div> This folder is empty</li>' );
						}
					} );
				}
			} );
		}



		{{--var url = '{{ route('admin.announcements.delete', ['idAnnounce']) }}';--}}
		{{--url = url.replace('idAnnounce', id);--}}

		{{--if(response === 'ok') {--}}
			{{--console.log("removing announcement");--}}
			{{--ajaxDelete(url, {}, function(){--}}
				{{--swal('@lang('messages.announcement_removed')', { icon: "success" });--}}
				{{--datatable.ajax.reload(null, false);--}}
			{{--});--}}
		{{--}--}}

	});
}

function estadoAnnouncements( idEstado ) {

	var ids = [];
	$('.email-checkbox input').each(function() {
		if($(this).prop('checked')){
			ids.push($(this).val());
		}
	});
	if(ids.length === 0){
		alert("Debe seleccionar al menos 1 registro.");
		return false;
	}

	var estado_ltl = '';
	switch(idEstado) {
		case {{ \App\Models\Publicacion::ESTADO_DRAFT }}: estado_ltl = ' "borrador" ';
			break;
		case {{ \App\Models\Publicacion::ESTADO_AWAITING_APPROVAL }}: estado_ltl = ' "esperando aprobacion" ';
			break;
		case {{ \App\Models\Publicacion::ESTADO_APPROVED }}: estado_ltl = ' "aprobado" ';
			break;
		case {{ \App\Models\Publicacion::ESTADO_REJECTED }}: estado_ltl = ' "rechazado" ';
			break;
		case {{ \App\Models\Publicacion::ESTADO_DISABLED }}: estado_ltl = ' "desactivado" ';
			break;

	}

	swal({
		text: 'Desea cambiar el estado a: ' + estado_ltl + ' los ' + ids.length + ' anuncios?',
		icon: 'warning',
		buttons: {
			cancel: '@lang('form.no')',
			ok: '@lang('form.yes')'
		},
		dangerMode: true
	}).then(function(response) {

		if (response === 'ok') {
			console.log( "mande a aprobar" );
			console.log( ids );

			var url = '{{ route('admin.announcementInbox.estado') }}';

			ajaxPost( url, {
				ids: ids,
				idEstado: idEstado
			}, function () {
				swal( 'Elementos aprobados', {icon: "success"} );

				reloadListing();

				var targetEmailList = '[data-checked=email-checkbox]:checked';
				if ( $( targetEmailList ).length !== 0 ) {
					$( targetEmailList ).closest( 'li' ).slideToggle( function () {
						$( this ).remove();
//					handleEmailActionButtonStatus();
						if ( $( '.list-email > li' ).length === 0 ) {
							$( '.list-email' ).html( '<li class="p-15 text-center"><div class="p-20"><i class="fa fa-trash fa-5x text-silver"></i></div> This folder is empty</li>' );
						}
					} );
				}
			} );
		}



		{{--var url = '{{ route('admin.announcements.delete', ['idAnnounce']) }}';--}}
		{{--url = url.replace('idAnnounce', id);--}}

		{{--if(response === 'ok') {--}}
			{{--console.log("removing announcement");--}}
			{{--ajaxDelete(url, {}, function(){--}}
				{{--swal('@lang('messages.announcement_removed')', { icon: "success" });--}}
				{{--datatable.ajax.reload(null, false);--}}
			{{--});--}}
		{{--}--}}

	});
}

var comparando = false;

function mostrarAnuncioOriginal() {
	productoEdit(1);
//	comparando = !comparando;
//	if( comparando ) {
//		$('#btnComparar').addClass('btn-primary');
//		productoEdit(1);
//	} else {
//		$('#btnComparar').removeClass('btn-primary');
//	}
}
</script>
@endpush