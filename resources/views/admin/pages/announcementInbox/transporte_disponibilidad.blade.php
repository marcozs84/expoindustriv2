@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Traduccion</li>
	</ol>

	<h1 class="page-header">Traducción</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Disponibilidad de transporte</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTranslation" method="get" class="form-horizontal">
				<h5>Transporte disponible en los siguientes destinos:</h5>
				<table class="table table-condensed table-striped">

					@foreach($destinos as $destino)
						<tr>
							<td class="tblLabel" style="width:50%;">
								{{ $destino->id }}) {{ $destino->nombre }} <br>
								{{ $destino->Pais->nombre }}<br>

							</td>
							<td class="tblValue">
								<input id="chkDest_{{ $destino->id }}" type="checkbox" value="{{ $destino->id }}" class="form-control chk_destinos"
								       @if(in_array($destino->id, $destinosPub)) checked @endif
								       @if($destino->estado == 0) disabled @endif
								>
								{{--{!! Form::checkbox('destino_'.$destino->id, $destino->id, null, [--}}
									{{--'id' => 'destino_'.$destino->id,--}}
									{{--'class' => 'form-control chk_destinos',--}}
								{{--]) !!}--}}
							</td>

						</tr>
					@endforeach

				</table>
				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="transporteDisponibilidadCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="transporteDisponibilidadSave()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@section('libraries')
@stop

@push('scripts')
	<script>

		$( "#formTranslation" ).submit(function( event ) {
			translationSave();
			event.preventDefault();
		});

		function transporteDisponibilidadSave(){

			var ids = [];
			$('.chk_destinos').each(function() {
				if($(this).prop('checked')){
					ids.push($(this).val());
				}
			});
			if(ids.length === 0){
				alert("Debe seleccionar al menos 1 destino.");
				return false;
			}

			var url = '{{ route('admin.announcementInbox.transporteDisponibilidad', [$idPublicacion, $idTransporte]) }}';
			ajaxPost(url, {
				idDestinos: ids
			}, function(){
				$(document).trigger('formTransporteDisponibilidad:success');
			});
		}

		function transporteDisponibilidadCancel(){
			$(document).trigger('formTransporteDisponibilidad:cancel');
		}

	</script>
@endpush

