@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.orden.index') }}">Orden</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $orden->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Orden</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Orden</h4>
		</div>
		<div class="panel panel-body">
			<form id="formOrden2Edit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Padre</label>
					<div class="col-8">
						{!! Form::number('idPadre', $orden->idPadre, [
							'id' => 'idPadre',
							'placeholder' => 'Id Padre',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cliente</label>
					<div class="col-8">
						{!! Form::number('idCliente', $orden->idCliente, [
							'id' => 'idCliente',
							'placeholder' => 'Id Cliente',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cotizacion</label>
					<div class="col-8">
						{!! Form::number('idCotizacion', $orden->idCotizacion, [
							'id' => 'idCotizacion',
							'placeholder' => 'Id Cotizacion',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Ubicacion Entrega</label>
					<div class="col-8">
						{!! Form::number('idUbicacionEntrega', $orden->idUbicacionEntrega, [
							'id' => 'idUbicacionEntrega',
							'placeholder' => 'Id Ubicacion Entrega',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Vendedor</label>
					<div class="col-8">
						{!! Form::number('idVendedor', $orden->idVendedor, [
							'id' => 'idVendedor',
							'placeholder' => 'Id Vendedor',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Horario</label>
					<div class="col-8">
						{!! Form::number('idHorario', $orden->idHorario, [
							'id' => 'idHorario',
							'placeholder' => 'Id Horario',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Orden Tipo</label>
					<div class="col-8">
						{!! Form::number('idOrdenTipo', $orden->idOrdenTipo, [
							'id' => 'idOrdenTipo',
							'placeholder' => 'Id Orden Tipo',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Orden Estado</label>
					<div class="col-8">
						{!! Form::number('idOrdenEstado', $orden->idOrdenEstado, [
							'id' => 'idOrdenEstado',
							'placeholder' => 'Id Orden Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Entrega</label>
					<div class="col-8">
						{!! Form::text('fechaEntrega', ($orden->fechaEntrega ? $orden->fechaEntrega->format('d/m/Y') : ''), [
							'id' => 'fechaEntrega',
							'placeholder' => 'Fecha Entrega',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio Transporte</label>
					<div class="col-8">
						{!! Form::text('precioTransporte', $orden->precioTransporte, [
							'id' => 'precioTransporte',
							'placeholder' => 'Precio Transporte',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio Facturado</label>
					<div class="col-8">
						{!! Form::text('precioFacturado', $orden->precioFacturado, [
							'id' => 'precioFacturado',
							'placeholder' => 'Precio Facturado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre Factura</label>
					<div class="col-8">
						{!! Form::text('nombreFactura', $orden->nombreFactura, [
							'id' => 'nombreFactura',
							'placeholder' => 'Nombre Factura',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nit</label>
					<div class="col-8">
						{!! Form::text('nit', $orden->nit, [
							'id' => 'nit',
							'placeholder' => 'Nit',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{!! Form::text('created_at', ($orden->created_at ? $orden->created_at->format('d/m/Y') : ''), [
							'id' => 'created_at',
							'placeholder' => 'Created_at',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="ordenCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ordenSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif

$( "#formOrden2Edit" ).submit(function( event ) {
	ordenSave();
	event.preventDefault();
});

function ordenSave(){
	var url = '{{ route('resource.orden.store') }}';
	ajaxPost(url, {
		idPadre: 		$('#formOrden2Edit #idPadre').val(),
		idCliente: 	$('#formOrden2Edit #idCliente').val(),
		idCotizacion: 	$('#formOrden2Edit #idCotizacion').val(),
		idUbicacionEntrega: $('#formOrden2Edit #idUbicacionEntrega').val(),
		idVendedor: 	$('#formOrden2Edit #idVendedor').val(),
		idHorario: 	$('#formOrden2Edit #idHorario').val(),
		idOrdenTipo: 	$('#formOrden2Edit #idOrdenTipo').val(),
		idOrdenEstado: $('#formOrden2Edit #idOrdenEstado').val(),
		fechaEntrega: 	$('#formOrden2Edit #fechaEntrega').val(),
		precioTransporte: $('#formOrden2Edit #precioTransporte').val(),
		precioFacturado: $('#formOrden2Edit #precioFacturado').val(),
		nombreFactura: $('#formOrden2Edit #nombreFactura').val(),
		nit: 			$('#formOrden2Edit #nit').val(),
		created_at: 	$('#formOrden2Edit #created_at').val()
	}, function(){
		$(document).trigger('formOrden2Edit:success');
	});
}

function ordenCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formOrden2Edit:cancel');
	@else
		redirect('{{ route('admin.orden.index')  }}');
	@endif
}

function formInit(){

	$('#formOrden2Edit #fechaEntrega, #formOrden2Edit #created_at').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		todayHighlight: true,
		orientation: 'bottom'
	})
		.on('changeDate', function(){
		});
	$("#formOrden2Edit #fechaEntrega, #formOrden2Edit #created_at").mask("99/99/9999");

}

</script>
@endpush

