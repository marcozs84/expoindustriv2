@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
    .col-form-label, .row.form-group>.col-form-label {
        padding:0px;
        text-align:right;
    }
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Orden</li>
	</ol>

	<h1 class="page-header">Nuevo Orden</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Orden</h4>
		</div>
		<div class="panel panel-body">
			<form id="formOrden2Show" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $orden->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Padre</label>
					<div class="col-8">
						{{ $orden->idPadre }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cliente</label>
					<div class="col-8">
						{{ $orden->idCliente }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cotizacion</label>
					<div class="col-8">
						{{ $orden->idCotizacion }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Ubicacion Entrega</label>
					<div class="col-8">
						{{ $orden->idUbicacionEntrega }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Vendedor</label>
					<div class="col-8">
						{{ $orden->idVendedor }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Horario</label>
					<div class="col-8">
						{{ $orden->idHorario }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Orden Tipo</label>
					<div class="col-8">
						{{ $orden->idOrdenTipo }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Orden Estado</label>
					<div class="col-8">
						{{ $orden->idOrdenEstado }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Entrega</label>
					<div class="col-8">
						{{ ($orden->fechaEntrega ? $orden->fechaEntrega->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio Transporte</label>
					<div class="col-8">
						{{ $orden->precioTransporte }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio Facturado</label>
					<div class="col-8">
						{{ $orden->precioFacturado }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre Factura</label>
					<div class="col-8">
						{{ $orden->nombreFactura }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nit</label>
					<div class="col-8">
						{{ $orden->nit }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{{ ($orden->created_at ? $orden->created_at->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>


                <div class="hr-line-dashed"></div>
                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="ordenCancel()">Cerrar</a>
                    </div>
                </div>
            </form>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif


function ordenCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formOrden2Show:cancel');
	@else
		redirect('{{ route('admin.orden.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

