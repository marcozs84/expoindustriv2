@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}

	.valores td {
		text-align:center;
		color: #000000;
		font-weight: bold;
	}
	.labels td {
		text-align:center;
		background-color: #EDEDED;
		/*padding: 3px !important;*/
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.orden.index') }}">Orden</a></li>
		<li class="breadcrumb-item active">Nuevo/a orden</li>
	</ol>

	<h1 class="page-header">Nuevo Orden</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Orden</h4>
		</div>
		<div class="panel panel-body">
			<form id="formOrdenCreate" method="post" class="form-horizontal row">

				@if($usuario->isAdmin())
					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Vendedor</label>
						<div class="col-8">
							{!! Form::select('idVendedor', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id'), $idSelectedVendedor, [
								'id' => 'idVendedor',
								'class' => 'form-control default-select2',
								'autofocus'
							]) !!}
						</div>
					</div>
				@else
					<div class="form-group col-md-6 col-sm-12 row m-b-15">
						<label class="col-form-label col-4">Vendedor</label>
						<div class="col-8">
							{!! Form::text('idVendedor', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id')->toArray()[$idSelectedVendedor], [
							'id' => 'idVendedor',
							'placeholder' => '',
							'class' => 'form-control',
							'disabled'
						]) !!}
						</div>
					</div>
				@endif

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::select('idOrdenEstado', $ordenEstados, null, [
							'id' => 'idOrdenEstado',
							'class' => 'form-control default-select2',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15 group-cliente">
					<label class="col-form-label col-4">Empresa / Cliente</label>
					<div class="col-8">
						{!! Form::select('idCliente', [], null, [
							'id' => 'idCliente',
							'class' => 'form-control',
							'autofocus'
						]) !!}
						<a href="javascript:;" class="btn btn-primary" onclick="clienteCreate()" style="margin-top:5px;">Nuevo Cliente</a>
						<a href="javascript:;" class="btn btn-primary btnClienteEdit hide" onclick="clienteEdit()" style="margin-top:5px;">Editar Cliente</a>
					</div>
				</div>


				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Entrega</label>
					<div class="col-8">
						{!! Form::text('fechaEntrega', '', [
							'id' => 'fechaEntrega',
							'placeholder' => 'Fecha Entrega',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


				<div class="col-md-12 p-b-10 " id="pnlContacto" style="display:none;">
					<table class="table tblProveedor">
						<tr class="active">
							<th style="width:50%">Datos Contacto</th>
							<th style="width:50%">Datos Empresa</th>
						</tr>
						<tr class="active">
							<td>
								<address>
									<strong class="pContacto"></strong><br />
									<a href="javascript:;" class="text-black"><span class="pCorreoContacto"></span></a>
									<br>
									<strong>Telefonos:</strong> <span class="pTelefonos"></span>
								</address>
							</td>
							<td>
								<address>
									<strong class="pNombreProv">Twitter, Inc.</strong><br />
									<span class="pDireccion"></span>
									<br>
									<strong>Telefonos:</strong> <span class="pTelefonos"></span>
								</address>
								<strong>Otros contactos:</strong><br>
								<span class="pContactos"></span>
							</td>
						</tr>
					</table>
				</div>



				<div class="form-group col-md-6 col-sm-12 row m-b-15 group-cliente">
					<label class="col-form-label col-4">Maquina</label>
					<div class="col-8">
						{!! Form::select('idProducto', [], null, [
							'id' => 'idProducto',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="col-md-12 p-b-10 panel-info" id="pnlMaquina" style="display:none;">
					<table class="table table-condensed table-bordered">
						<tr class="labels active">
							<td class="labels">Marca</td>
							<td class="labels">Modelo</td>
							<td class="labels">Categoria</td>
							<td class="labels">SubCategoria</td>
						</tr>
						<tr class="valores">
							<td class="valores lblMarca"></td>
							<td class="valores lblModelo"></td>
							<td class="valores lblCategoria"></td>
							<td class="valores lblSubcategoria"></td>
						</tr>
						<tr class="labels active">
							<td>Precio compra</td>
							<td>Precio Proveedor</td>
							<td>Precio Venta</td>
							<td>Precio Fijo</td>
						</tr>
						<tr class="valores text-normal">
							<td class="lblPrecioCompra"></td>
							<td class="lblPrecioProveedor"></td>
							<td class="lblPrecioVenta"></td>
							<td class="lblPrecioFijo"></td>
						</tr>
					</table>

				</div>


				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Precio Facturado</label>
					<div class="col-8">
						{!! Form::text('precioFacturado', '', [
							'id' => 'precioFacturado',
							'placeholder' => 'Precio Facturado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Moneda</label>
					<div class="col-8">
						{!! Form::select('moneda', $moneda, ($global_country_abr == 'se') ? 'sek' : '', [
							'id' => 'moneda',
							'class' => 'form-control default-select2',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nombre Factura</label>
					<div class="col-8">
						{!! Form::text('nombreFactura', '', [
							'id' => 'nombreFactura',
							'placeholder' => 'Nombre Factura',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Nit o código tributario</label>
					<div class="col-8">
						{!! Form::text('nit', '', [
							'id' => 'nit',
							'placeholder' => 'Nit',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="ordenCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ordenSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

	var idProducto = 0;

$( "#formOrdenCreate" ).submit(function( event ) {
	ordenSave();
	event.preventDefault();
});

function ordenSave(){
	var url = '{{ route('admin.orden.store') }}';
	ajaxPost(url, {
		idPadre: 		0,
		idCliente: 	    $('#formOrdenCreate #idCliente').val(),
		idCotizacion: 	0,
		idUbicacionEntrega: 0,
		@if($usuario->isAdmin())
		idVendedor: 	$('#formOrdenCreate #idVendedor').val(),
		@else
		idVendedor: 	{{ $idSelectedVendedor }},
		@endif
		idHorario: 	    0,
		idOrdenTipo: 	0,
		idOrdenEstado:  $('#formOrdenCreate #idOrdenEstado').val(),
		idProducto:     idProducto, //$('#formOrdenCreate #idProducto').val(),
		fechaEntrega: 	$('#formOrdenCreate #fechaEntrega').val(),
		precioTransporte: 0,
		precioFacturado: $('#formOrdenCreate #precioFacturado').val(),
		moneda: $('#formOrdenCreate #moneda').val(),
		nombreFactura:  $('#formOrdenCreate #nombreFactura').val(),
		nit: 			$('#formOrdenCreate #nit').val(),
	}, function( data ){
		$(document).trigger('formOrdenCreate:success', data.data);
	});
}

function ordenCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formOrdenCreate:cancel');
	@else
		redirect('{{ route('admin.orden.index')  }}');
	@endif
}

function formInit(){

	@if($usuario->isAdmin())
		$('#formOrdenCreate #idVendedor').select2();
	@endif

	$('#formOrdenCreate #fechaEntrega, #formOrdenCreate #created_at').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		todayHighlight: true,
		orientation: 'bottom'
	})
		.datepicker("setDate",'now')
		.on('changeDate', function(){
		});
	$("#formOrdenCreate #fechaEntrega, #formOrdenCreate #created_at").mask("99/99/9999");


	$('#formOrdenCreate #idCliente').select2({
//        allowClear: true,
		placeholder: {
			id: "",
			placeholder: "Leave blank to ..."
		},
		ajax: {
			{{--				url: '{{ route('resource.proveedorContacto.ds') }}',--}}
			url: '{{ route('resource.cliente.ds') }}',
			dataType: 'json',
			method: 'POST',
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			delay: 250,
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page,
					proveedor_data: 1 // Utilizado para que el 'ds' consultado retorne informacion más completa
				};
			},
			statusCode: {
				422: ajaxValidationHandler,
				500: ajaxErrorHandler
			},
			processResults: function (data, params) {
				// parse the results into the format expected by Select2
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data, except to indicate that infinite
				// scrolling can be used
				params.page = data.current_page || 1;

				return {
					results: data.data,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: function(repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if (repo.loading) return repo.text;

			var companyName = '';
			if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
				companyName = '- - -';
			} else {
				companyName = repo.usuario.owner.proveedor.nombre;
			}
			var otrosContactos = [];

			var markup = "<div>" + repo.agenda.nombres_apellidos + '<br><b>' + companyName + "</b><br>" + otrosContactos.join('<br> ') + "</div>";
//				var markup = "<div><b>" + repo.nombre + '</b><br>' + repo.descripcion + "</div>";
			return markup;
		},
		templateSelection: function formatRepoSelection (repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if(repo.usuario){

				var companyName = '';
				if(repo.usuario.owner.proveedor.nombre === '' || repo.usuario.owner.proveedor.nombre === null) {
					companyName = '- - -';
				} else {
					companyName = repo.usuario.owner.proveedor.nombre;
				}

				$('#formOrdenCreate #nuevoCliente option[value="0"]').attr('selected', 'selected');

				// ----------

				$('#formOrdenCreate .pNombreProv').html(companyName);

				var otrosContactos = [];
				if(repo.usuario.owner.proveedor.proveedor_contactos && repo.usuario.owner.proveedor.proveedor_contactos.length > 0) {
					$.each(repo.usuario.owner.proveedor.proveedor_contactos, function(k,v) {
						if(parseInt(v.id, 10) !== parseInt(repo.id, 10)) {
							otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
						}
					});
				}

				var telefonos = [];
				if(repo.usuario.owner.proveedor.telefonos && repo.usuario.owner.proveedor.telefonos.length > 0) {
					$.each(repo.usuario.owner.proveedor.telefonos, function(k,v) {
						if(v.id !== repo.id) {
							telefonos.push(v.numero);
						}
					});
				}

				var direcciones = [];
				if(repo.usuario.owner.proveedor.direcciones && repo.usuario.owner.proveedor.direcciones.length > 0) {
					$.each(repo.usuario.owner.proveedor.direcciones, function(k,v) {
						if(v.id !== repo.id) {
							direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
						}
					});
				}

				$('#formOrdenCreate .pContacto').html(repo.agenda.nombres_apellidos);
				$('#formOrdenCreate .pCorreoContacto').html(repo.usuario.username);
				$('#formOrdenCreate .pContactos').html(otrosContactos.join('<br> '));
				$('#formOrdenCreate .pCorreos').html(repo.usuario.owner.proveedor.correos);
				$('#formOrdenCreate .pTelefonos').html(telefonos.join(', '));
				$('#formOrdenCreate .pDireccion').html(direcciones.join('<br>'));
				$('#formOrdenCreate #nombreFactura').val( ( ! repo.agenda.nombreFactura  ? repo.agenda.nombres_apellidos : repo.agenda.nombreFactura ) );
				$('#formOrdenCreate #nit').val( repo.agenda.nit );

//				$('#pnlContacto').removeClass('hide');

				// ----------

				return repo.agenda.nombres_apellidos + ' [' + companyName + ']';
			} else {
//					$('#formOrdenCreate .btnProveedorEdit').addClass('hide');
				return repo.text;
			}
			//return repo.nombre + ' ' + repo.apellidos || repo.text;
		},
//	    allowClear: true
	}).on('', function() {
		$('#pnlContacto').slideDown('slow');
	});

	$('#formOrdenCreate #idProducto').select2({
//        allowClear: true,
		placeholder: {
			id: "",
			placeholder: "Leave blank to ..."
		},
		ajax: {
			url: '{{ route('resource.productoItem.ds') }}',
			dataType: 'json',
			method: 'POST',
			headers: {
				'X-CSRF-Token' : '{!! csrf_token() !!}'
			},
			delay: 250,
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page
				};
			},
			statusCode: {
				422: ajaxValidationHandler,
				500: ajaxErrorHandler
			},
			processResults: function (data, params) {
				// parse the results into the format expected by Select2
				// since we are using custom formatting functions we do not need to
				// alter the remote JSON data, except to indicate that infinite
				// scrolling can be used
				params.page = data.current_page || 1;

				return {
					results: data.data,
					pagination: {
						more: (params.page * 30) < data.total
					}
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: function(repo) {
			//	if (repo.placeholder) return repo.placeholder;
			if (repo.loading) return repo.text;

			var owner_pub = '';
			if(repo.producto) {
				owner_pub = repo.producto.owner.owner.agenda.nombres_apellidos;
				owner_pub += ' ( ' + repo.producto.owner.owner.proveedor.nombre + ')';
			}
			var markup = "<div><b>" + repo.producto.marca.nombre + ' ' + repo.producto.modelo.nombre + '</b><br>' + owner_pub + "</div>";
			return markup;
		},
		templateSelection: function formatRepoSelection (repo) {
			//	if (repo.placeholder) return repo.placeholder;

			if(repo.producto){
				return repo.producto.marca.nombre + ' - ' + repo.producto.modelo.nombre;
			} else {
				return repo.text;
			}
			//return repo.nombre + ' ' + repo.apellidos || repo.text;
		}
//	    allowClear: true
	}).on('select2:select', function(selection) {
		repo = selection.params.data;
		idProducto = repo.producto.id;
		$('.lblMarca').html(repo.producto.marca.nombre);
		$('.lblModelo').html(repo.producto.modelo.nombre);
		$('.lblCategoria').html(repo.producto.categorias[0].nombre);
		$('.lblSubcategoria').html(repo.producto.categorias[0].padre.nombre);

		$('.lblPrecioCompra').html( makeLink( repo.producto.precioCompra, repo.producto.monedaCompra ));
		$('.lblPrecioProveedor').html( makeLink( repo.producto.precioProveedor, repo.producto.monedaProveedor ));
		$('.lblPrecioVenta').html( makeLink( repo.producto.precioUnitario, repo.producto.monedaUnitario ));
		$('.lblPrecioFijo').html( makeLink( repo.producto.precioFijoUsd, 'usd') + ' ' +  makeLink( repo.producto.precioFijoEur, 'eur' ) + ' ' + makeLink( repo.producto.precioFijoSek, 'sek' ) );


		$('#pnlMaquina').slideDown('slow');
	});

	@if( $producto )

	idProducto = {{ $producto->id }};
	var option = $('<option>', {
			value: {{ $producto->id }},
			'selected' : 'selected'
		}).html('{{ $producto->Marca->nombre }} {{ $producto->Modelo->nombre }}');
	$('#formOrdenCreate #idProducto').html('');
	$('#formOrdenCreate #idProducto').append(option).trigger('change');

	$('.lblMarca').html('{{ $producto->Marca->nombre }}');
	$('.lblModelo').html('{{ $producto->Modelo->nombre }}');
	$('.lblCategoria').html('{{ $producto->Categorias[0]->nombre }}');
	$('.lblSubcategoria').html('{{ $producto->Categorias[0]->Padre->nombre }}');

	$('.lblPrecioCompra').html( makeLink( '{{ $producto->precioCompra }}', '{{ $producto->monedaCompra }}' ));
	$('.lblPrecioProveedor').html( makeLink( '{{ $producto->precioProveedor }}', '{{ $producto->monedaProveedor }}' ));
	$('.lblPrecioVenta').html( makeLink( '{{ $producto->precioUnitario }}', '{{ $producto->monedaUnitario }}' ));
	$('.lblPrecioFijo').html( makeLink( '{{ $producto->precioFijoUsd }}', 'usd') + ' ' +  makeLink( '{{ $producto->precioFijoEur }}', 'eur' ) + ' ' + makeLink( '{{ $producto->precioFijoSek }}', 'sek' ) );


	$('#pnlMaquina').slideDown('slow');

	@endif

}

function check(param) {
	return ( ! param ? '' : param ) ;
}

function makeLink(precio, moneda) {
	monedaLow = moneda;
	precio = check(precio);
	moneda = check(moneda).toUpperCase();
	return '<a href="javascript:;" onclick="usePrice('+precio+', \''+monedaLow+'\')">'+ precio + " " + moneda +'</a>';
}

function usePrice(precio, moneda) {
	$('#precioFacturado').val(precio);
	$('#moneda').val(moneda);
}

formInit();

// ---------- CUSTOM

function clienteCreate(){
	var url = '{{ route('admin.usuario.create', [ 'tipo' => 'cliente' ]) }}';
	var modal = openModal(url, 'Nuevo Cliente');
	setModalHandler('formUsuarioCreate:aceptar', function(event, data){
		var usuario = data;
		if(usuario.owner.proveedor.nombre === null) {
			usuario.owner.proveedor.nombre = '';
		}
		var companyName = '';
		if(usuario.owner.proveedor.nombre === '' || usuario.owner.proveedor.nombre === null) {
			companyName = '- - -';
		} else {
			companyName = usuario.owner.proveedor.nombre;
		}
		var option = $('<option>', {
			value: usuario.cliente.id,
			'selected' : 'selected'
		}).html(usuario.owner.agenda.nombres_apellidos + ' [' + companyName + ']');
		$('#formOrdenCreate #idCliente').html('');
		$('#formOrdenCreate #idCliente').append(option).trigger('change');

		$('#formOrdenCreate #nuevoCliente option[value="1"]').attr('selected', 'selected');

		$('#formOrdenCreate .pNombreProv').html(companyName);

		var otrosContactos = [];
		if(usuario.owner.proveedor.proveedor_contactos && usuario.owner.proveedor.proveedor_contactos.length > 0) {
			$.each(usuario.owner.proveedor.proveedor_contactos, function(k,v) {
				if(v.id !== usuario.id) {
					otrosContactos.push(v.agenda.nombres_apellidos + '&lt;' + v.usuario.username + '&gt;');
				}
			});
		}

		var telefonos = [];
		if(usuario.owner.proveedor.telefonos && usuario.owner.proveedor.telefonos.length > 0) {
			$.each(usuario.owner.proveedor.telefonos, function(k,v) {
				if(v.id !== usuario.id) {
					telefonos.push(v.numero);
				}
			});
		}

		var direcciones = [];
		if(usuario.owner.proveedor.direcciones && usuario.owner.proveedor.direcciones.length > 0) {
			$.each(usuario.owner.proveedor.direcciones, function(k,v) {
				if(v.id !== usuario.id) {
					direcciones.push(v.direccion.replace(/\r\n|\r|\n/g,"<br />"));
				}
			});
		}

		$('#formOrdenCreate .pContacto').html(usuario.owner.agenda.nombres_apellidos);
		$('#formOrdenCreate .pCorreoContacto').html(usuario.username);
		$('#formOrdenCreate .pContactos').html(otrosContactos.join('<br> '));
		$('#formOrdenCreate .pCorreos').html(usuario.owner.proveedor.correos);
		$('#formOrdenCreate .pTelefonos').html(telefonos.join(', '));
		$('#formOrdenCreate .pDireccion').html(direcciones.join('<br>'));

		$('#pnlContacto').removeClass('hide');

		dismissModal(modal);
	});
}

</script>
@endpush

