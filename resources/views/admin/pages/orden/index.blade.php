@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', ' Ordens')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />

<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"> Ordens</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"> Ordenes <small></small></h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Búsqueda</h4>
		</div>
		<div id="formSearchOrden" class="panel-body p-b-0">
			<div class="row" id="searchForm">
				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="periodoBusqueda">Periodo</label>
					<div style="width:100%;">
						<div id="periodoBusqueda" class="btn btn-default btn-block text-left f-s-12">
							<i class="fa fa-caret-down pull-right m-t-2"></i>
							<span></span>
						</div>
					</div>
				</div>

				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="vendedorId" style="">Vendedor</label>
					{!! Form::select('vendedorId', $vendedores->pluck('Owner.Agenda.nombres_apellidos', 'id'), null, [
						'id' => 'vendedorId',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ofertaDelete()"><i class="fa fa-trash"></i> Eliminar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:buscar();"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:limpiar();">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title"> Ordenes</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ordenCreate()"><i class="fa fa-plus"></i> Crear</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="ordenDelete()"><i class="fa fa-trash"></i> Eliminar</a>
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	var originalInicio = moment('2018/01/01', 'YYYY/M/D');
	var originalFin = moment();

	var fechaInicio = originalInicio;
	var fechaFin = originalFin;

	$(document).ready(function(){
		initSearchForm();
		initDataTable();
	});

	function initSearchForm() {

		$('#formSearchOrden #vendedorId').select2()
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});

		$('#formSearchOrden #periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
		$('#formSearchOrden #periodoBusqueda').daterangepicker({
			format: 'DD/MM/YYYY',
			startDate: originalInicio,
			endDate: originalFin.add(1, 'days'),
			minDate: '01/01/2018',
			maxDate: '31/12/{{ date('Y') + 1 }}',
			dateLimit: { months: 36 },
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			linkedCalendars: false,
			ranges: {
				'Hoy día': [moment(), moment()],
				'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Esta semana': [moment().startOf('week'), moment().endOf('week')],
				'Semana anterior': [moment().subtract(1, 'weeks').startOf('week'), moment().subtract(1, 'weeks').endOf('week')],
				'Últimos 7 días': [moment().subtract(1, 'weeks'), moment()],
				'Últimas 2 semanas': [moment().subtract(2, 'weeks'), moment()],
				'Últimos 30 días': [moment().subtract(1, 'month'), moment()],
				'Este més': [moment().startOf('month'), moment().endOf('month')],
				'Més anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
				'Últimos 3 meses': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')],
				'Último semestre': [moment().subtract(6, 'month').startOf('month'), moment().endOf('month')],
				'Último año': [moment().subtract(12, 'month').startOf('month'), moment().endOf('month')]

			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' a ',
//			singleDatePicker: true,
			locale: {
				applyLabel: 'Aceptar',
				cancelLabel: 'Cancelar',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Personalizado',
				daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#periodoBusqueda span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			fechaInicio = start;
			fechaFin = end;
			datatable.ajax.reload();
		});
	}

	function buscar() {
		datatable.ajax.reload();
	}

	function limpiar() {
		$('#vendedorId').val([]).trigger('change');
		fechaInicio = originalInicio;
		fechaFin = originalFin;
		$('#periodoBusqueda span').html(fechaInicio.format('MMMM D, YYYY') + ' - ' + fechaFin.format('MMMM D, YYYY'));
		datatable.ajax.reload();
	}


	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.orden.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.fechaInicio = fechaInicio.format('YYYY/M/D');
						d.fechaFin = fechaFin.format('YYYY/M/D');
						d.idVendedor = $('#vendedorId').val();
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                    var data = JSON.parse(data);
//	                    for(var i = 0 ; i < data.data.length; i++){
//	                        // ...
//	                    }
//	                    return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
					{ data: 'idCliente', title: 'IdCliente'},
					{ data: 'idCotizacion', title: 'Cotizacion'},
					{ data: 'idVendedor', title: 'Vendedor'},
					{ data: 'idHorario', title: 'IdHorario'},
					{ data: 'idOrdenTipo', title: 'IdOrdenTipo'},
					{ data: 'idOrdenEstado', title: 'IdOrdenEstado'},
					{ data: 'fechaEntrega', title: 'FechaEntrega'},
					{ data: 'precioTransporte', title: 'PrecioTransporte'},
					{ data: 'precioFacturado', title: 'PrecioFacturado'},
					{ data: 'nombreFactura', title: 'NombreFactura'},
					{ data: 'nit', title: 'Nit'},
					{ data: 'created_at', title: 'Created_at'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
				    @php $counter = 0 @endphp
				    {
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '30px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="ordenShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// idCliente,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							var html = '';
							html += '<a href="javascript:;" onclick="clienteShow('+ row.idCliente +')" >'+row.cliente.agenda.nombres_apellidos+'</a>';
							return html;
						}
					},{
						// idCotizacion,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							var html = '';
							if( row.idCotizacion > 0 ) {
								html += '<a href="javascript:;" onclick="cotizacionShow('+ row.idCotizacion +')" >Cotizacion</a>';
							}
							return html;
						}
					},{
						// idVendedor,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							var html = '';
							html += '<a href="javascript:;" onclick="usuarioShow('+ row.idVendedor +')" >'+row.vendedor.owner.agenda.nombres_apellidos+'</a>';
							return html;
						}
					},{
						// idHorario,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// idOrdenTipo,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// idOrdenEstado,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// fechaEntrega,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					},{
						// precioTransporte,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// precioFacturado,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// nombreFactura,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// nit,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// created_at,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '100px',
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';
                            html += '<a href="javascript:;" onclick="ordenEdit('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
                            return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
				    }
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function ordenShow(id){
		var url = '{{ route('admin.orden.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Orden', null, {
			'size': 'modal-lg'
		});
	}

	function ordenCreate(){
		var url = '{{ route('admin.orden.create') }}';
		var modal = openModal(url, 'Nuevo Orden', null, { size:'modal-lg' });
		setModalHandler('formOrdenCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function ordenEdit(id){
		var url = '{{ route('admin.orden.index') }}/'+id+'/edit';
		var modal = openModal(url, 'Editar Orden', null, { size:'modal-lg' });
		setModalHandler('formOrdenEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function ordenDelete(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length === 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.orden.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

	function clienteShow(id){
		var url = '{{ route('admin.cliente.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Cliente', null, {
			'size': 'modal-lg'
		});
	}

	function cotizacionShow(id){
		var url = '{{ route('admin.cotizacion.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Cotizacion', null, {
			'size': 'modal-lg'
		});
	}

	function usuarioShow(id){
		var url = '{{ route('admin.usuario.show', ['idUsuario']) }}';
		url = url.replace('idUsuario', id);
		var modal = openModal(url, 'Usuario', null, {
			'size': 'modal-lg'
		});
	}

</script>
@endpush