@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Agenda</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Agenda <small></small></h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Búsqueda</h4>
		</div>
		<div id="formSearchCliente" class="panel-body p-b-0">
			<div class="row" id="searchForm">

				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="idUsuario" style="">Empresa</label>
					{!! Form::select('idProveedor', [], null, [
						'id' => 'idProveedor',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>
				<div class="form-group col-md-6 col-sm-6 col-lg-6" style="vertical-align:top;">
					<label for="idUsuario" style="">Actividad</label>
					{!! Form::select('idActividad', [
					'1' => 'Comprador',
					'2' => 'Vendedor'
					], null, [
						'id' => 'idActividad',
						'class' => 'form-control multiple-select2',
						'style' => 'width:100%;',
						'multiple' => 'multiple'
					]) !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="productoCreate()"><i class="fa fa-plus"></i> Crear</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="productoDelete()"><i class="fa fa-trash"></i> Eliminar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:;" onclick="buscar()"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:;" onclick="limpiar()">Limpiar</a>

					{{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

				</div>
			</div>
		</div>
	</div>

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Agenda</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				{{--<a class="btn btn-primary btn-sm hide" href="javascript:;" onclick="agendaCreate()"><i class="fa fa-plus"></i> Crear</a>--}}
				{{--<a class="btn btn-primary btn-sm" href="javascript:;" onclick="agendaDelete()"><i class="fa fa-trash"></i> Eliminar</a>--}}
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;

	$(document).ready(function(){
		initSearch();
		initDataTable();
	});

	function initSearch() {
		$('#idActividad').select2()
			.on('select2:select', function (e) {
				datatable.ajax.reload();
			})
			.on('select2:unselect', function (e) {
				datatable.ajax.reload();
			});

		var searchProveedor = $('#formSearchCliente').find('#idProveedor');

		searchProveedor.select2({
            allowClear: true,
			placeholder: {
				id: "",
				placeholder: "Leave blank to ..."
			},
			ajax: {
				url: '{{ route('resource.proveedor.ds') }}',
				dataType: 'json',
				method: 'POST',
				headers: {
					'X-CSRF-Token' : '{!! csrf_token() !!}'
				},
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page
					};
				},
				statusCode: {
					422: ajaxValidationHandler,
					500: ajaxErrorHandler
				},
				processResults: function (data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = data.current_page || 1;

					return {
						results: data.data,
						pagination: {
							more: (params.page * 30) < data.total
						}
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			minimumInputLength: 1,
			templateResult: function(repo) {
				//	if (repo.placeholder) return repo.placeholder;
				if (repo.loading) return repo.text;

				return repo.nombre;
//				var owner_pub = '';
//				if(repo.producto) {
//					owner_pub = repo.producto.owner.owner.agenda.nombres_apellidos;
//					owner_pub += ' ( ' + repo.producto.owner.owner.proveedor.nombre + ')';
//				}
//				var markup = "<div><b>" + repo.producto.marca.nombre + ' ' + repo.producto.modelo.nombre + '</b><br>' + owner_pub + "</div>";
//				return markup;
			},
			templateSelection: function formatRepoSelection (repo) {
				//	if (repo.placeholder) return repo.placeholder;

				return repo.nombre;
				//return repo.nombre + ' ' + repo.apellidos || repo.text;
			}
//	    allowClear: true
		}).on('select2:select', function(selection) {
			datatable.ajax.reload();
			repo = selection.params.data;

		}).on('select2:unselect', function(selection) {
			datatable.ajax.reload();
			repo = selection.params.data;

		});
	}

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.proveedorContacto.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.proveedor_data = 1;
						d.idProveedor = $('#idProveedor').val();
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: 'Nombres y apellidos'},
					{ title: 'E-mail'},
					{ title: 'Empresa'},
					{ title: 'Comprador/Vendedor'},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'}
					// ...
				],
				columnDefs: [
					@php $counter = 0 @endphp
					{
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '30px'
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
							return '<a href="javascript:;" onclick="agendaShow('+row.id+')">'+row.agenda.nombres_apellidos+'</a>';

							var url = '{{ route('admin.agenda.show', [0]) }}';
							url = url.replace('0', row.id);
							if(row.agenda !== undefined) {
								return '<a href="'+url+'">'+row.agenda.nombres_apellidos+'</a>';
							} else {
								return '<a href="'+url+'">'+'---'+'</a>';
							}

						}
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
							return row.usuario.username;
						}
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
//							if( row.usuario.owner.proveedor ) {
//								return row.usuario.owner.proveedor.nombre;
//							} else {
//								return '---';
//							}

							return row.proveedor.nombre;

						}
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
							return 'Comprador';
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function agendaShow(id){
		var url = '{{ route('admin.agenda.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Tarjeta de identificación', null, {
			'size': 'modal-80'
		});
//		setModalHandler('formClienteShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}


	function agendaCreate(){
		var url = '';
		var modal = openModal(url, 'Nuevo Cliente');
		setModalHandler('formClienteCreate:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function agendaEdit(id){
		var url = '';
		url = url.replace('idCLiente', id);
		var modal = openModal(url, 'Editar Cliente');
		setModalHandler('formClienteEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function agendaDelete(){
		alert("No está permitido eliminar elementos de la agenda");
		return false;
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length == 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '';

		ajaxDelete(url, ids, function(){
			datatable.ajax.reload(null, false);
		});
	}

</script>
@endpush