@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />

<style>
	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}

	/* SOLO PARA SHOW */
	.form-group .col-sm-8 {
		padding-top:7px;
	}
	.form-group label {
		padding-top:3px;
	}

</style>


<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}


	.valores td {
		text-align:center;
		color: #000000;
		font-weight: bold;
	}
	.labels td {
		text-align:center;
		background-color: #EDEDED;
		/*padding: 3px !important;*/
	}

	.attached-document>li .document-name {
		white-space: none;
	}
	.text-right{
		font-weight:bold;
		width:30%;
		vertical-align: middle !important;
	}
	.panel-body h4{
		border-bottom:1px solid #e5e5e5;
		padding-bottom:7px;
		margin-bottom:10px;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.agenda.index') }}">Agenda</a></li>
		<li class="breadcrumb-item active">Agenda #{{ $pc->id }}</li>
	</ol>

	<h1 class="page-header">Detalles</h1>

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Datos de contacto</h4>
				</div>
				<div class="panel panel-body">

					<table class="table table-condensed table-striped">
						<tr>
							<td class="text-right">Nombres y apellidos</td>
							<td>{{ $pc->Agenda->nombres_apellidos }} ({{ $pc->id }})</td>
						</tr>
						<tr>
							<td class="text-right">E-mail</td>
							<td>
								@foreach($pc->Agenda->Correos as $correo)
									{{ $correo->correo }} ({{ ucfirst($correo->descripcion) }})<br>
								@endforeach
							</td>
						</tr>
						<tr>
							<td class="text-right">Telefonos</td>
							<td>
								@foreach($pc->Agenda->Telefonos as $telefono)
									{{ $telefono->numero }} ({{ ucfirst($telefono->descripcion) }})<br>
								@endforeach
							</td>
						</tr>
						<tr>
							<td class="text-right">País</td>
							@if(isset($global_paises[$pc->Agenda->pais]))
								<td>{{ $global_paises[$pc->Agenda->pais] }} ({{ strtoupper($pc->Agenda->pais) }})</td>
							@else
								<td></td>
							@endif

						</tr>
						<tr>
							<td class="text-right">Stripe ID</td>
							<td>
								{{ $pc->Usuario->getStripeId() }}
							</td>
						</tr>
						<tr>
							<td class="text-right">Login As...</td>
							<td>
								<a class="btn btn-danger" target="_blank" href="{{ route('admin.auth_as', [ $pc->Usuario->id ]) }}"><i class="fa fa-magic"></i> Impersonar</a>
							</td>
						</tr>
					</table>

				</div>
			</div>


			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Información de acceso</h4>
				</div>
				<div class="panel panel-body">

					<form id="formUsuarioEdit" method="get" class="form-horizontal">
						<table class="table table-condensed table-striped">
							<tr>
								<td class="text-right">Usuario</td>
								<td><input type="text" readonly="" class="form-control-plaintext" value="{{ $pc->Usuario->username }}"></td>
							</tr>
							<tr>
								<td class="text-right">Contraseña</td>
								<td><input id="password" type="password" class="form-control" placeholder="Contraseña"></td>
							</tr>
							<tr>
								<td class="text-right">Confirmar contraseña</td>
								<td><input id="password_confirmation" type="password" class="form-control" placeholder="Confirmación de contraseña"></td>
							</tr>
							<tr>
								<td class="text-right"></td>
								<td class="text-right"><a href="javascript:;" onclick="actualizarContrasenia()" class="btn btn-sm btn-primary m-r-5">Guardar</a></td>
							</tr>
						</table>

					</form>

				</div>
			</div>






		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Datos de la empresa</h4>
				</div>
				<div class="panel panel-body">

					<table class="table table-condensed table-striped">
						<tr>
							<td class="text-right">Nombre</td>
							<td>{{ ($pc->Proveedor->nombre == '') ? '- - - ' : $pc->Proveedor->nombre }}</td>
						</tr>
						<tr>
							<td class="text-right">Padre</td>
							<td>
								@if($pc->Proveedor->idPadre != 0)
									{{ $pc->Proveedor->Padre->nombre }}
								@endif
							</td>
						</tr>
						<tr>
							<td class="text-right">Telefono</td>
							<td>{{ implode(', ', $pc->Proveedor->Telefonos->pluck('numero')->toArray()) }}</td>
						</tr>
						<tr>
							<td class="text-right">VAT</td>
							<td>{{ $pc->Proveedor->nit }}</td>
						</tr>
						<tr>
							<td class="text-right">Teléfonos</td>
							<td>
								@if($pc->Proveedor->Telefonos)
									{{ implode(', ', $pc->Proveedor->Telefonos->pluck('numero')->toArray()) }}
								@endif
							</td>
						</tr>
					</table>

					<a href="javascript:;" onclick="toggleCollapse('#masinfo_tbl')" class="pull-right">Ver mas información</a>


					<div id="masinfo_tbl" style="display:none; clear: both; padding-top:5px;">
						<table class="table table-condensed table-striped">
							<tr>
								<td class="text-right">Direccion</td>
								<td>
									@if($pc->Proveedor->Direcciones)
										{!! implode('<br> ', $pc->Proveedor->Direcciones->pluck('direccion')->toArray()) !!}
									@endif
								</td>
							</tr>
							<tr>
								<td class="text-right">Sitio Web</td>
								<td>{{ $pc->Proveedor->urlSitio }}</td>
							</tr>
							<tr>
								<td class="text-right">Facebook</td>
								<td>{{ $pc->Proveedor->urlFacebook }}</td>
							</tr>
							<tr>
								<td class="text-right">Twitter</td>
								<td>{{ $pc->Proveedor->urlTwitter }}</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Contactos</h4>
				</div>
				<div class="panel panel-body">

					<div class="row m-t-10" >

						@foreach($pc->Proveedor->ProveedorContactos as $contacto)

						<div class="col-lg-6 col-md-6 col-sm-6">
							<h5>{{ $contacto->Agenda->nombres_apellidos }}</h5>
							@foreach($contacto->Agenda->Correos as $correo)
								{{ $correo->correo }} ({{ ucfirst($correo->descripcion) }})<br>
							@endforeach
							Telf.:
							@foreach($contacto->Agenda->Telefonos as $telefono)
								{{ $telefono->numero }} ({{ ucfirst($telefono->descripcion) }})<br>
							@endforeach
							@if(isset($contacto->Agenda->pais) && isset($global_paises[$contacto->Agenda->pais]))
								<b>{{ $global_paises[$contacto->Agenda->pais] }} ({{ strtoupper($contacto->Agenda->pais) }})</b>
							@endif
						</div>


						@endforeach
					</div>

				</div>
			</div>

			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-info" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Planes contratados</h4>
				</div>
				<div class="panel panel-body">

					<div id="subscripciones_holder"></div>

					<div class="row">
						<div class="col col-12">
							<a href="javascript:;" id="btnCrearSubscripcion" onclick="crearSubscripcion()" class="btn btn-primary pull-right">Crear Subscripcion</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>




@stop

@push('scripts')

<script src="/assets/plugins/moment/moment.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script>

//	$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
//		formInit();
//	});

	@if($isAjaxRequest)
	formInit();
	@else
	$(function(){ formInit(); });
	@endif

	var idUsuarioPropietario = 0;

	function actualizarContrasenia(){
		var url = '{{ route('resource.usuario.update', [$pc->Usuario->id]) }}';
		var data = {
			password : $('#password').val(),
			password_confirmation : $('#password_confirmation').val()
		};
		ajaxPatch(url, data, function(data){
			swal('Contraseña actualizada');
		}, function(data){
			swal('No se pudo actualizar la contraseña', 'warning');
		});
	}

	function productoCancelar(){
		@if($isAjaxRequest)
			$(document).trigger('formProductoShow:cancelar');
		@else
			redirect('{{ route('admin.producto.index')  }}');
		@endif
	}

	function formInit(){

		updateSubscripcionList();

	}


	function crearSubscripcion() {
		var url = '{{ route('admin.subscripcion.create', ['idUsuario' => '_idu_']) }}';
		url = url.replace('_idu_', {{ $pc->Usuario->id }});
		var modal = openModal(url, 'Crear subscripcion');
		setModalHandler('formSubscripcionCreate:aceptar', function () {
			dismissModal(modal);
	//				datatable.ajax.reload();
			updateSubscripcionList();
		});
	}

	function updateSubscripcionList() {
		var url = '{{ route('admin.cliente.subscripcionesTable', ['idCliente' => $pc->Usuario->Cliente->id]) }}';
		$('#subscripciones_holder').load(url, null, function(data){

		});
	}

	function translateFieldML(key){
		var url = '{{ route('admin.traduccion.multiline', [ 'owner' => 'proveedor', 'owner_id' => $pc->Proveedor->id, 'campo' => 'descripcion' ]) }}';
		url = url.replace('key', key);
		var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
		setModalHandler('formTraduccion:success', function ( event, response ) {
			$('#descripcion_es').html( response.es );
			$('#descripcion_se').html( response.se );
			$('#descripcion_en').html( response.en );
			dismissModal(modal);
		});
	}


</script>
@endpush

