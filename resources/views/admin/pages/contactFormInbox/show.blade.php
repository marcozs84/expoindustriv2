@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default', ['contentFullHeight' => true])

@section('title', 'Email - Inbox')

@push('css')
<link href="/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.css" rel="stylesheet" />
@endpush

@section('content')

<style>
.tblLabel{
	text-align:right;
	font-weight:normal;
	vertical-align: middle !important;
}
.tblValue{
	text-align:left;
	font-weight:bold;
	vertical-align: middle !important;
}
.tblValue .input-group{
	margin-bottom:0px !important;
}
</style>
<!-- begin wrapper -->
<div id="message_{{ $cot->id }}" class="wrapper">
	<h3 class="m-t-0 m-b-15 f-w-500">{{ $pub->Producto->Marca->nombre }} {{ $pub->Producto->Modelo->nombre }}
		<span class="badge

		@if($pub->estado == 1)
			badge-secondary
		@elseif($pub->estado == 2)
			badge-yellow
		@elseif($pub->estado == 3)
			badge-primary
		@elseif($pub->estado == 4)
			badge-danger
		@endif
		" style="font-size:13px;">@lang('messages.'.$pubEstados[$pub->estado])</span>
	</h3>
	<ul class="media-list underline m-b-15 p-b-15">
		<li class="media media-sm clearfix">
			{{--<a href="javascript:;" class="pull-left">--}}
				{{--<img class="media-object rounded-corner" alt="" src="../assets/img/user/user-12.jpg" />--}}
			{{--</a>--}}
			<div class="media-body">
				<div class="email-from text-inverse f-s-14 f-w-600 m-b-3">
					Cliente: {{ $cot->Cliente->Agenda->nombres_apellidos }}  <b>&lt;{{ $cot->Cliente->Usuario->username }}&gt;</b>
				</div>
				<div class="m-b-3"><i class="fa fa-clock fa-fw"></i> {{ $pub->created_at->toFormattedDateString() }} {{ $pub->created_at->format('H:i') }} - ( {{ $pub->created_at->diffForHumans(\Carbon\Carbon::now()) }} )</div>
				<div class="m-b-3"><i class="fa fa-check-square fa-fw"></i> {{ $pub->Producto->Categorias[0]->Padre->nombre }} / {{ $pub->Producto->Categorias[0]->nombre }}</div>

				{{--<div class="email-to">To: nguoksiong@live.co.uk</div>--}}
			</div>
		</li>
	</ul>

	<div class="row">
		<div class="col col-md-6">
			<h4>Precio maquinaria</h4>
			<table class="table table-condensed table-striped">
				<tr>
					<td class="tblLabel">Precio Cotizado<br><small>Servicios adicionales incluidos</small></td>
					<td class="tblValue"><span style="font-weight: bold; font-size:15px;">{{ number_format($cot->definicion['precio_final'], 2) }} USD</span></td>
				</tr>
				<tr>
					<td class="tblLabel">Precio Publicación<br><small>cotización al día</small></td>
					<td class="tblValue">{{ number_format($cot->definicion['precio_original'], 2) }} {{ strtoupper($pub->Producto->monedaProveedor) }}</td>
				</tr>
				<tr>
					<td class="tblLabel">Precio Anunciante<br></td>
					<td class="tblValue">{{ number_format($pub->Producto->precioProveedor , 2) }} {{ strtoupper($pub->Producto->monedaProveedor) }}</td>
				</tr>
			</table>

			<div style="width:100%; text-align:center; margin-top:20px;">
				<a href="javascript:;" class="btn btn-md btn-primary" onclick="crearOrden()">Crear Orden</a>
			</div>

		</div>
		<div class="col col-md-6">


			<!-- begin nav-tabs -->
			<ul class="nav nav-pills">
				<li class="nav-items">
					<a href="#default-tab-1" data-toggle="tab" class="nav-link active">
						<span class="d-sm-none">Cotizacion</span>
						<span class="d-sm-block d-none">Datos de la cotización</span>
					</a>
				</li>
				<li class="nav-items">
					<a href="#default-tab-2" data-toggle="tab" class="nav-link">
						<span class="d-sm-none">Pagos</span>
						<span class="d-sm-block d-none">Pagos</span>
					</a>
				</li>
				{{--<li class="">--}}
					{{--<a href="#default-tab-3" data-toggle="tab" class="nav-link">--}}
						{{--<span class="d-sm-none">Tab 3</span>--}}
						{{--<span class="d-sm-block d-none">Default Tab 3</span>--}}
					{{--</a>--}}
				{{--</li>--}}
			</ul>
			<!-- end nav-tabs -->
			<!-- begin tab-content -->
			<div class="tab-content">
				<!-- begin tab-pane -->
				<div class="tab-pane fade active show" id="default-tab-1">
					<h4>Datos de la cotización</h4>
					<table class="table table-condensed table-striped">
						@if(isset($cot->definicion['calc']['informe_tecnico']))
							<tr>
								<td class="tblLabel">Informe Técnico:</td>
								<td class="tblValue">{{ $cot->definicion['calc']['informe_tecnico'] }} USD <br>
									{{--(<a href="https://dashboard.stripe.com/test/payments/{{ $cot->definicion[] }}">Ver pago</a>)--}}
								</td>
							</tr>
						@endif
						@if(isset($cot->definicion['calc']['seguro_extra']))
							<tr>
								<td class="tblLabel">Seguro de maquinaria:</td>
								<td class="tblValue">{{ $cot->definicion['calc']['seguro_extra'] }} USD</td>
							<tr>
						@endif
								<td class="tblLabel">Transporte:<br>
									<b>{{ \App\Models\Transporte::find($cot->definicion['transporte']['id'])->nombre }}</b></td>
								<td class="tblValue">{{ number_format($cot->definicion['transporte']['precio'], 2) }} USD</td>
							</tr>
							<tr>
								<td class="tblLabel">Destino:<br>
									<b>({{ \App\Models\Destino::find($cot->definicion['destino']['id'])->Pais->nombre }}:
										{!! \App\Models\Destino::find($cot->definicion['destino']['id'])->nombre !!})</b></td>
								<td class="tblValue">{{ number_format($cot->definicion['destino']['precio'], 2) }} USD</td>
							</tr>
							<tr>
								<td class="tblLabel"><b>TOTAL:</b></td>
								<td class="tblValue"><span style="font-weight: bold; font-size:15px;">{{ number_format($cot->definicion['precio_final'], 2) }} USD</span></td>
							</tr>
					</table>
				</div>
				<!-- end tab-pane -->
				<!-- begin tab-pane -->
				<div class="tab-pane fade" id="default-tab-2">
					<h4>Pagos</h4>
					<table class="table table-condensed table-striped">
						<thead>
						<tr>
							<th>Id</th>
							<th>Descripcion</th>
							<th>Monto</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
							@foreach($transacciones as $transaccion)
								<tr>
									<td>{{ $transaccion->id }}</td>
									<td>{{ $transaccion->producto }}</td>
									<td>{{ $transaccion->monto }} {{ $transaccion->moneda }}</td>
									<td>
										<a target="_blank" href="https://dashboard.stripe.com/test/payments/{{ $transaccion->idStripeTransaction }}">Ver pago</a>
									</td>
								</tr>
							@endforeach

						</tbody>
					</table>
				</div>
				<!-- end tab-pane -->
				<!-- begin tab-pane -->
				<div class="tab-pane fade" id="default-tab-3">
					<p>
						<span class="fa-stack fa-4x pull-left m-r-10">
							<i class="fa fa-square-o fa-stack-2x"></i>
							<i class="fab fa-twitter fa-stack-1x"></i>
						</span>
						Praesent tincidunt nulla ut elit vestibulum viverra. Sed placerat magna eget eros accumsan elementum.
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis lobortis neque.
						Maecenas justo odio, bibendum fringilla quam nec, commodo rutrum quam.
						Donec cursus erat in lacus congue sodales. Nunc bibendum id augue sit amet placerat.
						Quisque et quam id felis tempus volutpat at at diam. Vivamus ac diam turpis.Sed at lacinia augue.
						Nulla facilisi. Fusce at erat suscipit, dapibus elit quis, luctus nulla.
						Quisque adipiscing dui nec orci fermentum blandit.
						Sed at lacinia augue. Nulla facilisi. Fusce at erat suscipit, dapibus elit quis, luctus nulla.
						Quisque adipiscing dui nec orci fermentum blandit.
					</p>
				</div>
				<!-- end tab-pane -->



			</div>
		</div>
	</div>




	<table class="table table-condensed-xtra table-striped hide">
		<tr>
			@foreach($primaryData as $data)
				{!! $data['html'] !!}
			@endforeach
		</tr>
	</table>

	<div class="row">
		<div class="col col-md-6">
			<h5>Ubicación maquinaria</h5>
			<table class="table table-condensed-xtra table-striped">
				<tr>
					<td class="tblLabel">País</td>
					<td class="tblValue">{{ $global_paises[$pub->Producto->ProductoItem->Ubicacion->pais] }} ({{ strtoupper($pub->Producto->ProductoItem->Ubicacion->pais) }})</td>
					<td class="tblLabel">Ciudad</td>
					<td class="tblValue">{!! $pub->Producto->ProductoItem->Ubicacion->ciudad !!}</td>
				</tr>
				<tr>
					<td class="tblLabel" style="vertical-align:middle;">Código Postal</td>
					<td class="tblValue" style="vertical-align:middle;">{!! $pub->Producto->ProductoItem->Ubicacion->cpostal !!}</td>
					<td class="tblLabel" style="vertical-align:middle;">Dirección</td>
					<td class="tblValue" style="vertical-align:middle;">{!! $pub->Producto->ProductoItem->Ubicacion->direccion !!}</td>
				</tr>
			</table>
		</div>
		<div class="col col-md-6">
			<h5>Ubicación destino</h5>
			<table class="table table-condensed-xtra table-striped">
				<tr>
					<td class="tblLabel">País</td>
					<td class="tblValue">{{ \App\Models\Destino::find($cot->definicion['destino']['id'])->Pais->nombre }} ({{ strtoupper(\App\Models\Destino::find($cot->definicion['destino']['id'])->Pais->codigo) }})</td>
					<td class="tblLabel">Zona</td>
					<td class="tblValue">{!! \App\Models\Destino::find($cot->definicion['destino']['id'])->nombre !!}</td>
				</tr>
				{{--<tr>--}}
					{{--<td class="tblLabel" style="vertical-align:middle;">Código Postal</td>--}}
					{{--<td class="tblValue" style="vertical-align:middle;">{!! $pub->Producto->ProductoItem->Ubicacion->cpostal !!}</td>--}}
					{{--<td class="tblLabel" style="vertical-align:middle;">Dirección</td>--}}
					{{--<td class="tblValue" style="vertical-align:middle;">{!! $pub->Producto->ProductoItem->Ubicacion->direccion !!}</td>--}}
				{{--</tr>--}}
				{{--<tr>--}}
					{{--<td colspan="4">--}}

					{{--</td>--}}
				{{--</tr>--}}
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col col-md-12">
			<style>
				#publish_map {
					height: 300px;
					width: 100%;
				}
			</style>
			<div id="publish_map"></div>
		</div>
	</div>

	<br>

	<h4>Información primaria</h4>
	<div class="row">
		@foreach($primaryData as $data)
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-6 tblLabel">{!! $data['text'] !!}</div>
					@if($data['type'] == 'select')
						<div class="col-md-6 tblValue">@lang('form.'.$data['value'])</div>
					@elseif($data['type'] == 'multiselect')
						<div class="col-md-6 tblValue">
							<ul class="p-0">
								@php
									if(!is_array($data['value'])){
										$data['value'] = [$data['value']];
									}
								@endphp
							@foreach($data['value'] as $val)
								<li>@lang('form.'.$val)</li>
							@endforeach
							</ul>
						</div>
					@elseif($data['type'] == 'range_year')
						<div class="col-md-6 tblValue">{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}</div>
					@else
						<div class="col-md-6 tblValue">{!! $data['value'] !!}</div>
					@endif
				</div>
			</div>
		@endforeach
	</div>

	<br>
	<h5>Información adicional</h5>
	@if(count($secondaryData) > 0)
		<div class="row">
			@foreach($secondaryData as $data)
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-6 tblLabel">{!! $data['text'] !!}</div>
						@if($data['type'] == 'select')
							<div class="col-md-6 tblValue">@lang('form.'.$data['value'])</div>
						@elseif($data['type'] == 'multiselect')
							<div class="col-md-6 tblValue">
								<ul class="p-0">
									@foreach($data['value'] as $val)
										<li>@lang('form.'.$val)</li>
									@endforeach
								</ul>
							</div>
						@elseif($data['type'] == 'range_year')
							<div class="col-md-6 tblValue">{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}</div>
						@else
							<div class="col-md-6 tblValue">{!! $data['value'] !!}</div>
						@endif
					</div>
				</div>
			@endforeach
		</div>
	@else
		<div class="row">
			<div class="col-md-12"><p>Sin información secundaria </p></div>
		</div>
	@endif

	<br>
	<h5>Dimensiones</h5>
	<table class="table table-condensed-xtra table-striped">
		<tr>
			<td class="tblLabel">Longitud</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['lng'] }}</td>
			<td class="tblLabel">Ancho</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['wdt'] }}</td>
		</tr>
		<tr>
			<td class="tblLabel">Alto</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['hgt'] }}</td>
			<td class="tblLabel">Peso</td>
			<td class="tblValue">{{ $pub->Producto->ProductoItem->dimension['wgt'] }}</td>
		</tr>
		<tr>
			<td class="tblLabel">Pallet</td>
			<td class="tblValue">{{ ($pub->Producto->ProductoItem->dimension['pal'] == 1) ? 'Si': 'No' }}</td>
		</tr>
	</table>



	<h5>Galería de imágenes</h5>
	@foreach($pub->Producto->Galerias as $galeria)
		<ul class="attached-document clearfix">
			@foreach($galeria->Imagenes as $imagen)
				<li class="fa-camera">
					<div class="document-file">
						<a href="javascript:;">
							<img src="{{ $imagen->ruta_publica_producto_thumb }}" alt="" />
						</a>
					</div>
					<div class="document-name"><a href="javascript:;">{{ $imagen->filename }}</a></div>
				</li>
			@endforeach
		</ul>
	@endforeach
	<br>

	<h5>Mensaje personalizado</h5>
	<form action="javascript:;" name="wysihtml5" method="POST">
		<textarea class="textarea form-control" id="wysihtml5" placeholder="Enter text ..." rows="12">Estimado Sr(a). {{ $cot->Cliente->Agenda->nombres_apellidos }},
			<br><br>A tiempo de saludarle cordialmente hacemos llegar a su persona la información solicitada lorem ipsum dolor sit amet consectetur dip adipisicing set.
			<br><br>Atentamente, el equipo de <b>ExpoIndustri</b>.
		</textarea>
	</form>
	<div class="row">
		<div class="col-md-12 text-right">
			{{--<a href="javascript:;" class="btn btn-primary" onclick="guardarCalculadora()">Guardar calculadora</a>--}}
			<br><br>
			{{--<a href="javascript:;" onclick="guardar({{ $pub->id }}, 'rejected')" class="btn btn-danger"><i class="fa fa-thumbs-down"></i> Rechazar</a>--}}
			<a href="javascript:;" onclick="enviarMensajeLibre()" class="btn btn-primary"><i class="fa fa-send"></i> Enviar</a>
			{{--<a href="javascript:;" onclick="guardar({{ $pub->id }}, 'approved')" class="btn btn-inverse"><i class="fa fa-thumbs-up"></i> Guardar y Aprobar</a>--}}
		</div>
	</div>

</div>
<!-- end wrapper -->
@endsection

@push('scripts')

<script src="/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="/assets/js/demo/form-wysiwyg.demo.js"></script>

<script>

	$(document).ready(function() {
		FormWysihtml5.init();
	});

$.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyALPqjAs5i_1KqcyQ_YMIEtxKblkrDIHaY').done(function() {

	initPublishMap();
	buscarEnMapa();
});

$(document).ready(function() {

});
function guardarAnuncio(){
	var url = '{{ route('admin.announcementInbox.update', [$pub->id]) }}';
	var data = {
		precio_final: $('#precio_final').val()
	};

}
function guardarCalculadora(){

	var url = '{{ route('admin.announcementInbox.updateCalculadora', [$pub->id]) }}';
	var data = {
		informe_tecnico: $('#informe_tecnico').val(),
		seguro_extra: $('#seguro_extra').val(),
		contenedor_compartido: $('#contenedor_compartido').val(),
		contenedor_exclusivo: $('#contenedor_exclusivo').val(),
		envio_abierto: $('#envio_abierto').val(),
		destino_1: $('#destino_1').val(),
		destino_2: $('#destino_2').val(),
		destino_3: $('#destino_3').val(),
		destino_4: $('#destino_4').val(),
		destino_5: $('#destino_5').val(),
		destino_6: $('#destino_6').val(),
	};

	ajaxPatch(url, data, function(data){
		console.log(data);
	});
}

function guardar(id, estado){
	guardarCalculadora();
	var url = '{{ route('admin.announcementInbox.update', [0]) }}';
	url = url.replace('0', id);
	var data = {
		precio_final: $('#precio_final').val(),
		estado: estado
	};

	ajaxPatch(url, data, function(data){
		console.log(data);
	});
}

var map = null;
var geocoder = null;
var marker = null;
function initPublishMap() {
	var uluru = {lat: -25.363, lng: 131.044};
	map = new google.maps.Map(document.getElementById('publish_map'), {
		scrollwheel: false,
		zoom: 4,
		center: uluru,
//			query: 'Bolivia'
	});

	geocoder = new google.maps.Geocoder();

	var address = '{{ $pub->Producto->ProductoItem->Ubicacion->direccion }}';
	geocodeAddress(geocoder, map, address);

//	google.maps.event.addListener(map, 'click', function(event) {
//		placeMarker(event.latLng, map);
//	});

//		var marker = new google.maps.Marker({
//			position: uluru,
//			map: map
//		});
}

function placeMarker(location, map) {
	if (marker === null){
		marker = new google.maps.Marker({
			position: location,
			map: map
		});
	} else {
		marker.setPosition(location);
	}

//		marker = new google.maps.Marker({
//			position: location,
//			map: map
//		});
	map.panTo(location);
}
function geocodeAddress(geocoder, resultsMap, address) {
//		var address = 'Bolivia';
//		var address = $("#pais option:selected").text();
	geocoder.geocode({'address': address}, function(results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: resultsMap,
					position: results[0].geometry.location
				});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}

function buscarEnMapa(){
	var address = '{{ $pub->Producto->ProductoItem->Ubicacion->direccion }}';
	map.setZoom(15);
	geocodeAddress(geocoder, map, address);
}

function enviarMensajeLibre(){
	var url = '{{ route('admin.quoteInbox.enviarMensajeLibre', [$cot->id]) }}';
	var data = {
		mensaje:$('#wysihtml5').val(),
		idCotizacion: {{ $cot->id }}
	};

	ajaxPost(url, data, function(data){
		swal('Envio exitoso!', '', 'success');
	});
}

function crearOrden(){
	var url = '{{ route('admin.quoteInbox.crearOrden', [$cot->id]) }}';
	var data = {
		idCotizacion: {{ $cot->id }}
	};

	ajaxPost(url, data, function(data){
		swal('Orden creada!', data.message, 'success');
	});
}


</script>
@endpush