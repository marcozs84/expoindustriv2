@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.tag.index') }}">Tag</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $tag->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Tag</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Tag</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTagCreate" method="post" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Padre</label>
					<div class="col-md-10">
						{!! Form::select('idPadre', $padres, $tag->idPadre, [
							'id' => 'idPadre',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>

				<table class="table table-condensed table-striped">
					<tr>
						<th class="text-center">Sueco</th>
						<th class="text-center">Español</th>
						<th class="text-center">Inglés</th>
					</tr>
					<tr>
						<td>
							{!! Form::text('se', $tag->Traduccion->se, [
								'id' => 'se',
								'class' => 'form-control',
								'onchange' => 'changed_input(this)',
							]) !!}
						</td>
						<td>
							{!! Form::text('es', $tag->Traduccion->es, [
								'id' => 'es',
								'class' => 'form-control',
								'onchange' => 'changed_input(this)',
							]) !!}
						</td>
						<td>
							{!! Form::text('en', $tag->Traduccion->en, [
								'id' => 'en',
								'class' => 'form-control',
								'onchange' => 'changed_input(this)',
							]) !!}
						</td>
					</tr>
				</table>

			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="tagCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="tagSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>


<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){

	$('#idPadre').select2();

}

$( "#formTagEdit" ).submit(function( event ) {
	tagSave();
	event.preventDefault();
});

function tagSave(){
	var url = '{{ route('admin.tag.update', [$tag->id]) }}';
	var form = $('#formTagCreate');
	ajaxPatch(url, {
		idPadre: form.find('#idPadre').val(),
		es: form.find('#es').val(),
		se: form.find('#se').val(),
		en: form.find('#en').val()
	}, function(){
		$(document).trigger('formTagEdit:success');
	});
}

function tagCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTagEdit:cancel');
	@else
		redirect('{{ route('admin.tag.index')  }}');
	@endif
}

function changed_input( e ) {
	var val = $(e).val();
	var cols = val.split(String.fromCharCode(9));
	console.log(cols);
}

</script>
@endpush

