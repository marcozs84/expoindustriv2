@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', ' Tags')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"> Tags</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"> Tags <small></small></h1>
	<!-- end page-header -->

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title"> Tags</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="tagCreate()"><i class="fa fa-plus"></i> Crear</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="tagDelete()"><i class="fa fa-trash"></i> Eliminar</a>
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	$(document).ready(function(){
		initDataTable();
	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.tag.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						// d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//						var data = JSON.parse(data);
//						for(var i = 0 ; i < data.data.length; i++){
//							// ...
//						}
//						return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
//					{ data: 'nombre', title: 'Nombre'},
					{ title: 'Grupo'},
					{ title: 'SE'},
					{ title: 'ES'},
					{ title: 'EN'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					@php $counter = 0 @endphp
					{
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '30px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="tagShow('+ row.id +')">'+ data +'</i></a>';
						}
					{{--},{--}}
						{{--// nombre,--}}
						{{--targets: {{ $counter++ }},--}}
						{{--render: function( data, type, row ) {--}}
							{{--return '<a href="javascript:;" onclick="tagShow('+ row.id +')">'+ data +'</i></a>';--}}
						{{--}--}}
					},{
						// Grupo,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							if( row.idPadre > 0 ) {
								data = row.padre.traduccion.se;
								return '<a href="javascript:;" onclick="translateFieldSL('+ row.id +')">'+ data +'</i></a>';
							}
							return '0';
						}
					},{
						// SE,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							data = row.traduccion.se;
							return '<a href="javascript:;" onclick="translateFieldSL('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// ES,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							data = row.traduccion.es;
							return '<a href="javascript:;" onclick="translateFieldSL('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// EN,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							data = row.traduccion.en;
							return '<a href="javascript:;" onclick="translateFieldSL('+ row.id +')">'+ data +'</i></a>';
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';
							html += '<a href="javascript:;" onclick="tagEdit('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
							return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function tagShow(id){
		var url = '{{ route('admin.tag.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Tag', null, {
			'size': 'modal-lg'
		});
	}

	function tagCreate(){
		var url = '{{ route('admin.tag.create') }}';
		var modal = openModal(url, 'Nuevo Tag', null, { size:'modal-lg' });
		setModalHandler('formTagCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function tagEdit(id){
		var url = '{{ route('admin.tag.edit', 'idRecord') }}';
		url = url.replace('idRecord', id);
		var modal = openModal(url, 'Editar Tag', null, { size:'modal-lg' });
		setModalHandler('formTagEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function tagDelete( id ){
		var ids = [];

		if ( id !== undefined ) {
			ids = [ id ];
		} else {
			$('.chkId').each(function() {
				if($(this).prop('checked')){
					ids.push($(this).val());
				}
			});
			if(ids.length === 0){
				alert("Debe seleccionar al menos 1 registro.");
				return false;
			}
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.tag.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

	function translateFieldSL(key){
		var url = '{{ route('admin.traduccion.singleline', [ 'owner' => 'tag', 'owner_id' => 'idTag', 'campo' => 'nombre' ]) }}';
		url = url.replace('idTag', key);
		var modal = openModal(url, 'Traduccion', null, {size:'modal-lg'});
		setModalHandler('formTraduccion:success', function ( event, response ) {
//			$('#descripcion_es').html( response.es );
//			$('#descripcion_se').html( response.se );
//			$('#descripcion_en').html( response.en );
			datatable.ajax.reload();
			dismissModal(modal);
		});
	}

</script>
@endpush