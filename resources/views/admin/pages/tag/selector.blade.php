@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')


<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}

	.table_tags tr td {
		color: #444;
		font-weight: 600;
		padding: 5px 0;
		line-height: 10px;
	}

	.card-columns {
		-webkit-column-count: 4;
		-moz-column-count: 4;
		column-count: 4;
		-webkit-column-gap: 1.25rem;
		-moz-column-gap: 1.25rem;
		column-gap: 0.25rem;
	}

	.card {
		position: relative;
		display: block;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-ms-flex-direction: column;
		flex-direction: column;
		background-color: #fff;
		border: none;
		border-radius: 4px;
	}

	.card-columns .card {
		/*display: inline-block;*/
		display:block !important;
		width: 100%;
		margin-bottom: 5px;
	}

	.card-block {
		padding:5px;
	}
	.ajax_content label {
		text-align:left !important;
		display:block;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Tag</li>
	</ol>

	<h1 class="page-header">Nuevo Tag</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Tag</h4>
		</div>
		<div class="panel panel-body">
			<div class="row m-t-10">

				<div class="col-md-12">
					<div class="card-columns" >
						<input type="hidden" id="formTagSelector">

						@if( isset($grouped_tags) )

							@foreach( $grouped_tags as $group => $tags )

								<div class="card" >
									<div class="card-block" >
										<h4 class="card-title" style="font-size:14px;">{{ $group }}</h4>
										<table>
											@foreach( $tags as $id => $tag )
												<tr>
													<td style="width:20px;"><input id="tagchk_{{ $id }}" class="tag_selector" name="tagchk_{{ $id }}" @if( in_array($id, $prod_tags) ) checked="checked" @endif data-nombre="{{ $tag }}" value="{{ $id }}" onclick="addTag(this)" type="checkbox"></td>
													<td style=""><label style="font-weight:normal;" for="tagchk_{{ $id }}">{{ $tag }}</label></td>
												</tr>
											@endforeach
										</table>
									</div>
								</div>

							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm btn-danger" href="javascript:;" onclick="tagCreate()"> <i class="fa fa-plus"></i> Nuevo</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="tagCancel()">Cerrar</a>
					<a class="btn btn-white btn-sm btn-primary" data-dismiss="modal" href="javascript:;" onclick="tagAceptar()">Aceptar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif


function tagCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTagShow:cancel');
	@else
		redirect('{{ route('admin.tag.index')  }}');
	@endif
}

function formInit(){

}

function addTag(e) {
	tagsSelected = [];
	$('.tag_selector').each( function(k,v) {
		if( $(v).prop('checked') === true ) {
			tagsSelected.push( $(v).val() );
		}
	});
//	$('#formProductoEdit #searchCliente').val( tagsSelected ).trigger('change');
}

function tagAceptar() {

	@if( $idProducto > 0 )

	var url = '{{ route('resource.producto.update_tags', [ $idProducto ]) }}';
	var data = {
		tags: tagsSelected
	};
	ajaxPost(url, data, function( response ) {
		console.log(response.data);
	});

	@endif

	$(document).trigger('formTagSelector:aceptar', { selectedtags: tagsSelected });
}

function tagCreate(){
	var url = '{{ route('admin.tag.create') }}';
	var modal = openModal(url, 'Nuevo Tag', null, { size:'modal-lg' });
	setModalHandler('formTagCreate:success', function( event, data ){

		if( $('#formProductoEdit #tags').length > 0 ) {
			var newOption = new Option(data.traduccion.es, data.id, false, false);
			$('#formProductoEdit #tags').append(newOption).trigger('change');
		}

		var closest = $('#formTagSelector').closest('.modal');
		reloadModal(closest);
		dismissModal(modal);
//		datatable.ajax.reload();
	});
}


</script>
@endpush

