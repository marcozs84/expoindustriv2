@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.tag.index') }}">Tag</a></li>
		<li class="breadcrumb-item active">Nuevo/a tag</li>
	</ol>

	<h1 class="page-header">Nuevo Tag</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Tag</h4>
		</div>
		<div class="panel panel-body">
			<form id="formTagCreate" method="post" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Grupo</label>
					<div class="col-md-10">
						{!! Form::select('idPadre', $padres, null, [
							'id' => 'idPadre',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>

				<table class="table table-condensed table-striped">
					<tr>
						<th class="text-center">Sueco</th>
						<th class="text-center">Español</th>
						<th class="text-center">Inglés</th>
					</tr>
					<tr>
						<td>
							{!! Form::text('se', '', [
								'id' => 'se',
								'class' => 'form-control',
								'onchange' => "changed_val(this.value)"
							]) !!}
						</td>
						<td>
							{!! Form::text('es', '', [
								'id' => 'es',
								'class' => 'form-control',
								'onchange' => "changed_val(this.value)"
							]) !!}
						</td>
						<td>
							{!! Form::text('en', '', [
								'id' => 'en',
								'class' => 'form-control',
								'onchange' => "changed_val(this.value)"
							]) !!}
						</td>
					</tr>
				</table>

			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="tagCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="tagSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>


<script>

function formInit(){

	$('#idPadre').select2();

	var tag_parent_id = window.localStorage.getItem('tag_parent_id');
	console.log(tag_parent_id);

	if( tag_parent_id == null ) {
		window.localStorage.setItem('tag_parent_id', 0);
		tag_parent_id = 0;
	}

	var form = $('#formTagCreate');
	form.find('#idPadre').val( tag_parent_id ).trigger('change');

	$('#formTagCreate #se').focus();
	console.log("formInit");
}

$( "#formTagCreate" ).submit(function( event ) {
	tagSave();
	event.preventDefault();
});

function tagSave(){
	var url = '{{ route('admin.tag.store') }}';
	var form = $('#formTagCreate');
	ajaxPost(url, {
		idPadre: form.find('#idPadre').val(),
		es: form.find('#es').val(),
		se: form.find('#se').val(),
		en: form.find('#en').val()
	}, function( response ){

		window.localStorage.setItem('tag_parent_id', form.find('#idPadre').val());
		$(document).trigger('formTagCreate:success', response.data);
	});
}

function tagCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formTagCreate:cancel');
	@else
		redirect('{{ route('admin.tag.index')  }}');
	@endif
}

function changed_val(val) {

	// Ref.: https://stackoverflow.com/a/1095255
	var cols = val.split(String.fromCharCode(9));

	if( cols.length === 3 ) {
		var form = $('#formTagCreate');
		form.find('#se').val( cols[0] );
		form.find('#es').val( cols[1] );
		form.find('#en').val( cols[2] );
	}
}


@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

</script>
@endpush

