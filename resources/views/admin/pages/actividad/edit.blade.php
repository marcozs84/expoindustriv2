@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.actividad.index') }}">Actividad</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $actividad->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Actividad</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando Actividad</h4>
		</div>
		<div class="panel panel-body">
			<form id="formActividadEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', $actividad->owner_type, [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', $actividad->owner_id, [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Actividad Tipo</label>
					<div class="col-8">
						{!! Form::select('idActividadTipo', [], '', [
							'id' => 'idActividadTipo',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Actividad Tipo</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idActividadTipo', $actividad->idActividadTipo, [--}}
							{{--'id' => 'idActividadTipo',--}}
							{{--'placeholder' => 'Id Actividad Tipo',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Calendario</label>
					<div class="col-8">
						{!! Form::select('idCalendario', [], '', [
							'id' => 'idCalendario',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Calendario</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idCalendario', $actividad->idCalendario, [--}}
							{{--'id' => 'idCalendario',--}}
							{{--'placeholder' => 'Id Calendario',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Titulo</label>
					<div class="col-8">
						{!! Form::text('titulo', $actividad->titulo, [
							'id' => 'titulo',
							'placeholder' => 'Titulo',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                        {!! Form::textarea('descripcion', $actividad->descripcion, [
                            'id' => 'descripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Inicio</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('fechaInicio', ($actividad->fechaInicio ? $actividad->fechaInicio->format('d/m/Y H:i:s') : ''), [
							'id' => 'fechaInicio',
							'placeholder' => 'Fecha Inicio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Fin</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('fechaFin', ($actividad->fechaFin ? $actividad->fechaFin->format('d/m/Y H:i:s') : ''), [
							'id' => 'fechaFin',
							'placeholder' => 'Fecha Fin',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Linked</label>
					<div class="col-8">
						{!! Form::text('linked', $actividad->linked, [
							'id' => 'linked',
							'placeholder' => 'Linked',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Extrainfo</label>
                    <div class="col-8">
                        {!! Form::textarea('extrainfo', $actividad->extrainfo, [
                            'id' => 'extrainfo',
                            'placeholder' => 'Extrainfo',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Completado</label>
					<div class="col-8">
						{!! Form::select('completado', [
							0 => 'No',
							1 => 'Si'
						], $actividad->completado, [
							'id' => 'completado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="actividadCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="actividadSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/min/moment.min.js"></script>
<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){

	$('#formActividadEdit #fechaInicio').datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss'
	})
		.on('dp.change', function(){
		});

	$('#formActividadEdit #fechaFin').datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss'
	})
		.on('dp.change', function(){
		});



	$idActividadTipoSearch = Search.resource('#formActividadEdit #idActividadTipo', {
		url: '{{ route('resource.actividadTipo.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.nombre;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.nombre;
		},
		defaultValue: {
			value: {{ $actividad->idActividadTipo }},
			text: '{{ $actividad->ActividadTipo->nombre }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idActividadTipoSearch, {{ $actividad->idActividadTipo }}, '{{ $actividad->ActividadTipo->id }});

	$idCalendarioSearch = Search.resource('#formActividadEdit #idCalendario', {
		url: '{{ route('resource.calendario.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.id;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.id;
		},
		defaultValue: {
			value: {{ $actividad->idCalendario }},
			text: '{{ $actividad->Calendario->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idCalendarioSearch, {{ $actividad->idCalendario }}, '{{ $actividad->Calendario->id }});



}

$( "#formActividadEdit" ).submit(function( event ) {
	actividadSave();
	event.preventDefault();
});

function actividadSave(){
	var url = '{{ route('resource.actividad.update', [$actividad->id]) }}';
	var formActividadEdit = $('#formActividadEdit');
	ajaxPatch(url, {
		owner_type: 	formActividadEdit.find('#owner_type').val(),
		owner_id: 		formActividadEdit.find('#owner_id').val(),
		idActividadTipo: formActividadEdit.find('#idActividadTipo').val(),
		idCalendario: 	formActividadEdit.find('#idCalendario').val(),
		titulo: 		formActividadEdit.find('#titulo').val(),
		descripcion: 	formActividadEdit.find('#descripcion').val(),
		fechaInicio: 	formActividadEdit.find('#fechaInicio').val(),
		fechaFin: 		formActividadEdit.find('#fechaFin').val(),
		linked: 		formActividadEdit.find('#linked').val(),
		extrainfo:      formActividadEdit.find('#extrainfo').val(),
		completado:     formActividadEdit.find('#completado').val()
	}, function(){
		$(document).trigger('formActividadEdit:success');
	});
}

function actividadCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formActividadEdit:cancel');
	@else
		redirect('{{ route('admin.actividad.index')  }}');
	@endif
}

</script>
@endpush

