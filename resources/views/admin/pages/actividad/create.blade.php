@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.actividad.index') }}">Actividad</a></li>
		<li class="breadcrumb-item active">Nueva actividad</li>
	</ol>

	<h1 class="page-header">Nueva Actividad</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nueva Actividad</h4>
		</div>
		<div class="panel panel-body">
			<form id="formActividadCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', '', [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', '', [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Actividad Tipo</label>
					<div class="col-8">
						{!! Form::select('idActividadTipo', [], '', [
							'id' => 'idActividadTipo',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Actividad Tipo</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idActividadTipo', '', [--}}
							{{--'id' => 'idActividadTipo',--}}
							{{--'placeholder' => 'Id Actividad Tipo',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Calendario</label>
					<div class="col-8">
						{!! Form::select('idCalendario', [], '', [
							'id' => 'idCalendario',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Calendario</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idCalendario', '', [--}}
							{{--'id' => 'idCalendario',--}}
							{{--'placeholder' => 'Id Calendario',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Titulo</label>
					<div class="col-8">
						{!! Form::text('titulo', '', [
							'id' => 'titulo',
							'placeholder' => 'Titulo',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Descripcion</label>
                    <div class="col-8">
                        {!! Form::textarea('descripcion', '', [
                            'id' => 'descripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Inicio</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('fechaInicio', '', [
							'id' => 'fechaInicio',
							'placeholder' => 'Fecha Inicio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fecha Fin</label>
					<div class="col-8 datetimepicker_pos">
						{!! Form::text('fechaFin', '', [
							'id' => 'fechaFin',
							'placeholder' => 'Fecha Fin',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Linked</label>
					<div class="col-8">
						{!! Form::text('linked', '', [
							'id' => 'linked',
							'placeholder' => 'Linked',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Extrainfo</label>
                    <div class="col-8">
                        {!! Form::textarea('extrainfo', '', [
                            'id' => 'extrainfo',
                            'placeholder' => 'Extrainfo',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Completado</label>
                    <div class="col-8">
                        {!! Form::select('completado', [
                            0 => 'No',
                            1 => 'Si'
                        ], '', [
                            'id' => 'completado',
                            'class' => 'form-control',
                        ]) !!}
                    </div>
                </div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="actividadCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="actividadSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/moment/min/moment.min.js"></script>
<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){

var defaultDate = moment().hours(0).minutes(0).seconds(0);

	$('#formActividadCreate #fechaInicio').datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss'
	})
		.on('dp.change', function(){
		})
		.data("DateTimePicker").defaultDate(defaultDate);

	$('#formActividadCreate #fechaFin').datetimepicker({
		format: 'DD/MM/YYYY HH:mm:ss'
	})
		.on('dp.change', function(){
		})
		.data("DateTimePicker").defaultDate(defaultDate);

	$idActividadTipoSearch = Search.resource('#formActividadCreate #idActividadTipo', {
		url: '{{ route('resource.actividadTipo.ds') }}',
		data: {
			soloHijos: 1
		},
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.nombre;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.nombre;
		}
	});

	$idCalendarioSearch = Search.resource('#formActividadCreate #idCalendario', {
		url: '{{ route('resource.calendario.ds') }}',
		templateResult: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.nombre;
		},
		templateSelection: function( data ) {
			console.log( data );  // <- Remove this line when search is defined.
			return data.nombre;
		}
	});



}

$( "#formActividadCreate" ).submit(function( event ) {
	actividadSave();
	event.preventDefault();
});

function actividadSave(){
	var url = '{{ route('resource.actividad.store') }}';
	var formActividadCreate = $('#formActividadCreate');
	ajaxPost(url, {
		owner_type: 	formActividadCreate.find('#owner_type').val(),
		owner_id: 		formActividadCreate.find('#owner_id').val(),
		idActividadTipo: formActividadCreate.find('#idActividadTipo').val(),
		idCalendario: 	formActividadCreate.find('#idCalendario').val(),
		titulo: 		formActividadCreate.find('#titulo').val(),
		descripcion: 	formActividadCreate.find('#descripcion').val(),
		fechaInicio: 	formActividadCreate.find('#fechaInicio').val(),
		fechaFin: 		formActividadCreate.find('#fechaFin').val(),
		linked: 		formActividadCreate.find('#linked').val(),
		extrainfo: 	    formActividadCreate.find('#extrainfo').val(),
		completado:     formActividadCreate.find('#completado').val()
	}, function(){
		$(document).trigger('formActividadCreate:success');
	});
}

function actividadCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formActividadCreate:cancel');
	@else
		redirect('{{ route('admin.actividad.index')  }}');
	@endif
}

</script>
@endpush

