@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.comentario.index') }}">Comentario</a></li>
		<li class="breadcrumb-item active">Nuevo/a comentario</li>
	</ol>

	<h1 class="page-header">Nuevo/a Comentario</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo/a Comentario</h4>
		</div>
		<div class="panel panel-body">
			<form id="formComentarioCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Usuario</label>
					<div class="col-8">
						{!! Form::select('idUsuario', [], '', [
							'id' => 'idUsuario',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Usuario</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idUsuario', '', [--}}
							{{--'id' => 'idUsuario',--}}
							{{--'placeholder' => 'Id Usuario',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Padre</label>
					<div class="col-8">
						{!! Form::select('idPadre', [], '', [
							'id' => 'idPadre',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Padre</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idPadre', '', [--}}
							{{--'id' => 'idPadre',--}}
							{{--'placeholder' => 'Id Padre',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', '', [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', '', [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Asunto</label>
                    <div class="col-8">
                        {!! Form::textarea('asunto', '', [
                            'id' => 'asunto',
                            'placeholder' => 'Asunto',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Mensaje</label>
                    <div class="col-8">
                        {!! Form::textarea('mensaje', '', [
                            'id' => 'mensaje',
                            'placeholder' => 'Mensaje',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="comentarioCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="comentarioSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){





	var idUsuarioSearch = Search.resource('#formComentarioCreate #idUsuario', {
		url: '{{ route('resource.usuario.ds') }}',
		templateResult: function( data ) {
			return data.username;
		},
		templateSelection: function( data ) {
			return data.username;
		}
	});

	var idPadreSearch = Search.resource('#formComentarioCreate #idPadre', {
		url: '{{ route('resource.comentario.ds') }}',
		templateResult: function( data ) {
			return data.mensaje;
		},
		templateSelection: function( data ) {
			return data.mensaje;
		}
	});



}

$( "#formComentarioCreate" ).submit(function( event ) {
	comentarioSave();
	event.preventDefault();
});

function comentarioSave(){
	var url = '{{ route('resource.comentario.store') }}';
	var formComentarioCreate = $('#formComentarioCreate');
	ajaxPost(url, {
		idUsuario: 	formComentarioCreate.find('#idUsuario').val(),
		idPadre: 		formComentarioCreate.find('#idPadre').val(),
		owner_type: 	formComentarioCreate.find('#owner_type').val(),
		owner_id: 		formComentarioCreate.find('#owner_id').val(),
		asunto: 		formComentarioCreate.find('#asunto').val(),
		mensaje: 		formComentarioCreate.find('#mensaje').val()
	}, function(){
		$(document).trigger('formComentarioCreate:success');
	});
}

function comentarioCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formComentarioCreate:cancel');
	@else
		redirect('{{ route('admin.comentario.index')  }}');
	@endif
}

</script>
@endpush

