@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
	.col-form-label, .row.form-group>.col-form-label {
		padding:0px;
		text-align:right;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Comentario</li>
	</ol>

	<h1 class="page-header">Nuevo Comentario</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Comentario</h4>
		</div>
		<div class="panel panel-body">
			<form id="formComentarioShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $comentario->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Usuario</label>
					<div class="col-8">
						{{ $comentario->idUsuario }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Padre</label>
					<div class="col-8">
						{{ $comentario->idPadre }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{{ $comentario->owner_type }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{{ $comentario->owner_id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Asunto</label>
                    <div class="col-8">
                       {!! $comentario->asunto !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Mensaje</label>
                    <div class="col-8">
                       {!! $comentario->mensaje !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{{ ($comentario->created_at ? $comentario->created_at->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="comentarioEdit({{ $comentario->id }})">Edit</a>
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="comentarioCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif


function comentarioCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formComentarioShow:cancel');
	@else
		redirect('{{ route('admin.comentario.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

