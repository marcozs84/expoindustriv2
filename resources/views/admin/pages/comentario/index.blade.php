@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', ' Comentarios')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active"> Comentarios</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header"> Comentarios <small></small></h1>
	<!-- end page-header -->

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title"> Comentarios</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="comentarioCreate()"><i class="fa fa-plus"></i> Nuevo/a</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="comentarioDelete()"><i class="fa fa-trash"></i> Eliminar</a>
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;
	var estados = '';

	$(document).ready(function(){
		initDataTable();
	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.comentario.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						// d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//						var data = JSON.parse(data);
//						for(var i = 0 ; i < data.data.length; i++){
//							// ...
//						}
//						return JSON.stringify( data );
					}
				},
				columns: [
					{ data: 'id', title: 'Id'},
					{ data: 'idUsuario', title: 'IdUsuario'},
					{ data: 'idPadre', title: 'IdPadre'},
					{ data: 'owner_type', title: 'Owner_type'},
					{ data: 'owner_id', title: 'Owner_id'},
					{ data: 'asunto', title: 'Asunto'},
					{ data: 'mensaje', title: 'Mensaje'},
					{ data: 'created_at', title: 'Created_at'},

					{ title: ''},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					// ...
				],
				columnDefs: [
					@php $counter = 0 @endphp
					{
						// id,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '30px',
						render: function( data, type, row ) {
							return '<a href="javascript:;" onclick="comentarioShow('+ row.id +')">'+ data +'</i></a>';
						}
					},{
						// idUsuario,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// idPadre,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// owner_type,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// owner_id,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// asunto,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// mensaje,
						targets: {{ $counter++ }},
						render: function( data, type, row ) {
							return data;
						}
					},{
						// created_at,
						targets: {{ $counter++ }},
						classname: 'text-center',
						width: '100px',
						render: function( data, type, row ) {
							return format_datetime(data);
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center with-btn',
						width: '20px',
						render: function(data, type, row){
							var html = '';
							html += '<a href="javascript:;" onclick="comentarioEdit('+ row.id +')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>';
							return html;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function comentarioShow(id){
		var url = '{{ route('admin.comentario.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Comentario', null, {
			'size': 'modal-lg'
		});
	}

	function comentarioCreate(){
		var url = '{{ route('admin.comentario.create') }}';
		var modal = openModal(url, 'Nuevo Comentario', null, { size:'modal-lg' });
		setModalHandler('formComentarioCreate:success', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function comentarioEdit(id){
		var url = '{{ route('admin.comentario.edit', 'idRecord') }}';
		url = url.replace('idRecord', id);
		var modal = openModal(url, 'Editar Comentario', null, { size:'modal-lg' });
		setModalHandler('formComentarioEdit:success', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function comentarioDelete( id ){
		var ids = [];

		if ( id !== undefined ) {
			ids = [ id ];
		} else {
			$('.chkId').each(function() {
				if($(this).prop('checked')){
					ids.push($(this).val());
				}
			});
			if(ids.length === 0){
				alert("Debe seleccionar al menos 1 registro.");
				return false;
			}
		}

		swal({
			text: 'Está seguro que desea eliminar los registros seleccionados?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.comentario.destroy', [0]) }}';
				ajaxDelete(url, ids, function(){
					swal('Registros eliminados', { icon: "success" });
					datatable.ajax.reload(null, false);
				});
			}
		});
	}

</script>
@endpush