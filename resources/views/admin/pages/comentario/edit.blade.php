@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.comentario.index') }}">Comentario</a></li>
		<li class="breadcrumb-item active">Actualizando: {{ $comentario->id }}</li>
	</ol>

	<h1 class="page-header">Actualizando Comentario</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Actualizando: {{ $comentario->id }}</h4>
		</div>
		<div class="panel panel-body">
			<form id="formComentarioEdit" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Usuario</label>
					<div class="col-8">
						{!! Form::select('idUsuario', [], '', [
							'id' => 'idUsuario',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Usuario</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idUsuario', $comentario->idUsuario, [--}}
							{{--'id' => 'idUsuario',--}}
							{{--'placeholder' => 'Id Usuario',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Padre</label>
					<div class="col-8">
						{!! Form::select('idPadre', [], '', [
							'id' => 'idPadre',
							'class' => 'form-control default-select2',
						]) !!}
					</div>
				</div>
				{{--<div class="form-group col-md-6 col-sm-12 row m-b-15">--}}
					{{--<label class="col-form-label col-4">Id Padre</label>--}}
					{{--<div class="col-8">--}}
						{{--{!! Form::number('idPadre', $comentario->idPadre, [--}}
							{{--'id' => 'idPadre',--}}
							{{--'placeholder' => 'Id Padre',--}}
							{{--'class' => 'form-control',--}}
						{{--]) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_type</label>
					<div class="col-8">
						{!! Form::text('owner_type', $comentario->owner_type, [
							'id' => 'owner_type',
							'placeholder' => 'Owner_type',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Owner_id</label>
					<div class="col-8">
						{!! Form::number('owner_id', $comentario->owner_id, [
							'id' => 'owner_id',
							'placeholder' => 'Owner_id',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Asunto</label>
                    <div class="col-8">
                        {!! Form::textarea('asunto', $comentario->asunto, [
                            'id' => 'asunto',
                            'placeholder' => 'Asunto',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Mensaje</label>
                    <div class="col-8">
                        {!! Form::textarea('mensaje', $comentario->mensaje, [
                            'id' => 'mensaje',
                            'placeholder' => 'Mensaje',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="comentarioCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="comentarioSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){



	var idUsuarioSearch = Search.resource('#formComentarioEdit #idUsuario', {
		url: '{{ route('resource.usuario.ds') }}',
		templateResult: function( data ) {
			return data.username;
		},
		templateSelection: function( data ) {
			return data.username;
		},
		defaultValue: {
			value: {{ $comentario->idUsuario }},
			text: '{{ $comentario->Usuario->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idUsuarioSearch, {{ $comentario->idUsuario }}, '{{ $comentario->Usuario->id }});

	var idPadreSearch = Search.resource('#formComentarioEdit #idPadre', {
		url: '{{ route('resource.padre.ds') }}',
		templateResult: function( data ) {
			return data.mensaje;
		},
		templateSelection: function( data ) {
			return data.mensaje;
		},
		defaultValue: {
			value: {{ $comentario->idPadre }},
			text: '{{ $comentario->Padre->id }}'   // <- Must match format with templateSelection;
		}
	});
	// Search.setDefault($idPadreSearch, {{ $comentario->idPadre }}, '{{ $comentario->Padre->id }});



}

$( "#formComentarioEdit" ).submit(function( event ) {
	comentarioSave();
	event.preventDefault();
});

function comentarioSave(){
	var url = '{{ route('resource.comentario.update', [$comentario->id]) }}';
	var formComentarioEdit = $('#formComentarioEdit');
	ajaxPatch(url, {
		idUsuario: 	formComentarioEdit.find('#idUsuario').val(),
		idPadre: 		formComentarioEdit.find('#idPadre').val(),
		owner_type: 	formComentarioEdit.find('#owner_type').val(),
		owner_id: 		formComentarioEdit.find('#owner_id').val(),
		asunto: 		formComentarioEdit.find('#asunto').val(),
		mensaje: 		formComentarioEdit.find('#mensaje').val()
	}, function(){
		$(document).trigger('formComentarioEdit:success');
	});
}

function comentarioCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formComentarioEdit:cancel');
	@else
		redirect('{{ route('admin.comentario.index')  }}');
	@endif
}

</script>
@endpush

