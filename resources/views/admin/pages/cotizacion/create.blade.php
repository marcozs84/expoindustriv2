@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.cotizacion.index') }}">Cotizacion</a></li>
		<li class="breadcrumb-item active">Nuevo/a cotizacion</li>
	</ol>

	<h1 class="page-header">Nuevo Cotizacion</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Cotizacion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formCotizacionCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Almacen</label>
					<div class="col-8">
						{!! Form::number('idAlmacen', '', [
							'id' => 'idAlmacen',
							'placeholder' => 'Id Almacen',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cliente</label>
					<div class="col-8">
						{!! Form::number('idCliente', '', [
							'id' => 'idCliente',
							'placeholder' => 'Id Cliente',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cotizacion Esquema</label>
					<div class="col-8">
						{!! Form::number('idCotizacionEsquema', '', [
							'id' => 'idCotizacionEsquema',
							'placeholder' => 'Id Cotizacion Esquema',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Publicacion</label>
					<div class="col-8">
						{!! Form::number('idPublicacion', '', [
							'id' => 'idPublicacion',
							'placeholder' => 'Id Publicacion',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Producto</label>
					<div class="col-8">
						{!! Form::number('idProducto', '', [
							'id' => 'idProducto',
							'placeholder' => 'Id Producto',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Revision Tecnica</label>
					<div class="col-8">
						{!! Form::text('revisionTecnica', '', [
							'id' => 'revisionTecnica',
							'placeholder' => 'Revision Tecnica',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Revision Tecnica Precio</label>
					<div class="col-8">
						{!! Form::text('revisionTecnicaPrecio', '', [
							'id' => 'revisionTecnicaPrecio',
							'placeholder' => 'Revision Tecnica Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fotografias Extra</label>
					<div class="col-8">
						{!! Form::text('fotografiasExtra', '', [
							'id' => 'fotografiasExtra',
							'placeholder' => 'Fotografias Extra',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fotografias Extra Precio</label>
					<div class="col-8">
						{!! Form::text('fotografiasExtraPrecio', '', [
							'id' => 'fotografiasExtraPrecio',
							'placeholder' => 'Fotografias Extra Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Seguro</label>
					<div class="col-8">
						{!! Form::text('seguro', '', [
							'id' => 'seguro',
							'placeholder' => 'Seguro',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Seguro Precio</label>
					<div class="col-8">
						{!! Form::text('seguroPrecio', '', [
							'id' => 'seguroPrecio',
							'placeholder' => 'Seguro Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Transporte</label>
					<div class="col-8">
						{!! Form::text('transporte', '', [
							'id' => 'transporte',
							'placeholder' => 'Transporte',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Transporte Precio</label>
					<div class="col-8">
						{!! Form::text('transportePrecio', '', [
							'id' => 'transportePrecio',
							'placeholder' => 'Transporte Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Destino Precio</label>
					<div class="col-8">
						{!! Form::text('destinoPrecio', '', [
							'id' => 'destinoPrecio',
							'placeholder' => 'Destino Precio',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Definicion</label>
                    <div class="col-8">
                        {!! Form::textarea('definicion', '', [
                            'id' => 'definicion',
                            'placeholder' => 'Definicion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{!! Form::number('estado', '', [
							'id' => 'estado',
							'placeholder' => 'Estado',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{!! Form::text('created_at', '', [
							'id' => 'created_at',
							'placeholder' => 'Created_at',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>



			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="cotizacionCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="cotizacionSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif

$( "#formCotizacionCreate" ).submit(function( event ) {
	cotizacionSave();
	event.preventDefault();
});

function cotizacionSave(){
	var url = '{{ route('resource.cotizacion.store') }}';
	ajaxPost(url, {
		idAlmacen: 	$('#formCotizacionCreate #idAlmacen').val(),
		idCliente: 	$('#formCotizacionCreate #idCliente').val(),
		idCotizacionEsquema: $('#formCotizacionCreate #idCotizacionEsquema').val(),
		idPublicacion: $('#formCotizacionCreate #idPublicacion').val(),
		idProducto: 	$('#formCotizacionCreate #idProducto').val(),
		revisionTecnica: $('#formCotizacionCreate #revisionTecnica').val(),
		revisionTecnicaPrecio: $('#formCotizacionCreate #revisionTecnicaPrecio').val(),
		fotografiasExtra: $('#formCotizacionCreate #fotografiasExtra').val(),
		fotografiasExtraPrecio: $('#formCotizacionCreate #fotografiasExtraPrecio').val(),
		seguro: 		$('#formCotizacionCreate #seguro').val(),
		seguroPrecio: 	$('#formCotizacionCreate #seguroPrecio').val(),
		transporte: 	$('#formCotizacionCreate #transporte').val(),
		transportePrecio: $('#formCotizacionCreate #transportePrecio').val(),
		destinoPrecio: $('#formCotizacionCreate #destinoPrecio').val(),
		definicion: 	$('#formCotizacionCreate #definicion').val(),
		estado: 		$('#formCotizacionCreate #estado').val(),
		created_at: 	$('#formCotizacionCreate #created_at').val()
	}, function(){
		$(document).trigger('formCotizacionCreate:success');
	});
}

function cotizacionCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formCotizacionCreate:cancel');
	@else
		redirect('{{ route('admin.cotizacion.index')  }}');
	@endif
}

function formInit(){

	$('#formCotizacionCreate #created_at').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true,
		todayHighlight: true,
		orientation: 'bottom'
	})
		.datepicker("setDate",'now')
		.on('changeDate', function(){
		});
	$("#formCotizacionCreate #created_at").mask("99/99/9999");

}

</script>
@endpush

