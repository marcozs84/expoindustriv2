@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

<style>
    .col-form-label, .row.form-group>.col-form-label {
        padding:0px;
        text-align:right;
    }
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Cotizacion</li>
	</ol>

	<h1 class="page-header">Nuevo Cotizacion</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Cotizacion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formCotizacionShow" method="get" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id</label>
					<div class="col-8">
						{{ $cotizacion->id }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Almacen</label>
					<div class="col-8">
						{{ $cotizacion->idAlmacen }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cliente</label>
					<div class="col-8">
						{{ $cotizacion->idCliente }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Cotizacion Esquema</label>
					<div class="col-8">
						{{ $cotizacion->idCotizacionEsquema }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Publicacion</label>
					<div class="col-8">
						{{ $cotizacion->idPublicacion }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Id Producto</label>
					<div class="col-8">
						{{ $cotizacion->idProducto }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Revision Tecnica</label>
					<div class="col-8">
						{{ $cotizacion->revisionTecnica }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Revision Tecnica Precio</label>
					<div class="col-8">
						{{ $cotizacion->revisionTecnicaPrecio }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fotografias Extra</label>
					<div class="col-8">
						{{ $cotizacion->fotografiasExtra }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Fotografias Extra Precio</label>
					<div class="col-8">
						{{ $cotizacion->fotografiasExtraPrecio }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Seguro</label>
					<div class="col-8">
						{{ $cotizacion->seguro }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Seguro Precio</label>
					<div class="col-8">
						{{ $cotizacion->seguroPrecio }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Transporte</label>
					<div class="col-8">
						{{ $cotizacion->transporte }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Transporte Precio</label>
					<div class="col-8">
						{{ $cotizacion->transportePrecio }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Destino Precio</label>
					<div class="col-8">
						{{ $cotizacion->destinoPrecio }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Definicion</label>
                    <div class="col-8">
<pre>
<?php
    print_r($cotizacion->definicion);
?>
</pre>
                    </div>
                </div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Estado</label>
					<div class="col-8">
						{{ $cotizacion->estado }}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Created_at</label>
					<div class="col-8">
						{{ ($cotizacion->created_at ? $cotizacion->created_at->format('d/m/Y H:i:s') : '') }}
					</div>
				</div>



            </form>
			<div class="hr-line-dashed"></div>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm pull-right" data-dismiss="modal" href="javascript:;" onclick="cotizacionCancel()">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/masked-input/masked-input.js"></script>

<script>

@if($isAjaxRequest)
    formInit();
@else
    $(function(){ formInit(); });
@endif


function cotizacionCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formCotizacionShow:cancel');
	@else
		redirect('{{ route('admin.cotizacion.index')  }}');
	@endif
}

function formInit(){

}

</script>
@endpush

