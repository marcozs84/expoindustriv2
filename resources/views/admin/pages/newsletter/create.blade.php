@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')

@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.newsletter.index') }}">Newsletter</a></li>
		<li class="breadcrumb-item active">Nuevo/a newsletter</li>
	</ol>

	<h1 class="page-header">Nuevo Newsletter</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Newsletter</h4>
		</div>
		<div class="panel panel-body">
			<form id="formNewsletterCreate" method="post" class="form-horizontal row">

				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Email</label>
					<div class="col-8">
						{!! Form::text('email', '', [
							'id' => 'email',
							'placeholder' => 'Email',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>
				<div class="form-group col-md-6 col-sm-12 row m-b-15">
					<label class="col-form-label col-4">Status</label>
					<div class="col-8">
						{!! Form::select('status', [
							'1' => 'Activo',
							'0' => 'Inactivo'
						], 1, [
							'id' => 'status',
							'class' => 'form-control',
						]) !!}
					</div>
				</div>


			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="newsletterCancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="newsletterSave()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')



<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){







}

$( "#formNewsletterCreate" ).submit(function( event ) {
	newsletterSave();
	event.preventDefault();
});

function newsletterSave(){
	var url = '{{ route('resource.newsletter.store') }}';
	ajaxPost(url, {
		email: 		$('#formNewsletterCreate #email').val(),
		status: 		$('#formNewsletterCreate #status').val()
	}, function(){
		$(document).trigger('formNewsletterCreate:success');
	});
}

function newsletterCancel(){
	@if($isAjaxRequest)
		$(document).trigger('formNewsletterCreate:cancel');
	@else
		redirect('{{ route('admin.newsletter.index')  }}');
	@endif
}

</script>
@endpush

