@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Announces')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Clientes</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Clientes <small></small></h1>
	<!-- end page-header -->

	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>--}}
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				{{--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>--}}
			</div>
			<h4 class="panel-title">Clientes</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<div class="form-group">
				<a class="btn btn-primary btn-sm hide" href="javascript:;" onclick="clienteCreate()"><i class="fa fa-plus"></i> Crear</a>
				<a class="btn btn-primary btn-sm" href="javascript:;" onclick="clienteDelete()"><i class="fa fa-trash"></i> Eliminar</a>
			</div>
			<table id="data-table" class="table table-striped table-bordered"></table>
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

{{--<script src="/assets/js/demo/table-manage-default.demo.js"></script>--}}
<script>

	var datatable = null;
	var modal = null;

	$(document).ready(function(){
		initDataTable();
	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
//				dom: 'Bfrtip',
				buttons: [
					{
						text: 'Nuevo',
						action: function() { //e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.cliente.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.proveedor_data = 1;
						d.idProveedor = $('#idProveedor').val();
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: 'Nombres y apellidos'},
					{ title: 'E-mail'},
					{ title: 'Empresa'},
					{ title: 'Plans'},
					{ title: '<input type="checkbox" id="idsChecker" value="" />'}
					// ...
				],
				columnDefs: [
					@php $counter = 0 @endphp
					{
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '30px'
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
//							return '<a href="javascript:;" onclick="clienteShow('+row.id+')">'+row.agenda.nombres_apellidos+'</a>';
							var url = '{{ route('admin.cliente.show', [0]) }}';
							url = url.replace('0', row.id);
							if(row.agenda !== undefined) {
								return '<a href="'+url+'">'+row.agenda.nombres_apellidos+'</a>';
							} else {
								return '<a href="'+url+'">'+'---'+'</a>';
							}

						}
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
							return row.usuario.username;
						}
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
//							if( row.usuario.owner.proveedor ) {
//								return row.usuario.owner.proveedor.nombre;
//							} else {
//								return '---';
//							}

							return row.usuario.owner.idProveedor;
							if( row.usuario.owner ) {
								return row.usuario.owner.proveedor.nombre;
							} else {
								return '---';
							}

						}
					},{
						targets: {{ $counter++ }},
						render: function(data, type, row){
							return row.usuario.username;
						}
					}, {
						targets: {{ $counter++ }},
						className: 'text-center',
						width: '20px',
						render: function(data, type, row){
							return '<input type="checkbox" class="chkId" name="id[]" value="'+row.id+'" />';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	function clienteShow(id){
		var url = '{{ route('admin.cliente.show', [0]) }}';
		url = url.replace(0, id);
		var modal = openModal(url, 'Cliente', null, {
			'size': 'modal-lg'
		});
//		setModalHandler('formClienteShow:aceptar', function(){
//			dismissModal(modal);
//			datatable.ajax.reload();
//		});
	}


	function clienteCreate(){
		var url = '{{ route('admin.cliente.create') }}';
		var modal = openModal(url, 'Nuevo Cliente');
		setModalHandler('formClienteCreate:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload();
		});
	}

	function clienteEdit(id){
		var url = '{{ route('admin.cliente.edit', 'idCliente') }}';
		url = url.replace('idCLiente', id);
		var modal = openModal(url, 'Editar Cliente');
		setModalHandler('formClienteEdit:aceptar', function(){
			dismissModal(modal);
			datatable.ajax.reload(null, false);
		});
	}

	function clienteDelete(){
		alert("No está permitido eliminar clientes, debe hacerlo desde AGENDA.");
		return false;
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length == 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '{{ route('resource.cliente.destroy', [0]) }}';

		ajaxDelete(url, ids, function(){
			datatable.ajax.reload(null, false);
		});
	}

</script>
@endpush