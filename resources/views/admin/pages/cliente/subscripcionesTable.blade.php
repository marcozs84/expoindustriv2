
	<table class="table table-condensed table-striped">
		<tr>
			<th>Tipo</th>
			<th>Desde</th>
			<th>Hasta</th>
			<th>Estado</th>
		</tr>
		<tbody>
		@foreach($subscripciones as $sub)
			<tr>
				<td>{{ $sub->Tipo->texto }}</td>
				<td>{{ $sub->fechaDesde }}</td>
				<td>{{ $sub->fechaHasta }}</td>
				<td class="text-center">
					@if($sub->estado == 'Activo')
						<i class="fa fa-check text-primary"></i>
					@else
						<i class="fa fa-times"></i>
					@endif
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>