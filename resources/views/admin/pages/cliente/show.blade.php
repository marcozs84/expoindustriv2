@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.cliente.index') }}">Clientes</a></li>
		<li class="breadcrumb-item active">{{ $obj->Agenda->nombres_apellidos }}</li>
	</ol>

	<h1 class="page-header">Cliente</h1>

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6">

			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Cliente</h4>
				</div>
				<div class="panel panel-body">
						{{--<h5>Cliente</h5>--}}
						<table class="table table-condensed table-striped">
							<tr>
								<td class="text-right">idCliente</td>
								<td>{{ $obj->id }}</td>
							</tr>
							<tr>
								<td class="text-right">Nombres y apellidos</td>
								<td>{{ $obj->Agenda->nombres_apellidos }}</td>
							</tr>
							<tr>
								<td class="text-right">E-mail</td>
								<td>
									@foreach($obj->Agenda->Correos as $correo)
										{{ $correo->correo }} ({{ ucfirst($correo->descripcion) }})<br>
									@endforeach
								</td>
							</tr>
							<tr>
								<td class="text-right">Telefonos</td>
								<td>
									@foreach($obj->Agenda->Telefonos as $telefono)
										{{ $telefono->numero }} ({{ ucfirst($telefono->descripcion) }})<br>
									@endforeach
								</td>
							</tr>
							<tr>
								<td class="text-right">País</td>
								@if(isset($global_paises[$obj->Agenda->pais]))
									<td>{{ $global_paises[$obj->Agenda->pais] }} ({{ strtoupper($obj->Agenda->pais) }})</td>
								@else
									<td></td>
								@endif

							</tr>
							<tr>
								<td class="text-right">Stripe ID</td>
								<td>
									{{ $obj->Usuario->getStripeId() }}
								</td>
							</tr>
						</table>
				</div>
			</div>

			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Información de acceso</h4>
				</div>
				<div class="panel panel-body">

					<form id="formUsuarioEdit" method="get" class="form-horizontal">
						<div class="form-group row m-b-15">
							<label class="col-sm-3 col-form-label">Usuario</label>
							<div class="col-sm-9">
								<input type="text" readonly="" class="form-control-plaintext" value="{{ $obj->Usuario->username }}">
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-sm-3 col-form-label">Contraseña</label>
							<div class="col-sm-9">
								<input id="password" type="password" class="form-control" placeholder="Contraseña">
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-sm-3 col-form-label">Confirme contraseña</label>
							<div class="col-sm-9">
								<input id="password_confirmation" type="password" class="form-control" placeholder="Confirmación de contraseña">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-7 offset-md-3">
								<a href="javascript:;" onclick="actualizarContrasenia()" class="btn btn-sm btn-primary m-r-5">Guardar</a>
							</div>
						</div>

					</form>
				</div>
			</div>

		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">


			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Información de Proveedor (empresa)</h4>
				</div>
				<div class="panel panel-body">

					@if($cci != null)
						<table class="table table-condensed table-striped">
							<tr>
								<td class="text-right">Nombre</td>
								<td>{{ $cci->Proveedor->nombre }}</td>
							</tr>
							<tr>
								<td class="text-right">VAT</td>
								<td>{{ $cci->Proveedor->nit }}</td>
							</tr>
							<tr>
								<td class="text-right">Otros contactos</td>
								<td>
									{{--@foreach($cci->Proveedor->ProveedorContactos as $contacto)--}}
{{--										<a href="{{ route('admin.cliente.show', [$contacto->id]) }}">{{ $contacto->Agenda->nombres_apellidos }}</a><br>--}}
										{{--{{ $contacto->Agenda->nombres_apellidos }}<br>--}}
									{{--@endforeach--}}
								</td>
							</tr>
						</table>
					@else
						<p>No tiene una empresa relacionada</p>
					@endif

				</div>
			</div>

			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Subsripciones</h4>
				</div>
				<div class="panel panel-body">
					<div id="subscripciones_holder"></div>

					<div class="row">
						<div class="col col-12">
							<a href="javascript:;" id="btnCrearSubscripcion" onclick="crearSubscripcion()" class="btn btn-primary pull-right">Crear Subscripcion</a>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class="panel-title">Pagos</h4>
				</div>
				<div class="panel panel-body">
					{{--<h5>Pagos</h5>--}}
					@if($linkStripe != '')
						<a href="{{ $linkStripe }}" target="_blank" class="btn btn-primary" style="vertical-align: middle;"><i class="fa fa-address-card "></i> Ir al perfil de pagos en Stripe</a>
						<br><br>

						<h5>Pagos realizados hasta la fecha</h5>

						<table class="table table-condensed table-striped">
							<tr>
								<th>Description</th>
								<th>Monto</th>
							</tr>
							@foreach($pagos['data'] as $pago)
								<tr>
									<td>{{ $pago['description'] }}</td>
									<td>{{ $pago['amount'] }} {{ $pago['currency'] }}</td>
								</tr>
							@endforeach

						</table>
					@else
						<p>No tiene perfil en Stripe</p>
					@endif
				</div>
			</div>
		</div>
	</div>

@stop

@section('libraries')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

@stop

@push('scripts')
	<script>

		function formInit(){
			updateSubscripcionList();
		}
		formInit();

		function actualizarContrasenia(){
			var url = '{{ route('resource.usuario.update', [$obj->Usuario->id]) }}';
			var data = {
				password : $('#password').val(),
				password_confirmation : $('#password_confirmation').val()
			};
			ajaxPatch(url, data, function(data){
				swal('Contraseña actualizada');
			}, function(data){
				swal('No se pudo actualizar la contraseña', 'warning');
			});
		}

		function crearSubscripcion() {
			var url = '{{ route('admin.subscripcion.create', ['idUsuario' => '_idu_']) }}';
			url = url.replace('_idu_', {{ $obj->Usuario->id }});
			var modal = openModal(url, 'Crear subscripcion');
			setModalHandler('formSubscripcionCreate:aceptar', function () {
				dismissModal(modal);
//				datatable.ajax.reload();
				updateSubscripcionList();
			});
		}

		function updateSubscripcionList() {
			var url = '{{ route('admin.cliente.subscripcionesTable', ['idCliente' => $obj->id]) }}';
			$('#subscripciones_holder').load(url, null, function(data){

			});
		}

	</script>
@endpush

