@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<style>
		.leftlabel {
			word-break:break-all;
		}
	</style>
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.excepcion.index') }}">Excepciones</a></li>
		<li class="breadcrumb-item active">Excepcion</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Editar Template</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">Nuevo template</h4>
        </div>
        <div class="panel-body">

	        <table class="table table-condensed table-striped">
		        <tr>
			        <td class="font-weight-bold text-right">Ip</td>
			        <td class="leftlabel">{{ $e->ip }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Pais</td>
			        <td class="leftlabel">{{ $e->pais }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Código</td>
			        <td class="leftlabel">{{ $e->codigo }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Tipo</td>
			        <td class="leftlabel">{{ $e->tipo }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Mensaje</td>
			        <td class="leftlabel">{{ $e->mensaje }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Url</td>
			        <td class="leftlabel">{{ $e->url }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Route</td>
			        <td class="leftlabel">{{ $e->route }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Archivo</td>
			        <td class="leftlabel">{{ $e->archivo }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Linea</td>
			        <td class="leftlabel">{{ $e->linea }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Params</td>
			        <td class="leftlabel">{{ $e->params }}</td>
		        </tr>
		        <tr>
			        <td class="font-weight-bold text-right">Stack Trace</td>
			        <td class="" style="overflow:auto; max-width:100%; background-color: #000000; color: #ffffff; font-family: monospace; font-size: 13px;">{!! $e->stackTrace !!}</td>
		        </tr>

		        <tr>
			        <td colspan="2" class="text-center">
				        <a id="btnExcepcionCancelar" class="btn btn-white" data-dismiss="modal" href="javascript:;">Cerrar</a>
			        </td>
		        </tr>
	        </table>
        </div>
    </div>

@stop

@section('libraries')
@stop

@section('scripts')
<script>

</script>
@stop

