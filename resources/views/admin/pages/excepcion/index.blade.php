@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('title', 'Excepciones')

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />

	<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
	<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Excepciones</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Listado de excepciones</h1>
	<!-- end page-header -->

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Excepciones</h4>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="form-group col-md-3 col-sm-3 col-lg-3" style="vertical-align:top;">
					<label for="ip" style="">IP</label>
						{!! Form::select('ip', ['uno', 'dos'], null, [
                            'id' => 'ip',
                            'class' => 'form-control multiple-select2',
                            'multiple' => 'multiple',
                            'style' => 'width:100%;'
                        ]); !!}
				</div>
				<div class="form-group col-md-3 col-sm-3 col-lg-3">
					<label class="">Fecha</label><br>
						{!! Form::text('fecha', '', [
							'id' => 'fecha',
							'placeholder' => 'dd/mm/yyyy',
							'class' => 'form-control'
						]) !!}
				</div>
				<div class="form-group col-md-3 col-sm-3 col-lg-3" style="vertical-align:top;">
					<label for="tipo" style="">Tipo</label>
					{!! Form::select('tipo', ['uno', 'dos'], null, [
						'id' => 'tipo',
						'class' => 'form-control multiple-select2',
						'multiple' => 'multiple',
						'style' => 'width:100%;'
					]); !!}
				</div>
				<div class="form-group col-md-3 col-sm-3 col-lg-3" style="vertical-align:top;">
					<label for="estado" style="">Estado</label>
					{!! Form::select('estado', ['uno', 'dos'], null, [
						'id' => 'estado',
						'class' => 'form-control multiple-select2',
						'multiple' => 'multiple',
						'style' => 'width:100%;'
					]); !!}
				</div>
				<div class="form-group m-t-1 p-b-5 col-md-12 col-sm-12 col-lg-12" style="vertical-align:bottom; clear:both;">

					{{--<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionCrear()"><i class="fa fa-plus"></i> Crear</a>--}}
					<a class="btn btn-sm btn-primary" href="javascript:;" onclick="excepcionEliminar()"><i class="fa fa-trash"></i> Eliminar</a>

					<a class="btn btn-sm btn-primary pull-right m-l-5" href="javascript:buscar();"><i class="fa fa-search"></i> Buscar</a>
					<a class="btn btn-sm btn-primary pull-right" href="javascript:limpiar();">Limpiar</a>

                    {{--<button type="submit" class="btn btn-sm btn-primary width-150">Buscar</button>--}}

                </div>
			</div>

			<table id="datatable" class="table table-striped table-bordered width-full table-condensed"></table>
		</div>
	</div>

@stop

@push('scripts')

	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
	<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="/assets/plugins/masked-input/masked-input.js"></script>

<style>
	.mzWordWrap{
		/*max-width:150px !important;*/
		word-wrap: break-word !important;
		white-space:pre-wrap !important;
        text-overflow: ellipsis !important;
	}
</style>
<script>
var datatable = null;
$(document).ready(function(){
	initView();
});

function initView(){

	$(".multiple-select2").select2();
	$(".multiple-select2").on("select2:select", function (e) {
        buscar();
    });

	$(".multiple-select2").on("select2:unselect", function (e) {
        buscar();
    });

	$('#fecha').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	}).on('changeDate', function(){
		buscar();
	});
	$("#fecha").mask("99/99/9999");

	$('#datetimepicker1').datepicker({
		todayHighlight: true
	});

	if ($('#datatable').length !== 0) {
	    datatable = $('#datatable').DataTable({
	        lengthMenu: [5, 10, 20, 30, 50],
	        pageLength: 20,
	        ajax: {
	            url: '{{ route('resource.excepcion.ds') }}',
				method: 'POST',
	            dataSrc: 'data',
	            'headers': {
	                'X-CSRF-TOKEN': '{{ csrf_token() }}'
	            },
                statusCode: {
                    422: ajaxValidationHandler,
                    500: ajaxErrorHandler
                },
	            data: function (d){ // Procesa datos enviados
	                d.fecha = $('#fecha').val();
	                // ...
	            },
	            dataFilter: function(data){ // Procesa datos recibidos
	                // return data;
	                var data = JSON.parse(data);
	                for(var i = 0 ; i < data.data.length; i++){
	                    // ...
	                }
	                return JSON.stringify( data );
	            }
	        },
	        columns: [
	            {data: 'id', title: 'ID'},
	            {data: 'ip', title: 'Ip'},
	            {data: 'pais', title: 'Pais'},
	            {data: 'tipo', title: 'Tipo'},
	            {data: 'url', title: 'Url'},
	            {data: 'mensaje', title: 'Mensaje'},
	            {data: 'params', title: 'Params'},
	            {data: 'estado', title: 'Estado'},
	            {data: 'created_at', title: 'Fecha'},
		        { title: '<input type="checkbox" id="idsChecker" value="" />'},
	            // ...
	        ],
	        columnDefs: [
	            {
	                targets: 0,
	                class: 'mzWordWrap',
	                width: '15px'
	            },{
	                targets: 1,
	                class: 'mzWordWrap',
                    render: function(data, type, row){
                        var html = '<a href="javascript:;" onclick="excepcionMostrar('+ row['id'] +')">'+data+'</a>';
                        {{--html += ' [ <a target="_blank" href="{{ route('admin.excepcion.index') }}/'+ row['id'] +'/editor_pdf">Editor</a> ]';--}}

                        return html;
                    }
	            },{
	                targets: 2,
	                class: 'mzWordWrap',
	            },{
	                targets: 3,
			        width: '100px',
	                class: 'text-left',
	            },{
	                targets: 4,
	                class: 'text-left',
	            },{
	                targets: 5,
	                class: 'text-left',
	            },{
	                targets: 6,
	                class: 'text-center',
			        render: function(data, type, row){
	                	if(data == 0){
	                		return 'Inactivo';
		                } else if(data == 1){
			                return 'Activo';
		                }
			        }
	            },{
	                targets: 7, // estado
	                width: '10px',
	                class: 'text-center',
			        render: function(data, type, row){
	                	var html = '';
	                	html += '<a href="javascript:;" data-toggle="tooltip" data-title="Add to LookUp" onclick="addToWatch(\''+ row.ip +'\', \''+ row.pais +'\')"><i class="fa fa-eye fa-md text-primary"></i></a>';
				        return html;
			        }
	            },{
	                targets: 8,
	                width: '110px',
	                class: 'text-center',
			        render: function(data, type, row) {

	                	return format_datetime(data);
			        }
	            },{
	                targets: 9,
	                width: '10px',
	                class: 'text-center',
	                render: function(data, type, row){
		                var html = '';
		                html += '<input type="checkbox" class="chkId" value="'+ row['id'] +'" />';
		                return html;
	                }
	            }
	        ],
	        fixedHeader: {
	            header: true,
	            headerOffset: $('#header').height()
	        },
	        bSort : false,
	        bAutoWidth: false,
	        processing: true,
	        serverSide: true,
	        responsive: true,
	        fixedColumns: true
	    }).on('draw.dt', function(){
		    $('#idsChecker').on('change', function(e){
			    $('.chkId').prop('checked', $(this).prop('checked'));
		    });
		    $('[data-toggle="tooltip"]').tooltip();
	    });
	}
}


function excepcionMostrar(id){
    var url = '{{ route('admin.excepcion.show', ['idExcepcion']) }}';
    url = url.replace('idExcepcion', id);
    var modal = openModal(url, 'Excepcion',undefined, {
    	'size' : 'modal-message'
    });
    setModalHandler('formExcepcionShow:aceptar', function(){
        dismissModal(modal);
        datatable.ajax.reload(null, false);
    });
}

function addToWatch(ip, pais) {
	var url = '{{ route('admin.excepcion.lookup') }}';
	var data = {
		ip : ip,
		pais: pais,
	};
	ajaxPost(url, data, {
		onSuccess: function(data) {

		}
	});
}

function excepcionEliminar(){
	var ids = [];
    $('.chkId').each(function() {
        if($(this).prop('checked')){
            ids.push($(this).val());
        }
    });

    var url = '{{ route('resource.excepcion.destroy', [0]) }}';

    ajaxDelete(url, ids, function(){
        datatable.ajax.reload(null, false);
    });
}

function buscar(){
	datatable.ajax.reload();
}

function limpiar(){
	datatable.ajax.reload();
}
</script>
@endpush

