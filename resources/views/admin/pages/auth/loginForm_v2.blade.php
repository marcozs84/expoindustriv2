@extends('layouts.empty', ['paceTop' => true, 'bgwhite' => true])

@section('title', 'Login Page')

@section('content')




	<!-- begin login -->
	{{--<div class="login bg-black animated fadeInDown">--}}
		{{--<!-- begin brand -->--}}
		{{--<div class="login-header">--}}
			{{--<div class="brand">--}}
				{{--<b>Expo</b>Industri--}}
				{{--<small>Importación y distribución</small>--}}
			{{--</div>--}}
			{{--<div class="icon">--}}
				{{--<i class="fa fa-lock"></i>--}}
			{{--</div>--}}
		{{--</div>--}}
		{{--<!-- end brand -->--}}
		{{--<!-- begin login-content -->--}}
		{{--<div class="login-content">--}}
			{{--<form action="{{ route('admin.login') }}" method="POST" class="margin-bottom-0">--}}
				{{--{{ csrf_field() }}--}}
				{{--<div class="form-group m-b-20">--}}
					{{--<input name="email" type="text" class="form-control form-control-lg inverse-mode" placeholder="Email Address" required  value="{{ old('email') }}"/>--}}
				{{--</div>--}}
				{{--<div class="form-group m-b-20">--}}
					{{--<input name="password" type="password" class="form-control form-control-lg inverse-mode" placeholder="Password" required />--}}
				{{--</div>--}}
				{{--@if ($errors->any())--}}
					{{--@foreach ($errors->all() as $error)--}}
						{{--<span class="help-block">--}}
			                    {{--<strong>{{ $error }}</strong>--}}
			                    {{--</span>--}}
					{{--@endforeach--}}
				{{--@endif--}}
				{{--<div class="checkbox checkbox-css m-b-20">--}}
					{{--<input type="checkbox" id="remember_checkbox" name="remember" />--}}
					{{--<label for="remember_checkbox">--}}
						{{--Remember Me--}}
					{{--</label>--}}
				{{--</div>--}}
				{{--<div class="login-buttons">--}}
					{{--<button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>--}}
				{{--</div>--}}
			{{--</form>--}}
		{{--</div>--}}
		{{--<!-- end login-content -->--}}
	{{--</div>--}}
	<!-- end login -->


	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-with-news-feed">
			<!-- begin news-feed -->
			<div class="news-feed">
				<div class="news-image" style="background-image: url(/assets/img/e-commerce/cover/cover-crane_opt.jpg)"></div>
				<div class="news-caption">
					<h4 class="caption-title"><b>Expo</b>Industri</h4>
					<p>
						@lang('messages.our_services_slogan')
					</p>
				</div>
			</div>
			<!-- end news-feed -->
			<!-- begin right-content -->
			<div class="right-content">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand">
						<b>Expo</b>Industri
						<small>@lang('messages.quote_joakim')</small>
					</div>
					<div class="icon">
						<i class="fa fa-sign-in"></i>
					</div>
				</div>
				<!-- end login-header -->
				<!-- begin login-content -->
				<div class="login-content">
					<form action="{{ route('public.login') }}" method="POST" class="margin-bottom-0">
						{{ csrf_field() }}
						<div class="form-group m-b-15">
							<input name="email" type="text" class="form-control form-control-lg" placeholder="Email Address" value="{{ old('email') }}" required />
							@if ($errors->has('email'))
								<span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
							@endif
						</div>
						<div class="form-group m-b-15">
							<input name="password" type="password" class="form-control form-control-lg" placeholder="Password" required />
							@if ($errors->has('password'))
								<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        </span>
							@endif
						</div>
						<div class="checkbox checkbox-css m-b-30">
							<input name="remember" type="checkbox" id="remember_checkbox" value="" />
							<label for="remember_checkbox">
								Remember Me
							</label>
						</div>
						<div class="login-buttons">
							<button type="submit" class="btn btn-primary btn-block btn-lg">@lang('messages.log_in')</button>
						</div>
						{{--<div class="m-t-20 m-b-40 p-b-40 text-inverse">--}}
							{{--Not a member yet? Click <a href="register_v3.html" class="text-success">here</a> to register.--}}
						{{--</div>--}}
						<hr />
						<p class="text-center text-grey-darker">
							&copy; ExpoIndustri All Right Reserved {{ date('Y') }}
						</p>
					</form>
				</div>
				<!-- end login-content -->
			</div>
			<!-- end right-container -->
		</div>
		<!-- end login -->

	</div>
	<!-- end page container -->







@endsection