@extends('layouts.empty', ['paceTop' => true])

@section('title', 'Login Page')

@section('content')
	<!-- begin login -->
	<div class="login bg-black animated fadeInDown">
		<!-- begin brand -->
		<div class="login-header">
			<div class="brand">
				<b>Expo</b>Industri
				<small>Importación y distribución</small>
			</div>
			<div class="icon">
				<i class="fa fa-lock"></i>
			</div>
		</div>
		<!-- end brand -->
		<!-- begin login-content -->
		<div class="login-content">
			<form action="{{ route('admin.login') }}" method="POST" class="margin-bottom-0">
				{{ csrf_field() }}
				<div class="form-group m-b-20">
					<input name="email" type="text" class="form-control form-control-lg inverse-mode" placeholder="Email Address" required  value="{{ old('email') }}"/>
				</div>
				<div class="form-group m-b-20">
					<input name="password" type="password" class="form-control form-control-lg inverse-mode" placeholder="Password" required />
				</div>
				@if ($errors->any())
					@foreach ($errors->all() as $error)
						<span class="help-block">
			                    <strong>{{ $error }}</strong>
			                    </span>
					@endforeach
				@endif
				<div class="checkbox checkbox-css m-b-20">
					<input type="checkbox" id="remember_checkbox" name="remember" />
					<label for="remember_checkbox">
						Remember Me
					</label>
				</div>
				<div class="login-buttons">
					<button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
				</div>
			</form>
		</div>
		<!-- end login-content -->
	</div>
	<!-- end login -->
@endsection