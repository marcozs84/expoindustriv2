@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
	<link href="/assets/plugins/jquery-tag-it/css/jquery.tagit.css" rel="stylesheet" />
<style>
	.stub {
		border-bottom:2px dashed #e5e5e5;
		padding-bottom:15px;
	}
	.stubs {
		display:none;
	}
	.danger_element {
		background-color:#ff0000;
	}
</style>
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.categoryDefinition.index') }}">Category Definitions</a></li>
		<li class="breadcrumb-item active">Edit</li>
	</ol>

	<h1 class="page-header">{{ $catd->Categoria->nombre }}</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">{{ $catd->Categoria->nombre }}</h4>
		</div>
		<div class="panel panel-body">

			<h2>Primary</h2>
			<div id="form_container_primary"></div>
			<h2>Secondary</h2>
			<div id="form_container_secondary"></div>

		</div>
	</div>

	<div class="stubs">
		<div id="stub_titulo1" class="form-group stub row m-b-15">
			<label class="col-form-label col-md-3"></label>
			<div class="col-md-9">
				<input type="text" value="" class="form-control text">
				<input type="checkbox" class="checkbox" value="0"> Primary
			</div>
			<div class="width-full p-r-10">
				<a href="javascript:;" class="btn btn-sm btn-primary pull-right">Guardar</a>
			</div>
		</div>

		<div id="stub_titulo2" class="form-group stub row m-b-15">
			<label class="col-form-label col-md-3"></label>
			<div class="col-md-9">
				<input type="text" value="" class="form-control text">
				<input type="checkbox" class="checkbox" value="0"> Primary
			</div>
			<div class="width-full p-r-10">
				<a href="javascript:;" class="btn btn-sm btn-primary pull-right">Guardar</a>
			</div>
		</div>

		<div id="stub_select" class="form-group stub row m-b-15">
			<label class="col-form-label col-md-3"></label>
			<div class="col-md-9">
				<input type="text" value="" class="form-control text">
				<ul id="" class="taggeable inverse"></ul>
				<input type="text" value="" class="form-control validation">
				<input type="checkbox" class="checkbox" value="0"> Primary
			</div>
			<div class="width-full p-r-10 p-t-10">
				<a href="javascript:;" class="btn btn-sm btn-primary pull-right">Guardar</a>
			</div>
		</div>

		<div id="stub_range_year" class="form-group stub row m-b-15">
			<label class="col-form-label col-md-3"></label>
			<div class="col-md-9">
				<input type="text" value="" class="form-control text">
				<input type="text" value="" class="form-control validation">
				<input type="checkbox" class="checkbox" value="0"> Primary
			</div>
			<div class="width-full p-r-10">
				<a href="javascript:;" class="btn btn-sm btn-primary pull-right">Guardar</a>
			</div>
		</div>

		<div id="stub_number" class="form-group stub row m-b-15">
			<label class="col-form-label col-md-3"></label>
			<div class="col-md-9">
				<input type="text" value="" class="form-control text">
				<input type="text" value="" class="form-control validation">
				<input type="checkbox" class="checkbox" value="0"> Primary
			</div>
			<div class="width-full p-r-10">
				<a href="javascript:;" class="btn btn-sm btn-primary pull-right">Guardar</a>
			</div>
		</div>

		<div id="stub_text" class="form-group stub row m-b-15">
			<label class="col-form-label col-md-3"></label>
			<div class="col-md-9">
				<input type="text" value="" class="form-control text">
				<input type="text" value="" class="form-control validation">
				<input type="checkbox" class="checkbox" value="0"> Primary
			</div>
			<div class="width-full p-r-10">
				<a href="javascript:;" class="btn btn-sm btn-primary pull-right">Guardar</a>
			</div>
		</div>
	</div>

@stop

@push('scripts')

<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/assets/plugins/jquery-tag-it/js/tag-it.min.js"></script>


	<script>
		$( "#formCategoryDefinitionEdit" ).submit(function( event ) {
			categoryDefinitionSave();
			event.preventDefault();
		});

		function categoryDefinitionSave(){
			{{--var url = '{{ route('resource.categoryDefinition.update', [$catd->id]) }}';--}}
			{{--ajaxPost(url, {--}}
				{{--nombre: $('#categoryDefinitionNombre').val(),--}}
				{{--descripcion: $('#categoryDefinitionDescripcion').val(),--}}
				{{--idConvenio: $('#categoryDefinitionConvenio').val()--}}
			{{--}, function(){--}}
				{{--$(document).trigger('formCategoryDefinitionEdit:aceptar');--}}
			{{--});--}}
		}

		function categoryDefinitionCancel(){
			@if($isAjaxRequest)
				$(document).trigger('formCategoryDefinitionEdit:cancelar');
			@else
				redirect('{{ route('admin.categoryDefinition.index')  }}');
			@endif
		}

		var formDef = {!! json_encode($catd->campos) !!};

		function formInit(){

			var container = $('#form_container');
			formDef.forEach(function(e) {

				if(e.primary === 1) {
					container = $('#form_container_primary');
				} else {
					container = $('#form_container_secondary');
				}

				switch(e.type) {
					case 'titulo1' :
						container.append(render_titulo1(e));
						break;
					case 'select' :
						container.append(render_select(e));
						$('#elem_'+ e.key +' .taggeable').tagit({
							allowSpaces: "true",
							placeholderText: '',
							afterTagAdded: tagAdded,
							afterTagRemoved: tagRemoved,
						});
						break;
					default:
						container.append(e.key, ' ', e.type);
						container.append('<br>');
				}

				console.log(e);
//				container.append(e.key, ' ', e.type);
//				container.append('<br>');
			});


//			$('#categoryDefinitionNombre').focus();
//			$('#fechaInicio_group').datetimepicker();
			{{--$('#fechaInicio_group').daterangepicker({--}}
				{{--opens: 'right',--}}
				{{--format: 'DD/MM/YYYY',--}}
				{{--separator: ' to ',--}}
				{{--startDate: moment().subtract('days', 29),--}}
				{{--endDate: moment(),--}}
				{{--minDate: '01/01/2012',--}}
				{{--maxDate: '12/31/{{ date('Y') + 1 }}',--}}
			{{--},--}}
			{{--function (start, end) {--}}
				{{--$('#fechaInicio_group input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));--}}
			{{--});--}}
		}
		//formInit();

		function reloadForm() {

			$('#form_container_primary').html('');
			$('#form_container_secondary').html('');

			var url = '{{ route('admin.categoryDefinition.getFormData', ['idCategoryDefinition']) }}';
			url = url.replace('idCategoryDefinition', {{ $catd->id }});
			ajaxGet(url, function(data) {

				formDef = data.data.campos;

				var container = $('#form_container');

				var totalElements = formDef.length;
				var counter = 0;
				var missing = '';

				formDef.forEach(function(e) {

					if(e.primary === 1) {
						container = $('#form_container_primary');
					} else {
						container = $('#form_container_secondary');
					}

					switch(e.type) {
						case 'titulo1' :
						case 'titulo2' :
							stub = render_titulo1(e);
							counter++;
							break;
						case 'select' :
						case 'multiselect' :
							stub = render_select(e);
							counter++;
							break;
						case 'text' :
						case 'range_year' :
						case 'number' :
						case 'boolean' :
						case 'percent10' :
						case 'multiline' :
							stub = render_text(e);
							counter++;
							break;
						default:
							container.append(e.key, ' ', e.type);
							container.append('<br>');
							missing += '\n' + e.key + "("+ e.type +")";
					}

					if(e.primary !== 1 && e.validation !== undefined) {
						if(e.validation !== null && e.validation.trim() !== '' ){
							stub.addClass('danger_element');
							swal({
								icon: 'error',
								text: 'ELEMENTOS SECUNDARIO CON VALIDACON'
							});
						}
					}
					container.append(stub);

				});

				if(totalElements !== counter) {
					swal({
						icon: 'error',
						text: 'ELEMENTOS SIN RENDERIZAR\n'+counter+' of ' + totalElements + "\n" + missing
					});
				}
			});
		}

		reloadForm();

		// ---------- EVENTS

		function getElement(key) {
			var elem = null;
			formDef.forEach(function(e) {
				if(e.key === key){
					elem = e;
				}
			});
			return elem;
		}

		function tagAdded(event, ui) {
			//console.log(ui.tagit('assignedTags'));
//			console.log(ui.getId);
//			console.log($(event.target).prop('id'));
			var id = $(event.target).prop('id');
//			console.log($('#' + id).tagit('assignedTags'));

//			saveTags(id);
		}

		function tagRemoved(event, ui) {
			//console.log(ui.tagit('assignedTags'));
//			console.log(ui.getId);
//			console.log($(event.target).prop('id'));
			var id = $(event.target).prop('id');
//			console.log($('#' + id).tagit('assignedTags'));

//			saveTags(id);
		}

		function saveTags(key) {

			console.log("saving: " + key);

//			return true;

			var elem = null;
			formDef.forEach(function(e) {
				if(e.key === key){
					elem = e;
				}
			});

			elem.data = {};

			var tags = $('#' + key).tagit('assignedTags');
			tags.forEach(function(t) {
				elem.data[t] = t;
			});
			console.log(elem);

		}

		function saveElement(key) {
			console.log("saving: " + key);
			var e = getElement(key);

			switch(e.type) {
				case 'titulo1' :
					e['text'] = $('#text_' + e.key).val();
					e['primary'] = ($('#primary_' + e.key).prop('checked')) ? 1 : 0;
					break;
				case 'titulo2' :
					e['text'] = $('#text_' + e.key).val();
					e['primary'] = ($('#primary_' + e.key).prop('checked')) ? 1 : 0;
					break;
				case 'select' :
				case 'multiselect' :
					saveTags(key);
					e['text'] = $('#text_' + e.key).val();
					e['primary'] = ($('#primary_' + e.key).prop('checked')) ? 1 : 0;
					e['validation'] = ($('#validation_' + e.key).val());
					break;
				case 'range_year' :
				case 'number' :
				case 'text' :
				case 'boolean' :
				case 'percent10' :
				case 'multiline' :
					e['text'] = $('#text_' + e.key).val();
					e['primary'] = ($('#primary_' + e.key).prop('checked')) ? 1 : 0;
					e['validation'] = ($('#validation_' + e.key).val());
					break;
			}

			var url = '{{ route('resource.categoryDefinition.update', [$catd->id]) }}';
			var data = {
				elemento: e
			};
			ajaxPatch(url, data, function(data) {
				swal('Elemento guardado');
				reloadForm();
			});
		}

		// ---------- FUNCTIONS

		function render_titulo1(e) {
			var stub = $('#stub_titulo1').clone();
			stub.prop('id', 'elem_'+e.key);
			stub.find('label')
				.append(e.text)
				.append('<br>')
				.append($('<small>').html(e.type))
				.append('<br>')
				.append($('<small>').html(e.key));
			stub.find('.text')
				.prop('placeholder', '')
				.prop('name', 'text_' + e.key)
				.prop('id', 'text_' + e.key)
				.val(e.text);
			var chk = stub.find('input[type=checkbox]')
				.prop('name', 'primary_' + e.key)
				.prop('id', 'primary_' + e.key);
			if(e.primary === 1) {
				chk.prop('checked', 'checked');
			}

			stub.find('.btn')
				.attr('onclick', 'saveElement("'+ e.key +'")');

			return stub;
		}

		function render_select(e) {
			var stub = $('#stub_select').clone();
			stub.prop('id', 'elem_'+e.key);
			stub.find('label')
				.append(e.text)
				.append('<br>')
				.append($('<small>').html(e.type))
				.append('<br>')
				.append($('<small>').html(e.key))
				.append('<br>')
				.append($('<small>').html(Object.keys(e.data).length + " Items"));

			stub.find('.text')
				.prop('placeholder', 'Texto')
				.prop('name', 'text_' + e.key)
				.prop('id', 'text_' + e.key)
				.val(e.text);
			stub.find('.validation')
				.prop('placeholder', 'Validaciones')
				.prop('name', 'validation_' + e.key)
				.prop('id', 'validation_' + e.key)
				.val(e.validation);
			var chk = stub.find('input[type=checkbox]')
				.prop('name', 'primary_' + e.key)
				.prop('id', 'primary_' + e.key);
			if(e.primary === 1) {
				chk.prop('checked', 'checked');
			}

			var ul = stub.find('ul');
			ul.prop('id', e.key);
			for(var key in e.data) {
				ul.append($('<li>').html(key));
			}
			stub.find('.taggeable').tagit({
				allowSpaces: "true",
				placeholderText: '',
				afterTagAdded: tagAdded,
				afterTagRemoved: tagRemoved,
			});

			stub.find('.btn')
				.attr('onclick', 'saveElement("'+ e.key +'")');

			return stub;
		}

		function render_text(e) {
			var stub = $('#stub_number').clone();
			stub.prop('id', 'elem_'+e.key);
			stub.find('label')
				.append(e.text)
				.append('<br>')
				.append($('<small>').html(e.type))
				.append('<br>')
				.append($('<small>').html(e.key));
			stub.find('.text')
				.prop('placeholder', 'Texto')
				.prop('name', 'text_' + e.key)
				.prop('id', 'text_' + e.key)
				.val(e.text);
			stub.find('.validation')
				.prop('placeholder', 'Validaciones')
				.prop('name', 'validation_' + e.key)
				.prop('id', 'validation_' + e.key)
				.val(e.validation);
			var chk = stub.find('input[type=checkbox]')
				.prop('name', 'primary_' + e.key)
				.prop('id', 'primary_' + e.key);
			if(e.primary === 1) {
				chk.prop('checked', 'checked');
			}

			stub.find('.btn')
				.attr('onclick', 'saveElement("'+ e.key +'")');

			return stub;
		}

	</script>
@endpush

