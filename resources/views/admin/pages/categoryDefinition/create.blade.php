@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Anuncios</li>
	</ol>

	<h1 class="page-header">Nuevo Anuncio</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Anuncio</h4>
		</div>
		<div class="panel panel-body">
			<form id="formAnuncioCreate" method="get" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Nombre</label>
					<div class="col-md-9">
						{!! Form::text('anuncioNombre', '', [
							'id' => 'anuncioNombre',
							'placeholder' => '',
							'class' => 'form-control',
							'autofocus'
						]) !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Convenio</label>
					<div class="col-md-9">
						{!! Form::select('producto', [], null, [
							'id' => 'anuncioConvenio',
							'placeholder' => 'Convenio',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Descripcion</label>
					<div class="col-md-9">
						{!! Form::textarea('anuncioDescripcion', '', [
							'id' => 'anuncioDescripcion',
							'placeholder' => 'Descripcion',
							'class' => 'form-control',
							'rows' => 6
						]) !!}
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-md-3 col-form-label">Periodo de vigencia</label>
					<div class="col-md-9">
						<div class="input-group date" id="fechaInicio_group">
							{!! Form::text('fechaInicio', '', [
								'id' => 'fechaInicio',
								'placeholder' => 'dd/mm/yyyy al dd/mm/yyyy',
								'class' => 'form-control'
							]) !!}
							<span class="input-group-append">
								<span class="input-group-text"><i class="fa fa-calendar"></i></span>
							</span>
						</div>
					</div>
				</div>

				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="anuncioCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="anuncioSave()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@section('libraries')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

@stop

@section('scripts')
	<script>
		$( "#formAnuncioCreate" ).submit(function( event ) {
			anuncioSave();
			event.preventDefault();
		});

		function anuncioSave(){
			var url = '{{ route('resource.anuncio.store') }}';
			ajaxPost(url, {
				nombre: $('#anuncioNombre').val(),
				descripcion: $('#anuncioDescripcion').val(),
				idConvenio: $('#anuncioConvenio').val()
			}, function(){
				$(document).trigger('formAnuncioCreate:aceptar');
			});
		}

		function anuncioCancel(){
			@if($isAjaxRequest)
				$(document).trigger('formAnuncioCreate:cancelar');
			@else
				redirect('{{ route('admin.anuncio.index')  }}');
			@endif
		}

		function formInit(){
			$('#anuncioNombre').focus();
//			$('#fechaInicio_group').datetimepicker();
			$('#fechaInicio_group').daterangepicker({
				opens: 'right',
				format: 'DD/MM/YYYY',
				separator: ' to ',
				startDate: moment().subtract('days', 29),
				endDate: moment(),
				minDate: '01/01/2012',
				maxDate: '12/31/{{ date('Y') + 1 }}',
			},
			function (start, end) {
				$('#fechaInicio_group input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
			});
		}
		formInit();

	</script>
@stop

