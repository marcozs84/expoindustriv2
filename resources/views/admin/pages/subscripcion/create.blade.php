@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="{{ route('admin.home') }}">Admin</a></li>
		<li><a href="{{ route('admin.subscripcion.index') }}">Subscripciones</a></li>
		<li class="">Create</li>
	</ol>

	<h1 class="page-header">Nueva Subscripcion</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nueva Subscripcion</h4>
		</div>
		<div class="panel panel-body">
			<form id="formSubscripcionCreate" method="get" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Tipo</label>
					<div class="col-md-9">
						{!! Form::select('tipo', $tipos, null, [
							'id' => 'tipo',
							'class' => 'default-select2 form-control',
						]); !!}
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Usuario</label>
					<div class="col-md-9">
						@if($idUsuario != 0)
							<span style="margin-top:8px; display:block;">{{ $usuario->username }}</span>
						@else
							{!! Form::select('searchUsuario', [], null, [
								'id' => 'searchUsuario',
								'class' => 'multiple-select2 form-control',
							]); !!}
						@endif
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-md-3 col-form-label">Fecha de inicio</label>
					<div class="col-md-9">
						<div class="input-group date" id="fechaInicio_group">
							{!! Form::text('fechaInicio', '', [
								'id' => 'fechaInicio',
								'placeholder' => 'dd/mm/yyyy',
								'class' => 'form-control'
							]) !!}
							<span class="input-group-append">
								<span class="input-group-text"><i class="fa fa-calendar"></i></span>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group row m-b-15">
					<label class="col-md-3 col-form-label">Fecha de finalizacion</label>
					<div class="col-md-9">
						<div class="input-group date" id="fechaFin_group">
							{!! Form::text('fechaFin', '', [
								'id' => 'fechaFin',
								'placeholder' => 'dd/mm/yyyy',
								'class' => 'form-control'
							]) !!}
							<span class="input-group-append">
								<span class="input-group-text"><i class="fa fa-calendar"></i></span>
							</span>
						</div>
					</div>
				</div>

				<div class="hr-line-dashed"></div>
				<div class="form-group row">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="subscripcionCancel()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="subscripcionSave()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@push('scripts')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="/assets/plugins/masked-input/masked-input.js"></script>

	<script>
		$( "#formSubscripcionCreate" ).submit(function( event ) {
			subscripcionSave();
			event.preventDefault();
		});

		function subscripcionSave(){
			var url = '{{ route('resource.subscripcion.store') }}';

			ajaxPost(url, {
				idSubscripcionTipo: $('#tipo').val(),
				@if($idUsuario != 0)
				idUsuario: {{ $idUsuario }},
				@else
				idUsuario: $('#searchUsuario').val(),
				@endif

				fechaDesde: $('#fechaInicio').val(),
				fechaHasta: $('#fechaFin').val()
			}, function(){
				$(document).trigger('formSubscripcionCreate:aceptar');
			});
		}

		function subscripcionCancel(){
			@if($isAjaxRequest)
				$(document).trigger('formSubscripcionCreate:cancelar');
			@else
				redirect('{{ route('admin.subscripcion.index')  }}');
			@endif
		}

		function formInit(){
			console.log("form init");
			$('#subscripcionNombre').focus();

			$('#fechaInicio').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
				todayHighlight: true
			}).on('changeDate', function(){
//		buscar();
			});
			$("#fechaInicio").mask("99/99/9999");

			$('#fechaFin').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
			}).on('changeDate', function(){
//		buscar();
			});
			$("#fechaFin").mask("99/99/9999");

			{{--$('#periodo_group').daterangepicker({--}}
				{{--opens: 'right',--}}
				{{--format: 'DD/MM/YYYY',--}}
				{{--separator: ' to ',--}}
				{{--startDate: moment().subtract('days', 29),--}}
				{{--endDate: moment(),--}}
				{{--minDate: '01/01/2012',--}}
				{{--maxDate: '12/31/{{ date('Y') + 1 }}',--}}
			{{--},--}}
			{{--function (start, end) {--}}
				{{--$('#periodo_group input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));--}}
			{{--});--}}


			$('#searchUsuario').select2({
//        allowClear: true,
				placeholder: {
					id: "",
					placeholder: "Leave blank to ..."
				},
				ajax: {
					url: '{{ route('resource.usuario.ds') }}',
					dataType: 'json',
					method: 'POST',
					headers: {
						'X-CSRF-Token' : '{!! csrf_token() !!}'
					},
					delay: 250,
					data: function (params) {
						return {
							q: params.term, // search term
							page: params.page
						};
					},
					statusCode: {
						422: ajaxValidationHandler,
						500: ajaxErrorHandler
					},
					processResults: function (data, params) {
						// parse the results into the format expected by Select2
						// since we are using custom formatting functions we do not need to
						// alter the remote JSON data, except to indicate that infinite
						// scrolling can be used
						params.page = data.current_page || 1;

						return {
							results: data.data,
							pagination: {
								more: (params.page * 30) < data.total
							}
						};
					},
					cache: true
				},
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				minimumInputLength: 1,
				templateResult: function(repo) {
					//	if (repo.placeholder) return repo.placeholder;
					if (repo.loading) return repo.text;
					var markup = "<div>" + repo.username + "</div>";
					return markup;
				},
				templateSelection: function formatRepoSelection (repo) {
					//	if (repo.placeholder) return repo.placeholder;
					if(repo.username){
						return repo.username;
					} else {
						return repo.text;
					}
					//return repo.nombre + ' ' + repo.apellidos || repo.text;
				},
//	    allowClear: true
			})
				.trigger('change');
		}
		formInit();

	</script>
@endpush

