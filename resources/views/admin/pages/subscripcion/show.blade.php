@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Anuncios</li>
	</ol>

	<h1 class="page-header">Anuncio</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Anuncio</h4>
		</div>
		<div class="panel panel-body">
			<form id="formAnuncioCreate" method="get" class="form-horizontal">
				<h5>Destino</h5>
				<table class="table table-condensed table-striped">
					<tr>
						<td class="text-right">idDestino</td>
						<td>{{ $pub->idProducto }}</td>
						<td class="text-right">estado</td>
						<td>{{ $pub->estado }}</td>
					</tr>
					<tr>
						<td class="text-right">fechaInicio</td>
						<td>{{ $pub->fechaInicio }}</td>
						<td class="text-right">fechaFin</td>
						<td>{{ $pub->fechaFin }}</td>
					</tr>
					<tr>
						<td class="text-right">dias</td>
						<td>{{ $pub->dias }}</td>
						<td class="text-right"></td>
						<td></td>
					</tr>
					<tr>

						<td class="text-right">precio</td>
						<td>{{ $pub->precio }}</td>
						<td class="text-right">moneda</td>
						<td>{{ $pub->moneda }}</td>
					</tr>
				</table>

				<h5>Producto</h5>
				@if($pub->Producto)

					<table class="table table-condensed table-striped">
						<tr>
							<td class="text-right">idMarca</td>
							<td>{{ $pub->Producto->idMarca }}
								@if($pub->Producto->Modelo)
									({{ $pub->Producto->Modelo->nombre }})
								@endif
							</td>
							<td class="text-right">idModelo</td>
							<td>{{ $pub->Producto->idModelo }}
								@if($pub->Producto->Modelo)
									({{ $pub->Producto->Modelo->nombre }})
								@endif
							</td>
						</tr>
						<tr>
							<td class="text-right">idProductoTipo</td>
							<td>{{ $pub->Producto->idProductoTipo }}</td>
							<td class="text-right">idProductoEstado</td>
							<td>{{ $pub->Producto->idProductoEstado }}</td>
						</tr>
						<tr>
							<td class="text-right">owner_type</td>
							<td>{{ $pub->Producto->owner_type }}</td>
							<td class="text-right">owner_id</td>
							<td>{{ $pub->Producto->owner_id }}</td>
						</tr>
						<tr>
							<td class="text-right">codigo</td>
							<td>{{ $pub->Producto->codigo }}</td>
							<td class="text-right">nombre</td>
							<td>{{ $pub->Producto->nombre }}</td>
						</tr>
						<tr>
							<td class="text-right">precioUnitario</td>
							<td>{{ $pub->Producto->precioUnitario }}</td>
							<td class="text-right">monedaUnitario</td>
							<td>{{ $pub->Producto->monedaUnitario }}</td>
						</tr>
						<tr>
							<td class="text-right">precioProveedor</td>
							<td>{{ $pub->Producto->precioProveedor }}</td>
							<td class="text-right">monedaProveedor</td>
							<td>{{ $pub->Producto->monedaProveedor }}</td>
						</tr>
					</table>


					<h5>ProductoItem</h5>
					@if($pub->Producto->ProductoItem)

						<table class="table table-condensed table-striped">
							<tr>
								<td class="text-right">definicion</td>
								<td>
									@if($pub->Producto->ProductoItem->definicion)
										@foreach($pub->Producto->ProductoItem->definicion as $key => $value)
											{{ $key }} => {{ $value }} <br>
										@endforeach
									@endif
								</td>
								<td class="text-right">dimension</td>
								<td>
									@if($pub->Producto->ProductoItem->dimension)
										@foreach($pub->Producto->ProductoItem->dimension as $key => $value)
											{{ $key }} => {{ $value }} <br>
										@endforeach
									@endif
								</td>
							</tr>
						</table>

						<h5>Ubicacion</h5>
						@if($pub->Producto->ProductoItem->Ubicacion)

							<table class="table table-condensed table-striped">
								<tr>
									<td class="text-right">pais</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->pais }}</td>
									<td class="text-right">ciudad</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->ciudad }}</td>
								</tr>
								<tr>
									<td class="text-right">codigo postal (cpostal)</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->cpostal }}</td>
									<td class="text-right">direccion</td>
									<td>{{ $pub->Producto->ProductoItem->Ubicacion->direccion }}</td>
								</tr>
							</table>
						@else
							<p>No tiene Ubicacion relacionada</p>
						@endif
					@else
						<p>No tiene ProductoItem relacionado</p>
					@endif

					<h5>Galerias</h5>
					@if($pub->Producto->Galerias)

						<table class="table table-condensed table-striped">
							@foreach($pub->Producto->Galerias as $gal)
							<tr>
								<td class="text-right">idGaleriaTipo</td>
								<td>{{ $gal->idGaleriaTipo }}</td>
								<td class="text-right">Imagenes</td>
								<td>{{ $gal->Imagenes->count() }}</td>
							</tr>
							<tr>
								<td colspan="2" class="text-right">
									@foreach($gal->Imagenes as $img)
										<img src="{{ $img->ruta_publica_producto_thumb }}" alt="" class="img-fluid" style="max-width:100px;">
									@endforeach
								</td>
							</tr>
							@endforeach
						</table>
					@else
						<p>No tiene ProductoItem relacionado</p>
					@endif

				@else
					<p>No tiene producto relacionado</p>
				@endif

			</form>
		</div>
	</div>

@stop

@section('libraries')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

@stop

@section('scripts')
	<script>

		function formInit(){
		}
		formInit();

	</script>
@stop

