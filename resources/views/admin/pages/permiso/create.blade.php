@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Permisos</li>
	</ol>

	<h1 class="page-header">Nuevo Permiso</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo Permiso</h4>
		</div>
		<div class="panel panel-body">
			<form id="formPermisoCreate" method="get" class="form-horizontal">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Nombre</label>
                    <div class="col-sm-10">
                        {!! Form::text('permisoNombre', '', [
                            'id' => 'permisoNombre',
                            'placeholder' => '',
                            'class' => 'form-control',
                            'autofocus'
                        ]) !!}
                    </div>
                </div>
				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-2">Descripcion</label>
                    <div class="col-md-10">
                        {!! Form::textarea('permisoDescripcion', '', [
                            'id' => 'permisoDescripcion',
                            'placeholder' => 'Descripcion',
                            'class' => 'form-control',
                            'rows' => 6
                        ]) !!}
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="permisoCancelar()">Cerrar</a>
                        <a class="btn btn-primary btn-sm" href="javascript:;" onclick="permisoGuardar()" >Guardar</a>
                    </div>
                </div>
            </form>
		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>
$( "#formPermisoCreate" ).submit(function( event ) {
	permisoGuardar();
	event.preventDefault();
});

function permisoGuardar(){
	var url = '{{ route('resource.permiso.store') }}';
	ajaxPost(url, {
		nombre: $('#permisoNombre').val(),
		descripcion: $('#permisoDescripcion').val(),
		estado: 1,
		idConvenio: $('#permisoConvenio').val()
    }, function(){
		$(document).trigger('formPermisoCreate:aceptar');
	});
}

function permisoCancelar(){
	@if($isAjaxRequest)
		$(document).trigger('formPermisoCreate:cancelar');
	@else
		redirect('{{ route('admin.permiso.index')  }}');
	@endif
}

function formInit(){
	$('#permisoNombre').focus();
}
formInit();

</script>
@endpush

