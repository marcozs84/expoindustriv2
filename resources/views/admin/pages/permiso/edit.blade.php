@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
	<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item active">Permisos</li>
	</ol>

	<h1 class="page-header">Editar permiso</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Editar permiso</h4>
		</div>
		<div class="panel panel-body">

			<ul class="nav nav-pills nav-tabs-inverse nav-justified nav-justified-mobile">
				<li class="nav-item"><a href="#permiso-tab-1" data-toggle="tab" class="nav-link active">Información</a></li>
				<li class="nav-item"><a href="#permiso-tab-2" data-toggle="tab" class="nav-link">Roles</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane fade active show" id="permiso-tab-1">

					<form id="formPermisoEdit" method="get" class="form-horizontal">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Nombre</label>
							<div class="col-sm-10">
								{!! Form::text('permisoNombre', $permiso->nombre, [
									'id' => 'permisoNombre',
									'placeholder' => '',
									'class' => 'form-control',
									'autofocus'
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Descripcion</label>
							<div class="col-md-10">
								{!! Form::textarea('permisoDescripcion', $permiso->descripcion, [
									'id' => 'permisoDescripcion',
									'placeholder' => 'Descripcion',
									'class' => 'form-control',
									'rows' => 6
								]) !!}
							</div>
						</div>

						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-lg-12 text-right">
								<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;">Cancelar</a>
								<a class="btn btn-primary btn-sm" href="javascript:;" onclick="permisoGuardar()" >Guardar</a>
							</div>
						</div>
					</form>
				</div>
				<div class="tab-pane fade" id="permiso-tab-2">

					<form id="formMateriaTutorAdd" action="post" class="form-inline">
						<div class="row">
							<div class="form-group m-t-10 col-md-10 col-sm-10 col-lg-10">
								{!! Form::select('searchRoles', $roles, null, [
									'id' => 'searchRoles',
									'class' => 'default-select2 form-control',
								]); !!}
							</div>
							<div class="form-group m-t-10 p-b-5  col-md-2 col-sm-2 col-lg-2" style="vertical-align:bottom;">
								<a class="btn btn-sm btn-primary" href="javascript:;" onclick="editarRole()"><i class="fa fa-info-circle"></i> Abrir</a>
							</div>
						</div>
					</form>
					<br>
					<h5><i class="fa fa-chevron-right"></i> Roles en los que el permiso está disponible</h5>
					<table class="table table-condensed table-striped" style="border-top:1px solid #e5e5e5;">
						<thead>
							<tr>
								<th>Role</th>
								<th>Convenios</th>
							</tr>
						</thead>
						<tbody>
						@foreach($rolesP as $roleId => $convenios)
							<tr>
								<td><a href="javascript:;" onclick="editarRole({{ $roleId }})">{{ $roles[$roleId] }}</a></td>
								<td style="text-transform: uppercase;">{{ implode(', ', ['']) }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>

@stop

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>
<script>

	$.getScript('/assets/plugins/select2/dist/js/select2.min.js');

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

$('#permisoNombre').focus();
$( "#formPermisoEdit" ).submit(function( event ) {
	permisoGuardar();
	event.preventDefault();
});

function formInit(){
	$('.multiple-select2, .default-select2').select2();
}

function permisoGuardar(){
	var url = '{{ route('resource.permiso.update', [$permiso->id]) }}';
	ajaxPatch(url, {
		nombre: $('#permisoNombre').val(),
		descripcion: $('#permisoDescripcion').val(),
		estado: 1,
		idConvenio: $('#permisoConvenio').val()
    }, function(){
		$(document).trigger('formPermisoEdit:aceptar');
	});
}

function permisoCancelar(){
	@if($isAjaxRequest)
		$(document).trigger('formPermisoEdit:cancelar');
	@else
		redirect('{{ route('admin.permiso.index')  }}');
	@endif
}

function editarRole(id){
	if(id == undefined){
		var id = $('#searchRoles').val();
	}

	var url = '{{ route('admin.role.index') }}/'+id+'/edit';
	var modal = openModal(url, 'Editar Role', null, { size: 'modal-lg' });
	setModalHandler('formRoleEdit:cancelar', function(){

		@if($isAjaxRequest)
			reloadModal('#formPermisoEdit');
		@endif

		dismissModal(modal);
	});
}

</script>
@endpush

