@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@section('css')
	<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
@stop

@section('content')

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="/admin">Admin</a></li>
		<li class="{{ route('admin.anuncio.index') }}">Anuncios</li>
	</ol>

	<h1 class="page-header">Anuncio</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Anuncio</h4>
		</div>
		<div class="panel panel-body">
			<form id="formConfiguracionEdit" method="get" class="form-horizontal">
				<h5>Anuncio</h5>
				<table class="table table-condensed table-striped">
					<tr>
						<td class="text-right">Llave</td>
{{--						<td>{!! Form::text('llave', $configuracion->llave, ['id' => 'llave', 'class' => 'form-control', 'placeholder' => 'Llave']) !!}</td>--}}
						<td>{{ $configuracion->llave }}</td>
						<td class="text-right">Valor</td>
						<td>{!! Form::text('valor', $configuracion->valor, ['id' => 'valor', 'class' => 'form-control', 'placeholder' => 'Valor']) !!}
						</td>
					</tr>
					<tr>
						<td class="text-right">Llave</td>
{{--						<td>{!! Form::text('llave', $configuracion->llave, ['id' => 'llave', 'class' => 'form-control', 'placeholder' => 'Llave']) !!}</td>--}}
						<td>{{ $configuracion->llave }}</td>
						<td class="text-right">Valor</td>
						<td>{!! Form::text('valor', $configuracion->valor, ['id' => 'valor', 'class' => 'form-control', 'placeholder' => 'Valor']) !!}
						</td>
					</tr>
					{{--<tr>--}}
						{{--<td class="text-right">fechaInicio</td>--}}
						{{--<td>{{ $configuracion->fechaInicio }}</td>--}}
						{{--<td class="text-right">fechaFin</td>--}}
						{{--<td>{{ $configuracion->fechaFin }}</td>--}}
					{{--</tr>--}}
					{{--<tr>--}}
						{{--<td class="text-right">dias</td>--}}
						{{--<td>{{ $configuracion->dias }}</td>--}}
						{{--<td class="text-right"></td>--}}
						{{--<td></td>--}}
					{{--</tr>--}}
					{{--<tr>--}}

						{{--<td class="text-right">precio</td>--}}
						{{--<td>{{ $configuracion->precio }}</td>--}}
						{{--<td class="text-right">moneda</td>--}}
						{{--<td>{{ $configuracion->moneda }}</td>--}}
					{{--</tr>--}}
				</table>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<div class="col-lg-12 text-right">
						<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="configuracionCancelar()">Cerrar</a>
						<a class="btn btn-primary btn-sm" href="javascript:;" onclick="configuracionGuardar()" >Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>

@stop

@section('libraries')
	<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

	<script src="/assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

@stop

@push('scripts')
<script>

$( "#formConfiguracionEdit" ).submit(function( event ) {
	configuracionGuardar();
	event.preventDefault();
});

function configuracionGuardar(){
	var url = '{{ route('resource.configuracion.update', [$configuracion->id]) }}';
//	console.log($('#formConfiguracionEdit #llave').val());
	console.log($('#formConfiguracionEdit #valor').val());
	ajaxPatch(url, {
//		llave: $('#formConfiguracionEdit #llave').val(),
		valor: $('#formConfiguracionEdit #valor').val(),
	}, function(){
		@if($isAjaxRequest)
			$(document).trigger('formConfiguracionEdit:aceptar');
		@else
			redirect('{{ route('admin.configuracion.index')  }}');
		@endif
	});
}


</script>
@endpush

