<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('admin.includes.head')

	@yield('css')
	<style>
		.select2-container { width:100% !important; }
	</style>
</head>
@php
	$bodyClass = (!empty($boxedLayout)) ? 'boxed-layout' : '';
	$bodyClass .= (!empty($paceTop)) ? 'pace-top ' : '';
	$bodyClass .= (!empty($bodyExtraClass)) ? $bodyExtraClass . ' ' : '';
	$sidebarHide = (!empty($sidebarHide)) ? $sidebarHide : '';
	$sidebarTwo = (!empty($sidebarTwo)) ? $sidebarTwo : '';
	$topMenu = (!empty($topMenu)) ? $topMenu : '';
	$footer = (!empty($footer)) ? $footer : '';

	$pageContainerClass = (!empty($topMenu)) ? 'page-with-top-menu ' : '';
	$pageContainerClass .= (!empty($sidebarRight)) ? 'page-with-right-sidebar ' : '';
	$pageContainerClass .= (!empty($sidebarLight)) ? 'page-with-light-sidebar ' : '';
	$pageContainerClass .= (!empty($sidebarWide)) ? 'page-with-wide-sidebar ' : '';
	$pageContainerClass .= (!empty($sidebarHide)) ? 'page-without-sidebar ' : '';
	$pageContainerClass .= (!empty($sidebarMinified)) ? 'page-sidebar-minified ' : '';
	$pageContainerClass .= (!empty($sidebarTwo)) ? 'page-with-two-sidebar ' : '';
	$pageContainerClass .= (!empty($contentFullHeight)) ? 'page-content-full-height ' : '';

	$contentClass = (!empty($contentFullWidth) || !empty($contentFullHeight)) ? 'content-full-width ' : '';
	$contentClass .= (!empty($contentInverseMode)) ? 'content-inverse-mode ' : '';
@endphp
<body class="{{ $bodyClass }}">
	@include('admin.includes.component.page-loader')

	<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed {{ $pageContainerClass }}">

		@includeWhen($headerStatus != 'hide', 'admin.includes.header')

		@includeWhen($topMenu, 'includes.top-menu')

		@includeWhen(!$sidebarHide, 'admin.includes.sidebar')

		@includeWhen($sidebarTwo, 'includes.sidebar-right')

		<div id="content" class="content {{ $contentClass }}">
			@yield('content')
		</div>

		@includeWhen($footerStatus != 'hide', 'includes.footer')

		@include('admin.includes.component.scroll-top-btn')

	</div>

	<div class="modal fade modal-root col-xs-12" id="stub_modal_window">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"></h4>
					{{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
					{{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
					{{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
					<a href="javascript:;" data-dismiss="modal" class="close " aria-hidden="true" data-toggle="tooltip" data-title="Cerrar ventana" style="float:right; margin-left:0px; font-size:17px; position:absolute; right:15px"><i class="fa fa-times"></i></a>
					<a href="javascript:;" class="close modal_btnReload" aria-hidden="true" data-toggle="tooltip" data-title="Recargar ventana" style="float:right; margin-left:0px; font-size:13px; position:absolute; right:40px"><i class="fa fa-sync"></i></a>
					<a href="javascript:;" class="close modal_btnExternal" aria-hidden="true" data-toggle="tooltip" data-title="Popup" style="float:right; margin-left:0px; font-size:14px; position:absolute; right:70px"><i class="fas fa-external-link-alt"></i></a>
					<a href="javascript:;" class="close modal_btnNewTab" aria-hidden="true" data-toggle="tooltip" data-title="Nuevo Tab" style="float:right; margin-left:0px; font-size:14px; position:absolute; right:100px"><i class="fas fa-external-link-alt"></i></a>

				</div>
				<div class="modal-body ajax_content"></div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Cerrar</a>
					<a href="javascript:;" class="btn btn-sm btn-success">Action</a>
				</div>
			</div>
		</div>
	</div>
	<div id="modals_container"></div>

	@include('includes.page-js')

{{--	@yield('libraries')--}}

	<script>

		var currentNav = '{{ $currentNav }}';
		var ul = $('ul#navigation');
		var badgesKeyCounter = [];

		$(document).ready(function(){
			var nav = {!! json_encode($navigation) !!};

			for(var i = 0 ; i < nav.length ; i++){
				li = navigationGetChilds(nav[i]);
				ul.append(li);
			}

			ul.append('<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>');

			App.initSidebar();

			var el1 = $(':data(route)');
			$(':data(route)').each(function(){
				var element = $(this);
				if(element.data('route') === currentNav){
					$(this).addClass('active');
					element.parents('li').each(function(){
						$(this).addClass('active');
					});
				}
			});

			$('[data-toggle="tooltip"]').tooltip();

			// -----------------------------------------
			@if(env('APP_STAT_INFO') == true)

				setTimeout( function() {
					getPendingFakturas();
					getCounters();
				}, 3000 );

			@endif
		});

		function getCounters() {
			var urlNC = '{{ route('admin.navigation.getCounters') }}';
			var data = {
				llaves: badgesKeyCounter
			};
				ajaxPost(urlNC, data, function(data){
				$.each(data.data, function(key, value){
					$('#nav_' + key + ' a span.badge').html(value);
				});
			}, undefined, { silent: true });
		}

		function getPendingFakturas() {

			var urlPF = '{{ route('admin.navigation.getPendingFakturas') }}';
			var data = {};
			ajaxPost(urlPF, data, function(data){

				$('.fakturas-pending-counter').html(data.data.total);
				var fakturaPendingList = $('.fakturas-pending');
				fakturaPendingList.find('.dropdown-footer').remove();

				$.each(data.data.data, function(key, value){

					var item = $('.faktura-alert-item').clone();
					item.removeClass('faktura-alert-item');
					item.find('.media-heading').html('Faktura #' + value.id);
					item.find('.notification-info').html( value.usuario.owner.agenda.nombres_apellidos );
					item.find('.media-date').html(format_datetime(value.created_at));
					item.find('.media-link').attr('onclick', 'transaccionProcessHome('+ value.id +')');

					fakturaPendingList.append(item);

				});

				var li = $('<li>', {
					'class': 'dropdown-footer text-center'
				}).html('<a href="{{ route('admin.transaccion.index') }}">Ver todas</a>');
				fakturaPendingList.append(li);

			}, undefined, { silent: true });
		}

		function transaccionProcessHome(id){
			var url = '{{ route('admin.transaccion.process', 'idRecord') }}';
			url = url.replace('idRecord', id);
			var modal = openModal(url, 'Procesar Transaccion', null, { size:'modal-lg' });
			setModalHandler('formTransaccionProcess:success', function(){
				dismissModal(modal);
			});
		}


//		$('img').attr('onerror', 'img_error(this);');

//		function img_error(img){
//			var nurl = $(img).prop('src');
//			nurl = nurl.replace('www2', 'www');
//			nurl = nurl.replace('expoindustriv2', 'expoindustri.com');
//			$(img).prop('src', nurl);
//		}

		function navigationGetChilds(nav){

			var li = $('<li>');
			li.data('route', nav.route);
			if(nav.type === 'header'){
				var span = $('<span>').html(nav.title);
				li.addClass('nav-header').append(span);
			} else if(nav.type === 'menu') {
				if(nav.childs !== undefined && nav.childs.length > 0){
					li.addClass('has-sub');  //.html(nav.title);
					var a = $('<a>').prop('href', 'javascript:;');
					var caret = $('<b>').addClass('caret pull-right');
					a.append(caret);

					if(nav.badge !== undefined){
						var badge = $('<span>', {
							class : 'badge pull-right'
						}).html(nav.badge);
						a.append(badge);

						// ------------------
						li.prop('id', 'nav_' + nav.key);
						badgesKeyCounter.push(nav.key);
					}

					if(nav.icon !== undefined && nav.icon !== ''){
						var ic = $('<i>').addClass(nav.icon);
						a.append(ic);
						a.append(' ');
					}
					var span = $('<span>').html(nav.title);
					a.append(span);
					li.append(a);

					var ul = $('<ul>').addClass('sub-menu');
					for(var j = 0 ; j < nav.childs.length ; j++){
						uli = navigationGetChilds(nav.childs[j]);
						ul.append(uli);
					}
					li.append(ul);
				} else {
					var a = $('<a>').prop('href', nav.href);
					if(nav.badge !== undefined){
						var badge = $('<span>', {
							class : 'badge pull-right'
						}).html(nav.badge);
						a.append(badge);

						// ------------------
						li.prop('id', 'nav_' + nav.key);
						badgesKeyCounter.push(nav.key);
					}
					if(nav.icon !== undefined && nav.icon !== ''){
						var ic = $('<i>').addClass(nav.icon);
						a.append(ic);
						a.append(' ');
					}
					var span = $('<span>').html(nav.title);
					a.append(span);
					li.append(a);
				}
			}

			return li;
		}

	</script>

{{--	@yield('scripts')--}}
</body>
</html>
