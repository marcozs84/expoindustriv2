@stack('css')

@yield('content')

@yield('libraries')

@stack('scripts')