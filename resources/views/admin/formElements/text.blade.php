<div class="form-group col-md-12 col-sm-12 row m-b-15">
	<label class="col-form-label col-md-2 col-sm-4">{!! __('form.'.$e['key']) !!}
		@if ( isset($e['translate']) && $e['translate'] == 1)
			<br><a href="javascript:;" onclick="translateFieldML('{{ $e['key'] }}')">Traducciones</a>
		@endif
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>
	<div class="col-md-10 col-sm-8">
	{!! Form::textarea($e['key'], ( isset($e['value']) ) ? $e['value'] : '', [
		'id' => $e['key'],
		'class' => 'form-control',
		'placeholder' => (isset($e['placeholder'])) ? $e['placeholder']: '',
		'rows' => 3
	]) !!}
	</div>
</div>