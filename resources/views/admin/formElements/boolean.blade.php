<div class="form-group col-md-6 col-sm-12 row m-b-15">
	<label class="col-form-label col-md-4 col-sm-4">{!! __('form.'.$e['key']) !!}
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>
	<div class="col-md-8 col-sm-8">
		{!! Form::select($e['key'], [
			'none' => __('form.none'),
			'yes' => __('form.yes'),
			'no' => __('form.no'),
		], ( isset($e['value']) ) ? $e['value'] : '', [
		    'id' => $e['key'],
		    'class' => 'form-control',

		]) !!}
		{{--'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : '',--}}
	</div>
</div>