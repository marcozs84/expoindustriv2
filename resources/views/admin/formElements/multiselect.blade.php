<div class="form-group col-md-6 col-sm-12 row m-b-15">
	<label class="col-form-label col-md-4 col-sm-4">{!! __('form.'.$e['key']) !!}
		{{--@if ($errors->has($key))--}}
			{{--<span class="text-danger">*</span>--}}
		{{--@endif--}}
		<a href="javascript:;" id="errorDisplay_{{ $e['key']}}" class="hide" data-toggle="tooltip" data-title="test message" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
	</label>
	<div class="col-md-8 col-sm-8">
		@php

			array_walk($e['data'], function(&$k, $v) {
				return $k = __('form.'.$v);
			});

			$attribs = [];
			$attribs['id'] = $e['key'];
			$attribs['class'] = 'form-control mselect';
			$attribs['onchange'] = 'registerOption(this)';
			if(isset($e['placeholder']) && $e['placeholder']!= ''){
				$attribs['placeholder'] = __('form.'.$e['placeholder']);
			}

			if(isset($e['value']) && is_array($e['value'])) {
				$newdata = [];
				$selection = [];
				foreach($e['data'] as $k => $v) {
					if( ! in_array($k, $e['value']) ) {
						$newdata[$k] = $v;
					} else {
						$selection[$k] = $v;
					}
				}
				$e['data'] = $newdata;
			}

		@endphp

		{!! Form::select($e['key'], $e['data'], '', $attribs) !!}

		<div id="mEle_{{ $e['key'] }}" class="mSelectOptions">
			<ul>
			@if(isset($e['value']))
				@foreach($selection as $k => $v)
				<li>
					{{ $v }}
					<a href="javascript:;"
					   class="pull-right bclose"
					   onclick="removeOption('{{ $e['key'] }}', '{{ $k }}')">
						<i class="fa fa-trash-alt"></i>
					</a>
				</li>
				@endforeach
			@endif
			</ul>
		</div>

		@if(isset($e['value']))
			<script>

				{{--console.log('{{ $e['key']  }}');--}}
				{{--console.log(hashOptions);--}}

				if(hashOptions['{{ $e['key'] }}'] === undefined){
					hashOptions['{{ $e['key'] }}'] = [];
				}

				@foreach($selection as $k => $v)
					hashOptions['{{ $e['key'] }}'].push({
						key : '{{ $k }}',
						text : '{{ $v }}'
					});

				updateOptions('{{ $e['key'] }}');
				@endforeach
			</script>
		@endif

	</div>
</div>