<div class="form-group col-md-6 col-sm-12 row m-b-15">
	<label class="col-form-label col-md-4 col-sm-4">{!! __('form.'.$e['key']) !!}
		{{--@if ($errors->has($key))--}}
			{{--<span class="text-danger">*</span>--}}
		{{--@endif--}}
		<a href="javascript:;" id="errorDisplay_{{ $e['key']}}" class="hide" data-toggle="tooltip" data-title="test message" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
	</label>
	<div class="col-md-8 col-sm-8">
		@php

			$attribs = [];
			$attribs['id'] = $e['key'];
			$attribs['class'] = 'form-control';
			if(isset($e['placeholder']) && $e['placeholder']!= ''){
				$attribs['placeholder'] = __('form.'.$e['placeholder']);
			}

			$arr = array_reverse($global_paises, true);
		    $arr[''] = __('form.pais_de_fabricacion');
		    $paises = array_reverse($arr, true);
			//$paises = $global_paises;

		@endphp

		{!! Form::select($e['key'], $paises, ( isset($e['value']) ) ? $e['value'] : '', $attribs) !!}
	</div>
</div>