<div class="form-group col-md-6 col-sm-12 row m-b-15">
	<label class="col-form-label col-md-4 col-sm-4">{!! __('form.'.$e['key']) !!}
		@if ( isset($e['translate']) && $e['translate'] == 1)
			<br><a href="javascript:;" onclick="translateField('{{ $e['key'] }}')">Traducciones</a>
		@endif
		{{--@if ($errors->has($e['key))--}}
		<a href="javascript:;" id="errorDisplay_{{ $e['key'] }}" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
		{{--@endif--}}
	</label>
	<div class="col-md-8 col-sm-8">
	@php
		$val = null;
		$val = ( isset($e['value']) ) ? $e['value'] : $val;

		if($e['key'] == 'medida_de_transporte_largo_x_ancho_x_alto'){
			if(session('pubsess', 'none') != 'none'){
				$sess = session('pubsess');
				$largo = $sess['longitud'];
				$ancho = $sess['ancho'];
				$alto = $sess['alto'];

				$val = "{$largo} x {$ancho} x {$alto}";
			}
		}

		if($e['key'] == 'peso_bruto'){
			if(session('pubsess', 'none') != 'none'){
				$sess = session('pubsess');
				$val = str_replace('_sb_', '', $sess['peso']);
			}
		}
	@endphp

	@if(isset($e['compliment']) && $e['compliment'] != '')
		<div class="input-group">
			{!! Form::text($e['key'], $val, [
			'id' => $e['key'],
			'class' => 'form-control',
			'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : ''
		]) !!}
			@if(isset($e['compliment']))
				<span class="input-group-addon">{{ $e['compliment'] }}</span>
			@endif
		</div>
	@else


		{!! Form::text($e['key'], ( isset($e['value']) ) ? $e['value'] : $val, [
			'id' => $e['key'],
			'class' => 'form-control', 
			'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : ''
		]) !!}
	@endif
	</div>
</div>