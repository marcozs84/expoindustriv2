<div class="form-group col-md-6">
	<label class="control-label">{!! __('form.'.$e['key']) !!}
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>
	<br><br>
	<center>
		{!! Form::checkbox($e['key'], '', null, [
		    'id' => $e['key'],
		    'class' => 'form-control',
		    'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : '',
		]) !!}
	</center>

</div>