<div class="form-group col-md-6 col-sm-12 row m-b-15">
	<label class="col-form-label col-md-4 col-sm-4">{!! __('form.'.$e['key']) !!}
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>
	<div class="col-md-8 col-sm-8">
	@php

		$data = [];

		if($e['index_0']!= '' && $e['index_0']!= '0'){
			$e['data'][$e['index_0']] = __('form.'.$e['index_0']);
		}

		for($i = 100; $i >= 0  ; $i-=10){
			$e['data'][$i] = $i;
		}


		$attribs = [];
		$attribs['id'] = $e['key'];
		$attribs['class'] = 'form-control';
		if(isset($e['placeholder']) && $e['placeholder']!= ''){
			$attribs['placeholder'] = __('form.'.$e['placeholder']);
		}

	@endphp

	{!! Form::select($e['key'], $e['data'], ( isset($e['value']) ) ? $e['value'] : '', $attribs) !!}
	</div>
</div>