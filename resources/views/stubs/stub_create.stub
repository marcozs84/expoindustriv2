@extends(($isAjaxRequest == true) ? 'admin.layouts.ajax' : 'admin.layouts.default')

@push('css')
{{ libs_css }}
@endpush

@section('content')

	<ol class="breadcrumb pull-right">
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.{{ resource_name }}.index') }}">{{ title_upper_separated }}</a></li>
		<li class="breadcrumb-item active">Nuevo/a {{ resource_name }}</li>
	</ol>

	<h1 class="page-header">Nuevo/a {{ title_upper_separated }}</h1>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			</div>
			<h4 class="panel-title">Nuevo/a {{ title_upper_separated }}</h4>
		</div>
		<div class="panel panel-body">
			<form id="{{ form_name }}" method="post" class="form-horizontal row">

{{ form_elements }}

			</form>
			<div class="form-group row">
				<div class="col-lg-12 text-right">
					<a class="btn btn-white btn-sm" data-dismiss="modal" href="javascript:;" onclick="{{ resource_name }}Cancel()">Cerrar</a>
					<a class="btn btn-primary btn-sm" href="javascript:;" onclick="{{ resource_name }}Save()" >Guardar</a>
				</div>
			</div>
		</div>
	</div>

@stop

@push('scripts')

{{ libs_js }}

<script>

@if($isAjaxRequest)
	formInit();
@else
	$(function(){ formInit(); });
@endif

function formInit(){

{{ datepickers_default_date }}

{{ datepickers_init }}

{{ searchelements_init }}

}

$( "#{{ form_name }}" ).submit(function( event ) {
	{{ resource_name }}Save();
	event.preventDefault();
});

function {{ resource_name }}Save(){
	var url = '{{ route('resource.{{ resource_name }}.store') }}';
	var {{ form_name }} = $('#{{ form_name }}');
	ajaxPost(url, {
		{{ form_assignments }}
	}, function(){
		$(document).trigger('{{ form_name }}:success');
	});
}

function {{ resource_name }}Cancel(){
	@if($isAjaxRequest)
		$(document).trigger('{{ form_name }}:cancel');
	@else
		redirect('{{ route('admin.{{ resource_name }}.index')  }}');
	@endif
}

</script>
@endpush

