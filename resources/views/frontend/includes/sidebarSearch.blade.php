@push('css')
<style>
	.inputError {
		border:1px solid #ff0000 !important;
	}

	.icon_finder {
		position: absolute;
		left: 80px;
		top:0;
		text-shadow: 3px 7px rgba(0,0,0,0.25);
		opacity: 0.2;
	}
	.icon_finder i {
		font-size:100px;
		/*color:rgba(112, 116, 120, 0.35);*/
		/*color:rgba(125, 125, 125, 0.4);*/
		color:#ffffff;
		text-shadow: 3px 7px rgba(0,0,0,0.25);
	}
	.icon_text {
		position:relative;
		color:#232a30;
		/*color:#ffffff;*/
		text-transform: uppercase;
		text-shadow: 1px 1px rgba(255,255,255,0.25);
	}
	.icon_text a{
		/*color:#232a30;*/
		color:#ffffff;
		font-weight: 900;
		font-size: 25px;
		line-height: 35px;
		text-decoration:none;
		transition-timing-function: cubic-bezier(0.390, 0.575, 0.565, 1.000);

	}

	.icon_text a:hover {
		font-size:28px;
	}
	.spacerbr {
		line-height:28px;
	}

	.bg-gradient-yellow {
		background: rgb(253,226,72) !important;
		background: -moz-linear-gradient(top, rgba(253,226,72,1) 0%, rgba(255,217,0,1) 100%) !important;
		background: -webkit-linear-gradient(top, rgba(253,226,72,1) 0%,rgba(255,217,0,1) 100%) !important;
		background: linear-gradient(to bottom, rgba(253,226,72,1) 0%,rgba(255,217,0,1) 100%) !important;
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fde248', endColorstr='#ffd900',GradientType=0 ) !important;
	}
	.bg-gradient-yellow2 {
		background: rgb(253,226,72) !important;
		background: -moz-linear-gradient(top, rgba(236,214,0,1) 0%, rgba(177,161,34,1) 100%) !important;
		background: -webkit-linear-gradient(top, rgba(236,214,0,1) 0%,rgba(177,161,34,1) 100%) !important;
		background: linear-gradient(to bottom, rgba(236,214,0,1) 0%,rgba(177,161,34,1) 100%) !important;
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ECD600', endColorstr='#B1A122',GradientType=0 ) !important;
	}
	.bg-gradient-blue {
		background: rgb(81,136,218);
		background: -moz-linear-gradient(-45deg, rgba(81,136,218,1) 0%, rgba(52,135,226,1) 100%) !important;
		background: -webkit-linear-gradient(-45deg, rgba(81,136,218,1) 0%,rgba(52,135,226,1) 100%) !important;
		background: linear-gradient(to bottom, rgba(81,136,218,1) 0%,rgba(52,135,226,1) 100%) !important;
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5188da', endColorstr='#3487e2',GradientType=1 ) !important;
	}
	.bg-gradient-blue2 {
		background: rgb(81,136,218);
		background: -moz-linear-gradient(-45deg, rgb(52, 170, 255) 0%, rgba(52, 143, 226, 1) 100%) !important;
		background: -webkit-linear-gradient(-45deg, rgb(52, 170, 255) 0%,rgba(52, 143, 226, 1) 100%) !important;
		background: linear-gradient(to bottom, rgb(52, 170, 255) 0%,rgba(52, 143, 226, 1) 100%) !important;
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#34AAFF', endColorstr='#348FE2',GradientType=1 ) !important;
	}

	.bg-gradient-red {
		background: rgb(255,124,121) !important;
		background: -moz-linear-gradient(top, rgba(255,124,121,1) 0%, rgba(255,91,87,1) 100%) !important;
		background: -webkit-linear-gradient(top, rgba(255,124,121,1) 0%,rgba(255,91,87,1) 100%) !important;
		background: linear-gradient(to bottom, rgba(255,124,121,1) 0%,rgba(255,91,87,1) 100%) !important;
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff7c79', endColorstr='#ff5b57',GradientType=0 ) !important;
	}

	.table_tags tr td {
		color: #444;
		font-weight: 600;
		padding: 5px 0;
		line-height: 10px;
	}

	.card-columns {
		-webkit-column-count: 3;
		-moz-column-count: 3;
		column-count: 4;
		-webkit-column-gap: 1.25rem;
		-moz-column-gap: 1.25rem;
		column-gap: 1.25rem;
	}

	.card {
		position: relative;
		display: block;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-ms-flex-direction: column;
		flex-direction: column;
		background-color: #fff;
		border: none;
		border-radius: 4px;
	}

	.card-columns .card {
		/*display: inline-block;*/
		width: 100%;
		margin-bottom: 5px;
	}

	.card-block {
		/*padding: 15px;*/
	}
</style>
@endpush
<!-- BEGIN search-sidebar -->
<div class="search-sidebar sidelink bg-gradient-yellow2" style="margin-bottom:15px;">
	<div class="text-center" style="font-weight:bold; font-size:14px; position:relative; padding: 20px 0px 10px 0px;">
		<div class="icon_finder">
			<i class="fa fa-search" style=""></i><br><br>
		</div>
		<div class="icon_text" style="">
			@lang('messages.search_alert')<span class="spacerbr"><br></span>
			<a href="javascript:;" onclick="modalAskConsultant()">@lang('messages.make_click_here')</a>
		</div>
	</div>
</div>

@if( $global_isProvider == true )
<div class="search-sidebar sidelink bg-gradient-blue2" style="margin-bottom:15px;">
	<div class="text-center" style="font-weight:bold; font-size:14px; position:relative; padding: 20px 0px 10px 0px;">
		<div class="icon_finder">
			<i class="fa fa-chart-line" style=""></i><br><br>
		</div>
		<div class="icon_text" style="">
			@lang('messages.wanna_expand_business') <span class="spacerbr"><br></span>
			<a href="{{ route("public.{$glc}.landing_expand") }}" >@lang('messages.start_selling')</a>
		</div>
	</div>
</div>
@endif
<div class="search-sidebar">
	<h4 class="title">¡@lang('messages.find_your_machine')!</h4>
	<form id="sideBarSearchForm" action="{{ route("public.{$glc}.search") }}" method="POST" name="filter_form">

		{{ csrf_field() }}
		{{--<div class="form-group">--}}
			{{--<label class="control-label">@lang('messages.criteria')</label>--}}
			{{--<input type="text" class="form-control input-sm" name="keyword" value="" placeholder="Inserte su búsqueda" />--}}
		{{--</div>--}}
		<div class="form-group">
			<label class="control-label">@lang('messages.machines_accesories')</label>

			{!! Form::select('sideSearchTipoMaquina', [
				0 => __('messages.both'),
				\App\Models\Producto::TIPO_MAQUINARIA => __('messages.machines'),
				\App\Models\Producto::TIPO_ACCESORIO => __('messages.accesories'),
			], old('sideSearchTipoMaquina'), [
				'id' => 'sideSearchTipoMaquina',
				'class' => 'form-control',
			]) !!}

		</div>
		<div class="form-group">
			<label class="control-label">@lang('messages.used_new')</label>

			{!! Form::select('sideSearchNuevo', [
				0 => __('messages.both'),
				1 => __('messages.new'),
				2 => __('messages.used'),
			], old('sideSearchNuevo'), [
				'id' => 'sideSearchNuevo',
				'class' => 'form-control',
			]) !!}

		</div>
		<div class="form-group">
			<label class="control-label">@lang('messages.category')</label>

			{!! Form::select('sideSearchCategoria', $global_categorias, old('sideSearchCategoria'), [
				'id' => 'sideSearchCategoria',
				'class' => 'form-control',
				'onchange' => 'changeSubs()',
				'placeholder' => ucfirst(__('messages.all_f'))
			]) !!}

		</div>
		{{--<div class="form-group">--}}
			{{--<label class="control-label">@lang('messages.sub_category')</label>--}}
			{{--{!! Form::select('sideSearchSubCategoria', (isset($subCatList_init)) ? $subCatList_init : [], old('sideSearchSubCategoria'), [--}}
			    {{--'id' => 'sideSearchSubCategoria',--}}
			    {{--'class' => 'form-control',--}}
			    {{--'placeholder' => ucfirst(__('messages.all_f')),--}}
			{{--]) !!}--}}
		{{--</div>--}}
		<div class="form-group">
			<label class="control-label">@lang('messages.tags')</label>
			{!! Form::select('sideSearchTags[]', $grouped_tags, old('sideSearchTags'), [
			    'id' => 'sideSearchTags',
			    'class' => 'form-control multiple-select2',
			    'onclick' => 'modalTags()',
			    'multiple' => 'multiple',
				// 'placeholder' => 'Haga click aquí'
			]) !!}
		</div>
		{{--<div class="form-group">--}}
			{{--<label class="control-label">@lang('messages.brand')</label>--}}
			{{--{!! Form::select('sideSearchMarca', $global_marcas, old('sideSearchMarca'), [--}}
			    {{--'id' => 'sideSearchMarca',--}}
			    {{--'class' => 'form-control',--}}
			    {{--'placeholder' => ucfirst(__('messages.all_f')),--}}
			{{--]) !!}--}}
		{{--</div>--}}
		<div class="form-group">
			<label class="control-label">@lang('messages.price') ({{ strtoupper(session('current_currency', 'usd')) }}):</label>
			<div class="row row-space-0">
				<div class="col-md-5">
					{!! Form::number('sideSearchPrecioFrom', old('sideSearchPrecioFrom'), [
						'id' => 'sideSearchPrecioFrom',
						'class' => 'form-control',
						'placeholder' => __('messages.from')
					]) !!}
				</div>
				<div class="col-md-1 text-center p-t-5 f-s-12 text-muted"></div>
				<div class="col-md-6">
					{!! Form::number('sideSearchPrecioTo', old('sideSearchPrecioTo'), [
						'id' => 'sideSearchPrecioTo',
						'class' => 'form-control',
						'placeholder' => __('messages.to')
					]) !!}
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">@lang('messages.year'):</label>
			<div class="row row-space-0">
				<div class="col-md-5">
					{!! Form::number('sideSearchAnioFrom', old('sideSearchAnioFrom'), [
						'id' => 'sideSearchAnioFrom',
						'class' => 'form-control',
						'placeholder' => __('messages.from')
					]) !!}
				</div>
				<div class="col-md-1 text-center p-t-5 f-s-12 text-muted"></div>
				<div class="col-md-6">
					{!! Form::number('sideSearchAnioTo', old('sideSearchAnioTo'), [
						'id' => 'sideSearchAnioTo',
						'class' => 'form-control',
						'placeholder' => __('messages.to')
					]) !!}
				</div>
			</div>
		</div>
		{{--<div class="form-group">--}}
			{{--<label class="control-label">Tags:</label>--}}
			{{--<table class="table_tags" style="width:100%;">--}}
				{{--@foreach( $tags as $id => $tag )--}}
				{{--<tr>--}}
					{{--<td style="width:20px;"><input type="checkbox"></td>--}}
					{{--<td style="">{{ $tag }}</td>--}}
					{{--<td style="width:10px;">({{ (isset($tag_counts[$id])) ? $tag_counts[$id] : 0 }})</td>--}}
				{{--</tr>--}}
				{{--@endforeach--}}
			{{--</table>--}}
		{{--</div>--}}
		<div class="m-b-30">
			<button type="submit" class="btn btn-sm btn-inverse"><i class="fa fa-search"></i> @lang('messages.search')</button>
		</div>
	</form>

	@if( isset($grouped_tags) )
	<h4 class="title m-b-0 ">Tags</h4>
	<div style="max-height:300px; overflow-y: scroll;">
		<ul class="search-category-list ">

			@foreach( $grouped_tags as $group => $tags )
				@foreach( $tags as $id => $tag )
					@if( isset($tag_counts[$id]) && $tag_counts[$id] > 0 )
						<li><a href="javascript:;" onclick="buscarTag({{ $id }})">{{ $tag }} <span class="pull-right">({{ (isset($tag_counts[$id])) ? $tag_counts[$id] : 0 }})</span></a></li>
					@endif
				@endforeach
			@endforeach
			{{--<li><a href="#">Transporte <span class="pull-right">(10)</span></a></li>--}}
			{{--<li><a href="#">Equipos <span class="pull-right">(15)</span></a></li>--}}
			{{--<li><a href="#">Industrial <span class="pull-right">(32)</span></a></li>--}}
			{{--<li><a href="#">Agricultura <span class="pull-right">(4)</span></a></li>--}}
			{{--<li><a href="#">Montacargas <span class="pull-right">(6)</span></a></li>--}}
			{{--<li><a href="#">Otros <span class="pull-right">(38)</span></a></li>--}}
		</ul>
	</div>

	@endif

</div>
<!-- END search-sidebar -->

<div class="modal fade" id="modalSearchConsultant">
	<div class="modal-dialog" >
		<div class="modal-content" style="margin-top:50px;">
			<div class="modal-body">
				<div class="row">
					<div class="col col-md-12">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.search_alert_title')</h4>
						</div>
						<p class="m-t-10">
							@lang('messages.search_alert_msg')
						</p>
						<hr>
						<p>
							@lang('messages.search_alert_msgfrm')
						</p>
					</div>
				</div>
				<div class="row">
					{{--<div class="col-md-6">--}}
						{{--<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">--}}
							{{--<h4 class="text-center m-t-0">Publicar una maquina</h4>--}}
						{{--</div>--}}

						{{--<div class="text-center" style="margin-top:100px;">--}}
							{{--@if(App::getLocale() == 'se')--}}
								{{--<a href="/publish"><img src="/assets/img/e-commerce/publish_machine2_se.png" alt="Publish a machine"></a>--}}
							{{--@else--}}
								{{--<a href="/publish"><img src="/assets/img/e-commerce/publish_machine2.png" alt="Publish a machine"></a>--}}
							{{--@endif--}}
						{{--</div>--}}
					{{--</div>--}}
					<div class="col-md-12" style="border-left:1px dashed #e5e5e5 !important;">

						<span id="sqError_messages" class="hide">
							<p class="alert alert-danger">@lang('messages.marked_fields_required')</p>
						</span>


						<form id="formSearchQuestion"  action="{{ route('public.registerAdPublisher') }}" method="post" class="text-center">

							<div class="row">
								<div class="col col-md-6">

									@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
										<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')">
										<div id="sqError_apellidos"></div>
									@else
										<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->apellidos }}" disabled>
									@endif

									<input type="hidden" id="sqTipoConsulta" value="search">

									@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
										<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" >
										<div id="sqError_nombres"></div>
									@else
										<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->nombres }}" disabled >
									@endif

									@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
										<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
										<div id="sqError_email"></div>
									@else
										<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->username }}" disabled >
									@endif


									<style>

									</style>
									{{--<select name="pais" id="sqPais" class="login-input">--}}
									{{--<option value="">@lang('messages.select_country')</option>--}}
									{{--</select>--}}

									@if($auth_nombres_apellidos == '')
										{!! Form::select('pais', $global_paises, $global_country_abr, [
											'id' => 'sqPais',
											'class' => 'login-input',
											'placeholder' => __('messages.select_country'),
											'style' => 'width:90% !important;  margin-top:10px;'
										]); !!}
										<div id="adPubError_pais"></div>
									@else
										{!! Form::select('pais', $global_paises, \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->pais, [
											'id' => 'sqPais',
											'class' => 'login-input',
											'placeholder' => __('messages.select_country'),
											'style' => 'width:90% !important; margin-top:10px;',
											'disabled'
										]); !!}
									@endif

									@if($auth_nombres_apellidos == '')
										<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
											<div id="sqError_telefono"></div>
									@else
										<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')"
										       value="{{ (count(\Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos)) ? \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos->first()->numero : '' }}">
									@endif

								</div>
								<div class="col col-md-6">
									<textarea class="login-input form-control" id="sqDescripcion" style="width:95%; height:193px;" placeholder="@lang('messages.comments')"></textarea>
								</div>
							</div>


							{{--<input type="submit" id="adPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
							<a id="btnSearchQuestion" href="javascript:;"  class="promotion-btn m-t-20 m-b-10" onclick="guardarConsultaExterna()" style="width:50%; font-size:16px;">@lang('messages.send')</a>
							{{--<a href="javascript:;" id="adPubBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modalSearchTags">
	<div class="modal-dialog" style="width:80% !important;" >
		<div class="modal-content" style="margin-top:50px;">
			<div class="modal-body">
				<div class="row">
					<div class="col col-md-12">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.tags')</h4>
						</div>
					</div>
				</div>
				<div class="row m-t-10">

					<div class="col-md-12">
						<div class="card-columns" >

					@if( isset($grouped_tags) )

						@foreach( $grouped_tags as $group => $tags )

								<div class="card" >
									<div class="card-block" >
										<h4 class="card-title" style="font-size:14px;">{{ $group }}</h4>
										<table>
											@foreach( $tags as $id => $tag )
											<tr>
												<td style="width:20px;"><input id="tagchk_{{ $id }}" class="tag_selector" name="tagchk_{{ $id }}" data-nombre="{{ $tag }}" value="{{ $id }}" onclick="addTag(this)" type="checkbox"></td>
												<td style=""><label style="font-weight:normal;" for="tagchk_{{ $id }}">{{ $tag }}</label></td>
											</tr>
											@endforeach
										</table>
									</div>
								</div>

						@endforeach
					@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.js"></script>
<script>

	// TODO: Agregar captcha al formulario de consulta externa.

	$(document).ready(function(){
		$('#sqPais').select2({
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			templateResult: formatRepo,
			templateSelection: formatRepoSelection
		});
		$('#sideSearchTags').select2({});
		$('#sideSearchTags').on("select2:unselecting", function(e) {
			$(this).data('state', 'unselected');

			var data = e.params.args.data;
//
			var index = tagsSelected.indexOf( data.id );
			if (index > -1) {
				tagsSelected.splice(index, 1);
				$('#tagchk_' + data.id).prop('checked', false);
			}
		}).on("select2:opening", function(e) {
			var $self = $(this);
			if ($(this).data('state') === 'unselected') {
				$(this).removeData('state');
			} else {
				modalTags();
			}
			e.preventDefault();
			$self.select2('close');
		});

		@if(isset($ajaxSearch) && $ajaxSearch)
		$('#sideBarSearchForm').submit(function(event){
			triggerSearch();
			event.preventDefault();
		});
		@endif

//		changeSubs();

	});

	function guardarConsultaExterna(){
		var url = '{{ route('public.consultaExterna.store') }}';
		var data = {
			nombres : $('#sqNombres').val(),
			apellidos : $('#sqApellidos').val(),
			email : $('#sqEmail').val(),
			pais : $('#sqPais').val(),
			telefono : $('#sqTelefono').val(),
			descripcion : $('#sqDescripcion').val(),
			tipo : $('#sqTipoConsulta').val(),
		};
		ajaxPost(url, data, {
			onSuccess: function(data){
				$('#modalSearchConsultant').modal('toggle');
				$('#sqNombres').val('');
				$('#sqApellidos').val('');
				$('#sqEmail').val('');
				$('#sqPais').val('');
				$('#sqTelefono').val('');
				$('#sqDescripcion').val('');
				swal("@lang('messages.comments_sent')!", "@lang('messages.su_consulta_enviada_correctamente')", "success");
				{{--.then(function(response){--}}
					{{--redirect('{{ route("private.{$glc}.announcements") }}');--}}
				{{--});--}}
			},
			onValidation: function(data){

				$('#sqNombres').removeClass('inputError');
				$('#sqApellidos').removeClass('inputError');
				$('#sqEmail').removeClass('inputError');
				$('#sqPais').removeClass('inputError');
				$('#sqTelefono').removeClass('inputError');
				$('#sqDescripcion').removeClass('inputError');
				$('.select2-container').removeClass('inputError');

				$.each(data, function(key, value){

					key = key.replace(/^\w/, function (chr) {
						return chr.toUpperCase();
					});

					if(key === 'Pais'){
						$('.select2-container').addClass('inputError');
					}

					$('#sq' + key).addClass('inputError');

				});

				$('#sqError_messages').removeClass('hide');
			}
		});
	}

	function modalAskConsultant(){

		$('#sqPais').val('{{ $global_localization }}');

		@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
			$('#sqNombres').val('');
			$('#sqApellidos').val('');
			$('#sqEmail').val('');
			$('#sqTelefono').val('');
			$('#sqDescripcion').val('');
		@endif

//		$('.select2-container').removeClass('inputError');

		$('#modalSearchConsultant').modal();
	}

	function modalTags(){
		$('#sideSearchTags').select2('close');
		$('#modalSearchTags').modal();
	}

	var tagsSelected = [];
	function addTag(e) {
		tagsSelected = [];
		$('.tag_selector').each( function(k,v) {
			if( $(v).prop('checked') === true ) {
				tagsSelected.push( $(v).val() );
			}
		});

		$('#sideSearchTags').val( tagsSelected ).trigger('change');
	}

	function buscarTag( id ) {
		tagsSelected = [];
		tagsSelected.push( id );
		$('#sideSearchTipoMaquina').val( 0 ).change();
		$('#sideSearchTags').val( tagsSelected ).trigger('change');
		$('#sideBarSearchForm').submit();
	}
//	$('#modalSearchConsultant').modal();

	var codigosPais = {!! json_encode($global_paisesCodigo) !!};

	function formatRepo (repo) {

		if (repo.loading) {
			return repo.text;
		}

		var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.text + "</div>";

		if (repo.description) {
			markup += "<div class='select2-result-repository__description'> " + repo.description + "</div>";
		}

		return markup;
	}

	function formatRepoSelection (repo) {
		return repo.full_name || getFullName(repo);
	}

	function getFullName(repo){
		if(repo.id != ''){
			return '<span class="flag-icon flag-icon-'+ repo.id +' "></span> &nbsp;&nbsp;' + '+' + codigosPais[repo.id] + ' ' + repo.text;
		} else {
			return repo.text;
		}

	}

	var cats = {!! json_encode($categorias) !!};


	function changeSubs(){
		cat = $('#sideSearchCategoria').val();

		{{--if(cat == '') {--}}
			{{--$('#sideSearchSubCategoria').append($('<option>', {--}}
				{{--'value': ''--}}
			{{--}).html('@lang("messages.select_category")'));--}}
		{{--}--}}

		if(cat > 0){
			$('#sideSearchSubCategoria').html('');
			$.each(cats, function(k,v){
				if(v.id == cat){
					subs = v.subcategorias;
					$('#sideSearchSubCategoria').append($('<option>', {
						'value': ''
					}).html('@lang("messages.select_subcategory")'));
					$.each(subs, function(k1, v1){

						@if(isset($_REQUEST['sideSearchSubCategoria']) && $_REQUEST['sideSearchSubCategoria'] > 0)

							if(v1.id === {{ $_REQUEST['sideSearchSubCategoria'] }}){
								$('#sideSearchSubCategoria').append($('<option>', {
									'value': v1.id,
									'selected' : 'selected'
								}).html(v1.traduccion.{{ $glc }}));
							} else {
								$('#sideSearchSubCategoria').append($('<option>', {
									'value': v1.id
								}).html(v1.traduccion.{{ $glc }}));
							}

						@else
							$('#sideSearchSubCategoria').append($('<option>', {
								'value': v1.id
							}).html(v1.traduccion.{{ $glc }}));
						@endif
					});
				}
			});
		} else {
			$('#sideSearchSubCategoria').html('');
			$('#sideSearchSubCategoria').append($('<option>', {
				'value': ''
			}).html('@lang('messages.all_f')'));
		}

		
	}


</script>
@endpush
