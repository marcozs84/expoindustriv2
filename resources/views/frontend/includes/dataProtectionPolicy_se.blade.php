<div class="section-terms m-0 p-0 ">

	<h5 style="margin-top:0px;">Integritetspolicy </h5>
	Vi på Expoindustri värnar om våra användare, vilket innebär att vi alltid ser till att samla in och använda dina uppgifter med hänsyn till din integritet. Det är viktigt för oss att du får reda på vilken information vi samlar in om dig och hur vi använder den. Du är alltid välkommen att kontakta oss om du har frågor om hur vi behandlar dina personuppgifter. Kontaktuppgifter står sist i denna text.
	<br><br>

	<h5>Vad är en personuppgift och vad är en behandling av personuppgifter? </h5>
	Personuppgifter är alla uppgifter om en levande fysisk person som direkt eller indirekt kan kopplas till den personen. Det handlar inte bara om namn och adress utan även om till exempel bilder och e-postadresser.
	<br><br>

	<h5>Personuppgiftsansvarig</h5>
	Expoindustri Sweden AB  (“expoindustri”) är personuppgiftsansvarig för behandlingen av dina personuppgifter, om inget annat anges i denna information. (Org. Nr.559174–5194).
	<br><br>

	<h5>Vilka personuppgifter samlar vi in om dig och varför?</h5>
	Vi samlar in information om dig när du registrerar ett användarkonto och när du använder våra tjänster. Då lämnar du uppgifter om dig själv och ditt företag såsom namn, telefonnummer, mejladress, organisationsnummer och adress men även innehåll i meddelanden och annonser. Personuppgifter i den här kategorin är sådana som du aktivt lämnar till oss genom att till exempel fylla i formulär, skicka meddelanden till oss.
	<br><br>
	Vi behandlar också uppgifter om dig som samlas in automatiskt när du använder våra tjänster. Vi registrerar till exempel vilka av våra tjänster du använder, vilka sidor du besöker och hur du beter dig på vår webbplats, vilken teknologi du använder, om du använder dator, surfplatta eller mobil och vilken webbläsare du använder när du surfar på expoindustri, vilken IP-adress du använder och liknande uppgifter. Vi samlar också in uppgifter om vilka annonser du klickar på och vilken reklam vi har visat för dig. Informationen som samlas in automatiskt när du använder tjänsterna.
	<br><br>

	<h5>Dina transaktioner</h5>
	Om du köper en tjänst av oss, till exempel annonsering på expoindustri. Denna information innehåller betalningsinformation, som numret på ditt kredit- eller betalkort och andra kortuppgifter, använder vi ett tredje part Stripe (läs hur dom hanterar dina kredit eller betalkort på, www.stripe.com) Expoindustri Sweden AB lagrar/hanterar inte några betalinformation.
	<br><br>

	<h5>Till vilka kan personuppgifter lämnas ut?</h5>
	Vi delar med oss av information om dig till andra, exempelvis leverantörer som behandlar personuppgifter för vår räkning, myndigheter och samarbetspartners. Här kan du läsa mer om vilka vi lämnar ut personuppgifter till.
	<br><br>

	<h5>Leverantörer som behandlar personuppgifter för vår räkning</h5>
	Vi behöver ibland anlita leverantörer för att leverera våra tjänster till dig. Sådana leverantörer kan till exempel leverera system för utskick av mejl och push notiser, molntjänster för lagring av data, analysverktyg eller system för publicering och visning av bannerreklam. Det kan också vara kundserviceföretag, IT-konsulter eller logistik- och transportföretag som anlitas för att leverera varor till dig. Vi är då fortsatt ansvariga för den behandling av dina personuppgifter som underleverantören gör. Leverantörerna får inte använda uppgifterna för andra ändamål än de som vi anger. Vi delar personuppgifter med leverantörer för att det är nödvändigt för att fullgöra vårt avtal med dig.
	<br><br>

	<h5>Hur länge sparar vi dina personuppgifter? </h5>
	Vi kommer att behandla dina personuppgifter under så lång tid som är nödvändig med hänsyn till ändamålet med behandlingen. Det innebär att olika uppgifter kommer att sparas olika länge. Vissa uppgifter måste vi spara under längre tid för att uppfylla våra lagstadgade skyldigheter.
	<br><br>

	<ul>
		<li>Kontoinformation - Uppgifter som du lämnat till oss som är kopplade till ditt konto, till exempel ditt användarnamn, e-postadress, postnummer m.m., och uppgift om när du registrerade ditt konto. Dessa uppgifter lagras så länge du är aktiv kund hos oss och under tre år därefter.</li>
		<li>Meddelanden som du skickat till Expoindustri kundservice. Dessa uppgifter lagras så länge du är aktiv kund hos oss och under tre år därefter.</li>
		<li>Uppgifter som du har lämnat och som du själv har tillgång till i din profil, till exempel logo, företagsinformation, sparade annonser. Dessa uppgifter lagras så länge du är aktiv kund hos oss och under tre år därefter.</li>
		<li>Annonser och information som samlats in i samband med din annonsering. Detta är uppgifter som du har lämnat till oss när du använt våra tjänster, men kanske inte kan se i din profil. Dessa uppgifter består av annonser som du lagt in på Expoindustri och information kring din annonsering. Dessa uppgifter lagras i högst tre år från att du annonserade eller på annat sätt lämnade uppgifterna till oss.</li>
		<li>Data som vi registrerar när du surfar runt på Expoindustri. Det kan vara uppgifter om vilka annonser du tittar på, vilka knappar du trycker på och vilka sidor du är inne på. Dessa uppgifter lagras i högst 18 månader från det att de samlades in.</li>
		<li>Uppgifter om din geografiska position. Dessa baseras på din IP-adress eller din mobila enhets platsinformation. Uppgifter om var du är när du använder Expoindustri. sparas i högst 30 dagar och förnyas så snart en ny uppgift kommer in, då raderas den gamla uppgiften.</li>
	</ul>

	<h5>Vad är dina rättigheter som registrerad? </h5>
	Som registrerad har du enligt gällande lagstiftning ett antal rättigheter. Du har rätt till att få ett utdrag som visar vilka personuppgifter vi har registrerade om dig. Du kan begära rättelse av felaktiga uppgifter och radering.
	<br><br>

	<h5>Kontakta oss vid frågor om hur vi behandlar personuppgifter.</h5>
	Om du har frågor om hur vi behandlar personuppgifter kontakta oss på info@expoindustri.com.


</div>