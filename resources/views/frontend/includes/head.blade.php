<meta charset="utf-8" />
<title>{{ $seo_title }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
@foreach($seo_metas as $meta)
<meta name="{{ $meta->name }}" lang="{{ strtoupper(\Illuminate\Support\Facades\App::getLocale()) }}" content="{{ $meta->content }}" />
@endforeach
<meta name="author" content="Marco Antonio Zeballos Sanjines <marcozs84@gmail.com>" />
<meta name="developer" content="Marco Antonio Zeballos Sanjines <marcozs84@gmail.com>" />
<meta name="csrf-token" content="{{ csrf_token() }}">

{{-- Ref.: https://www.favicon-generator.org/ --}}
<link rel="apple-touch-icon" sizes="57x57" href="/assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="/assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<link rel="shortcut icon" href="/assets/img/favicon/favicon.ico" type="image/x-icon">
<link rel="icon" href="/assets/img/favicon/favicon.ico" type="image/x-icon">


@stack('metas')

<!-- ================== BEGIN BASE CSS STYLE ================== -->
{{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />--}}
<link href="/assets/plugins/e-commerce/bootstrap3/css/bootstrap.min.css" rel="stylesheet" />
{{--<link href="/assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" />--}}
{{--<link href="/assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />--}}
<link href="/assets/plugins/e-commerce/animate/animate.min.css" rel="stylesheet" />
<link href="/assets/css/e-commerce/style.min.css" rel="stylesheet" />
<link href="/assets/css/e-commerce/style-responsive.css" rel="stylesheet" />
<link href="/assets/css/e-commerce/theme/blue.css" id="theme" rel="stylesheet" />

{{--<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" />--}}
{{--<link href="/assets/plugins/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" />--}}

<link href="/assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet" />

<link href="/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
<!-- ================== END BASE CSS STYLE ================== -->

<link href="/assets/css/e-commerce/modalHandler.css?v3.1" rel="stylesheet" />
<link href="/assets/fa/css/all.min.css" rel="stylesheet" />

<!-- ================== BEGIN BASE JS ================== -->
<script src="/assets/plugins/pace/pace.min.js"></script>
<!-- ================== END BASE JS ================== -->


@stack('css')