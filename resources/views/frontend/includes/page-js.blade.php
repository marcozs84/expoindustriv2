<!-- ================== BEGIN BASE JS ================== -->
<script src="/assets/plugins/e-commerce/jquery/jquery-3.2.1.min.js"></script>
{{--<script src="/assets/plugins/e-commerce/jquery/jquery-migrate-3.0.0.js"></script>--}}
<script src="/assets/plugins/e-commerce/bootstrap3/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="/assets/crossbrowserjs/html5shiv.js"></script>
<script src="/assets/crossbrowserjs/respond.min.js"></script>
<script src="/assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="/assets/plugins/e-commerce/js-cookie/js.cookie.js"></script>
<script src="/assets/js/e-commerce/apps.min.js"></script>
<!-- ================== END BASE JS ================== -->

<script src="/assets/js/theme/default.min.js"></script>

<script src="/assets/js/e-commerce/modalHandler.js?r={{ rand(1,111) }}"></script>
<script src="/assets/js/e-commerce/layout.js?r={{ rand(1,111) }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="/assets/plugins/select2/dist/js/select2.js"></script>

<script>
	$(document).ready(function() {
		App.init();
	});
</script>

@stack('scripts')