@if($email_pendiente != '')
<div class="section-container-m m-b-0 p-b-0 p-t-20">
	<div class="container m-b-0 p-b-0">
		<div class="alert alert-danger m-b-0 ">
			<span class="close" data-dismiss="alert">×</span>
			<h4>@lang('messages.top_message_unconfirmed_title')</h4>
			<p>@lang('messages.top_message_unconfirmed_msg', ['email' => $email_pendiente])</p>
		</div>
	</div>
</div>
@endif