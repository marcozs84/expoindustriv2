<div class="section-terms m-0 p-0 ">

	<h5 style="margin-top:0px;">Dina personuppgifter</h5>
	För att vi ska kunna ge dig tillgång till våra tjänster, till exempel ge dig möjlighet att annonsera eller köpa på <b>ExpoIndustri.com</b>  behöver vi samla in och behandla vissa personuppgifter om dig. I ExpoIndustris integritetspolicy finns information om vilken information vi samlar in, hur vi använder dina personuppgifter.<br><br>

	<h5>Reklam på <b>ExpoIndustri.com</b> </h5>
	Förordningen om reklam på <b>ExpoIndustri.com</b>  innehåller regler för att utforma och kategorisera annonser. För att annonsera på <b>ExpoIndustri.com</b>  måste du följa reklamreglerna och ha ett registrerat företag.<br><br>
	<b>ExpoIndustri Sweden AB</b> genom <b>ExpoIndustri.com</b>  förbehåller sig rätten att blockera eller inaktivera en användare som använder och / eller manipulerar tjänsten felaktigt.<br><br>

	<h5>Användarskapat innehåll</h5>
	Innehållet som skapats av användaren hänvisar till allt innehåll som en användare laddar i <b>ExpoIndustri.com</b> , till exempel bilder och reklamtexter.<br><br>
	Du garanterar att du äger nödvändiga rättigheter för innehållet du skapat, oavsett om det ägs eller tillhandahålls av tredje part. Det innebär att du garanterar att innehållet som skapats av användaren inte innehåller immateriella rättigheter, bilder, logotyper eller annat material som inte är behörigt att använda.<br><br>
	Du garanterar att enskilda personer eller företag som kan identifieras i innehåll som skapats av användaren, till exempel en bild eller helt enkelt genom namn, de är medvetna om hur materialet kommer att användas och att de har godkänt att medverka i och att <b>ExpoIndustri Sweden AB</b> även kan komma att använda det användarskapat innehål i marknadsföringssyfte.<br><br>
	Genom att lägga ut användarskapat innehåll på <b>ExpoIndustri.com</b> , till exempel genom att publicera en annons, ger du <b>ExpoIndustri Sweden AB</b> en obegränsad rätt att fritt förfoga över det. Genom till exempel bearbeta, anpassa, lagra, översätta eller kopiera innehållet och göra det tillgängligt för allmänheten oberoende av vilken kanal eller hjälpmedel som används, samt vidareupplåta dessa rättigheter till eventuella samarbetspartner.<br><br>
	<b>ExpoIndustri.com</b>  kan också använda innehållet som skapats av användaren för marknadsföringsändamål. <b>ExpoIndustri Sweden AB</b>s rättigheter kvarstår även att en annons har tagits bort. Du avvisar härmed anspråk på ersättning till <b>ExpoIndustri Sweden AB</b> för användning av "användarskapat innehåll".<br><br>


	<h5>Skadeersättning</h5>

	Som användare accepterar du att befria <b>ExpoIndustri Sweden AB</b> från ansvar om en tredje part hävdar kompensation för innehållet som skapats av användaren eller för att ha agerat annat i strid med dessa användarvillkor eller i strid med lagen. tillämplig eller rätt till tredje part.<br><br>


	<h5>Ansvar</h5>

	<b>ExpoIndustri Sweden AB</b> garanterar inte kontinuerlig, oavbruten eller säker tillgång till Tjänsterna. Driften av <b>ExpoIndustri.com</b>  kan komma att störas av ett antal faktorer utanför ExpoIndustris kontroll och <b>ExpoIndustri Sweden AB</b> har inga garantier gällande <b>ExpoIndustri.com</b>  funktion eller tillgänglighet. <b>ExpoIndustri Sweden AB</b> kan inte göras ansvarigt för skador som direkt eller indirekt orsakas av användning av <b>ExpoIndustri.com</b>  och dess innehåll. Vid tekniska fel ersätts högst annonskostnaden.<br><br>

	<b>ExpoIndustri.com</b>  är en annonsplats som låter användare annonsera, sälja och köpa maskiner i olika kategorier. <b>ExpoIndustri Sweden AB</b> medverkar i transaktionen mellan en köpare och en säljare och även ansvarar för leverans till slutdestinationen (läs gärna vad det gäller ang. transaktioner och slutdestination här nedan). Den leveranstiden som anges för maskinen till slutdestination är en uppskattning och <b>ExpoIndustri Sweden AB</b> tar inte ansvar för någon typ av förseningar i allmänhet så som , blockeringar, systemfel, dåligt väder eller annat som kan påverka leveransen och kommer inte ge någon form av ersättning. Du ska inte anta att ett erbjudande av ett maskin/utrustning är i godkännande skick så som dom beskriver eller visar på annonsen endast för att det förekommer på <b>ExpoIndustri.com</b> . <b>ExpoIndustri Sweden AB</b> ansvarar inte för den utannonserade maskinen/utrustning. Därför <b>ExpoIndustri.com</b>  erbjuder tjänsten att kontrollbesikta maskinen/utrustning innan köp. (läs gärna vad som gäller för kontrollbesiktning här nedan) <b>ExpoIndustri Sweden AB</b> ansvarar inte för skada som beror på lastning eller leverans till slutdestination, om inget annat anges.<br><br>

	<h5>Valuta</h5>

	<b>ExpoIndustri Sweden AB</b> har rätt att ändra / växla valutan som anget i annonsen till dollarvalutan. <b>ExpoIndustri Sweden AB</b> kommer inte ge någon ersättning för valutakursförändringen. Alla Köp som sker i <b>ExpoIndustri.com</b>  betalas i den valutan som annonsörer angav.<br><br>

	<h5>Transaktion</h5>

	<b>ExpoIndustri Sweden AB</b> medverkar i transaktioner mellan en köpare och en säljare, detta för att underlätta köp och sälj processen mellan Europa och Sydamerika.<br><br>
	<b>ExpoIndustri Sweden AB</b> kommer ALDRIG att begära upphämtning av maskinen innan dessa är full betalt, därför har <b>ExpoIndustri Sweden AB</b> ett 72 timmars förbindelse (läs gärna här nedan).<br><br>
	<b>ExpoIndustri Sweden AB</b> jobbar med välkända banker för att garantera och säkerställa transaktioner.<br><br>
	<b>ExpoIndustri Sweden AB</b> kommer ALDRIG att begära köparen att betala i nån bank eller namn som inte är Godkänd av <b>ExpoIndustri Sweden AB</b>  (har du en faktura som du vill betala, men vill säkerställa bank och namn, Kontakta oss
	<a href="{{ route("public.{$glc}.contact") }}" target="_blank">HÄR</a> )
	)<br><br>


	<h5>72 timmar förbindelse</h5>

	Varje annonsör förbinder sig att reservera maskinen/utrustning i 72 timmar från det att en av våra säljare ta kontakt med annonsören om ett eventuellt köp av maskinen/utrustning. Denna reservation är endast avsett för köparen ska hinna betala <b>ExpoIndustri Sweden AB</b> och göra transaktionen mellan Sydamerika och Europa, så snart som vi ser inbetalningen så deponerar vi beloppet till annonsörens konto för maskinen/utrustning inom dom 72 timmar, om ni inte ser betalningen efter det angivna timmar så avbryts förbindelsen. Om du bryter mot regeln genom att du har sålt/ ångrat inom dessa 72 timmar, så debiterar vi annonsören en faktura på administrativa omkostnader. Beloppet på det administrativa kostnader motsvarrar  3% på produktens maskinens köppris.<br><br>

	<h5>Inbetalning</h5>

	När köparen skickat en förfrågan till <b>ExpoIndustri Sweden AB</b> genom <b>ExpoIndustri.com</b> , så veriferar <b>ExpoIndustri Sweden AB</b> maskinens/utrustningen existens och därefter skicka vi en pre-faktura. Från och med vi skickar kommer vi ta kontakt med köparen per telefon /e-mail och då har köparen 24 timmar på att betala pre-fakturan. Om betalningen inte kommit in inom de angivna timmarna då kan inte <b>ExpoIndustri Sweden AB</b>  säkerställa att maskinen/utrustningen finns kvar eller är tillgänglig. En återbetalning på insättningen kommer att göras om inte köpet genomförs, Inbetalningen kommer att debiteras till det konto ni angav. Har ni inte anget konto så kommer en av våra säljare att ta kontakt med köparen för en återbetalning. En avgift för administrativa omkostnader kommer man att debitera. Hela beloppet ska det betalas inom dom 24 timmarna för att <b>ExpoIndustri Sweden AB</b> ska genomföra förvärvet av maskinen/utrustningen. Återbetalningen eller inbetalningen sker endast genom banköverföring eller kort. <b>ExpoIndustri Sweden AB</b> tar inte emot några kontanter eller något annat form av värde för nån slags handpenning. <b>ExpoIndustri Sweden AB</b> godtar inte någon form av avbetalningar för maskinen/utrustningen ni önskar om inget annat anges, för att säkra köp ska debiteras hela beloppet som anges i pre-fakturan (läs mer om pre-fakturan här nedan)<br><br>

	<h5>Pre-fakturan</h5>
	Vår pre-faktura är en dokument som specificerar den maskin/utrustning köparen tänker köpa, hur mycket du/ni måste betala, vilken typ av tjänst du/ni valde och vilken destination. Det anger också betalningsmetod och sista betalningstid du har att betala. Dena pre-faktura är unikt för varje maskin/utrustning och används endast av <b>ExpoIndustri Sweden AB</b>. Otillbörlig användning av pre-faktura kommer att sanktioneras och anmälas till enheterna i det landet som motsvarar.<br><br>

	<h5>Faktura</h5>
	När köparen har betalat in och vi ser betalningen då kommer vi att skicka ytterliggare en faktura, för att kunna hämta maskinen/utrustningen från vårt lager måste du visa fakturan, Denna faktura kan inte användas för att göra något typ av intullning betala tullavgifter, i respektive land (ta kontakt med din närmaste tullbyrå för mer information, Titta gärna våra tullbyråer) Detta är endast en faktura mellan köpare och <b>ExpoIndustri Sweden AB</b> som bekräftar betalningen för maskinen. Har ni valt ett annat destination än Iquique, så kommer ni att få ytterliggare Faktura (Exportfaktura )<br><br>
	Om du av någon anledning har tappat bort fakturan för den maskin/utrustning som du köpte, kontakta en av våra filialer, vi kommer att be om ditt id-handling för att bevisa att du är ägaren, skickar du någon annan så måste du skicka ditt id-handling för att kunna bekräfta att den personen har rätt att få fakturan.<br><br>



	<h5>Export faktura</h5>
	Exportfakturan är det dokument som krävs för att utföra exporten och lagligt avlägsna maskinen från landet. <b>ExpoIndustri Sweden AB</b> samarbetar med Bengtmachines E.I.R.L i Iquique Chile belägen i Zofri – Iquique. Vill ni tulla in till resten av Chile, Bolivia eller annat land kommer Bengtmachines E.I.R.L att ange er ett exportfaktura och tullagenten använder den för att föra in maskinen/utrustningen i sitt system. Varje faktura måste innehålla: Kundens / importörens bakgrund, säljarens / exportörens bakgrund, beskrivning av maskinen och värdet av produkten i dollar.(ta gärna kontakt med en tullbyrå i ditt land för mer information) Har du/ni köpt maskin som stannar i Iquique eller Arica kommer ni att få ett SRF faktura från Bengtmachines E.I.R.L, (vill du veta mer ang export faktura eller SRF faktura ta gärna kontakt med vår säljare i Iquique – Chile )<br><br>
	Garantier<br><br>

	<h5>Garantier</h5>
	<b>ExpoIndustri Sweden AB</b> erbjuder inte någon form av garanti, om inte annat avtalats skriftligen<br><br>

	<h5>Slutdestination</h5>
	Köparen bekräftar och accepterar slutpriset till slutdestinationen. Det slutliga destinationen kan endast vara till tullen i respektive land som anges av köparen. Slutpriset är endast för inköp och leverans till det slutdestination som köparen angav. Priset ingår inte någon form av tullförfarande eller någon annan typ av extra tjänster för maskinen/utrustningen. Köparen accepterar villkoren och slutbestämmelsen. När köparen har betalat Pre-fakturan kan Köparen begära en ändrad  destination. <b>ExpoIndustri Sweden AB</b> med vår samarbets partner kommer att utvärdera det men det kommer inte att finnas några utbetalningar genom att ändra destination. <b>ExpoIndustri Sweden AB</b> kan begära en deponering för att göra ändringen av destinationen.<br><br>

	<h5>Hämtning av produkten i slutdestination</h5>
	Om det inte finns någon köprepresentant för att ta emot maskinen/utrustningen inom två timmar efter leveransbilens ankomst. (Detta gäller inte i Iquique som slutdestination) vid den angivna destinationen, de extra kostnaderna på grund av förseningen betalas av köparen och betalas direkt till transportbolaget eller andra före avlastning av maskinen/utrustningen. Om köparen väljer Iquique som slutdestination, har köparen 10 arbetsdagar på sig att hämta maskinen/utrustningen, Har köparen inte hämtat maskinen/utrustningen inom det angivna dagar kommer att debiteras en avgift på 30 usd/dagen. <b>ExpoIndustri Sweden AB</b> kommer alltid att kontakta köparen genom telefon / e-mail för att informera om ankomst av maskinen/utrustningen oavsett vilket slutdestination ni angav. <b>ExpoIndustri Sweden AB</b> eller Bengtmachines E.I.R.L ansvarar inte på något sätt om köparens telefon är avstängd eller om ni inte är tillgängliga. <b>ExpoIndustri Sweden AB</b> kommer alltid att meddela i förväg genom telefon eller e-mail.<br><br>
	Om <b>ExpoIndustri Sweden AB</b> och vår samarbetsparnet Bengtmachines E.I.R.L inte kan leverera maskinen/utrustningen till köparen (till exempel på grund av omöjligheten att kontakta köparen eller en representant för leverans och lossning av maskinen) kan <b>ExpoIndustri Sweden AB</b> genom Bengtmachines E.I.R.L ordna lagringen av maskinen/utrustningen. Hyran och administrativa omkostnader  kommer att debiteras till Köparen. Köparen har skyldighet att betala hyran och administrativa omkostnader innan maskinen/utrustningen kan hämtas.<br><br>
	Om köparen inte kontaktar någon av våra säljare inom 60 dagar från dagen den lagras, kan <b>ExpoIndustri Sweden AB</b> tolka att maskinen/utrustningen har övergivits och utan föremål för rättsliga åtgärder får <b>ExpoIndustri Sweden AB</b> begära att Bengtmachines E.I.R.L lägger ut den  till försäljning. Vilket marknadsvärde som <b>ExpoIndustri Sweden AB</b> anser är lämpligt.<br><br>

	<h5>Ångerrätt</h5>

	När du köper tjänster via Internet har du som regel 14 dagars ångerrätt. För att annonsera på <b>ExpoIndustri.com</b>  måste du dock frånsäga dig din ångerrätt eftersom ExpoIndustris tjänst för annonsering innebär att du, genom att lägga in en annons, begär att tjänsten påbörjas omedelbart efter att du har betalat för den. Det innebär att du accepterar att ångerrätten upphör när tjänsten har fullgjorts. ExpoIndustris tjänst anses fullgjord så snart din annons har publicerats.<br><br>
	Det gäller ingen ångerrätt för köparen av maskinen/utrustningen om inget annat anges.<br><br>


	<h5>Ändringar i Allmänna villkoren</h5>

	<b>ExpoIndustri Sweden AB</b>  kan komma att ändra dessa Allmänna villkor i nån framtid. I den mån vi gör väsentliga förändringar som kräver ditt samtycke kommer vi dock att inhämta ditt samtycke innan förändringen träder i kraft.<br><br>

	<h5>Ändringar i Allmänna villkoren</h5>

	<b>ExpoIndustri Sweden AB</b> gör inte anspråk på att nedan angiven information, med avseende på vad som kan anses vara olagligt och således inte får förekomma på <b>ExpoIndustri.com</b> . Du ansvarar själv som användare för att den information som du lägger in i annonser inte strider mot gällande lagar och regler. Du som användare är personligen ansvarig för din annons.<br><br>
	Annonsörer åtar sig att följa konsumentskyddande lagar (bland annat konsumentköplagen, konsumenttjänstlagen och prisinformationslagen) och Allmänna reklamationsnämndens rekommendationer.<br><br>

	<h5>Granskning och kontroll av annonser och användare:</h5>

	<b>ExpoIndustri Sweden AB</b> genom <b>ExpoIndustri.com</b>  förbehåller sig rätten att granska samtliga annonser och att neka eller avlägsna en annons på grund av att den bryter mot de Allmänna villkoren, tredje parts upphovsrätt, annan rättslig reglering eller mot ExpoIndustris principer.<br><br>
	<b>ExpoIndustri Sweden AB</b> genom <b>ExpoIndustri.com</b>  har rätten att ta bort/ändra i annonstext om texten bryter mot annonseringsregler, och annonsen efter ändringen kan publiceras.<br><br>
	Slumpvisa kontroller genomförs fortlöpande, till exempel för verifiering av en användares identitet. Den som väljer att inte medverka till en sådan kontroll riskerar att stängas av från användning av <b>ExpoIndustri Sweden AB</b>.<br><br>


	<h5>Språk:</h5>
	Annonser på svenska, engelska och spanska tillåts, <b>ExpoIndustri Sweden AB</b> kommer översätta alla annonser till spanska så exakt som möjligt för bästa annonsering, men <b>ExpoIndustri</b> ansvarar inte för eventuella fel översättningar eller felstavningar och kommer inte ge någon form av ersättning. <b>ExpoIndustri Sweden AB</b> kan ändra / ta bort några ord för att förbättra översättningen. <b>ExpoIndustri Sweden AB</b> tar inget ansvar om annonsörens maskin/utrustning  inte säljs eller något annat typ på grund av översättningen.<br><br>

	<h5>Övriga information för annonsering:</h5>


	Information som du anger ska användas till att beskriva den specifika maskinen/utrustningen som erbjuds i annonsen. Övrig information, såsom beskrivning av en verksamhet eller ett generellt utbud, tillåts ej i annonsen,(Ta gärna kontakt med en av våra säljare angående vår webbutik tryck
	<a href="{{ route("public.{$glc}.servicios", ['istore']) }}" target="_blank">HÄR</a>
	)<br><br>

	<h5>Beskrivning:</h5>

	Det är inte tillåtet att länka till andra sidor. Det är inte tillåtet att ange en adress, telefonnummer eller e-post som kan publiceras i annonsen. Alla dessa uppgifter fylls i utrymmet från <b>ExpoIndustri.com</b>  och är endast kända för <b>ExpoIndustri Sweden AB</b>s personal.<br><br>


	<h5>Orealistiska erbjudanden:</h5>
	Det är inte tillåtet att annonsera orealistiska erbjudanden. <b>ExpoIndustri Sweden AB</b> förbehåller sig rätten att vägra eller avbryta offentliggörandet av annonser som enligt utvärderingen av <b>ExpoIndustri Sweden AB</b> inte är realistiska.<br><br>


	<h5>Kategorisering:</h5>
	Annonsen ska läggas i den kategori som bäst beskriver annonsörens maskin/utrustning. Det är inte tillåtet att blanda flera maskiner i samma annons när maskinen/utrustningen inte hör till samma kategori.<br><br>
	Endast en produkt per annons:<br><br>
	Det är inte tillåtet att lägga in mer än en maskin/utrustning per annons.<br><br>

	<h5>Kränkande innehåll:</h5>
	Annonser som kan verka kränkande för folkgrupper och/eller enskilda individer godkänns inte. Bilder och filmer av kränkande karaktär tillåts inte.<br><br>


	<h5>Tredjepartsinformation:</h5>
	När du lägger upp din annons kan visa tilläggsinformation kopplad till din annons. Sådan tilläggsinformation är baserad på den information som du som annonsör lägger in, T.ex. fordonsuppgifter kan visas upp med hjälp  av det registreringsnummer som du har angett för fordonet som annonseras. Dessa tilläggsuppgifter hämtas från externa system som tillhandahålls av tredje part och <b>ExpoIndustri</b>  Sweden AB tar inget ansvar för eventuella felaktigheter. Du måste själv som annonsör kontrollera att dessa uppgifter är korrekta.<br><br>

	<h5>Bilder:</h5>
	Bilder ska vara relevanta för maskinen/utrustningen som annonsören annonserar och får inte innehålla övrig text, företagsnamn, logotyper, URL:er eller andra grafiska tillägg som kan tolkas som marknadsföring. Det är inte tillåtet att ladda upp bilder som inte tydligt kan visa produkten du vill annonsera om.<br><br>
	Bilden måste vara den maskin/utrustning som faktiskt säljs, och inte en katalogbild eller en annan bild av en annan likadan maskin/utrustning. Detta då är viktiga för köparen när de bildar sig en uppfattning om maskinens/utrustningens skick och eventuellt köp. Det är inte tillåtet att ta bilder från andra annonser utan annonsörens medgivande. Dessa kan vara skyddade under upphovsrättslagen. Avseende bilder och annat innehåll i annonser, se också ovan i dessa Allmänna villkor under punkten Användarskapat innehåll.<br><br>

	<h5>Logotyper:</h5>
	Det är inte tillåtet att använda en annan persons / företags logotyp i en annons, utöver logotypen som visas på den utrustning som annonseras. Bolagets logotyper kan inte användas som bilder.<br><br>
	Företag som har en webbutik kan ladda upp, modifiera, deras logotyp eller bild (vill ni veta mer om webbutik tryck
	<a href="{{ route("public.{$glc}.servicios", ['istore']) }}" target="_blank">HÄR</a>
	)<br><br>

	<h5>Kontrollbesiktning</h5>
	ExpoIdustri Sweden AB erbjuder även tjänsten att inspektera den utrustning / maskin som intresserar dig. Kostnaden för kontrollbesiktning varierar beroende på många faktorer och kommer att betalas i förskott, men du som köpare har möjlighet att begära eller avstå från denna tjänst. Kontrollbesiktning innebär att en av våra specialister verifierar och kontrollerar utrustningen / maskinen. I vår inspektion kontrollerar vi utrustningens funktionalitet, skador, slitage, färg, tillbehör, dimensioner, oljor, smörjning, tillverkningsår, kraft, körsträcka, arbetstid, motor, låda, läckage av alla slag.<br><br>
	<b>ExpoIndustri Sweden AB</b> använder en extern inspektör när det gäller frontlastare, grävmaskiner, kompaktmaskiner, kranar, skördar, traktorer, lastbilar> 3,5 ton, släpvagnar och annat. Denna information har upprättats på bästa möjliga sätt, men är inte bindande i detalj. <b>ExpoIndustri Sweden AB</b> utför en kontroll i god tro och litar på att informationen som genereras av utrustningen / maskinen är korrekt, såsom: timmar, körsträcka. Om köparen begärt och gjort motsvarande betalning för kontrollbesiktning, skickar vi till köparens email adress den detaljerade informationen på utrustningen / maskinen mellan 7 arbetsdagar räknat från när betalningen genomfördes. Betalning för kontrollbesiktningen  kan genomföras enbartmed kort som VISA, MASTERCARD och betalas i förskott.<br><br>


</div>