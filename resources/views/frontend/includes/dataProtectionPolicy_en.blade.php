<div class="section-terms m-0 p-0 ">

	<h5 style="margin-top:0px;">Política de privacidad</h5>
	En Expoindustri cuidamos a nuestros usuarios, lo que significa que siempre nos aseguramos de utilizar su información cuidando su privacidad. Es importante para nosotros definir qué información recopilamos sobre usted y cómo la usamos. Siempre puede ponerse en contacto con nosotros si tiene preguntas sobre cómo procesamos sus datos personales. La información de contacto es la última en este texto.<br><br>


	<h5>¿Qué es la información personal y qué es el procesamiento de datos personales?</h5>

	Los datos personales son toda la información sobre una persona natural viva que puede estar directa o indirectamente conectada a esa persona. No se trata solo del nombre y la dirección, sino también de las imágenes y las direcciones de correo electrónico.<br><br>

	<h5>Personuppgiftsansvarig</h5>

	Expoindustri Sweden AB ("expoindustri") es responsable del procesamiento de sus datos personales, a menos que se indique lo contrario en esta información. (Org. No. 559174-5194).<br><br>


	<h5>¿Qué información personal recopilamos sobre usted y por qué?</h5>

	Recopilamos información sobre usted cuando registra una cuenta de usuario y cuando utiliza nuestros servicios. Luego proporciona información sobre usted y su empresa, como nombre, número de teléfono, dirección de correo electrónico, número de organización y dirección, pero también contenido en mensajes y anuncios. Los datos personales en esta categoría son aquellos que nos envía de forma activa al completar formularios y al enviarnos mensajes.
	<br><br>
	También procesamos información sobre usted que se recopila automáticamente cuando utiliza nuestros servicios. Por ejemplo, registramos cuál de nuestros servicios usa, qué páginas visita y cómo se comporta en nuestro sitio, qué tecnología utiliza, ya sea que use una computadora, tableta o dispositivo móvil, y qué navegador usa cuando navega en la industria de exposiciones, qué IP Dirección que utiliza e información similar. También recopilamos información sobre los anuncios en los que hace clic y sobre los anuncios que le mostramos. La información recopilada automáticamente cuando utiliza los servicios.
	<br><br>

	<h5>Sus transacciones</h5>

	Si compra un servicio en Expoindustri.com obtenemos información de pago, como el número de su tarjeta de crédito o débito y otros datos de la tarjeta. Usamos una franja de terceros (lea cómo manejan su tarjeta de crédito o débito en www.stripe.com) Expoindustri Sweden AB no almacena ni maneja ninguna información de pago.
	<br><br>

	<h5>¿A quién se pueden revelar los datos personales?</h5>
	Compartimos información sobre usted con otros, como proveedores que procesan datos personales en nuestro nombre, autoridades y socios. Aquí puede leer más sobre a quién proporcionamos información personal.
	<br><br>
	Proveedores que procesan datos personales en nuestro nombre.
	<br><br>
	A veces necesitamos contratar proveedores para entregarle nuestros servicios. Dichos proveedores pueden, por ejemplo, suministrar sistemas para enviar correos electrónicos y notificaciones automáticas, servicios en la nube para almacenar datos, herramientas de análisis o sistemas para publicar y mostrar anuncios publicitarios. También pueden ser empresas de servicio al cliente, consultores de TI o empresas de logística y transporte que se contratan para entregarle productos. Seguimos siendo responsables del procesamiento de sus datos personales que realiza el subcontratista. Los proveedores no pueden utilizar los datos para fines distintos de los que especificamos. Compartimos información personal con proveedores porque es necesario para cumplir con nuestro acuerdo con usted.
	<br><br>

	<h5>¿Cuánto tiempo guardamos sus datos personales?</h5>
	Procesaremos sus datos personales durante el tiempo que sea necesario con respecto al propósito del tratamiento. Esto significa que las diferentes tareas se guardarán durante mucho tiempo. Necesitamos guardar alguna información durante mucho tiempo para cumplir con nuestras obligaciones legales.
	<br><br>

	<ul>
		<li>Información de la cuenta: la información que nos proporcionó que está vinculada a su cuenta, como su nombre de usuario, dirección de correo electrónico, código postal, etc., e información sobre cuándo registró su cuenta. Esta información se almacena siempre y cuando sea un cliente activo con nosotros y durante los próximos tres años.</li>
		<li>Mensajes que enviaste al servicio de atención al cliente de Expoindustri. Esta información se almacena siempre y cuando sea un cliente activo con nosotros y durante los próximos tres años.</li>
		<li>La información que ha proporcionado y a la que tiene acceso en su perfil, como el logotipo, la información de la empresa y los anuncios guardados. Esta información se almacena siempre y cuando sea un cliente activo con nosotros y durante los próximos tres años.</li>
		<li>Anuncios e información recopilados en relación con su publicidad. Estos son datos que nos ha proporcionado cuando utiliza nuestros servicios, pero es posible que no pueda verlos en su perfil. Esta información consiste en anuncios que ha publicado en Expoindustri.com e información sobre su publicidad. Esta información se almacena durante un máximo de tres años desde el momento en que anunció o nos proporcionó la información.</li>
		<li>Datos que registramos al navegar por Expoindustri.com. Puede ser información sobre qué anuncios está viendo, qué botones presiona y en qué páginas se encuentra. Estos datos se almacenan durante un máximo de 18 meses desde el momento en que se recopilaron.</li>
		<li>Información sobre su posición geográfica. Estos se basan en su dirección IP o en la información de ubicación de su dispositivo móvil. Detalles de donde se encuentra al utilizar Expoindustri.com guardado por un máximo de 30 días y renovado tan pronto como una nueva tarea entra, entonces la tarea anterior se elimina.</li>
	</ul>

	<h5>¿Cuáles son sus derechos registrados?</h5>
	Como está registrado, tiene una serie de derechos en virtud de la legislación vigente. Tiene derecho a obtener un extracto que muestre qué datos personales hemos registrado sobre usted. Puede solicitar la corrección de los datos erróneos y su eliminación.
	<br><br>

	<h5>Contáctenos para preguntas sobre cómo procesamos los datos personales.</h5>

	Si tiene alguna pregunta sobre cómo procesamos la información personal, contáctenos en info@expoindustri.com.
	<br><br>
	Oficinas<br>
	Fordonsvägen 17<br>
	55302 Jönköping, Suecia

</div>