<div class="section-terms m-0 p-0 ">

	<h5 style="margin-top:0px;">Su información personal</h5>
	Para que podamos brindarle acceso a nuestros servicios, así como la oportunidad de anunciar o comprar en <b>ExpoIndustri.com</b>, necesitamos recopilar y procesar cierta información personal sobre usted. La política de privacidad de <b>ExpoIndustri</b> proporciona información sobre qué información recopilamos y cómo la utilizamos.<br><br>

	<h5>Publicidad en <b>ExpoIndustri.com</b> </h5>
	El reglamento sobre publicidad en <b>ExpoIndustri.com</b>  contiene reglas para diseñar y categorizar anuncios. Para hacer publicidad en <b>ExpoIndustri.com</b>  debe cumplir con las normas de publicidad y tener una empresa registrada.<br><br>
	<b>ExpoIndustri Sweden AB</b> por medio de <b>ExpoIndustri.com</b>  se reserva el derecho de bloquear o deshabilitar a un usuario que utiliza y/o manipula el servicio de forma incorrecta.<br><br>

	<h5>Contenido creado por el usuario</h5>
	El contenido creado por el usuario se refiere a todo el contenido que un usuario carga en <b>ExpoIndustri.com</b>, como imágenes y textos publicitarios.<br><br>
	Usted garantiza que posee los derechos necesarios para el contenido que ha creado, sea propio u otorgado por terceros. Esto significa que usted garantiza que el “contenido creado por el usuario” no contiene derechos de propiedad intelectual, imágenes, logotipos u otro material que no esté autorizado a utilizar.<br><br>
	Usted garantiza que las personas o empresas que pueden identificarse en el contenido creado por el usuario, por ejemplo, una imagen o simplemente por su nombre, son conscientes de cómo se utilizará el material y que han aceptado participar en <b>ExpoIndustri Sweden AB</b> mediante <b>ExpoIndustri.com</b>  así también puede usar el contenido creado por el usuario con fines de marketing.<br><br>
	Al publicar contenido creado por el usuario en <b>ExpoIndustri.com</b>, por ejemplo, al publicar un anuncio publicitario, le otorga a <b>ExpoIndustri Sweden AB</b> un derecho ilimitado a disponer de él libremente. Por ejemplo, del procesando, personalizando, almacenando, traduciendo o copiando el contenido y poniéndolo a disposición publico independientemente del canal o medios utilizados, así como para enviar ese contenido a cualquier colaborador.<br><br>
	<b>ExpoIndustri.com</b>  también puede utilizar el contenido creado por el usuario para fines de marketing. Los derechos de <b>ExpoIndustri Sweden AB</b> permanecen después de que se haya eliminado un anuncio. Por la presente, usted rechaza todo reclamo de compensación a <b>ExpoIndustri Sweden AB</b> por el uso del “contenido creado por el usuario”. <br><br>

	<h5>Indemnización</h5>
	Usted como usuario, se compromete a eximir de responsabilidad a <b>ExpoIndustri Sweden AB</b> en caso de que un tercero reclame una compensación por el contenido creado por el usuario o por haber actuado de otro modo en violación de estos Términos de servicio o en violación de la ley aplicable o derecho de terceros.<br><br>



	<h5>Responsabilidad</h5>
	<b>ExpoIndustri Sweden AB</b> no garantiza el acceso continuo o interrumpido de los servicios. El funcionamiento de <b>ExpoIndustri.com</b>  puede verse afectado por una serie de factores de fuerza mayor. <b>ExpoIndustri Sweden AB</b> no tiene garantías con respecto a la funcionabilidad o disponibilidad de <b>ExpoIndustri.com</b> . <b>ExpoIndustri Sweden AB</b> no se responsabiliza por los daños causados directa o indirectamente por el uso de <b>ExpoIndustri.com</b>  y sus contenidos. En el caso de errores técnicos, <b>ExpoIndustri Sweden AB</b> puede como máximo llegar a devolver el costo del anuncio publicado.<br><br>
	<b>ExpoIndustri.com</b>  es un espacio publicitario que permite a los usuarios anunciar, vender, comprar equipos/máquinas en diferentes categorías. <b>ExpoIndustri Sweden AB</b> participa en la transacción entre un comprador y un vendedor y también es responsable de la entrega destino final (lea sobre las transacciones y el destino final a continuación). EL tiempo de entrega especificado para un equipo/máquina hasta el destino final, es un tiempo estimado y <b>ExpoIndustri Sweden AB</b> no acepta responsabilidad por ningún tipo de demoras en general, que pueden o no ser de fuerza mayor, como bloqueos, fallas de sistema, mal tiempo, o cualquier otro que afecte la entrega y NO proporcionará ninguna compensación.<br><br>
	<b>ExpoIndustri Sweden AB</b> no garantiza que el equipo/maquinaria publicado en <b>ExpoIndustri.com</b>  esté en el estado que indica el vendedor/dueño del equipo/maquina. <b>ExpoIndustri Sweden AB</b> no es responsable de la maquinaria anunciada, por lo tanto, <b>ExpoIndustri Sweden AB</b> por medio de <b>ExpoIndustri.com</b>  ofrece el servicio para inspección del producto/ maquinaria antes de la compra (lea sobre inspección técnica a continuación) <b>ExpoIndustri Sweden AB</b> no se hace responsable por daños debidos a la carga o manipulación de la maquinaria, a menos que se indique lo contrario solicitando algún tipo de seguro (consúltenos sobre seguro contra daños).<br><br>

	<h5>Moneda</h5>
	<b>ExpoIndustri Sweden AB</b> tiene el derecho de cambiar la moneda de origen a la moneda de dólares americanos en caso de requerirlo. <b>ExpoIndustri Sweden AB</b> no compensará la variación por el tipo de cambio. Todas las compras realizadas en <b>ExpoIndustri.com</b>  se pagarán en la moneda indicada por los anunciantes.

	<h5>Transacción</h5>
	<b>ExpoIndustri Sweden AB</b> participa en transacciones entre un comprador y un vendedor, para facilitar la compra y venta entre Europa y Sudamérica.<br><br>
	<b>ExpoIndustri Sweden AB</b> NUNCA solicitará recoger de ningún tipo de maquinaria antes de que sea totalmente pagada, por lo tanto, <b>ExpoIndustri Sweden AB</b> tiene una regla de las 72 horas de conexión (lea a continuación)<br><br>
	<b>ExpoIndustri Sweden AB</b> trabaja con bancos conocidos para garantizar las transacciones.<br><br>
	<b>ExpoIndustri Sweden AB</b> NUNCA le pedirá al comprador que pague en un banco y a un nombre que no esté aprobado por <b>ExpoIndustri Sweden AB</b> ¿Tiene una factura que desea pagar, pero desea confirmar el nombre y banco? Contáctenos
	<a href="{{ route("public.{$glc}.contact") }}" target="_blank">AQUÍ</a> )
	<br><br>


	<h5>72 horas de conexión</h5>
	Cada anunciante se compromete a reservar el equipo/maquina durante 72 horas, si alguien de nuestro equipo de trabajo se comunica con el vendedor sobre cualquier posible compra de maquinaria, en ese momento se habilitará la “conexión de las 72 horas”. Esta reserva es solo para que el comprador haga el pago total del equipo a <b>ExpoIndustri Sweden AB</b> y así se realice la transacción entre Sudamérica y Europa. Tan pronto como se confirme el pago, se depositará el monto en la cuenta del vendedor del equipo/máquina, dentro de las 72 horas, si no puede ver el pago después de las horas especificadas se interrumpirá la conexión. Si esta regla es violada al vender el equipo o excusarse de la venta antes de las 72 horas, se cobrará una factura por gastos administrativos. El monto de los costos administrativos corresponde al 3 % del precio de la maquinaria.<br><br>

	<h5>Pago</h5>
	Cuando el comprador ha enviado la intención de compra a <b>ExpoIndustri Sweden AB</b> a través de <b>ExpoIndustri.com</b>, <b>ExpoIndustri Sweden AB</b> verifica la existencia de la máquina y luego le envía la pre-factura. Tan pronto enviamos la pre-factura, nos pondremos en contacto con usted por teléfono/ correo electrónico y el comprador tendrá 24 horas para pagar el monto indicado. Si el pago no se ha recibido dentro de las horas especificadas, <b>ExpoIndustri Sweden AB</b> no podrá garantizar que la maquinaria permanezca o esté disponible. Se realizará un reembolso del depósito a menos que se realice la compra. Esté reembolso se hará a la cuenta especificada por el comprador. Si no ha otorgado ninguna cuenta, uno de nuestros vendedores se pondrá en contacto con usted para obtener la información requerida. Se cobrará un cargo por gastos administrativos, en caso de que la transacción no se haya podido realizar por el atraso del comprador.<br><br>
	El pago total de la maquinaria se realizará en un plazo de 24 horas para que <b>ExpoIndustri Sweden AB</b> realice la adquisición del equipo/maquinaria. EL reembolso o pago se realiza solo a través de transferencia bancaria o tarjeta. <b>ExpoIndustri Sweden AB</b> no acepta efectivo ni ninguna otra forma de valor para ningún tipo de pago.<br><br>
	<b>ExpoIndustri Sweden AB</b> no le pedirá ningún tipo de anticipos por la maquinaria que desea obtener, a menos que se haya acordado lo contrario en casos específicos. Para garantizar la compra de la maquinaria tendrá que hacer la cancelación total que se especifica en la pre-factura (lea más sobre las facturas a continuación)<br><br>

	<h5>Pre-factura</h5>
	Nuestra pre-factura es un documento que especifica la máquina que desea comprar, cuánto tiene que pagar, qué tipo de servicio eligió y qué destino. También indica el método de pago y la fecha de vencimiento. Esta pre- factura es exclusiva de cada máquina y solo es utilizada por <b>ExpoIndustri Sweden AB</b>.<br><br>





	<h5>Factura</h5>
	Una vez que haya pagado y confirmemos la transferencia, le enviaremos una factura. Para que usted pueda recoger el equipo "máquina" de nuestro almacén. Allí debe mostrar la factura. Esta factura no se puede utilizar para realizar ningún tipo de pago en los aranceles. (Póngase en contacto con la oficina de aduanas más cercana para obtener más información; consulte nuestras agencias de aduanas). Esta es solo una factura entre el comprador y <b>ExpoIndustri Sweden AB</b> que confirma el pago de la máquina. Si ha elegido un destino que no sea Iquique, recibirá una factura adicional (factura de exportación)<br><br>
	Si por alguna razón ha perdido la factura por el equipo /máquina que compró, comuníquese con uno de nuestros afiliados (vea nuestras oficinas
	<a href="{{ route("public.{$glc}.contact") }}" target="_blank">AQUÍ</a>
	), le pediremos su documento de identificación para demostrar que usted es el propietario, si envía otra persona, esta debe contar con la identificación necesaria para confirmar que la persona tiene derecho a recibir la factura. El uso indebido de la factura será penalizado y notificado a las entidades necesarias en el país que corresponda.<br><br>

	<h5>Factura de exportación</h5>
	La factura de exportación es el documento requerido para exportar legalmente la máquina del país. <b>ExpoIndustri Sweden AB</b> colabora con Bengtmachines E.I.R.L en Iquique Chile ubicada en Zofri - Iquique. Si desea enviar la maquinaria al resto de Chile, Bolivia u otro país, Bengt machines E.I.R.L le facilitará una factura de exportación y el agente de aduanas la utilizará para ingresar la maquinaria a su sistema y así poder pagar los derechos de aduana. Cada factura debe incluir: Antecedentes del Cliente / Importador, Antecedentes del Vendedor / Exportador, Descripción de la Máquina y Valor del Producto en dólares americanos. Si elige Iquique o Arica como su destino final, recibirá una factura SRF de Bengtmachines EIRL (si desea obtener más información sobre los tipos de factura, comuníquese con nuestro vendedor en Iquique – Chile
	<a href="{{ route("public.{$glc}.contact") }}" target="_blank">AQUÍ</a>)<br><br>

	<h5>Garantías</h5>
	<b>ExpoIndustri Sweden AB</b> no ofrece ninguna forma de garantía, a menos que se acuerde lo contrario por escrito.<br><br>

	<h5>Destino final</h5>
	El comprador confirma y acepta el precio final del equipo o maquinaria puesto en el destino elegido por él comprador. El destino final solo se refiere a las aduanas de cada país especificado por el comprador. El precio no incluye ningún tipo de procedimiento aduanero ni ningún otro tipo de servicio adicional para el equipo/máquina, ya puesto en aduana. El comprador acepta los términos y condiciones. Una vez que el comprador ha pagado la Pre-factura, el Comprador puede solicitar un cambio de destino. <b>ExpoIndustri Sweden AB</b> con su socio operacional, hará la evaluación correspondiente para analizar la factibilidad del requerimiento del comprador, pero no habrá reembolso al cambiar el destino. <b>ExpoIndustri Sweden AB</b> puede solicitar un cobro adicional por realizar el cambio de destino.<br><br>

	<h5>Descarga del equipo/máquina en destino final.</h5>
	Si no hay un representante de quien adquirió el equipo/máquina para recibir la maquinaria dentro de las dos horas posteriores a la llegada del vehículo de entrega (esto no se aplica a Iquique como destino final) en el destino especificado, los costos adicionales debidos a la demora son pagados por el comprador y se pagan directamente a la empresa de envío u otros antes de descargar la maquinaria. Si elige Iquique como destino final, el comprador tiene 10 días hábiles para recoger la maquinaria. Si el comprador no ha recogido el equipo/máquina dentro de los días especificados, se le cobrará una tarifa de 30 usd / día. <b>ExpoIndustri Sweden AB</b> siempre se comunicará con el comprador por teléfono / correo electrónico para informarle sobre la llegada del equipo/máquina, sin importar el destino final que especificó. <b>ExpoIndustri Sweden AB</b> o Bengtmachines E.I.R.L no son responsables de ninguna manera si el número de teléfono del comprador está apagado o si no está disponible. <b>ExpoIndustri Sweden AB</b> siempre notificará por adelantado por teléfono o correo electrónico.<br><br>
	Si <b>ExpoIndustri Sweden AB</b> y nuestro socio operativo Bengtmachines EIRL no pueden entregar el equipo/máquina al comprador (por ejemplo, debido a la incapacidad de contactar al comprador o un representante para la entrega y descarga de la máquina) <b>ExpoIndustri Sweden AB</b> puede organizar el almacenamiento del producto a través de Bengtmachines EIRL. El alquiler y los gastos administrativos se cargarán al Comprador. El comprador está obligado a pagar el alquiler y los gastos administrativos antes de poder recuperar la maquinaria.<br><br>
	Si el comprador no se comunica con ninguno de nuestros vendedores dentro de los 60 días posteriores a la fecha de almacenamiento, <b>ExpoIndustri Sweden AB</b> puede interpretar que el "equipo" ha sido abandonado y sin ninguna acción legal, <b>ExpoIndustri Sweden AB</b> puede solicitar que Bengtmachines E.I.R.L ponga a la venta. El valor de mercado que <b>ExpoIndustri Sweden AB</b> considera apropiado.<br><br>

	<h5>Política de devoluciones</h5>
	Cuando compra servicios a través de Internet, generalmente tiene 14 días de devolución. Sin embargo, para hacer publicidad en <b>ExpoIndustri.com</b>, debe renunciar a su derecho de desistimiento porque el servicio de publicidad de <b>ExpoIndustri.com</b>  significa que, al enviar un anuncio, usted solicita que el servicio comience inmediatamente después de haberlo pagado. Esto significa que acepta que el derecho de desistimiento terminará cuando se complete el servicio. El servicio <b>ExpoIndustri Sweden AB</b> se considera completo en cuanto se publica su anuncio.<br><br>
	No hay derecho de desistimiento para el equipo máquina a menos que se indique lo contrario.<br><br>

	<h5>Cambios a los Términos y Condiciones Generales.</h5>
	<b>ExpoIndustri Sweden AB</b> se reserva el derecho de cambiar estos términos y condiciones en cualquier momento. Sin embargo, en la medida en que realicemos cambios significativos que requieran su consentimiento, solicitaremos su consentimiento antes de que el cambio entre en vigencia.<br><br>



	<h5>Reglas publicitarias</h5>
	<b>ExpoIndustri Sweden AB</b> no se responsabiliza de la información colocada por un vendedor si esta pudiera llegar a ser considerada como ilegal, sin embargo, se reserva el derecho de hacer revisiones periódicas y tomar acciones sobre los textos que le parezcan dudosos. Usted es responsable como usuario de la información que publica en los anuncios y asegura que no infringen las leyes y regulaciones aplicables. Usted como usuario es personalmente responsable de su anuncio.<br><br>
	Los anunciantes corporativos se comprometen a cumplir con las leyes de protección al consumidor (incluida la Ley de Compra del Consumidor, la Ley de Servicios al Consumidor y la Ley de Información de Precios) y las recomendaciones de la Junta General de Quejas.<br><br>

	<h5>Revisión y control de anuncios y usuarios:</h5>
	<b>ExpoIndustri Sweden AB</b> a través de <b>ExpoIndustri.com</b>  se reserva el derecho de revisar todos los anuncios y rechazar o eliminar un anuncio porque viola los Términos generales, derechos de autor de terceros, otras regulaciones legales o en contra de los principios de <b>ExpoIndustri Sweden AB</b>.<br><br>
	<b>ExpoIndustri Sweden AB</b> a través de <b>ExpoIndustri.com</b>  tiene el derecho de eliminar / editar el texto del anuncio si el texto infringe las reglas de publicidad, y el anuncio después del cambio puede publicarse.<br><br>
	Las verificaciones aleatorias se realizan de forma continua, por ejemplo, para verificar la identidad de un usuario. Quienes decidan no participar en dicho control no podrán hacer uso de los servicios de <b>ExpoIndustri Sweden AB</b>/<b>ExpoIndustri.com</b><br><br>

	<h5>Idioma:</h5>
	La publicidad en sueco, inglés y español está permitida, <b>ExpoIndustri Sweden AB</b> traducirá todos los anuncios al español con la mayor precisión posible para la mejor publicidad, pero <b>ExpoIndustri Sweden AB</b> no es responsable de ninguna traducción incorrecta o faltas de ortografía y no proporcionará ninguna compensación. <b>ExpoIndustri Sweden AB</b> puede cambiar / borrar algunas palabras para mejorar la traducción sin cambiar la base de la publicación. <b>ExpoIndustri Sweden AB</b> no acepta ninguna responsabilidad si su equipo / máquina no se vende o cualquier otro tipo debido a la traducción.<br><br>

	<h5>Otra información para publicidad:</h5>
	La información que proporcione se utilizará para describir el artículo específico ofrecido en el anuncio. Otra información, como una descripción de una actividad o una oferta general, no está permitida en el anuncio, (comuníquese con uno de nuestros representantes de ventas en relación con nuestra tienda en línea para empresas)<br><br>

	<h5>Descripción:</h5>
	No pueden aparecer nombres de empresas o enlaces (no se aplica a la tienda en línea, consulte sobre tienda en línea
	<a href="{{ route("public.{$glc}.servicios", ['istore']) }}" target="_blank">AQUÍ</a>
	) No se pueden usar caracteres innecesarios en el título. No está permitido enlazar a otras páginas. No está permitido ingresar una dirección, número telefónico o correo electrónico que pueda ser publicado en el anuncio, todos estos datos se llenan en los espacios dispuestos por <b>ExpoIndustri.com</b>  y son sólo serán de conocimiento del personal de <b>ExpoIndustri Sweden AB</b>.<br><br>

	<h5>Ofertas irreales:</h5>
	No está permitido anunciar ofertas poco realistas. <b>ExpoIndustri Sweden AB</b> se reserva el derecho de rechazar o suspender la publicación de anuncios que, según la evaluación de <b>ExpoIndustri Sweden AB</b>, no son realistas.<br><br>

	<h5>Categorización:</h5>
	El anuncio se debe colocar en la categoría que mejor describa su equipo/maquinaria. No está permitido mezclar varios productos en el mismo anuncio cuando el producto no pertenece a la misma categoría. En este caso, los diferentes equipos/máquinas deben dividirse en varios anuncios.<br><br>

	<h5>Solo un producto por anuncio:</h5>
	No está permitido agregar más de un equipo/máquina por anuncio.<br><br>

	<h5>Contenido ofensivo:</h5>
	Los anuncios que pueden ser ofensivos para el público y / o individuos pueden no ser aprobados. No se permiten imágenes y peliculas de carácter infractor.<br><br>

	<h5>Información de terceros:</h5>
	Cuando publica su anuncio, puede ver información adicional asociada con su anuncio. Dicha información adicional se basa en la información que ingresa como usuario / anunciante, por ejemplo, Los datos de transporte como camiones pueden mostrarse usando el número de registro. Esta información adicional se obtiene de sistemas externos de terceros, y <b>ExpoIndustri Sweden AB</b> no se responsabiliza por ningún error. Usted como anunciante debe asegurarse de que esta información sea correcta.<br><br>

	<h5>Fotos:</h5>
	Las imágenes deben ser relevantes para el producto sobre el que se anuncia y no pueden contener otro texto, nombres de empresas, logotipos, URL u otras adiciones gráficas que puedan interpretarse como marketing. No está permitido cargar imágenes que no puedan mostrar claramente el producto que desea anunciar.<br><br>
	La imagen debe ser del equipo/máquina que se está anunciando, no una imagen de catálogo u otra imagen de otro producto similar. Es por que las imágenes del equipo/máquina son importantes para el comprador cuando se hace una idea del estado del producto y de cualquier compra. No está permitido tomar fotografías de otros anuncios sin el consentimiento del anunciante. Estos pueden estar protegidos por la ley de derechos de autor.<br><br>
	Para imágenes y otro contenido en anuncios, vea también más arriba en estos Términos y condiciones en la sección Contenido creado por el usuario.<br><br>

	<h5>Logos:</h5>
	No está permitido utilizar el logotipo de otra persona / empresa en un anuncio, además del logotipo que se muestra en el equipo que se anuncia. Los tipos de logotipo de la empresa no se pueden utilizar como imágenes, sin embargo las empresas que cuentan con una tienda online podrán subir, modificar, su logotipo o imagen (quiere saber más sobre tienda en línea entre
	<a href="{{ route("public.{$glc}.servicios", ['istore']) }}" target="_blank">AQUÍ</a>
	)<br><br>

	<h5>Inspección</h5>
	ExpoIdustri Sweden AB también ofrece el servicio para inspeccionar el equipo/máquina que le interesa. El costo de la inspección de control varía dependiendo de muchos factores y será pagado anticipadamente, pero usted, como comprador, puede solicitar la cotización o abstenerse de este servicio. La verificación de inspección significa que uno de nuestros especialistas verifica y controla el equipo/máquina. En nuestra inspección, verificamos la funcionalidad del equipo, daños, desgaste, pintura, accesorios, dimensiones, aceites, lubricación, año de fabricación, potencia, kilometraje, horas de trabajo, motor, caja, fugas de todo tipo, entre otros.<br><br>
	<b>ExpoIndustri Sweden AB</b> utiliza un inspector externo cuando se trata de cargadores frontales, excavadoras, orugas, rodillos compactadores, grúas, cosechadoras, tractores, camiones> 3.5 toneladas, remolques entre otros. Esta información se ha preparado de la mejor manera posible, pero no es vinculante en detalle. <b>ExpoIndustri Sweden AB</b> realiza una verificación de buena fe y confía en que sea correcta la información que genera el equipo/máquina, como, por ejemplo: horas, kilometraje. Sí el comprador solicito e hizo el pago correspondiente por la inspección le haremos llegar a su correo electrónico la información detallada del equipo/maquina entre los 7 días hábiles contando desde que se ejecutó el pago. El pago por una inspección únicamente está habilitado para tarjetas como VISA, MASTERCARD y se paga por adelantado.<br><br>

</div>