<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title>ExpoIndustri</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">


	<style type="text/css">
		@media only screen and (max-width: 600px) {
			.body .container.content {
				width: 100% !important;
			}
			table[class="body"] .wrapper {
				padding-right: 15px !important;
			}
			.text-right {
				text-align: left !important;
			}
		}
	</style>
</head>

<body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #e0e0e0; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; line-height: 19px; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important;">
<style type="text/css">
	@media only screen and (max-width: 600px) {
		table[class="body"] img {
			width: auto !important;
			height: auto !important;
		}
		table[class="body"] center {
			min-width: 0 !important;
		}
		table[class="body"] .container {
			width: 95% !important;
		}
		table[class="body"] .row {
			width: 100% !important;
			display: block !important;
		}
		table[class="body"] .wrapper {
			display: block !important;
			padding-right: 0 !important;
		}
		table[class="body"] .columns,
		table[class="body"] .column {
			table-layout: fixed !important;
			float: none !important;
			width: 100% !important;
			padding-right: 0px !important;
			padding-left: 0px !important;
			display: block !important;
		}
		table[class="body"] .wrapper.first .columns,
		table[class="body"] .wrapper.first .column {
			display: table !important;
		}
		table[class="body"] table.columns td,
		table[class="body"] table.column td {
			width: 100% !important;
		}
		table[class="body"] .columns td.one,
		table[class="body"] .column td.one {
			width: 8.333333% !important;
		}
		table[class="body"] .columns td.two,
		table[class="body"] .column td.two {
			width: 16.666666% !important;
		}
		table[class="body"] .columns td.three,
		table[class="body"] .column td.three {
			width: 25% !important;
		}
		table[class="body"] .columns td.four,
		table[class="body"] .column td.four {
			width: 33.333333% !important;
		}
		table[class="body"] .columns td.five,
		table[class="body"] .column td.five {
			width: 41.666666% !important;
		}
		table[class="body"] .columns td.six,
		table[class="body"] .column td.six {
			width: 50% !important;
		}
		table[class="body"] .columns td.seven,
		table[class="body"] .column td.seven {
			width: 58.333333% !important;
		}
		table[class="body"] .columns td.eight,
		table[class="body"] .column td.eight {
			width: 66.666666% !important;
		}
		table[class="body"] .columns td.nine,
		table[class="body"] .column td.nine {
			width: 75% !important;
		}
		table[class="body"] .columns td.ten,
		table[class="body"] .column td.ten {
			width: 83.333333% !important;
		}
		table[class="body"] .columns td.eleven,
		table[class="body"] .column td.eleven {
			width: 91.666666% !important;
		}
		table[class="body"] .columns td.twelve,
		table[class="body"] .column td.twelve {
			width: 100% !important;
		}
		table[class="body"] td.offset-by-one,
		table[class="body"] td.offset-by-two,
		table[class="body"] td.offset-by-three,
		table[class="body"] td.offset-by-four,
		table[class="body"] td.offset-by-five,
		table[class="body"] td.offset-by-six,
		table[class="body"] td.offset-by-seven,
		table[class="body"] td.offset-by-eight,
		table[class="body"] td.offset-by-nine,
		table[class="body"] td.offset-by-ten,
		table[class="body"] td.offset-by-eleven {
			padding-left: 0 !important;
		}
		table[class="body"] table.columns td.expander {
			width: 1px !important;
		}
		table[class="body"] .right-text-pad,
		table[class="body"] .text-pad-right {
			padding-left: 10px !important;
		}
		table[class="body"] .left-text-pad,
		table[class="body"] .text-pad-left {
			padding-right: 10px !important;
		}
		table[class="body"] .hide-for-small,
		table[class="body"] .show-for-desktop {
			display: none !important;
		}
		table[class="body"] .show-for-small,
		table[class="body"] .hide-for-desktop {
			display: inherit !important;
		}
	}

	@media only screen and (max-width: 600px) {
		table[class="body"] img {
			width: auto !important;
			height: auto !important;
		}
		table[class="body"] center {
			min-width: 0 !important;
		}
		table[class="body"] .container {
			width: 95% !important;
		}
		table[class="body"] .row {
			width: 100% !important;
			display: block !important;
		}
		table[class="body"] .wrapper {
			display: block !important;
			padding-right: 0 !important;
		}
		table[class="body"] .columns,
		table[class="body"] .column {
			table-layout: fixed !important;
			float: none !important;
			width: 100% !important;
			padding-right: 0px !important;
			padding-left: 0px !important;
			display: block !important;
		}
		table[class="body"] .wrapper.first .columns,
		table[class="body"] .wrapper.first .column {
			display: table !important;
		}
		table[class="body"] table.columns td,
		table[class="body"] table.column td {
			width: 100% !important;
		}
		table[class="body"] .columns td.one,
		table[class="body"] .column td.one {
			width: 8.333333% !important;
		}
		table[class="body"] .columns td.two,
		table[class="body"] .column td.two {
			width: 16.666666% !important;
		}
		table[class="body"] .columns td.three,
		table[class="body"] .column td.three {
			width: 25% !important;
		}
		table[class="body"] .columns td.four,
		table[class="body"] .column td.four {
			width: 33.333333% !important;
		}
		table[class="body"] .columns td.five,
		table[class="body"] .column td.five {
			width: 41.666666% !important;
		}
		table[class="body"] .columns td.six,
		table[class="body"] .column td.six {
			width: 50% !important;
		}
		table[class="body"] .columns td.seven,
		table[class="body"] .column td.seven {
			width: 58.333333% !important;
		}
		table[class="body"] .columns td.eight,
		table[class="body"] .column td.eight {
			width: 66.666666% !important;
		}
		table[class="body"] .columns td.nine,
		table[class="body"] .column td.nine {
			width: 75% !important;
		}
		table[class="body"] .columns td.ten,
		table[class="body"] .column td.ten {
			width: 83.333333% !important;
		}
		table[class="body"] .columns td.eleven,
		table[class="body"] .column td.eleven {
			width: 91.666666% !important;
		}
		table[class="body"] .columns td.twelve,
		table[class="body"] .column td.twelve {
			width: 100% !important;
		}
		table[class="body"] td.offset-by-one,
		table[class="body"] td.offset-by-two,
		table[class="body"] td.offset-by-three,
		table[class="body"] td.offset-by-four,
		table[class="body"] td.offset-by-five,
		table[class="body"] td.offset-by-six,
		table[class="body"] td.offset-by-seven,
		table[class="body"] td.offset-by-eight,
		table[class="body"] td.offset-by-nine,
		table[class="body"] td.offset-by-ten,
		table[class="body"] td.offset-by-eleven {
			padding-left: 0 !important;
		}
		table[class="body"] table.columns td.expander {
			width: 1px !important;
		}
		table[class="body"] .right-text-pad,
		table[class="body"] .text-pad-right {
			padding-left: 10px !important;
		}
		table[class="body"] .left-text-pad,
		table[class="body"] .text-pad-left {
			padding-right: 10px !important;
		}
		table[class="body"] .hide-for-small,
		table[class="body"] .show-for-desktop {
			display: none !important;
		}
		table[class="body"] .show-for-small,
		table[class="body"] .hide-for-desktop {
			display: inherit !important;
		}
	}

	@media only screen and (max-width: 600px) {
		.body .container.content {
			width: 100% !important;
		}
		table[class="body"] .wrapper {
			padding-right: 15px !important;
		}
		.text-right {
			text-align: left !important;
		}
	}
</style>
<!-- begin page body -->
<table class="body" style="border-collapse: collapse; border-spacing: 0; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; height: 100%; line-height: 19px; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
	<tbody>
	<tr style="padding: 0; text-align: left; vertical-align: top;">
		<td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0; text-align: center; vertical-align: top; word-break: break-word;">
			<center style="min-width: 580px; width: 100%;">
				<!-- begin page header -->
				<table class="row header" style="/*background: #132d4c;*/ /*background-color:#eeeeee;*/ background-color:#ffffff; /*border-bottom: 5px solid #43bcca;*/ border-bottom: 5px solid #12b3e9; border-collapse: collapse; border-spacing: 0; color: #ffffff; padding: 0px; position: relative; text-align: left; vertical-align: top; width: 100%;">
					<tbody>
					<tr style="padding: 0; text-align: left; vertical-align: top;">
						<td class="center" align="center" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0; text-align: center; vertical-align: top; word-break: break-word;">
							<center style="min-width: 580px; width: 100%;">
								<!-- begin container -->
								<table class="container" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 580px;">
									<tbody>
									<tr style="padding: 0; text-align: left; vertical-align: top;">
										<td class="wrapper" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 15px 15px 0 15px; position: relative; text-align: left; vertical-align: top; word-break: break-word;">
											<!-- begin six columns -->
											<table class="six columns" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: left; vertical-align: top; width: 280px;">
												<tbody>
												<tr style="padding: 0; text-align: left; vertical-align: top;">
													<td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0 0 5px 0; text-align: left; vertical-align: top; word-break: break-word;">
														<img src="https://www2.expoindustri.com/assets/img/logo/logo_exind2_en.png" height="62" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; float: left; height: 62px; max-width: 100%; outline: none; text-decoration: none; width: auto;">
													</td>
													<td class="expander" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0 !important; text-align: left; vertical-align: top; visibility: hidden; width: 0px; word-break: break-word;"></td>
												</tr>
												</tbody>
											</table>
											<!-- end six columns -->
										</td>
										<td class="wrapper" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 15px 15px 0 15px; position: relative; text-align: left; vertical-align: top; word-break: break-word;">
											<!-- begin six columns -->
											<table class="six columns" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: left; vertical-align: top; width: 280px;">
												<tbody>
												<tr style="padding: 0; text-align: left; vertical-align: top;">
													<td class="text-right valign-middle" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0 0 15px 0; text-align: right; vertical-align: middle; word-break: break-word;">
														<span class="template-label" style="color:#ffffff; display:none;"></span>
													</td>
													<td class="expander" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0 !important; text-align: left; vertical-align: top; visibility: hidden; width: 0px; word-break: break-word;"></td>
												</tr>
												</tbody>
											</table>
											<!-- end six columns -->
										</td>
									</tr>
									</tbody>
								</table>
								<!-- end container -->
							</center>
						</td>
					</tr>
					</tbody>
				</table>
				<!-- end page header -->

				<!-- begin page container -->
				<table class="container content dark-theme" style="background: #f5f5f5; border: 1px solid #cccccc; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 580px;">
					<tbody>
					<tr style="padding: 0; text-align: left; vertical-align: top;">
						<td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0; text-align: left; vertical-align: top; word-break: break-word;">
							<!-- begin row -->
							<table class="row" style="border-collapse: collapse; border-spacing: 0; display: block; padding: 0px; position: relative; text-align: left; vertical-align: top; width: 100%;">
								<tbody>
								<tr style="padding: 0; text-align: left; vertical-align: top;">
									<!-- begin wrapper -->
									<td class="wrapper" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 15px 15px 0 15px; position: relative; text-align: left; vertical-align: top; word-break: break-word;">
										<table class="twelve columns" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: left; vertical-align: top; width: 580px;">
											<tbody>
											<tr style="padding: 0; text-align: left; vertical-align: top;">
												<td class="last" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0 0 15px 0; padding-bottom: 0 !important; padding-right: 0px; text-align: left; vertical-align: top; word-break: break-word;">
													<h4 style="color: #000000; font-family: 'Roboto', sans-serif; font-size: 18px; font-weight: normal; line-height: 1.3; margin: 5px 0 10px; padding: 0; text-align: left; word-break: normal;">{{ $titulo }}</h4>
												</td>
											</tr>
											</tbody>
										</table>
									</td>
									<!-- end wrapper -->
								</tr>
								</tbody>
							</table>
							<!-- end row -->
							<!-- begin divider -->
							<table class="divider" style="background: #000; border-collapse: collapse; border-spacing: 0; height: 1px; margin-top: 5px; padding: 0; text-align: left; vertical-align: top; width: 100%;"></table>
							<!-- end divider -->
							<!-- begin row -->
							<table class="row" style="border-collapse: collapse; border-spacing: 0; display: block; padding: 0px; position: relative; text-align: left; vertical-align: top; width: 100%;">
								<tbody>
								<tr style="padding: 0; text-align: left; vertical-align: top;">
									<!-- begin wrapper -->
									<td class="wrapper" style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 15px 15px 0 15px; position: relative; text-align: left; vertical-align: top; word-break: break-word;">
										<!-- begin twelve columns -->
										<table class="twelve columns" style="border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: left; vertical-align: top; width: 580px;">
											<tbody>
											<tr style="padding: 0; text-align: left; vertical-align: top;">
												<td style="-moz-hyphens: auto; -webkit-hyphens: auto; border-collapse: collapse !important; color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 19px; margin: 0; padding: 0 0 15px 0; text-align: left; vertical-align: top; word-break: break-word;">
													<p style="color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">
														{!! $content !!}
													</p>
													<p style="color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">
														<a href="https://www.expoindustri.com" style="color: #00acac; text-decoration: none;"><img src="https://www.expoindustri.com/assets/img/banners/email_banner_expoindustri_v2.jpg" width="580" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; float: left; max-width: 580px; width: 580px; outline: none; text-decoration: none; width: auto;"></a>
													</p>
												</td>
											</tr>
											</tbody>
										</table>
										<!-- end twelve columns -->
										<p style="color: rgba(0,0,0,0.87); font-family: 'Roboto', sans-serif; font-size: 12px; font-weight: normal; line-height: 19px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left;">
										</p>
									</td>
									<!-- end wrapper -->
								</tr>
								</tbody>
							</table>
							<!-- end row -->
						</td>
					</tr>
					</tbody>
				</table>
				<!-- end page container -->

				<!-- begin page footer -->
				<table class="row footer" style="border-top: 5px solid #005596;border-spacing: 0;border-collapse: collapse;padding: 0px;vertical-align: top;text-align: left;background: #ffffff;width: 100%;position: relative;">
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<td class="center" align="center" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: center;color: #222222;font-family: 'Helvetica', 'Arial', sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 12px;border-collapse: collapse !important;">
							<center style="width: 100%;min-width: 580px;">
								<!-- begin container -->
								<table class="container" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: inherit;width: 580px;margin: 0 auto;">
									<tr style="padding: 0;vertical-align: top;text-align: left;">
										<td class="wrapper" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 15px 15px 0 15px;vertical-align: top;text-align: left;color: #222222;font-family: 'Helvetica', 'Arial', sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 12px;position: relative;border-collapse: collapse !important;">
											<table class="four columns" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 180px;">
												<tr style="padding: 0;vertical-align: top;text-align: left;">
													<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 0 5px 0;vertical-align: top;text-align: left;color: #222222;font-family: 'Helvetica', 'Arial', sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 12px;border-collapse: collapse !important;">
														&copy; ExpoIndustri {{ date('Y') }}.
													</td>
													<td class="expander" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 !important;vertical-align: top;text-align: left;color: #222222;font-family: 'Helvetica', 'Arial', sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 12px;visibility: hidden;width: 0px;border-collapse: collapse !important;"></td>
												</tr>
											</table>
										</td>
										<td class="wrapper" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 15px 15px 0 15px;vertical-align: top;text-align: left;color: #222222;font-family: 'Helvetica', 'Arial', sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 12px;position: relative;border-collapse: collapse !important;padding-left: 0 !important;">
											<table class="eight columns" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 380px;">
												<tr style="padding: 0;vertical-align: top;text-align: left;">
													<td class="wrapper text-right valign-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 0 5px 0;vertical-align: middle;text-align: right;color: #222222;font-family: 'Helvetica', 'Arial', sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 12px;position: relative;border-collapse: collapse !important;">
														<!-- <a href="javascript:;">Unsubscribe</a> -->
														&nbsp;
														<a href="{{ route("public.{$glc}.nosotros") }}" style="color: #00acac;text-decoration: none;">@lang('messages.about_us')</a>
														&nbsp;
														<a href="{{ route('public.dataProtectionPolicy') }}" style="color: #00acac;text-decoration: none;">@lang('messages.privacy_policy')</a>
														&nbsp;
														<a href="{{ route('public.termsAndConditions') }}" style="color: #00acac;text-decoration: none;">@lang('messages.legal_terms_conditions')</a>
													</td>
													<td class="expander" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 !important;vertical-align: top;text-align: left;color: #222222;font-family: 'Helvetica', 'Arial', sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 12px;visibility: hidden;width: 0px;border-collapse: collapse !important;"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<!-- end container -->
							</center>
						</td>
					</tr>
				</table>
				<!-- end page footer -->
			</center>
		</td>
	</tr>
	</tbody>
</table>
<!-- end page body -->


</body>

</html>