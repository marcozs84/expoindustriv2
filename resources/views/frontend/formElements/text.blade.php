<div class="form-group col-md-12" style="display: block;">
	<label class="control-label">{!! __('form.'.$e['key']) !!}
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>
	{!! Form::textarea($e['key'], ( isset($e['value']) ) ? $e['value'] : '', [
		'id' => $e['key'],
		'class' => 'form-control',
		'placeholder' => (isset($e['placeholder'])) ? $e['placeholder']: '',
		'rows' => 3
	]) !!}
</div>