<div class="form-group col-md-6">
	<label class="control-label">{!! __('form.'.$e['key']) !!}
		{{--@if ($errors->has($e['key))--}}
		<a href="javascript:;" id="errorDisplay_{{ $e['key'] }}" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
		{{--@endif--}}
	</label>

	@php
		$val = null;
		$val = ( isset($e['value']) ) ? $e['value'] : $val;

		if($e['key'] == 'medida_de_transporte_largo_x_ancho_x_alto'){
			if(session('pubsess', 'none') != 'none'){
				$sess = session('pubsess');
				$largo = str_replace('_sb_', '', $sess['longitud']);
				$ancho = str_replace('_sb_', '', $sess['ancho']);
				$alto = str_replace('_sb_', '', $sess['alto']);

				$val = "{$largo} x {$ancho} x {$alto}";
			}
		}

		if($e['key'] == 'peso_bruto'){
			if(session('pubsess', 'none') != 'none'){
				$sess = session('pubsess');
				$val = str_replace('_sb_', '', $sess['peso']);
			}
		}
	@endphp

	@if(isset($e['compliment']) && $e['compliment'] != '')
		<div class="input-group">
			{!! Form::text($e['key'], $val, [
			'id' => $e['key'],
			'class' => 'form-control',
			'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : ''
		]) !!}
			@if(isset($e['compliment']))
				<span class="input-group-addon">{{ $e['compliment'] }}</span>
			@endif
		</div>
	@else


		{!! Form::text($e['key'], $val, [
			'id' => $e['key'],
			'class' => 'form-control', 
			'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : ''
		]) !!}
	@endif
</div>