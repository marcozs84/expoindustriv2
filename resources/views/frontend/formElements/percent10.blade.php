<div class="form-group col-md-6">
	<label class="control-label">{!! __('form.'.$e['key']) !!}
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>

	@php

		$data = [];

		if($e['index_0']!= '' && $e['index_0']!= '0'){
			$e['data'][$e['index_0']] = __('form.'.$e['index_0']);
		}

		for($i = 100; $i >= 0  ; $i-=10){
			$e['data'][$i] = $i;
		}


		$attribs = [];
		$attribs['id'] = $e['key'];
		$attribs['class'] = 'form-control';
		if(isset($e['placeholder']) && $e['placeholder']!= ''){
			$attribs['placeholder'] = __('form.'.$e['placeholder']);
		}

	@endphp

	{!! Form::select($e['key'], $e['data'], ( isset($e['value']) ) ? $e['value'] : '', $attribs) !!}
</div>