<div class="form-group col-md-6">
	<label class="control-label">{!! __('form.'.$e['key']) !!}
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>
	<br>
		{!! Form::checkbox($e['key'], 1, false, [
		    'id' => $e['key'],
		    'class' => 'form-control',
		    'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : '',
		    'data-render' => 'switchery',
		    'data-theme' => 'blue'
		]) !!}

	<input type="checkbox"> 

</div>