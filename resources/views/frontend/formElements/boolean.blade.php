<div class="form-group col-md-6">
	<label class="control-label">{!! __('form.'.$e['key']) !!}
		@if ($errors->has($e['key']))
			<span class="text-danger">*</span>
		@endif
	</label>
		{!! Form::select($e['key'], [
			'none' => __('form.none'),
			'yes' => __('form.yes'),
			'no' => __('form.no'),
		], ( isset($e['value']) ) ? $e['value'] : '', [
		    'id' => $e['key'],
		    'class' => 'form-control',

		]) !!}
		{{--'placeholder' => (isset($e['placeholder'])) ? $e['placeholder'] : '',--}}

</div>