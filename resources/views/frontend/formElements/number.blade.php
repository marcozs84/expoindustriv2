<div class="form-group col-md-6">
	<label class="control-label">
		@if( isset($e['label']) )
			{{ $e['label'] }}
		@else
			{!! __('form.'.$e['key']) !!}
		@endif
		<a href="javascript:;" id="errorDisplay_{{ $e['key']}}" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
	</label>

	@php
		$val = null;
		$val = ( isset($e['value']) ) ? $e['value'] : $val;

		if($e['key'] == 'medida_de_transporte_largo_x_ancho_x_alto'){
			if(session('pubsess', 'none') != 'none'){
				$sess = session('pubsess');
				$largo = str_replace('_sb_', '', $sess['longitud']);
				$ancho = str_replace('_sb_', '', $sess['ancho']);
				$alto = str_replace('_sb_', '', $sess['alto']);

				$val = "{$largo} x {$ancho} x {$alto}";
			}
		}

		if($e['key'] == 'peso_bruto'){
			if(session('pubsess', 'none') != 'none'){
				$sess = session('pubsess');
				$val = str_replace('_sb_', '', $sess['peso']);
			}
		}

		$attribs = [];
		$attribs['id'] = $e['key'];
		$attribs['class'] = 'form-control';
		if(isset($e['placeholder']) && $e['placeholder']!= ''){
			$attribs['placeholder'] = __('form.'.$e['placeholder']);
		}

	@endphp

	@if(isset($e['compliment']) && $e['compliment'] != '')
		<div class="input-group">
			{!! Form::number($e['key'], $val, $attribs) !!}
			@if(isset($e['compliment']))
			<span class="input-group-addon">{{ $e['compliment']}}</span>
			@endif
		</div>
	@else
		{!! Form::number($e['key'], $val, $attribs) !!}
	@endif
</div>