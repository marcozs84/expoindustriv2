<div class="form-group col-md-6">
	<label class="control-label">{!! __('form.'.$e['key']) !!}
		<a href="javascript:;" id="errorDisplay_{{ $e['key']}}" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
	</label>
	
		@php

			$data = [];

			if($e['index_0']!= '' && $e['index_0']!= '0'){
				$e['data'][$e['index_0']] = __('form.'.$e['index_0']);
			}

			for($i = (int)date('Y') +1; $i >= 1950  ; $i--){
				$e['data'][$i] = $i;
			}


			$attribs = [];
			$attribs['id'] = $e['key'];
			$attribs['class'] = 'form-control';
			if(isset($e['placeholder']) && $e['placeholder']!= ''){
				$attribs['placeholder'] = __('form.'.$e['placeholder']);
			}

		@endphp

		{!! Form::select($e['key'], $e['data'], ( isset($e['value']) ) ? $e['value'] : '', $attribs) !!}
</div>