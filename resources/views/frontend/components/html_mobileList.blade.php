<!-- BEGIN #mobile-list -->
<div id="mobile-list" class="section-container bg-silver p-t-0 ">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		<h4 class="section-title clearfix">
			<a href="#" class="pull-right">SHOW ALL</a>
			Mobile Phones
			<small>Shop and get your favourite phone at amazing prices!</small>
		</h4>
		<!-- END section-title -->
		<!-- BEGIN category-container -->
		<div class="category-container">
			<!-- BEGIN category-sidebar -->
			<div class="category-sidebar">
				<ul class="category-list">
					<li class="list-header">Top Categories</li>
					<li><a href="#">Microsoft</a></li>
					<li><a href="#">Samsung</a></li>
					<li><a href="#">Apple</a></li>
					<li><a href="#">Micromax</a></li>
					<li><a href="#">Karbonn</a></li>
					<li><a href="#">Intex</a></li>
					<li><a href="#">Sony</a></li>
					<li><a href="#">HTC</a></li>
					<li><a href="#">Asus</a></li>
					<li><a href="#">Nokia</a></li>
					<li><a href="#">Blackberry</a></li>
					<li><a href="#">All Brands</a></li>
				</ul>
			</div>
			<!-- END category-sidebar -->
			<!-- BEGIN category-detail -->
			<div class="category-detail">
				<!-- BEGIN category-item -->
				<a href="#" class="category-item full">
					<div class="item">
						<div class="item-cover">
							<img src="/assets/img/e-commerce/product/product-samsung-s7-edge.jpg" alt="" />
						</div>
						<div class="item-info bottom">
							<h4 class="item-title">Samsung Galaxy s7 Edge + Geat 360 + Gear VR</h4>
							<p class="item-desc">Redefine what a phone can do</p>
							<div class="item-price">$799.00</div>
						</div>
					</div>
				</a>
				<!-- END category-item -->
				<!-- BEGIN category-item -->
				<div class="category-item list">
					<!-- BEGIN item-row -->
					<div class="item-row">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-iphone.png" alt="" />
								<div class="discount">15% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_detail.html">iPhone 6s Plus<br />16GB</a>
								</h4>
								<p class="item-desc">3D Touch. 12MP photos. 4K video.</p>
								<div class="item-price">$649.00</div>
								<div class="item-discount-price">$739.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-samsung-note5.png" alt="" />
								<div class="discount">32% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">Samsung Galaxy Note 5<br />Black</a>
								</h4>
								<p class="item-desc">Super. Computer. Now in two sizes.</p>
								<div class="item-price">$599.00</div>
								<div class="item-discount-price">$799.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-iphone-se.png" alt="" />
								<div class="discount">20% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">iPhone SE<br />32/64Gb</a>
								</h4>
								<p class="item-desc">A big step for small.</p>
								<div class="item-price">$499.00</div>
								<div class="item-discount-price">$599.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END item-row -->
					<!-- BEGIN item-row -->
					<div class="item-row">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-zenfone2.png" alt="" />
								<div class="discount">15% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_detail.html">Assus ZenFone 2<br />‏(ZE550ML)</a>
								</h4>
								<p class="item-desc">See What Others Can’t See</p>
								<div class="item-price">$399.00</div>
								<div class="item-discount-price">$453.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-xperia-z.png" alt="" />
								<div class="discount">32% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">Sony Xperia Z<br />Black Color</a>
								</h4>
								<p class="item-desc">For unexpectedly beautiful moments</p>
								<div class="item-price">$599.00</div>
								<div class="item-discount-price">$799.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-lumia-532.png" alt="" />
								<div class="discount">20% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">Microsoft Lumia 531<br />Smartphone Orange</a>
								</h4>
								<p class="item-desc">1 Year Local Manufacturer Warranty</p>
								<div class="item-price">$99.00</div>
								<div class="item-discount-price">$199.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END item-row -->
				</div>
				<!-- END category-item -->
			</div>
			<!-- END category-detail -->
		</div>
		<!-- END category-container -->
	</div>
	<!-- END container -->
</div>
<!-- END #mobile-list -->

<!-- BEGIN #tablet-list -->
<div id="tablet-list" class="section-container bg-silver p-t-0 ">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		<h4 class="section-title clearfix">
			<a href="#" class="pull-right">SHOW ALL</a>
			Tablet
			<small>Shop and get your favourite tablet at amazing prices!</small>
		</h4>
		<!-- END section-title -->
		<!-- BEGIN category-container -->
		<div class="category-container">
			<!-- BEGIN category-sidebar -->
			<div class="category-sidebar">
				<ul class="category-list">
					<li class="list-header">Top Categories</li>
					<li><a href="#">Apple</a></li>
					<li><a href="#">HP</a></li>
					<li><a href="#">Huawei</a></li>
					<li><a href="#">Samsung</a></li>
					<li><a href="#">Sony</a></li>
					<li><a href="#">All Brands</a></li>
				</ul>
			</div>
			<!-- END category-sidebar -->
			<!-- BEGIN category-detail -->
			<div class="category-detail">
				<!-- BEGIN category-item -->
				<a href="#" class="category-item full">
					<div class="item">
						<div class="item-cover">
							<img src="/assets/img/e-commerce/product/product-huawei-mediapad.jpg" alt="" />
						</div>
						<div class="item-info bottom">
							<h4 class="item-title">Huawei MediaPad T1 7.0</h4>
							<p class="item-desc">Vibrant colors. Beautifully displayed</p>
							<div class="item-price">$299.00</div>
						</div>
					</div>
				</a>
				<!-- END category-item -->
				<!-- BEGIN category-item -->
				<div class="category-item list">
					<!-- BEGIN item-row -->
					<div class="item-row">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-ipad-pro.png" alt="" />
								<div class="discount">15% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_detail.html">9.7-inch iPad Pro<br />32GB</a>
								</h4>
								<p class="item-desc">3D Touch. 12MP photos. 4K video.</p>
								<div class="item-price">$649.00</div>
								<div class="item-discount-price">$739.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-galaxy-tab2.png" alt="" />
								<div class="discount">32% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">Samsung Galaxy Tab S2<br />Black</a>
								</h4>
								<p class="item-desc">A Brilliant Screen That Adjusts to You</p>
								<div class="item-price">$399.99</div>
								<div class="item-discount-price">$499.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-galaxy-taba.png" alt="" />
								<div class="discount">20% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">Samsung Galaxy Tab A<br />9.7" 16Gb(Wi-Fi)</a>
								</h4>
								<p class="item-desc">Keep All Your Samsung Devices In Sync</p>
								<div class="item-price">$349.99</div>
								<div class="item-discount-price">$399.99</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END item-row -->
					<!-- BEGIN item-row -->
					<div class="item-row">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-hp-spectrex2.png" alt="" />
								<div class="discount">15% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_detail.html">HP Spectre x2<br />‏12-a011nr</a>
								</h4>
								<p class="item-desc">Our thinnest detachable separates from all others.</p>
								<div class="item-price">$799.99</div>
								<div class="item-discount-price">$850.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-xperia-z2.png" alt="" />
								<div class="discount">32% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">Sony Xperia Z2 Tablet<br />Black Color</a>
								</h4>
								<p class="item-desc">For unexpectedly beautiful moments</p>
								<div class="item-price">$199.00</div>
								<div class="item-discount-price">$259.00</div>
							</div>
						</div>
						<!-- END item -->
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="/assets/img/e-commerce/product/product-ipad-air.png" alt="" />
								<div class="discount">20% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product.html">iPad Air 2<br />32/64Gb</a>
								</h4>
								<p class="item-desc">Light. Heavyweight.</p>
								<div class="item-price">$399.00</div>
								<div class="item-discount-price">$459.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END item-row -->
				</div>
				<!-- END category-item -->
			</div>
			<!-- END category-detail -->
		</div>
		<!-- END category-container -->
	</div>
	<!-- END container -->
</div>
<!-- END #tablet-list -->