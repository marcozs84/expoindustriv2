@extends(($isAjaxRequest == true) ? 'frontend.layouts.ajax' : 'frontend.layouts.default')

@section('title', 'Email - Inbox')

@section('content')

	<!-- BEGIN similar-product -->
	<h4 class="section-title clearfix m-t-20">
		@if($publications->currentPage() < $publications->lastPage())
			<a href="javascript:;" onclick="latestAddedItems({{ $publications->currentPage() + 1 }});" class="pull-right m-l-5"><i class="fa fa-angle-right f-s-18"></i></a>
		@endif
		@if($publications->currentPage() > 1)
			<a href="javascript:;" onclick="latestAddedItems({{ $publications->currentPage() - 1 }});" class="pull-right"><i class="fa fa-angle-left f-s-18"></i></a>
		@endif
		@lang('messages.recent_announces')
		<small>@lang('messages.be_first_visitor')!</small>
	</h4>
	<div class="row row-space-10">

		@foreach($publications as $pub)

			<div class="col-md-2 col-sm-4">
				<!-- BEGIN item -->
				<div class="item item-thumbnail">
					<a href="{{ $pub->Producto->url_csmmid }}" target="_blank" class="item-image">
						@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)
							<img src="{{ $pub->Producto->Galerias[0]->Imagenes[0]->ruta_publica_producto_thumb }}" alt="">
						@else
							@lang('messages.no_image_available')
						@endif
						{{--<div class="discount">15% OFF</div>--}}
					</a>
					<div class="item-info">
						<h4 class="item-title">
							<a href="{{ $pub->Producto->url_csmmid }}" target="_blank">{!! $pub->Producto->nombre_alter_ml !!}</a>
						</h4>
						{{--<p class="item-desc">3D Touch. 12MP photos. 4K video.</p>--}}
						@if($global_isProvider == true)
							@if($pub->Producto->precioSudamerica === true)
								<div class="item-price">@lang('messages.price_not_available')</div>
							@else
								<div class="item-price">{{ strtoupper(session('current_currency', 'usd')) }} {{ number_format($pub->Producto->precio_publicacion, 2) }}</div>
							@endif
						@else
							<div class="item-price">{{ strtoupper(session('current_currency', 'usd')) }} {{ number_format($pub->Producto->precio_publicacion, 2) }}</div>
						@endif
						{{--<div class="item-discount-price">$739.00</div>--}}
					</div>
				</div>
				<!-- END item -->
			</div>

		@endforeach

	</div>
	<!-- END similar-product -->

@endsection
