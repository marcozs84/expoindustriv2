@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">Inicio</a></li>
				<li><a href="/cuenta">Mi cuenta</a></li>
				<li class="active">Mis ordenes</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar">
					<div class="account-sidebar-cover">
						<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />
					</div>
					<div class="account-sidebar-content">
						<h4>Detalle de su consulta</h4>
						<p>
							Por favor no dude en contactarnos si requiere asistencia.
						</p>
					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">
							<!-- BEGIN checkout-body -->
							<div class="checkout-body">
								<!-- BEGIN checkout-message -->
								<div class="checkout-message">
									<div class="table-responsive2">
										<table class="table table-payment-summary">
											<tbody>
											<tr>
												<td class="field">Mensajes de la consulta</td>
												<td class="value"></td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!-- END checkout-message -->
							</div>
							<!-- END checkout-body -->
						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection