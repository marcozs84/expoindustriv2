@extends('frontend.layouts.default')
@push('css')
<link href="/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css" />
<style>

	label{
		/*display:inline-block !important;*/
		margin-bottom:0px !important;
		color: #4e4e4e;
	}
	/*input {*/
		/*display:block !important;*/
	/*}*/

	h4, h5 {
		display:block;
		clear:both;
		padding-left:17px;
	}

	h5{
		padding-bottom:10px;
		border-bottom:1px solid #eee;
	}

	.leftColumn{
		vertical-align:top;
	}
	.rightColumn{
		padding-left:10px !important;
	}

	.account-sidebar-cover{
		width:300px; overflow:hidden;
	}

	.account-body{
		margin-left:10px !important;
		padding-top:10px;
	}

	@media (min-width:991px) {
		.leftColumn {
			width:260px;
		}
		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:1200px) {
		.leftColumn {
			/*width:160px;*/
		}
	}
	@media (max-width:992px) {
		.leftColumn {
			width:190px;
			/*display:none;*/
		}

		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:769px) {
		.leftColumn {
			/*width:190px;*/
			display:none;
		}

		.account-sidebar{
			display:none;
		}
		.account-body{
			margin-left:-20px !important;
			padding-top:10px;
		}
	}

	.specialsel2 .select2-container { width:100% !important; margin-top:0px !important; }
</style>
@endpush
@section('content')

	@include('frontend.includes.topMessage')


	<!-- BEGIN #checkout-payment -->
	<div class="section-container" id="checkout-payment">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN checkout -->
			<div class="checkout">
				<form action="checkout_complete.html" method="POST" name="form_payment" class="form-horizontal">
					<!-- BEGIN checkout-header -->
					<div class="checkout-header">
						<!-- BEGIN row -->
						<div class="row">
							<!-- BEGIN col-3 -->
							<div class="col-md-3 col-sm-3">
								<div class="step">
									<a href="checkout_cart.html">
										<div class="number">1</div>
										<div class="info">
											<div class="title">Publicacion</div>
											<div class="desc">La publicación ya fué creada.</div>
										</div>
									</a>
								</div>
							</div>
							<!-- END col-3 -->
							<!-- BEGIN col-3 -->
							<div class="col-md-3 col-sm-3">
								<div class="step">
									<a href="checkout_info.html">
										<div class="number">2</div>
										<div class="info">
											<div class="title">verificacion de cuenta</div>
											<div class="desc">Usuario confirmado.</div>
										</div>
									</a>
								</div>
							</div>
							<!-- END col-3 -->
							<!-- BEGIN col-3 -->
							<div class="col-md-3 col-sm-3">
								<div class="step active">
									<a href="#">
										<div class="number">3</div>
										<div class="info">
											<div class="title">Pago</div>
											<div class="desc">Aenean ut pretium ipsum. </div>
										</div>
									</a>
								</div>
							</div>
							<!-- END col-3 -->
							<!-- BEGIN col-3 -->
							<div class="col-md-3 col-sm-3">
								<div class="step">
									<a href="checkout_complete.html">
										<div class="number">4</div>
										<div class="info">
											<div class="title">Pago completado</div>
											<div class="desc">Curabitur interdum libero.</div>
										</div>
									</a>
								</div>
							</div>
							<!-- END col-3 -->
						</div>
						<!-- END row -->
					</div>
					<!-- END checkout-header -->
					<!-- BEGIN checkout-body -->
					<div class="checkout-body">
						<h4 class="checkout-title">Por favor ingrese a su cuenta</h4>




						<div id="dibs-complete-checkout"> </div>

						<a href="javascript:;" onclick="sendPayment()" class="btn btn-primary">Send</a>

						{{--<button id="CreatePayment">Create payment</button>--}}





					</div>
					<!-- END checkout-body -->
					<!-- BEGIN checkout-footer -->
					<div class="checkout-footer">
						<a href="checkout_info.html" class="btn btn-white btn-lg pull-left">Back</a>
						<button type="submit" class="btn btn-inverse btn-lg p-l-30 p-r-30 m-l-10">Proceed</button>
					</div>
					<!-- END checkout-footer -->
				</form>
			</div>
			<!-- END checkout -->

			<h4 class="checkout-title m-t-30 m-b-15">Payment Frequently Asked Questions</h4>
			<!-- BEGIN checkout-question-list -->
			<div class="row checkout-question-list" id="checkout-faq">
				<!-- BEGIN col-6 -->
				<div class="col-md-6">
					<div class="clearfix">
						<div class="question">
							<a href="#checkout-faq-1" class="collapsed" data-toggle="collapse" data-parent="#checkout-faq">
								<span class="dash">-</span> Is my Credit Card / Debit Card details protected?
							</a>
						</div>
						<div class="answer collapse in" id="checkout-faq-1">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit, felis vel tincidunt sodales, urna metus rutrum leo, sit amet finibus velit ante nec lacus. Cras erat nunc, pulvinar nec leo at, rhoncus elementum orci. Nullam ut sapien ultricies, gravida ante ut, ultrices nunc.
						</div>
					</div>
					<div class="clearfix m-t-10">
						<div class="question">
							<a href="#checkout-faq-2" class="collapsed" data-toggle="collapse" data-parent="#checkout-faq">
								<span class="dash">-</span> Can I use a Debit Card to make payment??
							</a>
						</div>
						<div class="answer collapse in" id="checkout-faq-2">
							Curabitur vitae venenatis odio, eget molestie mauris. Nullam vitae turpis at mi consequat rutrum quis quis mi. Vestibulum imperdiet neque non libero condimentum, quis sodales magna molestie. Interdum et malesuada fames ac ante ipsum primis in faucibus.
						</div>
					</div>
					<div class="clearfix m-t-10">
						<div class="question">
							<a href="#checkout-faq-3" class="collapsed" data-toggle="collapse" data-parent="#checkout-faq">
								<span class="dash">-</span> Credit Card/Debit Card transaction keep failing. Why?
							</a>
						</div>
						<div class="answer collapse in" id="checkout-faq-3">
							Cras malesuada mi quis purus pharetra egestas. Curabitur auctor sapien est, eu porttitor velit ornare in. Fusce porta suscipit diam at placerat. Donec lobortis iaculis accumsan. Ut semper felis vel nisi aliquam facilisis.
						</div>
					</div>
				</div>
				<!-- END col-6 -->
				<!-- BEGIN col-6 -->
				<div class="col-md-6">
					<div class="clearfix">
						<div class="question">
							<a href="#checkout-faq-4" class="collapsed" data-toggle="collapse" data-parent="#checkout-faq">
								<span class="dash">-</span> Did not receive the Pin Code on my mobile?
							</a>
						</div>
						<div class="answer collapse in" id="checkout-faq-4">
							Duis gravida sem eu arcu efficitur, laoreet egestas nibh posuere. Pellentesque suscipit tincidunt porttitor. Aliquam vitae massa vel justo lobortis posuere. Nulla tempor enim at auctor dignissim. Aenean sit amet venenatis odio.
						</div>
					</div>
					<div class="clearfix m-t-10">
						<div class="question">
							<a href="#checkout-faq-5" class="collapsed" data-toggle="collapse" data-parent="#checkout-faq">
								<span class="dash">-</span> My credit card has been charged, but my order shows failed?
							</a>
						</div>
						<div class="answer collapse in" id="checkout-faq-5">
							Nunc consectetur tellus libero, at tempor dolor scelerisque id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat ligula vitae tincidunt sagittis. Etiam congue ligula purus, ut fringilla ante interdum eu.
						</div>
					</div>
				</div>
				<!-- END col-6 -->
			</div>
			<!-- END checkout-question-list -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #checkout-payment -->


@endsection

@push('scripts')

<script src="https://test.checkout.dibspayment.eu/v1/checkout.js?v=1"></script>
<script>

	$(document).ready(function() {
		$("#CreatePayment").on('click', function() {
			$.ajax({
				url: 'api/createpayment.php',
				data: {
					action: 'createPay',
//					orderID
				},
				dataType: 'json',
				success: function(data) {
					paymentID = JSON.stringify(data);
					var obj = jQuery.parseJSON(paymentID);
					paymentID = obj.paymentId;
					initCheckout(paymentID);
				}
			});
		});
	});

	function sendPayment() {
		var url = '{{ route('public.payment_dibs') }}';
		var data = {
			id: 1
		};

		ajaxPost(url, data, {
			onSuccess: function (data) {
				console.log(data);
				console.log(data.data.paymentId);
				initCheckout(data.data.paymentId, data.data.checkoutKey);
//			$(document).trigger('formPamentCreate:aceptar');
			}
		});
	}

	function initCheckout(paymentId, checkoutKey) {
		var checkoutOptions = {
			checkoutKey: checkoutKey, //[Required] Test or Live GUID with dashes

			paymentId : paymentId, //[required] GUID without dashes
			containerId : 'dibs-complete-checkout', //[optional] defaultValue: dibs-checkout-content
			language: "en-GB",            //[optional] defaultValue: en-GB
		};

		console.log(checkoutOptions);
		var checkout = new Dibs.Checkout(checkoutOptions);

		//this is the event that the merchant should listen to redirect to the “payment-is-ok” page

		checkout.on('payment-completed', function(response) {
			/*
			 Response:
			 paymentId: string (GUID without dashes)
			 */
			console.log("payment-completed");
			console.log(response);
			window.location = '/PaymentSuccessful';
		});

		checkout.on('pay-initialized', function(response) {
			console.log("payment-initialized");
			/*
			 Complete the desired operations such as update payment
			 */
			checkout.send('payment-order-finalized', false);
		});
	}
</script>


{{--<script src="https://js.stripe.com/v3/"></script>--}}

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>--}}
<script>

	var elements = '';
	var ccard = '';
	var ncard = '';
	var cecard = '';
	var cvcard = '';
	var stripe = '';

	// ----------

	var current_payment_type = 'dibs';

	// ----------
	$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
		initForm();
	});
	$.getScript('https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js').done(function(){
//		$('input').iCheck({
//			checkboxClass: 'icheckbox_minimal',
//			radioClass: 'iradio_minimal',
//			increaseArea: '20%' // optional
//		});
	});
	var stepNum = 1;
	var cats = {!! json_encode($categorias) !!};

	var codigosPais = {!! json_encode($global_paisesCodigo) !!};

	$(document).ready(function(){
//		initForm();

//		$( "#payment-form" ).submit(function( event ) {
//			if($('#legalterms').prop('checked')) {
//				sendForm(document.getElementById('submitPayment'));
//			} else {
//				console.log("checked");
//				alert('You must read and accept the Legal terms and conditions before submitting your payment.')
//
//			}
//			event.preventDefault();
//		});

		$('#pais, #regPubPais').select2({
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			templateResult: formatRepoPubr,
			templateSelection: formatRepoSelectionPubr
		});

		$( "#formLoginPub" ).submit(function( event ) {
			tryLoginPub();
			event.preventDefault();
		});

		$( "#formRegisterPub" ).submit(function( event ) {
			tryRegisterPub();
			event.preventDefault();
		});

		$('#chk_time_extended').on('change', function(e) {
			calc_price(undefined);
		});
	});

	var step6 = false;
	var step7 = false;
	var currentStep = 1;

	function showStep(n){
		console.log("step " + n);
		var validation1 = true;
		if(n === 2){
			var inps = ['marca', 'modelo'];

			$.each(inps, function(v, k){
				if($('#'+k).val() === ''){
					$('#'+k).css({'border':'1px solid #ff0000'});
					$('#errorDisplay_'+k).attr('data-toggle', 'tooltip')
						.attr('data-title', '@lang('messages.required')')
						.removeClass('hide');
					validation1 = false;
				} else {
					$('#'+k).css({'border':'1px solid #ccd0d4'});
					$('#errorDisplay_'+k).addClass('hide');
				}
			});
			$("[data-toggle=tooltip]").tooltip();
			if(validation1 === false){
				return false;
			}
			var data = {
				'marca' : $('#marca').val(),
				'modelo' : $('#modelo').val()
			};
			updateSession(data);
		}
		if(n === 3 || n === 4 || n === 5){

			if( n=== 3){
				inps = ['categoria', 'subcategoria'];
			}
			if( n=== 4){
				inps = ['pais', 'ciudad', 'codigo_postal', 'direccion'/*, 'ubicacion'*/];
			}
			if( n=== 5){
				inps = ['longitud', 'ancho', 'alto', 'peso_bruto_fix'];
			}
			console.log(inps);
			validation1 = true;
			$.each(inps, function(v, k){
				if($('#'+k).val() === ''){
					console.log("campo: " + k);
					console.log($('#'+k).val())
					$('#'+k).css({'border':'1px solid #ff0000'});
					$('#errorDisplay_'+k).attr('data-toggle', 'tooltip')
						.attr('data-title', '@lang('messages.required')')
						.removeClass('hide');
					validation1 = false;
				} else {
					$('#'+k).css({'border':'1px solid #ccd0d4'});
					$('#errorDisplay_'+k).addClass('hide');
				}
			});
			$("[data-toggle=tooltip]").tooltip();
			if(validation1 === false){
				return false;
			}

			if(n === 3){
				data = {
					'categoria' : $('#categoria').val(),
					'subcat' : $('#subcategoria').val()
				};
			}
			if(n === 4){
				data = {
					'pais' : $('#pais').val(),
					'ciudad' : $('#ciudad').val(),
					'pcode' : $('#codigo_postal').val(),
					'direccion' : $('#direccion').val(),
//					'ubicacion' : $('#ubicacion').val(),
				};
			}
			if(n === 5){
				data = {
					'longitud' : $('#longitud').val(),
					'ancho' : $('#ancho').val(),
					'alto' : $('#alto').val(),
					'peso' : $('#peso_bruto_fix').val(),
					'palet' : ($('#paletizado').prop('checked')) ? 1 : 0,
				};
			}
			updateSession(data);

			if(n === 5) {
				var scat = $('#subcategoria').val();
				if(scat === '' || scat < 1){
					return false;
				}
				if(currentStep < 5){
					getForm(scat);
				} else {
					step6 = false;
				}

			}
		}

		if(n === 6){
			if(step6 !== true){
				validateForm(function(){
					step6 = true;
					showStep(6);
				});
				return false;
			}
		}

		@if(Auth::guard('clientes')->user())
			step7 = true;
		@endif
		if(n === 7){
			if(step7 !== true){
				alert("You must log in first");
				return false;
			}
		}


		for(var i = 1 ; i <= 6 ; i++){
			if(i < n){
				$('.step_'+i+' input, .step_'+i+' number, .step_'+i+' select, .step_'+i+' textarea').each(function(e){
					$(this).attr('disabled','disabled');
				});
				$('.step_'+i+' .btnNext, .step_'+i+' .btnPrev').each(function(e){
					$(this).removeAttr('onclick');
					$(this).attr('disabled','disabled');
				});

			} else {
				$('.step_'+i+' input, .step_'+i+' number, .step_'+i+' select, .step_'+i+' select').each(function(e){
					$(this).removeAttr('disabled');
				});
				$('.step_'+i+' .btnPrev').each(function(e){
					$(this).removeAttr('disabled');
					$(this).attr('onclick', 'showStep('+(i-1)+')');
				});
				$('.step_'+i+' .btnNext').each(function(e){
					$(this).removeAttr('disabled');
					$(this).attr('onclick', 'showStep('+(i+1)+')');
				});
			}

		}
		$('tr.step_'+(n+1)).hide('slow');
		$('tr.step_'+n).show('slow');

		$('html, body').animate({
			scrollTop: $('tr.step_'+n).offset().top
		}, 1000);

		currentStep = n;
	}

	function changeSubs(e){
		cat = $(e).val();

		$('#subcategoria').html('');
		$.each(cats, function(k,v){
			if(v.id == cat){
				subs = v.subcategorias;
				$('#subcategoria').append($('<option>', {
					'value': ''
				}).html('@lang("messages.select_subcategory")'));
				$.each(subs, function(k1, v1){
					$('#subcategoria').append($('<option>', {
						'value': v1.id
//					}).html(v1.nombre));
					}).html(v1.traduccion.{{ App::getLocale() }}));
				});
			}
		});

	}

//	function loadForm(e){
//		scat = $(e).val();
//		getForm(scat);
//	}
//	getForm(12);

	function modalTerms() {
		$('#modal-announce').modal();
	}

	function getForm(scat){

		var url = '{{ route('public.publishForm', [0]) }}';
		url = url.replace(0, scat);
		var data = {
			idCategoria: scat,
		};

		$('#formHolder').html("@lang('messages.loading')...");

		$('#formHolder').load(url, function(response, status, xhr){
			if(xhr.status == 500){
				response = response.replace('position: fixed;', '');
				$(this).html(response);
			}

			if($('#medida_de_transporte_largo_x_ancho_x_alto').length > 0){
				var dims = $('#longitud').val() + ' x ' + $('#ancho').val() + ' x ' + $('#alto').val();
				$('#medida_de_transporte_largo_x_ancho_x_alto').val(dims);
			}
			if($('#peso_bruto').length > 0){
				var pbr = $('#peso_bruto_fix').val();
				$('#peso_bruto').val(pbr);
			}



			{{--console.log($('#dropzone'));--}}
			{{--$('#dropzone').dropzone({--}}
				{{--url: '{{ route('public.publishUpload') }}'--}}
			{{--});--}}

//			var myDropzone = new Dropzone("div#dropzone", { url: "/file/post"});
		});
	}

	function getModelos(e){
		var marca = $(e).val();
		console.log(marca);
		var url = '{{ route('public.getModelos') }}';
		var data = {
			marca : marca
		};

		ajaxPost(url, data, {
			onSuccess: function(data){
				if(data.data.length > 0){
					$('#modelo').autocomplete({
						source: data.data
					});
				}
			}
		});

	}

	function initForm(){
	}

	function updateSession(data){
		var url = '{{ route('public.updateSession') }}';

		ajaxPost(url, data, {
			onValidationOverride: true,
			onSuccess: function(data){
			},
			onFailure: function(data){
			},
			onDone: function(data){
			},
		});
	}

	var map = null;
	var geocoder = null;
	var marker = null;
	function initPublishMap() {
		var uluru = {lat: -25.363, lng: 131.044};
		map = new google.maps.Map(document.getElementById('publish_map'), {
			zoom: 4,
			center: uluru,
//			query: 'Bolivia'
		});

		geocoder = new google.maps.Geocoder();

		var address = $("#pais option:selected").text();
		geocodeAddress(geocoder, map, address);

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng, map);
		});

//		var marker = new google.maps.Marker({
//			position: uluru,
//			map: map
//		});
	}

	function placeMarker(location, map) {
		if (marker === null){
			marker = new google.maps.Marker({
				position: location,
				map: map
			});
		} else {
			marker.setPosition(location);
		}

//		marker = new google.maps.Marker({
//			position: location,
//			map: map
//		});
		map.panTo(location);
	}
	function geocodeAddress(geocoder, resultsMap, address) {
//		var address = 'Bolivia';
//		var address = $("#pais option:selected").text();
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);
//				var marker = new google.maps.Marker({
//					map: resultsMap,
//					position: results[0].geometry.location
//				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	function buscarEnMapa(){
		var address = $("#direccion").val();
		map.setZoom(15);
		geocodeAddress(geocoder, map, address);
	}

	{{--@push('mapsInit')--}}
		{{--initPublishMap();--}}
	{{--@endpush--}}

	// ----------

	function tryLoginPub(){
		console.log("tryloginpub");

		$('#regPubBtnEntrar').html('<i class="fas fa-circle-notch fa-spin"></i> Enviando...');
		$('#regPubBtnEntrar').attr('disabled', 'disabled');

		var url = '{{ route('public.login') }}';
		var data = {
			email: $('#loginPubUsername').val(),
			password: $('#loginPubPassword').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
				step7 = true;
//				redirect('/cuenta');
				$('.login-form-pub').addClass('hide');
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				console.log("validation");
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
				console.log(e);
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				console.log(errors);
				var message = '';
				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message += errors[elem].join('<br>') + '<br>';
					} else {
						message += errors[elem];
					}

				}
				$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});
	}

	function tryRegisterPub() {

		$('#regPubBtnEnviar').html('<i class="fas fa-circle-notch fa-spin"></i> Enviando...');
		$('#regPubBtnEnviar').attr('disabled', 'disabled');

		var url = '{{ route('public.register') }}';
		var data = {
			email: $('#regPubUsername').val(),
			apellidos: $('#regPubApellidos').val(),
			nombres: $('#regPubNombres').val(),
			empresa: $('#regPubEmpresa').val(),
			nit: $('#regPubNit').val(),
			pais: $('#regPubPais').val(),
			telefono: $('#regPubTelefono').val(),
			password: $('#regPubPassword').val(),
			password_confirmation: $('#regPubPassword2').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				redirect('/cuenta');

				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
//				redirect('/cuenta');
				$('.register-form-pub').addClass('hide');
				step7 = true;
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				var message = '';

				var elements = [
					'email',
					'apellidos',
					'nombres',
					'pais',
					'telefono',
					'password',
					'password_confirmation'
				];

				for(var elem2 in elements){
					$('#regPubError_'+elements[elem2]).html('');
				}

				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message = errors[elem].join('<br>') + '<br>';
					} else {
						message = errors[elem];
					}
					$('#regPubError_'+elem).html('<span class="help-block"><strong>'+message+'</strong></span>')
				}
//					$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});

	}

	function activateLoginRegisterPub(action){
		$('.login-form-pub, .register-form-pub').addClass('hide');
		$('.'+action+'-form-pub').removeClass('hide');
	}

	function calc_price(e){
		var val = $('#days').val();
		var price = val * 4.5;
		if($('#chk_time_extended').prop('checked')){
			price += 12.05;
		}
		$('#newprice').html("<b>" + price + "Kr.</b>");
	}

	function formatRepoPubr (repo) {

		if (repo.loading) {
			return repo.text;
		}

		var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.text + "</div>";

		if (repo.description) {
			markup += "<div class='select2-result-repository__description'> " + repo.description + "</div>";
		}

		return markup;
	}

	function formatRepoSelectionPubr (repo) {
		return repo.full_name || getFullNamePubr(repo);
	}

	function getFullNamePubr(repo){
		if(repo.id != ''){
			return '<span class="flag-icon flag-icon-'+ repo.id +' "></span> &nbsp;&nbsp;' + '+' + codigosPais[repo.id] + ' ' + repo.text;
		} else {
			return repo.text;
		}

	}

</script>
@endpush