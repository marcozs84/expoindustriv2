@extends('frontend.layouts.default')
@push('css')
<link href="/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css" />
<style>

	label{
		/*display:inline-block !important;*/
		margin-bottom:0px !important;
		color: #4e4e4e;
	}
	/*input {*/
		/*display:block !important;*/
	/*}*/

	h4, h5 {
		display:block;
		clear:both;
		padding-left:17px;
	}

	h5{
		padding-bottom:10px;
		border-bottom:1px solid #eee;
		padding-left:10px;
	}

	.btnPrev, .btnNext {
		display:inline-block !important;
	}

	.step .desc, .step a{
		color:#ffffff;
	}
	.leftColumn{
		vertical-align:top;
	}
	.rightColumn{
		padding-left:10px !important;
	}

	.account-sidebar-cover{
		width:300px; overflow:hidden;
	}

	.account-body{
		margin-left:10px !important;
		padding-top:10px;
		padding-left:0px !important;
	}

	@media (min-width:991px) {
		.leftColumn {
			width:290px;
		}
		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:1200px) {
		.leftColumn {
			/*width:160px;*/
		}
	}
	@media (max-width:991px) {
		.progressPie {
			display:none;
		}
		.leftColumn {
			width:220px;
			/*display:none;*/
		}

		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:769px) {
		.leftColumn {
			/*width:190px;*/
			display:none;
		}

		.account-sidebar{
			display:none;
		}
		.account-body{
			margin-left:-20px !important;
			padding-top:10px;
		}
	}

	.specialsel2 .select2-container { width:100% !important; margin-top:0px !important; }



	/* --------------------------------------------------------- */

	.StripeElement {
		background-color: white;
		height: 35px;
		font-size:14px;
		padding: 10px 8px;
		margin-bottom:5px;
		border-radius: 4px;
		border: 1px solid #ccd0d4;
		box-shadow: 0 1px 3px 0 #e6ebf1;
		-webkit-transition: box-shadow 150ms ease;
		transition: box-shadow 150ms ease;
	}

	.StripeElement--focus {
		box-shadow: 0 1px 3px 0 #cfd7df;
	}

	.StripeElement--invalid {
		border-color: #fa755a;
	}

	.StripeElement--webkit-autofill {
		background-color: #fefde5 !important;
	}
</style>

<style>
	.progressPie {
		width: 200px;
		margin: 6px 20px 20px;
		/*display: inline-block;*/
		position: relative;
		text-align: center;
		vertical-align: top;

		transition: .3s ease;
	}
	.progressPie strong {
		position: absolute;
		top: 17px;
		left: 0;
		width: 100%;
		text-align: center;
		line-height: 45px;
		font-size: 20px;
	}
	.circleBig strong {
		font-size:30px;
		top:30px;
		line-height:40px;
	}
</style>
@endpush
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20 p-b-0 m-b-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">@lang('messages.home')</a></li>
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.publish')</li>
			</ul>
			<!-- END breadcrumb -->



			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar" style="padding-top: 12px;">
					<div class="account-sidebar-cover" style="background-image:url('/assets/img/e-commerce/cover/cover-14.jpg')">
						{{--<img src="/assets/img/e-commerce/cover/cover-14.jpg" style="max-height:none;" alt="" />--}}
					</div>
					<div class="account-sidebar-content hide" style="width:100%;">
						<h4>Su cuenta</h4>
						<p>
							Modifique una orden, o haga seguimiento a su cargamento y actualice su información personal.
						</p>
						<p>
							Todo lo que necesita en un solo lugar.  Por favor no dude en contactarnos si requiere asistencia.
						</p>
					</div>
					<div class="account-sidebar-content" style="width:100%;">

						<div class="progressPie" id="progressPie">
							<strong>asdf</strong>
						</div>

					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body" style="">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN col-6 -->
						@php
							$step = 0;
							$tabindex = 1;
						@endphp
						<div class="col-md-12">
							<table class="table-condensed" style="width:100%;">
								<tr class="step_{{ $step }}">
									<td class="leftColumn">

									</td>
									<td class="rightColumn">
										<h4>@lang('messages.new_announce')</h4>
										<hr>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								{{-- -------------------------------------------------- BASIC INFORMATION -------------------------------------------------- --}}
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step step_content active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.basic_information')</div>
													<div class="desc">@lang('messages.basic_information_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div class="step_content">
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.brand')
													<a href="javascript:;" id="errorDisplay_marca" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('marca', $marca, [
													'id' => 'marca',
													'class' => 'form-control',
													'placeholder' => __('messages.brand'),
													'onblur' => 'getModelos(this)',
													'tabindex' => $tabindex++
												]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.model')
													<a href="javascript:;" id="errorDisplay_modelo" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('modelo', $modelo, [
													'id' => 'modelo',
													'class' => 'form-control',
													'placeholder' => __('messages.model'),
													'tabindex' => $tabindex++
												]) !!}
											</div>

											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.machines_accesories')
													<a href="javascript:;" id="errorDisplay_type" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('type', [
													1 => __('messages.machines'),
													2 => __('messages.accesories'),
												], $type, [
													'id' => 'type',
													'class' => 'form-control',
													'tabindex' => $tabindex++
												]) !!}
											</div>

											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.used_new')
													<a href="javascript:;" id="errorDisplay_status" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('status', [
													1 => __('messages.new'),
													0 => __('messages.used'),
												], $status, [
													'id' => 'status',
													'class' => 'form-control',
													'tabindex' => $tabindex++
												]) !!}
											</div>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})" tabindex="{{ $tabindex++ }}">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								{{--<tr class="step_1" style="display: none;">--}}
									{{--<td class="leftColumn">--}}
									{{--<td><hr></td>--}}
								{{--</tr>--}}
								@php
									$step++;
								@endphp
								{{-- -------------------------------------------------- C A T E G O R Y -------------------------------------------------- --}}
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step step_content active"  style="display: none;">
											<a href="#" style="min-height: 170px; ">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.select_your_category')</div>
													<div class="desc">@lang('messages.select_your_category_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div class="step_content" style="display:none">
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.category')
													<a href="javascript:;" id="errorDisplay_categoria" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('categoria', $categorias->pluck('traduccion.'.App::getLocale(), 'id'), [$categoria], [
												    'id' => 'categoria',
												    'class' => 'form-control',
												    'placeholder' => __('messages.select_category'),
												    'onchange' => 'changeSubs(this)',
													'tabindex' => $tabindex++
												]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.subcategory')
													<a href="javascript:;" id="errorDisplay_subcategoria" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('subcategoria', [], [], [
												    'id' => 'subcategoria',
												    'class' => 'form-control',
												    'placeholder' => __('messages.select_category'),
													'tabindex' => $tabindex++
												]) !!}
											</div>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})" tabindex="{{ $tabindex++ }}">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								{{-- -------------------------------------------------- M A C H I N E  L O C A T I O N -------------------------------------------------- --}}
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step step_content active" style="display: none;">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.machine_location')</div>
													<div class="desc">@lang('messages.machine_location_descr', ['route' => route('public.dataProtectionPolicy')])</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div class="step_content" style="display: none;">
											<div class="form-group col-md-6 specialsel2">
												<label class="control-label">@lang('messages.country')
													<a href="javascript:;" id="errorDisplay_pais" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('pais', $global_paises, $global_country_abr, [
												    'id' => 'pais',
												    'class' => 'form-control',
												    'placeholder' => __('messages.country'),
													'tabindex' => $tabindex++,
												]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.city')
													<a href="javascript:;" id="errorDisplay_ciudad" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('ciudad', $ciudad, [
													'id' => 'ciudad',
													'class' => 'form-control',
													'placeholder' => __('messages.city'),
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.postal_code')
													<a href="javascript:;" id="errorDisplay_codigo_postal" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('codigo_postal', $pcode, [
													'id' => 'codigo_postal',
													'class' => 'form-control',
													'placeholder' => __('messages.postal_code'),
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.address')
													{{--( <a href="javascript:;" onclick="buscarEnMapa()">@lang('messages.findInMap')</a> )--}}
													<a href="javascript:;" id="errorDisplay_direccion" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::textarea('direccion', $direccion, [
													'id' => 'direccion',
													'class' => 'form-control',
													'rows' => 4, 'placeholder' => __('messages.address'),
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											{{--<div class="form-group col-md-6">--}}
												{{--<label class="control-label">@lang('messages.location')--}}
													{{--<a href="javascript:;" id="errorDisplay_ubicacion" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>--}}
												{{--</label>--}}
												{{--<style>--}}
													{{--#publish_map {--}}
														{{--height: 300px;--}}
														{{--width: 100%;--}}
													{{--}--}}
												{{--</style>--}}

												{{--<div id="publish_map"></div>--}}
											{{--</div>--}}
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})" tabindex="{{ $tabindex++ }}">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								{{-- -------------------------------------------------- T R A N S P O R T  I N F O R M A T I O N -------------------------------------------------- --}}
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step step_content active" style="display: none;">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.transport_info')</div>
													<div class="desc">@lang('messages.transport_info_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div class="step_content" style="display: none;">
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.longitude_mm')
													<a href="javascript:;" id="errorDisplay_longitud" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('longitud', (($longitud == 0) ? '' : $longitud), [
													'id' => 'longitud',
													'class' => 'form-control',
													'placeholder' => __('messages.longitude_mm'),
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.width_mm')
													<a href="javascript:;" id="errorDisplay_ancho" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('ancho', (($ancho == 0) ? '' : $ancho) , [
													'id' => 'ancho',
													'class' => 'form-control',
													'placeholder' => __('messages.width_mm'),
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.height_mm')
													<a href="javascript:;" id="errorDisplay_alto" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('alto', (($alto == 0) ? '' : $alto), [
													'id' => 'alto',
													'class' => 'form-control',
													'placeholder' => __('messages.height_mm'),
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.weight_brute')
													<a href="javascript:;" id="errorDisplay_peso_bruto_fix" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('peso_bruto_fix', (($peso == 0) ? '' : $peso), [
													'id' => 'peso_bruto_fix',
													'class' => 'form-control',
													'placeholder' => __('messages.weight_brute'),
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label" for="paletizado">@lang('messages.palletized')
													<a href="javascript:;" id="errorDisplay_paletizado" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::checkbox('paletizado', 'paletizado', $palet, [
													'id' => 'paletizado',
													'style' => 'font-size:15px; width:20px; height:20px; vertical-align:middle;',
													'tabindex' => $tabindex++
											    ]) !!}
											</div>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})" tabindex="{{ $tabindex++ }}">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								{{-- -------------------------------------------------- A N N O U N C E  D E T A I L -------------------------------------------------- --}}
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step step_content active" style="display:none;">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.announce_detail')</div>
													<div class="desc">@lang('messages.announce_detail_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div class="step_content" style="display: none;">
											<div id="formHolder" class="formHolder"></div>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})" tabindex="{{ $tabindex++ }}">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								{{-- -------------------------------------------------- U S E R  I N F O R M A T I O N -------------------------------------------------- --}}
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step step_content active" style="display: none;">
											<a href="#" style="min-height: 170px;">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.user_info')</div>
													<div class="desc">@lang('messages.user_info_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div class="step_content" style=" display: none;">
											<div id="uli" class="col-md-12 @if(!Auth::guard('clientes')->user()) hide @endif">
												@if(Auth::guard('clientes')->user())
													<h4 class="p-l-0"><span id="uli_nombre">{{ Auth::guard('clientes')->user()->Owner->Agenda->nombres_apellidos }}</span></h4>
													<b><span id="uli_email">{{ Auth::guard('clientes')->user()->username }}</span></b>
												@else
													<h4 class="p-l-0"><span id="uli_nombre"></span></h4>
													<b><span id="uli_email"></span></b>
												@endif
												<br>
												<small>@lang('messages.click_next')</small>
											</div>
											<div class="login-form-pub col-md-8 col-md-offset-2  @if(Auth::guard('clientes')->user()) hide @endif ">
												<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
													<h4 class="text-center m-t-0">@lang('messages.log_in')</h4>
												</div>
												<form id="formLoginPub" action="{{ route('public.login') }}" method="post" class="text-center">
													{{ csrf_field() }}
													<input type="text" name="email" id="loginPubUsername" class="login-input" placeholder="@lang('messages.e_mail')" tabindex="{{ $tabindex++ }}" >
													<input type="password" name="password" id="loginPubPassword" class="login-input" placeholder="@lang('messages.password')" tabindex="{{ $tabindex++ }}" >

													<div id="loginPubErrors"></div>

													<input type="checkbox" name="remember" id="frmRemember" tabindex="{{ $tabindex++ }}" /> @lang('messages.remember_me')
													<br>
													{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.log_in')">--}}
													<button type="submit" id="regPubBtnEntrar" form="formLoginPub" value="@lang('messages.log_in')" tabindex="{{ $tabindex++ }}" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.log_in')</button>
													{{--<a href="javascript:;" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" onclick="tryLogin()">Entrar</a>--}}
													{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Entrar">--}}
													<br>
													<a href="javascript:;" class="" onclick="launchLoginRegister('forgot')" style="font-size:12px; /*color:#333333;*/">@lang('messages.forgot_password')</a>

												</form>
												<hr />
												<p class="text-center">
													<a href="javascript:;" class="btn btn-warning btn-lg" onclick="activateLoginRegisterPub('register')" style="font-size:18px; font-weight:bold; /*color:#333333;*/">@lang('messages.create_account')</a>
												</p>
											</div>
											<div class="register-form-pub hide col-md-8 col-md-offset-2">
												<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
													<h4 class="text-center m-t-0">@lang('messages.register')</h4>
												</div>
												<form id="formRegisterPub"  action="{{ route('public.register') }}" method="post" class="text-center">
													<input type="text" name="email" id="regPubUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
													<div id="regPubError_email"></div>

													<input type="text" name="apellidos" id="regPubApellidos" class="login-input" placeholder="@lang('messages.lastname')" >
													<div id="regPubError_apellidos"></div>

													<input type="text" name="nombres" id="regPubNombres" class="login-input" placeholder="@lang('messages.firstname')" >
													<div id="regPubError_nombres"></div>

													<input type="text" name="empresa" id="regPubEmpresa" class="login-input" placeholder="@lang('messages.company')" >
													<div id="regPubError_empresa"></div>

													<input type="text" name="nit" id="regPubNit" class="login-input" placeholder="@lang('messages.company_nit')" >
													<div id="regPubError_nit"></div>

													{{--<select name="pais" id="regPubPais" class="login-input">--}}
													{{--<option value="">@lang('messages.select_country')</option>--}}
													{{--</select>--}}
													{!! Form::select('pais', $global_paises, $global_country_abr, [
																'id' => 'regPubPais',
																'class' => 'login-input',
																'placeholder' => __('messages.select_country')
													]); !!}
													<div id="regPubError_pais"></div>

													<input type="text" name="telefono" id="regPubTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
													<div id="regPubError_telefono"></div>

													<input type="password" name="password" id="regPubPassword" class="login-input" placeholder="@lang('messages.password')" >
													<div id="regPubError_password"></div>

													<input type="password" name="password_confirmation" id="regPubPassword2" class="login-input" placeholder="@lang('messages.password_confirmation')" >
													<div id="regPubError_password_confirm"></div>


													<br>
													{!! Form::checkbox('legalterms', 'legalterms', null, [
														'id' => 'legalterms',
														'style' => 'font-size:15px; margin:0px; vertical-align:middle;'
													]) !!}
													<label class="control-label " for="legalterms">
														@lang('messages.accept_terms', ['link' => '<a href="javascript:;" onclick="modalTerms()" style="font-weight:bold;">'.__('messages.legal_terms').'</a>'])
													</label>

													{{--<input type="submit" id="regPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
													<button type="submit" id="regPubBtnEnviar" form="formRegisterPub" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
													{{--<a href="javascript:;" id="regPubBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterPub').submit();">@lang('messages.send')</a>--}}
												</form>
												<hr />
												<p class="text-center">
													<a href="javascript:;" class="" onclick="activateLoginRegisterPub('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
												</p>
											</div>

											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})" tabindex="{{ $tabindex++ }}">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								{{-- -------------------------------------------------- P A Y M E N T  O P T I O N S -------------------------------------------------- --}}
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step step_content active @if($np) hide @endif np_class" style="display: none;">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.payment_options')</div>
													<div class="desc">@lang('messages.payment_options_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div class="step_content" style="min-height: 170px; display: none;">
											<hr>
											<h4 class="@if($np) hide @endif np_class">@lang('messages.payment_options')</h4>
											<div style="padding:17px;" class="@if($np) hide @endif np_class">
												@lang('messages.payment_announce_message', ['price' => '2,50Kr.'])
												<br><br>
												<input type="number" name="days" id="days" class="form-control" value="180" min="180" max="600" style="width:80px; display:inline;" onchange="calc_price()"> @lang('messages.days')
												 = <span id="newprice"><b>{{ 2.5 * 180 }} Kr.</b></span><b> *@lang('messages.includes_taxes')</b>
												<br><br>
												<div class="alert alert-info fade show in">
													{{--<h5 class="p-l-0">@lang('messages.time_extended_title')</h5>--}}
													{{--<p>--}}
														{{--@lang('messages.time_extended_description')--}}
													{{--</p>--}}
													<p>
														<input id="chk_time_extended" type="checkbox">
														@lang('messages.time_extended_checkbox', ['price' => '25'])
													</p>
												</div>
											</div>

											<ul class="nav nav-tabs nav-justified nav-justified-mobile nav-condensed @if($np) hide @endif np_class">
												<li class="active"><a href="#default-tab-1" style="min-height:45px;" data-toggle="tab" rel="stripe"><i class="fab fa-cc-stripe"></i> @lang('messages.payment_creditcard')</a></li>
												<li class=""><a href="#default-tab-2" style="min-height:45px;" data-toggle="tab" rel="faktura"><i class="fab fa-cc-stripe"></i> @lang('messages.payment_faktura')</a></li>
											</ul>
											<div class="tab-content @if($np) hide @endif np_class">
											    <div class="tab-pane fade active in" id="default-tab-1" style="background-color:#f0f3f4;">

												    <div style="margin:10px;">
													    <form action="" method="post" id="payment-form">
														    <div class="form-row">
															    <div class="row">
																    <div class="pull-left" style="">
																	    <p style="margin:20px 20px;">@lang('messages.payment_creditcard_descr')</p>
																    </div>

																    <div class="pull-right p-r-30">
																	    <img style="height:70px;" src="/assets/img/e-commerce/visa.png" alt="Visa">
																	    <img style="height:70px;" src="/assets/img/e-commerce/mastercard.png" alt="MasterCard">
																    </div>
															    </div>

															    <div class="card-message hide"></div>
															    {{--<label for="card-element">--}}
																    {{--Número de tarjeta--}}
															    {{--</label>--}}
															    {{--<div id="card-element">--}}
																    {{--<!-- A Stripe Element will be inserted here. -->--}}
															    {{--</div>--}}

															    <div class="cardsHolder">

															    </div>

															    <div class="form-group col-md-12 card_data hide">
																    <label class="control-label" for="card-element">
																	    @lang('messages.card_number')
																    </label>
																    <div id="card-number"></div>
															    </div>

															    <div class="form-group col-md-6 card_data hide">
																    <label class="control-labFecha de expiraciónel" for="card-element">
																	    @lang('messages.expiration_date')
																    </label>
																    <div id="card-expiry"></div>
															    </div>
															    <div class="form-group col-md-6 card_data hide">
																    <label class="control-label" for="card-element">
																	    @lang('messages.security_code')
																    </label>
																    <div id="card-cvc"></div>
															    </div>
														    </div>
														    <br>
														    {{--<button id="submitPayment" class="btn btn-primary btn-inverse"><i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')</button>--}}
													    </form>
												    </div>

												    <div style="clear:both;"></div>
											    </div>

												<div class="tab-pane fade text-center" id="default-tab-2" style="background-color:#f0f3f4;">
													<div style="margin:10px;">
														<div class="form-row">
															<div class="row">
																<div class="pull-left" style="">
																	<p style="margin:20px 20px;">@lang('messages.payment_faktura_descr')</p>
																</div>
															</div>
														</div>
														<div style="clear:both;"></div>
													</div>
												</div>
											</div>

											<div class="panel panel-primary @if($np) hide @endif np_class" style="clear: both;">
												<div class="panel-body " style="background-color:#f0f3f4;">
													{{--Una ves vendida la maquinaria en ExpoIndustri, recibirá un documento sellado de ADUANA que certifica que la maquinaria fue exportada fuera de país de origen.--}}
													{!! Form::checkbox('legalterms', 'legalterms', $palet, [
														'id' => 'legalterms',
														'style' => 'font-size:15px; width:20px; height:20px; vertical-align:middle;'
												    ]) !!}
													<label class="control-label " for="legalterms">
														@lang('messages.accept_terms', ['link' => '<a href="javascript:;" onclick="modalTerms()" style="font-weight:bold;">'.__('messages.legal_terms').'</a>'])
													</label>

													<br>
													{!! Form::checkbox('goodfaith', 'legalterms', $palet, [
														'id' => 'goodfaith',
														'style' => 'font-size:15px; width:20px; height:20px; vertical-align:middle;'
												    ]) !!}
													<label class="control-label " for="goodfaith" style="/*width:700px;*/">
														@lang('messages.goodfaith')
													</label>
												</div>
											</div>
											<BR>
											<div class="form-group col-md-12">
												<div class="text-center">
													<a href="javascript:;" id="btnPublishSendFormNoPay" style="padding:13px; font-size:20px;" class="btn btn-danger @if(!$np) hide @endif np_class_not" onclick="crearPublicacion('none')"><i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')</a>
													<a href="javascript:;" id="btnPublishSendForm" style="padding:13px; font-size:20px;" class="btn btn-inverse @if($np) hide @endif np_class" onclick="sendForm()"><i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')</a>
												</div>
											</div>
											{{--<div class="form-group col-md-12">--}}
												{{--<div class="text-right">--}}
													{{--<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>--}}
													{{--<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>--}}
												{{--</div>--}}
											{{--</div>--}}
										</div>
									</td>
								</tr>
							</table>
						</div>
						<!-- END col-6 -->

					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->

	<style>
		h5 {
			margin-left:0px;
			/*margin-top:25px;*/
			font-size:15px;
		}
		.section-terms b{
			color:#333333;
		}
	</style>

	<div class="modal fade" id="modal-announce">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title text-center " id="title_banner">@lang('messages.legal_terms_conditions')</h4>
					{{--<a class="close" data-dismiss="modal" aria-hidden="true">×</a>--}}
				</div>
				<div class="modal-body">

					<div style="width:100%; height:400px; overflow:auto;">
						@include('frontend.includes.terms_conditions_'.App::getLocale())
					</div>

					<div style="width:100%; clear:both;"></div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('scripts')
{{--<script src="https://js.stripe.com/v3/"></script>--}}

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>--}}
<script>




	/**
	 * Ref.: http://kottenator.github.io/jquery-circle-progress/
	 * */
	$.getScript('/assets/plugins/circle-progress.min.js').done(function() {
		/**
		 *Exampe from https://kottenator.github.io/jquery-circle-progress/
		 */
		var progressBarOptions = {
			startAngle: -1.55,
			size: 80,
			value: 0.10,
			fill: {
				color: '#ffffff'
			}
		}

		$('#progressPie').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
//			$(this).find('strong').text(Math.round(stepValue * progress) + "%");
			text = String(stepValue.toFixed(2)).substr(2);

			var extra = '';
			if( text == '00') {
				text = '100';
				extra = '<br><i class="fa fa-check"></i>';
			}
			$(this).find('strong').html(text + "%" + extra);
		});

	});

	var stepY = 0;
	function refreshProgress(stepV) {

		stepV = parseInt(stepV, 10);
		var prevStep = stepV - 1;
		var val = ((100/7) * stepV) / 100;

		var progressPie = $('#progressPie');
		var startValue = progressPie.circleProgress('value');

		var sizePie = 90;
		if(stepV == 7) {
			sizePie = 130;
			$('.progressPie').addClass('circleBig');
		}

		progressPie.circleProgress({
			value : val,
			size: sizePie,
			animationStartValue : startValue,
			fill: {
				color: '#ffffff'
			}
		});

		offsetTop = $('tr.step_'+stepV).offset().top;
		if( stepV == 1 ) {
			offsetTop = 130;
		} else if( stepV == 2 ) {
			offsetTop += 20;
		} else if( stepV == 3 ) {
			offsetTop += 70;
		} else if( stepV == 4 ) {
			offsetTop += 40;
		} else if( stepV == 5 ) {
			offsetTop += 60;
		}


//		stepY += 100;
		stepY = offsetTop - 130;
		$('.progressPie').animate({"top": stepY+"px"}, 400);
//		$('.step_' + prevStep).find('.step').fadeOut();

	}

	var elements = '';
	var ccard = '';
	var ncard = '';
	var cecard = '';
	var cvcard = '';
	var stripe = '';
	$.getScript('https://js.stripe.com/v3/').done(function() {
		// Create a Stripe client.
//		stripe = Stripe('pk_test_DfIQrd547tyZOojRBU6cD5YH');    // MZ
//		stripe = Stripe('pk_test_jo9qYMCsfdSwFBb1N4ay8vdR');    // JB
		stripe = Stripe('{{ env('STRIPE_KEY_PK') }}');    // JB

		// Create an instance of Elements.
		elements = stripe.elements();

		// Custom styling can be passed to options when creating an Element.
		// (Note that this demo uses a wider set of styles than the guide below.)
		var style = {
			base: {
				color: '#32325d',
				lineHeight: '18px',
				fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing: 'antialiased',
				fontSize: '16px',
				'::placeholder': {
					color: '#aab7c4'
				}
			},
			invalid: {
				color: '#fa755a',
				iconColor: '#fa755a'
			}
		};

		// Create an instance of the card Element.
//		ccard = elements.create('card', {style: style});
		ncard = elements.create('cardNumber', {style: style});
		cecard = elements.create('cardExpiry', {style: style});
		cvcard = elements.create('cardCvc', {style: style});

		// Add an instance of the card Element into the `card-element` <div>.
//		ccard.mount('#card-element');
		ncard.mount('#card-number');
		cecard.mount('#card-expiry');
		cvcard.mount('#card-cvc');
	});

	function stripeTokenHandler(token) {
		// Insert the token ID into the form so it gets submitted to the server
		var form = document.getElementById('payment-form');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);

		// Submit the form
		form.submit();
	}

	// ----------

//	var current_payment_type = 'dibs';
	@if($tipo_pago == 'subscripcion')
	var current_payment_type = 'subscripcion';
	@else
	var current_payment_type = 'stripe';
	@endif

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		current_payment_type = $(e.target).prop('rel');
	})

	// ----------
	$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
		initForm();
	});
	$.getScript('https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js').done(function(){
//		$('input').iCheck({
//			checkboxClass: 'icheckbox_minimal',
//			radioClass: 'iradio_minimal',
//			increaseArea: '20%' // optional
//		});
	});
	var stepNum = 1;
	var cats = {!! json_encode($categorias) !!};

	var codigosPais = {!! json_encode($global_paisesCodigo) !!};

	$(document).ready(function(){
//		initForm();

		@if($categoria != 0)
			changeSubs(document.getElementById('categoria'));
		@endif

//		$( "#payment-form" ).submit(function( event ) {
//			if($('#legalterms').prop('checked')) {
//				sendForm(document.getElementById('submitPayment'));
//			} else {
//				alert('You must read and accept the Legal terms and conditions before submitting your payment.')
//
//			}
//			event.preventDefault();
//		});

		$('#pais, #regPubPais').select2({
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			templateResult: formatRepoPubr,
			templateSelection: formatRepoSelectionPubr
		});

		$( "#formLoginPub" ).submit(function( event ) {
			tryLoginPub();
			event.preventDefault();
		});

		$( "#formRegisterPub" ).submit(function( event ) {
			tryRegisterPub();
			event.preventDefault();
		});

		$('#chk_time_extended').on('change', function(e) {
			calc_price(undefined);
		});
	});

	var step6 = false;
	var step7 = false;
	var currentStep = 1;

	@if(Auth::guard('clientes')->user())
	var customerToken = '{{ Auth::guard('clientes')->user()->id }}';
	@else
	var customerToken = '';
	@endif


	function showStep(n){
		console.log("step " + n);
		var validation1 = true;
		if(n === 2){
			var inps = ['marca', 'modelo'];

			$.each(inps, function(v, k){
				if($('#'+k).val() === ''){
					$('#'+k).css({'border':'1px solid #ff0000'});
					$('#errorDisplay_'+k).attr('data-toggle', 'tooltip')
						.attr('data-title', '@lang('messages.required')')
						.removeClass('hide');
					validation1 = false;
				} else {
					$('#'+k).css({'border':'1px solid #ccd0d4'});
					$('#errorDisplay_'+k).addClass('hide');
				}
			});
			$("[data-toggle=tooltip]").tooltip();
			if(validation1 === false){
				return false;
			}
			var data = {
				'marca' : $('#marca').val(),
				'modelo' : $('#modelo').val(),
				'type' : $('#type').val(),
				'status' : $('#status').val(),
			};
			updateSession(data);
		}
		if(n === 3 || n === 4 || n === 5){

			if( n=== 3){
				inps = ['categoria', 'subcategoria'];
			}
			if( n=== 4){
				inps = ['pais', 'ciudad', 'codigo_postal', 'direccion'/*, 'ubicacion'*/];
			}
			if( n=== 5){
				inps = ['longitud', 'ancho', 'alto', 'peso_bruto_fix'];
			}
			validation1 = true;
			$.each(inps, function(v, k){
				if($('#'+k).val() === '' || $('#'+k).val() === undefined || $('#'+k).val() === null){
					$('#'+k).css({'border':'1px solid #ff0000'});
					$('#errorDisplay_'+k).attr('data-toggle', 'tooltip')
						.attr('data-title', '@lang('messages.required')')
						.removeClass('hide');
					validation1 = false;
				} else {
					$('#'+k).css({'border':'1px solid #ccd0d4'});
					$('#errorDisplay_'+k).addClass('hide');
				}
			});
			$("[data-toggle=tooltip]").tooltip();
			if(validation1 === false){
				return false;
			}

			if(n === 3){
				data = {
					'categoria' : $('#categoria').val(),
					'subcat' : $('#subcategoria').val()
				};
			}
			if(n === 4){
				data = {
					'pais' : $('#pais').val(),
					'ciudad' : $('#ciudad').val(),
					'pcode' : $('#codigo_postal').val(),
					'direccion' : $('#direccion').val(),
//					'ubicacion' : $('#ubicacion').val(),
				};
			}
			if(n === 5){
				data = {
					'longitud' : $('#longitud').val(),
					'ancho' : $('#ancho').val(),
					'alto' : $('#alto').val(),
					'peso' : $('#peso_bruto_fix').val(),
					'palet' : ($('#paletizado').prop('checked')) ? 1 : 0,
				};
			}
			updateSession(data);

			if(n === 5) {
				var scat = $('#subcategoria').val();
				if(scat === '' || scat < 1){
					return false;
				}
				if(currentStep < 5){
					getForm(scat);
				} else {
					step6 = false;
				}

			}
		}

		if(n === 6){
			if(step6 !== true){
				validateForm(function(){
					step6 = true;
					showStep(6);
				});
				return false;
			}
		}

		@if(Auth::guard('clientes')->user())
		step7 = true;
		@endif

		if(n === 7){
			if(step7 !== true){
				alert("You must log in first");
				return false;
			}
			loadCustomerCards();
		}


		for(var i = 1 ; i <= 6 ; i++){
			if(i < n){
				$('.step_'+i+' input, .step_'+i+' number, .step_'+i+' select, .step_'+i+' textarea').each(function(e){
					$(this).attr('disabled','disabled');
				});
				$('.step_'+i+' .btnNext, .step_'+i+' .btnPrev').each(function(e){
					$(this).removeAttr('onclick');
					$(this).attr('disabled','disabled');
				});

			} else {
				$('.step_'+i+' input, .step_'+i+' number, .step_'+i+' select, .step_'+i+' select').each(function(e){
					$(this).removeAttr('disabled');
				});
				$('.step_'+i+' .btnPrev').each(function(e){
					$(this).removeAttr('disabled');
					$(this).attr('onclick', 'showStep('+(i-1)+')');
				});
				$('.step_'+i+' .btnNext').each(function(e){
					$(this).removeAttr('disabled');
					$(this).attr('onclick', 'showStep('+(i+1)+')');
				});
			}

		}
//		$('tr.step_'+(n+1)).hide('slow');
//		$('tr.step_'+n).show('slow');
		$('tr.step_'+ (n+1) + ' td .step_content').slideUp();
		$('tr.step_'+ n + ' td .step_content').slideDown();

		$('html, body').animate({
			scrollTop: $('tr.step_'+n).offset().top
		}, 1000);

		refreshProgress(n);

		currentStep = n;
	}

	function changeSubs(e){
		cat = $(e).val();

		$('#subcategoria').html('');
		$.each(cats, function(k,v){
			if(v.id == cat){
				subs = v.subcategorias;
				$('#subcategoria').append($('<option>', {
					'value': '',
					'selected': 'selected',
				}).html('@lang("messages.select_subcategory")'));
				$.each(subs, function(k1, v1){
					$('#subcategoria').append($('<option>', {
						'value': v1.id
//					}).html(v1.nombre));
					}).html(v1.traduccion.{{ App::getLocale() }}));
				});
			}
		});

		@if($subcat != 0)
			$('#subcategoria').val({{ $subcat }});
		@endif
	}

//	function loadForm(e){
//		scat = $(e).val();
//		getForm(scat);
//	}
//	getForm(12);

	function modalTerms() {
		$('#modal-announce').modal();
	}

	function getForm(scat){

		var url = '{{ route('public.publishForm', [0]) }}';
		url = url.replace(0, scat);
		var data = {
			idCategoria: scat,
		};

		$('#formHolder').html("@lang('messages.loading')...");

		$('#formHolder').load(url, function(response, status, xhr){
			if(xhr.status == 500){
				response = response.replace('position: fixed;', '');
				$(this).html(response);
			}

			if($('#medida_de_transporte_largo_x_ancho_x_alto').length > 0){
				var dims = $('#longitud').val() + ' x ' + $('#ancho').val() + ' x ' + $('#alto').val();
				$('#medida_de_transporte_largo_x_ancho_x_alto').val(dims);
			}
			if($('#peso_bruto').length > 0){
				var pbr = $('#peso_bruto_fix').val();
				$('#peso_bruto').val(pbr);
			}



			{{--console.log($('#dropzone'));--}}
			{{--$('#dropzone').dropzone({--}}
				{{--url: '{{ route('public.publishUpload') }}'--}}
			{{--});--}}

//			var myDropzone = new Dropzone("div#dropzone", { url: "/file/post"});
		});
	}

	function getModelos(e){
		var marca = $(e).val();
		var url = '{{ route('public.getModelos') }}';
		var data = {
			marca : marca
		};

		ajaxPost(url, data, {
			onSuccess: function(data){
				if(data.data.length > 0){
					$('#modelo').autocomplete({
						source: data.data
					});
				}
			}
		});

	}

	function initForm(){
		var availableTags = {!! json_encode($marcas) !!};
		$('#marca').autocomplete({
			source: availableTags
		});
	}

	function updateSession(data){
		var url = '{{ route('public.updateSession') }}';

		ajaxPost(url, data, {
			onValidationOverride: true,
			onSuccess: function(data){
			},
			onFailure: function(data){
			},
			onDone: function(data){
			},
		});
	}

	var map = null;
	var geocoder = null;
	var marker = null;
	function initPublishMap() {
		var uluru = {lat: -25.363, lng: 131.044};
		map = new google.maps.Map(document.getElementById('publish_map'), {
			zoom: 4,
			center: uluru,
//			query: 'Bolivia'
		});

		geocoder = new google.maps.Geocoder();

		var address = $("#pais option:selected").text();
		geocodeAddress(geocoder, map, address);

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng, map);
		});

//		var marker = new google.maps.Marker({
//			position: uluru,
//			map: map
//		});
	}

	function placeMarker(location, map) {
		if (marker === null){
			marker = new google.maps.Marker({
				position: location,
				map: map
			});
		} else {
			marker.setPosition(location);
		}

//		marker = new google.maps.Marker({
//			position: location,
//			map: map
//		});
		map.panTo(location);
	}
	function geocodeAddress(geocoder, resultsMap, address) {
//		var address = 'Bolivia';
//		var address = $("#pais option:selected").text();
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);
//				var marker = new google.maps.Marker({
//					map: resultsMap,
//					position: results[0].geometry.location
//				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	function buscarEnMapa(){
		var address = $("#direccion").val();
		map.setZoom(15);
		geocodeAddress(geocoder, map, address);
	}

	{{--@push('mapsInit')--}}
		{{--initPublishMap();--}}
	{{--@endpush--}}

	// ----------

	function tryLoginPub(){

		$('#regPubBtnEntrar').html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
		$('#regPubBtnEntrar').attr('disabled', 'disabled');

		var url = '{{ route('public.login') }}';
		var data = {
			email: $('#loginPubUsername').val(),
			password: $('#loginPubPassword').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
				step7 = true;
				customerToken = data.data.id;
//				redirect('/cuenta');
				$('.login-form-pub').addClass('hide');

				if(data.subs_activas === 1) {
					$('.np_class').addClass('hide');
					$('.np_class_not').removeClass('hide');
					current_payment_type = 'subscripcion';
				}
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				var message = '';
				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message += errors[elem].join('<br>') + '<br>';
					} else {
						message += errors[elem];
					}

				}
				$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});
	}

	function tryRegisterPub() {

		if(!$('#legalterms').prop('checked')) {
			swal('', "@lang('messages.please_accept_terms')", "error");
			return false;
		}

		$('#regPubBtnEnviar').html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
		$('#regPubBtnEnviar').attr('disabled', 'disabled');

		var url = '{{ route('public.register') }}';
		var data = {
			email: $('#regPubUsername').val(),
			apellidos: $('#regPubApellidos').val(),
			nombres: $('#regPubNombres').val(),
			empresa: $('#regPubEmpresa').val(),
			nit: $('#regPubNit').val(),
			pais: $('#regPubPais').val(),
			telefono: $('#regPubTelefono').val(),
			password: $('#regPubPassword').val(),
			password_confirmation: $('#regPubPassword2').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				redirect('/cuenta');

				customerToken = data.data.id;

				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
//				redirect('/cuenta');
				$('.register-form-pub').addClass('hide');
				step7 = true;
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				var message = '';

				var elements = [
					'email',
					'apellidos',
					'nombres',
					'pais',
					'telefono',
					'password',
					'password_confirmation'
				];

				for(var elem2 in elements){
					$('#regPubError_'+elements[elem2]).html('');
				}

				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message = errors[elem].join('<br>') + '<br>';
					} else {
						message = errors[elem];
					}

					$('#regPubError_'+elem).html('<span class="help-block">'+message+'</span>')
				}
//					$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});

	}

	function activateLoginRegisterPub(action){
		$('.login-form-pub, .register-form-pub').addClass('hide');
		$('.'+action+'-form-pub').removeClass('hide');
	}

	function calc_price(e){
		var val = $('#days').val();
		if(val < 180){
			$('#days').val(180);
			val = 180;
			swal('', 'You must choose at least 180 days for your announcement.', 'warning');
		}
		var price = val * 2.5;
		if($('#chk_time_extended').prop('checked')){
			price += 25;
		}
		$('#newprice').html("<b>" + price + "Kr.</b>");
	}

	function formatRepoPubr (repo) {

		if (repo.loading) {
			return repo.text;
		}

		var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.text + "</div>";

		if (repo.description) {
			markup += "<div class='select2-result-repository__description'> " + repo.description + "</div>";
		}

		return markup;
	}

	function formatRepoSelectionPubr (repo) {
		return repo.full_name || getFullNamePubr(repo);
	}

	function getFullNamePubr(repo){
		if(repo.id != ''){
			return '<span class="flag-icon flag-icon-'+ repo.id +' "></span> &nbsp;&nbsp;' + '+' + codigosPais[repo.id] + ' ' + repo.text;
		} else {
			return repo.text;
		}

	}

	function loadCustomerCards() {
		var url = '{{ route('public.getCustomerCards') }}';
		var data = {
			customerToken: customerToken
		};

		$('.cardsHolder').html('@lang('messages.loading') @lang('messages.payment_options')...');

		ajaxPost(url, data, {
			onSuccess: function(data) {
				data = data.data;
				if(data.length === 0) {
					$('.cardsHolder').html('');
					$('.card_data').removeClass('hide');
				} else if(data.length > 0) {
					var counter = 0;
					$('.cardsHolder').html('');
					$.each(data, function(k,v) {
						var radio = $('<input>', {
							'id' : 'card_' + k,
							'type' : 'radio',
							'name' : 'card_id',
							'class' : 'm-r-10',
							'onclick' : 'onCard(\''+ v.id +'\')',
							'value' : v.id
						});
						if(counter === 0) {
							radio.prop('checked', 'checked');
							onCard(v.id);
						}

						var label = $('<label>', {
							'style' : '',
							'for' : 'card_' + k
						}).html(' xxxx&nbsp;&nbsp;xxxx&nbsp;&nbsp;xxxx&nbsp;&nbsp;' + v.last4);

						$('.cardsHolder').append(radio);
						$('.cardsHolder').append(label);
						$('.cardsHolder').append('<br>');
						counter++;
					});

					var radio = $('<input>', {
						'id' : 'card_new',
						'type' : 'radio',
						'name' : 'card_id',
						'class' : 'm-r-10',
						'onclick' : 'onCard(0)',
						'value' : ''
					});

					var label = $('<label>', {
						'style' : '',
						'for' : 'card_new'
					}).html('@lang('messages.use_different_card')');

					$('.cardsHolder').append(radio);
					$('.cardsHolder').append(label);
					$('.cardsHolder').append('<br><br>');
				}
			}
		});
	}

	var current_card = '';

	function onCard(id) {
		if(id === 0) {
			$('.card_data').removeClass('hide');
			current_card = '';
		} else {
			current_card = id;
			$('.card_data').addClass('hide');
		}
	}
</script>
@endpush