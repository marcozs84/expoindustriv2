@extends('frontend.layouts.default')
@push('css')
<style>
	/* -------------------------------
   12.0 Pricing Setting
------------------------------- */



	.btn-block {
		padding-top: 15px;
		padding-bottom: 15px;
		padding-left: 12px;
		padding-right: 12px;
	}

	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}

	.checkout-footer {
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 1);  /* blue */
	}

</style>
@endpush

@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -360px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.plan_'.$plan)</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN checkout-body -->
			<div class="checkout-body">
				<div class="table-responsive">
					<div>

						<div id="uli" class="col-md-12 @if(!Auth::guard('clientes')->user()) hide @endif">
							@if(Auth::guard('clientes')->user())
								<h4 class="p-l-0"><span id="uli_nombre">{{ Auth::guard('clientes')->user()->Owner->Agenda->nombres_apellidos }}</span></h4>
								<b><span id="uli_email">{{ Auth::guard('clientes')->user()->username }}</span></b>
							@else
								<h4 class="p-l-0"><span id="uli_nombre"></span></h4>
								<b><span id="uli_email"></span></b>
							@endif
							<br>
							<small>@lang('messages.click_next')</small>
						</div>
						<div class="login-form-pub col-md-8 col-md-offset-2  @if(Auth::guard('clientes')->user()) hide @endif ">
							<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
								<h4 class="text-center m-t-0">@lang('messages.log_in')</h4>
							</div>
							<form id="formLoginPub" action="{{ route('public.login') }}" method="post" class="text-center">
								{{ csrf_field() }}
								<input type="text" name="email" id="loginPubUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
								<input type="password" name="password" id="loginPubPassword" class="login-input" placeholder="@lang('messages.password')" >

								<div id="loginPubErrors"></div>

								<input type="checkbox" name="remember" id="frmRemember" /> @lang('messages.remember_me')
								<br>
								{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.log_in')">--}}
								<button type="submit" id="regPubBtnEntrar" form="formLoginPub" value="@lang('messages.log_in')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.log_in')</button>
								{{--<a href="javascript:;" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" onclick="tryLogin()">Entrar</a>--}}
								{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Entrar">--}}
								<br>
								<a href="javascript:;" class="" onclick="launchLoginRegister('forgot')" style="font-size:12px; /*color:#333333;*/">@lang('messages.forgot_password')</a>

							</form>
							<hr />
							<p class="text-center">
								{{--<a href="javascript:;" class="btn btn-danger" onclick="openLoginRegister('register')" style="font-size:14px; font-weight:bold; width:50%; padding:10px; /*color:#333333;*/">@lang('messages.create_account')</a><br><br>--}}
								{{--<a href="javascript:;" class="btn btn-info" onclick="openLoginRegister('register')" style="font-size:14px; font-weight:bold; width:50%; padding:10px; /*color:#333333;*/">@lang('messages.create_account')</a><br><br>--}}
								{{--<a href="javascript:;" class="btn btn-warning" onclick="openLoginRegister('register')" style="font-size:14px; font-weight:bold; width:50%; padding:10px; /*color:#333333;*/">@lang('messages.create_account')</a><br><br>--}}
								{{--<a href="javascript:;" class="btn btn-primary" onclick="openLoginRegister('register')" style="font-size:14px; font-weight:bold; padding:10px 20px; /*color:#333333;*/">@lang('messages.create_account')</a><br><br>--}}
								<a href="javascript:;" class="btn btn-primary" onclick="activateLoginRegisterPub('register')" style="font-size:14px; font-weight:bold; padding:10px 20px; /*color:#333333;*/">@lang('messages.create_account')</a><br><br>
								{{--<a href="javascript:;" class="btn btn-inverse" onclick="openLoginRegister('register')" style="font-size:14px; font-weight:bold; width:50%; padding:10px; /*color:#333333;*/">@lang('messages.create_account')</a><br><br>--}}
								{{--<a href="javascript:;" class="btn btn-theme btn-primary" onclick="openLoginRegister('register')" style="font-size:14px; font-weight:bold; width:50%; padding:10px; background-color:#D9C400; /*color:#333333;*/">@lang('messages.create_account')</a><br><br>--}}
							</p>
						</div>
						<div class="register-form-pub hide col-md-8 col-md-offset-2">
							<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
								<h4 class="text-center m-t-0">@lang('messages.register')</h4>
							</div>
							<form id="formRegisterPub"  action="{{ route('public.register') }}" method="post" class="text-center">
								{{ csrf_field() }}
								<input type="text" name="email" id="regPubUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
								<div id="regPubError_email"></div>

								<input type="text" name="apellidos" id="regPubApellidos" class="login-input" placeholder="@lang('messages.lastname')" >
								<div id="regPubError_apellidos"></div>

								<input type="text" name="nombres" id="regPubNombres" class="login-input" placeholder="@lang('messages.firstname')" >
								<div id="regPubError_nombres"></div>

								<input type="text" name="empresa" id="regPubEmpresa" class="login-input" placeholder="@lang('messages.company')" >
								<div id="regPubError_empresa"></div>

								<input type="text" name="nit" id="regPubNit" class="login-input" placeholder="@lang('messages.company_nit')" >
								<div id="regPubError_nit"></div>

								{{--<select name="pais" id="regPubPais" class="login-input">--}}
								{{--<option value="">@lang('messages.select_country')</option>--}}
								{{--</select>--}}
								{!! Form::select('pais', $global_paises, $global_country_abr, [
											'id' => 'regPubPais',
											'class' => 'login-input',
											'placeholder' => __('messages.select_country')
								]); !!}
								<div id="regPubError_pais"></div>

								<input type="text" name="telefono" id="regPubTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
								<div id="regPubError_telefono"></div>

								<input type="password" name="password" id="regPubPassword" class="login-input" placeholder="@lang('messages.password')" >
								<div id="regPubError_password"></div>

								<input type="password" name="password_confirmation" id="regPubPassword2" class="login-input" placeholder="@lang('messages.password_confirmation')" >
								<div id="regPubError_password_confirm"></div>

								{{--<input type="submit" id="regPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
								<button type="submit" id="regPubBtnEnviar" form="formRegisterPub" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
								{{--<a href="javascript:;" id="regPubBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterPub').submit();">@lang('messages.send')</a>--}}
							</form>
							<hr />
							<p class="text-center">
								{{--<a href="javascript:;" class="btn btn-primary" onclick="openLoginRegister('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>--}}
								<a href="javascript:;" class="btn btn-primary" onclick="activateLoginRegisterPub('login')" style="font-size:14px; font-weight:bold; padding:10px 20px; /*color:#333333;*/">@lang('messages.have_account')</a>
							</p>
						</div>

					</div>

				</div>
			</div>
			<!-- END checkout-body -->
			<!-- BEGIN checkout-footer -->
			<div class="checkout-footer">
				{{--<a href="#" class="btn btn-white btn-lg pull-left">Continue Shopping</a>--}}
				<a href="{{ route($route) }}" class="btn btn-inverse btn-lg p-l-30 p-r-30 m-l-10">@lang('messages.next')</a>
			</div>
			<!-- END checkout-footer -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection

@push('scripts')
<script>

	$(function() {

		$( "#formLoginPub" ).submit(function( event ) {
			tryLoginPub();
			event.preventDefault();
		});

		$( "#formRegisterPub" ).submit(function( event ) {
			tryRegisterPub();
			event.preventDefault();
		});

	});

	function activateLoginRegisterPub(action){
		$('.login-form-pub, .register-form-pub').addClass('hide');
		$('.'+action+'-form-pub').removeClass('hide');
	}

	function tryLoginPub(){

		$('#regPubBtnEntrar').html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
		$('#regPubBtnEntrar').attr('disabled', 'disabled');

		var url = '{{ route('public.login') }}';
		var data = {
			email: $('#loginPubUsername').val(),
			password: $('#loginPubPassword').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
				step7 = true;
				customerToken = data.data.id;
//				redirect('/cuenta');
				$('.login-form-pub').addClass('hide');

				if(data.subs_activas === 1) {
					$('.np_class').addClass('hide');
					$('.np_class_not').removeClass('hide');
					current_payment_type = 'subscripcion';
				}
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				var message = '';
				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message += errors[elem].join('<br>') + '<br>';
					} else {
						message += errors[elem];
					}

				}
				$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});
	}

	function tryRegisterPub() {

		$('#regPubBtnEnviar').html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
		$('#regPubBtnEnviar').attr('disabled', 'disabled');

		var url = '{{ route('public.register') }}';
		var data = {
			email: $('#regPubUsername').val(),
			apellidos: $('#regPubApellidos').val(),
			nombres: $('#regPubNombres').val(),
			empresa: $('#regPubEmpresa').val(),
			nit: $('#regPubNit').val(),
			pais: $('#regPubPais').val(),
			telefono: $('#regPubTelefono').val(),
			password: $('#regPubPassword').val(),
			password_confirmation: $('#regPubPassword2').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				redirect('/cuenta');

				customerToken = data.data.id;

				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
//				redirect('/cuenta');
				$('.register-form-pub').addClass('hide');
				step7 = true;
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				var message = '';

				var elements = [
					'email',
					'apellidos',
					'nombres',
					'pais',
					'telefono',
					'password',
					'password_confirmation'
				];

				for(var elem2 in elements){
					$('#regPubError_'+elements[elem2]).html('');
				}

				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message = errors[elem].join('<br>') + '<br>';
					} else {
						message = errors[elem];
					}
					$('#regPubError_'+elem).html('<span class="help-block"><strong>'+message+'</strong></span>')
				}
//					$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});

	}

</script>
@endpush