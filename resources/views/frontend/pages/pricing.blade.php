@extends('frontend.layouts.default')
@push('css')
<style>
	/* -------------------------------
   12.0 Pricing Setting
------------------------------- */

	/* 12.1 Pricing Element Setting */

	.pricing-table {
		list-style-type: none;
		margin: 0 -10px;
		padding: 0;
		text-align: center;
	}
	.pricing-table:before,
	.pricing-table:after {
		content: '';
		display: table;
		clear: both;
	}
	.pricing-table > li {
		float: left;
		padding: 10px;
	}
	.pricing-table.pricing-col-4 > li {
		width: 25%;
	}
	.pricing-table.pricing-col-3 > li {
		width: 33.33333%;
	}
	.pricing-table .pricing-container {
		overflow: hidden;
		border-radius: 6px;
		background: #f0f3f4;
		box-shadow: 0 3px #b6c2c9;
	}
	.pricing-table h3 {
		background: #242a30;
		margin: 0;
		color: #fff;
		font-size: 14px;
		padding: 15px 30px;
		font-weight: bold;
	}
	.pricing-table .features {
		list-style-type: none;
		margin: 0;
		padding: 0 30px;
	}
	.pricing-table .features > li {
		padding: 10px 0;
	}
	.pricing-table .features > li + li {
		border-top: 1px solid #e2e7eb;
	}
	.pricing-table .price {
		width: 100%;
		display: table;
		background: #2d353c;
	}
	.pricing-table .price .price-figure {
		vertical-align: middle;
		display: table-cell;
		text-align: center;
		height: 100px;
	}
	.pricing-table .price .price-number {
		font-size: 28px;
		/*color: #00a3a3;*/
		color: #348fe2;
		display: block;
	}
	.pricing-table .price .price-tenure {
		font-size: 12px;
		color: #fff;
		color: rgba(255, 255, 255, 0.7);
		display: block;
		text-align: center;
	}
	.pricing-table .footer {
		padding: 15px 20px;
	}
	.pricing-table .highlight {
		padding: 0px;
		margin-top: -30px;
	}
	.pricing-table .highlight .features > li {
		padding: 15px 0;
	}
	.pricing-table .highlight h3 {
		padding: 20px 30px;
		/*background:#008a8a;*/
		background: #1e7bd0;
	}
	.pricing-table .highlight .price {
		/*background-color:rgb(0, 172, 172);*/
		background-color:#348fe2;
	}
	.pricing-table .highlight .footer {
		/*background-color:#008a8a;*/
		box-shadow: none;
		-webkit-box-shadow:none;
	}
	.pricing-table .highlight .price .price-figure {
		height: 90px;
	}
	.pricing-table .highlight .price .price-number {
		color: #fff;
	}

	.btn-block {
		padding-top: 15px;
		padding-bottom: 15px;
		padding-left: 12px;
		padding-right: 12px;
	}

	@media (max-width:990px) {

		.col-sm-12 {
			margin-top:40px !important;
		}
		.highlight {
			margin-top:50px !important;
		}
	}

	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}

</style>
@endpush

@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -360px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.pricing')</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">@lang('messages.home')</a></li>
				@if($auth_username != '')
					<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				@endif
				<li class="active">@lang('messages.our_price')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN panel-group -->
			{{--<p class="content-desc text-center">--}}
				{{--@lang('messages.best_prices')--}}
			{{--</p>--}}
			<br><br>
			<div style="clear: both;"></div>
			<!-- begin pricing-table -->



			<div class="row pricing-table">

				<div class="col-md-4 col-sm-12">
					<div class="pricing-container">
						<h3>@lang('messages.plan_basic')</h3>
						<div class="price">
							<div class="price-figure">
								<span style="color:#fff;">@lang('messages.payment_per_announcement')</span>
								<br>
								<span class="price-number">360 kr</span>
								<span style="color:#fff;">@lang('messages.per_180days') (exkl. moms)</span>
							</div>
						</div>
						<ul class="features">
							<li>@lang('messages.single_announcement_post')</li>
							<li>@lang('messages.time_defined_user')</li>
							<li>@lang('messages.gallery_unlimited_images')</li>
							<li>@lang('messages.detailed_machine_description')</li>
							<li>@lang('messages.automatic_translations')</li>
							{{--							<li>@lang('messages.experienced_salesman')</li>--}}
						</ul>
						<div class="footer">
							<a href="{{ route('public.publish') }}" class="btn btn-inverse btn-block">@lang('messages.post_an_announcement')</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 highlight">
					<div class="pricing-container">
						<h3 style="color:#ffffff !important;">@lang('messages.plan_business')</h3>
						<div class="price">
							<div class="price-figure">
								<span class="price-number" style="color:#ffffff !important;">6.900 kr</span>
								<span class="price-tenure" style="color:#ffffff !important;">@lang('messages.per_year') (exkl. moms)</span>

							</div>
						</div>
						<ul class="features">
							<li>@lang('messages.thirty_announcements')</li>
							<li>@lang('messages.one_year_availability')</li>
							<li>@lang('messages.gallery_unlimited_images')</li>
							<li>@lang('messages.detailed_machine_description')</li>
							<li>@lang('messages.automatic_translations')</li>
							<li>
								<b>@lang('messages.customizable_online_store')</b><br>
								@lang('messages.link_to_your_site')<br>
								@lang('messages.post_logo_address')
							</li>
							<li>@lang('messages.experienced_salesman')</li>
						</ul>
						<div class="footer">
							<a href="{{ route("public.{$glc}.get_business") }}" class="btn btn-primary btn-theme btn-block">@lang('messages.buy_now')</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="pricing-container">
						<h3>Premium</h3>
						<div class="price">
							<div class="price-figure">
								<span class="price-number">12.900 kr</span>
								<span class="price-tenure" style="color:#fff;">@lang('messages.per_year') (exkl. moms)</span>
							</div>
						</div>
						<ul class="features">
							<li>@lang('messages.unlimited_announcements')</li>
							<li>@lang('messages.one_year_availability')</li>
							<li>@lang('messages.gallery_unlimited_images')</li>
							<li>@lang('messages.detailed_machine_description')</li>
{{--							<li>@lang('messages.free_banner_bonus')</li>--}}
							<li>@lang('messages.automatic_translations')</li>
							<li>
								<b>@lang('messages.customizable_online_store')</b><br>
								@lang('messages.link_to_your_site')<br>
								@lang('messages.post_logo_address')
							</li>
							<li>@lang('messages.experienced_salesman')</li>
						</ul>
						<div class="footer">
							<a href="{{ route("public.{$glc}.get_plus") }}" class="btn btn-inverse btn-block" style="color:#ffffff !important;">@lang('messages.buy_now')</a>
						</div>
					</div>
				</div>

			</div>

			<!-- END panel-group -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection