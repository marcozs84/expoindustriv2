@extends('frontend.layouts.default')
@push('metas')

<meta property="og:app_id"        content="243295883080737" />
<meta property="og:url"           content="https://www2.expoindustri.com/producto/{{ $id }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="Used Volvo F10 two-way sie tipper truck" />
<meta property="og:description"   content="Lorem ipsum dolor sit amet set consectetuer dip adipisicing" />
<meta property="og:image"         content="http://www2.expoindustri.com/assets/img/gallery/gallery-2.jpg" />


<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />
<style>


	.thumbnails {
		margin-top:20px;
		width:100%;
		/*max-height:200px;*/
		/*position: relative;*/
		/*border:1px solid #000000;*/
		overflow:auto;
	}

	.thumbnails a {
		width:200px;
		height:120px;
		background-color:#000000;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		display:block;
		margin:0px 10px 0px 0px;
		text-align:center;
		overflow:hidden;
	}
	.thumbnails a img {
		max-width:100%;
		max-height:100%;
		margin:auto;
		position: relative;
	}


	.popover{
		width:500px;
	}

	/* Ribbon Ref.: https://codepen.io/nxworld/pen/oLdoWb */
	/* common */
	.ribbon {
		width: 150px;
		height: 150px;
		overflow: hidden;
		position: absolute;
		/*position: relative;*/
		z-index: 1000;
	}
	.ribbon::before,
	.ribbon::after {
		position: absolute;
		z-index: -1;
		content: '';
		display: block;
		border: 5px solid #d00404;
	}
	.ribbon div {
		position: absolute;
		display: block;
		width: 225px;
		/*padding: 15px 0;*/
		padding: 8px 0;
		/*background-color: #3498db;*/
		background-color: #ff0000;
		box-shadow: 0 5px 10px rgba(0,0,0,.1);
		color: #fff;
		font: 700 15px/1 'Lato', sans-serif;
		font-size:13px;
		text-shadow: 0 1px 1px rgba(0,0,0,.2);
		text-transform: uppercase;
		text-align: center;
		line-height: 16px;
	}

	.ribbon div a {
		color: #fff;
		text-decoration: none;
	}

	/* top right*/
	.ribbon-top-right {
		top: -10px;
		right: -10px;
	}
	.ribbon-top-right::before,
	.ribbon-top-right::after {
		border-top-color: transparent;
		border-right-color: transparent;
	}
	.ribbon-top-right::before {
		top: 0;
		left: 0;
	}
	.ribbon-top-right::after {
		bottom: 0;
		right: 0;
	}
	.ribbon-top-right div {
		left: -25px;
		top: 30px;
		transform: rotate(45deg);
	}

	.product-price {
		font-size:13px !important;
	}

	@media (min-width:991px) {


		.product-price .price {
			font-size:32px !important;
		}
	}
	@media (min-width:1919px) {
	}
	@media (min-width:1200px) {
	}
	@media (max-width:992px) {
	}
	@media (max-width:892px) {
	}
	@media (max-width:768px) {
		.product-purchase-container {
			padding:10px 20px !important;
		}
		.product-purchase-container .product-discount, .product-purchase-container .product-price{
			position:static !important;
			margin-bottom:0px;
		}

		.product-price .price {
			font-size:32px !important;
		}
	}
	@media (max-width:500px) {
		.product-price-label {
			display:none;
		}
		.product-purchase-container .product-discount, .product-purchase-container .product-price{
			position:static !important;
		}
		.product-image-alt {
			margin-left:0px;
			margin-top:0px;
			top:0px;
			left:0px;
		}

		.product-price .price {
			font-size:32px !important;
		}
	}


</style>

@endpush
@push('css')
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />
<style>
	.fa-ul li {
		padding:4px;
	}
</style>
@endpush
@section('content')

	@include('frontend.includes.topMessage')

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=243295883080737&autoLogAppEvents=1';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

	<!-- BEGIN #product -->
	<div id="product" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li><a href="{{ route("private.{$glc}.announcements") }}">@lang('messages.my_announces')</a></li>
				<li class="active">{{ $pub->Producto->Categorias[0]->Traduccion[$global_localization] }}</li>
			</ul>
			<!-- END breadcrumb -->

			<div class="row">
				<div class="col-md-8">

					{{--<div class="ribbon ribbon-top-right @if($pub->Owner->Owner->Proveedor->tieneStore == 1) hide @endif">--}}
						{{--<div>--}}
							{{--<a href="#modal-store" data-toggle="modal" data-><span>e-store<br>customers</span></a>--}}
						{{--</div>--}}
					{{--</div>--}}
					@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)
						@php
							$imagen = $pub->Producto->Galerias[0]->Imagenes[0];
						@endphp
						{{--								@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)--}}
						<div style="text-align:center; background-color:#000000; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
							<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">
								<img data-u="image" src="{{ $imagen->ruta_publica_producto }}" style="max-width:100%; max-height:500px;" />
							</a>
						</div>

						<div class="thumbnails">

							<table class="img_holder">
								<tr>

									@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)

										<td>
											<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">
												<img src="{{ $imagen->ruta_publica_producto_thumb }}" />
											</a>
										</td>

									@endforeach

								</tr>
							</table>

						</div>
					@endif

					<br>
					<div class="row m-b-20">
						<div class="col-md-12">
							<div style="background-color:#ffffff; padding: 20px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">

								<h3>{{ $pub->Producto->nombre_alter }}</h3>

								<table class="">
									<tbody>
									@foreach($secondaryData as $data)
										<tr>
											<td class="field" style="font-weight: bold; padding: 5px;">@lang('form.'.$data['key']): </td>
											<td style="padding: 5px;">
												@if($data['type'] == 'select')
													@lang('form.'.$data['value'])
												@elseif($data['type'] == 'multiselect')
													<ul class="fa-ul">
														@foreach($data['value'] as $val)
															<li>
																<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
																@lang('form.'.$val)
															</li>
														@endforeach
													</ul>
												@elseif($data['type'] == 'range_year')
													{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
												@elseif($data['type'] == 'boolean')
													@lang('form.'.$data['value'])
												@else
													{{ $data['value'] }}
												@endif
											</td>
										</tr>
									@endforeach

									</tbody>
								</table>



							</div>
						</div>
					</div>

				<!-- BEGIN product-main-image -->
					{{--<div class="product-main-image" data-id="main-image">--}}

					{{--<img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" />--}}
					{{--</div>--}}

					{{--@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)--}}
					{{--@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)--}}
					{{--<div data-p="170.00">--}}
					{{--<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">--}}
					{{--<img data-u="image" src="{{ $imagen->ruta_publica_producto_thumb }}" />--}}
					{{--</a>--}}
					{{--</div>--}}
					{{--@endforeach--}}
					{{--@endif--}}

				</div>
				<div class="col-md-4">
					<div style="min-height:500px; width:100%; /*border:1px solid #333;*/">

						<!-- BEGIN product-info -->
						<div class="product-info" style="padding-top:0px; position: relative;">
							<!-- BEGIN product-info-header -->
							<div class="product-info-header">

								<h1 class="product-title">
									@if($pub->Owner->Owner->Proveedor->tieneStore == 1)
										<div>
											@if($tieneImagen == true)
												<div style="background-color:{{ $imagen->backgroundColor }}; /*background-color:#ffffff;*/ /*border:1px solid #c5ced4;*/ text-align:center; padding:10px; margin:0px 0px 0px 0px;">
													<a class="text-white" href="{{ route("public.{$glc}.perfilCliente", [$pub->Owner->Owner->id]) }}">
														<img src="{{ $logoUrl }}" alt="Logo proveedor" style="max-width:100%;">
													</a>
												</div>
											@endif
										</div>

										@if( $pub->Owner->Owner->Proveedor->descripcion != '' )
											<div class="">
												<a href="javascript:;" class="see_hide_link text-right" style="float:right; font-size:12px; text-decoration:none;" onclick="show_hide_description()"><i class="fa fa-eye"></i> @lang('messages.see_more_desc')</a>

												<div class="prov_description  " style="font-size:12px; clear:both; padding-bottom:10px; display:none;">
													{{ $pub->Owner->Owner->Proveedor->descripcion }}
												</div>
												<div style="font-size:12px; clear:both; "></div>
											</div>
										@endif
									@endif



									<script>
										function show_hide_description() {
											if( $('.prov_description').is(':visible') ) {
//													$('.prov_description').addClass('hide');
												$('.prov_description').hide(500);
												$('.see_hide_link').html('<i class="fa fa-eye"></i>&nbsp;&nbsp; @lang('messages.see_more_desc')');
											} else {
//													$('.prov_description').removeClass('hide');
												$('.prov_description').show(500);
												$('.see_hide_link').html('<i class="fa fa-eye-slash"></i>&nbsp; @lang('messages.hide_more')');
											}
										}
									</script>

									<span class="label label-primary @if($pub->Owner->Owner->Proveedor->tieneStore != 1) hide @endif" style="width:auto !important; max-width:none !important; padding:10px; font-weight:bold;">
										<a class="text-white" target="_blank" href="{{ route("public.{$glc}.perfilCliente", [$pub->Owner->Owner->id]) }}">
											@lang('messages.goto_store')
										</a>
									</span>

									{{ $pub->Producto->nombre_alter }}
								</h1>
								<ul class="product-category">
									<li><a href="#">{{ $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] }}</a></li>
									<li>/</li>
									<li><a href="#">{{ $pub->Producto->Categorias[0]->Traduccion[$global_localization] }}</a></li>
								</ul>
							</div>
							<!-- END product-info-header -->
							<!-- BEGIN product-warranty -->
							<div class="product-warranty">
								<div class="pull-right">{{ $pub->created_at->format('d/m/Y') }}</div>
								<div><b>@lang('messages.availability'):</b> {{ ($pub->Producto->ProductoItem->Disponibilidad) ? $pub->Producto->ProductoItem->Disponibilidad->Traduccion[$global_localization] : '---' }}</div>
							</div>
							<!-- END product-warranty -->
							<!-- BEGIN product-info-list -->
							<ul class="fa-ul">
								@foreach($primaryData as $data)
									<li>
										<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
										<b>@lang('form.'.$data['key'])</b>:
										@if($data['type'] == 'select')
											@lang('form.'.$data['value'])
										@elseif($data['type'] == 'multiselect')
											<ul class="fa-ul">
												@php
													if(!is_array($data['value'])){
														$data['value'] = [$data['value']];
													}
												@endphp
												@foreach($data['value'] as $val)
													<li>
														<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
														@lang('form.'.$val)
													</li>
												@endforeach
											</ul>
										@elseif($data['type'] == 'range_year')
											{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
										@elseif($data['type'] == 'boolean')
											@lang('form.'.$data['value'])
										@else
											{{ $data['value'] }}
										@endif
									</li>
								@endforeach
							</ul>
							<!-- END product-info-list -->
							<!-- BEGIN product-purchase-container -->
							<div class="product-purchase-container">
								{{--							<h4 class="m-b-0">@lang('messages.price_in_europe'):</h4>--}}
								<br>
								@if( $pub->Producto->precioProveedor > 0 )

								<div class="product-price">
									{{--								<div class="price">{{ strtoupper($pub->Producto->monedaUnitario) }} {{ number_format($pub->Producto->precioUnitario, 2) }}</div>--}}
									<div class="product-discount">@lang('messages.price'):</div>
									<div class="price">
										{{ number_format($pub->Producto->precioProveedor, 0) }} {{ strtoupper($pub->Producto->monedaProveedor) }}
									</div>


									{{--<div class="product-discount">--}}
									{{--<span class="discount">{{ $pub->moneda }}869.00</span>--}}
									{{--</div>--}}
								</div>
								@endif

								@if( $pub->Producto->precioEuropa > 0 )
								<div class="product-price">
									<div class="product-discount">@lang('messages.price_europa'):</div>
									<div class="price">
										{{--									{{ strtoupper($pub->Producto->monedaUnitario) }}--}}
										{{--@if($pub->Producto->precioFijo == 1)--}}
										{{--										{{ number_format($pub->Producto->precioUnitario, 0) }}--}}
										{{--{{ $pub->Producto->precioUnitario }}--}}
										{{--@else--}}
										{{--{{ number_format(($pub->Producto->precioProveedor / $tipo_cambio_usado), 2) }}--}}
										{{--@endif--}}
										{{ number_format($pub->Producto->precioEuropa, 0) }} {{ strtoupper($pub->Producto->monedaEuropa) }}
										{{--{{ number_format($pub->Producto->precio_publicacion, 0) }} {{ strtoupper(session('current_currency', 'usd')) }}--}}
									</div>
								</div>
								@endif

								@if( $pub->Producto->precioFijo == 1 )
									<div class="product-price">
										<div class="product-discount">@lang('messages.price_fixed_by_consultant'):</div>
										<div class="price">
											{{--									{{ strtoupper($pub->Producto->monedaUnitario) }}--}}
											{{--@if($pub->Producto->precioFijo == 1)--}}
											{{--										{{ number_format($pub->Producto->precioUnitario, 0) }}--}}
											{{--{{ $pub->Producto->precioUnitario }}--}}
											{{--@else--}}
											{{--{{ number_format(($pub->Producto->precioProveedor / $tipo_cambio_usado), 2) }}--}}
											{{--@endif--}}
											@php
												$precioFijo = 'precioFijo'.ucwords( session('current_currency', 'usd') )
											@endphp
											{{ number_format($pub->Producto->$precioFijo, 0) }} {{ strtoupper( session('current_currency', 'usd') ) }}
											{{--{{ number_format($pub->Producto->precio_publicacion, 0) }} {{ strtoupper(session('current_currency', 'usd')) }}--}}
										</div>
									</div>
								@endif

								{{--<h4 class="m-b-0">@lang('messages.owner_price'):</h4>--}}
								{{--<div class="product-price">--}}
								{{--<div class="price">{{ strtoupper($pub->Producto->monedaProveedor) }} {{ number_format($pub->Producto->precioProveedor, 0) }}</div>--}}
								{{--<div class="product-discount">--}}
								{{--<span class="discount">{{ $pub->moneda }}869.00</span>--}}
								{{--</div>--}}
								{{--</div>--}}

							</div>
						{{--<div class="alert alert-primary " style="background-color:#e9edef; display:none;">--}}
						{{--<b>@lang('messages.precios_not_match')</b><br>--}}
						{{--@lang('messages.precios_not_match_msg')--}}
						{{--</div>--}}
						<!-- END product-purchase-container -->
							<!-- BEGIN product-social -->
							<div class="product-social" style="border-bottom:0px; padding-top:20px; border-top:1px solid #D8E0E4; display:none;">
								<ul>
									<li><a href="javascript:;" class="facebook" data-toggle="tooltip" data-trigger="hover" data-title="Facebook" data-placement="top"><i class="fab fa-facebook"></i></a></li>
									<li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww2.expoindustriv2%2Fproducto%2F25&text=@lang('messages.hi_tweet')"
									       class="twitter" data-toggle="tooltip" data-trigger="hover" data-title="Twitter" data-placement="top"><i class="fab fa-twitter"></i></a></li>
									{{--<li><a href="javascript:;" class="google-plus" data-toggle="tooltip" data-trigger="hover" data-title="Google Plus" data-placement="top"><i class="fab fa-google-plus"></i></a></li>--}}
									{{--<li><a href="javascript:;" class="whatsapp" data-toggle="tooltip" data-trigger="hover" data-title="Whatsapp" data-placement="top"><i class="fab fa-whatsapp"></i></a></li>--}}
									{{--<li><a href="javascript:;" class="tumblr" data-toggle="tooltip" data-trigger="hover" data-title="Tumblr" data-placement="top"><i class="fab fa-tumblr"></i></a></li>--}}
									<li><a href="javascript:;" class="tumblr" data-toggle="tooltip" data-trigger="hover" data-title="E-mail" data-placement="top"><i class="fa fa-envelope"></i></a></li>
									<li>
										<div class="fb-share-button"
										     data-href="https://www2.expoindustri.com/producto/25"
										     data-layout="button"
										     data-size="small"
										     data-mobile-iframe="false">
											<a target="_blank"
											   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww2.expoindustri.com%2Fproducto%2F25&amp;src=sdkpreparse"
											   class="fb-xfbml-parse-ignore">Compartir_mz</a>
										</div>
									</li>
								</ul>
							</div>
							<!-- END product-social -->
						</div>
						<!-- END product-info -->

					</div>
				</div>
			</div>


			<div class="modal fade" id="modal-proceso">
				<div class="modal-dialog" style="width:760px !important;">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" style="display: inline;">@lang('messages.how_guarantee_buy')</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							@lang('messages.guarantee_content')
						</div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-pagoseguro">
				<div class="modal-dialog" style="width:760px !important;">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" style="display: inline;">@lang('messages.is_secure_payment')</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							@lang('messages.is_secure_payment_content')
						</div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- END container -->
	</div>
	<!-- END #product -->

	<div id="product" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container" id="relatedItemsContainer">

		</div>
	</div>

	<div class="modal fade" id="modalInfoAdicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Informe Técnico</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div>
						<b>ExpoIndustri</b> utiliza una empresa externa experimentada en la área de reviciones
						tecnicas de alto nivel, con un personal altamente capacitada,para brindar a sus clientes
						un informe al detalle .
						<a href="javascript:;">Vea el formulario</a>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					{{--<button type="button" class="btn btn-primary">Save changes</button>--}}
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalSeguro" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Seguro para maquinaria</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div>
						Muy Pronto...
						<br>
						<b>ExpoIndustri</b>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					{{--<button type="button" class="btn btn-primary">Save changes</button>--}}
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modal-store">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">e-store Service</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					Coming soon...
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('scripts')

{{--<script src="/assets/js/jssor.slider-27.1.0.min.js"></script>--}}

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>

<script>

	$(document).ready(function(){

//		var jssor_1_SlideshowTransitions = [
//			{$Duration:800,$Opacity:2}
//		];
//
//		var jssor_1_options = {
//			$AutoPlay: 1,
//			$FillMode: 5,
//			$SlideshowOptions: {
//				$Class: $JssorSlideshowRunner$,
//				$Transitions: jssor_1_SlideshowTransitions,
//				$TransitionsOrder: 1
//			},
//			$ArrowNavigatorOptions: {
//				$Class: $JssorArrowNavigator$
//			},
//			$BulletNavigatorOptions: {
//				$Class: $JssorBulletNavigator$
//			}
//		};
//
//		var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		/*#region responsive code begin*/

//		var MAX_WIDTH = 600;

//		function ScaleSlider() {
//			var containerElement = jssor_1_slider.$Elmt.parentNode;
//			var containerWidth = containerElement.clientWidth;
//
//			if (containerWidth) {
//
//				var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
//
//				jssor_1_slider.$ScaleWidth(expectedWidth);
//			}
//			else {
//				window.setTimeout(ScaleSlider, 30);
//			}
//		}

//		ScaleSlider();
//
//		$(window).bind("load", ScaleSlider);
//		$(window).bind("resize", ScaleSlider);
//		$(window).bind("orientationchange", ScaleSlider);


		// ---------- POPOVERS

		$('[data-toggle="popover"]').popover();

		// ---------- SHARE BUTTONS

		$('#btnShareFacebook').click(function(e){
			FB.ui({
				method: 'share',
				href: 'https://www2.expoindustri.com/producto/{{ $id }}',
			}, function(response){});
		})

		// ---------- TRANSPORTE
		$('.tableTransportation input').change(function(e){
			$('.spnVal, .spnTiempo').addClass('hide');
			var t = $(this).val();
			$('.spn_'+t+', .spnVal_'+t).removeClass('hide');
		});
		// ---------- PAISES Y ZONAS



		initRelatedItems();

	});

	function initRelatedItems(){
		var url = '{{ route('component.relatedItems', ['cat' => $pub->Producto->Categorias[0]->Padre]) }}';
		$('#relatedItemsContainer').load(url);
	}


</script>
@endpush