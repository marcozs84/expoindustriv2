@extends('frontend.layouts.default')

@push('css')
<script src='https://www.google.com/recaptcha/api.js?render={{ $global_recaptcha_public_key }}'></script>
@endpush

@push('css')
<style>
	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
		font-size:35px;
	}

</style>
@endpush

@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="assets/img/e-commerce/cover/cover-15.jpg" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>@lang('messages.contact_us')</b> </h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #product -->
	<div id="product" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.contact_us')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN row -->
			<div class="row row-space-30">
				<!-- BEGIN col-8 -->
				<div class="col-md-8">
					{{--<h4 class="m-t-0">@lang('messages.contact_form')</h4>--}}
					{{--<p class="m-b-30 f-s-13">--}}
						{{--@lang('messages.contact_form_info')--}}
					{{--</p>--}}
					<h4>@lang('messages.contact_doubts')</h4>
					<p>@lang('messages.contact_doubts_body')</p>
					<br><br>

					@if(isset($mail_enviado) && $mail_enviado == true)
						<div class="alert alert-success m-b-0">
							<span class="close" data-dismiss="alert">×</span>
							<h4>@lang('messages.contact_form_sent')!</h4>
							<p>@lang('messages.contact_form_sent_info')</p>
						</div>
					@else


					<form class="form-horizontal" name="contact_us_form" action="{{ route('public.contactPost') }}" method="post">
						{{ csrf_field() }}

						{!! Form::hidden('g-recaptcha-response', null, [
							'id' => 'g-recaptcha-response',
						]) !!}

						<div class="form-group">
							<label class="control-label col-md-3">@lang('messages.firstname')
								@if ($errors->has('nombres'))
									<span class="text-danger">*</span>
								@endif
							</label>

							<div class="col-md-7">
								{!! Form::text('nombres', null, ['id' => 'nombres', 'class' => 'form-control', 'placeholder' => __('messages.firstname')]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">@lang('messages.lastname')
								@if ($errors->has('apellidos'))
									<span class="text-danger">*</span>
								@endif
							</label>

							<div class="col-md-7">
								{!! Form::text('apellidos', null, ['id' => 'apellidos', 'class' => 'form-control', 'placeholder' => __('messages.firstname')]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">@lang('messages.e_mail')
								@if ($errors->has('email'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-7">
								{!! Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => __('messages.e_mail')]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">@lang('messages.country')
								@if ($errors->has('pais'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-7">
								{{--<select name="pais" id="pais" class="form-control" >--}}
									{{--<option value="">@lang('messages.select_country')</option>--}}
								{{--</select>--}}
								{!! Form::select('pais', $global_paises, $global_localization, ['class' => 'form-control', 'placeholder' => __('messages.select_country')]); !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">@lang('messages.subject')
								@if ($errors->has('asunto'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-7">
								{!! Form::text('asunto', null, ['id' => 'asunto', 'class' => 'form-control', 'placeholder' => __('messages.subject')]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">@lang('messages.message')
								@if ($errors->has('mensaje'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-7">
								{!! Form::textarea('mensaje', '', [
									'id' => 'mensaje',
									'placeholder' => __('messages.message'),
									'class' => 'form-control',
									'rows' => 10
								]) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-7">
								<button id="btnContactoEnviar" type="submit" class="btn btn-inverse btn-lg">@lang('messages.send_message')</button>

								{{--<table cellpadding="10">--}}
									{{--<tr>--}}
										{{--<td style="padding:10px; padding-left:0px;">--}}
											{{--<i class="flag-icon flag-icon-bo" style="font-style: 20px;"></i> BOLIVIA--}}
										{{--</td>--}}
										{{--<td style="padding:10px;">--}}
											{{--<i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> CHILE--}}
										{{--</td>--}}
										{{--<td style="padding:10px;">--}}
											{{--<i class="flag-icon flag-icon-pe" style="font-style: 20px;"></i> PERU--}}
										{{--</td>--}}
									{{--</tr>--}}
								{{--</table>--}}
							</div>
						</div>
					</form>
					@endif
				</div>
				<!-- END col-8 -->
				<!-- BEGIN col-4 -->
				<div class="col-md-4">
					<h4 class="m-t-0">@lang('messages.our_contacts')</h4>
					<div class="embed-responsive embed-responsive-16by9 m-b-15">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2131.1642970880116!2d14.30762861600756!3d57.71366688112296!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x465a0d0922fbb27f%3A0xfe8003776ed5b3df!2zSsO2bmvDtnBpbmdzdsOkZ2VuIDI3LCA1NjAgMjcgVGVuaHVsdCwgU3VlY2lh!5e0!3m2!1ses-419!2sbo!4v1579526615192!5m2!1ses-419!2sbo" width="100%" height="200" frameborder="0" style="border:1px solid #e5e5e5;" allowfullscreen></iframe>
					</div>

					<div>
						<div><i class="flag-icon flag-icon-se" style="font-style: 20px;"></i> <b>@lang('messages.sweden')</b></div>
						<p class="m-b-15">
							Jönköpingsvägen 27 <br>
							561 61 Tenhult <br>
{{--							<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +46 (0) 723 933 700<br />--}}
							<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />
						</p>
					</div>
					<div class="hide">
						<div><i class="flag-icon flag-icon-bo" style="font-style: 20px;"></i> <b>Bolivia</b></div>
						<p class="m-b-15">
							795 Folsom Ave, Suite 600<br />
							San Francisco, CA 94107<br />
							P: (123) 456-7890<br />
						</p>
					</div>

					<div class="">
						<div><i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> <b>Chile</b></div>
						<p class="m-b-15">
							Oficina mapocho 48D<br />
							Zofri - Iquique<br />
							{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
							<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />
						</p>

						{{--<div><i class="flag-icon flag-icon-pe" style="font-style: 20px;"></i> <b>Perú</b></div>--}}
						{{--<p class="m-b-15">--}}
							{{--795 Folsom Ave, Suite 600<br />--}}
							{{--San Francisco, CA 94107<br />--}}
							{{--P: (123) 456-7890<br />--}}
						{{--</p>--}}
					</div>
					{{--<div><b>@lang('messages.e_mail')</b></div>--}}
					{{--<p class="m-b-15">--}}
						{{--<a href="mailto:hello@emailaddress.com" class="text-inverse">info@seantheme.com</a><br />--}}
						{{--<a href="mailto:hello@emailaddress.com" class="text-inverse">business@seantheme.com</a><br />--}}
						{{--<a href="mailto:hello@emailaddress.com" class="text-inverse">support@seantheme.com</a><br />--}}
					{{--</p>--}}
					<div class="m-b-5"><b>@lang('messages.social_networks')</b></div>
					<p class="m-b-15">
						<a href="https://www.facebook.com/ExpoIndustri" target="_blank" class="btn btn-icon btn-white btn-circle"><i class="fab fa-facebook fa-lg"></i></a>
						{{--<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-twitter"></i></a>--}}
						{{--<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-google-plus"></i></a>--}}
						{{--<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-instagram"></i></a>--}}
						{{--<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-dribbble"></i></a>--}}
					</p>
				</div>
				<!-- END col-4 -->
			</div>
			<!-- END row -->
		</div>
		<!-- END row -->
	</div>
	<!-- END #product -->

@endsection

@push('scripts')

<script>
	grecaptcha.ready(function() {
		grecaptcha.execute('{{ $global_recaptcha_public_key }}', {action: 'action_name'})
			.then(function(token) {
				console.log("re-captcha token received");
				console.log(token);
				$('#g-recaptcha-response').val(token);
// Verify the token on the server.
			});
	});
</script>

<script>

	$(document).ready(function(){

		$('#btnContactoEnviar').click(function(e){
			$(this).html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
		});

		{{--var paises = {!! json_encode($global_paises) !!};--}}
		{{--console.log(paises.length);--}}
		{{--$.each(paises, function(k,v){--}}
			{{--$('#pais').append($('<option>', { value:v }).html(k));--}}
		{{--});--}}

	});

</script>
@endpush