@php
	$grp = 0;
	$openGroup = false;
@endphp
<div class="row">
@foreach($primaryFields as $element)

	@if($element['type'] == 'titulo1')
		@include('frontend.formElements.titulo1', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'titulo2')
		@include('frontend.formElements.titulo2', [
			'e' => $element,
			'group' => $grp
		])
	@endif

	@if($element['type'] == 'text')

		@if($element['key'] == 'pais_de_fabricacion')
			@include('frontend.formElements.select_country', [
				'e' => $element,
			])
		@else
			@include('frontend.formElements.input', [
				'e' => $element,
			])
		@endif
	@endif

	@if($element['type'] == 'number')
		@include('frontend.formElements.number', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'boolean')
		@include('frontend.formElements.boolean', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'multiline')
		@include('frontend.formElements.text', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'select')
		@include('frontend.formElements.select', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'multiselect')
		@include('frontend.formElements.multiselect', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'percent10')
		@include('frontend.formElements.percent10', [
			'e' => $element,
		])
	@endif

	@if($element['type'] == 'range_year')
		@include('frontend.formElements.rangeYear', [
			'e' => $element,
		])
	@endif
@endforeach



	@php
		$grp = 1;
		$openGroup = false;
	@endphp
	@foreach($secondaryFields as $element)

		@if($element['type'] == 'titulo1')
			@include('frontend.formElements.titulo1', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'titulo2')
			@if($openGroup == true)
				@php echo '</div>'; @endphp
			@endif

			@include('frontend.formElements.titulo2', [
					'e' => $element,
					'group' => $grp
				])

			<div id="group_{{ $grp++ }}" class="collapse row" >
					@php
						$openGroup = true;
					@endphp
		@endif

				@if($element['type'] == 'text')
					@if($element['key'] == 'pais_de_fabricacion')
						@include('frontend.formElements.select_country', [
							'e' => $element,
						])
					@else
						@include('frontend.formElements.input', [
							'e' => $element,
						])
					@endif
				@endif

				@if($element['type'] == 'number')
					@include('frontend.formElements.number', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'boolean')
					@include('frontend.formElements.boolean', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'multiline')
					@include('frontend.formElements.text', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'select')
					@include('frontend.formElements.select', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'multiselect')
					@include('frontend.formElements.multiselect', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'percent10')
					@include('frontend.formElements.percent10', [
						'e' => $element,
					])
				@endif

				@if($element['type'] == 'range_year')
					@include('frontend.formElements.rangeYear', [
						'e' => $element,
					])
				@endif
				@endforeach

	</div>