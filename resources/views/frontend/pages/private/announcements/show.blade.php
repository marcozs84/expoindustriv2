@extends('frontend.layouts.default')
@push('css')
<link href="/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css" />
<style>

	label{
		/*display:inline-block !important;*/
		margin-bottom:0px !important;
		color: #4e4e4e;
	}
	/*input {*/
	/*display:block !important;*/
	/*}*/

	h4, h5 {
		display:block;
		clear:both;
		padding-left:17px;
	}

	h5{
		padding-bottom:10px;
		border-bottom:1px solid #eee;
	}

	.leftColumn{
		vertical-align:top;
	}
	.rightColumn{
		padding-left:10px !important;
	}

	.account-sidebar-cover{
		width:300px; overflow:hidden;
	}

	.account-body{
		margin-left:10px !important;
		padding-top:10px;
	}

	@media (min-width:991px) {
		.leftColumn {
			width:260px;
		}
		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:1200px) {
		.leftColumn {
			/*width:160px;*/
		}
	}
	@media (max-width:992px) {
		.leftColumn {
			width:190px;
			/*display:none;*/
		}

		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:769px) {
		.leftColumn {
			/*width:190px;*/
			display:none;
		}

		.account-sidebar{
			display:none;
		}
		.account-body{
			margin-left:-20px !important;
			padding-top:10px;
		}
	}
</style>
@endpush
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20 p-b-0 m-b-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.publish')</li>
			</ul>
			<!-- END breadcrumb -->



			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar">
					<div class="account-sidebar-cover" style="background-image:url('/assets/img/e-commerce/cover/cover-14.jpg')">
						{{--<img src="/assets/img/e-commerce/cover/cover-14.jpg" style="max-height:none;" alt="" />--}}
					</div>
					<div class="account-sidebar-content hide" style="width:100%;">
						<h4>Su cuenta</h4>
						<p>
							Modifique una orden, o haga seguimiento a su cargamento y actualice su información personal.
						</p>
						<p>
							Todo lo que necesita en un solo lugar.  Por favor no dude en contactarnos si requiere asistencia.
						</p>
					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body" style="">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN col-6 -->
						@php
							$step = 0;
						@endphp
						<div class="col-md-12">
							<table class="table-condensed" style="width:100%;">
								<tr class="step_{{ $step }}">
									<td class="leftColumn">

									</td>
									<td class="rightColumn">
										<h4>@lang('messages.new_announce')</h4>
										<hr>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								<tr class="step_{{ $step }}">
									<td class="leftColumn">
										<div class="step active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.basic_information')</div>
													<div class="desc">@lang('messages.basic_information_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.brand')
													<a href="javascript:;" id="errorDisplay_marca" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('marca', $pub->Producto->idMarca, [
													'id' => 'marca',
													'class' => 'form-control',
													'placeholder' => __('messages.brand'),
													'onblur' => 'getModelos(this)'
												]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.model')
													<a href="javascript:;" id="errorDisplay_modelo" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('modelo', $pub->Producto->idModelo, [
													'id' => 'modelo',
													'class' => 'form-control',
													'placeholder' => __('messages.model')
												]) !!}
											</div>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								{{--<tr class="step_1" style="display: none;">--}}
								{{--<td class="leftColumn">--}}
								{{--<td><hr></td>--}}
								{{--</tr>--}}
								@php
									$step++;
								@endphp
								<tr class="step_{{ $step }}" style="/*display: none;*/">
									<td class="leftColumn">
										<div class="step active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.select_your_category')</div>
													<div class="desc">@lang('messages.select_your_category_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.category')
													<a href="javascript:;" id="errorDisplay_categoria" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('categoria', $categorias->pluck('nombre', 'id'), [$pub->Producto->Categorias[0]->id], [
												    'id' => 'categoria',
												    'class' => 'form-control',
												    'placeholder' => __('messages.select_category'),
												    'onchange' => 'changeSubs(this)'
												]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.subcategory')
													<a href="javascript:;" id="errorDisplay_subcategoria" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('subcategoria', [], [], [
												    'id' => 'subcategoria',
												    'class' => 'form-control',
												    'placeholder' => __('messages.select_category'),
												]) !!}
											</div>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								<tr class="step_{{ $step }}" style="/*display: none;*/">
									<td class="leftColumn">
										<div class="step active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.machine_location')</div>
													<div class="desc">@lang('messages.machine_location_descr', ['route' => route('public.dataProtectionPolicy')])</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.country')
													<a href="javascript:;" id="errorDisplay_pais" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::select('pais', $global_paises, $global_country_abr, [
												    'id' => 'pais',
												    'class' => 'form-control',
												    'placeholder' => __('messages.country'),

												]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.city')
													<a href="javascript:;" id="errorDisplay_ciudad" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('ciudad', '' /*$ciudad*/, [
													'id' => 'ciudad',
													'class' => 'form-control', 'placeholder' => __('messages.city'),

											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.postal_code')
													<a href="javascript:;" id="errorDisplay_codigo_postal" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::text('codigo_postal', '' /*$pcode*/, ['id' => 'codigo_postal', 'class' => 'form-control', 'placeholder' => __('messages.postal_code'),

											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.address')
													{{--( <a href="javascript:;" onclick="buscarEnMapa()">@lang('messages.findInMap')</a> )--}}
													<a href="javascript:;" id="errorDisplay_direccion" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::textarea('direccion', ''/*$direccion*/, ['id' => 'direccion', 'class' => 'form-control', 'rows' => 4, 'placeholder' => __('messages.address'),

											    ]) !!}
											</div>
											{{--<div class="form-group col-md-6">--}}
											{{--<label class="control-label">@lang('messages.location')--}}
											{{--<a href="javascript:;" id="errorDisplay_ubicacion" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>--}}
											{{--</label>--}}
											{{--<style>--}}
											{{--#publish_map {--}}
											{{--height: 300px;--}}
											{{--width: 100%;--}}
											{{--}--}}
											{{--</style>--}}

											{{--<div id="publish_map"></div>--}}
											{{--</div>--}}
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								<tr class="step_{{ $step }}" style="/*display: none;*/">
									<td class="leftColumn">
										<div class="step active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.transport_info')</div>
													<div class="desc">@lang('messages.transport_info_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.longitude_mm')
													<a href="javascript:;" id="errorDisplay_longitud" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('longitud', (($longitud == 0) ? '' : $longitud), [
													'id' => 'longitud',
													'class' => 'form-control',
													'placeholder' => __('messages.longitude_mm'),
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.width_mm')
													<a href="javascript:;" id="errorDisplay_ancho" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('ancho', (($ancho == 0) ? '' : $ancho) , [
													'id' => 'ancho',
													'class' => 'form-control',
													'placeholder' => __('messages.width_mm'),
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.height_mm')
													<a href="javascript:;" id="errorDisplay_alto" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('alto', (($alto == 0) ? '' : $alto), [
													'id' => 'alto',
													'class' => 'form-control',
													'placeholder' => __('messages.height_mm'),
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label">@lang('messages.weight_brute')
													<a href="javascript:;" id="errorDisplay_peso_bruto" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::number('peso_bruto', (($peso == 0) ? '' : $peso), [
													'id' => 'peso_bruto',
													'class' => 'form-control',
													'placeholder' => __('messages.weight_brute'),
											    ]) !!}
											</div>
											<div class="form-group col-md-6">
												<label class="control-label" for="paletizado">@lang('messages.palletized')
													<a href="javascript:;" id="errorDisplay_paletizado" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
												</label>
												{!! Form::checkbox('paletizado', 'paletizado', $palet, [
													'id' => 'paletizado',
													'style' => 'font-size:15px; width:20px; height:20px; vertical-align:middle;'
											    ]) !!}
											</div>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								<tr class="step_{{ $step }}" style="display:none;">
									<td class="leftColumn">
										<div class="step active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.announce_detail')</div>
													<div class="desc">@lang('messages.announce_detail_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div id="formHolder"></div>
										<div class="form-group col-md-12">
											<div class="text-right">
												<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
												<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								<tr class="step_{{ $step }}" style="/*display: none;*/">
									<td class="leftColumn">
										<div class="step active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.user_info')</div>
													<div class="desc">@lang('messages.user_info_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div>

											<div id="uli" class="col-md-12 @if(!Auth::guard('clientes')->user()) hide @endif">
												@if(Auth::guard('clientes')->user())
													<h4 class="p-l-0"><span id="uli_nombre">{{ Auth::guard('clientes')->user()->Owner->Agenda->nombres_apellidos }}</span></h4>
													<b><span id="uli_email">{{ Auth::guard('clientes')->user()->username }}</span></b>
												@else
													<h4 class="p-l-0"><span id="uli_nombre"></span></h4>
													<b><span id="uli_email"></span></b>
												@endif
												<br>
												<small>@lang('messages.click_next')</small>
											</div>
											<div class="login-form-pub col-md-8 col-md-offset-2  @if(Auth::guard('clientes')->user()) hide @endif ">
												<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
													<h4 class="text-center m-t-0">@lang('messages.log_in')</h4>
												</div>
												<form id="formLoginPub" action="{{ route('public.login') }}" method="post" class="text-center">
													{{ csrf_field() }}
													<input type="text" name="email" id="loginPubUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
													<input type="password" name="password" id="loginPubPassword" class="login-input" placeholder="@lang('messages.password')" >

													<div id="loginPubErrors"></div>

													<input type="checkbox" name="remember" id="frmRemember" /> @lang('messages.remember_me')
													<br>
													{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.log_in')">--}}
													<button type="submit" id="regPubBtnEntrar" form="formLoginPub" value="@lang('messages.log_in')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.log_in')</button>
													{{--<a href="javascript:;" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" onclick="tryLogin()">Entrar</a>--}}
													{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Entrar">--}}
													<br>
													<a href="javascript:;" class="" style="font-size:12px; /*color:#333333;*/">@lang('messages.forgot_password')</a>

												</form>
												<hr />
												<p class="text-center">
													<a href="javascript:;" class="" onclick="activateLoginRegisterPub('register')" style="font-size:12px; /*color:#333333;*/">@lang('messages.create_account')</a>
												</p>
											</div>
											<div class="register-form-pub hide col-md-8 col-md-offset-2">
												<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
													<h4 class="text-center m-t-0">@lang('messages.register')</h4>
												</div>
												<form id="formRegisterPub"  action="{{ route('public.register') }}" method="post" class="text-center">
													<input type="text" name="email" id="regPubUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
													<div id="regPubError_email"></div>

													<input type="text" name="apellidos" id="regPubApellidos" class="login-input" placeholder="@lang('messages.lastname')" >
													<div id="regPubError_apellidos"></div>

													<input type="text" name="nombres" id="regPubNombres" class="login-input" placeholder="@lang('messages.firstname')" >
													<div id="regPubError_nombres"></div>

													{{--<select name="pais" id="regPubPais" class="login-input">--}}
													{{--<option value="">@lang('messages.select_country')</option>--}}
													{{--</select>--}}
													{!! Form::select('pais', $global_paises, null, [
																'id' => 'regPubPais',
																'class' => 'login-input',
																'placeholder' => __('messages.select_country')
													]); !!}
													<div id="regPubError_pais"></div>

													<input type="text" name="telefono" id="regPubTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
													<div id="regPubError_telefono"></div>

													<input type="password" name="password" id="regPubPassword" class="login-input" placeholder="@lang('messages.password')" >
													<div id="regPubError_password"></div>

													<input type="password" name="password_confirmation" id="regPubPassword2" class="login-input" placeholder="@lang('messages.password_confirmation')" >
													<div id="regPubError_password_confirm"></div>

													{{--<input type="submit" id="regPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
													<button type="submit" id="regPubBtnEnviar" form="formRegisterPub" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
													{{--<a href="javascript:;" id="regPubBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterPub').submit();">@lang('messages.send')</a>--}}
												</form>
												<hr />
												<p class="text-center">
													<a href="javascript:;" class="" onclick="activateLoginRegisterPub('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
												</p>
											</div>

											<div class="form-group col-md-12">
												<div class="text-right">
													<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>
													<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>
												</div>
											</div>
										</div>
									</td>
								</tr>
								@php
									$step++;
								@endphp
								<tr class="step_{{ $step }}" style="/*display: none;*/">
									<td class="leftColumn">
										<div class="step active">
											<a href="#">
												<div class="number">{{ $step }}</div>
												<div class="info">
													<div class="title">@lang('messages.payment_options')</div>
													<div class="desc">@lang('messages.payment_options_descr')</div>
												</div>
											</a>
										</div>
									</td>
									<td class="rightColumn">
										<div>
											<hr>
											<h4>@lang('messages.payment_options')</h4>
											<div style="padding:17px;">
												@lang('messages.payment_announce_message', ['price' => '4,50Kr.'])
												<br><br>
												<input type="number" name="days" id="days" class="form-control" value="10" min="10" max="300" style="width:80px; display:inline;" onchange="calc_price()"> @lang('messages.days')
												= <span id="newprice"><b>45Kr</b></span>
												<br><br>
												<div class="alert alert-info fade show in">
													<h5 class="p-l-0">@lang('messages.time_extended_title')</h5>
													<p>
														@lang('messages.time_extended_description')
													</p>
												</div>
											</div>

											<ul class="nav nav-tabs nav-justified nav-justified-mobile">
												<li class="active"><a href="#default-tab-1" data-toggle="tab"><i class="fab fa-cc-stripe"></i> @lang('messages.payment_creditcard')</a></li>
												<li class=""><a href="#default-tab-2" data-toggle="tab"><i class="fas fa-shopping-bag"></i> @lang('messages.payment_trustly')</a></li>
												<li class=""><a href="#default-tab-3" data-toggle="tab"><i class="fa fa-phone-square"></i> @lang('messages.payment_cash')</a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane fade active in" id="default-tab-1" style="background-color:#f0f3f4;">

													<div style="margin:10px;">
														<style>
															/**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
															.StripeElement {
																background-color: white;
																height: 35px;
																font-size:14px;
																padding: 10px 8px;
																margin-bottom:5px;
																border-radius: 4px;
																border: 1px solid #ccd0d4;
																box-shadow: 0 1px 3px 0 #e6ebf1;
																-webkit-transition: box-shadow 150ms ease;
																transition: box-shadow 150ms ease;
															}

															.StripeElement--focus {
																box-shadow: 0 1px 3px 0 #cfd7df;
															}

															.StripeElement--invalid {
																border-color: #fa755a;
															}

															.StripeElement--webkit-autofill {
																background-color: #fefde5 !important;
															}
														</style>
														<form action="/charge" method="post" id="payment-form">
															<div class="form-row">
																<div class="row">
																	<div class="pull-left" style="">
																		<p style="margin:20px 20px;">@lang('messages.provide_card_info')</p>
																	</div>

																	<div class="pull-right p-r-30">
																		<img style="height:70px;" src="/assets/img/e-commerce/visa.png" alt="Visa">
																		<img style="height:70px;" src="/assets/img/e-commerce/mastercard.png" alt="MasterCard">
																	</div>
																</div>

																<div class="card-message hide"></div>
																{{--<label for="card-element">--}}
																{{--Número de tarjeta--}}
																{{--</label>--}}
																{{--<div id="card-element">--}}
																{{--<!-- A Stripe Element will be inserted here. -->--}}
																{{--</div>--}}


																<div class="form-group col-md-12">
																	<label class="control-label" for="card-element">
																		@lang('messages.card_number')
																	</label>
																	<div id="card-number"></div>
																</div>

																<div class="form-group col-md-6">
																	<label class="control-labFecha de expiraciónel" for="card-element">
																		@lang('messages.expiration_date')
																	</label>
																	<div id="card-expiry"></div>
																</div>
																<div class="form-group col-md-6">
																	<label class="control-label" for="card-element">
																		@lang('messages.security_code')
																	</label>
																	<div id="card-cvc"></div>
																</div>
															</div>
															<br>
															{{--<button id="submitPayment" class="btn btn-primary btn-inverse"><i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')</button>--}}
														</form>
													</div>

													<div style="clear:both;"></div>
												</div>

												<div class="tab-pane fade text-center" id="default-tab-2" style="background-color:#f0f3f4;">
													<div style="margin:10px;">
														@lang('messages.payment_trustly_descr')
														<img src="/assets/img/e-commerce/trustly.PNG" alt="">
													</div>
												</div>
												<div class="tab-pane fade" id="default-tab-3" style="background-color:#f0f3f4;">
													<div style="margin:10px;">
														@lang('messages.payment_cash_descr')
													</div>
												</div>
											</div>

											<div class="panel panel-primary" style="clear: both;">
												<div class="panel-body " style="background-color:#f0f3f4;">
													{{--Una ves vendida la maquinaria en ExpoIndustri, recibirá un documento sellado de ADUANA que certifica que la maquinaria fue exportada fuera de país de origen.--}}
													{!! Form::checkbox('legalterms', 'legalterms', $palet, [
													'id' => 'legalterms',
													'style' => 'font-size:15px; width:20px; height:20px; vertical-align:middle;'
											    ]) !!}
													<label class="control-label " for="legalterms">
														@lang('messages.accept_terms', ['link' => '<a href="javascript:;" style="font-weight:bold;">'.__('messages.legal_terms').'</a>'])
													</label>
												</div>
											</div>
											<BR>
											<div class="form-group col-md-12">
												<div class="text-right">
													<a href="javascript:;" class="btn btn-inverse" onclick="sendForm(this)"><i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')</a>
												</div>
											</div>
											{{--<div class="form-group col-md-12">--}}
											{{--<div class="text-right">--}}
											{{--<a id="btnPrev_{{ $step }}" class="btn btnPrev btn-sm btn-inverse btn-lg" onclick="showStep({{ $step - 1 }})"><i class="fa fa-angle-left"></i> @lang('messages.previous')</a>--}}
											{{--<a id="btnSgte_{{ $step }}" class="btn btnNext btn-sm btn-inverse btn-lg" onclick="showStep({{ $step + 1 }})">@lang('messages.next') <i class="fa fa-angle-right"></i></a>--}}
											{{--</div>--}}
											{{--</div>--}}
										</div>
									</td>
								</tr>
							</table>
						</div>
						<!-- END col-6 -->

					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->

@endsection

@push('scripts')
{{--<script src="https://js.stripe.com/v3/"></script>--}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>--}}
<script>

	var elements = '';
	var ccard = '';
	var ncard = '';
	var cecard = '';
	var cvcard = '';
	var stripe = '';
	$.getScript('https://js.stripe.com/v3/').done(function() {
		// Create a Stripe client.
//		stripe = Stripe('pk_test_DfIQrd547tyZOojRBU6cD5YH');    // MZ
//		stripe = Stripe('pk_test_jo9qYMCsfdSwFBb1N4ay8vdR');    // JB
		stripe = Stripe({{ env('STRIPE_KEY_PK') }});    // JB

		// Create an instance of Elements.
		elements = stripe.elements();

		// Custom styling can be passed to options when creating an Element.
		// (Note that this demo uses a wider set of styles than the guide below.)
		var style = {
			base: {
				color: '#32325d',
				lineHeight: '18px',
				fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing: 'antialiased',
				fontSize: '16px',
				'::placeholder': {
					color: '#aab7c4'
				}
			},
			invalid: {
				color: '#fa755a',
				iconColor: '#fa755a'
			}
		};

		// Create an instance of the card Element.
//		ccard = elements.create('card', {style: style});
		ncard = elements.create('cardNumber', {style: style});
		cecard = elements.create('cardExpiry', {style: style});
		cvcard = elements.create('cardCvc', {style: style});

		// Add an instance of the card Element into the `card-element` <div>.
//		ccard.mount('#card-element');
		ncard.mount('#card-number');
		cecard.mount('#card-expiry');
		cvcard.mount('#card-cvc');
	});

	function stripeTokenHandler(token) {
		// Insert the token ID into the form so it gets submitted to the server
		var form = document.getElementById('payment-form');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);

		// Submit the form
		form.submit();
	}

	// ----------
	$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
		initForm();
	});
	$.getScript('https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js').done(function(){
//		$('input').iCheck({
//			checkboxClass: 'icheckbox_minimal',
//			radioClass: 'iradio_minimal',
//			increaseArea: '20%' // optional
//		});
	});
	var stepNum = 1;
	var cats = {!! json_encode($categorias) !!};
	$(document).ready(function(){
//		initForm();

		@if($categoria != 0)
			changeSubs(document.getElementById('categoria'));
		@endif

		//		$( "#payment-form" ).submit(function( event ) {
		//			if($('#legalterms').prop('checked')) {
		//				sendForm(document.getElementById('submitPayment'));
		//			} else {
		//				console.log("checked");
		//				alert('You must read and accept the Legal terms and conditions before submitting your payment.')
		//
		//			}
		//			event.preventDefault();
		//		});

		$( "#formLoginPub" ).submit(function( event ) {
			tryLoginPub();
			event.preventDefault();
		});

		$( "#formRegisterPub" ).submit(function( event ) {
			tryRegisterPub();
			event.preventDefault();
		});
	});

	var step6 = false;
	var currentStep = 1;

	function showStep(n){
		console.log("step " + n);
		var validation1 = true;
		if(n === 2){
			var inps = ['marca', 'modelo'];

			$.each(inps, function(v, k){
				if($('#'+k).val() === ''){
					$('#'+k).css({'border':'1px solid #ff0000'});
					$('#errorDisplay_'+k).attr('data-toggle', 'tooltip')
						.attr('data-title', '@lang('messages.required')')
						.removeClass('hide');
					validation1 = false;
				} else {
					$('#'+k).css({'border':'1px solid #ccd0d4'});
					$('#errorDisplay_'+k).addClass('hide');
				}
			});
			$("[data-toggle=tooltip]").tooltip();
			if(validation1 === false){
				return false;
			}
			var data = {
				'marca' : $('#marca').val(),
				'modelo' : $('#modelo').val()
			};
			updateSession(data);
		}
		if(n === 3 || n === 4 || n === 5){

			if( n=== 3){
				inps = ['categoria', 'subcategoria'];
			}
			if( n=== 4){
				inps = ['pais', 'ciudad', 'codigo_postal', 'direccion'/*, 'ubicacion'*/];
			}
			if( n=== 5){
				inps = ['longitud', 'ancho', 'alto', 'peso_bruto'];
			}
			console.log(inps);
			validation1 = true;
			$.each(inps, function(v, k){
				if($('#'+k).val() === ''){
					console.log("campo: " + k);
					console.log($('#'+k).val())
					$('#'+k).css({'border':'1px solid #ff0000'});
					$('#errorDisplay_'+k).attr('data-toggle', 'tooltip')
						.attr('data-title', '@lang('messages.required')')
						.removeClass('hide');
					validation1 = false;
				} else {
					$('#'+k).css({'border':'1px solid #ccd0d4'});
					$('#errorDisplay_'+k).addClass('hide');
				}
			});
			$("[data-toggle=tooltip]").tooltip();
			if(validation1 === false){
				return false;
			}

			if(n === 3){
				data = {
					'categoria' : $('#categoria').val(),
					'subcat' : $('#subcategoria').val()
				};
			}
			if(n === 4){
				data = {
					'pais' : $('#pais').val(),
					'ciudad' : $('#ciudad').val(),
					'pcode' : $('#codigo_postal').val(),
					'direccion' : $('#direccion').val(),
//					'ubicacion' : $('#ubicacion').val(),
				};
			}
			if(n === 5){
				data = {
					'longitud' : $('#longitud').val(),
					'ancho' : $('#ancho').val(),
					'alto' : $('#alto').val(),
					'peso' : $('#peso_bruto').val(),
					'palet' : ($('#paletizado').prop('checked')) ? 1 : 0,
				};
			}
			updateSession(data);

			if(n === 5) {
				var scat = $('#subcategoria').val();
				if(scat === '' || scat < 1){
					return false;
				}
				if(currentStep < 5){
					getForm(scat);
				} else {
					step6 = false;
				}

			}
		}

		if(n === 6){
			if(step6 !== true){
				validateForm(function(){
					step6 = true;
					showStep(6);
				});
				return false;
			}
		}


		for(var i = 1 ; i <= 6 ; i++){
			if(i < n){
				$('.step_'+i+' input, .step_'+i+' number, .step_'+i+' select, .step_'+i+' textarea').each(function(e){
					$(this).attr('disabled','disabled');
				});
				$('.step_'+i+' .btnNext, .step_'+i+' .btnPrev').each(function(e){
					$(this).removeAttr('onclick');
					$(this).attr('disabled','disabled');
				});

			} else {
				$('.step_'+i+' input, .step_'+i+' number, .step_'+i+' select, .step_'+i+' select').each(function(e){
					$(this).removeAttr('disabled');
				});
				$('.step_'+i+' .btnPrev').each(function(e){
					$(this).removeAttr('disabled');
					$(this).attr('onclick', 'showStep('+(i-1)+')');
				});
				$('.step_'+i+' .btnNext').each(function(e){
					$(this).removeAttr('disabled');
					$(this).attr('onclick', 'showStep('+(i+1)+')');
				});
			}

		}
		$('tr.step_'+(n+1)).hide('slow');
		$('tr.step_'+n).show('slow');

		$('html, body').animate({
			scrollTop: $('tr.step_'+n).offset().top
		}, 1000);

		currentStep = n;
	}

	function changeSubs(e){
		cat = $(e).val();

		$('#subcategoria').html('');
		$.each(cats, function(k,v){
			if(v.id == cat){
				subs = v.subcategorias;
				$('#subcategoria').append($('<option>', {
					'value': ''
				}).html('@lang("messages.select_subcategory")'));
				$.each(subs, function(k1, v1){
					$('#subcategoria').append($('<option>', {
						'value': v1.id
					}).html(v1.nombre));
				});
			}
		});

		@if($subcat != 0)
			$('#subcategoria').val({{ $subcat }});
		@endif
	}

	//	function loadForm(e){
	//		scat = $(e).val();
	//		getForm(scat);
	//	}
	//	getForm(12);
	function getForm(scat){

		var url = '{{ route('public.publishForm', [0]) }}';
		url = url.replace(0, scat);
		var data = {
			idCategoria: scat,
		};

		$('#formHolder').html("@lang('messages.loading')...");

		$('#formHolder').load(url, function(response, status, xhr){
			if(xhr.status == 500){
				response = response.replace('position: fixed;', '');
				$(this).html(response);
			}

			{{--console.log($('#dropzone'));--}}
			{{--$('#dropzone').dropzone({--}}
			{{--url: '{{ route('public.publishUpload') }}'--}}
			{{--});--}}

			//			var myDropzone = new Dropzone("div#dropzone", { url: "/file/post"});
		});
	}

	function getModelos(e){
		var marca = $(e).val();
		console.log(marca);
		var url = '{{ route('public.getModelos') }}';
		var data = {
			marca : marca
		};

		ajaxPost(url, data, {
			onSuccess: function(data){
				if(data.data.length > 0){
					$('#modelo').autocomplete({
						source: data.data
					});
				}
			}
		});

	}

	function initForm(){
		var availableTags = {!! json_encode($marcas) !!};
		$('#marca').autocomplete({
			source: availableTags
		});
	}

	function updateSession(data){
		var url = '{{ route('public.updateSession') }}';

		ajaxPost(url, data, {
			onValidationOverride: true,
			onSuccess: function(data){
			},
			onFailure: function(data){
			},
			onDone: function(data){
			},
		});
	}

	var map = null;
	var geocoder = null;
	var marker = null;
	function initPublishMap() {
		var uluru = {lat: -25.363, lng: 131.044};
		map = new google.maps.Map(document.getElementById('publish_map'), {
			zoom: 4,
			center: uluru,
//			query: 'Bolivia'
		});

		geocoder = new google.maps.Geocoder();

		var address = $("#pais option:selected").text();
		geocodeAddress(geocoder, map, address);

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng, map);
		});

//		var marker = new google.maps.Marker({
//			position: uluru,
//			map: map
//		});
	}

	function placeMarker(location, map) {
		if (marker === null){
			marker = new google.maps.Marker({
				position: location,
				map: map
			});
		} else {
			marker.setPosition(location);
		}

//		marker = new google.maps.Marker({
//			position: location,
//			map: map
//		});
		map.panTo(location);
	}
	function geocodeAddress(geocoder, resultsMap, address) {
//		var address = 'Bolivia';
//		var address = $("#pais option:selected").text();
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);
//				var marker = new google.maps.Marker({
//					map: resultsMap,
//					position: results[0].geometry.location
//				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	function buscarEnMapa(){
		var address = $("#direccion").val();
		map.setZoom(15);
		geocodeAddress(geocoder, map, address);
	}

	{{--@push('mapsInit')--}}
	{{--initPublishMap();--}}
	{{--@endpush--}}

	// ----------

	function tryLoginPub(){
		console.log("tryloginpub");

		$('#regPubBtnEntrar').html('<i class="fas fa-circle-notch fa-spin"></i> Enviando...');
		$('#regPubBtnEntrar').attr('disabled', 'disabled');

		var url = '{{ route('public.login') }}';
		var data = {
			email: $('#loginPubUsername').val(),
			password: $('#loginPubPassword').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
//				redirect('/cuenta');
				$('.login-form-pub').addClass('hide');
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				console.log("validation");
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
				console.log(e);
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				console.log(errors);
				var message = '';
				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message += errors[elem].join('<br>') + '<br>';
					} else {
						message += errors[elem];
					}

				}
				$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});
	}

	function tryRegisterPub() {

		$('#regPubBtnEnviar').html('<i class="fas fa-circle-notch fa-spin"></i> Enviando...');
		$('#regPubBtnEnviar').attr('disabled', 'disabled');

		var url = '{{ route('public.register') }}';
		var data = {
			email: $('#regPubUsername').val(),
			apellidos: $('#regPubApellidos').val(),
			nombres: $('#regPubNombres').val(),
			pais: $('#regPubPais').val(),
			telefono: $('#regPubTelefono').val(),
			password: $('#regPubPassword').val(),
			password_confirmation: $('#regPubPassword2').val(),
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				redirect('/cuenta');

				$('#uli_nombre').html(data.data.owner.agenda.nombres_apellidos);
				$('#uli_email').html(data.data.username);
				$('#uli').removeClass('hide');
//				redirect('/cuenta');
				$('.register-form-pub').addClass('hide');
			},
			onError: function(e){
				$('#regPubBtnEntrar').html('@lang('messages.log_in')');
				$('#regPubBtnEntrar').removeAttr('disabled');
			},
			onValidation: function(e){
				$('#regPubBtnEnviar').html('@lang('messages.send')');
				$('#regPubBtnEnviar').removeAttr('disabled');
//				var errors = JSON.parse(e.responseText);
				var errors = e;
				var message = '';

				var elements = [
					'email',
					'apellidos',
					'nombres',
					'pais',
					'telefono',
					'password',
					'password_confirmation'
				];

				for(var elem2 in elements){
					$('#regPubError_'+elements[elem2]).html('');
				}

				for(var elem in errors) {
					if(Array.isArray(errors[elem])){
						message = errors[elem].join('<br>') + '<br>';
					} else {
						message = errors[elem];
					}
					$('#regPubError_'+elem).html('<span class="help-block"><strong>'+message+'</strong></span>')
				}
//					$('#loginPubErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});

	}

	function activateLoginRegisterPub(action){
		$('.login-form-pub, .register-form-pub').addClass('hide');
		$('.'+action+'-form-pub').removeClass('hide');
	}

	function calc_price(e){
		var val = $('#days').val();
		var price = val * 4.5;
		$('#newprice').html("<b>" + price + "Kr.</b>");
	}

</script>
@endpush