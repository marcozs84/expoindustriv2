@extends('frontend.layouts.default')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet"/>
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet"/>
@endpush

@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.my_announces')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">

				@include('frontend.pages.private.component.sidebarmenu', ['selection' => 'my_announcements'])

				<!-- BEGIN account-body -->
				<div class="account-body" style="min-height:800px;">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">
							<h4>@lang('messages.my_announces')</h4>
							<p>
								@lang('messages.my_announces.leftmessage')
							</p>
							<br>
							<div class="row m-b-10">
								<div class="col-md-4">
									{!! Form::select('estado', [
										\App\Models\Publicacion::ESTADO_APPROVED => __('messages.approved'),
										\App\Models\Publicacion::ESTADO_DRAFT => __('messages.draft'),
										\App\Models\Publicacion::ESTADO_AWAITING_APPROVAL => __('messages.awaiting_approval'),
										\App\Models\Publicacion::ESTADO_REJECTED => __('messages.rejected'),
										\App\Models\Publicacion::ESTADO_DISABLED => __('messages.inactive'),
									], null, [
										'id' => 'estado',
										'class' => 'form-control',
										'placeholder' => __('messages.status'),
										'onchange' => 'refresh_table()'
										]) !!}
								</div>
								<div class="col-md-4">
								</div>
								<div class="col-md-4">
									<a href="{{ route('public.publish') }}" class="btn btn-warning width-full">
										<i class="fa fa-plus"></i> @lang('messages.create_announce')
									</a>
								</div>
							</div>

							<table id="data-table" class="table table-striped width-full"></table>

						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

<script>

	var datatable = null;
	var modal = null;

	$( document ).ready( function () {
		initDataTable();
	} );

	var estadosPub = {!! json_encode($estados)  !!};

	function initDataTable() {
		if ( !$.fn.DataTable.isDataTable( '#data-table' ) ) {
			datatable = $( '#data-table' ).DataTable( {
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
				searching: false,
				language: {
					url: '{{ $global_dtlang }}'
				},
				buttons: [
					{
						text: 'Nuevo',
						action: function ( e, dt, node, config ) {
							alert( 'Button activated' );
						}
					}
				],
				ajax: {
					url: '{{ route('private.announcements.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function ( d ) { // Procesa datos enviados
//	                d.element = $('#element').val();
						d.estado = $('#estado').val();
//						d.removed = $('#eliminados').val();
						// ...
					},
					dataFilter: function ( data ) { // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{data: "id", title: 'Id'},
					{title: '@lang('messages.product')'},
					{title: '@lang('messages.validity')'},
					{data: 'visitas', title: '@lang('messages.visits')'},
					{data: 'producto.precio_publicacion', title: '@lang('messages.price')'},
					{title: '@lang('messages.state')'},
					{data: 'fechaInicio', title: '@lang('messages.publication_date')'},
{{--					{data: '', title: '@lang('messages.invoice')'},--}}
					{data: '', title: ''},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
						visible: false
					}, {
						targets: 1,
						render: function ( data, type, row ) {
							var nombre = '';
							if ( row.producto.marca != undefined ) {
										{{--var url = '{{ route("private.{$glc}.announcements.show", [0]) }}';--}}
								var url = '{{ route('public.announce.preview', ['idAnnouncement']) }}';
								url = url.replace( 'idAnnouncement', row.id );
								nombre = row.producto.nombre_alter;
							}

							if( nombre == '' ) {
								if( row.producto.producto_item.traducciones.length > 0 ) {
									nombre = row.producto.producto_item.traducciones[0].se;
								}
							}
							return '<a href="' + url + '">' + nombre + '</a>';
						}
					}, {
						targets: 2,
						render: function ( data, type, row ) {

							return format_date( row.fechaInicio ) + ' <br>' + format_date( row.fechaFin );
						}
					}, {
						targets: 4,
						render: function ( data, type, row ) {
							{{--return data + ' <span style="text-transform:uppercase;">' + /*row.moneda*/ '{{ strtoupper(session('current_currency', 'usd')) }}' + '</span>';--}}
								return row.producto.precio_publicacion + ' <span style="text-transform:uppercase;">{{ strtoupper(session('current_currency', 'usd')) }}</span>';
						}
					}, {
						targets: 5,
						className: 'text-center',
						render: function ( data, type, row ) {
							var html = '';
							if( row.estado == {{ \App\Models\Publicacion::ESTADO_APPROVED }}) {
								html = '<i class="text-primary fa fa-check"></i>';
							} else if( row.estado == {{ \App\Models\Publicacion::ESTADO_REJECTED }} ) {
								html = '<i class="text-danger fa fa-times"></i>';
							} else if( row.estado == {{ \App\Models\Publicacion::ESTADO_DRAFT }} ) {
								html = '<i class="text-info fa fa-eraser"></i>';
							} else if( row.estado == {{ \App\Models\Publicacion::ESTADO_DISABLED }} ) {
								html = '<i class="text-info fa fa-eraser"></i>';
							} else if( row.estado == {{ \App\Models\Publicacion::ESTADO_AWAITING_APPROVAL }} ) {
								html = '<a href="javascript:;" data-tooltip="Pending approval">PENDING</a>';
							} else {
								html = row.estado;
							}
							return html;
//							return estadosPub[row.estado];
						}
					}, {
						targets: 6,
						className: 'text-center',
						width: '150px',
						render: function ( data, type, row ) {
							return format_date( data );
						}
					{{--}, {--}}
						{{--targets: 7,--}}
						{{--className: 'text-center',--}}
						{{--width: '2px',--}}
						{{--render: function ( data, type, row ) {--}}

							{{--var html = '';--}}
							{{--var url = '{{ route("private.{$glc}.announcements.invoice", ['idAnnouncement']) }}';--}}
							{{--url = url.replace('idAnnouncement', row.id);--}}
							{{--html += '<a href="'+ url +'" class="" data-toggle="tooltip" data-title="@lang('messages.go_to_invoice')"><i class="fas fa-eye"></i></a>';--}}

								{{--return html;--}}
						{{--}--}}
					}, {
						targets: 7,
						className: 'text-center',
						width: '80px',
						render: function ( data, type, row ) {

							var html = '';
							var urlEdit = '{{ route("private.{$glc}.announcements.edit", ['idAnuncio']) }}';
							urlEdit = urlEdit.replace('idAnuncio', row.id);

							html += '<a href="'+ urlEdit +'" class="m-r-10" data-toggle="tooltip" data-title="@lang('messages.edit')"><i class="fa fa-edit"></i></a>';
							html += '<a href="javscript:;" onclick="removeAnnouncement(' + row.id + ')" class="m-r-10" data-toggle="tooltip" data-title="@lang('messages.remove')"><i class="fa fa-trash-alt"></i></a>';

							return html;
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $( '#header' ).height()
				},
				bSort: false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			} ).on( 'draw.dt', function () {
				$( '#idsChecker' ).on( 'change', function ( e ) {
					$( '.chkId' ).prop( 'checked', $( this ).prop( 'checked' ) );
				} );

				$( '[data-toggle="tooltip"]' ).tooltip();
			} );
		} else {
			datatable.ajax.reload();
		}

	}

	function editAnnouncement( id ) {

		swal( {
			text: '@lang('messages.wanna_remove_announcement')',
			icon: 'warning',
			buttons: {
				cancel: '@lang('form.no')',
				ok: '@lang('form.yes')'
			},
			dangerMode: true
		} ).then( function ( response ) {
			var url = '{{ route('private.announcements.delete', ['idAnnounce']) }}';
			url = url.replace( 'idAnnounce', id );

			if ( response === 'ok' ) {
				console.log( "removing announcement" );
				ajaxDelete( url, {}, function () {
					swal( '@lang('messages.announcement_removed')', {icon: "success"} );
					datatable.ajax.reload( null, false );
				} );
			}

		} );


	}

	function removeAnnouncement( id ) {

		swal( {
			text: '@lang('messages.wanna_remove_announcement')',
			icon: 'warning',
			buttons: {
				cancel: '@lang('form.no')',
				ok: '@lang('form.yes')'
			},
			dangerMode: true
		} ).then( function ( response ) {
			var url = '{{ route('private.announcements.delete', ['idAnnounce']) }}';
			url = url.replace( 'idAnnounce', id );

			if ( response === 'ok' ) {
				console.log( "removing announcement" );
				ajaxDelete( url, {}, function () {
					swal( '@lang('messages.announcement_removed')', {icon: "success"} );
					datatable.ajax.reload( null, false );
				} );
			}

		} );


	}

	function refresh_table(){
		datatable.ajax.reload();
	}

</script>

@endpush