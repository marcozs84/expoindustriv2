@extends('frontend.layouts.default')

@push('css')

<link href="/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />

<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />

<link href="/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />

<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />

<style>
	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}

	.image_file {
		margin-bottom:30px;
	}
</style>

<style>
	.tblProveedor tr th {
		text-align:center;
		font-size:14px;
	}
	.tblProveedor tr td {
		vertical-align: top;
		padding-left:10px;
	}
	.tblProveedor {
		width:100%
	}
	.tblLabel {
		font-weight:bold;
		text-align: right;
	}
	h4 {
		clear:both;
	}

	.account-body h4 {
		padding-bottom:20px;
	}

	.col-form-label {
		padding-top:7px !important;
	}

	.select2-container {
		margin-top:0px !important;
	}
	.select2-container .select2-selection--single {
		height:34px !important;
	}
</style>
@endpush

@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.my_announces')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">

			@include('frontend.pages.private.component.sidebarmenu', ['selection' => 'my_announcements'])

			<!-- BEGIN account-body -->
				<div id="formProductoEdit" class="account-body" style="min-height:800px;">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="/*table-responsive*/">
							<h2>{{ $producto->nombre_alter }}</h2>
							{{--<p>--}}
								{{--@lang('messages.my_announces.leftmessage')--}}
							{{--</p>--}}
							{{--<div class="pull-right">--}}
								{{--@if($puedeCrear)--}}
								{{--<a href="{{ route('public.publish') }}" class="btn btn-primary">--}}
									{{--<i class="fa fa-plus"></i> @lang('messages.create_announce')--}}
								{{--</a>--}}
								{{--@endif--}}
							{{--</div>--}}


							<h4>
								@lang('messages.basic_information')
								<br>
								<small>@lang('messages.basic_information_descr')</small>
							</h4>

							<div class="row">

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.brand')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('marca', ($producto->Marca) ? $producto->Marca->nombre : '', [
											'id' => 'marca',
											'placeholder' => __('messages.brand'),
											'class' => 'form-control',
											'onblur' => 'getModelos(this)',
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.model')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('modelo', ($producto->Modelo) ? $producto->Modelo->nombre : '', [
											'id' => 'modelo',
											'placeholder' => __('messages.model'),
											'class' => 'form-control',
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.category')</label>
									<div class="col-md-8 col-sm-8">

										{!! Form::select('categoria', $categorias->pluck('traduccion.'.App::getLocale(), 'id'), ($producto->Categorias->count() > 0) ? $producto->Categorias->first()->Padre->id : null, [
											'id' => 'categoria',
											'placeholder' => __('messages.select_category'),
											'class' => 'form-control',
											'onchange' => 'changeSubs(this)',
										]) !!}

										{{--{!! Form::text('categoria', $producto->Categorias->first()->Padre->Traduccion->$lang, [--}}
											{{--'id' => 'categoria',--}}
											{{--'class' => 'form-control',--}}
										    {{--'placeholder' => __('messages.select_category'),--}}
											{{--'disabled' => 'disabled'--}}
										{{--]) !!}--}}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.subcategory')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::select('subcategoria', $subcats_list, ($producto->Categorias->count() > 0) ? $producto->Categorias->first()->id : null, [
											'id' => 'subcategoria',
											'class' => 'form-control',
											'placeholder' => __('messages.select_category'),
											//'onchange' => 'loadForm()',
										]) !!}
										{{--{!! Form::text('subcategoria', $producto->Categorias->first()->Traduccion->$lang, [--}}
											{{--'id' => 'subcategoria',--}}
											{{--'class' => 'form-control',--}}
											{{--'placeholder' => __('messages.select_category'),--}}
											{{--'disabled' => 'disabled'--}}
										{{--]) !!}--}}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.status')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::select('esNuevo', [
											1 => __('messages.new'),
											2 => __('messages.used'),
										], $producto->esNuevo, [
											'id' => 'esNuevo',
											'class' => 'form-control default-select2',
											//'onchange' => 'loadForm()',
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.type')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::select('idProductoTipo', [
											1 => __('messages.machines'),
											2 => __('messages.accesories'),
										], $producto->idProductoTipo, [
											'id' => 'idProductoTipo',
											'class' => 'form-control default-select2',
											//'onchange' => 'loadForm()',
										]) !!}
									</div>
								</div>

							</div>




							<h4>
								@lang('messages.machine_location')
								<br>
								<small>@lang('messages.machine_location_descr', ['route' => route('public.dataProtectionPolicy')])</small>
							</h4>
							<p></p>

							<div class="row">
								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.country')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::select('pais', $global_paises, $producto->ProductoItem->Ubicacion->pais, [
											'id' => 'pais',
											'class' => 'form-control',
										    'placeholder' => __('messages.country'),
											//'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.city')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('ciudad', $producto->ProductoItem->Ubicacion->ciudad, [
											'id' => 'ciudad',
											'placeholder' => __('messages.city'),
											'class' => 'form-control',
											//'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.postal_code')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('codigo_postal', $producto->ProductoItem->Ubicacion->cpostal, [
											'id' => 'codigo_postal',
											'placeholder' => __('messages.postal_code'),
											'class' => 'form-control',
											//'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4">@lang('messages.address')</label>
									<div class="col-md-8 col-sm-12">
										{!! Form::textarea('direccion', $producto->ProductoItem->Ubicacion->direccion, [
										'id' => 'direccion',
										'class' => 'form-control',
										'rows' => 4,
										'placeholder' => __('messages.address'),
										//'disabled' => 'disabled'
									]) !!}
									</div>
								</div>

							</div>

							<h4>
								@lang('messages.transport_info')
								<br>
								<small>@lang('messages.transport_info_descr')</small>
							</h4>

							<div class="row">

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.longitude_mm')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('longitud', $producto->ProductoItem->dimension['lng'], [
											'id' => 'longitud',
											'placeholder' => __('messages.longitude_mm'),
											'class' => 'form-control',
											'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.width_mm')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('ancho', $producto->ProductoItem->dimension['wdt'], [
											'id' => 'ancho',
											'placeholder' => __('messages.width_mm'),
											'class' => 'form-control',
											'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.height_mm')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('alto', $producto->ProductoItem->dimension['hgt'], [
											'id' => 'alto',
											'placeholder' => __('messages.height_mm'),
											'class' => 'form-control',
											'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4">@lang('messages.weight_brute')</label>
									<div class="col-md-8 col-sm-8">
										{!! Form::text('peso_bruto_fix', $producto->ProductoItem->dimension['wgt'], [
											'id' => 'peso_bruto_fix',
											'placeholder' => __('messages.weight_brute'),
											'class' => 'form-control',
											'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-4 col-sm-4 col-xs-2">@lang('messages.palletized')</label>
									<div class="col-md-8 col-sm-8 col-xs-10">
										{!! Form::checkbox('paletizado', 1, $producto->ProductoItem->dimension['pal'], [
											'id' => 'paletizado',
											'class' => '/*form-control*/',
											'style' => 'margin-top: 10px;',
											'disabled' => 'disabled'
										]) !!}
									</div>
								</div>

							</div>


							<h4>@lang('messages.price')</h4>


							<div class="row">

								<div class="form-group col-md-6 col-sm-8 row m-b-15">
									<label class="col-form-label col-md-6 col-sm-6">@lang('messages.price_southamerica') <br><small>( @lang('messages.exclude_moms') )</small></label>
									<div class="col-md-6 col-sm-6">
										{!! Form::text('precio', $producto->precioProveedor, [
											'id' => 'precio',
											'class' => 'form-control',
										]) !!}
									</div>
								</div>

								<div class="form-group col-md-6 col-sm-4 row m-b-15">
									<div class="col-md-12">
										{!! Form::select('currency', $moneda, $producto->monedaProveedor, [
											'id' => 'currency',
											'class' => 'form-control',
										]) !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6 col-sm-8 row m-b-15">
									<label class="col-form-label col-md-6 col-sm-6">@lang('messages.price_europa') <br><small>( @lang('messages.exclude_moms') )</small></label>
									<div class="col-md-6 col-sm-6">
										{!! Form::text('precioEuropa', $producto->precioEuropa, [
											'id' => 'precioEuropa',
											'placeholder' => '',
											'class' => 'form-control',
										]) !!}
									</div>
								</div>
								<div class="form-group col-md-6 col-sm-4 row m-b-15">
									<div class="col-md-12 col-sm-12">
										{!! Form::select('currencyEuropa', $moneda, $producto->monedaEuropa, [
											'id' => 'currencyEuropa',
											'class' => 'form-control',
										]) !!}
									</div>
								</div>
							<div class="row">
							</div>
								<div class="form-group col-md-12 col-sm-12 row m-b-15">
									<label class="col-form-label col-md-6 col-sm-6 col-xs-6">@lang('messages.price_southamerica_only')</label>
									<div class="col-md-6 col-sm-6 col-xs-6">
										{!! Form::checkbox('priceSouthamerica', 1, $producto->precioSudamerica, [
											'id' => 'priceSouthamerica',
											'class' => '/*form-control*/',
											'style' => 'margin-top: 10px;',
										]) !!}
									</div>
								</div>

							</div>

							<h4>@lang('messages.announce_detail')</h4>

							<div id="formHolder" class="/*hide*/"></div>




							@php
								$nombre = $producto->Traducciones->where('campo', 'nombre')->first();
								$nombre_se = $nombre_en = $nombre_es = '';
								if($nombre) {
									$nombre_se     = $nombre->se;
									$nombre_en     = $nombre->en;
									$nombre_es     = $nombre->es;
								}

								$resumen = $producto->Traducciones->where('campo', 'resumen')->first();
								$resumen_se = $resumen_en = $resumen_es = '';
								if($resumen) {
									$resumen_se     = $resumen->se;
									$resumen_en     = $resumen->en;
									$resumen_es     = $resumen->es;
								}

								$descripcion = $producto->Traducciones->where('campo', 'descripcion')->first();
								$descripcion_se = $descripcion_en = $descripcion_es = '';
								if($descripcion) {
									$descripcion_se     = $descripcion->se;
									$descripcion_en     = $descripcion->en;
									$descripcion_es     = $descripcion->es;
								}
							@endphp

							<h5>@lang('messages.name')</h5>
							<div class="row">
								<div class="col-md-12">
									{!! Form::text('nombre_se', $nombre_se, [
										'id' => 'nombre_se',
										'class' => 'form-control'
									]) !!}
								</div>

							</div>

							<br>
							<h5>@lang('messages.summary')</h5>
							<div class="row m-t-10">

								<div class="col-md-12">
									{!! Form::textarea('resumen_se', $resumen_se, [
										'id' => 'resumen_se',
										'class' => ''
									]) !!}
								</div>
							</div>

							<br>
							<h5>@lang('messages.description')</h5>
							<div class="row m-t-10">

								<div class="col-md-12">
									{!! Form::textarea('descripcion_se', $descripcion_se, [
										'id' => 'descripcion_se',
										'class' => ''
									]) !!}
								</div>
							</div>



							<h4>@lang('messages.gallery')</h4>

							<div id="gallery" class="gallery row m-b-20">
								@foreach($producto->Galerias as $galeria)
									@foreach($galeria->Imagenes as $imagen)
										<div class="col-md-2 col-sm-4 col-xs-4 image_file" style="" id="prod_img_{{ $imagen->id }}">
											<div class="document-file text-center" style="min-height:120px;">
												<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-{{ $producto->id }}">
													<img src="{{ $imagen->ruta_publica_producto_thumb }}" alt="" style=" max-height:120px; max-width:100%;" />
												</a>
											</div>
											<div class="document-name text-center m-t-10">
												{{--<a href="javascript:;">{{ $imagen->filename }}</a>--}}
												{{--<br>--}}
												<a href="javascript:;" onclick="imagenEliminar({{ $imagen->id }})" class="btn btn-danger" style="margin: auto; text-align:center;"><i class="fa fa-times"></i> @lang('messages.remove')</a>
											</div>
										</div>
									@endforeach
								@endforeach
							</div>


							<div id="group_imagenes">
								<form action="{{ route('private.announcements.editUpload', [ 'id' => $producto->id ]) }}" id="dropzone_edit_announcement" class="dropzone">
									<input type="hidden" name="_token" value="{{ csrf_token() }}" id="dropToken">
									<input type="hidden" name="idProducto" value="{{ $producto->id }}" id="dropToken">
									<div class="dz-message" data-dz-message><span>@lang('messages.dz_upload')</span></div>
								</form>
							</div>

							{{--<div id="gallery" class="gallery row">--}}
								{{--@foreach($producto->Galerias as $galeria)--}}
									{{--<ul class="attached-document clearfix">--}}
										{{--@foreach($galeria->Imagenes as $imagen)--}}
											{{--<li class="fa-camera" id="prod_img_{{ $imagen->id }}">--}}
												{{--<div class="document-file">--}}
													{{--<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-{{ $producto->id }}">--}}
														{{--<img src="{{ $imagen->ruta_publica_producto_thumb }}" alt="" />--}}
													{{--</a>--}}
												{{--</div>--}}
												{{--<div class="document-name">--}}
													{{--<a href="javascript:;">{{ $imagen->filename }}</a>--}}
													{{--<br>--}}
													{{--<a href="javascript:;" onclick="imagenEliminar({{ $imagen->id }})" style="margin: auto; text-align:center;">Eliminar</a>--}}
												{{--</div>--}}

											{{--</li>--}}
										{{--@endforeach--}}
									{{--</ul>--}}
								{{--@endforeach--}}
							{{--</div>--}}

							<hr>

							<div class="form-group col-md-12">
								<div class="text-center">
									<a class="btn btn-white btn-lg" data-dismiss="modal" href="{{ route("private.{$glc}.announcements") }}"><i class="fa fa-reply"></i> @lang('messages.my_announces')</a>
									<a href="javascript:;" class="btn btn-primary btn-lg" onclick="productoGuardar(this)" ><i class="fa fa-save"></i> @lang('messages.save')</a>
								</div>
							</div>



							<div class="hide">
								<ul>
									<li class="fa-camera" id="stub_li_imagen_old">
										<div class="document-file">
											<a href="#" data-lightbox="gallery-group-prodid">
												<img src="" alt="" />
											</a>
										</div>
										<div class="document-name">
											<a href="javascript:;"></a>
											<br>
											<a href="javascript:;" class="linkdelete" onclick="imagenEliminar()" style="margin: auto; text-align:center;">@lang('messages.remove')</a>
										</div>
									</li>
								</ul>


								<div class="col-md-2 col-sm-4 col-xs-4 image_file" style="" id="stub_imagen">
									<div class="document-file text-center" style="min-height:120px;">
										<a href="" class="ilink" data-lightbox="gallery-group-{{ $producto->id }}">
											<img src="" alt="" style=" max-height:120px; max-width:100%;" />
										</a>
									</div>
									<div class="document-name text-center m-t-10">
										{{--<a href="javascript:;">{{ $imagen->filename }}</a>--}}
										{{--<br>--}}
										<a href="javascript:;" onclick="imagenEliminar()" class="btn btn-danger" style="margin: auto; text-align:center;"><i class="fa fa-times"></i> @lang('messages.remove')</a>
									</div>
								</div>

							</div>





						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection

@push('scripts')


<script src="/assets/plugins/select2/dist/js/select2.min.js"></script>

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>

<script src="/assets/plugins/ckeditor/ckeditor.js"></script>

<script>

	$.getScript('/assets/plugins/jquery-ui/jquery-ui.min.js').done(function() {
		formInit();
	});

	var dropzone_edit_announcement = '';
	$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
		Dropzone.autoDiscover = false;
		setTimeout(function(){
			{{--$('div#dropzone').dropzone({--}}
					{{--url: '{{ route('public.publishUpload') }}'--}}
					{{--});--}}

				dropzone_edit_announcement = new Dropzone("#dropzone_edit_announcement", {
				url: "{{ route('private.announcements.editUpload', [ 'id' => $producto->id ]) }}",
				addRemoveLinks: true,
				autoProcessQueue: true,
				parallelUploads: 1,
				maxFilesize: 3, // MB
				init: function(){
					{{--@foreach($producto->Galerias as $galeria)--}}
						{{--@foreach($galeria->Imagenes as $image)--}}

							{{--var mockFile = {--}}
								{{--name: '{{ $image['filename'] }}',--}}
								{{--size: 123,--}}
							{{--};--}}
							{{--this.options.addedfile.call(this, mockFile);--}}
							{{--this.options.thumbnail.call(this, mockFile, '{{ $image['ruta_publica_producto'] }}');--}}
							{{--this.emit('thumbnail', mockFile, '{{ $image['ruta_publica_producto_thumb'] }}');--}}
							{{--this.emit('complete', mockFile);--}}

						{{--@endforeach--}}
					{{--@endforeach--}}





						this.on("removedfile", function(file) {

							console.log('Eliminado: ' + file.name);
//					alert(file.name);


						{{----}}
						{{--// Create the remove button--}}
						{{--var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");--}}

						{{--//Capture the Dropzone instance as closure.--}}
						{{--var _this = this;--}}

						{{--// Listen to the click event--}}
						{{--//					removeButton.addEventListener();--}}

						{{--// Add the button to the file preview element.--}}
						{{--file.previewElement.appendChild(removeButton);--}}

						});
				},
				success: function(file, response){
					console.log("success");
					console.log(file.name);
					console.log(response);

					file.previewElement.remove();

					if(response.result === true) {

						imagen = response.data;

						var holder = $('#stub_imagen').clone();

						holder.attr( 'id', 'prod_img_' + imagen.id );
						holder.find( '.ilink' ).attr( 'href', imagen.ruta_publica_producto );
						holder.find( '.ilink' ).attr( 'data-lightbox', 'gallery-group-{{ $producto->id }}' );
						holder.find( '.ilink img' ).attr( 'src', imagen.ruta_publica_producto_thumb );
						holder.find( '.btn' ).attr( 'onclick', 'imagenEliminar(' + imagen.id + ')' );
						$('#gallery').append(holder);

						$('#dropzone_edit_announcement').removeClass('dz-started');


					}
				},
//			error: function(file, errormessage, data) {
//				console.log("error -----------------------------");
//				console.log(file);
//				console.log(errormessage);
//				console.log(data);
//				console.log(data.status);
//				if( data.status === 422 ) {
//					var errors = $.parseJSON(data.responseText);
//					$.each(errors, function (key, value) {
//						// console.log(key+ " " +value);
////					$('#response').addClass("alert alert-danger");
//
//						if($.isPlainObject(value)) {
//							$.each(value, function (key, value) {
//								console.log(key+ " " +value);
////							$('#response').show().append(value+"<br/>");
//								$('.dz-error-message span').append(value+"<br/>");
//
//							});
//						}else{
////						$('#response').show().append(value+"<br/>"); //this is my div with messages
//							$('.dz-error-message span').append(value+"<br/>");
//						}
//					});
//				}
//			},
				removedfile: function(file){
					x = confirm('@lang('messages.confirm_removal')');
					if(!x) return false;
					removeImage(1, file.name);
//				for(var i = 0 ; i < file_up_names.length; i++){
//					if(file_up_names[i] === file.name){
//						ajaxPost('public.publishRemove', {
//							'archivo' : ''
//						});
//					}
//				}
				}
			});

			dropzone_edit_announcement.on('error', function(file, errormessage, data) {
				if( data.status === 422 ) {

					console.log("error 422");

					var errorMessageElement = $(file.previewElement).find('.dz-error-message');
					errorMessageElement.html('')
					try {
						var errors = $.parseJSON(data.responseText);
					} catch(e) {
						errorMessageElement.append(data.responseText+"<br/>");
						return false;
					}

					$.each(errors, function (key, value) {
						 console.log(key+ " " +value);
						if($.isPlainObject(value)) {
							$.each(value, function (key, value) {
//								console.log(key+ " " +value);
								errorMessageElement.append(value+"<br/>");

							});
						}else{
							errorMessageElement.append(value+"<br/>");
						}
					});
				}
			});

		}, 1000);

		$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
		$("[data-toggle=tooltip]").tooltip();


	});


	var idUsuarioPropietario = {{ $producto->Owner->id }}; // idUsuario
	var campos = {!! json_encode($campos) !!};
	var availableTags = {!! json_encode($marcas) !!};

	function formInit(){

		CKEDITOR.replace( 'resumen_se' );

		CKEDITOR.replace( 'descripcion_se' );

		$("[data-toggle=tooltip]").tooltip();

		$('#marca').autocomplete({
			source: availableTags
		});

		$('#pais').select2();

	}

	function productoGuardar(elem){
		var btnHtml = $(elem).html();
		$(elem).html('Saving...');
		var url = '{{ route('private.announcements.update', [ $producto->id ]) }}';

		var data = {
			marca: $('#marca').val(),
			modelo: $('#modelo').val(),
			esNuevo: $('#esNuevo').val(),
			idProductoTipo: $('#idProductoTipo').val(),

			//		idCategoria: $('#subcategoria').val(),
			//
			pais:           $('#pais').val(),
			ciudad:         $('#ciudad').val(),
			codigo_postal:  $('#codigo_postal').val(),
			direccion:      $('#direccion').val(),
//
//			longitud:       $('#longitud').val(),
//			ancho:          $('#ancho').val(),
//			alto:           $('#alto').val(),
//			peso_bruto_fix: $('#peso_bruto_fix').val(),
//			paletizado: ( $('#paletizado').prop('checked') ) ? 1 : 0 ,
//
			idUsuario: idUsuarioPropietario, //$('#searchCliente').val(),
			precio: $('#precio').val(),
			currency: $('#currency').val(),
			precioVenta: $('#precioVenta').val(),
			currencyVenta: $('#currencyVenta').val(),
//		precioMinimo: $('#precioMinimo').val(),
//		currencyMinimo: $('#currencyMinimo').val(),
			precioEuropa: $('#precioEuropa').val(),
			currencyEuropa: $('#currencyEuropa').val(),
			priceSouthamerica: ($('#priceSouthamerica').prop('checked')) ? 1 : 0,
			//
			nombre_se:      $('#nombre_se').val(),
			resumen_se:     CKEDITOR.instances.resumen_se.getData(),
			descripcion_se:     CKEDITOR.instances.descripcion_se.getData(),
			//
			multiselects: getKeys()
		};

		$.each(campos, function(k,v) {
//		console.log(v.key);
			data[v.key] = $('#'+v.key).val();
		});

		console.log(data);

		ajaxPatch(url, data, function(data){

			$(elem).html(btnHtml);

			swal({
				text: '@lang('messages.saved_succesfully'). @lang('messages.remember_review')',
				icon: 'success',
			});

			if ( dropzone_edit_announcement.files.length === 0 ) {
				$( document ).trigger( 'formProductoEdit:aceptar' );
			} else {
				dropzone_edit_announcement.processQueue();
			}
		}, null, {
			onDone: function() {
				$(elem).html(' @lang('messages.save') ');
			}
		});
	}

	function getModelos(e){
		var marca = $(e).val();
		var url = '{{ route('private.announcements.getModelos') }}';
		var data = {
			marca : marca
		};

		$('#modelo').val('');

		ajaxPost(url, data, {
			onSuccess: function(data){
				if(data.data.length > 0){
					$('#modelo').autocomplete({
						source: data.data
					});
				}
			}
		});
	}

	function openCloseDiv(id) {
		$('#'+id).toggleClass('hide');
	}

	function imagenEliminar(id) {

		swal({
			text: 'Está seguro que desea eliminar esta imágen de la galería?',
			icon: 'warning',
			buttons: {
				cancel: 'No',
				ok: 'Si'
			},
			dangerMode: true
		}).then(function(response) {
			if (response === 'ok') {
				var url = '{{ route('resource.imagen.destroy', [0]) }}';
				var ids = [id];
				ajaxDelete(url, ids, function(){
					swal('Imágen eliminada', { icon: "success" });
					$('#prod_img_'+id).remove();
				});
			}
		});

	}

	// ---------- MULTISELECT JS SCRIPTS
	var hashOptions = {};
	function initMultiselect(){

		$('.mselect').on('change', function(e){

		});
	}

	function registerOption(e){


		var id = $(e).attr('id');

		if(hashOptions[id] === undefined){
			hashOptions[id] = [];
		}

		hashOptions[id].push({
			key : $('#'+id).val(),
			text : $('#'+id + ' option:selected').text()
		});

		updateOptions(id);

	}

	function removeOption(id, op){
		console.log("buscando: ");
		var opIndex = -1;
		$.each(hashOptions[id], function(k, v){
			if(hashOptions[id][k].key == op) {
				opIndex = k;
			}
		});

		if(opIndex >= 0){
			hashOptions[id].splice(opIndex, 1);
		}

		updateOptions(id);
	}

	function updateOptions(id){

		$('#'+id).val('');
		$('#'+id + ' option').removeClass('hide');

		// ----------

		$('#mEle_'+id).html('');
		var ul = $('<ul>');

		$.each(hashOptions[id], function(k, v){

			$('#'+id + " option[value='"+v.key+"']").addClass('hide');

			// ----------

			var a = $('<a>', {
				'class': 'pull-right bclose',
				href: 'javascript:;',
				onclick: 'removeOption("'+id+'", "'+v.key+'")'
			}).html('<i class="fa fa-trash-alt"></i>');

			var li = $('<li>');
			li
				.append(v.text)
				.append(a);
			ul.append(li);
		});

		$('#mEle_'+id).append(ul);
	}

	function getKeys(){
		var optSelected = {};

		$.each(hashOptions, function(k,v){
			if(optSelected[k] === undefined){
				optSelected[k] = [];
			}

			$.each(hashOptions[k], function(i, l){
				optSelected[k].push(l.key);
			})
		});

		return optSelected;
	}

	var cats = {!! json_encode($categorias) !!};

	function changeSubs(e){
		cat = $(e).val();

		$('#subcategoria').html('');

		$.each(cats, function(k,v){

			console.log(v.id);
			if(v.id === parseInt(cat, 10)){
				subs = v.subcategorias;
				$('#subcategoria').append($('<option>', {
					'value': '',
					'selected': 'selected',
				}).html('@lang("messages.select_subcategory")'));
				$.each(subs, function(k1, v1){
					$('#subcategoria').append($('<option>', {
						'value': v1.id
					}).html(v1.nombre));

					{{--}).html(v1.traduccion.{{ App::getLocale() }}));--}}
				});
			}
		});
	}

</script>

@endpush