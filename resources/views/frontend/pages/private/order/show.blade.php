@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li><a href="{{ route("private.{$glc}.order.index") }}">@lang('messages.my_orders')</a></li>
				<li class="active">@lang('messages.order_detail')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar">
					<div class="account-sidebar-cover">
						<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />
					</div>
					<div class="account-sidebar-content">
						<h4>@lang('messages.order_detail')</h4>
						<p>
							Se despliega un resumen del estado de su orden/cotización.
						</p>
						<p>
							Por favor no dude en contactarnos si requiere asistencia.
						</p>
					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">
							<!-- BEGIN checkout-body -->
							<div class="checkout-body">
								<!-- BEGIN checkout-message -->
								<div class="checkout-message">
									<h1>Cotización en progreso <small>Estamos trabajando para brindarle la información que usted necesita.</small></h1>
									<div class="table-responsive2">
										<table class="table table-payment-summary">
											<tbody>
											<tr>
												<td class="field">@lang('messages.status')</td>
												<td class="value">{{ $estados[$order->estado] }}</td>
											</tr>
											<tr>
												<td class="field">@lang('messages.paysuc_referenceno')</td>
												<td class="value">REF-{{ str_pad($order->id, 4, '0', STR_PAD_LEFT) }}</td>
											</tr>
											<tr>
												<td class="field">@lang('messages.due_date')</td>
												<td class="value">{{ $order->created_at->addDays(5)->format('d M Y H:i') }}</td>
											</tr>
											<tr>
												<td class="field">@lang('messages.product')</td>
												<td class="value product-summary">
													<div class="product-summary-img hide">
														<img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" />
													</div>
													<div class="product-summary-info">
														<div class="title"><a href="{{ route('public.producto', $order->Publicacion->id) }}" style="/*color: #333;*/">{{ $order->Publicacion->Producto->Marca->nombre }} {{ $order->Publicacion->Producto->Modelo->nombre }}</a></div>
														<div class="desc">Publicado el {{ $order->created_at->addDays(5)->format('d/m/Y H:i') }}</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="field">@lang('messages.price')</td>
												<td class="value">{{ $order->definicion['precio_final'] }} {{ (isset($order->definicion['moneda'])) ? strtoupper($order->definicion['moneda']) : '' }}</td>
											</tr>
											</tbody>
										</table>
									</div>
									<p class="text-muted text-center m-b-0">Si usted requiere asistencia adicional, puede ponerse en contacto con un miembro de nuestro equipo a través del siguiente número: (123) 456-7890</p>
								</div>
								<!-- END checkout-message -->
							</div>
							<!-- END checkout-body -->
						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection