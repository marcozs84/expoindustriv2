@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.my_orders')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">

				@include('frontend.pages.private.component.sidebarmenu', ['selection' => 'my_orders'])

				<!-- BEGIN account-body -->
				<div class="account-body" style="min-height:800px;">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">

							<h4>@lang('messages.my_orders')</h4>
							<p>
								@lang('messages.my_orders.leftmessage')
							</p>
							<br>
							<table id="data-table" class="table table-striped width-full table-condensed"></table>

						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

<script>

	var datatable = null;
	var modal = null;

	$(document).ready(function(){
		initDataTable();
	});

	var estadosOrd = {!! json_encode($estados)  !!};

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
				searching: false,
				language: {
					url: '{{ $global_dtlang }}'
				},
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('private.order.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: '@lang('messages.product')'},
					{ title: '@lang('messages.date')'},
					{ title: '@lang('messages.price')'},
					{ title: '@lang('messages.status')'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
							if(row.publicacion.producto.marca != undefined){
										{{--var url = '{{ route("private.{$glc}.announcements.show", [0]) }}';--}}
								var url = '{{ route("private.{$glc}.order.show", [0]) }}';
								url = url.replace('0', row.id);
								return '<a href="'+url+'">'+ row.publicacion.producto.marca.nombre + ' ' + row.publicacion.producto.modelo.nombre +'</a>';
							}
						}
					},{
						targets: 2,
						render: function(data, type, row){
							return row.created_at_format;
						}
					},{
						targets: 3,
						render: function(data, type, row){
							moneda = '';
							if( row.definicion.moneda ) {
								moneda = row.definicion.moneda.toUpperCase();
							}
							return row.definicion.precio_final.toFixed(2) + " " + moneda;
						}
					},{
						targets: 4,
						className: 'text-center',
						render: function(data, type, row){
							return estadosOrd[row.estado];
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

</script>
@endpush