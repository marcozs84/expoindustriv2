@push('css')
<style>

	.account-container .account-sidebar .account-sidebar-cover:before {
		background-color:#ffffff !important;
	}



	.account-container .account-sidebar {
		padding: 20px 0px;
	}

	.account-sidebar h4 {
		/*color:#ffffff;*/
		/*color:#d9c400;*/  /* AMARILLO */
		/*color:#232a30;   !* GRIS OSCURO *!*/

		color:#348fe2;   /* AZUL */
		font-weight:bold;
		/*margin-top:20px;*/
		padding: 10px 20px 5px 15px;
		/*border-top: 1px solid rgba(255, 255, 255, 0.5) !important;*/
		/*border-bottom: 1px solid rgba(72, 72, 72, 0.3) !important;*/
		/*text-shadow: 0px 1px rgba(255,255,255,0.5);*/
		font-size:16px !important;
		/*font-weight:600 !important;*/
		/*border-top: 1px solid rgba(255, 255, 255, 0.2);*/
		/*background: linear-gradient(90deg, rgba(255,255,255,0.5) 0%, rgba(255,255,255,0) 100%);*/
		background-color:#e5e5e5;

		border-top: 1px solid #ececec;
		border-bottom: 1px solid #348fe2;
		background: linear-gradient(0deg, rgba(229, 229, 229, 0.7) 0%, rgba(255,255,255,0) 100%);
	}
	.account-sidebar h4 i {
		/*color:#ffffff;*/
		/*color:#d9c400;*/  /* AMARILLO */
		/*color:#232a30;   !* GRIS OSCURO *!*/
		color:#348fe2;   /* AZUL */
		text-shadow: 0px 1px rgba(255,255,255,0.5);
	}

	.account-sidebar ul {
		padding:0px 0px;
	}

	.account-sidebar ul li a {
		/*color:#ffffff;*/
		color:#232a30;   /* GRIS OSCURO */
		margin:0px !important;
		/*margin-left:0px !important;*/
		padding-left:40px !important;
		padding-right:0px;
		padding-top:7px;
		padding-bottom:7px;
		/*font-weight: 600;*/

		transition: .3s ease;
	}
	.account-sidebar ul li a i.fa-caret-left {
		font-weight: 600;
		font-size:20px;
		color:#fff;
		display:none;
		margin-right: -1px;
	}

	.account-sidebar ul li a:hover {
		/*color:#000000;*/
		color:#ffffff;

		/*background-color:#232a30;*/    /* GRIS OSCURO */
		/*background-color:#348fe2;  !* AZUL *!*/
		background-color:#f59c1a;  /* NARANJA WARNING */

		/*background-color: rgba(255, 255, 255, 0.7);*/
		/*background: linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,0.5) 100%);*/
		/*-webkit-border-radius: 5px;*/
		/*-moz-border-radius: 5px;*/
		/*border-radius: 5px;*/
	}

	.account-sidebar ul li a:hover i.fa-caret-left {
		display:block;
	}

	.account-sidebar ul li a.active i.fa-caret-left {
		display:block;
	}
	.account-sidebar ul li a.active {
		/*color:#000000;*/
		/*color:#232a30;*/
		color:#ffffff;

		/*background-color:#232a30;*/    /* GRIS OSCURO */
		/*background-color:#348fe2;  !* AZUL *!*/
		background-color:#f59c1a;  /* NARANJA WARNING */
		/*background-color: rgba(255, 255, 255, 0.7);*/
		/*background: linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,0.5) 100%);*/
		/*margin:10px;*/
		/*margin:5px 0px 5px 0px !important;*/
		font-weight:bold;
		/*-webkit-border-radius: 5px;*/
		/*-moz-border-radius: 5px;*/
		/*border-radius: 5px;*/
	}

	@media( max-width:500px ) {
		.account-sidebar {
			margin-bottom:20px;
		}
	}

	.btnAnnouncementAccount {
		background: linear-gradient(to bottom, rgba(236,214,0,1) 0%,rgba(177,161,34,1) 100%) !important;
		border-radius: 5px;
		margin-bottom:20px !important;
		color:#232a30 !important;
		/*font-weight:bold;*/
		margin-left:20px !important;
		padding:10px !important;
		font-size:16px;
	}


	.account-sidebar-fixed {
		/*position: fixed;*/
		/*margin-top: 155px;*/
		/*left: calc((102% - 1169px) / 2);*/
	}


	@media (min-width:1900px) {
		.account-sidebar-fixed {
			/*margin-top:176px;*/
			/*left:calc((102% - 1164px) /2) !important;*/
		}
	}
	@media (max-width:1920px) {
		.account-sidebar-fixed {
			/*margin-top:208px;*/
			/*left:calc((102% - 1364px) /2) !important;*/
		}
	}
	@media (max-width:1700px) {
		.account-sidebar-fixed {
			/*margin-top:174px;*/
			/*left:calc((102% - 1167px) /2) !important;*/
		}
	}
	/*@media (min-width:1200px) {*/
		/*.account-sidebar-fixed {*/
			/*position: fixed !important;*/
			/*margin-top:176px;*/
			/*left:calc((102% - 1164px) /2) !important;*/
		/*}*/
	/*}*/
	@media (max-width:1200px) {
		.account-sidebar-fixed {
			/*margin-top:175px;*/
			/*left:calc((102% - 959px) /2) !important;*/
		}
	}
	@media (max-width:992px) {
	}
	@media (max-width:892px) {
	}
	@media (max-width:768px) {
	}
	@media (max-width:500px) {
	}

	.dataTable a{
		font-weight:bold;
		color:#232a30;
	}
	.dataTable a i{
		font-weight:normal;
		color:#348fe2;
	}
</style>
@endpush



	<!-- BEGIN account-sidebar -->
	<div class="account-sidebar account-sidebar-fixed" style="">
		<div class="account-sidebar-cover" style="/*background-image:url('/assets/img/e-commerce/cover/cover-14.jpg')*/">
			{{--<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />--}}
		</div>
		<div class="account-sidebar-content">

			{{--<ul class="nav nav-list" style="margin-bottom:10px; margin-left:20px; margin-right:20px;">--}}
				{{--<li><a href="{{ route('public.publish') }}" class="btnAnnouncementAccount" ><i class="fa fa-truck"></i>  @lang('messages.post_an_announcement')</a></li>--}}
			{{--</ul>--}}
			<ul class="nav nav-list">
				<li> <a href="{{ route("public.{$glc}.privCuenta") }}" class="@if( $selection == 'my_account') active @endif"> @lang('messages.my_account') <i class="fa fa-caret-left pull-right"></i></a></li>
			</ul>


			<h4><i class="fas fa-id-badge fa-fw "></i> @lang('messages.public_profile')</h4>
			<ul class="nav nav-list">
				<li> <a href="{{ route('private.providerProfileEdit') }}" class="@if( $selection == 'my_profile') active @endif"> @lang('messages.profile_info') <i class="fa fa-caret-left pull-right"></i></a></li>
			</ul>


			{{--<h4><i class="fas fa-gem fa-fw text-muted"></i> @lang('messages.orders')</h4>--}}
			{{--<ul class="nav nav-list">--}}

				{{--<li> <a href="{{ route("private.{$glc}.order.index") }}" class="@if( $selection == 'my_orders') active @endif"> @lang('messages.my_orders') <i class="fa fa-caret-left pull-right"></i></a></li>--}}
				{{--<li>  class="fa fa-chevron-right" <i<a href="{{ route('public.privConsultas') }}">Ver mis consultas privadas</a></li>--}}
				{{--<li>  class="fa fa-chevron-right" <i<a href="#">Ver mi historial de órdenes</a></li>--}}
				{{--<li> <a href="{{ route("public.{$glc}.privFavoritos") }}" class="@if( $selection == 'my_favs') active @endif"> @lang('messages.saved_announces') <i class="fa fa-caret-left pull-right"></i></a></li>--}}
			{{--</ul>--}}


			<h4><i class="fas fa-truck fa-fw "></i> @lang('messages.announcements')</h4>
			<ul class="nav nav-list">
				<li><a href="{{ route('public.publish') }}" class="" style="font-weight:bold;" >
						{{--<i class="fa fa-truck"></i>--}}
						@lang('messages.post_an_announcement')</a></li>
				<li> <a href="{{ route("private.{$glc}.announcements") }}" class="@if( $selection == 'my_announcements') active @endif"> @lang('messages.my_announces') <i class="fa fa-caret-left pull-right"></i></a></li>

			</ul>


			<h4><i class="fas fa-universal-access fa-fw text-muted"></i> @lang('messages.account_config')</h4>
			<ul class="nav nav-list">
				<li> <a href="{{ route('private.changePassword') }}" class="@if( $selection == 'my_password') active @endif"> @lang('messages.udate_mail_password') <i class="fa fa-caret-left pull-right"></i></a></li>
				{{--<li>  class="fa fa-chevron-right" <i<a href="#">Actualizar mis direcciones disponibles</a></li>--}}
			</ul>




			<h4><i class="fab fa-wpforms fa-fw "></i> @lang('messages.support')</h4>
			<ul class="nav nav-list">
				<li> <a href="{{ route('public.faq') }}" class="@if( $selection == 'my_faq') active @endif"> @lang('messages.frequent_questions') <i class="fa fa-caret-left pull-right"></i></a></li>
			</ul>
			{{-- @if($esProveedor) --}}
			{{-- ------------------- TEMPORALMENTE DESHABILITADO ---------------- --}}

		</div>
	</div>

@push('scripts')

<script>
	window.onscroll = function() {floatingMenuScroll()};

	// Get the navbar
//	var navbar = document.getElementById("navbar");

	// Get the offset position of the navbar
//	var sticky = navbar.offsetTop;
	var sticky = 200;

	// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
	function floatingMenuScroll() {
//		console.log(window.pageYOffset);
		var margin = 0;
		if (window.pageYOffset >= sticky && window.innerWidth > 770) {
//			console.log("STICK TO IT ");
			margin = window.pageYOffset - sticky;
//			$('.account-sidebar-fixed').css({'position' : 'fixed'})
			$('.account-sidebar-fixed').css({'top' : margin})
//			navbar.classList.add("sticky")
		} else {
//			console.log("GO NORMAL " + margin);
//			$('.account-sidebar-fixed').css({'position' : 'absolute'})
			$('.account-sidebar-fixed').css({'top' : margin})
//			navbar.classList.remove("sticky");
		}
	}
</script>

@endpush