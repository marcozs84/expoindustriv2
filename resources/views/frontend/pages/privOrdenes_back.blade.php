@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #product -->
	<div id="product" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="#">Inicio</a></li>
				<li><a href="#">Mi Cuenta</a></li>
				<li class="active">Ordenes</li>
			</ul>
			<!-- END breadcrumb -->

			<!-- BEGIN product -->
			<div class="product">
				<!-- BEGIN product-tab -->
				<div class="product-tab">
					<!-- BEGIN #product-tab-content -->
					<div id="product-tab-content" class="tab-content p-t-0">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">
							<!-- BEGIN table-product -->
							<table class="table table-listing table-striped ">
								<thead>
								<tr>
									<th># ID</th>
									<th>Producto</th>
									<th>Fecha</th>
									<th>Estado</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td class="field">15652</td>
									<td>
										<a href="/producto/123" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>En progreso</td>
								</tr>
								<tr>
									<td class="field">15652</td>
									<td>
										<a href="/producto/123" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>En progreso</td>
								</tr>
								<tr>
									<td class="field">15652</td>
									<td>
										<a href="/producto/123" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>En progreso</td>
								</tr>
								</tbody>
							</table>
							<!-- END table-product -->
						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END #product-tab-content -->
				</div>
				<!-- END product-tab -->
			</div>
			<!-- END product -->
		</div>
		<!-- END container -->
		<!-- BEGIN #trending-items -->
		<div id="trending-items" class="section-container bg-silver ">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN section-title -->
				<h4 class="section-title clearfix">
					<a href="#" class="pull-right m-l-5"><i class="fa fa-angle-right f-s-18"></i></a>
					<a href="#" class="pull-right"><i class="fa fa-angle-left f-s-18"></i></a>
					Los mas visitados
					<small>Los productos más visitados en el último més.</small>
				</h4>
				<!-- END section-title -->

				<!-- BEGIN row -->
				<div class="row row-space-10">
					<div class="col-md-2 col-sm-4">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="https://www.rbauction.com/cms_assets/images/s-pages/used-tractors-for-sale/2wd.jpg" alt="" />
								<div class="discount">15% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_detail.html" data-toggle="tooltip" data-title="Used Toyota Dyna three-way side tipper truck BU30 4x2 Diesel">Used Toyota Dyna three-way side tipper truck BU30 4x2 Diesel</a>
								</h4>
								<p class="item-desc">3D Touch. 12MP photos. 4K video.</p>
								<div class="item-price">$649.00</div>
								<div class="item-discount-price">$739.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END col-2 -->
					<!-- BEGIN col-2 -->
					<div class="col-md-2 col-sm-4">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="https://i.ytimg.com/vi/kAyJDB14dqE/maxresdefault.jpg" alt=""  />
								<div class="discount">32% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_alt.html" data-toggle="tooltip" data-title="Used Toyota Dyna dropside truck BU30 4x2 Diesel">Used Toyota Dyna dropside truck BU30 4x2 Diesel</a>
								</h4>
								<p class="item-desc">Super. Computer. Now in two sizes.</p>
								<div class="item-price">$599.00</div>
								<div class="item-discount-price">$799.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END col-2 -->
					<!-- BEGIN col-2 -->
					<div class="col-md-2 col-sm-4">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="http://www.fordrelease.net/wp-content/uploads/2017/05/ford-tractors-canada.jpg" alt="" />
								<div class="discount">20% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_alt.html" data-toggle="tooltip" data-title="Used Renault Midliner tarp truck S 100 4x2 Diesel">Used Renault Midliner tarp truck S 100 4x2 Diesel</a>
								</h4>
								<p class="item-desc">Retina. Now in colossal and ginormous.</p>
								<div class="item-price">$1,099.00</div>
								<div class="item-discount-price">$1,299.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END col-2 -->
					<!-- BEGIN col-2 -->
					<div class="col-md-2 col-sm-4">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="https://www.ameriquestcorp.com/images/content/small-banner-used-truck-tandem-sleeper.jpg" alt="" />
								<div class="discount">13% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_alt.html" data-toggle="tooltip" data-title="Used Renault Midliner tarp truck S 100 4x2 Diesel">Used Renault Midliner tarp truck S 100 4x2 Diesel</a>
								</h4>
								<p class="item-desc">You. At a glance.</p>
								<div class="item-price">$599.00</div>
								<div class="item-discount-price">$799.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END col-2 -->
					<!--
						</div>
					<div class="row row-space-10">
					-->
					<!-- BEGIN col-2 -->
					<div class="col-md-2 col-sm-4">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="https://st.mascus.com/imagetilewm/product/75f878a5/renault-premium-370-dci-with,86fb9410-6.jpg" alt="" />
								<div class="discount">15% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_detail.html" data-toggle="tooltip" data-title="Used Schmitz Cargobull tautliner semi-trailer Curtainsider 3 axles">Used Schmitz Cargobull tautliner semi-trailer Curtainsider 3 axles</a>
								</h4>
								<p class="item-desc">3D Touch. 12MP photos. 4K video.</p>
								<div class="item-price">$649.00</div>
								<div class="item-discount-price">$739.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
					<!-- END col-2 -->
					<!-- BEGIN col-2 -->
					<div class="col-md-2 col-sm-4">
						<!-- BEGIN item -->
						<div class="item item-thumbnail">
							<a href="product_detail.html" class="item-image">
								<img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt=""  />
								<div class="discount">32% OFF</div>
							</a>
							<div class="item-info">
								<h4 class="item-title">
									<a href="product_alt.html" data-toggle="tooltip" data-title="Used Volvo F10 two-way side tipper truck 320 6x2 Diesel">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
								</h4>
								<p class="item-desc">Super. Computer. Now in two sizes.</p>
								<div class="item-price">$599.00</div>
								<div class="item-discount-price">$799.00</div>
							</div>
						</div>
						<!-- END item -->
					</div>
				</div>
				<!-- END row -->
			</div>
			<!-- END container -->
		</div>
		<!-- END #trending-items -->
	</div>
	<!-- END #product -->

@endsection