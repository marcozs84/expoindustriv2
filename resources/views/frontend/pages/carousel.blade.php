@extends('frontend.layouts.min')

@push('css')
<style>
	.halfCover{
		opacity: 0.9;
		filter: alpha(opacity=90); /* For IE8 and earlier */
		background-color:#000000;

	}
	.title {
		font-size:21px !important;
	}

	.carousel-caption p{
		font-weight:500;
		font-size:30px;
	}
</style>
@endpush
@section('content')


	<!-- BEGIN #slider -->
	<div id="slider" class="section-container p-0 bg-black-darker">
		<!-- BEGIN carousel -->
		<div id="main-carousel" class="carousel slide" data-ride="carousel">
			<!-- BEGIN carousel-inner -->
			<div class="carousel-inner">

				@for($i = 0 ; $i < count($imagenes) ; $i++)
					<div class="item @if($i == 0) active @endif">
						<img src="{{ $imagenes[$i] }}" class="bg-cover-img cover-special" alt="" />
						<div class="container fadeInRightBig animated">

						</div>
						<div class="carousel-caption carousel-caption-right">
							<div class="container">

							</div>
						</div>
					</div>
				@endfor

			</div>
			<!-- END carousel-inner -->
			<a class="left carousel-control" href="#main-carousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="right carousel-control" href="#main-carousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
		<!-- END carousel -->
	</div>
	<!-- END #slider -->




@endsection

@push('scripts')
<script>
	$(document).ready(function(){
	});
</script>
@endpush