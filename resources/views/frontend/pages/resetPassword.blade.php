@extends('frontend.layouts.default')
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-13.jpg" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.confirm_cuenta')<b></b></h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.search") }}">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.confirm_cuenta')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN panel-group -->
			<div class="panel-group faq" id="faq-list">


				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-close fa-fw m-r-5"></i> {{ $titulo }}
						</h4>
					</div>
					<div id="faq-1" class="panel ">
						<div class="panel-body">

							@if($pass_changed)
								<p class="alert alert-success">
									@lang('messages.password_reseted')
									<a href="{{ route("public.{$glc}.privCuenta") }}" style="">@lang('messages.my_account')</a>
								</p>
							@endif

							<form id="formResetPassword"  action="{{ route('public.resetPassword', ['token' => $token]) }}" method="post" class="text-center @if($pass_changed) hide @endif">
								{{ csrf_field() }}
								<p>
									@lang('messages.write_new_password')
								</p>

								@if ($errors->any())
									<div class="alert alert-danger">
											@foreach ($errors->all() as $error)
												{{ $error }}<br>
											@endforeach
									</div>
								@endif

								<input type="password" name="password" id="password" class="login-input" placeholder="@lang('messages.password')" >
								<input type="password" name="password_confirmation" id="password_confirmation" class="login-input" placeholder="@lang('messages.password_confirmation')" >

								{{--<input type="submit" id="regTopBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
								<button type="submit" id="regTopBtnForgot" form="formResetPassword" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
								{{--<a href="javascript:;" id="regTopBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}
							</form>

							{{--<div class="text-center">--}}
								{{--<a href="/" class="btn btn-inverse btn-lg" >Continuar <i class="fa fa-angle-right"></i></a>--}}
							{{--</div>--}}

						</div>
					</div>
				</div>
				<!-- END panel -->

			</div>
			<!-- END panel-group -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection

@push('scripts')
<script>
	@if($pass_changed)
	$(document).ready(function() {
		setTimeout(function(){
			redirect('{{ route("public.{$glc}.privCuenta") }}');
		}, 5000);
	});
	@endif
</script>
@endpush