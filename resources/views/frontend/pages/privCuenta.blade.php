@extends('frontend.layouts.default')
@push('css')
<style>


	.table-planes tr td, .table-planes tr th{
		text-align:center;
	}


	.panel-heading {
		font-weight: bold;
		font-size: 20px;
		text-align: center;
	}

	.inputError {
		border:1px solid #ff0000 !important;
	}

	.bg_blue_degradee {
		background: rgb(81,136,218);
		background: -moz-linear-gradient(-45deg, rgb(52, 170, 255) 0%, rgba(52, 143, 226, 1) 100%) !important;
		background: -webkit-linear-gradient(-45deg, rgb(52, 170, 255) 0%,rgba(52, 143, 226, 1) 100%) !important;
		background: linear-gradient(to bottom, rgb(52, 170, 255) 0%,rgba(52, 143, 226, 1) 100%) !important;
	}

</style>
<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />
@endpush
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li class="active">@lang('messages.my_account')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">

				@include('frontend.pages.private.component.sidebarmenu', ['selection' => 'my_account'])

				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body" style="min-height:800px;">
					<!-- BEGIN row -->
					<div class="row">
						<div class="col-md-12">

							<h3 style="margin-top:0px;">@lang('messages.welcome') {{ $auth_nombres_apellidos }}!</h3>
							<h4 style="margin-top:20px;">
								{{--<span class="fa-stack ">--}}
								  {{--<i class="fa fa-circle fa-stack-2x"></i>--}}
								  {{--<i class="fa fa-check fa-stack-1x fa-inverse"></i>--}}
								{{--</span>--}}
								@lang('messages.active_subscriptions')

								<a class="btn btn-warning pull-right m-t-5 m-b-5" href="javascript:;" onclick="$('.inactive_subs').toggleClass('hide')" ><i class="fa fa-eye"></i> @lang('messages.view_all_your_plans')</a>
							</h4>

							<br>
							<table class="table table-striped table-planes table-bordered">
								<tr>
									<th>Plan</th>
									<th>@lang('messages.due_date')</th>
									<th>@lang('messages.status')</th>
								</tr>
								@foreach($subscripciones as $subscripcion)

									<tr>
										<td class="text-left">{{ $subscripcion->Tipo->texto }}</td>
										<td>{{ $subscripcion->fechaHasta->format('d.m.Y') }}</td>
										<td>
											@if( \Carbon\Carbon::today() > $subscripcion->fechaHasta->subDays(31))
												<a href="javascript:;" onclick="requestRenewal({{ $subscripcion->id }})" class="btn btn-white btn-md pull-right m-t-20 width-150">
													@lang('messages.renew')
												</a>
											@else
												<span class="badge badge-primary" style="padding-left:20px; padding-right:20px;">@lang('messages.active')</span>
											@endif
										</td>
									</tr>

								@endforeach

								@foreach($subscripcionesInactive as $subscripcion)
									<tr class="inactive_subs hide">
										<td class="text-left">{{ $subscripcion->Tipo->texto }}</td>
										<td>{{ $subscripcion->fechaHasta->format('d.m.Y') }}</td>
										<td><span class="badge badge-danger" style="padding-left:20px; padding-right:20px;">@lang('messages.overdue')</span></td>
									</tr>
								@endforeach

							</table>

							<div class="panel panel-info" hidden>
								<div class="panel-heading">@lang('messages.active_subscriptions')</div>
								<div class="panel-body bg-aqua text-white">
									@forelse($subscripciones as $subscripcion)

										@if(\Carbon\Carbon::today() >$subscripcion->fechaHasta->subDays(31))
											<a href="javascript:;" onclick="requestRenewal({{ $subscripcion->id }})" class="btn btn-white btn-md pull-right m-t-20 width-150">@lang('messages.renew')</a>
											<h3>Plan: {{ $subscripcion->Tipo->texto }}
												{{--- <small class="text-inverse f-w-600">@lang('messages.requires_action')</small>--}}
											</h3>
											<span class="text-blue">@lang('messages.due_date'): {{ $subscripcion->fechaHasta->format('d.m.Y') }}</span>
										@else
											<h3>Plan: {{ $subscripcion->Tipo->texto }}</h3>
											<span class="text-black">@lang('messages.due_date'): {{ $subscripcion->fechaHasta->format('d.m.Y') }}</span>
										@endif
										<hr>
									@empty
										<h5>@lang('messages.no_active_plans')</h5><br>
									@endforelse
										<div class="stats-link" style="background-color:rgba(0,0,0,.4); padding:15px;">
											<a href="javascript:;" onclick="$('.inactive_subs').toggleClass('hide')" style=" color:rgba(255,255,255,0.7);">@lang('messages.view_all_your_plans')</a>
										</div>

									<div class="inactive_subs hide">
										@foreach($subscripcionesInactive as $subscripcion)
											<h3>Plan: {{ $subscripcion->Tipo->texto }} - <small class="text-inverse f-w-600">@lang('messages.overdue')</small></h3>
											<span class="text-blue">@lang('messages.due_date'): {{ $subscripcion->fechaHasta->format('d.m.Y') }}</span>
											<hr>
										@endforeach
									</div>

								</div>
							</div>

							<hr style="clear: both; width:100%;">
							<h4>
								@lang('messages.most_visited') <a class="btn btn-warning pull-right m-t-5 m-b-5" href="{{ route("private.{$glc}.announcements") }}"><i class="fa fa-eye"></i> @lang('messages.view_all')</a>
							</h4>
							<br>
							<table class="table table-valign-middle">
								<tr>
									<th class="text-center" style="border-top:none;">@lang('messages.product')</th>
									<th class="text-center" style="border-top:none;">@lang('messages.visits')</th>
									<th style="border-top:none;"></th>
								</tr>
								<tbody>

								@foreach($masVisitados as $pub)
									<tr>
										<td><a href="{{ route('public.producto', [$pub->id]) }}" target="_blank">{{ $pub->Producto->Marca->nombre }} {{ $pub->Producto->Modelo->nombre }}</a></td>
										<td class="text-center">{{ $pub->visitas }} </td>
										<td class="text-center" style="height:50px;">
										@if($pub->Producto->Galerias->count() > 0)
											@php $imagen = $pub->Producto->Galerias->first()->Imagenes->first(); @endphp
											@if( $imagen )
											<a href="{{ $imagen->rutaPublicaProducto }}" data-lightbox="gallery-group-{{ $pub->id }}">
												<img src="{{ $imagen->rutaPublicaProductoThumb }}" alt="" style="max-width:30px;" />
											</a>
											@endif

										@endif
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
						<!-- BEGIN col-6 -->






						<div class="col-12">
							{{--<h4>--}}
								{{--<span class="fa-stack fa-1x">--}}
								  {{--<i class="fa fa-circle fa-stack-2x"></i>--}}
								  {{--<i class="far fa-circle fa-stack-1x fa-inverse"></i>--}}
								  {{--<i class="fa fa-search fa-stack-1x fa-inverse"></i>--}}
								{{--</span>--}}
								{{--@lang('messages.acc_serv_sales')--}}
							{{--</h4>--}}

							<hr style="clear: both; width:100%;">

							{{--<div class=" ">--}}
								{{--<div class="panel-heading" style="font-size:30px; line-height: 20px;">--}}
									{{--<span class="fa-stack fa-1x">--}}
										{{--<i class="fa fa-search fa-stack-1x"></i>--}}
									{{--</span>--}}
									{{--@lang('messages.acc_serv_sales')--}}
								{{--</div>--}}
								{{--<div class="panel-body " style="background-color:#ffffff; -webkit-box-shadow: -5px 0px   #348fe2;-moz-box-shadow: -5px 0px   #348fe2;box-shadow: -5px 0px   #348fe2; -webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">--}}
									{{--<div class="row">--}}
										{{--<div class="col-md-3 col-sm-3" style="text-align:center;">--}}
											{{--<i class="fa fa-globe-americas fa-3x" style="text-shadow: 3px 3px rgba(0,0,0,0.20);"></i>--}}
										{{--</div>--}}
										{{--<div class="col-md-9 col-sm-9" style="background-color:#eff2f4;">--}}
											{{--<table style=" height:50px;">--}}
												{{--<tr>--}}
													{{--<td style="vertical-align: middle; text-align:justify;">--}}
														{{--<p>@lang('messages.acc_serv_sales_body')</p>--}}
													{{--</td>--}}
												{{--</tr>--}}
											{{--</table>--}}

										{{--</div>--}}
									{{--</div>--}}
									{{--<hr style="margin:7px;">--}}
									{{--<div class="row">--}}
										{{--<div class="col-md-3 col-sm-3" style="text-align:center;">--}}
											{{--<i class="fa fa-users fa-3x" style="text-shadow: 3px 3px rgba(0,0,0,0.20);"></i>--}}
										{{--</div>--}}
										{{--<div class="col-md-9 col-sm-9" style="background-color:#eff2f4;">--}}
											{{--<table style=" height:50px;">--}}
												{{--<tr>--}}
													{{--<td style="vertical-align: middle; text-align:justify;">--}}
														{{--<p>@lang('messages.acc_serv_sales_body2')</p>--}}
													{{--</td>--}}
												{{--</tr>--}}
											{{--</table>--}}
										{{--</div>--}}
									{{--</div>--}}

								{{--</div>--}}
							{{--</div>--}}
							<h4 class="section-title clearfix ">@lang('messages.news')</h4>
							<br>
							<div style="background-color:#ffffff; /*border: 1px solid #c5ced4; padding: 10px;*/">
								<table>
									@for( $i = 0 ; $i < count($noticias) ; $i++)
										<tr class="news_link">
											<td style="width:100px; padding-right:10px;">
												<small class="pull-right">{{ $noticias[$i]->fechaPublicacion->format('d.m.Y') }}</small>
											</td>
											<td style="padding-left:10px;">
												<a href="javascript:;" style="padding:10px; border-left:1px solid #e5e5e5; display:block;" onclick="showNews({{ $noticias[$i]->id }})">{{ (trim($noticias[$i]['titulo_'.App::getLocale()]) == '') ? '- - -' : $noticias[$i]['titulo_'.App::getLocale()] }}</a>
											</td>
										</tr>
										@if($i < count($noticias) - 1)
											<tr>
												<td colspan="2" style="padding:5px;"><hr style="border-color:#e5e5e5; margin:0px;"></td>
											</tr>
										@endif
									@endfor
								</table>
							</div>

							{{--<br>--}}
							{{--<h4>--}}

								{{--<span class="fa-stack fa-1x">--}}
								  {{--<i class="fa fa-circle fa-stack-2x"></i>--}}
								  {{--<i class="fa fa-chart-line fa-stack-1x fa-inverse"></i>--}}
								{{--</span>--}}
								{{--@lang('messages.acc_serv_sales2')--}}
							{{--</h4>--}}
							<br>
							{{--<div class="panel ">--}}
								{{--<div class="panel-heading" style="font-size:30px; line-height: 35px;">--}}
									{{--@lang('messages.acc_serv_sales2_title')--}}
								{{--</div>--}}
								{{--<div class="panel-body bg-white" style=" -webkit-box-shadow: -5px 0px   #348fe2;-moz-box-shadow: -5px 0px   #348fe2;box-shadow: -5px 0px   #348fe2; -webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">--}}
									{{--<div class="row">--}}
										{{--<div class="col-md-3" style="text-align:center;">--}}
											{{--<table style="height:100%; width:100%; min-height:145px; text-align: center;">--}}
												{{--<tr>--}}
													{{--<td cla style="vertical-align: middle;">--}}
														{{--<i class="fa fa-cogs fa-3x" style="text-shadow: 3px 3px rgba(0,0,0,0.20);"></i>--}}
													{{--</td>--}}
												{{--</tr>--}}
											{{--</table>--}}

										{{--</div>--}}
										{{--<div class="col-md-9 bg_blue_degradee" style="padding:20px;">--}}
											{{--<table style="height:100%; width:100%; min-height:50px;">--}}
												{{--<tr>--}}
													{{--<td class=" text-white" style="vertical-align: middle; text-align:justify;">--}}
														{{--<p>@lang('messages.acc_serv_sales2_body')</p>--}}
														{{--<div class="text-right">--}}
															{{--<a href="javascript:;" class="btn btn-inverse" onclick="modalContactConsultant()" style=" font-size:14px; font-weight:bold; padding:10px 20px; width:30%;">@lang('messages.contact_us')</a>--}}
														{{--</div>--}}
													{{--</td>--}}
												{{--</tr>--}}
											{{--</table>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}

							{{--<div class="panel">--}}
								{{--<div class="panel-heading" style="font-size:30px; line-height: 20px;">--}}
									{{--<span class="fa-stack fa-1x">--}}
										{{--<i class="fa fa-crosshairs fa-stack-1x fa-inverse"></i>--}}
									{{--</span>--}}
									{{--@lang('messages.acc_serv_sales3_title')--}}
								{{--</div>--}}
								{{--<div class="panel-body bg-blue text-white">--}}



									{{--<div class="row">--}}
										{{--<div class="col-md-3" style="text-align:center;">--}}
											{{--<table style="height:100%; width:100%; min-height:115px; text-align: center;">--}}
												{{--<tr>--}}
													{{--<td style="vertical-align: middle;">--}}
														{{--<i class="fa fa-check-double fa-6x" style="text-shadow: 3px 5px rgba(0,0,0,0.20);"></i>--}}
													{{--</td>--}}
												{{--</tr>--}}
											{{--</table>--}}

										{{--</div>--}}
										{{--<div class="col-md-9">--}}
											{{--<table style="height:100%; width:100%; min-height:115px;">--}}
												{{--<tr>--}}
													{{--<td style="vertical-align: middle; font-size:18px; text-align:justify;">--}}
														{{--<p>@lang('messages.acc_serv_sales3_body')</p>--}}
														{{--<div class="text-right">--}}
															{{--<a href="{{ route('public.publish') }}" class="btn btn-inverse" style=" font-size:14px; font-weight:bold; padding:10px 20px; width:30%;">@lang('messages.announce_here')</a>--}}
														{{--</div>--}}
													{{--</td>--}}
												{{--</tr>--}}
											{{--</table>--}}
										{{--</div>--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}

						</div>





						<div class="col-md-6" hidden>
							<h4><i class="fas fa-gem fa-fw text-muted"></i> @lang('messages.orders')</h4>
							<ul class="nav nav-list">

								<li><a href="{{ route("private.{$glc}.announcements") }}">@lang('messages.my_announces')</a></li>
								<li><a href="{{ route("private.{$glc}.order.index") }}">@lang('messages.my_orders')</a></li>
								<li><a href="{{ route("public.{$glc}.privFavoritos") }}">@lang('messages.saved_announces')</a></li>
								{{--<li><a href="{{ route('public.privConsultas') }}">Ver mis consultas privadas</a></li>--}}
								{{--<li><a href="#">Ver mi historial de órdenes</a></li>--}}
							</ul>
							<h4><i class="fas fa-universal-access fa-fw text-muted"></i> @lang('messages.account_config')</h4>
							<ul class="nav nav-list">
								<li><a href="{{ route('private.changePassword') }}">@lang('messages.udate_mail_password')</a></li>
								{{--<li><a href="#">Actualizar mis direcciones disponibles</a></li>--}}
							</ul>
						</div>
						<!-- END col-6 -->
						<!-- BEGIN col-6 -->
						<div class="col-md-6" hidden>
							{{-- ------------------- TEMPORALMENTE DESHABILITADO ---------------- --}}
							{{--<h4><i class="fab fa-cc-visa fa-fw text-muted"></i> @lang('messages.payments_finance')</h4>--}}
							{{--<ul class="nav nav-list">--}}
								{{--<li><a href="{{ route('private.privCreditCards') }}">@lang('messages.list_enabled_cards')</a></li>--}}
							{{--</ul>--}}
							{{-- ------------------- TEMPORALMENTE DESHABILITADO END ---------------- --}}



							<h4><i class="fas fa-truck fa-fw text-primary"></i> @lang('messages.announcements')</h4>
							<ul class="nav nav-list">
								<li><a href="{{ route('public.publish') }}">@lang('messages.post_an_announcement')</a></li>
							</ul>
							<h4><i class="fab fa-wpforms fa-fw text-muted"></i> @lang('messages.support')</h4>
							<ul class="nav nav-list">
								<li><a href="{{ route('public.faq') }}">@lang('messages.frequent_questions')</a></li>
							</ul>
							{{-- @if($esProveedor) --}}
							{{-- ------------------- TEMPORALMENTE DESHABILITADO ---------------- --}}
							<h4><i class="fas fa-id-badge fa-fw text-muted"></i> @lang('messages.public_profile')</h4>
							<ul class="nav nav-list">
								<li><a href="{{ route('private.providerProfileEdit') }}">@lang('messages.profile_info')</a></li>
							</ul>
							{{-- ------------------- TEMPORALMENTE DESHABILITADO END ---------------- --}}
							{{-- @endif --}}
						</div>
						<!-- END col-6 -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->

	{{-- MODAL PRODUCT CONTACT CONSULTANT --}}

	<div class="modal fade" id="modalContactConsultant">
		<div class="modal-dialog" >
			<div class="modal-content" style="margin-top:50px;">
				<div class="modal-body">
					<div class="row">
						<div class="col col-md-12">
							<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
								<h4 class="text-center m-t-0">@lang('messages.service_requirement')</h4>
							</div>

<br>

							<p>@lang('messages.service_requirement_autoimport')</p>

							<hr>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="border-left:1px dashed #e5e5e5 !important;">

						<span id="sqError_messages" class="hide">
							<p class="alert alert-danger">@lang('messages.marked_fields_required')</p>
						</span>


							<form id="formContactConsultant"  action="" method="post" class="text-center">

								<div class="row">
									<div class="col col-md-6">

										@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
											<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')">
										@else
											<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->apellidos }}" disabled>
										@endif
											<div id="sqError_apellidos"></div>

										<input type="hidden" id="sqTipoConsulta" value="product_client_automation">

										@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
											<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" >
										@else
											<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->nombres }}" disabled >
										@endif
											<div id="sqError_nombres"></div>

										@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
											<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
											<div id="sqError_email"></div>
										@else
											<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->username }}" disabled >
										@endif


										<style>

										</style>
										{{--<select name="pais" id="sqPais" class="login-input">--}}
										{{--<option value="">@lang('messages.select_country')</option>--}}
										{{--</select>--}}

										@if($auth_nombres_apellidos == '')
											{!! Form::select('pais', $global_paises, $global_country_abr, [
												'id' => 'sqPais',
												'class' => 'login-input',
												'placeholder' => __('messages.select_country'),
												'style' => 'width:90% !important;  margin-top:10px;'
											]); !!}
											<div id="adPubError_pais"></div>
										@else
											{!! Form::select('pais', $global_paises, \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->pais, [
												'id' => 'sqPais',
												'class' => 'login-input',
												'placeholder' => __('messages.select_country'),
												'style' => 'width:90% !important; margin-top:10px;',
												'disabled'
											]); !!}
										@endif

										@if($auth_nombres_apellidos == '')
											<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
											<div id="sqError_telefono"></div>
										@else
											<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')"
											       value="{{ (count(\Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos)) ? \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos->first()->numero : '' }}">
										@endif

									</div>
									<div class="col col-md-6">
										<textarea class="login-input form-control" id="sqDescripcion" style="width:95%; height:193px;" placeholder="@lang('messages.comments')"></textarea>
									</div>
								</div>


								{{--<input type="submit" id="adPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
								<a id="btnSearchQuestion" href="javascript:;"  class="promotion-btn m-t-20 m-b-10" onclick="guardarContactoConsulta()" style="width:50%; font-size:16px;">@lang('messages.send')</a>
								{{--<a href="javascript:;" id="adPubBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('scripts')

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>
<script>

	function requestRenewal(idSubscripcion) {
		var url = '{{ route('private.requestRenewal') }}';
		var data = {
			idSubscripcion: idSubscripcion
		};

		ajaxPost(url, data, {
			onSuccess: function(data) {
				console.log(data);
			}
		})
	}

	// ---------- PRODUCTO CONSULTA CONTACTO

	function modalContactConsultant(){

		$('#sqPais').val('{{ $global_localization }}');


		//		$('.select2-container').removeClass('inputError');

		$('#modalContactConsultant').modal();
	}

	function guardarContactoConsulta(){
		var url = '{{ route('public.consultaExterna.store') }}';
		var data = {
			nombres : $('#sqNombres').val(),
			apellidos : $('#sqApellidos').val(),
			email : $('#sqEmail').val(),
			pais : $('#sqPais').val(),
			telefono : $('#sqTelefono').val(),
			descripcion : $('#sqDescripcion').val(),
			tipo : $('#sqTipoConsulta').val(),
			pub_id : 0
		};
		ajaxPost(url, data, {
			onSuccess: function(data){
				$('#modalSearchConsultant').modal('toggle');

				@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())

				$('#sqNombres').val('');
				$('#sqApellidos').val('');
				$('#sqEmail').val('');
				$('#sqPais').val('');
				$('#sqTelefono').val('');

				@endif

				$('#sqDescripcion').val('');

				$('#modalContactConsultant').modal('hide');

				swal("@lang('messages.comments_sent')!", "@lang('messages.su_consulta_enviada_correctamente')", "success");
//				.then(function(response){
//
//				});
			},
			onValidation: function(data){

				$('#sqNombres').removeClass('inputError');
				$('#sqApellidos').removeClass('inputError');
				$('#sqEmail').removeClass('inputError');
				$('#sqPais').removeClass('inputError');
				$('#sqTelefono').removeClass('inputError');
				$('#sqDescripcion').removeClass('inputError');
				$('.select2-container').removeClass('inputError');

				$.each(data, function(key, value){

					key = key.replace(/^\w/, function (chr) {
						return chr.toUpperCase();
					});

					if(key === 'Pais'){
						$('.select2-container').addClass('inputError');
					}

					$('#sq' + key).addClass('inputError');

				});

				$('#sqError_messages').removeClass('hide');
			}
		});
	}

</script>

@endpush