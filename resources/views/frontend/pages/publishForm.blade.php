@extends(($isAjaxRequest == true) ? 'frontend.layouts.ajax' : 'frontend.layouts.default')
@push('css')

<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />

{{--<link href="/assets/plugins/powerange/powerange.min.css" rel="stylesheet" />--}}
{{--<link href="/assets/plugins/candlestick/dist/candlestick.min.css" rel="stylesheet" />--}}

<style>
	.dropzone .dz-preview .dz-image{
		border-radius:7px !important;
	}
	.dropzone .dz-preview .dz-image img{
		max-width:120px;
	}

	.dropzone .dz-preview .dz-error-message {
		top: 150px!important;
	}

	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}

	h5 {
		clear:both;
	}
</style>
@endpush
@section('content')

	@if($isAjaxRequest == false)
		@include('frontend.includes.topMessage')
	@endif


	@foreach($primaryFields as $element)

		@if($element['type'] == 'titulo1')
			@include('frontend.formElements.titulo1', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'titulo2')
			@include('frontend.formElements.titulo2', [
				'e' => $element,
				'group' => $grp
			])
		@endif

		@if($element['type'] == 'text')

			@if($element['key'] == 'pais_de_fabricacion')
				@include('frontend.formElements.select_country', [
					'e' => $element,
				])
			@else
				@include('frontend.formElements.input', [
					'e' => $element,
				])
			@endif
		@endif

		@if($element['type'] == 'number')
			@include('frontend.formElements.number', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'boolean')
			@include('frontend.formElements.boolean', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'multiline')
			@include('frontend.formElements.text', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'select')
			@include('frontend.formElements.select', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'multiselect')
			@include('frontend.formElements.multiselect', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'percent10')
			@include('frontend.formElements.percent10', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'range_year')
			@include('frontend.formElements.rangeYear', [
				'e' => $element,
			])
		@endif
	@endforeach

	<h4><i class="fa fa-angle-right text-muted"></i> @lang('messages.price')</h4>
	{{--<h5><a href="javascript:;" data-toggle="collapse" data-target="#group_precio">@lang('messages.price') </a></h5>--}}

	<div id="group_precio">
		@include('frontend.formElements.number', [
			'e' => [
				'key' => 'precio',
				'label' => __('messages.price_southamerica') .' ( ' . __('messages.exclude_moms') . ' )',
				'compliment' => '.00'
			]
		])

		<div class="form-group col-md-6">
			<label class="control-label">@lang('messages.currency') </label>
			{!! Form::select('currency', $moneda, ($global_country_abr == 'se') ? 'sek' : '', [
				'id' => 'currency',
				'class' => 'form-control',
			]) !!}
		</div>
		<div class="form-group col-md-12">
			<div class="alert alert-info fade show in" style="margin-bottom:0px;">
				<p>
					{!! Form::checkbox('priceSouthamerica', 1, false, [
						'id' => 'priceSouthamerica',
						'class' => '',
					]) !!}
					@lang('messages.price_southamerica_only')
				</p>
			</div>
		</div>

		<div class="form-group col-md-12 hide">
			<div class="alert alert-info fade show in" style="padding-left:0px; padding-right:0px;">
				@include('frontend.formElements.number', [
					'e' => [
						'key' => 'precioMinimo',
						'label' => __('messages.minimum_price_required'),
						'compliment' => '.00'
					]
				])

				<div class="form-group col-md-6">
					<label class="control-label">@lang('messages.currency') &nbsp; </label>
					{!! Form::select('currencyMinimo', $moneda, ($global_country_abr == 'se') ? 'sek' : '', [
						'id' => 'currencyMinimo',
						'class' => 'form-control',
					]) !!}
				</div>

				<p style="padding:0px 15px 0px 15px;">
					@lang('messages.minimum_price_text')
				</p>
			</div>
		</div>

		<div class="form-group col-md-12">
			<div class="alert alert-info fade show in" style="padding-left:0px; padding-right:0px;">
				@include('frontend.formElements.number', [
					'e' => [
						'key' => 'precioEuropa',
						'label' => __('messages.price_europa'),
						'compliment' => '.00'
					]
				])

				<div class="form-group col-md-6">
					<label class="control-label">@lang('messages.currency') &nbsp; </label>
					{!! Form::select('currencyEuropa', $moneda, ($global_country_abr == 'se') ? 'sek' : '', [
						'id' => 'currencyEuropa',
						'class' => 'form-control',
					]) !!}
				</div>

				<p style="padding:0px 15px 0px 15px;">
					@lang('messages.price_europe_message')
				</p>
			</div>
		</div>


	</div>

	<style>
		.small_gallery{

		}
		.small_image{
			display:inline-block;
			padding:10px;
		}
		.small_image img{
			max-width:100px;
			max-height:100px;
		}
	</style>

	<h5><a href="javascript:;" data-toggle="collapse" data-target="#group_imagenes">@lang('messages.gallery') </a></h5>

	<div id="group_imagenes">

		{{--<div class="small_gallery">--}}
			{{--@php--}}
				{{--$n = 0;--}}
			{{--@endphp--}}
			{{--@foreach($images as $image)--}}

				{{--@php--}}
					{{--$n1 = $n++;--}}
				{{--@endphp--}}

				{{--<div id="img_{{ $n1 }}" class="small_image">--}}
					{{--<img src="/tmpimgs/{{ $image['filename'] }}" alt="" />--}}
					{{--<br>--}}
					{{--<a href="javascript:;" onclick="removeImage({{ $n1 }}, '{{ $image['filename'] }}')" data-toggle="tooltip" title="@lang('messages.remove')"><i class="fa fa-trash"></i></a>--}}
				{{--</div>--}}

			{{--@endforeach--}}
		{{--</div>--}}

		<form action="{{ route('public.publishUpload') }}" class="dropzone">
			<input type="hidden" name="_token" value="" id="dropToken">
			<input type="hidden" name="sesna" value="{{ $sesna }}">
			<div class="dz-message" data-dz-message><span>@lang('messages.dz_upload')</span></div>
		</form>
		{{--<div id="dropzone"></div>--}}
	</div>

	@php
		$grp = 1;
		$openGroup = false;
	@endphp
	@foreach($secondaryFields as $element)

		@if($element['type'] == 'titulo1')
			@include('frontend.formElements.titulo1', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'titulo2')
			@if($openGroup == true)
				@php echo '</div>'; @endphp
			@endif

			@include('frontend.formElements.titulo2', [
				'e' => $element,
				'group' => $grp
			])
			<div id="group_{{ $grp++ }}" class="collapse">
			@php
				$openGroup = true;
			@endphp
		@endif

		@if($element['type'] == 'text')
			@if($element['key'] == 'pais_de_fabricacion')
				@include('frontend.formElements.select_country', [
					'e' => $element,
				])
			@else
				@include('frontend.formElements.input', [
					'e' => $element,
				])
			@endif
		@endif

		@if($element['type'] == 'number')
			@include('frontend.formElements.number', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'boolean')
			@include('frontend.formElements.boolean', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'multiline')
			@include('frontend.formElements.text', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'select')
			@include('frontend.formElements.select', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'multiselect')
			@include('frontend.formElements.multiselect', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'percent10')
			@include('frontend.formElements.percent10', [
				'e' => $element,
			])
		@endif

		@if($element['type'] == 'range_year')
			@include('frontend.formElements.rangeYear', [
				'e' => $element,
			])
		@endif
	@endforeach

	</div>
	<div class="collapse in">
		<div class="form-group col-md-12 hide">
			<div class="text-right">
				<a href="javascript:;" class="btn btn-primary" onclick="sendForm(this)"><i class="fa fa-send"></i> @lang('messages.send')</a>
			</div>
		</div>

@endsection

@push('scripts')
{{--<script src="/assets/plugins/dropzone/dist/dropzone.js"></script>--}}


{{--<script src="/assets/plugins/switchery/switchery.min.js"></script>--}}
{{--<script src="/assets/plugins/powerange/powerange.min.js"></script>--}}
{{--<script type="text/javascript" src="/assets/plugins/candlestick/dist/candlestick.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>--}}

<script>

//$.getScript('/assets/plugins/candlestick/dist/candlestick.min.js').done(function() {
//	$.getScript('https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js').done(function() {
//		$.getScript('/assets/plugins/candlestick/dist/jquery.hammer.js').done(function() {
//			setTimeout(function(){
//				formInit();
//			}, 500);
//		});
//	});
//});
var myDropzone
$.getScript('/assets/plugins/dropzone/dist/dropzone.js').done(function() {
	setTimeout(function(){
		{{--$('div#dropzone').dropzone({--}}
			{{--url: '{{ route('public.publishUpload') }}'--}}
		{{--});--}}

		myDropzone = new Dropzone(".dropzone", {
			url: "{{ route('public.publishUpload') }}",
			addRemoveLinks: true,
			autoProcessQueue: true,
			parallelUploads: 1,
			maxFilesize: 3, // MB
			init: function(){
				@foreach($images as $image)
					var mockFile = {
						name: '{{ $image['filename'] }}',
						size: 123,
					};
					this.options.addedfile.call(this, mockFile);
					this.options.thumbnail.call(this, mockFile, '/tmpimgs/{{ $image['filename'] }}');
					this.emit('thumbnail', mockFile, '/tmpimgs/{{ $image['filename'] }}');
					this.emit('complete', mockFile);
				// Src.: https://stackoverflow.com/a/22719947;
				@endforeach

				this.on("removedfile", function(file) {
//					alert(file.name);
					console.log('Eliminado: ' + file.name);

					file.previewElement.remove();
					{{----}}
					{{--// Create the remove button--}}
					{{--var removeButton = Dropzone.createElement("<button>@lang('messages.remove')</button>");--}}

					{{--//Capture the Dropzone instance as closure.--}}
					{{--var _this = this;--}}

					{{--// Listen to the click event--}}
{{--//					removeButton.addEventListener();--}}

					{{--// Add the button to the file preview element.--}}
					{{--file.previewElement.appendChild(removeButton);--}}

				});
			},
			success: function(file, response){
				console.log("success");
				console.log(file.name);
				console.log(response);
			},
//			error: function(file, errormessage, data) {
//				console.log("error -----------------------------");
//				console.log(file);
//				console.log(errormessage);
//				console.log(data);
//				console.log(data.status);
//				if( data.status === 422 ) {
//					var errors = $.parseJSON(data.responseText);
//					$.each(errors, function (key, value) {
//						// console.log(key+ " " +value);
////					$('#response').addClass("alert alert-danger");
//
//						if($.isPlainObject(value)) {
//							$.each(value, function (key, value) {
//								console.log(key+ " " +value);
////							$('#response').show().append(value+"<br/>");
//								$('.dz-error-message span').append(value+"<br/>");
//
//							});
//						}else{
////						$('#response').show().append(value+"<br/>"); //this is my div with messages
//							$('.dz-error-message span').append(value+"<br/>");
//						}
//					});
//				}
//			},
			removedfile: function(file){
				x = confirm('@lang('messages.confirm_removal')');
				if(!x) return false;
				removeImage(1, file.name);
//				for(var i = 0 ; i < file_up_names.length; i++){
//					if(file_up_names[i] === file.name){
//						ajaxPost('public.publishRemove', {
//							'archivo' : ''
//						});
//					}
//				}
			}
		});

		myDropzone.on('error', function(file, errormessage, data) {
				if( data.status === 422 ) {

					var errorMessageElement = $(file.previewElement).find('.dz-error-message');
					errorMessageElement.html('')
					try {
						var errors = $.parseJSON(data.responseText);
					} catch(e) {
						errorMessageElement.append(data.responseText+"<br/>");
						return false;
					}

					$.each(errors, function (key, value) {
						// console.log(key+ " " +value);
						if($.isPlainObject(value)) {
							$.each(value, function (key, value) {
//								console.log(key+ " " +value);
								errorMessageElement.append(value+"<br/>");

							});
						}else{
							errorMessageElement.append(value+"<br/>");
						}
					});
				}
		});

	}, 1000);

	$('#dropToken').val($('meta[name="csrf-token"]').attr('content'));
	$("[data-toggle=tooltip]").tooltip();

//	Dropzone.options.myDropzone = {
//		init: function() {
//			addRemoveLinks: true,
//				thisDropzone = this;
//			$.get('photo-add.php', function(data) {
//				$.each(data, function(key,value){
//					var mockFile = { name: value.name, size: value.size };
//					thisDropzone.options.addedfile.call(thisDropzone, mockFile);
//					thisDropzone.options.thumbnail.call(thisDropzone, mockFile,      "upload/"+value.name);
//
//				});
//			});
//
//			this.on("removedfile", function(file) {
//				alert(file.name);
//				console.log(file);
//				// Create the remove button
//				var removeButton = Dropzone.createElement("<button>Remove file</button>");
//
//				//Capture the Dropzone instance as closure.
//				var _this = this;
//
//				// Listen to the click event
//				removeButton.addEventListener();
//
//				// Add the button to the file preview element.
//				file.previewElement.appendChild(removeButton);
//			});
//		}
//	};

});

var ran = false;
function formInit(){
	if(ran == false){
		ran = true;
		console.log("only ran once");
//		renderSwitcher();
//		checkSwitcherState();

//		initCandleStick();

		initMultiselect();

	}



}

function validateForm(success, failure){
	var url = '{{ route('public.publishValidate') }}';
	var data = {
		idc: {{ $idCategoria }},
//		marca: $('#marca').val(),
//		modelo: $('#modelo').val(),
//		subcat: $('#subcategoria').val(),
		precio: $('#precio').val(),
		currency: $('#currency').val(),
	@foreach($campos as $campo)
		{{ $campo['key'] }}: $('#{{ $campo['key'] }}').val(),
	@endforeach
	};

	$.each(data, function(k, v){
		$('#'+k).css({'border':'1px solid #ccd0d4'});
//		$('#errorDisplay_'+k).attr('data-toggle', 'tooltip');
		{{--$('#errorDisplay_'+k).attr('data-title', '@lang('messages.required')');--}}
		$('#errorDisplay_'+k).addClass('hide');
//		$("[data-toggle=tooltip]").tooltip();
	});

	ajaxPost(url, data, {
		onValidationOverride: true,
		onValidation: function(data){
			console.log("validation handler");
			$.each(data, function(k, v){
				if(k == 'AnnounceImages'){
					swal("@lang('messages.publish_review_announce')!", "@lang('messages.publish_missing_images')", "warning");
				}
				$('#'+k).css({'border':'1px solid #ff0000'});
				$('#errorDisplay_'+k).attr('data-toggle', 'tooltip');
				$('#errorDisplay_'+k).attr('data-title', '@lang('messages.required')');
				$('#errorDisplay_'+k).removeClass('hide');
//				$("[data-toggle=tooltip]").tooltip();
				$('#errorDisplay_'+k).tooltip();
				$('#errorDisplay_'+k).tooltip('show');
			});
			{{--$(e).html('<i class="fas fa-send"></i> @lang('messages.send')');--}}
			{{--$(e).removeAttr('disabled');--}}
		},
		onSuccess: function(data){
			success();
			{{--$(e).html('<i class="fas fa-send"></i> @lang('messages.sent')');--}}
			{{--$(e).attr('disabled', 'disabled');--}}
			{{--$(e).attr('onclick', '');--}}

			{{--swal("Publicaciòn exitosa!", "Un consultor se pondrá en contacto con usted en la brevedad posible.", "success")--}}
				{{--.then((response) => {--}}
				{{--location.reload();--}}
				{{--});--}}
		},
		onFailure: function(data){
			failure();
		},
		onDone: function(data){

		},
	});
}

function sendForm(){



	if($('#legalterms').prop('checked')) {
	} else {
		alert('You must read and accept the Legal terms and conditions before submitting your payment.')
		return false;
	}


	if($('#goodfaith').prop('checked')) {
	} else {
		alert('Machine cannot be published if it is not free of any other responsibility.  Please mark the checkbox giving your approval to this.')
		return false;
	}

	$('#btnPublishSendForm')
		.html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...')
		.attr('disabled', 'disabled')
		.attr('onclick', '');

	if(current_payment_type === 'stripe'){
		if(current_card === '') {
			stripe.createToken(ncard).then(function(result) {
				if(result.error !== undefined){
					$('.card-message')
						.html('<div class="alert alert-danger fade show in">@lang('messages.verify_ccard')</div>')
						.removeClass('hide');
					$('#btnPublishSendForm')
						.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')')
						.removeAttr('disabled')
						.attr('onclick', 'sendForm()');
				} else {
					crearPublicacion(result.token.id);
				}
				// Handle result.error or result.token
			});
		} else {
			crearPublicacion('card');
		}

	} else if(current_payment_type === 'faktura') {
		crearPublicacion('none');
	} else if(current_payment_type === 'subscripcion') {
		crearPublicacion('none');
	}
	return false;
}

function crearPublicacion(token){

	var url = '{{ route('public.publishFormPost') }}';
	var data = {
		idc: {{ $idCategoria }},
		token: token,
		days: $('#days').val(),
		marca: $('#marca').val(),
		modelo: $('#modelo').val(),
		type: $('#type').val(),
		status: $('#status').val(),
		subcat: $('#subcategoria').val(),
		precio: $('#precio').val(),
		currency: $('#currency').val(),
		precioMinimo: $('#precioMinimo').val(),
		currencyMinimo: $('#currencyMinimo').val(),
		precioEuropa: $('#precioEuropa').val(),
		currencyEuropa: $('#currencyEuropa').val(),
		priceSouthamerica: ($('#priceSouthamerica').prop('checked')) ? 1 : 0,
		payment_type: current_payment_type,
		time_extended: ($('#chk_time_extended').prop('checked')) ? 1 : 0,
		current_card: current_card,
	@foreach($campos as $campo)
	{{ $campo['key'] }}: $('#{{ $campo['key'] }}').val(),
	@endforeach
		multiselects: getKeys()
	};

	ajaxPost(url, data, {
		onValidationOverride: true,
		onValidation: function(data){
			$.each(data, function(k, v){

				if(k === 'AnnounceImages'){
					swal("@lang('messages.publish_review_announce')!", "@lang('messages.publish_missing_images')", "warning");
				} else if (k === 'CardError') {
					swal("Error", v[0], "error");
				}

				$('#'+k).css({'border':'1px solid #ff0000'});
				$('#errorDisplay_'+k)
					.attr('data-toggle', 'tooltip')
					.attr('data-title', '@lang('messages.required')')
					.removeClass('hide');
			});
			$("[data-toggle=tooltip]").tooltip();

			$('#btnPublishSendForm')
				.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')')
				.removeAttr('disabled')
				.attr('onclick', 'sendForm()');
		},
		onSuccess: function(data){

			$('#btnPublishSendForm')
				.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.sent')')
				.attr('disabled', 'disabled')
				.attr('onclick', '');

			swal("@lang('messages.publish_successful')!", "@lang('messages.publish_success_msg')", "success")
				.then(function(response){
					redirect('{{ route("private.{$glc}.announcements") }}');
{{--					redirect('{{ route('public.checkout') }}');--}}
				});
		},
		onFailure: function(data){
			$('#btnPublishSendForm')
				.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')')
				.removeAttr('disabled')
				.attr('onclick', 'sendForm()');
		},
		onError: function(data){
			swal("@lang('messages.internal_error_title')", "@lang('messages.internal_error_message')", "error")
				.then(function(response){
					// TODO: Announce Here: Preguntar si deberia irse a buscar o quedarse en la página para volver a intentar.
					//redirect('{{ route("public.{$glc}.search") }}');
				});
			$('#btnPublishSendForm')
				.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')')
				.removeAttr('disabled')
				.attr('onclick', 'sendForm()');
		},
		onDone: function(data){
			console.log("came to done");
		},
	});
}

function removeImage(index, name){
	console.log("Removing image: "+name);
	var url = '{{ route('public.publishRemove') }}';
	var data = {
		image: name
	};

	ajaxPost(url, data, {
		onSuccess: function(data){
			$('#img_'+index).remove();
		}
	});
}


// ------------------------------------

var renderSwitcher = function() {
	if ($('[data-render=switchery]').length !== 0) {
		$('[data-render=switchery]').each(function() {
			var themeColor = COLOR_GREEN;
			if ($(this).attr('data-theme')) {
				switch ($(this).attr('data-theme')) {
					case 'red':
						themeColor = COLOR_RED;
						break;
					case 'blue':
						themeColor = COLOR_BLUE;
						break;
					case 'purple':
						themeColor = COLOR_PURPLE;
						break;
					case 'orange':
						themeColor = COLOR_ORANGE;
						break;
					case 'black':
						themeColor = COLOR_BLACK;
						break;
				}
			}
			var option = {};
			option.color = themeColor;
			option.secondaryColor = ($(this).attr('data-secondary-color')) ? $(this).attr('data-secondary-color') : '#dfdfdf';
			option.className = ($(this).attr('data-classname')) ? $(this).attr('data-classname') : 'switchery';
			option.disabled = ($(this).attr('data-disabled')) ? true : false;
			option.disabledOpacity = ($(this).attr('data-disabled-opacity')) ? parseFloat($(this).attr('data-disabled-opacity')) : 0.5;
			option.speed = ($(this).attr('data-speed')) ? $(this).attr('data-speed') : '0.5s';
			var switchery = new Switchery(this, option);
		});
	}
};

var checkSwitcherState = function() {
	$(document).on('click', '[data-click="check-switchery-state"]', function() {
		alert($('[data-id="switchery-state"]').prop('checked'));
	});
	$(document).on('change', '[data-change="check-switchery-state-text"]', function() {
		$('[data-id="switchery-state-text"]').text($(this).prop('checked'));
	});
};

// --------------------------------------

$(document).ready(function(){
//	formInit();
});

{{--@if($isAjaxRequest == true)--}}
	{{--formInit();--}}
{{--@endif--}}

function initCandleStick(){
	$(".formHolder input[type='checkbox']").candlestick({


		// options or contents
		'mode': 'options',

		'contents': {
			'left': 'Left',
			'middle': 'Middle',
			'right': 'Right',
			'swipe': false
		},

		// for on value
		'on': '1',

		// for off value
		'off': '0',

		// for non value
		'default': '',

		// enable touch swipe
		// requires hammer.js and jquery hammer.js plugin
		'swipe': true,

		// lg, md (default), sm, xs
		'size': 'lg',

		// for none/default value
		'nc': 'none',

		// Enable the three options, set to false will disable the default option
		'allowManualDefault': true,

		// callbacks
		afterAction: function() {},
		afterRendering: function() {},
		afterOrganization: function() {},
		afterSetting: function() {}

	});

	$(".formHolder input[type='checkbox']").candlestick('reset');
}

// ---------- MULTISELECT JS SCRIPTS
var hashOptions = {};
function initMultiselect(){

	$('.mselect').on('change', function(e){

	});
}

function registerOption(e){


	var id = $(e).attr('id');

	if(hashOptions[id] == undefined){
		hashOptions[id] = [];
	}

	hashOptions[id].push({
		key : $('#'+id).val(),
		text : $('#'+id + ' option:selected').text()
	});

	updateOptions(id);

}

function removeOption(id, op){
	console.log("buscando: ");
	var opIndex = -1;
	$.each(hashOptions[id], function(k, v){
		if(hashOptions[id][k].key == op) {
			opIndex = k;
		}
	});

	if(opIndex >= 0){
		hashOptions[id].splice(opIndex, 1);
	}

	updateOptions(id);
}

function updateOptions(id){

	$('#'+id).val('');
	$('#'+id + ' option').removeClass('hide');

	// ----------

	$('#mEle_'+id).html('');
	var ul = $('<ul>');

	$.each(hashOptions[id], function(k, v){

		$('#'+id + " option[value='"+v.key+"']").addClass('hide');

		// ----------

		var a = $('<a>', {
			'class': 'pull-right bclose',
			href: 'javascript:;',
			onclick: 'removeOption("'+id+'", "'+v.key+'")'
		}).html('<i class="fa fa-trash-alt"></i>');

		var li = $('<li>');
		li
			.append(v.text)
			.append(a);
		ul.append(li);
	});

	$('#mEle_'+id).append(ul);
}

function getKeys(){
	var optSelected = {};

	$.each(hashOptions, function(k,v){
		if(optSelected[k] === undefined){
			optSelected[k] = [];
		}

		$.each(hashOptions[k], function(i, l){
			optSelected[k].push(l.key);
		})
	});

	return optSelected;
}

</script>
@endpush