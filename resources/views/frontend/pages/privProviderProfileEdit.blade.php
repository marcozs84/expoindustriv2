@extends(($isAjaxRequest == true) ? 'frontend.layouts.ajax' : 'frontend.layouts.default')
@push('css')
<link href="/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css" />--}}

<link href="/assets/plugins/dropzone/dist/min/basic.min.css" rel="stylesheet" />
<link href="/assets/plugins/dropzone/dist/min/dropzone.min.css" rel="stylesheet" />

<link href="/assets/plugins/powerange/powerange.min.css" rel="stylesheet" />
<link href="/assets/plugins/candlestick/dist/candlestick.min.css" rel="stylesheet" />


<link href="/assets/plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
<link href="/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet" />

<style>
	.dropzone .dz-preview .dz-image{
		border-radius:7px !important;
	}
	.dropzone .dz-preview .dz-image img{
		max-width:120px;
	}

	.mSelectOptions{
		/******border:1px solid #e5e5e5;*/
	}
	.mSelectOptions ul {
		padding:0px;
	}
	.mSelectOptions ul li{
		list-style:none;
		background-color: #fff;
		border: 1px solid #e5e5e5;
		padding: 3px;
		margin: 0px;
		margin-top: 2px;
	}
	.mSelectOptions ul li a.bclose{
		font-weight: bold;
		padding: 0px;
		padding-right: 5px;
	}

	h5 {
		clear:both;
	}

	.address {
		margin-bottom:10px;
	}
</style>
@endpush
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.profile_info')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">


				@include('frontend.pages.private.component.sidebarmenu', ['selection' => 'my_profile'])


				<!-- BEGIN account-body -->
				<div class="account-body">
					<!-- BEGIN row -->
					<div class="row">

						<h4>@lang('messages.profile_info')</h4>
						<p>
							@lang('messages.public_profile_msg')

							<br><br>

							@lang('messages.preview_link', ['link' => route("public.{$glc}.perfilCliente", ['id' => $prov->id])])
						</p>
						<br>
						<form id="formActualizarPerfilEmpresa" action="{{ route('private.providerProfileEditSave') }}" method="POST" enctype="multipart/form-data">
						<!-- BEGIN col-6 -->
						<div class="col-lg-12 /*col-md-offset-2*/">

							<h4><i class="fas fa-universal-access fa-fw text-muted"></i> @lang('messages.company_information')</h4>
							<br>

							<div class="row">
								<div class="col col-md-12">

									@if ($errors->any())
										<div class="alert alert-danger">
											<ul>
												@if($errors->has('companyName'))
													<li>@lang('messages.company_name_required')</li>
												@endif
												@if($errors->has('companyNit'))
													<li>@lang('messages.company_nit_required')</li>
												@endif
												@if($errors->has('companyLogo'))
													@foreach($errors->get('companyLogo') as $message)
														<li>{{ $message }}</li>
													@endforeach
												@endif
											</ul>
										</div>
									@endif


										{{ csrf_field() }}
										<fieldset>
											{{--<legend class="m-b-15">Actualizar correo y contraseña</legend>--}}
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.company_name')</label>
												<div class="col-md-8">
													<input name="companyName" id="companyName" type="text" value="{{ $companyName }}" class="form-control" placeholder="">
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.company_nit')</label>
												<div class="col-md-8">
													<input name="companyNit" id="companyNit" type="text" value="{{ $companyNit }}" class="form-control" placeholder="">
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.company_phone')</label>
												<div class="col-md-8">
													<input name="companyPhone" id="companyPhone" type="text" value="{{ $companyPhone }}" class="form-control" placeholder="">
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">Logo
												<br>
												<small class="text-muted">@lang('messages.suggested_dimention')</small>
												</label>
												<div class="col-md-8">
													@if(!$imagen)
														@lang('messages.no_image_available')<br><br>
													@else
														<div id="logo-container" style="background-color:{{ $imagen->backgroundColor }}; /*background-color:#ffffff;*/ border:1px solid #c5ced4; text-align:center; padding:10px; margin:0px 0px 10px 0px;">
															<img src="{{ $logoUrl }}" alt="Logo proveedor" style="max-width:100%;">
														</div>
													@endif
													<input name="companyLogo" id="companyLogo" type="file" value="" class="form-control" placeholder="">
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.logo_background_color')</label>
												<div class="col-md-8">
													@if(!$imagen)
														<input name="companyBackground" type="text" value="#ffffff" class="form-control" id="colorpicker" autocomplete="off" />
													@else
														<input name="companyBackground" type="text" value="{{ $imagen->backgroundColor }}" class="form-control" id="colorpicker" autocomplete="off" />
													@endif
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.urlSite')</label>
												<div class="col-md-8">
													<input name="urlSitio" type="text" value="{{ $urlSitio }}" class="form-control" />
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.urlFacebook')</label>
												<div class="col-md-8">
													<input name="urlFacebook" type="text" value="{{ $urlFacebook }}" class="form-control" />
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.urlTwitter')</label>
												<div class="col-md-8">
													<input name="urlTwitter" type="text" value="{{ $urlTwitter }}" class="form-control" />
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.urlGooglePlus')</label>
												<div class="col-md-8">
													<input name="urlGooglePlus" type="text" value="{{ $urlGooglePlus }}" class="form-control" />
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label" for="companyDesc">@lang('messages.descriptionCompany')</label>
												<div class="col-md-8">
													<textarea name="companyDesc" id="companyDesc" rows="6" class="form-control">{{ $companyDesc }}</textarea>
												</div>
											</div>

										</fieldset>

								</div>
							</div>
						{{--</div>--}}
						{{--<!-- END col-6 -->--}}

						{{--<div class="col col-lg-12">--}}

							<br><br>

							{{--                  Agregue más información                  --}}


							<h4><i class="fa fa-map fa-fw text-muted"></i> @lang('messages.company_address')</h4>



							<br>

							<div class="form-group row m-b-15">
								<label class="col-md-4 col-form-label">@lang('messages.select_main_office_location')</label>
								<div class="col-md-8">
									{!! Form::textarea('direccion_primaria', $ubicacionPrimaria->direccion, [
									'id' => 'direccion_primaria',
									'class' => 'form-control address',
									'rows' => 4, 'placeholder' => __('messages.address'),
									'tabindex' => 0
								]) !!}
								</div>

								{{--<a href="javscript:;" onclick="buscarEnMapa()" class="btn btn-primary"><i class="fa fa-search"></i> @lang('messages.search')</a>--}}

								{{--<br>--}}

								<a href="javascript:;" id="errorDisplay_ubicacion" class="hide" style="color:#ff0000;"><i class="fa fa-info-circle"></i></a>
							</div>


							<div class="form-group">

								{{--<style>--}}
									{{--#publish_map {--}}
										{{--height: 300px;--}}
										{{--width: 100%;--}}
									{{--}--}}
								{{--</style>--}}

								{{--{!! Form::hidden('latitud', $ubicacionPrimaria->latitud, [--}}
									{{--'id' => 'latitud',--}}
							    {{--]) !!}--}}
								{{--{!! Form::hidden('longitud', $ubicacionPrimaria->longitud, [--}}
									{{--'id' => 'longitud',--}}
							    {{--]) !!}--}}

								{{--<div id="publish_map"></div>--}}
							</div>

							<a href="javascript:;" onclick="addAddress()" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> @lang('messages.add_secondary_addresses')</a>
							<br><br>

							<div id="addresses_div">
							@if($prov)
								<table class="table table-condensed">
								@foreach($ubicacionesSecundarias as $ubicacion)
									<tr>
										<td><b>{{ $ubicacion->descripcion }}</b><br>{!! nl2br($ubicacion->direccion) !!}</td>
										<td><a href="javascript:;" class="btn btn-sm" onclick="removeAddress({{ $ubicacion->id }})"><i class="fa fa-trash"></i></a></td>
									</tr>
								@endforeach
								</table>
							@endif
							</div>




							<div class="form-group row">
								<div class="col-md-3 col-md-offset-5">
									<input type="submit" value="@lang('messages.save')" class="btn btn-warning form-control">
									{{--<a href="javascript:;" onclick="actualizarPerfilEmpresa()" class="btn btn-sm btn-primary m-r-5">Guardar</a>--}}
								</div>
							</div>
						</div>
						</form>
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->

@endsection

@push('scripts')

<script src="/assets/plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

<script src="/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script src="/assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
<script>

	$(document).ready(function(){
		formInit();
	});

	function formInit(){
		$('#colorpicker').colorpicker({format: 'hex'})
			.on('changeColor', function(event) {
				$('#logo-container').css('background-color', event.color.toString());
			});
	}



//	formInit();

	// ----------

//	$( "#formActualizarPerfilEmpresa" ).submit(function( event ) {
//		actualizarPerfilEmpresa();
//		event.preventDefault();
//	});

	function actualizarPerfilEmpresa(){
		var url = '{{ route('resource.usuario.update', [\Illuminate\Support\Facades\Auth::user()->id]) }}';
		var data = {
			username : $('#email').val(),
		};
		ajaxPatch(url, data, function(data){
			console.log('Cuenta de correo actualizada');
		}, function(data){
			console.log('No se pudo actualizar su cuenta de correo', 'warning');
		});
	}

	// ----------
	var map = null;
	var geocoder = null;
	var marker = null;
	function initPublishMap() {
		@if($ubicacionPrimaria->latitud != 0)
			var uluru = {lat: {{ $ubicacionPrimaria->latitud }}, lng: {{ $ubicacionPrimaria->longitud }}};
		@else
			var uluru = {lat: -25.363, lng: 131.044};
		@endif

		map = new google.maps.Map(document.getElementById('publish_map'), {
			zoom: 4,
			center: uluru,
//			query: 'Bolivia'
		});

		@if($ubicacionPrimaria->latitud != 0)
		marker = new google.maps.Marker({
				position: uluru,
				map: map,
			});
		map.panTo(uluru);
		@else
			geocoder = new google.maps.Geocoder();
	//		var address = $("#pais option:selected").text();
			var address = '{{ $global_country_name }}';
			geocodeAddress(geocoder, map, address);

		@endif

		google.maps.event.addListener(map, 'click', function(event) {
			placeMarker(event.latLng, map);
		});

	}

	function placeMarker(location, map) {
		if (marker === null){
			marker = new google.maps.Marker({
				position: location,
				map: map
			});
		} else {
			marker.setPosition(location);
		}

		$('#latitud').val(location.lat);
		$('#longitud').val(location.lng);

//		marker = new google.maps.Marker({
//			position: location,
//			map: map
//		});
		map.panTo(location);
	}
	function geocodeAddress(geocoder, resultsMap, address) {
//		var address = 'Bolivia';
//		var address = $("#pais option:selected").text();
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);
//				var marker = new google.maps.Marker({
//					map: resultsMap,
//					position: results[0].geometry.location
//				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	function buscarEnMapa(){
		var address = $("#direccion_primaria").val();
		map.setZoom(15);
		geocodeAddress(geocoder, map, address);
	}

	{{--@push('mapsInit')--}}
//			initPublishMap();
	{{--@endpush--}}

	function addAddress() {

		var add = '{!! Form::text('dir_nombre[]', '', [
						'class' => 'form-control address',
						'rows' => 4, 'placeholder' => __('messages.filial_name'),
						'tabindex' => 0
				    ]) !!}{!! Form::textarea('direcciones[]', '', [
						'class' => 'form-control address',
						'rows' => 4, 'placeholder' => __('messages.address'),
						'tabindex' => 0
				    ]) !!}';

		$('#addresses_div').prepend(add);
	}

	function removeAddress(id) {
		var url = '{{ route('resource.ubicacion.destroy', [0]) }}';
		ajaxDelete(url, { id: id }, function(data) {
			swal('Ubicación eliminada correctamente.').then(function() {
				refreshPage();
			});
		}, null, {});
	}

</script>
@endpush
