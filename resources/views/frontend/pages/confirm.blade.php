@extends('frontend.layouts.default')
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-13.jpg" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.confirm_cuenta')<b></b></h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.search") }}">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.confirm_cuenta')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN panel-group -->
			<div class="panel-group faq" id="faq-list">

				@if($invalidToken == false)

				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-check fa-fw m-r-5"></i> {{ $titulo }}
						</h4>
					</div>
					<div id="faq-1" class="panel ">
						<div class="panel-body">
							<p class="alert alert-success">
								{!! $mensaje !!}
							</p>

							<div class="text-center">
								<a href="{{ route("public.{$glc}.search") }}" class="btn btn-inverse btn-lg" >@lang('messages.continue') <i class="fa fa-angle-right"></i></a>
							</div>

						</div>
					</div>
				</div>
				<!-- END panel -->

				@else

				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-close fa-fw m-r-5"></i> {{ $titulo }}
						</h4>
					</div>
					<div id="faq-1" class="panel ">
						<div class="panel-body">
							<p class="alert alert-danger">
								{!! $mensaje !!}
							</p>

							{{--<div class="text-center">--}}
								{{--<a href="/" class="btn btn-inverse btn-lg" >Continuar <i class="fa fa-angle-right"></i></a>--}}
							{{--</div>--}}

						</div>
					</div>
				</div>
				<!-- END panel -->

				@endif
			</div>
			<!-- END panel-group -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection