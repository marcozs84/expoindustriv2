@extends('frontend.layouts.default')

@push('css')
<style>
	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}

</style>
@endpush
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black has-bg ">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-tractor1.jpg" style="margin-top:-250px;" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>@lang('messages.welcome')!</b></h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #about-us-content -->
	<div id="about-us-content" class="section-container bg-white p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN about-us-content -->
			<div class="about-us-content">
				<!-- BEGIN row -->
				<div class="row">
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4 col-md-offset-2">
						<div class="service">
							<div class="info">
								<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
									<h4 class="text-center m-t-0">@lang('messages.log_in')</h4>
								</div>
								<form action="{{ route('public.login') }}" method="post" class="text-center loginRegion">
									{{ csrf_field() }}
									<input name="email" id="loginUsername" value="{{ old('email') }}" type="text" class="login-input" placeholder="@lang('messages.e_mail')" >
									@if ($errors->has('email'))
										<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
									@endif
									<input name="password" id="loginPassword" value="{{ old('password') }}" type="password" class="login-input" placeholder="@lang('messages.password')" >
									@if ($errors->has('password'))
										<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        </span>
									@endif
									<br><br>

									<input type="checkbox" name="remember" id="frmRemember"> @lang('messages.remember_me')
									<br>
									<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Entrar">
									<br>

									<a href="javascript:;" onclick="launchLoginRegister('forgot')" style="font-size:12px; /*color:#333333;*/">@lang('messages.forgot_password')</a>
								</form>
								<hr />
								<p class="text-center">
									<a href="javascript:;" class="" onclick="activateLoginRegister('register')" style="font-size:12px; /*color:#333333;*/">@lang('messages.create_account')</a>
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4" style="border-left:1px solid #D8E0E4">
						<div class="service">
							<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
								<h4 class="text-center m-t-0">@lang('messages.register')</h4>
							</div>
							<form action="{{ route('public.register') }}" method="post" class="text-center loginRegion">
								{{ csrf_field() }}
								{!! Form::text('email', null, [
									'id' => 'regUsername',
									'class' => 'login-input',
									'placeholder' => __('messages.e_mail')
								]) !!}
								@if ($errors->has('email'))
									<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
								@endif

								{!! Form::text('apellidos', null, [
									'id' => 'regApellidos',
									'class' => 'login-input',
									'placeholder' => __('messages.lastname')
								]) !!}
								@if ($errors->has('apellidos'))
									<span class="help-block">
                                        <strong>{{ $errors->first('apellidos') }}</strong>
                                        </span>
								@endif

								{!! Form::text('nombres', null, [
									'id' => 'regNombres',
									'class' => 'login-input',
									'placeholder' => __('messages.firstname')
								]) !!}
								@if ($errors->has('nombres'))
									<span class="help-block">
                                        <strong>{{ $errors->first('nombres') }}</strong>
                                        </span>
								@endif

								{!! Form::text('empresa', null, [
									'id' => 'regEmpresa',
									'class' => 'login-input',
									'placeholder' => __('messages.company')
								]) !!}
								@if ($errors->has('empresa'))
									<span class="help-block">
                                        <strong>{{ $errors->first('empresa') }}</strong>
                                        </span>
								@endif

								{!! Form::text('nit', null, [
									'id' => 'regNit',
									'class' => 'login-input',
									'placeholder' => __('messages.company_nit')
								]) !!}
								@if ($errors->has('nit'))
									<span class="help-block">
                                        <strong>{{ $errors->first('nit') }}</strong>
                                        </span>
								@endif

								{{--<select name="pais" id="regPais" class="login-input" >--}}
									{{--<option value="">@lang('messages.select_country')</option>--}}
								{{--</select>--}}
								{!! Form::select('pais', $global_paises, $global_country_abr, [
										'id' => 'regPais',
										'class' => 'login-input',
										'placeholder' => __('messages.select_country')
								]); !!}
								@if ($errors->has('pais'))
									<span class="help-block">
                                        <strong>{{ $errors->first('pais') }}</strong>
                                        </span>
								@endif

								<input type="text" id="regTelefono" name="telefono" value="{{ old('telefono') }}" class="login-input" placeholder="@lang('messages.telephone')" >
								@if ($errors->has('telefono'))
									<span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                        </span>
								@endif

								<input type="password" id="regPassword" name="password" class="login-input" placeholder="@lang('messages.password')" >
								@if ($errors->has('password'))
									<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                        </span>
								@endif

								<input type="password" id="regPassword2" name="password_confirmation" class="login-input" placeholder="@lang('messages.password_confirmation')" >

								<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Enviar">
							</form>
						</div>
					</div>
					<!-- end col-4 -->

				</div>
				<!-- END row -->
			</div>
			<!-- END about-us-content -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-content -->

@endsection

@push('scripts')
	<script>

		{{--$(document).ready(function(){--}}

			{{--var paises = {!! json_encode($global_paises) !!};--}}
			{{--console.log(paises.length);--}}
			{{--$.each(paises, function(k,v){--}}
				{{--$('#regPais').append($('<option>', { value:v }).html(k));--}}
			{{--});--}}

		{{--});--}}

//		$('#formLoginTop').submit(function(e){
//			tryLogin();
//			e.preventDefault();
//		});

	</script>
@endpush