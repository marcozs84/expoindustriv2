@extends('frontend.layouts.default')
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black has-bg ">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/about-us-cover.jpg" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">Inicio</a></li>
				<li class="active">Home</li>
			</ul>
			<!-- END breadcrumb -->
			<h1 class="page-header"><b>Bienvenido!</b></h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #about-us-content -->
	<div id="about-us-content" class="section-container bg-white p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN about-us-content -->
			<div class="about-us-content">
				<!-- BEGIN row -->
				<div class="row">
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4 col-md-offset-2">
						<div class="service">
							<div class="info">
								<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
									<h4 class="text-center m-t-0">Ingresar</h4>
								</div>

								<hr />
								<p class="text-center">
									<a href="javascript:;" class="" onclick="activateLoginRegister('register')" style="font-size:12px; /**color:#333333;/">Crear una cuenta</a>
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4" style="border-left:1px solid #D8E0E4">
						<div class="service">
							<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
								<h4 class="text-center m-t-0">Registrarse</h4>
							</div>

						</div>
					</div>
					<!-- end col-4 -->

				</div>
				<!-- END row -->
			</div>
			<!-- END about-us-content -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-content -->

	<div class="modal fade" id="modal-historia">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">Nuestra Historia</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<p>
						En el año 2006 en Iquique, Chile, se inician las actividades de importación tradicional desde Suecia, de maquinarias, equipos y herramientas usadas, de distintos rubros hasta la zona franca, para posteriormente ser vendidas a clientes de diversos países tales como Bolivia, Chile y Perú.
					</p>
					<p>
						Comienza sus actividades con un flujo de 5 contenedores por año y evoluciona, hasta consolidarse en el año 2013 como la empresa Bengtmachines E.I.R.L, misma que acelera su crecimiento a más de 10 contenedores por año, ampliando las opciones para sus clientes, incluyendo la provisión de equipos de acuerdo a pedidos puntuales, con el único fin de satisfacer las necesidades de sus clientes de la mejor manera posible.
					</p>
					<p>
						A medida que la empresa crece, se observa la necesidad de eliminar distancias, facilitar procesos y acercar más al comprador sudamericano con el vendedor europeo.
					</p>
					<p>
						Es así que nace y se concreta la idea de <b>ExpoIndustri</b>, la plataforma online que permite al cliente sudamericano buscar DIRECTAMENTE en Europa el equipo, maquinaria, herramienta que necesite, en cualquier rubro, ya sea nuevo o usado, simplificando a los clientes el tedioso proceso de exportación-importación entre estos dos continentes, permitiendo al usuario final encontrar el equipo que más le convenga al precio más razonable posible.
					</p>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-proceso">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">Flujo del proceso</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
						<ol>
							<li>Vendedor europeo anuncia su producto en <b>ExpoIndustri</b>.</li>
							<li>Comprador sudamericano busca en <b>ExpoIndustri</b> el equipo/maquinaria/herramienta requerida.</li>
							<li>El comprador cotiza los productos a través de la elección de una serie de alternativas.</li>
							<li>El comprador decide realizar la compra de un equipo y envía su solicitud de compra a <b>ExpoIndustri</b>.</li>
							<li><b>ExpoIndustri</b> realiza todas las verificaciones requeridas, en base a las alternativas elegidas por el comprador.</li>
							<li><b>ExpoIndustri</b> confirma la existencia real y disponibilidad de la maquinaria/equipo y envía confirmación al comprador para que realice el pago correspondiente.</li>
							<li><b>ExpoIndustri</b> realiza el pago al vendedor y recoge la maquinaria.</li>
							<li>La maquinaria es cargada en el tipo de contenedor especificado por el cliente y despachado a Sudamérica.</li>
							<li><b>ExpoIndustri</b> realiza la gestión administrativa y el proceso de consolidación del contenedor.</li>
							<li><b>ExpoIndustri</b> realiza la gestión administrativa y el proceso de envío de la maquinaria/equipo al destino final, especificado por el comprador.</li>
							<li>El comprador recoge su maquinaria en el destino final elegido.</li>
						</ol>

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-seguridad">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">Seguridad</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<p>
						Para ExpoIndustri la tranquilidad de sus clientes es lo MAS importante, es por eso que enfoca todos sus esfuerzos en:
					</p>
					<ul class="fa-ul">
						<li><span class="fa-li"><i class="fa fa-check"></i></span>Verificar la existencia real del equipo/maquinaria que el comprador desea adquirir.</li>
						<li><span class="fa-li"><i class="fa fa-check"></i></span>Realizar verificaciones y emitir informes técnicos especiales al equipo/maquinaria, si así el comprador lo solicitase. (<a
									href="javascript:;">vea aquí el formulario</a>).</li>
						<li><span class="fa-li"><i class="fa fa-check"></i></span>Realizar los procesos de transporte terrestre y marítimo cuidadosamente,con personal capacitado para mantener el estado original en el que el comprador adquirió la maquinaria a través de ExpoIndustri.</li>
						<li><span class="fa-li"><i class="fa fa-check"></i></span>Informar sistemáticamente al comprador sobre el estado de su proceso de compra.</li>
						<li><span class="fa-li"><i class="fa fa-check"></i></span>Cumplir con la entrega de la mercadería en la fecha mencionada.</li>
						<li><span class="fa-li"><i class="fa fa-check"></i></span>Contar con oficinas y personal capacitado en los países mencionados, (<a
									href="javascript:;">ver nuestras oficinas</a>)</li>
						<li><span class="fa-li"><i class="fa fa-check"></i></span>Asegurar su mercadería a través de seguros adicionales ,si asi el comprador lo solicita.</li>
					</ul>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-sostenibilidad">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">Sostenibilidad</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<p>En ExpoIndustri nuestra energía la direccionamos para que todos, tanto en Europa como en Sudamerica, obtengamos el beneficio que buscamos, en calidad, precios y eficiencia en los servicios.</p>
					<p>Nuestra <strong>MISIÓN</strong> es la de facilitar y simplificar la vida de nuestros clientes al momento de vender/comprar entre estos dos continentes, el equipo/maquinaria requerido, con procesos eficientes y eficaces.</p>
					<p>Nuestra <strong>VISIÓN</strong> es la de llegar a todas las partes del mundo, permitiendo y facilitando los intercambios comerciales industriales, tanto para empresa, como para personas particulares, manteniendo el mismo grado de simplicidad.</p>
					<p>Nuestra <strong>ESTRATEGIA</strong> radica en ofrecer ambientes laborales agradables para ofrecer servicios de alta calidad, trabajando para generar las soluciones a medida para cada uno de nuestros clientes y así permitir un uso eficiente y productivo de los recursos mientras nos esforzamos por conseguir nuestra visión.</p>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cerrar</a>
				</div>
			</div>
		</div>
	</div>

@endsection