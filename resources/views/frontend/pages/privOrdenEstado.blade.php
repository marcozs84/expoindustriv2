@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">Inicio</a></li>
				<li><a href="/cuenta">Mi cuenta</a></li>
				<li class="active">Mis ordenes</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar">
					<div class="account-sidebar-cover">
						<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />
					</div>
					<div class="account-sidebar-content">
						<h4>Detalle de Orden</h4>
						<p>
							Se despliega un resumen del estado de su orden/cotización.
						</p>
						<p>
							Por favor no dude en contactarnos si requiere asistencia.
						</p>
					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">
							<!-- BEGIN checkout-body -->
							<div class="checkout-body">
								<!-- BEGIN checkout-message -->
								<div class="checkout-message">
									<h1>Cotización en progreso <small>Estamos trabajando para brindarle la información que usted necesita.</small></h1>
									<div class="table-responsive2">
										<table class="table table-payment-summary">
											<tbody>
											<tr>
												<td class="field">Estado de la orden</td>
												<td class="value">En cotización</td>
											</tr>
											<tr>
												<td class="field">No. de transacción</td>
												<td class="value">REF000001</td>
											</tr>
											<tr>
												<td class="field">Fecha limite para aprobación</td>
												<td class="value">05 APR 2016 07:30PM</td>
											</tr>
											<tr>
												<td class="field">Producto</td>
												<td class="value product-summary">
													<div class="product-summary-img">
														<img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" />
													</div>
													<div class="product-summary-info">
														<div class="title"><a href="/producto" style="/*color: #333;*/">Used Volvo F10 two-way side ...</a></div>
														<div class="desc">Publicado el 26/04/2016</div>
													</div>
												</td>
											</tr>
											<tr>
												<td class="field">Cotización inicial</td>
												<td class="value">$999.00</td>
											</tr>
											</tbody>
										</table>
									</div>
									<p class="text-muted text-center m-b-0">Si usted requiere asistencia adicional, puede ponerse en contacto con un miembro de nuestro equipo a través del siguiente número: (123) 456-7890</p>
								</div>
								<!-- END checkout-message -->
							</div>
							<!-- END checkout-body -->
						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection