@extends('frontend.layouts.default')

@push('css')
<style>
	.halfCover{
		opacity: 0.9;
		filter: alpha(opacity=90); /* For IE8 and earlier */
		background-color:#000000;

	}
	.title {
		font-size:21px !important;
	}

	.carousel-caption p{
		font-weight:500;
		font-size:30px;
	}
	.news_link {
		width:100%;
		max-height:100px;
		/* border-bottom:1px solid #e5e5e5; */
		line-height:18px;
	}
	.news_link td {
		padding:5px;
	}
	.news_date {
		color:#e5e5e5;
	}

	/* *************************************** */

	.discount{ display:none !important; }
	.item-desc{ display:none !important; }
	.item-discount-price{ display:none !important; }
	.item.item-thumbnail {
		border:none;

		/*-webkit-box-shadow: 0px -3px #348fe2;*/
		/*-moz-box-shadow: 0px -3px #348fe2;*/
		/*box-shadow: 0px -3px #348fe2;*/
		margin-bottom: 15px;
		background-color: #ffffff;
	}
	.item.item-thumbnail .item-image{
		padding:2px;
		/*background-color:#f0f3f4;*/
		background-color:#242a31;
		/*background-color:#000000;*/
		/*background-color: #ffffff;*/
		-webkit-border-radius: 3px 3px 0px 0px;
		-moz-border-radius: 3px 3px 0px 0px;
		border-radius: 3px 3px 0px 0px;
	}
	.item.item-thumbnail .item-info {
		border: 1px solid #d8d8d8;
	}
	.item.item-thumbnail .item-title, .item.item-thumbnail .item-title a {
		color:#242a31;
	}

	.promotion {
		margin-bottom:10px;
	}
	.promotion-caption {
		padding:0px;
	}
	.promotion-caption:hover h4 {
		margin-top:115px;
		padding-top:15px;
		padding-right:15px;
		font-size:20px;
	}
	.promotion-caption h4 {
		/*text-shadow: 1px 1px 0 black;*/
		/*background-color:rgba(0, 0, 0, 0.7);*/
		background: rgb(255, 255, 255);
		background: -moz-linear-gradient(-90deg, rgb(0,0,0,.1) 10%, rgba(255, 255, 255, 1) 90%) !important;
		background: -webkit-linear-gradient(-90deg, rgba(0,0,0,.1) 10%,rgba(255, 255, 255, 1) 90%) !important;
		background: linear-gradient(to right, rgba(0,0,0,.1) 10%,rgba(255, 255, 255, 1) 90%) !important;

		margin-top:130px;
		padding-top: 8px;
		padding-right:11px;
		height:150px;
		transition: .3s ease;
		font-style: italic;
		text-transform: uppercase;
		text-shadow: -1px 1px 1px #ffffff;
		color: #000000 !important;
		font-weight: 900 !important;
		font-family: sans-serif;

	}
	.column_4 .item {
		margin-top:10px;
	}

	.promotion-image img {
		max-height:inherit !important;
	}

	.banner_accesorios {
		position: relative;
		overflow:hidden;
		width:100%;
		height:inherit;
	}
	.banner_cover {
		width:100%;
		height:100%;
		opacity: 0;
		background-color:#ffffff;
	}
	.banner_accesorios img, .banner_accesorios .banner_cover {
		position: absolute;
		left:0;
		top:0;

		transition-delay:0s;
		transition-duration:0.2s;
		transition-property:all;
		transition-timing-function:cubic-bezier(0.39, 0.575, 0.565, 1);
	}
	.banner_accesorios .base {
		position: relative;
	}
	.banner_accesorios .barra {
		left:-40px;
	}
	.banner_accesorios .texto {
		/*left:-40px;*/
		transition-delay:0.1s;
	}

	.banner_accesorios:hover .barra {
		left:0px;
	}

	.banner_accesorios:hover .texto {
		left:25px;
	}
	.banner_accesorios:hover .banner_cover {
		opacity: 0.1;
	}

	.partner_logo {
		width:100%;
		height:100%;
		text-align:center;
		padding:10px;
		vertical-align: middle;
		position: relative;

		transition-timing-function: cubic-bezier(0.390, 0.575, 0.565, 1.000);
	}

	.partner_logo a {
		/*border: 3px solid #e5e5e5;*/
	}

	.partner_logo a img{
		/*border: 3px solid #e5e5e5;*/

		width: 85%;
		height: 85%;
		margin-left:7px;
		margin-top:7px;


		transition-delay:0s;
		transition-duration:0.2s;
		transition-property:all;
		transition-timing-function:cubic-bezier(0.39, 0.575, 0.565, 1);
	}

	/*.partner_logo_cover {*/
		/*background-color: rgba(255, 255, 255, 0);*/
		/*position: absolute;*/
		/*left: 0;*/
		/*top: 0;*/
		/*!*border: 1px solid #000;*!*/
		/*width: 100%;*/
		/*height: 100%;*/
		/*transition-delay:0s;*/
		/*transition-duration:0.2s;*/
		/*transition-property:all;*/
		/*transition-timing-function:cubic-bezier(0.39, 0.575, 0.565, 1);*/
	/*}*/

	/*.partner_logo:hover .partner_logo_cover {*/
		/*background-color: rgba(255, 255, 255, 0.5);*/
		/*position: absolute;*/
		/*left: 15%;*/
		/*top: 15%;*/
		/*width: 70%;*/
		/*height: 70%;*/

		/*margin:auto;*/
		/*text-align:center;*/

		/*transition-timing-function: cubic-bezier(0.390, 0.575, 0.565, 1.000);*/
	/*}*/

	.partner_logo:hover img {

		margin-left:5px;
		margin-top:5px;
		width: 90%;
		height: 90%;
	}

	/* Ref.: https://www.w3schools.com/howto/howto_css_center-vertical.asp */

	/* *************************************** */

	/*@media (min-width:1920px) {*/
		/*.carousel .carousel-inner,*/
	    /*.carousel .carousel-inner .item,*/
	    /*.slider .carousel  {*/
			/*!*margin-top:-110px;*!*/
			/*!*min-width:1330px;*!*/
			/*min-height:480px;*/
		/*}*/
		/*.promotion-image img {*/
			/*width:100%;*/
		/*}*/
		/*.promotion-caption h4 {*/
			/*margin-top: 155px;*/
		/*}*/
		/*.promotion-caption:hover h4 {*/
			/*margin-top: 135px;*/
		/*}*/
	/*}*/
	@media (max-width:1190px) {
		.carousel .carousel-inner,
	    .carousel .carousel-inner .item,
	    .slider .carousel  {
			/*margin-top:-50px;*/
			/*min-width:1330px;*/
			min-height:411px;
		}
	}
	@media (max-width:1200px) {
		.carousel .carousel-inner,
	    .carousel .carousel-inner .item,
	    .slider .carousel  {
			/*margin-top:-50px;*/
			/*min-width:1330px;*/
			min-height:340px;
		}
		.promotion-caption h4 {
			margin-top:115px;
		}
		.promotion-caption:hover h4 {
			margin-top:100px;
		}
	}
	@media (max-width:992px) {
		.carousel .carousel-inner,
	    .carousel .carousel-inner .item,
	    .slider .carousel  {
			/*margin-top:-50px;*/
			/*min-width:1330px;*/
			min-height:260px;
		}
	}
	@media (max-width:892px) {
		.carousel .carousel-inner,
	    .carousel .carousel-inner .item,
	    .slider .carousel  {
			/*margin-top:-20px;*/
			/*min-width:1330px;*/
			min-height:260px;
		}
	}
	@media (max-width:769px) {
		.carousel .carousel-inner,
	    .carousel .carousel-inner .item,
	    .slider .carousel  {
			/*margin-top:50px;*/
		}
		.carousel .carousel-caption, .carousel .carousel-caption.carousel-caption-left, .carousel .carousel-caption.carousel-caption-right {
			background:none !important;
		}
	}
	@media (max-width:560px) {
		.carousel .carousel-inner,
	    .carousel .carousel-inner .item,
	    .slider .carousel  {
			/*margin-top:50px;*/
			/*width:160%;*/
			min-height:176px;
		}
		.promotion {
			padding-top:124px !important;
		}
		.promotion-caption:hover h4 {
			margin-top:69px;
		}
		.promotion-caption h4 {
			margin-top:97px;
			padding-top:5px;
			height:60px;
			transition: .3s ease;
		}
	    .promotion-image {
		    max-width:100%;
	    }
	}



</style>
@endpush
@section('content')

@if(count($banners) > 0)
	<div class="container" hidden>
	<!-- BEGIN #slider -->
	<div id="slider" class="section-container p-0 bg-black-darker">
		<!-- BEGIN carousel -->
		<div id="main-carousel" class="carousel slide" data-ride="carousel">
			<!-- BEGIN carousel-inner -->
			<div class="carousel-inner">

				{{--@if( App::getLocale() == 'se' )--}}
					{{--<div class="item active ">--}}
							{{--<a href="javascript:;" onclick="displayWelcomeBanner()">--}}
								{{--<img src="/assets/banner_se.jpeg" class="bg-cover-img cover-special" alt="" />--}}
							{{--</a>--}}
					{{--</div>--}}
				{{--@elseif( App::getLocale() == 'en' )--}}
					{{--<div class="item active ">--}}
						{{--<a href="javascript:;" onclick="displayWelcomeBanner()">--}}
							{{--<img src="/assets/banner_en_2.jpeg" class="bg-cover-img cover-special" alt="" />--}}
						{{--</a>--}}
					{{--</div>--}}
				{{--@else--}}
					{{--<div class="item active ">--}}

						{{--@if($global_isProvider == true)--}}
							{{--<a href="javascript:;" onclick="displayWelcomeBanner()">--}}
								{{--<img src="/assets/banner_es_2_vende.jpeg" class="bg-cover-img cover-special" alt="" />--}}
							{{--</a>--}}
						{{--@else--}}
							{{--<a href="javascript:;" onclick="displayWelcomeBanner()">--}}
								{{--<img src="/assets/banner_es_compra.jpeg" class="bg-cover-img cover-special" alt="" />--}}
							{{--</a>--}}
						{{--@endif--}}

					{{--</div>--}}
				{{--@endif--}}


{{--				@for($i = 0 ; $i < count($imgBanners) ; $i++)--}}
				@php
					$i = 0;
				@endphp
				@foreach($banners as $banner)
					<div class="item @if($i == 0) active @endif">
						@if( trim($banner->enlace) == '')
							@if( trim($banner->onclick) != '')
								<a href="javascript:;" onclick="{{ $banner->onclick }}">
									<img src="{{ $banner->Galeria->Imagenes->first()->ruta_publica_banner }}" class="bg-cover-img cover-special" alt="" />
								</a>
							@else
								<img src="{{ $banner->Galeria->Imagenes->first()->ruta_publica_banner }}" class="bg-cover-img cover-special" alt="" />
							@endif
						@else
							<a href="{{ $banner->enlace }}"
							    @if( preg_match("/javascript:/", $banner->enlace) < 1 ) target="_blank" @endif
								@if( trim($banner->onclick) != '') onclick="{{ $banner->onclick }}" @endif
							>
								<img src="{{ $banner->Galeria->Imagenes->first()->ruta_publica_banner }}" class="bg-cover-img cover-special" alt="" />
							</a>
						@endif

						<div class="container fadeInRightBig animated">

						</div>
						<div class="carousel-caption carousel-caption-right">
							<div class="container">

							</div>
						</div>
					</div>
				@php
					$i++;
				@endphp
				@endforeach

			</div>
			<!-- END carousel-inner -->
			<a class="left carousel-control" href="#main-carousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="right carousel-control" href="#main-carousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
		<!-- END carousel -->
	</div>
	<!-- END #slider -->
	</div>
@endif

	<!-- BEGIN search-results -->
	<div id="search-results" class="section-container bg-silver__ p-t-20 p-b-10">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN search-container -->
			<div class="search-container">
				<!-- BEGIN search-content -->
				<div class="search-content m-b-40" style="min-height: 1180px;">

					<!-- BEGIN row -->
					<div class="row row-space-10">
						<!-- BEGIN col-3 -->
						<div class="col-md-4 col-sm-6 col-xs-6">
							<!-- BEGIN promotion -->

							<div class="promotion bg-white">
								<a href="{{ route('public.transport') }}">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="left:0px; top:0px;">
										<img src="/assets/img/e-commerce/cat_transporte_7_400_opt.jpg" alt="" />
									</div>
									<div class="promotion-caption promotion-caption-inverse text-right">
										<h4 class="promotion-title">@lang('messages.transportation')</h4>
									</div>
								</a>
							</div>

						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">

							<div class="promotion bg-white">
							<a href="{{ route('public.lantbruksmaskiner') }}">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="top:0px; left:0px;">
										<img src="/assets/img/e-commerce/cat_agricultura1_400_opt.jpg" alt="" />
									</div>
									<div class="promotion-caption text-center">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.agriculture')</h4>
									</div>
								</a>
							</div>

							<!-- END promotion -->
						</div>
						<!-- END col-3 -->
						<!-- BEGIN col-3 -->
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="promotion bg-white">
								<a href="{{ route('public.entreprenadmaskiner') }}">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="left:0px; top:0px;">
										<img src="/assets/img/e-commerce/cat_equipment_2_400_opt.jpg" alt="" />
									</div>
									<div class="promotion-caption text-center">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.equipments')</h4>
									</div>
								</a>
							</div>

						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="promotion bg-white">
								<a href="{{ route('public.truckar') }}">
									<div class="promotion-image promotion-image-overflow-right  promotion-image-overflow-bottom" style="left:0px; top:0px;">
										<img src="/assets/img/e-commerce/cat_almacen_1_400_opt.jpg" alt="" />
									</div>
									<div class="promotion-caption text-center">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.forklifts')</h4>
									</div>

								</a>
							</div>
						</div>
						<!-- END col-3 -->

						<!-- BEGIN col-3 -->
						<div class="col-md-4 col-sm-6 col-xs-6">
							<!-- BEGIN promotion -->
							<div class="promotion bg-white">
								<a href="{{ route('public.industri') }}">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="left:0px; top:0px;">
										<img src="/assets/img/e-commerce/cat_industrial1_400_opt.jpg" alt="" />
									</div>
									<div class="promotion-caption promotion-caption-inverse">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.industrial')</h4>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="promotion bg-white">
								<a href="{{ route('public.ovriga') }}">
									<div class="promotion-image promotion-image-overflow-right  promotion-image-overflow-bottom" style="left:0px; top:0px;">
										<img src="/assets/img/e-commerce/cat_otros_400_opt.jpg" alt="" />
									</div>
									<div class="promotion-caption promotion-caption-inverse">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.others')</h4>
									</div>
								</a>
							</div>
							<!-- END promotion -->
						</div>
						<!-- END col-3 -->

					</div>
					<!-- END row -->

					{{--<h4 class="section-title clearfix m-t-20">@lang('messages.accesories')</h4>--}}

					<div class="row m-t-40">
						<div class="col col-md-12">
							<div class="banner_accesorios">
								<a href="javascript:;" onclick="searchAccesorios()">
									<img class="base" style="width:100%;" src="/assets/img/banners/accesorios/Banner-accesorios-v2.7_base.jpg" alt="">
									<img class="barra" style="width:100%;" src="/assets/img/banners/accesorios/Banner-accesorios-v2.7_barra.png" alt="">
									<img class="texto" style="width:100%;" src="/assets/img/banners/accesorios/Banner-accesorios-v2.7_text_{{ $glc }}.png" alt="">
									<div class="banner_cover"></div>
								</a>
							</div>

						</div>
					</div>

					<div class="" id="latestAddedItemsContainer" style="min-height:200px; margin-top:40px;">

					</div>

					<h4 class="section-title clearfix m-t-40">@lang('messages.partners')</h4>

					<div class="row">

						<div class="col-md-4 col-sm-6">
							<div class="partner_logo">
								{{--<div class="partner_logo_cover"></div>--}}
								<a href="javascript:;"><img src="/assets/img/logo/Schuchardt_logo_2018_2x.png" alt="Logo proveedor" style="max-width:100%; padding: 80px 0px;"></a></td>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="partner_logo">
								{{--<div class="partner_logo_cover"></div>--}}
								<a href="javascript:;"><img src="/assets/img/logo/jungbymaskin.jpg" alt="Logo proveedor" style="max-width:100%; padding: 40px 0px;"></a>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="partner_logo">
								{{--<div class="partner_logo_cover"></div>--}}
								<a href="javascript:;"><img src="/company_logo/logo_jPpeL.png" alt="Logo proveedor" style="max-width:100%;"></a>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="partner_logo">
								{{--<div class="partner_logo_cover"></div>--}}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-6">
							<div class="partner_logo">
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="partner_logo">
								{{--<div class="partner_logo_cover"></div>--}}
								<a href="javascript:;"><img src="/company_logo/logo_8OP1A.png" alt="Logo proveedor" style="max-width:100%;"></a>
							</div>
						</div>


						{{--<div class="col col-md-4 text-center">--}}
							{{--<img src="/company_logo/logo_jPpeL.png" alt="Logo proveedor" style="max-width:100%;">--}}
						{{--</div>--}}
						{{--<div class="col col-md-4 text-center">--}}
							{{--<img src="/company_logo/logo_8OP1A.png" alt="Logo proveedor" style="max-width:100%;">--}}
						{{--</div>--}}
						{{--<div class="col col-md-4 text-center">--}}
							{{--<img src="/assets/img/logo/jungbymaskin.jpg" alt="Logo proveedor" style="max-width:100%;">--}}
						{{--</div>--}}
					</div>

					{{--<h4 class="section-title clearfix m-t-20">@lang('messages.news')</h4>--}}

					{{--<div style="background-color:#ffffff; border: 1px solid #c5ced4; padding: 10px;">--}}
						{{--<table>--}}
							{{--@for( $i = 0 ; $i < count($noticias) ; $i++)--}}
							{{--<tr class="news_link">--}}
								{{--<td style="width:100px; padding-right:10px;">--}}
									{{--<small class="pull-right">{{ $noticias[$i]->fechaPublicacion->format('d.m.Y') }}</small>--}}
								{{--</td>--}}
								{{--<td style="padding-left:10px;">--}}
									{{--<a href="javascript:;" style="padding:10px; border-left:1px solid #e5e5e5; display:block;" onclick="showNews({{ $noticias[$i]->id }})">{{ (trim($noticias[$i]['titulo_'.App::getLocale()]) == '') ? '- - -' : $noticias[$i]['titulo_'.App::getLocale()] }}</a>--}}
								{{--</td>--}}
							{{--</tr>--}}
							{{--@if($i < count($noticias) - 1)--}}
								{{--<tr>--}}
									{{--<td colspan="2" style="padding:5px;"><hr style="border-color:#e5e5e5; margin:0px;"></td>--}}
								{{--</tr>--}}
							{{--@endif--}}
							{{--@endfor--}}
						{{--</table>--}}
					{{--</div>--}}

				</div>
				<!-- END search-content -->
				<!-- BEGIN search-sidebar -->
				@include('frontend.includes.sidebarSearch', [
					'categorias' => $categorias,
					'grouped_tags' => $grouped_tags,
				])

				{{--<div class="search-sidebar m-t-10 mz_adverts" style="padding:0px; border:none;">--}}
					{{--<h4>@lang('messages.news')</h4>--}}

					{{--<div class="item">--}}
						{{--<p class="news_link"><a href="javascript:;">Chile - El Banco Central confirmó que la economía se expandió 4% en 2018, equivalente en dólares a US$298,7 mil millones, con una recuperación del crecimiento en todos los sectores y un indiscutible protagonista: la inversión.</a></p>--}}
						{{--<p class="news_link"><a href="javascript:;">Chile: Electrificación de equipos mineros: una tendencia que crece.  El auge que está teniendo la electromovilidad a nivel global no sólo representa un estímulo relevante para el sector minero, debido a los volúmenes de cobre y litio que los vehículos eléctricos requieren, también un pacto positivo en el plano ambiental. </a></p>--}}

						{{--@foreach($noticias as $noticia)--}}
							{{--<p class="news_link"><a href="{{ $noticia->fuenteUrl }}">{{ $noticia['titulo_'.App::getLocale()] }}</a></p>--}}
						{{--@endforeach--}}
					{{--</div>--}}
				{{--</div>--}}
				@include('frontend.includes.banners_left')
				<!-- END search-sidebar -->
			</div>
			<!-- END search-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END search-results -->

	{{--@include('frontend.components.html_promotions')--}}
	{{--@include('frontend.components.html_mobileList')--}}
{{--	@include('frontend.components.html_subscriptions')--}}


	{{--<div id="product" class="section-container p-t-20">--}}
		{{--<!-- BEGIN container -->--}}
		{{--<div class="container" id="latestAddedItems_footerContainer">--}}

		{{--</div>--}}
	{{--</div>--}}

	<div id="product" class="section-container p-t-0">
		<!-- BEGIN container -->
		<div class="container" id="mostVisitedItemsContainer">

		</div>
	</div>

	<!-- BEGIN #policy -->
	<div id="policy" class="section-container p-t-30 p-b-30">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-4 -->
				<div class="col-md-4 col-sm-4">
					<!-- BEGIN policy -->
					<div class="policy">
						<div class="policy-icon"><i class="fa fa-truck"></i></div>
						<div class="policy-info">
							<h4>
								<a href="#modal-proceso" data-toggle="modal">@lang('messages.process_flow')</a>
							</h4>
							<p>@lang('messages.index_process_flow_content')</p>
						</div>
					</div>
					<!-- END policy -->
				</div>
				<!-- END col-4 -->
				<!-- BEGIN col-4 -->
				<div class="col-md-4 col-sm-4">
					<!-- BEGIN policy -->
					<div class="policy">
						<div class="policy-icon"><i class="fas fa-people-arrows"></i></div>
						<div class="policy-info">
							<h4>
								<a href="#modal-aduana" data-toggle="modal">@lang('messages.index_brokers_title')</a>
							</h4>
							<p>@lang('messages.index_brokers_content')</p>
						</div>
					</div>
					<!-- END policy -->
				</div>
				<!-- END col-4 -->
				<!-- BEGIN col-4 -->
				<div class="col-md-4 col-sm-4">
					<!-- BEGIN policy -->
					<div class="policy">
						<div class="policy-icon"><i class="fas fa-smile"></i></div>
						<div class="policy-info">
							<h4>
								<a href="#modal-seguridad" data-toggle="modal">@lang('messages.index_security_title')</a>
							</h4>
							<p>@lang('messages.index_security_content')</p>
						</div>
					</div>
					<!-- END policy -->
				</div>
				<!-- END col-4 -->
			</div>
			<!-- END row -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #policy -->



	<div class="modal fade" id="modal-proceso">
		<div class="modal-dialog" style="width:60% !important;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('about_us.process_flow_popup_title')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					{{--<video id="example_video_1" class="video-js vjs-default-skin"--}}
					       {{--controls preload="auto" width="740" height="364"--}}
					       {{--poster="http://video-js.zencoder.com/oceans-clip.png"--}}
					       {{--data-setup='{"example_option":true}'>--}}
						{{--<source src="/about_us2.mp4" type="video/mp4" />--}}
						{{--<source src="http://video-js.zencoder.com/oceans-clip.webm" type="video/webm" />--}}
						{{--<source src="http://video-js.zencoder.com/oceans-clip.ogv" type="video/ogg" />--}}
						{{--<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>--}}
					{{--</video>--}}

					<img src="/assets/img/sobre_nosotros/flujo_{{ App::getLocale() }}.gif" alt="" style="width:100%; margin:auto;">

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-aduana">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('messages.index_brokers_title')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					@lang('about_us.broker_popup_content')
					<br><br>
					<div class="row">
						<div class="col col-md-6 text-center">
							{{--<div class="text-center">COPERSA S.R.L.<br><br></div>--}}
							<a href="http://www.copersa.net" target="_blank"><img src="/assets/img/e-commerce/COPERSA-LOGO.gif" alt=""  style="width:250px;"></a>

							<div class="text-left m-t-20" style="font-size:12px; color:#333333;">

								<b>@lang('messages.telephone')</b><br>
								(+591) 4408301<br>
								(+591) 4408302<br>
								(+591) 4408304<br>
								<br>
								<b>@lang('messages.e_mail')</b><br>
								gerencia@copersa.net<br>
								<br>
								<b>@lang('messages.address')</b><br>
								Av. Dorgbigni #1599 esq. Diego de Almagro<br>
								Edificio COPERSA primer piso<br>
								COCHABAMBA - BOLIVIA<br>
							</div>
						</div>
						<div class="col col-md-6 text-center" style="border-left:1px solid #e5e5e5;">
							<div class="text-center" style="font-size: 22px; font-weight: bold; line-height: 42px;">EX EDUARDO LINARES MACIAS Y CIA LTDA.</div>
							{{--<img src="/assets/img/e-commerce/aduana_linares.jpg" alt=""  style="width:250px;">--}}
							<div class="text-left m-t-20" style="font-size:12px; color:#333333;">

								<b>@lang('messages.telephone')</b><br>
								(+56) 57 2462238<br>
								<br>
								<b>@lang('messages.e_mail')</b><br>
								cpalape@eduardolinares.cl<br>
								<br>
								<b>@lang('messages.address')</b><br>
								Recinto amurallado <br>
								Edif. Casa Blanca<br>
								ZOFRI - IQUIQUE - CHILE<br>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-seguridad">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('about_us.security_popup_title')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					@lang('about_us.security_popup_content')
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-noticias">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('messages.news')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					Loading...
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	{{--<div class="modal fade" id="modal-welcome-banner">--}}
		{{--<div class="modal-dialog modal-lg">--}}
			{{--<div class="modal-content">--}}
				{{--<div class="modal-header">--}}
					{{--<h4 class="modal-title" style="display: inline;">@lang('messages.welcome')</h4>--}}
					{{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
				{{--</div>--}}
				{{--<div class="modal-body">--}}
					{{--<a href="#modal-proceso" data-toggle="modal" data-dismiss="modal">--}}
						{{--<img src="/assets/img/e-commerce/welcome_banner.jpg" alt="" style="width:100%;" />--}}
					{{--</a>--}}
				{{--</div>--}}
				{{--<div class="modal-footer">--}}
					{{--<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--</div>--}}

<div class="modal fade" id="modal-welcome-banner">
	<div class="modal-dialog video-dialog" style="width:60% !important;">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" style="display: inline;">@lang('messages.welcome')</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body" style="background-color:#000000; padding:0px;">
				<video id="welcome_video" class="video-js vjs-default-skin"
				controls preload="auto" width="100%"
				{{--poster="public_assets/banner/bnr_ub0In.jpeg"--}}
				data-setup='{"example_option":true}'>
					{{--<source id="video_source" src="" type="video/mp4" />--}}
				{{--<source src="http://video-js.zencoder.com/oceans-clip.webm" type="video/webm" />--}}
				{{--<source src="http://video-js.zencoder.com/oceans-clip.ogv" type="video/ogg" />--}}
				<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
				</video>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.dotdotdot/4.0.5/dotdotdot.js"></script>
<script>
	$(document).ready(function(){
		mostVisitedItems();
		latestAddedItems();
//		var dot = new Dotdotdot( $('.item'), {
//			truncate: "word",
//			height: 60
//		} );
//		$('.news_link').dotdotdot({
//			truncate: "word",
//		});

		{{--@if($global_isProvider == true && $welcomeBanner == true)--}}
			{{--setTimeout( function() {--}}
			{{--displayWelcomeBanner();--}}
		{{--} , 21600);--}}
		{{--@endif--}}

		@if( $autoVideo == true )
			displayWelcomeBanner();
		@endif
	});

	function mostVisitedItems(page){
		{{--var url = '{{ route('component.mostVisitedItems') }}';--}}
		{{--$('#mostVisitedItemsContainer').load(url);--}}

		var url = '';
		if(page === undefined) {
			url = '{{ route('component.mostVisitedItems') }}';
			$('#mostVisitedItemsContainer').load(url);
		} else {
			if(page < 1) {
				page = 1;
			}
			url = '{{ route('component.mostVisitedItems', ['page' => 'noPage']) }}';
			url = url.replace('noPage', page);
			$('#mostVisitedItemsContainer').load(url);
			curPage = page;
		}
	}

	function latestAddedItems(page){
		{{--var url = '{{ route('component.latestAddedItems') }}';--}}
		{{--$('#latestAddedItemsContainer').load(url);--}}

		var url = '';
		if(page === undefined) {
			url = '{{ route('component.latestAddedItems') }}';
			$('#latestAddedItemsContainer').load(url);
		} else {
			if(page < 1) {
				page = 1;
			}
			url = '{{ route('component.latestAddedItems', ['page' => 'noPage']) }}';
			url = url.replace('noPage', page);
			$('#latestAddedItemsContainer').load(url);
			curPage = page;
		}

		var url = '';
		if(page === undefined) {
			url = '{{ route('component.latestAddedItems_footer') }}';
			$('#latestAddedItems_footerContainer').load(url);
		} else {
			if(page < 1) {
				page = 1;
			}
			url = '{{ route('component.latestAddedItems_footer', ['page' => 'noPage']) }}';
			url = url.replace('noPage', page);
			$('#latestAddedItems_footerContainer').load(url);
			curPage = page;
		}
	}

	function showNews(id) {
		$('#modal-noticias .modal-body').html('@lang('messages.loading')...');
		var url = '{{ route('public.getnews', ['idn']) }}';
		url = url.replace('idn', id);
		$('#modal-noticias .modal-body').load(url, function(response, status, xhr){

//			if(xhr.status == 500){
//				response = response.replace('position: fixed;', '');
//				$(this).html(response.data.resumen_es);
//				$(this).closest('.modal-dialog').css({
////                    'height': '100%',
//					'width': '90%'
//				});
//				$(this).find('.left-panel').css({'height':'800px'});
////                $(this).closest('.modal-content').css('height', '100%');
//
//			}
		});
		$('#modal-noticias').modal('show');
	}

	function displayWelcomeBanner() {

		var video = document.getElementById('welcome_video');
		var source = document.createElement('source');

		@if( App::getLocale() == 'es' )
			@if($global_isProvider == true)
				source.setAttribute('src', '/videos/welcome_video_es_vende_720.mp4');
			@else
				source.setAttribute('src', '/videos/welcome_video_es_compra.mp4');
			@endif
		@elseif( App::getLocale() == 'en' )
			source.setAttribute('src', '/videos/welcome_video_en_720.mp4');
		@else
			source.setAttribute('src', '/videos/welcome_video_se_720.mp4');
		@endif

		video.appendChild(source);

		var modalWelcome = $('#modal-welcome-banner').modal();
		{{--ajaxPost('{{ route('public.setWelcomeBanner') }}',{ nuevoUsuario: 1 } );--}}

		modalWelcome.on('shown.bs.modal', function (e) {
			$("#welcome_video").each(function () { this.play() });
		});

		modalWelcome.on('hidden.bs.modal', function (e) {
			$("#welcome_video").each(function () { this.pause() });
		});
	}

	function searchAccesorios() {
		$('#sideSearchTipoMaquina').val({{ \App\Models\Producto::TIPO_ACCESORIO }});
		$('#sideBarSearchForm').submit();
	}
</script>
@endpush
