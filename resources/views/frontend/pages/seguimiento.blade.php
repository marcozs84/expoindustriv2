@extends('layouts.default')

@section('title', 'Seguimiento')

@push('css')

<link href="/assets/plugins/isotope/isotope.css" rel="stylesheet" />
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />
<style>
	.remove_btn {
		position: absolute;
		bottom: 0;
		left:0;
	}
	.timeline-comment-box p {
		/*border-top:1px dashed #a9a9a9;*/
		padding:6px;
		border-bottom: 1px solid #e3e4e5;
	}

	.timeline-body.timeline_disabled:before {
		border-right-color: #e5e5e5;
	}
	.timeline-body.timeline_disabled {
		background-color:#e5e5e5;
	}
	.timeline_disabled .timeline-header .username a {
		color:#adadad;
	}
</style>
@endpush

@section('content')
	<!-- begin breadcrumb -->
	{{--<ol class="breadcrumb pull-right">--}}
		{{--<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>--}}
		{{--<li class="breadcrumb-item"><a href="javascript:;">Extra</a></li>--}}
		{{--<li class="breadcrumb-item active">Timeline</li>--}}
	{{--</ol>--}}
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Seguimiento de entrega
		{{--<small>Desde la puerta del proveedor hasta la puerta de su casa.</small>--}}
	</h1>
	<!-- end page-header -->

	<ul class="timeline">
		@foreach( $actividades as $actividad )
			<li>
				<div class="timeline-time">
					@php
						$fecha = $hora = '';
						$class_body = '';
						$class_comentarios = '';
						if( $actividad->Archivos && $actividad->Archivos->count() > 0 ) {
							if($actividad->Archivos[0]->created_at->startOfDay() == \Carbon\Carbon::today()->startOfDay()) {
								$fecha = 'Hoy día';
							} elseif($actividad->Archivos[0]->created_at->startOfDay() == \Carbon\Carbon::today()->subDays(1)->startOfDay()) {
								$fecha = 'Ayer';
							} else {
								$fecha = $actividad->Archivos[0]->created_at_literal;
							}
							$hora = $actividad->Archivos[0]->created_at->format('H:i');
						} elseif( $actividad->Comentarios && $actividad->Comentarios->count() > 0 ) {
							if($actividad->Comentarios[0]->created_at->startOfDay() == \Carbon\Carbon::today()->startOfDay()) {
								$fecha = 'Hoy día';
							} elseif($actividad->Comentarios[0]->created_at->startOfDay() == \Carbon\Carbon::today()->subDays(1)->startOfDay()) {
								$fecha = 'Ayer';
							} else {
								$fecha = $actividad->Comentarios[0]->created_at_literal;
							}
							$hora = $actividad->Comentarios[0]->created_at->format('H:i');
						} else {
							$class_body = 'timeline_disabled';
							$class_comentarios = 'hide';
						}
					@endphp

					<span class="date">{!! ( trim($fecha) != '' ) ? $fecha : '<b>EN PROCESO</b>' !!}</span>
					<span class="time">{{ $hora }}</span>
				</div>
				<!-- begin timeline-icon -->
				<div class="timeline-icon">
					<a href="javascript:;">&nbsp;</a>
				</div>
				<!-- end timeline-icon -->
				<!-- begin timeline-body -->
				<div class="timeline-body {{ $class_body }}">
					<div class="timeline-header">
						{{--<span class="userimage"><img src="../assets/img/user/user-1.jpg" alt="" /></span>--}}
						<span class="username"><a href="javascript:;">{{ $actividad->ActividadTipo->nombre }}</a> <small></small></span>
						<span class="pull-right text-muted">{{ $fecha }}</span>
					</div>
					<div class="timeline-content">
						<p>
							{{ $actividad->ActividadTipo->descripcion }}
						</p>

						<div id="archivos_{{ $actividad->id }}" class="row">
							@foreach( $actividad->Archivos as $archivo )
								@if( in_array( $archivo->mimetype, [ 'image/gif', 'image/jpeg', 'image/jpg', 'image/png', ]) )
									<div class="col col-md-3 text-center" id="archivo_{{ $archivo->id }}">
										<a href="{{ route('resource.archivo.stream', [$archivo->id]) }}" data-lightbox="gallery-group-{{ $actividad->id }}" title="{{ route('resource.archivo.stream', [$archivo->id]) }}">
											<img src="{{ route('resource.archivo.stream', [$archivo->id]) }}" alt="{{ route('resource.archivo.stream', [$archivo->id]) }}" class="" style="max-height:100%; display:block; clear:both;">
										</a>
									</div>
								@else
									<div class="col col-md-12 text-left" id="archivo_{{ $archivo->id }}">
										<a href="{{ route('resource.archivo.stream', [$archivo->id]) }}" target="_blank" title="{{ route('resource.archivo.stream', [$archivo->id]) }}" style="font-size:14px;">
											<i class="fa fa-file fa-lg m-r-10"></i> {{ $archivo->realname }}
										</a>
									</div>
								@endif
							@endforeach
						</div>

						<div id="gallery_{{ $actividad->id }}" class="row">
							@if( $actividad->Galeria && $actividad->Galeria->Imagenes->count() > 0 )
								@foreach($actividad->Galeria->Imagenes as $imagen )
									<div class="col col-md-3">
										<img src="{{ $imagen->ruta_publica }}" alt="" style="max-width:100%;">
									</div>
								@endforeach
							@endif
						</div>
					</div>

					{{--<div class="timeline-likes">--}}
						{{--<div class="stats-right">--}}
							{{--<span class="stats-text">259 Shares</span>--}}
							{{--<span class="stats-text">21 Comments</span>--}}
						{{--</div>--}}
						{{--<div class="stats">--}}
						{{--<span class="fa-stack fa-fw stats-icon">--}}
						  {{--<i class="fa fa-circle fa-stack-2x text-danger"></i>--}}
						  {{--<i class="fa fa-heart fa-stack-1x fa-inverse t-plus-1"></i>--}}
						{{--</span>--}}
							{{--<span class="fa-stack fa-fw stats-icon">--}}
						  {{--<i class="fa fa-circle fa-stack-2x text-primary"></i>--}}
						  {{--<i class="fa fa-thumbs-up fa-stack-1x fa-inverse"></i>--}}
						{{--</span>--}}
							{{--<span class="stats-total">4.3k</span>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<div class="timeline-footer">--}}
						{{--<a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-thumbs-up fa-fw fa-lg m-r-3"></i> Like</a>--}}
						{{--<a href="javascript:;" class= m-r-15 text-inverse-lighter"><i class="fa fa-comments fa-fw fa-lg m-r-3"></i> Comment</a>--}}
						{{--<a href="javascript:;" class="m-r-15 text-inverse-lighter"><i class="fa fa-share fa-fw fa-lg m-r-3"></i> Share</a>--}}
					{{--</div>--}}
					<div class="timeline-comment-box {{ $class_comentarios }}">
						<div class="comments_{{ $actividad->id }}"></div>
						{{--<div class="user"><img src="../assets/img/user/user-13.jpg" /></div>--}}
						<div class="input">
							<form action="">
								<div class="input-group">
									<textarea id="comment_{{ $actividad->id }}" class="form-control rounded-corner" style="-webkit-border-radius: 15px !important; -moz-border-radius: 15px !important;border-radius: 15px !important;" placeholder="Escriba un comentario..." rows="3" ></textarea>
									<span class="input-group-btn p-l-10">
									<a href="javascript:;" class="btn btn-primary f-s-12 rounded-corner" onclick="postComment({{ $actividad->id }})" type="button">Comentar</a>
								</span>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- end timeline-body -->
			</li>
		@endforeach
	</ul>
	
@endsection

@push('scripts')

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>

    {{--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>--}}
<script src="/assets/js/demo/timeline.demo.js"></script>
<script>
	$(document).ready(function() {
//		Timeline.init();
		loadCommentsMass();
	});

	function loadCommentsMass() {

		var url = '{{ route('resource.actividad.getComentarios', [ 0 ]) }}';
		var data = {
			ids: {!! json_encode( $actividades->pluck('id')->toArray() ) !!}
		};
		ajaxPost(url, data, function( response ) {
			$.each( response.data, function( idActividad, comentarios ) {
				$('.comments_' + idActividad).html( '' );
				$.each( comentarios , function( index, comentario) {
					comment = $('<p>');
					comment.append( comentario.mensaje );

					if( comentario.idUsuario > 0 ) {
						nombres_apellidos = comentario.usuario.nombres_apellidos;
					} else {
						nombres_apellidos = 'Cliente';
					}

					span    = $('<span>', {
						'style' : ''
					}).html(' &mdash; <b>' + nombres_apellidos + '</b> ');
					span.append( format_datetime( comentario.created_at , { format : 'legible_resumido' } ) );
					comment.append( span );
					$('.comments_' + idActividad).append( comment );
				})
			} );
		});

	}

	function loadComments( idActividad ) {

		var url = '{{ route('resource.actividad.getComentarios', [ 'idActividad' ]) }}';
		url = url.replace('idActividad', idActividad);
		ajaxGet(url, function( response ) {
			$('.comments_' + idActividad).html( '' );
			$.each( response.data , function( index, comentario) {
				comment = $('<p>');
				comment.append( comentario.mensaje );

				if( comentario.idUsuario > 0 ) {
					nombres_apellidos = comentario.usuario.nombres_apellidos;
				} else {
					nombres_apellidos = 'Cliente';
				}

				span    = $('<span>', {
					'style' : ''
				}).html(' &mdash; <b>' + nombres_apellidos + '</b> ');
				span.append( format_datetime( comentario.created_at , { format : 'legible_resumido' } ) );
				comment.append( span );
				$('.comments_' + idActividad).append( comment );
			})
		});
	}

	function postComment( idActividad ) {
		var comment = $('#comment_' + idActividad).val();
				{{--var url = '{{ route('resource.comentario.store') }}';--}}
		var url = '{{ route('resource.actividad.addComentario', [ 'idActividad' ]) }}';
		url = url.replace('idActividad', idActividad);

		var data = {
			'asunto': '',
			'mensaje' : comment
		};
		ajaxPost(url, data, function( response ){

			$('#comment_' + idActividad).val('');
			loadComments( idActividad );

		});
	}


</script>
@endpush