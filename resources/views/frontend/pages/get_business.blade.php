@extends('frontend.layouts.default')
@push('css')
<style>
	/* -------------------------------
   12.0 Pricing Setting
------------------------------- */

	/* 12.1 Pricing Element Setting */

	.pricing-table {
		list-style-type: none;
		margin: 0 -10px;
		padding: 0;
		text-align: center;
	}
	.pricing-table:before,
	.pricing-table:after {
		content: '';
		display: table;
		clear: both;
	}
	.pricing-table > li {
		float: left;
		padding: 10px;
	}
	.pricing-table.pricing-col-4 > li {
		width: 25%;
	}
	.pricing-table.pricing-col-3 > li {
		width: 33.33333%;
	}
	.pricing-table .pricing-container {
		overflow: hidden;
		border-radius: 6px;
		background: #f0f3f4;
		box-shadow: 0 3px #b6c2c9;
	}
	.pricing-table h3 {
		background: #242a30;
		margin: 0;
		color: #fff;
		font-size: 14px;
		padding: 15px 30px;
		font-weight: bold;
	}
	.pricing-table .features {
		list-style-type: none;
		margin: 0;
		padding: 0 30px;
	}
	.pricing-table .features > li {
		padding: 10px 0;
	}
	.pricing-table .features > li + li {
		border-top: 1px solid #e2e7eb;
	}
	.pricing-table .price {
		width: 100%;
		display: table;
		background: #2d353c;
	}
	.pricing-table .price .price-figure {
		vertical-align: middle;
		display: table-cell;
		text-align: center;
		height: 80px;
	}
	.pricing-table .price .price-number {
		font-size: 28px;
		color: #00a3a3;
		display: block;
	}
	.pricing-table .price .price-tenure {
		font-size: 12px;
		color: #fff;
		color: rgba(255, 255, 255, 0.7);
		display: block;
		text-align: center;
	}
	.pricing-table .footer {
		padding: 15px 20px;
	}
	.pricing-table .highlight {
		padding: 0px;
		margin-top: -30px;
	}
	.pricing-table .highlight .features > li {
		padding: 15px 0;
	}
	.pricing-table .highlight h3 {
		padding: 20px 30px;
		background:#008a8a;
	}
	.pricing-table .highlight .price {
		background-color:rgb(0, 172, 172);
	}
	.pricing-table .highlight .footer {
		/*background-color:#008a8a;*/
		box-shadow: none;
		-webkit-box-shadow:none;
	}
	.pricing-table .highlight .price .price-figure {
		height: 90px;
	}
	.pricing-table .highlight .price .price-number {
		color: #fff;
	}

	.btn-block {
		padding-top: 15px;
		padding-bottom: 15px;
		padding-left: 12px;
		padding-right: 12px;
	}

	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}

	.checkout-footer {
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 1);  /* blue */
	}
</style>
@endpush

@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -360px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.plan_business')</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN checkout-body -->
			<div class="checkout-body">
				<div class="table-responsive">
					<table class="table table-cart">
						<thead>
						<tr>
							<th>@lang('messages.product_name')</th>
							<th class="text-center">@lang('messages.price')</th>
							{{--<th class="text-center">Quantity</th>--}}
							<th class="text-center" style="min-width:120px;">@lang('messages.total')</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td class="cart-product">
								{{--<div class="product-img">--}}
									{{--<img src="../assets/img/product/product-iphone-6s-plus.png" alt="" />--}}
								{{--</div>--}}
								<div class="product-info">
									<div class="title">Business plan</div>
									<div class="desc">From {{ \Carbon\Carbon::today()->format('d/m/Y') }} to {{ \Carbon\Carbon::today()->addYear(1)->format('d/m/Y') }}</div>
								</div>
							</td>
							<td class="cart-price text-center">6.900 kr</td>
							{{--<td class="cart-qty text-center">--}}
								{{--<div class="cart-qty-input">--}}
									{{--<a href="#" class="qty-control left disabled" data-click="decrease-qty" data-target="#qty"><i class="fa fa-minus"></i></a>--}}
									{{--<input type="text" name="qty" value="1" class="form-control" id="qty" />--}}
									{{--<a href="#" class="qty-control right disabled" data-click="increase-qty" data-target="#qty"><i class="fa fa-plus"></i></a>--}}
								{{--</div>--}}
								{{--<div class="qty-desc">1 to max order</div>--}}
							{{--</td>--}}
							<td class="cart-total text-center">
								6.900 kr
							</td>
						</tr>
						<tr>
							<td class="cart-product">
								{{--<div class="product-img">--}}
									{{--<img src="../assets/img/product/product-iphone-6s-plus.png" alt="" />--}}
								{{--</div>--}}
								<div class="product-info">
									<div class="title" style="text-transform:capitalize;">@lang('messages.taxes') <small>( 25% )</small></div>
								</div>
							</td>
							<td class="cart-price text-center">{{ ( (6900 / 100) * 25 ) }} kr</td>
							<td class="cart-total text-center">{{ ( (6900 / 100) * 25 ) }} kr</td>
						</tr>
						<tr>
							<td class="cart-summary" colspan="4">
								<div class="summary-container">
									{{--<div class="summary-row">--}}
										{{--<div class="field">Cart Subtotal</div>--}}
										{{--<div class="value">{{ 6900 + ( (6900 / 100) * 25 ) }} kr</div>--}}
									{{--</div>--}}
									{{--<div class="summary-row text-danger">--}}
										{{--<div class="field">Free Shipping</div>--}}
										{{--<div class="value">$0.00</div>--}}
									{{--</div>--}}
									<div class="summary-row total" style="border:none;">
										<div class="field">@lang('messages.total')</div>
										<div class="value">{{ 6900 + ( (6900 / 100) * 25 ) }} kr</div>
									</div>
								</div>
							</td>
						</tr>
						</tbody>
					</table>

				</div>
			</div>
			<!-- END checkout-body -->
			<!-- BEGIN checkout-footer -->
			<div class="checkout-footer">
				{{--<a href="#" class="btn btn-white btn-lg pull-left">Continue Shopping</a>--}}
{{--				<a href="{{ route('public.checkout_business') }}" class="btn btn-inverse btn-lg p-l-30 p-r-30 m-l-10">@lang('messages.buy_now')</a>--}}


				<!-- Load Stripe.js on your website. -->
				<script src="https://js.stripe.com/v3"></script>













				<!-- Create a button that your customers click to complete their purchase. Customize the styling to suit your branding. -->
				{{--<a href="{{ route('public.checkout_business', [ 'type' => 'faktura' ]) }}" class="btn btn-inverse btn-lg p-l-30 p-r-30 m-l-10">--}}
					{{--@lang('messages.payment_faktura')--}}
				{{--</a>--}}
				<a href="{{ route("public.{$glc}.factura_detail", [ 'type' => 'faktura', 'plan' => 'business' ]) }}" class="btn btn-inverse btn-lg p-l-30 p-r-30 m-l-10">
					@lang('messages.payment_faktura')
				</a>










				<button
						{{--style="background-color:#6772E5;color:#FFF;padding:8px 12px;border:0;border-radius:4px;font-size:1em"--}}
						id="checkout-button-3"
						role="link"
				        class="btn btn-inverse btn-lg p-l-30 p-r-30 m-l-10"
				>
					@lang('messages.pay_with_card')
				</button>

				<div id="error-message"></div>

				<script>
					(function() {
						var stripe = Stripe( '{{ $product_key }}' );

						var checkoutButton = document.getElementById('checkout-button-3');
						checkoutButton.addEventListener('click', function () {
							// When the customer clicks on the button, redirect
							// them to Checkout.
							stripe.redirectToCheckout({
//								items: [{plan: '3', quantity: 1}],
								{{--clientReferenceId: '{{ $usuario->getStripeId() }}',--}}
								{{--customerEmail: '{{ $usuario->username }}',--}}
								sessionId: '{{ $stripe_session_id }}'
								// Do not rely on the redirect to the successUrl for fulfilling
								// purchases, customers may not always reach the success_url after
								// a successful payment.
								// Instead use one of the strategies described in
								// https://stripe.com/docs/payments/checkout/fulfillment
//								successUrl: window.location.protocol + '//www2.expoindustriv2/checkout_business?session_id={CHECKOUT_SESSION_ID}',
//								cancelUrl: window.location.protocol + '//www2.expoindustriv2/canceled',
							})
								.then(function (result) {
									if (result.error) {
										// If `redirectToCheckout` fails due to a browser or network
										// error, display the localized error message to your customer.
										var displayError = document.getElementById('error-message');
										displayError.textContent = result.error.message;
									}
								});
						});
					})();
				</script>
			</div>
			<!-- END checkout-footer -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection