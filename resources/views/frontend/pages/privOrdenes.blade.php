@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.my_orders')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar">
					<div class="account-sidebar-cover">
						<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />
					</div>
					<div class="account-sidebar-content">
						<h4>@lang('messages.my_orders')</h4>
						<p>
							@lang('messages.my_orders.leftmessage')
						</p>
					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">

							<table id="data-table" class="table table-striped width-full table-condensed"></table>

							<!-- BEGIN table-product -->
							<table class="table table-listing table-striped ">
								<thead>
								<tr>
									<th># ID</th>
									<th>Producto</th>
									<th>Fecha</th>
									<th>Cotización</th>
									<th>Estado</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td class="field"><a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>15.000 USD</td>
									<td>En progreso</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>15.000 USD</td>
									<td>En progreso</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>15.000 USD</td>
									<td>En progreso</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>15.000 USD</td>
									<td>En progreso</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>15.000 USD</td>
									<td>En progreso</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privOrdenEstado', [0]) }}" style="color: #000;">Used Volvo F10 two-way side tipper truck 320 6x2 Diesel</a>
									</td>
									<td>10/09/2018</td>
									<td>15.000 USD</td>
									<td>En progreso</td>
								</tr>
								</tbody>
							</table>
							<!-- END table-product -->
						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

<script>

	var datatable = null;
	var modal = null;

	$(document).ready(function(){
		initDataTable();
	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
				searching: false,
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('private.announcements.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
					{ data: "id", title: 'Id' },
					{ title: 'Producto'},
					{ title: 'Vigencia'},
					{ data: 'visitas', title: 'Visitas'},
					{ data: 'producto.precioProveedor', title: 'Precio'},
					{ title: 'Estado'},
					{ data: 'fecha_inicio', title: 'Fecha Publicacion'},
					// ...
				],
				columnDefs: [
					{
						targets: 0,
						className: 'text-center',
						width: '30px',
					},{
						targets: 1,
						render: function(data, type, row){
							if(row.producto.marca != undefined){
										{{--var url = '{{ route("private.{$glc}.announcements.show", [0]) }}';--}}
								var url = '{{ route('public.announce.preview', [0]) }}';
								url = url.replace('0', row.id);
								return '<a href="'+url+'">'+ row.producto.marca.nombre + ' ' + row.producto.modelo.nombre +'</a>';
							}
						}
					},{
						targets: 2,
						render: function(data, type, row){

							return row.fecha_inicio + ' - ' + row.fecha_fin;
						}
					},{
						targets: 4,
						render: function(data, type, row){
							return data + ' <span style="text-transform:uppercase;">' + row.moneda + '</span>';
						}
					},{
						targets: 5,
						render: function(data, type, row){
							return estadosPub[row.estado];
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

</script>
@endpush