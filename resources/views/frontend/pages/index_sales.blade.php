<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	{{--<title>ExpoIndustri - @lang('messages.expand_your_business')</title>--}}
	<title>{{ $seo_title }}</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	@foreach($seo_metas as $meta)
	<meta name="{{ $meta->name }}" content="{{ $meta->content }}" />
	@endforeach
	<meta name="author" content="Marco Antonio Zeballos Sanjines <marcozs84@gmail.com>" />
	<meta name="developer" content="Marco Antonio Zeballos Sanjines <marcozs84@gmail.com>" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="/assets/css/one-page-parallax/app.min.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->

	<link href="/assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet" />
	{{--<link href="/assets/css/one-page-parallax/theme/yellow.min.css" rel="stylesheet" id="theme-css-link">--}}
	<link href="/assets/css/one-page-parallax/theme/blue.min.css" rel="stylesheet" id="theme-css-link">
	{{--<link href="/assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" id="theme-css-link">--}}
	{{--<link href="/assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet">--}}

	<link href="/assets/css/e-commerce/landing_sales.css?v=1" rel="stylesheet" />
	<link href="/assets/css/e-commerce/modalHandler.css?v=2" rel="stylesheet" />


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
	<link rel="stylesheet" href="/assets/css/carrousel.css">


	{{--<script src='https://www.google.com/recaptcha/api.js?render={{ $global_recaptcha_public_key }}'></script>--}}

	<style>
		.main_circles {
			position:absolute;
			top:100px;
			right:50px;
			max-width:600px;
		}
		.circle_1 {
			top:-40px;
			right:-100px;
			z-index:10;
		}
		.circle_2 {
			top:200px;
			right:70px;
			z-index:15;
		}
		.circle_3 {
			top:500px;
			right:-80px;
			z-index:11;
		}

		.content {
			overflow: hidden;
		}

		.navbar-sm .logotop_white {
			/*display:none;*/
			filter:invert(100);
		}
		/*.navbar-sm .logotop_black {*/
			/*display:block;*/
		/*}*/
		.header.navbar-transparent {
			background-color:#000000;
		}

		@media (max-width:750px) {
			.main_circles {
				display:none;
			}
		}


		.slide_holder {
			height:150px;
			overflow: hidden;
			position:relative;
		}
		.slide_holder h3 {
			/*display:none;*/
			position:absolute;
			font-size:3em;
			margin-top:0px;
			color:#9fce62 !important;
			font-weight:bold;
		}
		.content .content-title {
			font-weight:bold;
		}

		.about-author .quote {
			background-color: #348fe2 !important;
			color: #ffffff;
		}
		.about-author .quote:before {
			color:#000000 !important;
			border-top-color:#348fe2 !important;
			border-left-color:#348fe2 !important;

		}
	</style>
</head>
<body data-spy="scroll" data-target="#header" data-offset="51">
<!-- begin #page-container -->
<div id="page-container" class="fade">


	<!-- begin #header -->
	<div id="header" class="header navbar navbar-transparent navbar-fixed-top navbar-expand-lg">


		<!-- begin container -->
		<div class="container">
			<!-- begin navbar-brand -->

			<a href="/" style="font-size:35px;" class="logotop logotop_white">
				{{--<span style=" color: #fdda01;">Expo</span>Industri--}}
				{{--<small>@lang('messages.import_export')</small>--}}
				<img src="/assets/img/logo/logo_outline_white.png" alt="ExpoIndustri">
			</a>
			{{--<a href="/" style="font-size:35px;" class="logotop logotop_black">--}}
				{{--<span style=" color: #fdda01;">Expo</span>Industri--}}
				{{--<small>@lang('messages.import_export')</small>--}}
				{{--<img src="/assets/img/logo/logo_outline_black.png" alt="ExpoIndustri">--}}
			{{--</a>--}}

			{{--<a href="index.html" class="navbar-brand">--}}
				{{--<span class="brand-logo"></span>--}}
				{{--<span class="brand-text">--}}
						{{--<span class="text-primary">Color</span> Admin--}}
					{{--</span>--}}
			{{--</a>--}}
			<!-- end navbar-brand -->
			<!-- begin navbar-toggle -->
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- end navbar-toggle -->
			<!-- begin navbar-collapse -->
			<div class="collapse navbar-collapse" id="header-navbar">
				<ul class="nav navbar-nav navbar-right">

					<li class="nav-item"><a class="nav-link" href="{{ route('public.index_sales') }}" >Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="{{ route('public.index_nosotros') }}" >Nosotros</a></li>
					<li class="nav-item"><a class="nav-link" href="{{ route('public.index_servicios') }}" >Servicios</a></li>

					<li class="nav-item dropdown">
						<a class="nav-link @if(App::getLocale() != 'es') hide @endif" href="javascript:;" data-click="scroll-to-target" data-scroll-target="#home" data-toggle="dropdown">&nbsp;&nbsp;@lang('messages.spanish') <b class="caret"></b></a>
						<a class="nav-link @if(App::getLocale() != 'en') hide @endif" href="javascript:;" data-click="scroll-to-target" data-scroll-target="#home" data-toggle="dropdown">&nbsp;&nbsp;@lang('messages.english') <b class="caret"></b></a>
						<a class="nav-link @if(App::getLocale() != 'se') hide @endif" href="javascript:;" data-click="scroll-to-target" data-scroll-target="#home" data-toggle="dropdown">&nbsp;&nbsp;@lang('messages.swedish') <b class="caret"></b></a>
						<div class="dropdown-menu dropdown-menu-left animated fadeInDown">
							<a class="dropdown-item @if(App::getLocale() == 'es') hide @endif" onclick="changeLanguage('es')" href="javascript:;">&nbsp;&nbsp;@lang('messages.spanish')</a>
							<a class="dropdown-item @if(App::getLocale() == 'en') hide @endif" onclick="changeLanguage('en')" href="javascript:;">&nbsp;&nbsp;@lang('messages.english')</a>
							<a class="dropdown-item @if(App::getLocale() == 'se') hide @endif" onclick="changeLanguage('se')" href="javascript:;">&nbsp;&nbsp;@lang('messages.swedish')</a>
						</div>
					</li>




					{{--<li class="nav-item"><a class="nav-link" href="#milestone" data-click="scroll-to-target">EXPOINDUSTRI</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#partners" data-click="scroll-to-target">@lang('messages.partners')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#client" data-click="scroll-to-target">VENDER</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#pricing" data-click="scroll-to-target">@lang('messages.pricing')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#news" data-click="scroll-to-target">@lang('messages.news')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#categories" data-click="scroll-to-target">@lang('messages.categories')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#contact" data-click="scroll-to-target">@lang('messages.contact_us')</a></li>--}}
					{{--<li class="nav-item mobile_announce mobile_login"><a class="nav-link" href="{{ route('public.login') }}" >@lang('messages.log_in')</a></li>--}}


					{{--Comprar - NO--}}
					{{--Inicio--}}
					{{--Nosotros--}}
					{{--ExpoIndustri--}}
					{{--Socios--}}
					{{--Vender--}}
					{{--Categorias--}}
					{{--Contacto--}}
				</ul>
			</div>
			<!-- end navbar-collapse -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #header -->

	<!-- begin #home -->
	<div id="home" class="content has-bg home" style="background-color: #000;">
		<!-- begin content-bg -->
		<div class="content-bg bge2" style="/*background-image: url(/assets/img/bg/bg-home_opt.jpg);*/"
		     data-paroller="true"
		     data-paroller-factor="0.5"
		     data-paroller-factor-xs="0.25">
		</div>


		<div class="main_circles circle_white"></div>
		<img src="/assets/img/sales/container.png" class="main_circles circle_1" alt="">
		<img src="/assets/img/sales/navio.png" class="main_circles circle_2" alt="">
		<img src="/assets/img/sales/camion.png" class="main_circles circle_3" alt="">
		<!-- end content-bg -->
		<!-- begin container -->
		<div class="container home-content">
			{{--<h1>@lang('messages.welcome_to') ExpoIndustri</h1>--}}

			<h1 style="width:50%; text-align:left;" class="titleh1">HACEMOS MAGIA</h1>
			<h3 style="width:50%; text-align:left;" class="titleh3 ">Todo en uno, desde la búsqueda inteligente de su maquina, hasta la entrega en el destino que usted elija.</h3>
			<h3 style="width:50%; text-align:left; color:#9FCE62 !important; font-size:1.5rem;" class="titleh32 ">SOLO UN CONTACTO, UN SISTEMA, UNA FACTURA</h3>
			{{--<p>--}}
				{{--We have created a multi-purpose theme that take the form of One-Page or Multi-Page Version.<br />--}}
				{{--Use our <a href="#">theme panel</a> to select your favorite theme color.--}}
			{{--</p>--}}
			{{--<a href="#" class="btn btn-theme">Buscar maquinaria</a>--}}
			{{--<a href="#" class="btn btn-outline">Deseo Expandir mis ventas</a><br />--}}
			{{--<br />--}}
			{{--or <a href="#">subscribe</a> newsletter--}}

			{{--<a href="{{ route('public.buy_page') }}" target="_blank" class="btn btn-theme btn-primary" style="background-color:#D9C400; border-color:#D9C400; color:#000000;"><i class="fa fa-search"></i> @lang('messages.find_your_machine')</a>--}}
			{{--<a href="{{ route('public.buy_page') }}" target="_blank" class="btn btn-theme btn-primary" style="background-color:#D9C400; border-color:#D9C400; color:#000000;"><i class="fa fa-search"></i> @lang('messages.machine_storage')</a>--}}
			{{--<a href="#pricing" data-click="scroll-to-target" class="btn btn-theme btn-white"><i class="fa fa-chart-line"></i> @lang('messages.start_selling')</a><br />--}}
			<br /><br />
			{{--<span style="color:#ffffff;"><a href="javascript:;" onclick="displayWelcomeBanner()" style="color:#ffffff; font-weight:bold">@lang('messages.view_explained_video')</a></span>--}}
		</div>
		<!-- end container -->
	</div>
	<!-- end #home -->

	<!-- begin #about -->
	<div id="about" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container" data-animation="true" data-animation-type="fadeInUp">
			<h2 class="content-title">@lang('messages.about_us')</h2>
			<p class="content-desc">
				@lang('messages.our_services_slogan')
			</p>
			<!-- begin row -->
			<div class="row">
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-12">
					<!-- begin about -->
					<div class="about">
						<h3 class="mb-3">ExpoIndustri</h3>
						<p class="first-cap">
							@lang('messages.expo_description_sales')
						</p>
						{{--<p>--}}
							{{--In non libero at orci rutrum viverra at ac felis.--}}
							{{--Curabitur a efficitur libero, eu finibus quam.--}}
							{{--Pellentesque pretium ante vitae est molestie, ut faucibus tortor commodo.--}}
							{{--Donec gravida, eros ac pretium cursus, est erat dapibus quam,--}}
							{{--sit amet dapibus nisl magna sit amet orci.--}}
						{{--</p>--}}
					</div>
					<!-- end about -->
				</div>
				<!-- end col-4 -->
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-12">
					<h3 class="mb-3">@lang('messages.our_philosophy')</h3>
					<!-- begin about-author -->
					<div class="about-author">
						<div class="quote">
							<i class="fa fa-quote-left"></i>
							<h3 style="color:#ffffff !important; !important; font-size: 18px; padding: 4px;">@lang('messages.quote_joakim_sales')</h3>
							<i class="fa fa-quote-right"></i>
						</div>
						<div class="author">
							<div class="image">
								<img src="/assets/img/user/user-joakim_opt.jpg" alt="Sean Ngu" />
							</div>
							<div class="info">
								Joakim Byren
								<small>@lang('messages.managing_director')</small>
							</div>
						</div>
					</div>
					<!-- end about-author -->
				</div>
				<!-- end col-4 -->
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-12">
					<h3 class="mb-3">@lang('messages.our_experience')</h3>
					<!-- begin skills -->
					<div class="skills">
						<div class="skills-name">@lang('messages.euro_south_market')</div>
						<div class="progress mb-3">
							<div class="progress-bar progress-bar-striped progress-bar-animated bg-theme" style="width: 100%">
								<span class="progress-number">100 %</span>
							</div>
						</div>
						<div class="skills-name">@lang('messages.logistics')</div>
						<div class="progress mb-3">
							<div class="progress-bar progress-bar-striped progress-bar-animated bg-theme" style="width: 100%">
								<span class="progress-number">100 %</span>
							</div>
						</div>
						<div class="skills-name">@lang('messages.import_export_process')</div>
						<div class="progress mb-3">
							<div class="progress-bar progress-bar-striped progress-bar-animated bg-theme" style="width: 100%">
								<span class="progress-number">100 %</span>
							</div>
						</div>
						<div class="skills-name">@lang('messages.experienced_team')</div>
						<div class="progress mb-3">
							<div class="progress-bar progress-bar-striped progress-bar-animated bg-theme" style="width: 100%">
								<span class="progress-number">100 %</span>
							</div>
						</div>

						<div class="text-right" style="width:100%;">
							<a href="{{ route('public.index_nosotros') }}" style="color:#000000!important; font-size:1.6em; border-bottom:2px solid #000000; text-align:center; text-decoration:none;">Leer más...</a>
						</div>
					</div>
					<!-- end skills -->
				</div>
				<!-- end col-4 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #about -->

	<!-- begin #milestone -->
	<div id="milestone" class="content bg-black-darker has-bg" data-scrollview="true" >
		<!-- begin content-bg -->
		<div class="content-bg" style="background-image: url(/assets/img/bg/bg-milestone_opt.jpg)"
		     data-paroller="true"
		     data-paroller-factor="0.5"
		     data-paroller-factor-md="0.01"
		     data-paroller-factor-xs="0.01"></div>
		<!-- end content-bg -->
		<!-- begin container -->
		<div class="container">

			<h2 class="content-title">@lang('messages.our_services')</h2>
			{{--<p class="content-desc" data-animation="true" data-animation-type="fadeInRight">--}}
				{{--@lang('messages.our_price_slogan')--}}
			{{--</p>--}}
			<!-- begin row -->
			<div class="row">
				<div class="col-lg-6 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInLeft">
					<div>
						{{--<h3 style="color:#9fce62!important;">TODO EN UNO</h3>--}}
						<h3 style="color:#9fce62!important;">SOLO UN CONTACTO, UN SISTEMA, UNA FACTURA</h3>
						<p class="text-white" style="font-size: 1.5em;">
							Desde la búsqueda inteligente en Europa de la maquinaria que necesita, hasta la entrega de la misma en su empresa.
							<br><br>
							En Expoindustri somos parte de cada eslabón de la cadena y con nosotros puedes solicitar el servicio completo o parte de él. Siempre le ofrecemos servicios a su medida, con atención personalizada.
						</p>
						<p>
							<a href="{{ route('public.index_servicios') }}" style="color:#9fce62!important; font-size:2em; border-bottom:2px solid #9fce62; text-decoration:none;">Leer más</a>
						</p>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12">
					<div class="row">
						<!-- begin col-3 -->
						<div class="col-md-4 ">
							<div class="milestone"  data-animation="true" data-animation-type="fadeInRight">
								{{--<div class="number" data-animation="true" data-animation-type="number" data-final-number="9039">9,039</div>--}}
								<div class="number"><img src="/assets/img/sales/briefing_white.png" style="max-width:70%; padding-bottom:10px;" alt=""></div>
								<div class="title" style="color:#9fce62;">El <b>CLIENTE</b> nos explica su requerimiento</div>
							</div>
						</div>
						<!-- end col-3 -->
						<!-- begin col-3 -->
						<div class="col-md-4 ">
							<div class="milestone"  data-animation="true" data-animation-type="fadeInRight">
								{{--<div class="number" data-animation="true" data-animation-type="number" data-final-number="9039">9,039</div>--}}
								<div class="number">
									<div class="number"><img src="/assets/img/sales/providers_search_green.png" style="max-width:80%; padding-top:20px; padding-bottom:10px;" alt=""></div>
								</div>
								<div class="title" style="color:#fff;">Buscamos la máquina entre <b>MILES</b> de proveedores en Europa</div>
							</div>
						</div>
						<!-- end col-3 -->
						<!-- begin col-3 -->
						<div class="col-md-4 ">
							<div class="milestone"  data-animation="true" data-animation-type="fadeInRight">
								<div class="number"><img src="/assets/img/sales/proposals_white_many.png" style="max-width:80%; padding-top:20px; padding-bottom:10px;" alt=""></div>
								<div class="title" style="color:#9fce62;">Presentamos distintas alternativas</div>
							</div>
						</div>
						<!-- end col-3 -->
						<!-- begin col-3 -->
						<div class="col-md-4 ">
							<div class="milestone"  data-animation="true" data-animation-type="fadeInRight">
								<div class="number"><img src="/assets/img/sales/paperwork_green.png" style="max-width:70%; padding-top:20px; padding-bottom:10px;" alt=""></div>
								<div class="title" style="color:#fff;">Gestionamos todo el proceso administrativo</div>
							</div>
						</div>
						<!-- end col-3 -->
						<!-- begin col-3 -->
						<div class="col-md-4 ">
							<div class="milestone"  data-animation="true" data-animation-type="fadeInRight">
								{{--<div class="number" data-animation="true" data-animation-type="number" data-final-number="129">129</div>--}}
								<div class="number"><i class="fa fa-truck-pickup" style="margin-top:30px; margin-bottom:40px;"></i></div>
								<div class="title" style="color:#9fce62;">Nos encargamos del soporte logístico</div>
							</div>
						</div>
						<!-- end col-3 -->
						<!-- begin col-3 -->
						<div class="col-md-4 ">
							<div class="milestone"  data-animation="true" data-animation-type="fadeInRight">
								<div class="number"><img src="/assets/img/sales/delivery_green.png" style="max-width:70%; padding-top:20px; padding-bottom:10px;" alt=""></div>
								<div class="title" style="color:#fff;">Entregamos la maquinaria en su empresa</div>
							</div>
						</div>
						<!-- end col-3 -->
					</div>
				</div>

			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #milestone -->

	<!-- begin #team -->
	<div id="nosotros" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container">
			<h2 class="content-title">¿ Porqué trabajar con nosotros ?</h2>
			<p class="content-desc">
				@lang('messages.our_services_slogan')
			</p>
			{{--<p class="content-desc">--}}
				{{--Phasellus suscipit nisi hendrerit metus pharetra dignissim. Nullam nunc ante, viverra quis<br />--}}
				{{--ex non, porttitor iaculis nisi.--}}
			{{--</p>--}}
			<!-- begin row -->
			<div class="row">
				<!-- begin col-4 -->
				<div class="col-lg-6 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInLeft">
					<div style="width: 200%;
    height: 110%;
    background-color: #9fce62;
    position: absolute;
    z-index: -200;
    left: -150%;
    top: -10%;"></div>
					<p style=" margin-top:40px; margin-bottom:40px; font-size:1.2em;">
						Con más de 20 años de experiencia en el rubro de la venta de máquinas y soporte logistico desde Europa a Sudamérica, es Expoindustri un socio firme y confiable.
						<br><br>
						Ofrecemos un servicio personalizado, en el que la relación con nuestro cliente, entender y satisfacer sus necesidades, además de entregarle el producto sin que esto implique tiempo y costos extras, es nuestra razón de ser.
					</p>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInRight">
					<ul class="fa-ul">
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:-10px;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Expertos en el rubro
						</li>
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:-10px;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Soluciones a medida
						</li>
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:-10px;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Confiabilidad absoluta
						</li>
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:0;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Optimización costos de Transporte
						</li>
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:0;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Seguimiento digital a su pedido y al estado de su envío 24/7
						</li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInRight">
					<ul class="fa-ul">
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:-10px;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Optimización de tiempo
						</li>
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:0;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Presencia en Europa y Sudamérica
						</li>
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:0;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Equipo de Trabajo internacional
						</li>
						<li style="padding-top:15px; padding-left:20px; padding-bottom:10px;">
							<span class="fa-li" style="margin-top:0;"><i class="far fa-3x fa-check-square" style="color:#9fce62;"></i></span>
							Un solo responsable por todo el proceso
						</li>
					</ul>
				</div>
				<!-- end col-4 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #team -->

	<!-- begin #team -->
	<div id="partners" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container">
			<h2 class="content-title" data-animation="true" data-animation-type="fadeInDown">@lang('messages.partners')</h2>
			{{--<p class="content-desc">--}}
				{{--Phasellus suscipit nisi hendrerit metus pharetra dignissim. Nullam nunc ante, viverra quis<br />--}}
				{{--ex non, porttitor iaculis nisi.--}}
			{{--</p>--}}
			<!-- begin row -->
			<div class="row">
				<!-- begin col-4 -->
				<div class="col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInUp">
					<div class="brand-carousel section-padding owl-carousel">
						<div class="single-logo">
							<img src="/assets/img/socios_schenker.jpg" style="margin-top:30px;" alt="">
						</div>
						<div class="single-logo">
							<img src="/assets/img/socios_mevas.jpg" alt="">
						</div>
						<div class="single-logo">
							<img src="/assets/img/socios_zofri.jpg" alt="">
						</div>
						<div class="single-logo">
							<img src="/assets/img/socios_dsv.png" style="margin-top:70px;" alt="">
						</div>
						<div class="single-logo">
							<img src="/assets/img/socios_dachser.png" style="margin-top:50px;" alt="">
						</div>
						<div class="single-logo">
							<img src="/assets/img/socios_wmwag.png" style="margin-top:-20px;" alt="">
						</div>
						<div class="single-logo">
							<img src="/assets/img/socios_gnos.png" style="margin-top:70px; filter: invert(100);" alt="">
						</div>
						<div class="single-logo">
							<img src="/assets/img/socios_burbage.png" style="margin-top:0;" alt="">
						</div>
					</div>

				</div>
				<!-- end col-4 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #team -->




	<!-- beign #action-box -->
	<div id="action-box" class="content has-bg" data-scrollview="true">
		<!-- begin content-bg -->
		<div class="content-bg" style="background-image: url(/assets/img/e-commerce/cover/cover-crane_opt.jpg)"
		     data-paroller-factor="0.5"
		     data-paroller-factor-md="0.01"
		     data-paroller-factor-xs="0.01">
		</div>
		<!-- end content-bg -->
		<!-- begin container -->
		<div class="container newsletter_info" style="display:none;" >
			<div class="row action-box" style="padding: 10px; background: rgb(30 32 39 / 0.7);">
				<p class="newsletter_description col-md-12 m-t-5 m-b-0">
					Subscribed!
				</p>
			</div>
		</div>
		<div class="container newsletter_box" data-animation="true" data-animation-type="fadeInUp">
			<!-- begin row -->
			<div class="row action-box" style="padding: 10px; background: rgba(0,0,0, 0.7);">
				<!-- begin col-9 -->
				<div class="col-md-9 col-sm-12">
					<div class="icon-large text-primary">
						<i class="fa fa-envelope-open-text" style="color:#ffffff !important; padding-top: 7px; margin-top:3px;"></i>
					</div>
					<h3 style="font-weight: 600 !important; color: #000000 !important; padding-top: 10px;" data-animation="true" data-animation-type="fadeInUp"><input class="newsletter_email" type="text" id="newsletter_email" name="newsletter_email" placeholder="E-mail"></h3>
				</div>
				<!-- end col-9 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-12">
					<a href="javascript:;" onclick="subscribe('newsletter_email')" style="font-weight: 900; text-align:center; margin-top: 10px;" class="btn btn-primary btn-sm btn-theme btn-block btn-newsletter">@lang('messages.send')</a>
				</div>
				<!-- end col-3 -->

				<p class="newsletter_description col-md-12 m-t-5">
					@lang('messages.newsletter_description')
				</p>
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #action-box -->


@if(1 == 2)
	<!-- begin #pricing -->
	<div id="pricing" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container">
			<h2 class="content-title">@lang('messages.our_price')</h2>
			<p class="content-desc alert alert-success" style="font-weight:bold;">
				@lang('messages.our_price_slogan')
			</p>
			<!-- begin pricing-table -->
			<ul class="pricing-table pricing-col-3">
				<li data-animation="true" data-animation-type="fadeInUp">
					<div class="pricing-container">
						<h3>@lang('messages.plan_basic')</h3>
						<div class="price">
							<div class="price-figure">
								<span style="color:#fff;">@lang('messages.payment_per_announcement')</span>
								<br>
								<span class="price-number">360 kr</span>
								<span style="color:#fff;">@lang('messages.per_180days') (exkl. moms)</span>
							</div>
						</div>
						<ul class="features">
							<li>@lang('messages.single_announcement_post')</li>
							<li>@lang('messages.time_defined_user')</li>
							<li>@lang('messages.gallery_unlimited_images')</li>
							<li>@lang('messages.detailed_machine_description')</li>
							<li>@lang('messages.automatic_translations')</li>
{{--							<li>@lang('messages.experienced_salesman')</li>--}}
						</ul>
						<div class="footer">
							<a href="{{ route('public.publish') }}" class="btn btn-inverse btn-theme btn-block">@lang('messages.post_an_announcement')</a>
						</div>
					</div>
				</li>
				<li class="highlight" data-animation="true" data-animation-type="fadeInUp">
					<div class="pricing-container">
						<h3 style="color:#ffffff !important;">@lang('messages.plan_business')</h3>
						<div class="price">
							<div class="price-figure">
								<span class="price-number" style="color:#ffffff !important;">6.900 kr</span>
								<span class="price-tenure" style="color:#ffffff !important;">@lang('messages.per_year') (exkl. moms)</span>
							</div>
						</div>
						<ul class="features">
							<li>@lang('messages.thirty_announcements')</li>
							<li>@lang('messages.one_year_availability')</li>
							<li>@lang('messages.gallery_unlimited_images')</li>
							<li>@lang('messages.detailed_machine_description')</li>
							<li>@lang('messages.automatic_translations')</li>
							<li>
								<b>@lang('messages.customizable_online_store')</b><br>
								@lang('messages.link_to_your_site')<br>
								@lang('messages.post_logo_address')
							</li>
							<li>@lang('messages.experienced_salesman')</li>
						</ul>
						<div class="footer">
							<a href="{{ route("public.{$glc}.get_business") }}" class="btn btn-primary btn-theme btn-block">@lang('messages.buy_now')</a>
						</div>
					</div>
				</li>
				<li data-animation="true" data-animation-type="fadeInUp">
					<div class="pricing-container">
						<h3>Premium</h3>
						<div class="price">
							<div class="price-figure">
								<span class="price-number">12.900 kr</span>
								<span class="price-tenure">@lang('messages.per_year') (exkl. moms)</span>
							</div>
						</div>
						<ul class="features">
							<li>@lang('messages.unlimited_announcements')</li>
							<li>@lang('messages.one_year_availability')</li>
							<li>@lang('messages.gallery_unlimited_images')</li>
							<li>@lang('messages.detailed_machine_description')</li>
{{--							<li>@lang('messages.free_banner_bonus')</li>--}}
							<li>@lang('messages.automatic_translations')</li>
							<li>
								<b>@lang('messages.customizable_online_store')</b><br>
								@lang('messages.link_to_your_site')<br>
								@lang('messages.post_logo_address')
							</li>
							<li>@lang('messages.experienced_salesman')</li>
						</ul>
						<div class="footer">
							<a href="{{ route("public.{$glc}.get_plus") }}" class="btn btn-inverse btn-theme btn-block" style="color:#ffffff !important;">@lang('messages.buy_now')</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<!-- end container -->
	</div>
	<!-- end #pricing -->
@endif
{{--@if( 1 == 2 )--}}

	{{--<!-- begin #pricing -->--}}
		{{--<div id="news" class="content" data-scrollview="true">--}}
			{{--<!-- begin container -->--}}
			{{--<div class="container">--}}
				{{--<h2 class="content-title">@lang('messages.news')</h2>--}}
				{{--<p class="content-desc alert alert-success__" style="font-weight:bold;font-size: 20px; margin: 10px;">--}}
					{{--@lang('messages.business_opportunity_southamerica')--}}
				{{--</p>--}}
				{{--<!-- begin pricing-table -->--}}
				{{--<div style="background-color:#ffffff; border: 1px solid #c5ced4; padding: 10px;">--}}
					{{--<table>--}}
						{{--@for( $i = 0 ; $i < count($noticias) ; $i++)--}}
							{{--<tr class="news_link">--}}
								{{--<td style="width:100px; padding-right:10px;">--}}
									{{--<small class="pull-right">{{ $noticias[$i]->fechaPublicacion->format('d.m.Y') }}</small>--}}
								{{--</td>--}}
								{{--<td style="padding-left:10px;">--}}
									{{--<a href="javascript:;" style="padding:10px; border-left:1px solid #e5e5e5; display:block;" onclick="showNews({{ $noticias[$i]->id }})">{{ (trim($noticias[$i]['titulo_'.App::getLocale()]) == '') ? '- - -' : $noticias[$i]['titulo_'.App::getLocale()] }}</a>--}}
								{{--</td>--}}
							{{--</tr>--}}
							{{--@if($i < count($noticias) - 1)--}}
								{{--<tr>--}}
									{{--<td colspan="2" style="padding:5px;"><hr style="border-color:#e5e5e5; margin:0;"></td>--}}
								{{--</tr>--}}
							{{--@endif--}}
						{{--@endfor--}}
					{{--</table>--}}
				{{--</div>--}}
			{{--</div>--}}
			{{--<!-- end container -->--}}
		{{--</div>--}}
		{{--<!-- end #pricing -->--}}
{{--@endif--}}






@if(1 == 2)
	<!-- beign #action-box -->
	<div id="action-box" class="content has-bg" data-scrollview="true">
		<!-- begin content-bg -->
		<div class="content-bg" style="background-image: url(/assets/img/e-commerce/cover/cover-tractor1_opt.jpg)"
		     data-paroller-factor="0.5"
		     data-paroller-factor-md="0.01"
		     data-paroller-factor-xs="0.01">
		</div>
		<!-- end content-bg -->
		<!-- begin container -->
		<div class="container" data-animation="true" data-animation-type="fadeInUp">
			<!-- begin row -->
			<div class="row action-box" style="padding: 10px; background: rgba(255, 255, 255, 0.5);">
				<!-- begin col-9 -->
				<div class="col-md-9 col-sm-9">
					<div class="icon-large text-primary">
						<i class="fa fa-search-plus" style="color:#000000 !important; padding-top: 7px;"></i>
					</div>
					<h3 style="font-weight: 600 !important; color: #000000 !important; padding-top: 10px;">@lang('messages.just_single_announce')</h3>
					{{--<p>--}}
					{{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus faucibus magna eu lacinia eleifend.--}}
					{{--</p>--}}
				</div>
				<!-- end col-9 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-3">
					<a href="{{ route('public.publish') }}" target="_blank" style="font-weight: 900;" class="btn btn-primary btn-theme btn-block">@lang('messages.announce_here')</a>
				</div>
				<!-- end col-3 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #action-box -->


	<!-- beign #action-box -->
	<div id="action-box" class="content has-bg" data-scrollview="true">
		<!-- begin content-bg -->
		<div class="content-bg" style="background-image: url(/assets/img/e-commerce/cover/cover-tractor1_opt.jpg)"
		     data-paroller-factor="0.5"
		     data-paroller-factor-md="0.01"
		     data-paroller-factor-xs="0.01">
		</div>
		<!-- end content-bg -->
		<!-- begin container -->
		<div class="container" data-animation="true" data-animation-type="fadeInRight">
			<!-- begin row -->
			<div class="row action-box" style="padding: 10px; background: rgba(255, 255, 255, 0.5);">
				<!-- begin col-9 -->
				<div class="col-md-9 col-sm-9">
					<div class="icon-large text-primary">
						<i class="fa fa-search-plus" style="color:#000000 !important; padding-top: 7px;"></i>
					</div>
					<h3 style="font-weight: 600 !important; color: #000000 !important; padding-top: 10px;">@lang('messages.have_stock_machine')</h3>
					{{--<p>--}}
					{{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus faucibus magna eu lacinia eleifend.--}}
					{{--</p>--}}
				</div>
				<!-- end col-9 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-3">
					<a href="#contact" style="font-weight: 900;" class="btn btn-primary btn-theme btn-block">@lang('messages.contact_us')</a>
				</div>
				<!-- end col-3 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #action-box -->
@endif

	<!-- begin #quote -->
	{{--<div id="quote" class="content bg-black-darker has-bg" data-scrollview="true" >--}}
		{{--<!-- begin content-bg -->--}}
		{{--<div class="content-bg" style="background-image: url(../assets/img/bg/bg-quote_opt.jpg)"--}}
		     {{--data-paroller-factor="0.5"--}}
		     {{--data-paroller-factor-md="0.01"--}}
		     {{--data-paroller-factor-xs="0.01">--}}
		{{--</div>--}}
		{{--<!-- end content-bg -->--}}
		{{--<!-- begin container -->--}}
		{{--<div class="container" data-animation="true" data-animation-type="fadeInLeft">--}}
			{{--<!-- begin row -->--}}
			{{--<div class="row">--}}
				{{--<!-- begin col-12 -->--}}
				{{--<div class="col-md-12 quote">--}}
					{{--<i class="fa fa-quote-left"></i>--}}
					{{--Passion leads to design, design leads to performance, <br />--}}
					{{--performance leads to <span class="text-primary">success</span>!--}}

					{{--@lang('messages.quote_joakim')--}}
					{{--<i class="fa fa-quote-right"></i>--}}
					{{--<small>Joakim Byren, @lang('messages.managing_director')</small>--}}
				{{--</div>--}}
				{{--<!-- end col-12 -->--}}
			{{--</div>--}}
			{{--<!-- end row -->--}}
		{{--</div>--}}
		{{--<!-- end container -->--}}
	{{--</div>--}}
	<!-- end #quote -->



	<!-- begin #work -->
	<div id="categories" class="content hide" data-scrollview="true">
		<!-- begin container -->
		<div class="container" data-animation="true" data-animation-type="fadeInDown">

		</div>

		<!-- end container -->

		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN search-container -->
			<div class="search-container">
				<!-- BEGIN search-content -->
				<div class="search-content">

					<h2 class="content-title" data-animation="true" data-animation-type="fadeInUp">@lang('messages.our_categories')</h2>
					{{--<p class="content-desc">--}}
						{{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur eros dolor,<br />--}}
						{{--sed bibendum turpis luctus eget--}}
					{{--</p>--}}
					<!-- begin row -->

					<!-- BEGIN row -->
					<div class="row row-space-10" data-animation="true" data-animation-type="fadeInUp">
						<!-- BEGIN col-3 -->
						<div class="col-md-4 col-sm-6 col-xs-6 col-6">
							<!-- BEGIN promotion -->

							<div class="promotion bg-white">
								<a href="{{ route('public.transport') }}" target="_blank">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="left:0; top:0;">
										<img src="/assets/img/e-commerce/cat_transporte_7_400_opt.jpg" class="img_transport" alt="" />
									</div>
									<div class="promotion-caption promotion-caption-inverse text-right">
										<h4 class="promotion-title">@lang('messages.transportation')</h4>
									</div>
								</a>
							</div>

						</div>
						<div class="col-md-4 col-sm-6 col-xs-6 col-6">

							<div class="promotion bg-white">
								<a href="{{ route('public.lantbruksmaskiner') }}" target="_blank">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="top:0; left:0;">
										<img src="/assets/img/e-commerce/cat_agricultura1_400_opt.jpg" class="img_agricultura" alt="" />
									</div>
									<div class="promotion-caption text-center">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.agriculture')</h4>
									</div>
								</a>
							</div>

							<!-- END promotion -->
						</div>
						<!-- END col-3 -->
						<!-- BEGIN col-3 -->
						<div class="col-md-4 col-sm-6 col-xs-6 col-6">
							<div class="promotion bg-white">
								<a href="{{ route('public.entreprenadmaskiner') }}" target="_blank">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="left:0; top:0;">
										<img src="/assets/img/e-commerce/cat_equipment_2_400_opt.jpg" class="img_equipment" alt="" />
									</div>
									<div class="promotion-caption text-center">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.equipments')</h4>
									</div>
								</a>
							</div>

						</div>
						<div class="col-md-4 col-sm-6 col-xs-6 col-6">
							<div class="promotion bg-white">
								<a href="{{ route('public.truckar') }}" target="_blank">
									<div class="promotion-image promotion-image-overflow-right  promotion-image-overflow-bottom" style="left:0; top:0;">
										<img src="/assets/img/e-commerce/cat_almacen_1_400_opt.jpg" class="img_montacargas" alt="" />
									</div>
									<div class="promotion-caption text-center">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.forklifts')</h4>
									</div>

								</a>
							</div>
						</div>
						<!-- END col-3 -->

						<!-- BEGIN col-3 -->
						<div class="col-md-4 col-sm-6 col-xs-6 col-6">
							<!-- BEGIN promotion -->
							<div class="promotion bg-white">
								<a href="{{ route('public.industri') }}" target="_blank">
									<div class="promotion-image promotion-image-overflow-right promotion-image-overflow-bottom" style="left:0; top:0;">
										<img src="/assets/img/e-commerce/cat_industrial1_400_opt.jpg" class="img_industrial" alt="" />
									</div>
									<div class="promotion-caption promotion-caption-inverse">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.industrial')</h4>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-6 col-6">
							<div class="promotion bg-white">
								<a href="{{ route('public.ovriga') }}" target="_blank">
									<div class="promotion-image promotion-image-overflow-right  promotion-image-overflow-bottom" style="left:0; top:0;">
										<img src="/assets/img/e-commerce/cat_otros_400_opt.jpg" class="img_otros" alt="" />
									</div>
									<div class="promotion-caption promotion-caption-inverse">
										<h4 class="promotion-title promotion-caption-inverse text-right">@lang('messages.others')</h4>
									</div>
								</a>
							</div>
							<!-- END promotion -->
						</div>
						<!-- END col-3 -->

					</div>
					<!-- END row -->

				</div>
				<!-- END search-content -->
				<!-- BEGIN search-sidebar -->
			<!-- END search-sidebar -->
			</div>
			<!-- END search-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- end #work -->


	<!-- begin #contact -->
	<div id="contact" class="content bg-silver-lighter" data-scrollview="true">
		<!-- begin container -->
		<div class="container">
			<h2 class="content-title" data-animation="true" data-animation-type="fadeInUp">@lang('messages.contact_us')</h2>

			{{--<p class="content-desc">--}}
				{{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur eros dolor,<br />--}}
				{{--sed bibendum turpis luctus eget--}}
			{{--</p>--}}
			<!-- begin row -->
			<div class="row">
				<!-- begin col-6 -->
				<div class="col-md-6" data-animation="true" data-animation-type="fadeInLeft">
					<div class="slide_holder" style="">
						{{--<h3>@lang('messages.contact_doubts')</h3>--}}
						<h3 class="title_1">¿Quiere hablar sobre su proyecto?</h3>
						<h3 class="title_2">¿En que le podemos ayudar?</h3>
						<h3 class="title_3">¡Cuéntenos que necesita!</h3>
					</div>
					<h4>!CONVERSEMOS¡</h4>
					<p>@lang('messages.contact_doubts_body')</p>
					{{--<p>--}}
						{{--Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu pulvinar risus, vitae facilisis libero dolor a purus.--}}
					{{--</p>--}}
					{{--<p>--}}
						{{--<strong>SeanTheme Studio, Inc</strong><br />--}}
						{{--795 Folsom Ave, Suite 600<br />--}}
						{{--San Francisco, CA 94107<br />--}}
						{{--P: (123) 456-7890<br />--}}
					{{--</p>--}}
					{{--<p>--}}
						{{--<span class="phone">+11 (0) 123 456 78</span><br />--}}
						{{--<a href="mailto:hello@emailaddress.com" class="text-primary">seanthemes@support.com</a>--}}
					{{--</p>--}}

					<div class="row">
						<div class="col-md-6">
							
{{--							<h4>@lang('messages.contact_details')</h4>--}}
							{{--<p>@lang('messages.contact_details_body')</p>--}}
							<p class="m-b-15">
								<b>CASA MATRIZ</b><br>
								<b>EXPOINDUSTRI SWEDEN AB | N° Org.: 559174-5194</b><br>
								<b>VAT: SE559174519401</b><br>
								Jönköpingsvägen 27, 56161, Tenhult – Suecia<br>
								Telf: +46 72 393 3700<br>
								info@expoindustri.com<br>


{{--								@lang('messages.sweden')--}}
								{{--							<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +46 (0) 723 933 700<br />--}}
								{{--									<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />--}}
							</p>

							<div class="hide">
								<div><i class="flag-icon flag-icon-bo" style="font-style: 20px;"></i> <b>Bolivia</b></div>
								<p class="m-b-15">
									795 Folsom Ave, Suite 600<br />
									San Francisco, CA 94107<br />
									P: (123) 456-7890<br />
								</p>
							</div>

							<div class="hide">
								<div><i class="flag-icon flag-icon-cl" style="font-size:20px;"></i> <b>Chile</b></div>
								<p class="m-b-15">
									Oficina mapocho 48D<br />
									Zofri - Iquique<br />
									{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
									<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />
								</p>

								{{--<div><i class="flag-icon flag-icon-pe" style="font-style: 20px;"></i> <b>Perú</b></div>--}}
								{{--<p class="m-b-15">--}}
								{{--795 Folsom Ave, Suite 600<br />--}}
								{{--San Francisco, CA 94107<br />--}}
								{{--P: (123) 456-7890<br />--}}
								{{--</p>--}}
							</div>
						</div>
						<div class="col-md-6">

{{--							<h4>@lang('messages.find_here')</h4>--}}

							<div>
								{{--<div><i class="fas flag-icon flag-icon-se fa-flag-se" style="font-style: 20px;"></i> <b>@lang('messages.sweden')</b></div>--}}
								<p class="m-b-15">
									<b>DISTRIBUCION Y VENTAS</b>
									<b>EXPOINDUSTRI CHILE LTDA</b><br>
									<b>RUT: 76.388.887-8</b><br>
									Oficina Mapocho, 48D, Zofri<br>
									Iquique – Chile<br>
									Telf: +56 (57) 2473844<br>
									info@expoindustri.com<br>

									{{--<a href="https://api.whatsapp.com/send?phone=56572473844"><img style=" width:40px;" src="/assets/img/whatsapp_icon.png" alt=""> Contactenos por Whatsapp</a>--}}

									{{--								@lang('messages.sweden')--}}
									{{--							<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +46 (0) 723 933 700<br />--}}
									{{--									<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />--}}
								</p>
							</div>
							
							{{--<h4>@lang('messages.hours_available')</h4>--}}
							{{--<p>@lang('messages.hours_list')</p>--}}
						</div>
						<div class="col-md-12">

							<p class="text-center" style="margin-top:30px;">
								<a href="https://api.whatsapp.com/send?phone=46723933700" class="btn btn-warning" style="font-weight:700; width:50%; margin:auto;"><img style=" width:40px;" src="/assets/img/whatsapp_icon.png" alt=""> &nbsp;&nbsp;&nbsp;Whatsapp</a>
							</p>
						</div>
					</div>


				</div>
				<!-- end col-6 -->
				<!-- begin col-6 -->
				<div class="col-md-6 form-col" data-animation="true" data-animation-type="fadeInRight">

					@if( isset($mail_enviado) && $mail_enviado == true )
						<div class="alert alert-success m-b-0">
							<span class="close" data-dismiss="alert">×</span>
							<h4>@lang('messages.contact_form_sent')!</h4>
							<p>@lang('messages.contact_form_sent_info')</p>
						</div>
					@else

						<div class="text-center" style="padding-left:40px; padding-right:40px;">
							@lang('messages.wanna_contact_us')
							<br><br>
						</div>

					<form class="form-horizontal" name="contact_us_form" action="{{ route('public.contactPost') }}#contact" method="post">
						{{ csrf_field() }}

						{{--{!! Form::hidden('g-recaptcha-response', null, [--}}
							{{--'id' => 'g-recaptcha-response',--}}
						{{--]) !!}--}}

						{!! Form::hidden('referrer', 'landing_sales', [
							'id' => 'referrer',
						]) !!}

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3 text-md-right">@lang('messages.firstname')
								@if ($errors->has('nombres'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-9">
								{!! Form::text('nombres', null, ['id' => 'nombres', 'class' => 'form-control', 'placeholder' => __('messages.firstname')]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3 text-md-right">@lang('messages.lastname')
								@if ($errors->has('apellidos'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-9">
								{!! Form::text('apellidos', null, ['id' => 'apellidos', 'class' => 'form-control', 'placeholder' => __('messages.firstname')]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3 text-md-right">E-mail
								@if ($errors->has('email'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-9">
								{!! Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => __('messages.e_mail')]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3 text-md-right">@lang('messages.country')
								@if ($errors->has('pais'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-9">
								{!! Form::select('pais', $global_paises, $global_localization, ['class' => 'form-control', 'placeholder' => __('messages.select_country')]); !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3 text-md-right">@lang('messages.subject')
								@if ($errors->has('asunto'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-9">
								{!! Form::text('asunto', null, ['id' => 'asunto', 'class' => 'form-control', 'placeholder' => __('messages.subject')]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3 text-md-right">@lang('messages.message')
								@if ($errors->has('mensaje'))
									<span class="text-danger">*</span>
								@endif
							</label>
							<div class="col-md-9">
								{!! Form::textarea('mensaje', '', [
									'id' => 'mensaje',
									'placeholder' => __('messages.message'),
									'class' => 'form-control',
									'rows' => 10
								]) !!}
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3 text-md-right"></label>
							<div class="col-md-9 text-left">
								<button type="submit" class="btn btn-warning btn-theme btn-block" id="btnContactoEnviar" ><i class="fa fa-paper-plane"></i> @lang('messages.send_message')</button>

								<p style="text-align:right; font-style: italic; " >
									@lang('messages.contact_disclaimer', ['link' => '<a href="'.route('public.dataProtectionPolicy').'" target="_blank" style="font-weight:bold;">'.__('messages.privacy_politics').'</a>'])
								</p>
							</div>
						</div>
					</form>
					@endif
				</div>
				<!-- end col-6 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #contact -->

	<!-- begin #footer -->
	<div id="footer" class="footer">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-3 -->
				{{--<div class="col-md-3" hidden>--}}
					{{--<h4 class="footer-header">@lang('messages.about_us')</h4>--}}
					{{--<p>--}}
						{{--@lang('messages.about_us_footer')--}}
					{{--</p>--}}

				{{--</div>--}}
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.site_map')</h4>
					<ul class="fa-ul">
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.nosotros") }}">@lang('messages.about_us')</a></li>--}}
{{--						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_anunciantes") }}">@lang('messages.information_for_announcers')</a></li>--}}
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_compradores") }}">@lang('messages.information_for_buyers')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.faq') }}">@lang('messages.frequent_questions')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.servicios") }}">@lang('messages.services')</a></li>
						{{--							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.toc') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.termsAndConditions') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.dataProtectionPolicy') }}">@lang('messages.privacy_policy')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.contact") }}">@lang('messages.contact_us')</a></li>--}}
					</ul>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.terms')</h4>
					<ul class="fa-ul">
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.nosotros") }}">@lang('messages.about_us')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_anunciantes") }}">@lang('messages.information_for_announcers')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_compradores") }}">@lang('messages.information_for_buyers')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.faq') }}">@lang('messages.frequent_questions')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.servicios") }}">@lang('messages.services')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.toc') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.termsAndConditions') }}">@lang('messages.legal_terms_conditions')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.dataProtectionPolicy') }}">@lang('messages.privacy_policy')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.index_sales") }}#contact">@lang('messages.contact_us')</a></li>
					</ul>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.central_office')</h4>
					<address>
						<strong><i class="flag-icon flag-icon-se" style="font-style: 20px;"></i> @lang('messages.sweden')</strong><br />
						Jönköpingsvägen 27 <br>
						561 61 Tenhult <br>
						{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +46 (0) 723 933 700<br />--}}
						<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />

						{{--<br />--}}
						{{--<strong>Bolivia</strong><br />--}}
						{{--1355 Market Street, Suite 900<br />--}}
						{{--San Francisco, CA 94103<br />--}}
						{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
						{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

						{{--<br />--}}
						{{--<strong><i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> Chile</strong><br />--}}
						{{--Oficina mapocho 48D<br />--}}
						{{--Zofri - Iquique<br />--}}
						{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
						{{--<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />--}}

						{{--<br />--}}
						{{--<strong>Peru</strong><br />--}}
						{{--1355 Market Street, Suite 900<br />--}}
						{{--San Francisco, CA 94103<br />--}}
						{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
						{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

						<br>
						<b><a href="{{ route("public.{$glc}.contact") }}">@lang('messages.our_offices_southamerica')</a></b>

					</address>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.distrib_office')</h4>
					<address>
						<strong><i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> Chile</strong><br />
						Oficina mapocho 48D<br />
						Zofri - Iquique<br />
						{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
						<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />

						{{--<br />--}}
						{{--<strong>Peru</strong><br />--}}
						{{--1355 Market Street, Suite 900<br />--}}
						{{--San Francisco, CA 94103<br />--}}
						{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
						{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

					</address>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3 hide">
					<h4 class="footer-header">Oficinas en Europa</h4>
					<address>
						<strong>Twitter, Inc.</strong><br />
						1355 Market Street, Suite 900<br />
						San Francisco, CA 94103<br /><br />
						<br>
						<strong>Av. Las palmas, Inc.</strong><br />
						1355 Market Street, Suite 900<br />
						San Francisco, CA 94103<br /><br />

						<abbr title="Phone">Phone:</abbr> (123) 456-7890<br />
						<abbr title="Fax">Fax:</abbr> (123) 456-7891<br />
						<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />
						<abbr title="Skype">Skype:</abbr> <a href="skype:myshop">myshop</a>
					</address>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				{{--<div class="col-md-3 p-0 text-center" style="/* padding:20px !important; background-color:#ffffff; */" hidden>--}}
					{{--<style>--}}
					{{--#loc_map {--}}
					{{--height: 270px;--}}
					{{--width: 100%;--}}
					{{--}--}}
					{{--</style>--}}
					{{--<div id="loc_map"></div>--}}
					{{--@if($global_isProvider == true)--}}
					{{--@endif--}}


					{{--@if(App::getLocale() == 'se')--}}
					{{--<img src="/assets/img/e-commerce/FooterLogoWhite_Export.png" alt="" style="width:100%; ">--}}
					{{--@else--}}
					{{--<img src="/assets/img/e-commerce/FooterLogoWhite_Import.png" alt="" style="width:100%; ">--}}
					{{--@endif--}}

					{{--<img src="/assets/img/e-commerce/uc_logo_{{ App::getLocale() }}.png" alt="" style="width:90%; ">--}}

					{{--<img src="/assets/img/e-commerce/SmallLogo.png" alt="Map Route Export" style="width:100%;">--}}
					{{--<img src="/assets/img/e-commerce/256_iso_fav.png" alt="Map Route Export" style="width:100%;">--}}
					{{--<img src="/assets/img/e-commerce/mapa_ruta_export.png" alt="Map Route Export" style="width:100%;">--}}
					{{--<img src="/assets/img/e-commerce/sello_footer1.png" alt="Map Route Export" style="max-height:230px; ">--}}

					{{--<img src="/assets/img/logo/logo_exind2_{{ session('lang', 'en') }}.png" alt="Map Route Export" style="width:100%; background-color:#ffffff; pading:10px;">--}}

				{{--</div>--}}
				<!-- END col-3 -->
			</div>
			<!-- END row -->
		</div>
		<!-- END container -->
	</div>
	<!-- end #footer -->

	<!-- begin theme-panel -->
	{{--<div class="theme-panel">--}}
		{{--<a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>--}}
		{{--<div class="theme-panel-content">--}}
			{{--<ul class="theme-list clearfix">--}}
				{{--<li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="../assets/css/one-page-parallax/theme/red.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-pink" data-theme="pink" data-theme-file="../assets/css/one-page-parallax/theme/pink.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Pink" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="../assets/css/one-page-parallax/theme/orange.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-yellow" data-theme="yellow" data-theme-file="../assets/css/one-page-parallax/theme/yellow.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Yellow" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-lime" data-theme="lime" data-theme-file="../assets/css/one-page-parallax/theme/lime.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Lime" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-green" data-theme="green" data-theme-file="../assets/css/one-page-parallax/theme/green.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Green" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li class="active"><a href="javascript:;" class="bg-teal" data-theme-file="../assets/css/one-page-parallax/theme/teal.min.css" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-aqua" data-theme="aqua" data-theme-file="../assets/css/one-page-parallax/theme/aqua.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Aqua" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="../assets/css/one-page-parallax/theme/blue.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="../assets/css/one-page-parallax/theme/purple.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-indigo" data-theme="indigo" data-theme-file="../assets/css/one-page-parallax/theme/indigo.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Indigo" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="../assets/css/one-page-parallax/theme/black.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black" data-original-title="" title="">&nbsp;</a></li>--}}
			{{--</ul>--}}
		{{--</div>--}}
	{{--</div>--}}
	<!-- end theme-panel -->




	<div class="modal fade" id="modal-newsletter">
		<div class="modal-dialog video-dialog" style="">
			<div class="modal-content" style="-webkit-box-shadow: none !important;  -moz-box-shadow: none !important; box-shadow: none !important; ">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">Newsletter</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body" style="background-color:#000000; padding:0;">

					<!-- beign #action-box -->
					<div id="action-box" class="content has-bg" data-scrollview="true" style="border: 5px solid #ffffff;">
						<!-- begin content-bg -->
						<div class="content-bg" style="background-image: url(/assets/img/e-commerce/cover/cover-crane_opt.jpg)"
						     data-paroller-factor="0.5"
						     data-paroller-factor-md="0.01"
						     data-paroller-factor-xs="0.01">
						</div>
						<!-- end content-bg -->

						<div class="container newsletter_info" style="display:none;" >
							<div class="row action-box" style="padding: 10px; background: rgb(30 32 39 / 0.7);">
								<p class="newsletter_description col-md-12 m-t-5 m-b-0">
									Subscribed!
								</p>
							</div>
						</div>
						<!-- begin container -->
						<div class="container newsletter_box" data-animation="true" data-animation-type="fadeInRight">
							<!-- begin row -->
							<div class="row action-box" style="padding: 10px; background: rgb(30 32 39 / 0.8);">
								<!-- begin col-9 -->
								<div class="col-md-9 col-sm-12">
									<div class="icon-large text-primary">
										<i class="fa fa-envelope-open-text" style="color:#ffffff !important; padding-top: 7px; margin-top:3px;"></i>
									</div>
									<h3 style="font-weight: 600 !important; color: #000000 !important; padding-top: 10px;"><input class="newsletter_email" type="text" id="newsletter_email_modal" name="newsletter_email_modal" placeholder="E-mail"></h3>
								</div>
								<!-- end col-9 -->
								<!-- begin col-3 -->
								<div class="col-md-3 col-sm-12">
									<a href="javascript:;" onclick="subscribe('newsletter_email_modal')" style="font-weight: 900; text-align:center; margin-top: 10px;" class="btn btn-primary btn-sm btn-theme btn-block btn-newsletter">@lang('messages.send')</a>
								</div>
								<!-- end col-3 -->

								<p class="newsletter_description col-md-12 m-t-5" style="font-weight:600;">
									@lang('messages.newsletter_description')
								</p>
							</div>
							<!-- end row -->
						</div>
						<!-- end container -->
					</div>
					<!-- end #action-box -->



				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modal-welcome-banner">
		<div class="modal-dialog video-dialog" style="">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('messages.welcome')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body" style="background-color:#000000; padding:0;">
					<video id="welcome_video" class="video-js vjs-default-skin"
					       controls preload="auto" width="100%"
							{{--@if( App::getLocale() == 'se' )--}}
								{{--poster="/assets/banner_se.jpeg"--}}
							{{--@elseif( App::getLocale() == 'en' )--}}
								{{--poster="/assets/banner_en_2.jpeg"--}}
							{{--@else--}}
								{{--@if($global_isProvider == true)--}}
									{{--poster="/assets/banner_es_2_vende.jpeg"--}}
								{{--@else--}}
									{{--poster="/assets/banner_es_compra.jpeg"--}}
								{{--@endif--}}
							{{--@endif--}}
					       data-setup='{"example_option":true}'>

						{{--<source src="http://video-js.zencoder.com/oceans-clip.webm" type="video/webm" />--}}
						{{--<source src="http://video-js.zencoder.com/oceans-clip.ogv" type="video/ogg" />--}}
						<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
					</video>
				</div>
			</div>
		</div>
	</div>



	<div class="modal fade" id="modal-noticias">
		<div class="modal-dialog modal-60">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('messages.news')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					Loading...
				</div>
				<div class="modal-footer">

					<a href="mailto:info@expoindustri.com" target="_blank" class="btn btn-primary"><i class="fa fa-envelope"></i> @lang('messages.send_offer')</a>
					<a href="javascript:;" onclick="move_to_form()" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-paper-plane"></i> @lang('messages.contact_us2')</a>
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade loginRegion" id="modalLoginRegister">
		<div class="modal-dialog" >
			<div class="modal-content" style="margin-top:50px;">
				<div class="modal-body">
					<div class="login-form hide">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.log_in')</h4>
						</div>
						<form id="formLoginTop" action="{{ route('public.login') }}" method="post" class="text-center">
							{{ csrf_field() }}
							<input type="text" name="email" id="loginTopUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
							<input type="password" name="password" id="loginTopPassword" class="login-input" placeholder="@lang('messages.password')" >

							<div id="loginTopErrors"></div>

							<input type="checkbox" name="remember" id="frmRememberTop" style="margin-top:20px;" /> @lang('messages.remember_me')
							<br>
							{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.log_in')">--}}
							<button type="submit" id="regTopBtnEntrar" form="formLoginTop" value="@lang('messages.log_in')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.log_in')</button>
							{{--<a href="javascript:;" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" onclick="tryLogin()">Entrar</a>--}}
							{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Entrar">--}}
							<br>
							<a href="javascript:;" class="" onclick="activateLoginRegister('forgot')" style="font-size:12px; /*color:#333333;*/">@lang('messages.forgot_password')</a>

						</form>
						<hr />
						<p class="text-center">
							<a href="javascript:;" class="" onclick="activateLoginRegister('register')" style="font-size:12px; /*color:#333333;*/">@lang('messages.create_account')</a>
						</p>
					</div>
					<div class="register-form hide">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.register')</h4>
						</div>
						<form id="formRegisterTop"  action="{{ route('public.register') }}" method="post" class="text-center">
							<input type="text" name="email" id="regTopUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
							<div id="regTopError_email"></div>

							<input type="text" name="apellidos" id="regTopApellidos" class="login-input" placeholder="@lang('messages.lastname')" >
							<div id="regTopError_apellidos"></div>

							<input type="text" name="nombres" id="regTopNombres" class="login-input" placeholder="@lang('messages.firstname')" >
							<div id="regTopError_nombres"></div>

							<input type="text" name="empresa" id="regTopEmpresa" class="login-input" placeholder="@lang('messages.company')" >
							<div id="regTopError_empresa"></div>

							<input type="text" name="nit" id="regTopNit" class="login-input" placeholder="@lang('messages.company_nit')" >
							<div id="regTopError_nit"></div>

							{{--<select name="pais" id="regTopPais" class="login-input">--}}
							{{--<option value="">@lang('messages.select_country')</option>--}}
							{{--</select>--}}
							{!! Form::select('pais', $global_paises, $global_country_abr, [
										'id' => 'regTopPais',
										'class' => 'login-input',
										'placeholder' => __('messages.select_country')
							]); !!}
							<div id="regTopError_pais"></div>

							<input type="text" name="telefono" id="regTopTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
							<div id="regTopError_telefono"></div>

							<input type="password" name="password" id="regTopPassword" class="login-input" placeholder="@lang('messages.password')" >
							<div id="regTopError_password"></div>

							<input type="password" name="password_confirmation" id="regTopPassword2" class="login-input" placeholder="@lang('messages.password_confirmation')" >
							<div id="regTopError_password_confirm"></div>

							<br>
							{!! Form::checkbox('legalterms', 'legalterms', null, [
								'id' => 'legalterms',
								'style' => 'font-size:15px; margin:0; vertical-align:middle;'
							]) !!}
							<label class="control-label " for="legalterms">
								@lang('messages.accept_terms', ['link' => '<a href="javascript:;" onclick="modalTerms()" style="font-weight:bold;">'.__('messages.legal_terms').'</a>'])
							</label>

							{{--<input type="submit" id="regTopBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
							<button type="submit" id="regTopBtnEnviar" form="formRegisterTop" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
							{{--<a href="javascript:;" id="regTopBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}
						</form>
						<hr />
						<p class="text-center">
							<a href="javascript:;" class="" onclick="activateLoginRegister('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
						</p>
					</div>
					<div class="forgot-form hide">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.forgot_password')</h4>
						</div>
						<form id="formForgotTop"  action="{{ route('public.forgotPassword') }}" method="post" class="text-center">
							<p>
								@lang('messages.forgot_text')
							</p>
							<input type="text" name="forgotEmail" id="regTopForgotEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
							<div id="regTopError_forgotEmail"></div>

							{{--<input type="submit" id="regTopBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
							<button type="submit" id="regTopBtnForgot" form="formForgotTop" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
							{{--<a href="javascript:;" id="regTopBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}
						</form>
						<hr />
						<p class="text-center">
							<a href="javascript:;" class="" onclick="activateLoginRegister('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
						</p>
					</div>
				</div>
			</div>
		</div>




</div>
<!-- end #page-container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="/assets/js/one-page-parallax/app.min.js"></script>

<script src="/assets/js/e-commerce/modalHandler.js?r={{ rand(1,111) }}"></script>
<script src="/assets/js/e-commerce/layout.js?r={{ rand(1,111) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/gsap.min.js" integrity="sha512-1dalHDkG9EtcOmCnoCjiwQ/HEB5SDNqw8d4G2MKoNwjiwMNeBAkudsBCmSlMnXdsH8Bm0mOd3tl/6nL5y0bMaQ==" crossorigin="anonymous"></script>

	{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

	{{--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
<!-- ================== END BASE JS ================== -->

<script>

	var totalHeight = $(window).height();
	var circleHeight = totalHeight / 2;
	$('.main_circles').css('max-height', circleHeight + "px");
	$('.circle_1').css('top', ( ( circleHeight / 2 ) * (1-1)) + "px");
	$('.circle_2').css('top', ( ( circleHeight / 2 ) * (2-1)) + "px");
	$('.circle_3').css('top', ( ( circleHeight / 2 ) * (3-1)) + "px");
	$('.circle_white').css({
		'top': "0",
		'right': "0",
		'width': (circleHeight / 3) + "px",
		'height': totalHeight + "px",
		'max-height': totalHeight + "px",
		'background-color': "#ffffff"
	});

//	$('#home').height($(window).height());
	$(window).on('resize', function() {
//		$('#home').height($(window).height());

		totalHeight = $(window).height();
		circleHeight = totalHeight / 2;
		$('.main_circles').css('max-height', circleHeight + "px");
		$('.circle_1').css('top', ( ( circleHeight / 2 ) * (1-1)) + "px");
		$('.circle_2').css('top', ( ( circleHeight / 2 ) * (2-1)) + "px");
		$('.circle_3').css('top', ( ( circleHeight / 2 ) * (3-1)) + "px");
		$('.circle_white').css({
			'top': "0",
			'right': "0",
			'width': (circleHeight / 3) + "px",
			'height': totalHeight + "px",
			'max-height': totalHeight + "px",
			'background-color': "#ffffff"
		});

	});



	var tl1 = gsap.timeline({defaults: { ease: Back.easeOut.config(2), opacity:0 }});
	var tl2 = gsap.timeline({defaults: { ease: Back.easeOut.config(2), opacity:0 }});
	var tl3 = gsap.timeline({defaults: { ease: "power4.out" }, repeat:-1});

	tl1.from('.circle_2', { delay:1, duration: 3, transformOrigin: 'center', right:-600})
		.from('.circle_1', { duration: 3, transformOrigin: 'center', right:-600}, '<0.3')
		.from('.circle_3', { duration: 3, transformOrigin: 'center', right:-600}, '<0.3')
		.from('.circle_white', { duration: 3, transformOrigin: 'center', right:-600, ease: Circ.easeOut}, '<0.3');

	tl2.from('.titleh1', { delay:1, duration: 3, transformOrigin: 'center', marginLeft:-700})
		.from('.titleh3', { duration: 3, transformOrigin: 'center', marginLeft:-700}, '<0.3')
		.from('.titleh32', { duration: 3, transformOrigin: 'center', marginLeft:-700}, '<0.3');


	tl3.from('.title_1',    {duration: 3, transformOrigin:'center', display:'none', marginTop:-70})
		.to('.title_1',     {duration: 3, transformOrigin:'center', marginTop:150})
		.from('.title_2',   {duration: 3, transformOrigin:'center', display:'none', marginTop:-110}, '<0.3')
		.to('.title_2',     {duration: 3, transformOrigin:'center', marginTop:150})
		.from('.title_3',   {duration: 3, transformOrigin:'center', display:'none', marginTop:-110}, '<0.3')
		.to('.title_3',     {duration: 3, transformOrigin:'center', marginTop:150});

	$(document).ready(function(){



		// ---------- CAROUSEL BRANDS

		$('.brand-carousel').owlCarousel({
			loop:true,
			margin:10,
			autoplay:true,
			nav: false,
			dots:false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:5
				}
			}
		})


	});


	// ---------- TOP BLACK BAR FUNCTIONS
	function changeLanguage(lang){
		var url = '{{ route('public.changeLanguage') }}';
		var data = {
			lang: lang,
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				location.reload();
			},
			onError: function(e){

			},
			onValidation: function(e){

			}
		});

	}

	function displayWelcomeBanner() {

		var video = document.getElementById('welcome_video');
		var source = document.createElement('source');

		@if( App::getLocale() == 'es' )
			@if($global_isProvider == true)
				source.setAttribute('src', '/videos/welcome_video_es_vende_720.mp4');
		@else
source.setAttribute('src', '/videos/welcome_video_es_compra.mp4');
		@endif
	@elseif( App::getLocale() == 'en' )
source.setAttribute('src', '/videos/welcome_video_en_720.mp4');
		@else
			source.setAttribute('src', '/videos/welcome_video_se_720.mp4');
		@endif

		video.appendChild(source);


		var modalWelcome = $('#modal-welcome-banner').modal();
		{{--ajaxPost('{{ route('public.setWelcomeBanner') }}',{ nuevoUsuario: 1 } );--}}

		modalWelcome.on('shown.bs.modal', function (e) {
			$("#welcome_video").each(function () { this.play() });
		});

		modalWelcome.on('hidden.bs.modal', function (e) {
			$("#welcome_video").each(function () { this.pause() });
		});
	}
</script>

</body>
</html>
