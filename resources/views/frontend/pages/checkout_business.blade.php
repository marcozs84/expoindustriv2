@extends('frontend.layouts.default')
@push('css')
<link href="/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/flat/blue.css" />
<style>

	label{
		/*display:inline-block !important;*/
		margin-bottom:0px !important;
		color: #4e4e4e;
	}
	/*input {*/
		/*display:block !important;*/
	/*}*/

	h4, h5 {
		display:block;
		clear:both;
		padding-left:17px;
	}

	h5{
		padding-bottom:10px;
		border-bottom:1px solid #eee;
	}

	.leftColumn{
		vertical-align:top;
	}
	.rightColumn{
		padding-left:10px !important;
	}

	.account-sidebar-cover{
		width:300px; overflow:hidden;
	}

	.account-body{
		margin-left:10px !important;
		padding-top:10px;
	}

	.nav-sections {
		margin-top:20px;
	}
	.nav-sections a {
		padding:10px;
		width:30%;

		font-weight:bold !important;
		font-size:14px;
	}
	.nav-sections i {
		margin-right:10px;
	}

	@media (min-width:991px) {
		.leftColumn {
			width:260px;
		}
		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:1200px) {
		.leftColumn {
			/*width:160px;*/
		}
	}
	@media (max-width:992px) {
		.leftColumn {
			width:190px;
			/*display:none;*/
		}

		.account-sidebar-cover{
			width:300px; overflow:hidden;
		}
	}
	@media (max-width:769px) {
		.leftColumn {
			/*width:190px;*/
			display:none;
		}

		.account-sidebar{
			display:none;
		}
		.account-body{
			margin-left:-20px !important;
			padding-top:10px;
		}
	}

	.specialsel2 .select2-container { width:100% !important; margin-top:0px !important; }



	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}
	.checkout-footer {
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 1);  /* blue */
	}
</style>
@endpush
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -360px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.paysuc_thankyou')</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	@include('frontend.includes.topMessage')


	<!-- BEGIN #checkout-payment -->
	<div class="section-container" id="checkout-payment" style="padding:0px;">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN checkout -->
			<div class="checkout">
				<form action="checkout_complete.html" method="POST" name="form_payment" class="form-horizontal">
					<!-- BEGIN checkout-body -->
					<div class="checkout-body" style="padding:0px; padding-bottom:40px;">
						<!-- BEGIN checkout-message -->
						<div class="checkout-message">
							<h1>
								{{--@lang('messages.paysuc_thankyou')--}}
								@if( $esFaktura )
									<small>@lang('messages.payment_faktura_note')</small>
								@endif
							</h1>

							<p style="font-weight:bold;">@lang('messages.paysuc_detail')</p>
							<div class="table-responsive2">
								<table class="table table-payment-summary">
									<tbody>
									<tr>
										<td class="field">@lang('messages.paysuc_transtatus')</td>
										<td class="value">@lang('messages.paysuc_succesful')</td>
									</tr>
									<tr>
										<td class="field">@lang('messages.paysuc_referenceno')</td>
										<td class="value">REF{{ $referencia }}</td>
									</tr>
									{{--<tr>--}}
										{{--<td class="field">Bank Authorised Code</td>--}}
										{{--<td class="value">AUTH000001</td>--}}
									{{--</tr>--}}
									<tr>
										<td class="field">@lang('messages.paysuc_datetime')</td>
										<td class="value">{{ \Carbon\Carbon::today()->format('d M Y h:i') }} </td>
									</tr>
									<tr>
										<td class="field">@lang('messages.paysuc_order')</td>
										<td class="value product-summary">
											{{--<div class="product-summary-img">--}}
												{{--<img src="../assets/img/product/product-iphone-6s-plus.png" alt="" />--}}
											{{--</div>--}}
											<div class="product-summary-info">
												<div class="title">@lang('messages.plan_business')</div>
												<div class="desc">From {{ \Carbon\Carbon::today()->format('d/m/Y') }} to {{ \Carbon\Carbon::today()->addYear(1)->format('d/m/Y') }}</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="field">@lang('messages.paysuc_payment')</td>
										<td class="value">{{ 6900 + ( (6900 / 100) * 25 ) }} kr (inkl. moms)</td>
									</tr>
									</tbody>
								</table>
							</div>
							<p class="text-muted text-center m-b-0">@lang('messages.paysuc_needassitance')
								{{--Should you require any assistance, you can get in touch with Support Team at <b>info@expoindustri.com</b>--}}
							</p>

							<div style="text-align:center;" class="nav-sections">
								<a href="{{ route('private.providerProfileEdit') }}" class="btn btn-info" style=" font-size:14px; font-weight:bold; padding:10px 20px;"><i class="fa fa-user"></i> @lang('messages.go_myprofile')</a>
								<a href="{{ route("public.{$glc}.privCuenta") }}" class="btn btn-success" style="font-size:14px; font-weight:bold; padding:10px 20px;"><i class="fa fa-home"></i> @lang('messages.go_myaccount')</a>
							</div>
						</div>

						<!-- END checkout-message -->
					</div>
					<!-- END checkout-body -->
					<!-- BEGIN checkout-footer -->
					{{--<div class="checkout-footer">--}}
						{{--<a href="checkout_info.html" class="btn btn-white btn-lg pull-left">Back</a>--}}
						{{--<button type="submit" class="btn btn-inverse btn-lg p-l-30 p-r-30 m-l-10">Proceed</button>--}}
					{{--</div>--}}
					<!-- END checkout-footer -->
				</form>
			</div>
			<!-- END checkout -->

		</div>
		<!-- END container -->
	</div>
	<!-- END #checkout-payment -->


@endsection

@push('scripts')

@if( $transaction_id > 0)
<script>

	var dataLayer = window.dataLayer || [];

	// Send transaction data with a pageview if available
	// when the page loads. Otherwise, use an event when the transaction
	// data becomes available.
	dataLayer.push({
		'event': 'transaction',
		'ecommerce': {
			'purchase': {
				'actionField': {
					'id': '{{ $transaction_id }}',                         // Transaction ID. Required for purchases and refunds.
					'affiliation': '{{ $plan_name }}',
					'revenue': '{{ $transaccion->monto }}',                     // Total transaction value (incl. tax and shipping)
					'tax':'{{ $transaccion->impuesto }}',
					'payment_type': '{{ $payment_type }}',
					'shipping': '0',
					'coupon': ''
				},
				'products': [{                            // List of productFieldObjects.
					'name': '{{ $plan_name }}',     // Name or ID is required.
					'id': '{{ $plan_id }}',
					'price': '{{ $transaccion->monto }}',
					'brand': 'ExpoIndustri',
					'category': 'plan',
					'variant': '',
					'quantity': 1,
					'coupon': ''                            // Optional fields may be omitted or set to empty string.
				}]
			}
		}
	});

</script>
@endif

@endpush