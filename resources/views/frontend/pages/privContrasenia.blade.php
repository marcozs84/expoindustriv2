@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.password_update')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">

				@include('frontend.pages.private.component.sidebarmenu', ['selection' => 'my_password'])

				<!-- BEGIN account-body -->
				<div class="account-body" style="min-height:800px;">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN col-6 -->
						<div class="col-md-12">

							<h4>@lang('messages.password_update')</h4>
							<p>
								@lang('messages.password_msg')
							</p>
							<p>
								@lang('messages.all_you_need_one_place')
							</p>
							<br>

							<div class="row">
								<div class="col col-lg-12 col-sm-12 m-b-20">
									<h4><i class="fas fa-universal-access fa-fw text-muted"></i> @lang('messages.update_email')</h4>
									<br>
									<form id="formActualizarCorreo" action="{{ route('private.changePassword') }}" method="POST">
										{{ csrf_field() }}
										<fieldset>
											<div class="alert alert-warning">
												<p>@lang('messages.update_email_info').</p>
											</div>
											{{--<legend class="m-b-15">Actualizar correo y contraseña</legend>--}}
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.current_email')</label>
												<div class="col-md-8">
													<input id="email" type="text" value="{{ Auth::guard('clientes')->user()->username }}" class="form-control" placeholder="@lang('messages.current_email')">
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-3 col-offset-3">
													<a href="javascript:;" onclick="actualizarCorreo()" class="btn btn-md btn-warning m-r-5"><i class="fa fa-save"></i> @lang('messages.save')</a>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
								<div class="col col-lg-12 col-sm-12">
									<br>
									<h4><i class="fas fa-universal-access fa-fw text-muted"></i> @lang('messages.password_update')</h4>
									<br>
									<div id="regTopError_forgotEmail"></div>
									<form id="formActualizarContrasenia" action="{{ route('private.changePassword') }}" method="POST">
										{{ csrf_field() }}
										<fieldset>
											{{--<legend class="m-b-15">Actualizar correo y contraseña</legend>--}}
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.current_password')</label>
												<div class="col-md-8">
													<input id="password_current" type="password" class="form-control" placeholder="@lang('messages.current_password')">
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.new_password')</label>
												<div class="col-md-8">
													<input id="password" type="password" class="form-control" placeholder="@lang('messages.new_password')">
												</div>
											</div>
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">@lang('messages.password_confirmation')</label>
												<div class="col-md-8">
													<input id="password2" type="password" class="form-control" placeholder="@lang('messages.password_confirmation')">
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-3 col-offset-md-3">
													<a href="javascript:;" onclick="actualizarContrasenia()" class="btn btn-md btn-warning m-r-5"><i class="fa fa-save"></i> @lang('messages.save')</a>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
						<!-- END col-6 -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->

@endsection

@push('scripts')
<script>

	function formInit(){
	}
	formInit();

	// ----------

	$( "#formActualizarCorreo" ).submit(function( event ) {
		actualizarCorreo();
		event.preventDefault();
	});

	function actualizarCorreo(){
		var url = '{{ route('resource.usuario.update', [\Illuminate\Support\Facades\Auth::user()->id]) }}';
		var data = {
			username : $('#email').val(),
		};
		ajaxPatch(url, data, function(data){
			console.log('Cuenta de correo actualizada');
		}, function(data){
			console.log('No se pudo actualizar su cuenta de correo', 'warning');
		});
	}

	// ----------

	$( "#formActualizarContrasenia" ).submit(function( event ) {
		actualizarContrasenia();
		event.preventDefault();
	});

	function actualizarContrasenia() {
{{--		var url = '{{ route('resource.usuario.update', [\Illuminate\Support\Facades\Auth::user()->id]) }}';--}}
		var url = '{{ route('private.changePassword') }}';
		var data = {
			password_current: $('#password_current').val(),
			password: $('#password').val(),
			password_confirmation: $('#password2').val()
		};

		$('#regTopError_forgotEmail').html('');

		ajaxPost(url, data, {
			silent: true,
			onSuccess: function(data){
				swal("", "@lang('messages.password_updated_succesfully')", "success")
					.then(function (response) {
						$('#password_current').val('');
						$('#password').val('');
						$('#password2').val('');
					});
			}, onFailure: function(data){
				console.log('No se pudo actualizar la contraseña', 'warning');
				$('#regTopError_forgotEmail').html('<span class="help-block"><strong>' + 'An error occurred during the transaction, please contact our system administrator.' + '</strong></span>');
			}, onValidation: function(e){
				$('#regTopBtnForgot').html('@lang('messages.send')');
				$('#regTopBtnForgot').removeAttr('disabled');
//					var errors = JSON.parse(e.responseText);
				var errors = e;
				var message = '';

				for (var elem in errors) {
					message += errors[elem] + '<br>';
				}
				$('#regTopError_forgotEmail').html('<div class="alert alert-warning">' + message + '</div>');
//					$('#loginTopErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
				return false;
			}
		});
	}



</script>
@endpush
