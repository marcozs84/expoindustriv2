@extends('frontend.layouts.default')

@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush

@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.my_favorites')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">

				@include('frontend.pages.private.component.sidebarmenu', ['selection' => 'my_favs'])

				<!-- BEGIN account-body -->
				<div class="account-body" style="min-height:800px;">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">

							<h4>@lang('messages.my_favorites')</h4>
							<p>
								@lang('messages.my_favorites.leftmessage')
							</p>
							<br>
							<table id="data-table" class="table table-striped width-full table-condensed"></table>

						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>

<script>

	var datatable = null;
	var modal = null;

	$(document).ready(function(){
		initDataTable();
	});

	var estadosPub = {!! json_encode($estados)  !!};

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 50,
				searching: false,
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('private.announcements.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
						d.filters = 'favoritos';
//	                d.element = $('#element').val();
						// ...
					},
					dataFilter: function(data){ // Procesa datos recibidos
						return data;
//	                var data = JSON.parse(data);
//	                for(var i = 0 ; i < data.data.length; i++){
//	                    // ...
//	                }
//	                return JSON.stringify( data );
					}
				},
				columns: [
//					{ data: "id", title: 'Id' },
					{ title: 'Anuncio'},
					{ data: 'interesados', title: '@lang('messages.interested')'},
					{ data: 'producto.precio_publicacion', title: '@lang('messages.price')'},
					{ title: '@lang('messages.status')'},
					{ data: 'fecha_inicio', title: '@lang('messages.publication_date')'},
//					{ title: '<input type="checkbox" id="idsChecker" value="" />'},
					{ title: ''},
					// ...
				],
				columnDefs: [
					{
//						targets: 0,
//						className: 'text-center',
//						width: '30px',ef
//					},{
						targets: 0,
						render: function(data, type, row){
							if(row.producto.marca != undefined){
								{{--var url = '{{ route("private.{$glc}.announcements.show", [0]) }}';--}}
								var url = '{{ route('public.producto', ['idProducto']) }}';
								url = url.replace('idProducto', row.id);
								return '<a href="'+ row.producto.url_csmmid +'">'+ row.producto.marca.nombre + ' ' + row.producto.modelo.nombre +'</a>';
							}
						}
					},{
						targets: 1,
						className: 'text-center',
						render: function(data, type, row){
							return 0;
						}
					},{
						targets: 2,
						className: 'text-right',
						render: function(data, type, row){
							@if($global_isProvider == true)
								if( row.producto.precioSudamerica === true ) {
									return '@lang('messages.price_not_available')';
								} else {
									return data + " {{ strtoupper(session('current_currency', 'usd')) }}";
								}
							@else
								return data + " {{ strtoupper(session('current_currency', 'usd')) }}";
							@endif

						}
					},{
						targets: 3,
						className: 'text-right',
						render: function(data, type, row){
							return estadosPub[row.estado];
						}
					},{
						targets: 4,
						className: 'text-center',
					},{
						targets: 5,
						className: 'text-center',
						render: function(data, type, row){
//							return '<input type="checkbox" value="'+row.id+'">';
							return '<a href="javascript:;" onclick="removeFavorite('+row.id+')" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt text-white"></i></a>';
						}
					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});
			});
		} else {
			datatable.ajax.reload();
		}

	}

	{{--function permisoCrear(){--}}
		{{--var url = '{{ route('admin.permiso.create') }}';--}}
		{{--var modal = openModal(url, 'Nuevo Permiso');--}}
		{{--setModalHandler('formPermisoCreate:aceptar', function(){--}}
			{{--dismissModal(modal);--}}
			{{--datatable.ajax.reload();--}}
		{{--});--}}
	{{--}--}}

	{{--function permisoEditar(id){--}}
		{{--var url = '{{ route('admin.permiso.index') }}/'+id+'/edit';--}}
		{{--var modal = openModal(url, 'Editar Permiso');--}}
		{{--setModalHandler('formPermisoEdit:aceptar', function(){--}}
			{{--dismissModal(modal);--}}
			{{--datatable.ajax.reload(null, false);--}}
		{{--});--}}
	{{--}--}}


	//TODO: Eliminar/modificar esta funcion
	function permisoEliminar(){
		var ids = [];
		$('.chkId').each(function() {
			if($(this).prop('checked')){
				ids.push($(this).val());
			}
		});
		if(ids.length == 0){
			alert("Debe seleccionar al menos 1 registro.");
			return false;
		}
		var url = '{{ route('private.announcements.delete', [0]) }}';

		ajaxDelete(url, ids, function(){
			datatable.ajax.reload(null, false);
		});
	}

	var esUsuario = false;
	function removeFavorite(pubId){

		@if($auth_username != '')
			esUsuario = true;
		@endif

		if(esUsuario === true){
			var url = '{{ route('public.removeFavorite') }}';
			var data = {
				idPublicacion: pubId
			};
			ajaxPost(url, data, {
				onSuccess: function(data){
					datatable.ajax.reload(false);
				}
			});
		} else {
			alert('@lang('messages.no_active_users').');
		}
	}
</script>

@endpush