@extends('frontend.layouts.default')
@push('metas')

<meta property="og:app_id"        content="243295883080737" />
<meta property="og:url"           content="https://www.expoindustri.com/producto/{{ $pub->id }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ $meta_description }}" />
<meta property="og:description"   content="{{ $meta_description }}" />
@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)
	<meta property="og:image"         content="http://www.expoindustri.com/{{ $pub->Producto->Galerias[0]->Imagenes[0]->ruta_publica_producto_thumb }}" />
@else
	<meta property="og:image"         content="http://www.expoindustri.com/assets/img/logo/logo_exind2_en.png" />
@endif




@endpush
@push('css')
<link href="/assets/plugins/lightbox2/css/lightbox.css" rel="stylesheet" />

{{--<style>--}}
	{{--/*jssor slider loading skin spin css*/--}}
	{{--.jssorl-009-spin img {--}}
		{{--animation-name: jssorl-009-spin;--}}
		{{--animation-duration: 1.6s;--}}
		{{--animation-iteration-count: infinite;--}}
		{{--animation-timing-function: linear;--}}
	{{--}--}}

	{{--@keyframes jssorl-009-spin {--}}
		{{--from { transform: rotate(0deg); }--}}
		{{--to { transform: rotate(360deg); }--}}
	{{--}--}}

	{{--/*jssor slider bullet skin 072 css*/--}}
	{{--.jssorb072 .i {position:absolute;color:#000;font-family:"Helvetica neue",Helvetica,Arial,sans-serif;text-align:center;cursor:pointer;z-index:0;}--}}
	{{--/*.jssorb072 .i .b {fill:#fff;opacity:.3;}*/--}}
	{{--.jssorb072 .i .b {fill:#e5e5e5;opacity:.8;}--}}
	{{--.jssorb072 .i:hover {opacity:.7;}--}}
	{{--.jssorb072 .iav {color:#fff;}--}}
	{{--.jssorb072 .iav .b {fill:#000;opacity:.5;}--}}
	{{--.jssorb072 .i.idn {opacity:.3;}--}}

	{{--/*jssor slider arrow skin 073 css*/--}}
	{{--.jssora073 {display:block;position:absolute;cursor:pointer;}--}}
	{{--.jssora073 .a {fill:#ddd;fill-opacity:.7;stroke:#000;stroke-width:160;stroke-miterlimit:10;stroke-opacity:.7;}--}}
	{{--.jssora073:hover {opacity:.8;}--}}
	{{--.jssora073.jssora073dn {opacity:.4;}--}}
	{{--.jssora073.jssora073ds {opacity:.3;pointer-events:none;}--}}

	{{--#jssor_1 > div { position:relative !important; }--}}
	{{--#jssor_1 > div > div { position:relative !important; }--}}
{{--</style>--}}
<style>
	.fa-ul li {
		padding:4px;
	}


	.thumbnails {
		margin-top:20px;
		width:100%;
		/*max-height:200px;*/
		/*position: relative;*/
		/*border:1px solid #000000;*/
		overflow:auto;
	}

	.thumbnails a {
		width:200px;
		height:120px;
		background-color:#000000;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		display:block;
		margin:0px 10px 0px 0px;
		text-align:center;
		overflow:hidden;
	}
	.thumbnails a img {
		max-width:100%;
		max-height:100%;
		margin:auto;
		position: relative;
	}

	.img_holder tr td {
		/*width:200px;*/
	}
</style>

<style>
	.popover{
		width:500px;
	}
	.product-image-alt {
		position:relative !important;
		margin:0 auto !important;
		margin-left:10px;
		margin-top:10px;
		top:0px;
		left:0px;
		/*width:525px;height:350px;*/
		overflow:hidden;
		visibility:hidden;
		background-color:#fff;
	}

	.slides_t {
		cursor:default;
		position:relative;
		top:0px;
		left:0px;
		/*width:525px;*/
		height:450px;
		overflow:hidden;

		width:100%;
	}

	.product-social{
		margin: 0px !important;
		padding: 0px !important;
	}

	.item.item-thumbnail {
		border:none;

		/*-webkit-box-shadow: 0px -3px #348fe2;*/
		/*-moz-box-shadow: 0px -3px #348fe2;*/
		/*box-shadow: 0px -3px #348fe2;*/
		margin-bottom: 15px;
		background-color: #ffffff;
	}
	.item.item-thumbnail .item-image{
		padding:2px;
		/*background-color:#f0f3f4;*/
		background-color:#242a31;
		-webkit-border-radius: 3px 3px 0px 0px;
		-moz-border-radius: 3px 3px 0px 0px;
		border-radius: 3px 3px 0px 0px;

		/* Ref.: https://stackoverflow.com/a/17736788 */
		display: flex; flex-direction: column; justify-content: center; align-items: center;
	}
	.item.item-thumbnail .item-info {
		border: 1px solid #d8d8d8;
	}

	.item.item-thumbnail .item-title, .item.item-thumbnail .item-title a {
		color:#242a31 !important;
	}
	.item .item-thumbnail .item-title, .item .item-thumbnail .item-title a {
		color:#242a31 !important;
	}



	@media (min-width:991px) {

	}
	@media (max-width:1200px) {
	}
	@media (max-width:992px) {
	}
	@media (max-width:892px) {
	}
	@media (max-width:768px) {
		.product-purchase-container {
			padding:10px 20px !important;
		}
		.product-purchase-container .product-discount, .product-purchase-container .product-price{
			position:static !important;
			margin-bottom:0px;
		}
	}
	@media (max-width:500px) {
		.product-price-label {
			display:none;
		}
		.product-purchase-container .product-discount, .product-purchase-container .product-price{
			position:static !important;
		}
		.product-image-alt {
			margin-left:0px;
			margin-top:0px;
			top:0px;
			left:0px;
		}
	}

	.inputError {
		border:1px solid #ff0000 !important;
	}

	.checkbox.checkbox-css {
		padding-left:10px;
		padding-right:10px;
	}

	.rcp {
		/*background-color:#f0f3f4;*/
		text-align:center;
		border-left: 1px solid #d8d8d8;
	}

</style>
@endpush
@section('content')

	@include('frontend.includes.topMessage')

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=243295883080737&autoLogAppEvents=1';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

	<!-- BEGIN #product -->
	<div id="product" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.buy_page') }}">@lang('messages.home')</a></li>
{{--				<li><a href="{{ route("public.{$glc}.search", [$pub->Producto->Categorias[0]->Padre->id]) }}">{{ $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] }}</a></li>--}}
				<li><a href="{{ $pub->Producto->url_c }}">{{ $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] }}</a></li>
				<li><a href="{{ $pub->Producto->url_cs }}">{{ $pub->Producto->Categorias[0]->Traduccion[$global_localization] }}</a></li>
				{{--<li class="active">{{ $pub->Producto->Categorias[0]->Traduccion[$global_localization] }}</li>--}}
			</ul>
			<!-- END breadcrumb -->

			<div class="row">
				<div class="col-md-8">


					@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)
						@php
							$imagen = $pub->Producto->Galerias[0]->Imagenes[0];
						@endphp
						{{--								@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)--}}
						<div style="text-align:center; background-color:#000000; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
							<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">
								<img data-u="image" src="{{ $imagen->ruta_publica_producto }}" style="max-width:100%; max-height:500px;" />
							</a>
						</div>

						<div class="thumbnails">

							<table class="img_holder">
								<tr>

									@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)

										<td>
											<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">
												<img src="{{ $imagen->ruta_publica_producto_thumb }}" />
											</a>
										</td>

									@endforeach

								</tr>
							</table>

						</div>
					@endif

				<!-- BEGIN product-main-image -->
					{{--<div class="product-main-image" data-id="main-image">--}}

						{{--<img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" />--}}
					{{--</div>--}}

					{{--@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)--}}
					{{--@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)--}}
					{{--<div data-p="170.00">--}}
					{{--<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">--}}
					{{--<img data-u="image" src="{{ $imagen->ruta_publica_producto_thumb }}" />--}}
					{{--</a>--}}
					{{--</div>--}}
					{{--@endforeach--}}
					{{--@endif--}}

				</div>
				<div class="col-md-4">

					<div style="min-height:500px; width:100%; /*border:1px solid #333;*/">

						<div class="product-info" style="padding-top:0px; position: relative;">
							<!-- BEGIN product-info-header -->
							<div class="product-info-header">

								<h1 class="product-title">


									@if($pub->Owner->Owner->Proveedor->tieneStore == 1)
										<div>
											@if($tieneImagen == true)
												<div style="background-color:{{ $imagen->backgroundColor }}; /*background-color:#ffffff;*/ /*border:1px solid #c5ced4;*/ text-align:center; padding:10px; margin:0px 0px 0px 0px;">
													<a class="text-white" href="{{ route("public.{$glc}.perfilCliente", [$pub->Owner->Owner->id]) }}">
														<img src="{{ $logoUrl }}" alt="Logo proveedor" style="max-width:100%;">
													</a>
												</div>
											@endif
										</div>

										@if( $pub->Owner->Owner->Proveedor->descripcion != '' )
										<div class="">
											<a href="javascript:;" class="see_hide_link text-right" style="float:right; font-size:12px; text-decoration:none;" onclick="show_hide_description()"><i class="fa fa-eye"></i> @lang('messages.see_more_desc')</a>

											<div class="prov_description  " style="font-size:12px; clear:both; padding-bottom:10px; display:none;">
												{!! nl2br($companyDesc) !!}
											</div>
											<div style="font-size:12px; clear:both; "></div>
										</div>
										@endif
									@endif



										<script>
											function show_hide_description() {
												if( $('.prov_description').is(':visible') ) {
//													$('.prov_description').addClass('hide');
													$('.prov_description').hide(500);
													$('.see_hide_link').html('<i class="fa fa-eye"></i>&nbsp;&nbsp; @lang('messages.see_more_desc')');
												} else {
//													$('.prov_description').removeClass('hide');
													$('.prov_description').show(500);
													$('.see_hide_link').html('<i class="fa fa-eye-slash"></i>&nbsp; @lang('messages.hide_more')');
												}
											}
										</script>

									<span class="label label-primary @if($pub->Owner->Owner->Proveedor->tieneStore != 1) hide @endif" style="width:auto !important; max-width:none !important; padding:10px; font-weight:bold;">
										<a class="text-white" target="_blank" href="{{ route("public.{$glc}.perfilCliente", [$pub->Owner->Owner->id]) }}">
											@lang('messages.goto_store')
										</a>
									</span>

									{{ $pub->Producto->Marca->nombre }} {{ $pub->Producto->Modelo->nombre }}
								</h1>
								<ul class="product-category">
									<li><a href="#">{{ $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] }}</a></li>
									<li>/</li>
									<li><a href="#">{{ $pub->Producto->Categorias[0]->Traduccion[$global_localization] }}</a></li>
								</ul>
							</div>
							<!-- END product-info-header -->
							<!-- BEGIN product-warranty -->
							<div class="product-warranty">
								<div class="pull-right">{{ $pub->created_at->format('d/m/Y') }}</div>
								<div style="padding-bottom:15px;"><b>@lang('messages.availability'):</b> {{ $pub->Producto->ProductoItem->Disponibilidad->Traduccion[$global_localization] }}</div>
								<div><b>@lang('messages.location'):</b> {{ $global_paises[$pub->Producto->ProductoItem->Ubicacion->pais] }}</div>
							</div>
							<!-- END product-warranty -->
							<!-- BEGIN product-info-list -->
							{{--<ul class="product-info-list">--}}

							<ul class="fa-ul">
								@foreach($primaryData as $data)
									<li>
										<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
										<b>@lang('form.'.$data['key'])</b>:
										@if($data['type'] == 'select')
											@lang('form.'.$data['value'])
										@elseif($data['type'] == 'multiselect')
											<ul class="fa-ul">
												@php
													if(!is_array($data['value'])){
														$data['value'] = [$data['value']];
													}
												@endphp
												@foreach($data['value'] as $val)
													<li>
														<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
														@lang('form.'.$val)
													</li>
												@endforeach
											</ul>
										@elseif($data['type'] == 'range_year')
											{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
										@elseif($data['type'] == 'boolean')
											@lang('form.'.$data['value'])
										@else
											@if($data['key'] == 'pais_de_fabricacion' && $data['value'] != '' && isset( $global_paises[$data['value']] ))
												{{ $global_paises[$data['value']] }} ({{ strtoupper($data['value']) }})
											@else
												{{ $data['value'] }}
											@endif
										@endif
									</li>
								@endforeach
							</ul>
							<!-- END product-info-list -->
							<!-- BEGIN product-purchase-container -->
							<div class="product-purchase-container" style="margin-top:15px !important; padding-top:15px !important; border-top:1px solid #D8E0E4;">
								{{--<h4 class="m-b-0 product-price-label">@lang('messages.price_in_europe'):</h4>--}}
								{{--							<div class="product-discount">@lang('messages.price_in_europe'):</div>--}}
								<div class="product-discount">@lang('messages.price_at_origin'):</div>

								<div class="product-price">
									{{--								<div class="price">{{ strtoupper($pub->Producto->monedaUnitario) }} {{ number_format($pub->Producto->precioUnitario, 0) }}</div>--}}

										<div class="price" style="text-transform:uppercase;">
											@if($global_isProvider == true && $pub->Producto->precioSudamerica == 1)
												<a href="javascript:;" onclick="modalContactConsultant()" class="btn btn-xl btn-danger">@lang('messages.contact_consultant')</a>
											@else
												{{ number_format($pub->Producto->precio_publicacion, 2, '.', ',') }} {{ session('current_currency', 'usd') }}
											@endif
										</div>

										<!-- BEGIN product-social -->
										<div class="product-social pull-right" style="border-bottom:0px; padding-top:15px !important; margin-top:15px !important; border-top:1px solid #D8E0E4; width:100%; clear:both;">
											<ul>

												{{--<li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww2.expoindustriv2%2Fproducto%2F25&text=@lang('messages.hi_tweet')"--}}
												{{--class="twitter" data-toggle="tooltip" data-trigger="hover" data-title="Twitter" data-placement="top"><i class="fab fa-twitter"></i></a></li>--}}
												{{--<li><a href="javascript:;" class="google-plus" data-toggle="tooltip" data-trigger="hover" data-title="Google Plus" data-placement="top"><i class="fab fa-google-plus"></i></a></li>--}}
												{{--<li><a href="javascript:;" class="whatsapp" data-toggle="tooltip" data-trigger="hover" data-title="Whatsapp" data-placement="top"><i class="fab fa-whatsapp"></i></a></li>--}}
												{{--<li><a href="javascript:;" class="tumblr" data-toggle="tooltip" data-trigger="hover" data-title="Tumblr" data-placement="top"><i class="fab fa-tumblr"></i></a></li>--}}

												{{--<li><a href="javascript:;" class="tumblr" data-toggle="tooltip" data-trigger="hover" data-title="E-mail" data-placement="top"><i class="fa fa-envelope"></i></a></li>--}}


												{{-- ------------------- TEMPORALMENTE DESHABILITADO ---------------- --}}
												{{--<li><a href="javascript:;" class="facebook" data-toggle="tooltip" data-trigger="hover" data-title="Facebook" data-placement="top"><i class="fab fa-facebook"></i></a></li>--}}
												{{--<li>--}}
												{{--<div class="fb-share-button"--}}
												{{--data-href="https://www.expoindustri.com/producto/25"--}}
												{{--data-layout="button"--}}
												{{--data-size="small"--}}
												{{--data-mobile-iframe="false">--}}
												{{--<a target="_blank"--}}
												{{--href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww2.expoindustri.com%2Fproducto%2F25&amp;src=sdkpreparse"--}}
												{{--class="fb-xfbml-parse-ignore">Compartir_mz</a>--}}
												{{--</div>--}}
												{{--</li>--}}
												{{-- ------------------- TEMPORALMENTE DESHABILITADO END ---------------- --}}
												<li>
													<a id="btnRemoveFavorite" class="tumblr btn-danger @if($esFavorito != true) hide @endif" onclick="removeFavorite()" data-toggle="tooltip" data-title="@lang('messages.remove_favorite')" data-placement="top"><i class="fa fa-heart"></i> @lang('messages.remove_favorite')</a>
													<a id="btnAddFavorite" class="facebook @if($esFavorito == true) hide @endif" onclick="addFavorite(this)" data-toggle="tooltip" data-title="@lang('messages.add_favorites')" data-placement="top"><i class="fa fa-heart"></i></a>
												</li>
											</ul>
										</div>
										<!-- END product-social -->

									{{--<div class="product-discount">--}}
									{{--<span class="discount">{{ $pub->moneda }}869.00</span>--}}
									{{--</div>--}}


								</div>
							</div>
							<!-- END product-purchase-container -->

						</div>
					</div>

				</div>
			</div>



			<!-- BEGIN product -->
			<div class="product" style="margin-top:10px; padding-top:20px; clear:both;">







				<!-- BEGIN product-detail -->
				<div class="product-detail" style="display:none;">
					<!-- BEGIN product-image -->
					<div class="product-image" style="/*width:630px !important;*/" hidden>
						<!-- BEGIN product-thumbnail -->
						{{--<div class="product-thumbnail">--}}
							{{--<ul class="product-thumbnail-list">--}}
								{{--<li class="active"><a href="#" data-click="show-main-image" data-url="http://images.wisegeek.com/tractor-for-sale.jpg"><img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" /></a></li>--}}
								{{--<li><a href="#" data-click="show-main-image" data-url="http://images.wisegeek.com/tractor-for-sale.jpg"><img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" /></a></li>--}}
								{{--<li><a href="#" data-click="show-main-image" data-url="http://images.wisegeek.com/tractor-for-sale.jpg"><img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" /></a></li>--}}
								{{--<li><a href="#" data-click="show-main-image" data-url="http://images.wisegeek.com/tractor-for-sale.jpg"><img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" /></a></li>--}}
								{{--<li><a href="#" data-click="show-main-image" data-url="http://images.wisegeek.com/tractor-for-sale.jpg"><img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" /></a></li>--}}
								{{--<li><a href="#" data-click="show-main-image" data-url="http://images.wisegeek.com/tractor-for-sale.jpg"><img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" /></a></li>--}}
							{{--</ul>--}}
						{{--</div>--}}
						{{--<!-- END product-thumbnail -->--}}
						{{--<!-- BEGIN product-main-image -->--}}
						{{--<div class="product-main-image" data-id="main-image">--}}
							{{--<img src="http://images.wisegeek.com/tractor-for-sale.jpg" alt="" />--}}
						{{--</div>--}}







						{{--<div id="jssor_1" class="product-image product-image-alt" style="">--}}
							{{--<!-- Loading Screen -->--}}
							{{--<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">--}}
								{{--<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="/assets/img/spin.svg" />--}}
							{{--</div>--}}
							{{--<!-- Slides -->--}}
							{{--<div data-u="slides" class="slides_t" style="">--}}
								{{--@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)--}}
									{{--@foreach($pub->Producto->Galerias[0]->Imagenes as $imagen)--}}
										{{--<div data-p="170.00">--}}
											{{--<a href="{{ $imagen->ruta_publica_producto }}" data-lightbox="gallery-group-1">--}}
												{{--<img data-u="image" src="{{ $imagen->ruta_publica_producto }}" />--}}
											{{--</a>--}}
										{{--</div>--}}
									{{--@endforeach--}}
								{{--@endif--}}
							{{--</div>--}}
							{{--<!-- Bullet Navigator -->--}}
							{{--<div data-u="navigator" class="jssorb072" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">--}}
								{{--<div data-u="prototype" class="i" style="width:24px;height:24px;font-size:12px;line-height:24px;">--}}
									{{--<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;z-index:-1;">--}}
										{{--<circle class="b" cx="8000" cy="8000" r="6666.7"></circle>--}}
									{{--</svg>--}}
									{{--<div data-u="numbertemplate" class="n"></div>--}}
								{{--</div>--}}
							{{--</div>--}}
							{{--<!-- Arrow Navigator -->--}}
							{{--<div data-u="arrowleft" class="jssora073" style="width:40px;height:50px;top:0px;left:30px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">--}}
								{{--<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">--}}
									{{--<path class="a" d="M4037.7,8357.3l5891.8,5891.8c100.6,100.6,219.7,150.9,357.3,150.9s256.7-50.3,357.3-150.9 l1318.1-1318.1c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3L7745.9,8000l4216.4-4216.4 c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3l-1318.1-1318.1c-100.6-100.6-219.7-150.9-357.3-150.9 s-256.7,50.3-357.3,150.9L4037.7,7642.7c-100.6,100.6-150.9,219.7-150.9,357.3C3886.8,8137.6,3937.1,8256.7,4037.7,8357.3 L4037.7,8357.3z"></path>--}}
								{{--</svg>--}}
							{{--</div>--}}
							{{--<div data-u="arrowright" class="jssora073" style="width:40px;height:50px;top:0px;right:30px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">--}}
								{{--<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">--}}
									{{--<path class="a" d="M11962.3,8357.3l-5891.8,5891.8c-100.6,100.6-219.7,150.9-357.3,150.9s-256.7-50.3-357.3-150.9 L4037.7,12931c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3L8254.1,8000L4037.7,3783.6 c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3l1318.1-1318.1c100.6-100.6,219.7-150.9,357.3-150.9 s256.7,50.3,357.3,150.9l5891.8,5891.8c100.6,100.6,150.9,219.7,150.9,357.3C12113.2,8137.6,12062.9,8256.7,11962.3,8357.3 L11962.3,8357.3z"></path>--}}
								{{--</svg>--}}
							{{--</div>--}}
						{{--</div>--}}







						{{--<div class="row m-t-30">--}}
							{{--<div class="col-md-12 text-center">--}}
								{{--<a name="ancCalculadora"></a>--}}
								{{--<a href="#ancCalculadora" onclick="mostrarCalculadora()" class="btn btn-inverse btn-lg" ><i class="fa fa-dollar-sign"></i> @lang('messages.quote_final_destination')</a>--}}
							{{--</div>--}}
						{{--</div>--}}

						{{--<script>--}}
							{{--twttr.events.bind('tweet', function (event) {--}}
								{{--// Do something there--}}
								{{--alert('Tweeted');--}}
							{{--});--}}
						{{--</script>--}}
						<!-- END product-main-image -->
					</div>
					<!-- END product-image -->
					<!-- BEGIN product-info -->
					<div class="product-info" hidden>
						<!-- BEGIN product-info-header -->
						<div class="product-info-header">

							<h1 class="product-title">
								<span class="label label-info @if($pub->Owner->Owner->Proveedor->tieneStore != 1) hide @endif" style="width:auto !important; max-width:none !important; padding:10px; font-weight:bold;">
									<a class="text-white" href="{{ route("public.{$glc}.perfilCliente", [$pub->Owner->Owner->id]) }}">{{ $pub->Owner->Owner->Proveedor->nombre }}</a>
{{--									{{ $pub->Owner->Owner->Agenda->nombres_apellidos }}--}}
{{--									{{ $pub->Owner->Owner->Proveedor->nombre }}--}}
								</span>

								@if($pub->Owner->Owner->Proveedor->tieneStore == 1)
								<div>
									@if($tieneImagen == true)
										<div style="background-color:{{ $imagen->backgroundColor }}; /*background-color:#ffffff;*/ /*border:1px solid #c5ced4;*/ text-align:center; padding:10px; margin:10px 0px 10px 0px;">
											<a class="text-white" href="{{ route("public.{$glc}.perfilCliente", [$pub->Owner->Owner->id]) }}">
												<img src="{{ $logoUrl }}" alt="Logo proveedor" style="max-width:100%;">
											</a>
										</div>
									@endif
								</div>
								@endif

								{{ $pub->Producto->Marca->nombre }} {{ $pub->Producto->Modelo->nombre }}
							</h1>
							<ul class="product-category">
								<li><a href="#">{{ $pub->Producto->Categorias[0]->Padre->Traduccion[$global_localization] }}</a></li>
								<li>/</li>
								<li><a href="#">{{ $pub->Producto->Categorias[0]->Traduccion[$global_localization] }}</a></li>
							</ul>
						</div>
						<!-- END product-info-header -->
						<!-- BEGIN product-warranty -->
						<div class="product-warranty">
							<div class="pull-right">{{ $pub->created_at->format('d/m/Y') }}</div>
							<div style="padding-bottom:15px;"><b>@lang('messages.availability'):</b> {{ $pub->Producto->ProductoItem->Disponibilidad->Traduccion[$global_localization] }}</div>
							<div><b>@lang('messages.location'):</b> {{ $global_paises[$pub->Producto->ProductoItem->Ubicacion->pais] }}</div>
						</div>
						<!-- END product-warranty -->
						<!-- BEGIN product-info-list -->
						{{--<ul class="product-info-list">--}}

						<ul class="fa-ul">
							@foreach($primaryData as $data)
								<li>
									<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
									<b>@lang('form.'.$data['key'])</b>:
									@if($data['type'] == 'select')
										@lang('form.'.$data['value'])
									@elseif($data['type'] == 'multiselect')
										<ul class="fa-ul">
											@php
												if(!is_array($data['value'])){
													$data['value'] = [$data['value']];
												}
											@endphp
											@foreach($data['value'] as $val)
												<li>
													<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
													@lang('form.'.$val)
												</li>
											@endforeach
										</ul>
									@elseif($data['type'] == 'range_year')
										{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
									@elseif($data['type'] == 'boolean')
										@lang('form.'.$data['value'])
									@else
										@if($data['key'] == 'pais_de_fabricacion' && $data['value'] != '' && isset( $global_paises[$data['value']] ))
											{{ $global_paises[$data['value']] }} ({{ strtoupper($data['value']) }})
										@else
											{{ $data['value'] }}
										@endif
									@endif
								</li>
							@endforeach
						</ul>
						<!-- END product-info-list -->
						<!-- BEGIN product-purchase-container -->
						<div class="product-purchase-container">
							{{--<h4 class="m-b-0 product-price-label">@lang('messages.price_in_europe'):</h4>--}}
{{--							<div class="product-discount">@lang('messages.price_in_europe'):</div>--}}
							<div class="product-discount">@lang('messages.price'):</div>
							<div class="product-price">
{{--								<div class="price">{{ strtoupper($pub->Producto->monedaUnitario) }} {{ number_format($pub->Producto->precioUnitario, 0) }}</div>--}}

								@if($global_isProvider == true && $pub->Producto->precioSudamerica == 1)
									<div class="price" style="text-transform:uppercase;">
										<a href="javascript:;" onclick="modalContactConsultant()" class="btn btn-xl btn-danger">@lang('messages.contact_consultant')</a>
									</div>
								@else
									<div class="price" style="text-transform:uppercase;">
										{{ session('current_currency', 'usd') }}
										{{ number_format($pub->Producto->precio_publicacion, 2, '.', ',') }}


										<!-- BEGIN product-social -->
										<div class="product-social pull-right" style="border-bottom:0px; padding-top:0px; border-top:1px solid #D8E0E4;">
											<ul>

												{{--<li><a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww2.expoindustriv2%2Fproducto%2F25&text=@lang('messages.hi_tweet')"--}}
												{{--class="twitter" data-toggle="tooltip" data-trigger="hover" data-title="Twitter" data-placement="top"><i class="fab fa-twitter"></i></a></li>--}}
												{{--<li><a href="javascript:;" class="google-plus" data-toggle="tooltip" data-trigger="hover" data-title="Google Plus" data-placement="top"><i class="fab fa-google-plus"></i></a></li>--}}
												{{--<li><a href="javascript:;" class="whatsapp" data-toggle="tooltip" data-trigger="hover" data-title="Whatsapp" data-placement="top"><i class="fab fa-whatsapp"></i></a></li>--}}
												{{--<li><a href="javascript:;" class="tumblr" data-toggle="tooltip" data-trigger="hover" data-title="Tumblr" data-placement="top"><i class="fab fa-tumblr"></i></a></li>--}}

												{{--<li><a href="javascript:;" class="tumblr" data-toggle="tooltip" data-trigger="hover" data-title="E-mail" data-placement="top"><i class="fa fa-envelope"></i></a></li>--}}


												{{-- ------------------- TEMPORALMENTE DESHABILITADO ---------------- --}}
												{{--<li><a href="javascript:;" class="facebook" data-toggle="tooltip" data-trigger="hover" data-title="Facebook" data-placement="top"><i class="fab fa-facebook"></i></a></li>--}}
												{{--<li>--}}
												{{--<div class="fb-share-button"--}}
												{{--data-href="https://www.expoindustri.com/producto/25"--}}
												{{--data-layout="button"--}}
												{{--data-size="small"--}}
												{{--data-mobile-iframe="false">--}}
												{{--<a target="_blank"--}}
												{{--href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww2.expoindustri.com%2Fproducto%2F25&amp;src=sdkpreparse"--}}
												{{--class="fb-xfbml-parse-ignore">Compartir_mz</a>--}}
												{{--</div>--}}
												{{--</li>--}}
												{{-- ------------------- TEMPORALMENTE DESHABILITADO END ---------------- --}}
												<li>
													<a id="btnRemoveFavorite" class="tumblr btn-danger @if($esFavorito != true) hide @endif" onclick="removeFavorite()" data-toggle="tooltip" data-title="@lang('messages.remove_favorite')" data-placement="top"><i class="fa fa-heart"></i> @lang('messages.remove_favorite')</a>
													<a id="btnAddFavorite" class="facebook @if($esFavorito == true) hide @endif" onclick="addFavorite(this)" data-toggle="tooltip" data-title="@lang('messages.add_favorites')" data-placement="top"><i class="fa fa-heart"></i></a>
												</li>
											</ul>
										</div>
										<!-- END product-social -->
									</div>
								@endif

								{{--<div class="product-discount">--}}
									{{--<span class="discount">{{ $pub->moneda }}869.00</span>--}}
								{{--</div>--}}


							</div>
						</div>
						<!-- END product-purchase-container -->

					</div>
					<!-- END product-info -->

				</div>


				<!-- END product-detail -->
				<!-- BEGIN product-tab -->

				<div class="product-tab m-t-0">
					<!-- BEGIN #product-tab -->
					<ul id="product-tab" class="nav nav-tabs">
						@if($global_isProvider == false)
						<li class="active"><a href="#product-calc" data-toggle="tab">@lang('messages.calculator')</a></li>
						@endif
						<li class="@if($global_isProvider == true) active @endif"><a href="#product-info" data-toggle="tab">@lang('messages.additional_info')</a></li>
						@if($global_isProvider == true)
						<li class=""><a href="#product-calc" data-toggle="tab">@lang('messages.calculator')</a></li>
						@endif
					</ul>
					<!-- END #product-tab -->
					<!-- BEGIN #product-tab-content -->
					<div id="product-tab-content" class="tab-content p-0">
						<!-- BEGIN #product-desc -->

						<div class="tab-pane fade @if($global_isProvider == false) active in @endif" id="product-calc">
							<!-- BEGIN product-desc -->

							<div class="account-container">
								<!-- BEGIN account-sidebar -->
								<div class="account-sidebar" style="-webkit-border-radius: 0px;-moz-border-radius: 0px;border-radius: 0px;">
									<div class="account-sidebar-cover">
										<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />
									</div>
									<div class="account-sidebar-content">
										{{--<h4>Calculadora</h4>--}}
										@lang('messages.calculator_text', ['form_link' => '<a href="'.route("public.{$glc}.contact").'" class="text-white">'.__('messages.contact_form').'</a>.'])

									</div>
								</div>

								<!-- END account-sidebar -->
								<!-- BEGIN account-body -->
								<div class="account-body">
									<!-- BEGIN row -->
									<div class="row">
										<!-- BEGIN col-6 -->
										<div class="col-md-12">
											<h4><i class="fa fa-angle-right fa-fw text-muted"></i> @lang('messages.quote_equipment_in_final_destination')</h4>
											<table class="table table-condensed table-stripped m-t-20">
												@if($global_isProvider == true && $pub->Producto->precioSudamerica == 1)
												@else




													{{-- ##################################### PRECIO DE LA MAQUINA EN EUROPA ##################################### --}}
													<tr style="background-color:#e8e8e8;">
														<td>

															<div class="checkbox checkbox-css /*panel panel-warning m-b-0*/">
																<div class="panel-body /*text-white*/" style="/*background-color:#12b3e9;*/ padding-left:0px;"><b>@lang('messages.price_machine_in_europe')</b></div>
															</div>

														</td>
														<td class="rcp">

															<div class="panel panel-warning m-b-0" style=" background:none;">
																{{--<div class="panel-body /*text-white*/" style="/*background-color:#12b3e9;*/"><b>{{ $pub->Producto->precioUnitario }} {{ strtoupper($pub->Producto->monedaUnitario) }} </b></div>--}}
																<div class="panel-body /*text-white*/" style="/*background-color:#12b3e9;*/ text-transform:uppercase; margin-top:10px;"><b>
																		{{ number_format($pub->Producto->precio_publicacion, 2, '.', ',') }} {{ session('current_currency', 'usd') }}
																	</b></div>
															</div>

														</td>
													</tr>
												@endif
													{{-- ##################################### REQUIERE INFORMACION ADICIONAL ##################################### --}}
												<tr>
													<td>
														<div class="checkbox checkbox-css">

															<b>@lang('messages.require_additional_info')</b> &nbsp;&nbsp;
															<a href="javascript:;" data-toggle="modal" data-target="#modalInfoAdicional"><i class="fa fa-info-circle fa-lg"></i></a>

															<br><br>
															<input type="checkbox" id="chkInformeTecnico" style="margin-left:0px" onclick="onInformeTecnico(this)">
															<label for="chkInformeTecnico">@lang('messages.technichal_information')
																{{--<br><small class="text-muted">@lang('messages.request_require_payment')</small>--}}
															</label>
														</div>
													</td>
													<td class="rcp"><br><br><span id="chk_inf_tecnico" class="hide" style="font-weight:bold; text-transform:uppercase;">{{ number_format($precio_informe, 2, '.', ',') }} {{ session('current_currency', 'usd') }}</span></td>
												</tr>

													{{-- ##################################### REQUIERE SEGURO EXTRA ##################################### --}}

												<tr style="background-color:#e8e8e8;">
													<td>
														<div class="checkbox checkbox-css">
															<b>@lang('messages.require_extra_assurance')</b>
															<a href="javascript:;" data-toggle="modal" data-target="#modalSeguro"><i class="fa fa-info-circle fa-lg"></i></a>
															<br><br>
															<input type="checkbox" id="chkSeguro" style="margin-left:0px" onclick="onSeguroExtra(this)">
															<label for="chkSeguro">@lang('messages.extra_insurance')</label>
														</div>
													</td>
													<td class="rcp"><br><br><span id="chk_inf_seguro" class="hide" style="font-weight:bold; text-transform:uppercase;">{{ number_format($precio_seguro, 2, '.', ',') }} {{ session('current_currency', 'usd') }}</span></td>
												</tr>

													{{-- ##################################### SELECCIONE DESTINO FINAL ##################################### --}}

												<tr>
													<td>
														<div class="checkbox checkbox-css">
															<b>@lang('messages.select_final_destination')</b>
															<br><br>
															{!! Form::select('selPais', $paises_destino->pluck('nombre', 'codigo'), [], [
															    'id' => 'selPais',
															    'class' => 'form-control',
															    'placeholder' => __('messages.select_country'),
															    'onchange' => 'onDestinoPais()',
															]) !!}
															<br>
															{!! Form::select('selPaisZona', [], [], [
															    'id' => 'selPaisZona',
															    'class' => 'form-control',
															    'placeholder' => __('messages.select_country_first'),
															]) !!}

														</div>
													</td>
													<td class="rcp"><br><br><br><br><span id="chk_inf_zone_old" class="hide" style="font-weight:bold; text-transform:uppercase;"> {{ session('current_currency', 'usd') }}</span></td>
												</tr>

													{{-- ##################################### DIRECCION SUECIA ##################################### --}}

												<tr class="direccion_suecia hide" style="background-color:#e8e8e8;">
													<td>
														<div class="checkbox checkbox-css">

															<div class="row">
																<div class="col-md-8">
																	<b>@lang('messages.address')</b>
																	{!! Form::text('se_direccion', null, [
																		'id' => 'se_direccion',
																		'class' => 'form-control',
																		'placeholder' => __('messages.address').', '.__('messages.zone'),
																	]) !!}
																</div>
																<div class="col-md-4">
																	<b>@lang('messages.postal_code')</b>
																	{!! Form::text('se_pcode', null, [
																		'id' => 'se_pcode',
																		'class' => 'form-control',
																		'placeholder' => __('messages.postal_code'),
																	]) !!}
																</div>
															</div>

														</div>
													</td>
													<td class="rcp"><br><br><br><br><span id="chk_inf_zone_old" class="hide" style="font-weight:bold; text-transform:uppercase;"> {{ session('current_currency', 'usd') }}</span></td>
												</tr>

													{{-- ##################################### METODOS DE TRANSPORTE ##################################### --}}
												<tr class="metodo_transporte" style="background-color:#e8e8e8;">
													<td>
														<div class="checkbox checkbox-css">
															<b>@lang('messages.how_transport_machine')</b>
															<p class="info_select_destination"><br>Please select a destination first</p>
															<table class="table table-condensed tableTransportation" style="background:none; margin-top:10px;">

																@foreach($transportesIndex as $transporte)
																	@if(isset($transportesPubList[$transporte->id]))

																		@if( $transportesPubList[$transporte->id]->pivot->disponibilidad == '' )
																			<tr class=" @if($transporte->esEspecial == 1) warning @endif">
																				<td>
																					<input type="radio" class="selTransporte" id="selTransporte_{{ $transporte->id }}" value="{{ $transporte->id }}" name="selTransport" style="margin-left:0px"  onclick="onTransporte({{ $transporte->id }})" >
																					<label for="selTransporte_{{ $transporte->id }}">{{ $transporte->Traduccion[0][$global_localization] }}
																						@if($transporte->esEspecial == 1)
																							<a href="javascript:;" data-toggle="tooltip" data-title="Servicio especial"><i class="fa fa-star" style="color: #ffd700;"></i></a>
																						@endif
																					</label>

																				</td>
																				<td>
																					<span class="spnDesc_{{ $transporte->id }} spnTiempo /*hide*/" >{{ $transporte->Traduccion->where('campo', 'descripcion')->first()[$global_localization] }}</span>
																				</td>
																			</tr>
																		@else
																			<tr class=" hide transporte_variable transporte_{{ $transporte->id }} @if($transporte->esEspecial == 1) warning @endif">
																				<td>
																					<input type="radio" class="selTransporte" id="selTransporte_{{ $transporte->id }}" value="{{ $transporte->id }}" name="selTransport" style="margin-left:0px"  onclick="onTransporte({{ $transporte->id }})" >
																					<label for="selTransporte_{{ $transporte->id }}">{{ $transporte->Traduccion[0][$global_localization] }}
																						@if($transporte->esEspecial == 1)
																							<a href="javascript:;" data-toggle="tooltip" data-title="Servicio especial"><i class="fa fa-star" style="color: #ffd700;"></i></a>
																						@endif
																					</label>

																				</td>
																				<td>
																					<span class="spnDesc_{{ $transporte->id }} spnTiempo /**/" >{{ $transporte->Traduccion->where('campo', 'descripcion')->first()[$global_localization] }}</span>
																				</td>
																			</tr>
																		@endif

																	@endif
																@endforeach
															</table>
														</div>
													</td>
													<td class="rcp">
														<br>
														<br>
														<table class="m-t-10 table-condensed" style="width:100%;">

															@foreach($transportesIndex as $transporte)

																@if(isset($transportesPub[$transporte->id]))
																	<tr>
																		<td class="p-l-0 text-center">
																			<span class="spnVal_{{ $transporte->id }} spnVal hide" ><b>{{ (isset($transportesPub[$transporte->id])) ? number_format($transportesPub[$transporte->id], 2, '.', ',') : 0 }} {{ strtoupper(session('current_currency', 'usd')) }}</b></span>&nbsp;
																		</td>
																	</tr>
																@endif

															@endforeach

														</table>
													</td>
												</tr>
													{{-- ##################################### RESUMEN DE COSTOS ##################################### --}}
												<tr class="resumen_costos">
													<td>
														<div class="checkbox checkbox-css">
															<b style=" line-height:3;">@lang('messages.transport_terrain')</b><br>
															<b style=" line-height:3;">@lang('messages.transport_sea')</b>
														</div>
													</td>
													<td class="rcp">
														{{--<span id="chk_transport_terrain" class="/*hide*/" style="font-weight:bold; text-transform:uppercase; line-height:3;">0.00</span>--}}
														<span id="chk_inf_zone" class="/*hide*/" style="font-weight:bold; text-transform:uppercase; line-height:3;">0.00</span><br>
														<span id="chk_transport_sea" class="/*hide*/" style="font-weight:bold; text-transform:uppercase; line-height:3;">0.00</span>


													</td>
												</tr>
													{{-- ##################################### COSTOS OPERATIVOS COMPLEMENTARIOS ##################################### --}}
												<tr style="background-color:#e8e8e8;">
													<td>
														<div class="checkbox checkbox-css">
															<b>@lang('messages.handling_fee')</b>
															<a href="javascript:;" data-toggle="modal" data-target="#modalHandling"><i class="fa fa-info-circle fa-lg"></i></a>
														</div>
													</td>
													<td class="rcp"><span id="chk_inf_handling" class="/*hide*/" style="font-weight:bold; text-transform:uppercase; line-height:3;">{{ number_format($tasa_tramite, 2, '.', ',') }} {{ session('current_currency', 'usd') }}</span></td>
												</tr>
													{{-- ##################################### COSTOS FINAL ##################################### --}}
												<tr class="costo_final">
													<td>
														<div class="checkbox checkbox-css">
															<h4>@lang('messages.price_in_final_destiny')</h4>
														</div>
													</td>
													@if($global_isProvider == true && $pub->Producto->precioSudamerica == 1)
														<td>
															<div class="checkbox checkbox-css">
															<h4 class="text-center" id="precio_final" style="font-weight:bold; text-transform:uppercase;">
																0
																{{ session('current_currency', 'usd') }}
															</h4>
															</div>
														</td>
													@else
														<td>
															<div class="checkbox checkbox-css">
															<h4 class="text-center" id="precio_final" style="font-weight:bold; text-transform:uppercase;">
																{{ number_format($pub->Producto->precio_publicacion, 2, '.', ',') }}
																{{ session('current_currency', 'usd') }}
															</h4>
															</div>
														</td>
													@endif

												</tr>
											</table>
											@if($global_isProvider == true && $pub->Producto->precioSudamerica == 1)
												<div class="row">
													<div class="col-md-12 text-center">
														<a href="javascript:;" onclick="modalContactConsultant()" class="btn btn-lg btn-danger"><i class="fa fa-paper-plane"></i> @lang('messages.contact_consultant')</a>
														{{--<a href="javascript:;" id="btnRequestQuote" class="btn btn-inverse btn-lg" onclick="enviarCotizacion()"><i class="fa fa-paper-plane"></i> @lang('messages.request_quote')</a>--}}
													</div>
												</div>
											@else
												<div class="row hide" id="creditCardInfo">
													<div class="col-md-12">
														<div style="background-color:#e8e8e8; margin-bottom:10px;">
															<style>
																/**
	 * The CSS shown here will not be introduced in the Quickstart guide, but shows
	 * how you can use CSS to style your Element's container.
	 */
																.StripeElement {
																	background-color: white;
																	height: 35px;
																	font-size:14px;
																	padding: 10px 8px;
																	margin-bottom:5px;
																	border-radius: 4px;
																	border: 1px solid #ccd0d4;
																	box-shadow: 0 1px 3px 0 #e6ebf1;
																	-webkit-transition: box-shadow 150ms ease;
																	transition: box-shadow 150ms ease;
																}

																.StripeElement--focus {
																	box-shadow: 0 1px 3px 0 #cfd7df;
																}

																.StripeElement--invalid {
																	border-color: #fa755a;
																}

																.StripeElement--webkit-autofill {
																	background-color: #fefde5 !important;
																}
															</style>
															<form action="/charge" method="post" id="payment-form">
																<div class="form-row">
																	<div class="row">
																		<div class="pull-left" style="">
																			<p style="margin:20px 20px;">@lang('messages.provide_card_info')</p>
																			<p style="margin:20px 20px; margin-bottom:10px;">@lang('messages.charges_technical_report')</p>
																		</div>

																		<div class="pull-right p-r-30">
																			<img style="height:70px;" src="/assets/img/e-commerce/visa.png" alt="Visa">
																			<img style="height:70px;" src="/assets/img/e-commerce/mastercard.png" alt="MasterCard">
																		</div>
																	</div>

																	<div class="card-message hide m-10"></div>
																	{{--<label for="card-element">--}}
																	{{--Número de tarjeta--}}
																	{{--</label>--}}
																	{{--<div id="card-element">--}}
																	{{--<!-- A Stripe Element will be inserted here. -->--}}
																	{{--</div>--}}


																	<div class="form-group col-md-12">
																		<label class="control-label" for="card-element">
																			@lang('messages.card_number')
																		</label>
																		<div id="card-number"></div>
																	</div>

																	<div class="form-group col-md-6">
																		<label class="control-labFecha de expiraciónel" for="card-element">
																			@lang('messages.expiration_date')
																		</label>
																		<div id="card-expiry"></div>
																	</div>
																	<div class="form-group col-md-6">
																		<label class="control-label" for="card-element">
																			@lang('messages.security_code')
																		</label>
																		<div id="card-cvc"></div>
																	</div>
																</div>
																<br>
																{{--<button id="submitPayment" class="btn btn-primary btn-inverse"><i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')</button>--}}
															</form>
															<div style="clear: both;"></div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 text-center">
														<a href="javascript:;" id="btnRequestQuote" class="btn btn-inverse btn-lg" onclick="enviarCotizacion()"><i class="fa fa-paper-plane"></i> @lang('messages.request_quote')</a>
													</div>
												</div>
											@endif




										</div>
										<!-- END col-6 -->

									</div>
									<!-- END row -->
									<br><br><br>

									<div class="row">
										<!-- BEGIN about-us-content -->
										<div class="about-us-content">

											<!-- BEGIN row -->
											<div class="row">

												<!-- begin col-4 -->
												<div class="col-md-4 col-sm-4">
													<div class="service">
														<div class="icon text-primary"><i class="fas fa-lock"></i></div>
														<div class="info">
															<h4 class="title">@lang('messages.how_guarantee_buy')</h4>
															{{--<p class="desc">--}}
																{{--<a href="#modal-proceso" data-toggle="modal">@lang('messages.see_more')...</a>--}}
															{{--</p>--}}
														</div>
													</div>
												</div>
												<!-- end col-4 -->
												<!-- begin col-4 -->
												<div class="col-md-4 col-sm-4">
													<div class="service">
														<div class="icon text-info"><i class="fas fa-money-bill-alt"></i></div>
														<div class="info">
															<h4 class="title">@lang('messages.is_secure_payment')</h4>
															{{--<p class="desc">--}}
																{{--Porque su tranquilidad es lo más importante, proteja su inversión con nosotros.--}}
																{{--<a href="#modal-pagoseguro" data-toggle="modal">@lang('messages.see_more')...</a> --}}
															{{--</p>--}}
														</div>
													</div>
												</div>
												<!-- end col-4 -->
												<!-- begin col-4 -->
												<div class="col-md-4 col-sm-4">
													<div class="service">
														<div class="icon text-danger"><i class="fas fa-comments"></i></div>
														<div class="info">
															{{--<h4 class="title">¿Dudas? Hable con nosotros</h4>--}}
{{--															<h4 class="title">@lang('messages.doubts_contact_us')</h4>--}}
															<h4 class="title"><a href="javascript:;" onclick="modalContactConsultant()" data-toggle="modal">@lang('messages.make_an_offer')</a></h4>
															<p class="desc" hidden>
																{{--Nuestros esfuerzos para el presente y el futuro.--}}
																{{--<a href="{{ route("public.{$glc}.contact") }}" data-toggle="modal">@lang('messages.go_to_contact').</a></p>--}}
																<a href="javascript:;" onclick="modalContactConsultant()" data-toggle="modal">@lang('messages.go_to_contact').</a></p>
														</div>
													</div>
												</div>
												<!-- end col-4 -->
											</div>
											<!-- END row -->
										</div>
										<!-- END about-us-content -->
									</div>


								</div>

								<!-- END account-body -->
							</div>

							<!-- END product-desc -->
						</div>

						<!-- END #product-desc -->
						<!-- BEGIN #product-info -->
						<div class="tab-pane fade p-40 @if($global_isProvider == true) active in @endif" id="product-info">
							<!-- BEGIN table-responsive -->
							<div class="table-responsive">
								<!-- BEGIN table-product -->
								<table class="table table-product table-striped">
									<thead>
									<tr>
										<th></th>
										<th></th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									@foreach($secondaryData as $data)
										@if($data['type'] == 'boolean' && $data['value'] == 'none')
											@continue
										@endif
										<tr>
											<td class="field">@lang('form.'.$data['key'])</td>
											<td colspan="2">
												@if($data['type'] == 'select')
													@lang('form.'.$data['value']) {{ $data['compliment'] }}

													@if($data['key'] == 'calificacion_general')
														&nbsp;
														@if($data['value'] == 'grating_veryGood')
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
														@elseif($data['value'] == 'grating_good')
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
														@elseif($data['value'] == 'grating_regular')
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
														@elseif($data['value'] == 'grating_bad')
															<i class="fa fa-star" style="color: #ffd700;"></i>
															<i class="fa fa-star" style="color: #ffd700;"></i>
														@endif
													@endif

												@elseif($data['type'] == 'multiselect')
													<ul class="fa-ul">
														@foreach($data['value'] as $val)
															<li>
																<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
																@lang('form.'.$val)
															</li>
														@endforeach
													</ul>
												@elseif($data['type'] == 'range_year')
													{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
												@elseif($data['type'] == 'boolean')
													@lang('form.'.$data['value'])
												@else
													@if($data['key'] == 'pais_de_fabricacion')
														@php
															if($data['value'] == 'usa') {
																$data['value'] = 'us';
															}
														@endphp
														{{ $global_paises[$data['value']] }} ({{ strtoupper($data['value']) }})
													@else
														{{ $data['value'] }} {{ $data['compliment'] }}
													@endif

												@endif
											</td>
										</tr>
									@endforeach

									</tbody>
								</table>
								<!-- END table-product -->
							</div>
							<!-- END table-responsive -->
						</div>
						<!-- END #product-info -->
					</div>
					<!-- END #product-tab-content -->
				</div>
				<!-- END product-tab -->
			</div>
			<!-- END product -->

			<div class="modal fade" id="modal-proceso">
				<div class="modal-dialog" style="width:760px !important;">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" style="display: inline;">@lang('messages.how_guarantee_buy')</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							@lang('messages.guarantee_content')
						</div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-pagoseguro">
				<div class="modal-dialog" style="width:760px !important;">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" style="display: inline;">@lang('messages.is_secure_payment')</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							@lang('messages.is_secure_payment_content')
						</div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- END container -->
	</div>
	<!-- END #product -->

	<div id="product" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container" id="relatedItemsContainer">

		</div>
	</div>

	<div class="modal fade" id="modalInfoAdicional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">@lang('messages.technichal_information')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div>
						@lang('messages.technical_info_company')
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('messages.close')</button>
					{{--<button type="button" class="btn btn-primary">Save changes</button>--}}
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalSeguro" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">@lang('messages.insurande_for_machine')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div>
						@lang('messages.soon_at_expoindustri')
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('messages.close')</button>
					{{--<button type="button" class="btn btn-primary">Save changes</button>--}}
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalHandling" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">@lang('messages.handling_fee')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div>
						@lang('messages.handling_fee_description')
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('messages.close')</button>
					{{--<button type="button" class="btn btn-primary">Save changes</button>--}}
				</div>
			</div>
		</div>
	</div>

	{{-- MODAL PRODUCT CONTACT CONSULTANT --}}

	<div class="modal fade" id="modalContactConsultant">
		<div class="modal-dialog" >
			<div class="modal-content" style="margin-top:50px;">
				<div class="modal-body">
					<div class="row">
						<div class="col col-md-12">
							<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
								<h4 class="text-center m-t-0">@lang('messages.product_contact_title')</h4>
							</div>
							<p class="m-t-10">
								@lang('messages.product_contact_message', ['product_name' => $pub->Producto->Marca->nombre.' '.$pub->Producto->Modelo->nombre])
							</p>

							<table style="margin:0px; padding:0px; width:100%;">
								<tr>
									<td style="width:50%; text-align:center;">
										@if(count($pub->Producto->Galerias) > 0 && count($pub->Producto->Galerias[0]->Imagenes) > 0)
											<div data-p="170.00" style=" background-color:#000000; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;">
												<img data-u="image" src="{{ $pub->Producto->Galerias[0]->Imagenes[0]->ruta_publica_producto_thumb }}" style="max-width:200px; max-height:200px;" />
											</div>
										@endif
									</td>
									<td style="vertical-align:top; width:50%;">
										<ul class="fa-ul">
											@foreach($primaryData as $data)
												<li>
													<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
													<b>@lang('form.'.$data['key'])</b>:
													@if($data['type'] == 'select')
														@lang('form.'.$data['value'])
													@elseif($data['type'] == 'multiselect')
														<ul class="fa-ul">
															@php
																if(!is_array($data['value'])){
																	$data['value'] = [$data['value']];
																}
															@endphp
															@foreach($data['value'] as $val)
																<li>
																	<span class="fa-li"><i class="fas fa-chevron-circle-right"></i></span>
																	@lang('form.'.$val)
																</li>
															@endforeach
														</ul>
													@elseif($data['type'] == 'range_year')
														{{ ($data['value'] == 'unknown_year') ? __('form.'.$data['value']) : $data['value'] }}
													@elseif($data['type'] == 'boolean')
														@lang('form.'.$data['value'])
													@else
														{{ $data['value'] }}
													@endif
												</li>
											@endforeach
										</ul>
									</td>
								</tr>
							</table>
							<hr>
						</div>
					</div>
					<div class="row">
						{{--<div class="col-md-6">--}}
						{{--<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">--}}
						{{--<h4 class="text-center m-t-0">Publicar una maquina</h4>--}}
						{{--</div>--}}

						{{--<div class="text-center" style="margin-top:100px;">--}}
						{{--@if(App::getLocale() == 'se')--}}
						{{--<a href="/publish"><img src="/assets/img/e-commerce/publish_machine2_se.png" alt="Publish a machine"></a>--}}
						{{--@else--}}
						{{--<a href="/publish"><img src="/assets/img/e-commerce/publish_machine2.png" alt="Publish a machine"></a>--}}
						{{--@endif--}}
						{{--</div>--}}
						{{--</div>--}}
						<div class="col-md-12" style="border-left:1px dashed #e5e5e5 !important;">

						<span id="sqError_messages" class="hide">
							<p class="alert alert-danger">@lang('messages.marked_fields_required')</p>
						</span>


							<form id="formContactConsultant"  action="" method="post" class="text-center">

								<div class="row">
									<div class="col col-md-6">

										@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
											<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')">
											<div id="sqError_apellidos"></div>
										@else
											<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->apellidos }}" disabled>
										@endif

										<input type="hidden" id="sqTipoConsulta" value="product_contact_consultant">

										@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
											<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" >
											<div id="sqError_nombres"></div>
										@else
											<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->nombres }}" disabled >
										@endif

										@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
											<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
											<div id="sqError_email"></div>
										@else
											<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->username }}" disabled >
										@endif


										<style>

										</style>
										{{--<select name="pais" id="sqPais" class="login-input">--}}
										{{--<option value="">@lang('messages.select_country')</option>--}}
										{{--</select>--}}

										@if($auth_nombres_apellidos == '')
											{!! Form::select('pais', $global_paises, $global_country_abr, [
												'id' => 'sqPais',
												'class' => 'login-input',
												'placeholder' => __('messages.select_country'),
												'style' => 'width:90% !important;  margin-top:10px;'
											]); !!}
											<div id="adPubError_pais"></div>
										@else
											{!! Form::select('pais', $global_paises, \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->pais, [
												'id' => 'sqPais',
												'class' => 'login-input',
												'placeholder' => __('messages.select_country'),
												'style' => 'width:90% !important; margin-top:10px;',
												'disabled'
											]); !!}
										@endif

										@if($auth_nombres_apellidos == '')
											<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
											<div id="sqError_telefono"></div>
										@else
											<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')"
											       value="{{ (count(\Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos)) ? \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos->first()->numero : '' }}">
										@endif

									</div>
									<div class="col col-md-6">
										<textarea class="login-input form-control" id="sqDescripcion" style="width:95%; height:193px;" placeholder="@lang('messages.comments')"></textarea>
									</div>
								</div>


								{{--<input type="submit" id="adPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
								<a id="btnSearchQuestion" href="javascript:;"  class="promotion-btn m-t-20 m-b-10" onclick="guardarContactoConsulta()" style="width:50%; font-size:16px;">@lang('messages.send')</a>
								{{--<a href="javascript:;" id="adPubBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@push('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="/assets/js/jssor.slider-27.1.0.min.js"></script>

<script src="/assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="/assets/plugins/lightbox2/js/lightbox.min.js"></script>

<script>

	var elements = '';
	var ccard = '';
	var ncard = '';
	var cecard = '';
	var cvcard = '';
	var stripe = '';


	@if( $enableStripe )

	$.getScript('https://js.stripe.com/v3/').done(function() {
		// Create a Stripe client.
		stripe = Stripe('{{ env('STRIPE_KEY_PK') }}');    // JB

		// Create an instance of Elements.
		elements = stripe.elements();

		// Custom styling can be passed to options when creating an Element.
		// (Note that this demo uses a wider set of styles than the guide below.)
		var style = {
			base: {
				color: '#32325d',
				lineHeight: '18px',
				fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing: 'antialiased',
				fontSize: '16px',
				'::placeholder': {
					color: '#aab7c4'
				}
			},
			invalid: {
				color: '#fa755a',
				iconColor: '#fa755a'
			}
		};

		// Create an instance of the card Element.
//		ccard = elements.create('card', {style: style});
		ncard = elements.create('cardNumber', {style: style});
		cecard = elements.create('cardExpiry', {style: style});
		cvcard = elements.create('cardCvc', {style: style});

		// Add an instance of the card Element into the `card-element` <div>.
//		ccard.mount('#card-element');
		ncard.mount('#card-number');
		cecard.mount('#card-expiry');
		cvcard.mount('#card-cvc');
	});

	@endif

	function stripeTokenHandler(token) {
		// Insert the token ID into the form so it gets submitted to the server
		var form = document.getElementById('payment-form');
		var hiddenInput = document.createElement('input');
		hiddenInput.setAttribute('type', 'hidden');
		hiddenInput.setAttribute('name', 'stripeToken');
		hiddenInput.setAttribute('value', token.id);
		form.appendChild(hiddenInput);

		// Submit the form
		form.submit();
	}

// ---------- PAISES Y ZONAS

	var destinos = {!! json_encode($paises_destino) !!};
	var destinosIndex_old = {!! json_encode($destinosIndex) !!};
	var destinosIndex = {
	@foreach($destinosIndex as $di)
		destino_{{ $di->id }}: {{ $di->precio }},
	@endforeach
	};

	var zones = {!! json_encode($destinos) !!};

	$(document).ready(function(){



//		initJSORCarousel();

		// ---------- POPOVERS
		$('[data-toggle="popover"]').popover();

		// ---------- SHARE BUTTONS

		$('#btnShareFacebook').click(function(e){
			FB.ui({
				method: 'share',
				href: 'https://www2.expoindustri.com/producto/{{ $pub->id }}',
			}, function(response){});
		})

		// ---------- TRANSPORTE
		$('.tableTransportation input').change(function(e){
//			$('.spnVal, .spnTiempo').addClass('hide');
//			var t = $(this).val();
//			$('.spnDesc_'+t+', .spnVal_'+t).removeClass('hide');
		});

		// ---------- PAIS -> ZONA
		{{--@if(isset($esquema))--}}
			{{--var zones = {!! json_encode($esquema->zonasPrecio) !!};--}}
		{{--@endif--}}

//		$.each(zones, function(k,v){
//			$('#selPais').append($('<option>', {
//				'value': k
//			}).html(v.text));
//		});

		$('#selPais').change(function(e){

			var pref = $(this).val();
			if(pref !== ''){
				var subs = eval('zones.'+$(this).val() + '.subs');

				$('#selPaisZona').html('');
				$('#selPaisZona').append($('<option>', {
					'value': ''
				}).html("@lang('messages.select_preferred_zone')"));

				$.each(subs, function(k,v){
					$('#selPaisZona').append($('<option>', {
						'value': v.value
					}).html(v.text));
				});

			} else {
				$('#selPaisZona').html('');
				$('#selPaisZona').append($('<option>', {
					'value': ''
				})).html("@lang('messages.select_country_first')");
			}

			$('#chk_inf_zone').html('0.00 {{ strtoupper(session('current_currency', 'usd')) }}');
			$('#chk_transport_sea').html('0.00 {{ strtoupper(session('current_currency', 'usd')) }}');

			if( $(this).val() === 'se' ){
				$('#selPaisZona').val(9);
			}

			@foreach($destinosIndex as $di)
				destino_{{ $di->id }} = false;
			@endforeach
			@foreach($transportesPub as $id => $price)
				valTrans_{{ $id }} = false;
			@endforeach
			_idDestino = 0;
			actualizarPrecio();

			// SHOW / HIDE tipos de transporte disponibles ----------------------------
			$('.transporte_variable').addClass('hide');
			$('.selTransporte').prop('checked', false);
		});


		var disponibilidadTransporte = {};

		@foreach( $transportesPubList as $id => $transporte )
			@if( $transporte->pivot->disponibilidad != '' )
			disponibilidadTransporte[{{ $transporte->id }}] = {!! $transporte->pivot->disponibilidad !!};
			@endif
		@endforeach

		$('#selPaisZona').change(function(e){

			$('.info_select_destination').addClass('hide');

			for(var i = 1 ; i < 7 ; i++){
				eval('destino_' + i + '= false');
			}

			@foreach($transportesPub as $id => $price)
				valTrans_{{ $id }} = false;
			@endforeach

			var chk_transport_sea = $('#chk_transport_sea');
				chk_transport_sea.html('0.00');

			var chk_inf_zone = $('#chk_inf_zone');
			if($(this).val() !== '') {
				chk_inf_zone.removeClass('hide');
				//chk_inf_zone.html($(this).val() + ' USD');
//				eval($(this).val() + '= true');
				eval('destino_' + $(this).val() + '= true');
				_idDestino = $(this).val();
				var subs = eval('zones.'+ $('#selPais').val() + '.subs');
				var precio_format = eval('precio_' + $('#selPaisZona').val());
				precio_format = precio_format.toFixed(2);
				precio_format = getNumberFormat(precio_format);
				chk_inf_zone.html( precio_format + ' {{ strtoupper(session('current_currency', 'usd')) }}');
//				$.each(subs, function(k,v){
//					if(v.value == $('#selPaisZona').val()){
////						chk_inf_zone.html(v.price + ' USD');
//						chk_inf_zone.html( eval('precio_' + v.val) + ' USD');
//					}
//				});
				actualizarPrecio();
			}else {
				chk_inf_zone.addClass('hide');
				chk_inf_zone.html('0.00');
				chk_transport_sea.html('0.00');
				_idDestino = 0;
				@foreach($destinosIndex as $di)
					destino_{{ $di->id }} = false;
				@endforeach
				actualizarPrecio();
			}

			// SHOW / HIDE tipos de transporte disponibles ----------------------------

			destinoId = $(this).val();

			$('.selTransporte').prop('checked', false);

			$.each(disponibilidadTransporte, function(k,v) {
				if(v.indexOf( parseInt(destinoId, 10) ) >= 0){
					$('.transporte_' + k).removeClass('hide');
				} else {
					$('.transporte_' + k).addClass('hide');
				}
			})

		});

		initRelatedItems();

		// ----------

		if($('.selTransporte').length === 1) {
			$('.selTransporte').each(function(index) {
				$(this).attr('checked', 'checked');
				onTransporte($(this).val());
			} );
		}

		actualizarPrecio();
	});

	function initJSORCarousel() {
		var jssor_1_SlideshowTransitions = [
			{$Duration:800,$Opacity:2}
		];

		var jssor_1_options = {
			$AutoPlay: 1,
			$SlideEasing: $Jease$.$OutQuint,
//			$FillMode: 5,
			$FillMode: 1,
			$PlayOrientation: 1,
			$SlideshowOptions: {
				$Class: $JssorSlideshowRunner$,
				$Transitions: jssor_1_SlideshowTransitions,
				$TransitionsOrder: 1
			},
			$ArrowNavigatorOptions: {
				$Class: $JssorArrowNavigator$
			},
			$BulletNavigatorOptions: {
				$Class: $JssorBulletNavigator$
			}
		};

		var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		/*#region responsive code begin*/

		var MAX_WIDTH = 600;

		function ScaleSlider() {
			var containerElement = jssor_1_slider.$Elmt.parentNode;
			var containerWidth = containerElement.clientWidth;

			if (containerWidth) {

				var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

				jssor_1_slider.$ScaleWidth(expectedWidth);
			}
			else {
				window.setTimeout(ScaleSlider, 30);
			}
		}

		ScaleSlider();

		$(window).bind("load", ScaleSlider);
		$(window).bind("resize", ScaleSlider);
		$(window).bind("orientationchange", ScaleSlider);
	}

	/**
	 * Carga los Productos relacionados en la parte inferior de la pagina.
	 */
	function initRelatedItems(){
		relatedItems();
		{{--var url = '{{ route('component.relatedItems', ['cat' => $pub->Producto->Categorias[0]->Padre]) }}';--}}
		{{--$('#relatedItemsContainer').load(url);--}}
	}

	var esUsuario = false;
	// ---------- FAVORITOS
	function addFavorite(){

		@if(Auth::guard('clientes')->user())
			esUsuario = true;
		@endif

		if(esUsuario === true){
			var id = {{ $pub->id }};
			var url = '{{ route('public.addFavorite') }}';
			var data = {
				idPublicacion: {{ $pub->id }}
			};
			ajaxPost(url, data, {
				onSuccess: function(data){
					$('#btnAddFavorite').addClass('hide');
					$('#btnRemoveFavorite').removeClass('hide');
					swal("", "@lang('messages.added_favorites')", "success");
				}
			});
		} else {
			setModalHandler('loginRegister:loginSuccessful', function(){
				esUsuario = true;
				$('#modalLoginRegister').modal('hide');
				addFavorite();
			});
			launchLoginRegister('login');
		}
	}

	/**
	 * Elimina el producto del listado de productos favoritos
	 */
	function removeFavorite(){

		@if(Auth::guard('clientes')->user())
			esUsuario = true;
		@endif

		if(esUsuario === true){
			var id = {{ $pub->id }};
			var url = '{{ route('public.removeFavorite') }}';
			var data = {
				idPublicacion: {{ $pub->id }}
			};
			ajaxPost(url, data, {
				onSuccess: function(data){
					$('#btnAddFavorite').removeClass('hide');
					$('#btnRemoveFavorite').addClass('hide');
				}
			});
		} else {
			alert('There are not active users.');
		}
	}

	function mostrarCalculadora(){
		$('#product-tab a[href="#product-calc"]').tab('show')
	}

	var esUsuarioCot = false;

	/**
	 * Btn: Crear pedido
	 * Inicia el registro de la orden (cotización)
	 */
	function enviarCotizacion(){
		@if(Auth::guard('clientes')->user())
			esUsuarioCot = true;
		@endif

		$('#btnRequestQuote')
			.html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...')
			.attr('disabled', 'disabled')
			.attr('onclick', '');

		if(esUsuarioCot === true){

			{{--if($('#chkInformeTecnico').prop('checked')){--}}
				{{--stripe.createToken(ncard).then(function(result) {--}}
					{{--console.log(result);--}}
					{{--if(result.error !== undefined){--}}
						{{--$('.card-message')--}}
							{{--.html('<div class="alert alert-danger fade show in m-b-0">@lang('messages.verify_ccard')</div>')--}}
							{{--.removeClass('hide');--}}
						{{--$('#btnPublishSendForm')--}}
							{{--.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.publish_pay')')--}}
							{{--.removeAttr('disabled')--}}
							{{--.attr('onclick', 'sendForm()');--}}

						{{--$('#btnRequestQuote')--}}
							{{--.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.request_quote')')--}}
							{{--.removeAttr('disabled')--}}
							{{--.attr('onclick', 'enviarCotizacion()');--}}
					{{--} else {--}}
{{--//						crearPublicacion(result.token.id);--}}
						{{--$('#btnRequestQuote')--}}
							{{--.html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...')--}}
							{{--.attr('disabled', 'disabled')--}}
							{{--.attr('onclick', '');--}}

						{{--enviarCotizacionResponse(result.token.id);--}}
					{{--}--}}
					{{--// Handle result.error or result.token--}}
				{{--});--}}
			{{--} else {--}}


				enviarCotizacionResponse();
//			}
		} else {
			setModalHandler('loginRegister:loginSuccessful', function(){
				esUsuarioCot = true;
				$('#modalLoginRegister').modal('hide');
				enviarCotizacion();
			});
			setModalHandler('modal:hidden', function(){
				$('#btnRequestQuote')
					.html('<i class="far fa-lg fa-fw m-r-10 fa-paper-plane"></i> @lang('messages.request_quote')')
					.removeAttr('disabled')
					.attr('onclick', 'enviarCotizacion()');
			});
			launchLoginRegister('login');
		}
	}

	function enviarCotizacionResponse(idPayment){
		if(idPayment === undefined){
			idPayment = 0;
		}

		var zone = $('#selPaisZona').val();
		if(zone === '' || zone === '0'){
			swal("Oops!", "@lang('messages.select_preferred_zone')", "error");
			$('#btnRequestQuote')
				.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.request_quote')')
				.removeAttr('disabled')
				.attr('onclick', 'enviarCotizacion()');
			return false;
		}

		@php
			$filtro = [];
			foreach($transportesPub as $id => $price){
				$filtro[] = "valTrans_{$id} === false";
			}
		@endphp

		@if(count($transportesPub) > 0)

		var country_code = $('#selPais').val();
		if( country_code !== 'se' ) {
			if(
				{!! implode(' && ', $filtro) !!}
			){
				swal("Oops!", "@lang('messages.select_preferred_transport')", "error");
				$('#btnRequestQuote')
					.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.request_quote')')
					.removeAttr('disabled')
					.attr('onclick', 'enviarCotizacion()');
				return false;
			}
		}

		@endif

		var url = '{{ route('public.enviarCotizacion') }}';
		var data = {
			id: {{ $pub->id }},
			tokenPayment: idPayment,
			informe_tecnico : informe_tecnico,
			seguro_extra : seguro_extra,
			contenedor_compartido : contenedor_compartido,
			contenedor_exclusivo : contenedor_exclusivo,
			envio_abierto : envio_abierto,
			se_direccion : $('#se_direccion').val(),
			se_pcode : $('#se_pcode').val(),
			@foreach($transportesIndex as $transporte)
				@if(isset($transportesPub[$transporte->id]))
					transporte_{{ $transporte->id }} : valTrans_{{ $transporte->id }},
				@endif
			@endforeach
			@foreach($destinosIndex as $di)
				destino_{{ $di->id }} : destino_{{ $di->id }},
			@endforeach
			idDestino : $('#selPaisZona').val(),
		};
		ajaxPost(url, data,{
			silent: false,
			onSuccess: function(data){
				swal("Success!", "@lang('messages.quote_requested_correctly')", "success").then( function(value) {
					redirect('{{ route("public.{$glc}.search", [$pub->Producto->Categorias[0]->idPadre]) }}');
				});
				$('#btnRequestQuote')
					.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.request_quote')')
					.removeAttr('disabled')
					.attr('onclick', 'enviarCotizacion()');
			},
			onError: function(e){

				swal('@lang('messages.internal_error_title')', '@lang('messages.internal_error_message')', 'error');

				$('#btnRequestQuote')
					.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.request_quote')')
					.removeAttr('disabled')
					.attr('onclick', 'enviarCotizacion()');
			},
			onValidation: function(e){
				$('#btnRequestQuote')
					.html('<i class="far fa-lg fa-fw m-r-10 fa-check-circle"></i> @lang('messages.request_quote')')
					.removeAttr('disabled')
					.attr('onclick', 'enviarCotizacion()');
			}
		});
	}

	var informe_tecnico = false;
	var seguro_extra = false;
	var contenedor_compartido = false;
	var contenedor_exclusivo = false;
	var envio_abierto = false;

	@foreach($transportesIndex as $transporte)
		@if(isset($transportesPub[$transporte->id]))
			var valTrans_{{ $transporte->id }} = false;
		@endif
	@endforeach
	@foreach($destinosIndex as $di)
		var destino_{{ $di->id }} = false;
	@endforeach

	@foreach($destinosIndex as $di)
		var precio_{{ $di->id }} = {{ (isset($destinosPub[$di->id])) ? $destinosPub[$di->id] : 0 }};
	@endforeach

	var _idDestino = 0;


//	$('img').attr('onerror', 'img_error(this);');
	function img_error(img){

//		console.log("eror img " + $(img).prop('src'));
//		var nurl = $(img).prop('src');
//		nurl = nurl.replace('www2', 'www');
//		nurl = nurl.replace('expoindustriv2', 'expoindustri.com');
//		$(img).prop('src', nurl);
	}


	function onInformeTecnico(e){
		if(e.checked){
//			$('#creditCardInfo').removeClass('hide');
			$('#chk_inf_tecnico').removeClass('hide');
			informe_tecnico = true;
		} else {
//			$('#creditCardInfo').addClass('hide');
			$('#chk_inf_tecnico').addClass('hide');
			informe_tecnico = false;
		}
		actualizarPrecio();
	}

	function onSeguroExtra(e){
		if(e.checked){
			$('#chk_inf_seguro').removeClass('hide');
			seguro_extra = true;
		} else {
			$('#chk_inf_seguro').addClass('hide');
			seguro_extra = false;
		}
		actualizarPrecio();
	}

	function onDestinoPais() {
		var id = $('#selPais').val();

		// ----------
		if( id === 'se' ) {
			$('.resumen_costos').addClass('hide');
			$('.metodo_transporte').addClass('hide');
			$('.direccion_suecia').removeClass('hide');
			$('#selPaisZona').addClass('hide');
			$('.costo_final').addClass('hide');
		} else {
			$('.resumen_costos').removeClass('hide');
			$('.metodo_transporte').removeClass('hide');
			$('.direccion_suecia').addClass('hide');
			$('#selPaisZona').removeClass('hide');
			$('.costo_final').removeClass('hide');
		}
		// ----------
	}

	function onTransporte(id){
//		$('.spnVal, .spnTiempo').addClass('hide');
//		$('.spnDesc_'+id+', .spnVal_'+id).removeClass('hide');

		var html = $('.spnVal_'+id).html().trim();
		$('#chk_transport_sea').html( (html === '') ? '0.00' : html );

		@foreach($transportesIndex as $transporte)
			@if(isset($transportesPub[$transporte->id]))
				valTrans_{{ $transporte->id }} = false;
			@endif
		@endforeach

		eval('valTrans_'+id+' = true');
		actualizarPrecio();
	}

	function actualizarPrecio(){
		@if($global_isProvider == true && $pub->Producto->precioSudamerica == 1)
		var precio = 0;
		@else
		var precio = {{ number_format($pub->Producto->precio_publicacion, 2, '.', '') }};
		@endif


		@if($tasa_tramite > 0)
		precio += {{ $tasa_tramite }};
		@endif

		if(informe_tecnico){
			precio += {{ $precio_informe }};
		}
		if(seguro_extra){
			precio += {{ $precio_seguro }};
		}

		@foreach($transportesIndex as $di)
			@if(isset($transportesPub[$di->id]))
				if(valTrans_{{ $di->id }} === true){
					precio += {{ $transportesPub[$di->id] }};
				}
			@endif
		@endforeach
		@foreach($destinosIndex as $di)
			if(destino_{{ $di->id }}){
				precio += {{ (isset($destinosPub[$di->id])) ? $destinosPub[$di->id] : 0 }};
			}
		@endforeach
//		if(_idDestino > 0){
//			precio += destinosIndex['destino_' + _idDestino];
//		}

		precio = precio.toFixed(2);

		precio = getNumberFormat(precio);
		$('#precio_final').html(precio + " {{ strtoupper(session('current_currency', 'usd')) }}");

	}

	function getNumberFormat(x){
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
	}

//	$(document).ready(function(){
//		mostVisitedItems();
//		latestAddedItems();
//	});

	function relatedItems(page){
				{{--var url = '{{ route('component.mostVisitedItems') }}';--}}
				{{--$('#mostVisitedItemsContainer').load(url);--}}

		var url = '';
		if(page === undefined) {
			url = '{{ route('component.relatedItems', ['cat' => $pub->Producto->Categorias[0]->Padre]) }}';
			$('#relatedItemsContainer').load(url);
		} else {
			if(page < 1) {
				page = 1;
			}
			url = '{{ route('component.relatedItems', ['page' => 'noPage', 'cat' => $pub->Producto->Categorias[0]->Padre]) }}';
			url = url.replace('noPage', page);
			$('#relatedItemsContainer').load(url);
			curPage = page;
		}
	}

	// ---------- PRODUCTO CONSULTA CONTACTO

	function modalContactConsultant(){

		$('#sqPais').val('{{ $global_localization }}');

		@if(!\Illuminate\Support\Facades\Auth::guard('clientes')->check())
			$('#sqNombres').val('');
			$('#sqApellidos').val('');
			$('#sqEmail').val('');
			$('#sqTelefono').val('');
			$('#sqDescripcion').val('');
		@endif

		//		$('.select2-container').removeClass('inputError');

		$('#modalContactConsultant').modal();
	}

	function guardarContactoConsulta(){
		var url = '{{ route('public.consultaExterna.store') }}';
		var data = {
			nombres : $('#sqNombres').val(),
			apellidos : $('#sqApellidos').val(),
			email : $('#sqEmail').val(),
			pais : $('#sqPais').val(),
			telefono : $('#sqTelefono').val(),
			descripcion : $('#sqDescripcion').val(),
			tipo : $('#sqTipoConsulta').val(),
			pub_id : {{ $pub->id }},
		};
		ajaxPost(url, data, {
			onSuccess: function(data){
				$('#modalSearchConsultant').modal('toggle');
				$('#sqNombres').val('');
				$('#sqApellidos').val('');
				$('#sqEmail').val('');
				$('#sqPais').val('');
				$('#sqTelefono').val('');
				$('#sqDescripcion').val('');
				swal("@lang('messages.comments_sent')!", "@lang('messages.su_consulta_enviada_correctamente')", "success");
				{{--.then(function(response){--}}
				{{--redirect('{{ route("private.{$glc}.announcements") }}');--}}
				{{--});--}}
			},
			onValidation: function(data){

				$('#sqNombres').removeClass('inputError');
				$('#sqApellidos').removeClass('inputError');
				$('#sqEmail').removeClass('inputError');
				$('#sqPais').removeClass('inputError');
				$('#sqTelefono').removeClass('inputError');
				$('#sqDescripcion').removeClass('inputError');
				$('.select2-container').removeClass('inputError');

				$.each(data, function(key, value){

					key = key.replace(/^\w/, function (chr) {
						return chr.toUpperCase();
					});

					if(key === 'Pais'){
						$('.select2-container').addClass('inputError');
					}

					$('#sq' + key).addClass('inputError');

				});

				$('#sqError_messages').removeClass('hide');
			}
		});
	}

</script>
@endpush