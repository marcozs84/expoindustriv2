@extends('frontend.layouts.default')

@push('css')
<style>

	.section-container.has-bg .cover-bg:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	.about-us h1{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
	}
	.about-us p{
		/*color:rgba(0,0,0,0.63);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
	}
	.breadcrumb>.active {
		color:#ffffff;
	}

	.team_member {
		text-align:center;
	}
	.team_pic {
		width:80%;
	}
	.team_cargo {
		text-align:center !important;
		font-size:15px;
	}

	@media(max-width:760px){
		.cover-bg img {
			margin-top: 0px;
		}
		.nosotros_head {
			display:none;
		}
	}



	@media(max-width:1200px){
		.nosotros_head {
			width:90%;
		}
		.cover-bg img {
			margin-top: -100px;
		}
	}
	@media(max-width:1000px){
		.nosotros_head {
			width:90%;
		}
	}
	@media(max-width:970px){
		.cover-bg img {
			margin-top: 0px;
		}
		.nosotros_head {
			display:none;
		}
		.cover-bg img {
			margin-top: 0px;
		}
	}
	@media(min-width:1200px){
		.cover-bg img {
			margin-top: -200px;
		}
	}

	@media(min-width:1400px){
		.cover-bg img {
			margin-top: -260px;
		}
	}

</style>
@endpush
@section('content')

	<div id="banner" class="hidden-xs text-center hide">
		<div class="container">
			<img src="/assets/img/e-commerce/980x300_PanTrading_v2.gif"
			     alt=""
			     style="width:100%; /*max-width:1170px;*/ max-width:980px; margin:auto;">
		</div>
	</div>

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black hide">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -360px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>Sobre</b> Nosotros</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #about-us-cover -->
	<div id="about-us-cover" class="has-bg section-container">
		<!-- BEGIN cover-bg -->
		<div class="cover-bg">
			{{--<img src="/assets/img/e-commerce/about-us-cover.jpg" alt="" />--}}
			<img src="/assets/img/e-commerce/cover/cover-meeting1.jpg" alt="" style=" width:100%;" />
			{{--<img src="/assets/img/e-commerce/cover/cover-bigoffice.jpg" alt="" style="margin-top: -360px; width:100%;" />--}}
			{{--<img src="/assets/img/e-commerce/cover/cover-estar.jpg" alt="" style="margin-top: -360px; width:100%;" />--}}
			{{--<img src="/assets/img/e-commerce/cover/cover-estar-pizarra.jpg" alt="" style="margin-top: -460px; width:100%;" />--}}
			{{--<img src="/assets/img/e-commerce/slider/stock-photo--agricultural-soy-plantation-on-sunny-day-green-growing-soybeans-plant-against-sunlight-699261259.jpg" alt="" />--}}
			{{--<img src="/assets/img/e-commerce/slider/stock-photo-empty-highway-asphalt-road-beautiful-sky-774022651.jpg" alt="" style="margin-top:-300px; min-width:100%;" />--}}
			{{--<img src="/assets/img/e-commerce/slider/stock-photo-green-ripening-soybean-field-agricultural-landscape-1039284670.jpg" alt="" style="margin-top:-300px; min-width:100%;" />--}}
			{{--<img src="/assets/img/e-commerce/slider/stock-photo-warehouse-empty-dark-car-showroom-3d-621174866.jpg" alt="" style="margin-top:-300px; min-width:100%;" />--}}
			{{--<img src="/assets/img/e-commerce/slider/stock-photo-warehouse-largescale-shopping-center-541183837.jpg" alt="" style="margin-top:-300px; min-width:100%;" />--}}
		</div>
		<!-- END cover-bg -->
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.about_us')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN about-us -->
			<div class="about-us text-center">
				<h1>@lang('messages.about_us')</h1>
				<p>
					@lang('messages.aboutus_slogan')
				</p>
			</div>
			<!-- END about-us -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->

	<div id="about-us-content" class="section-container bg-white">
		<!-- BEGIN container -->
		<div class="container">
			<div class="row text-center" style="background-color: #ffffff;">
				<br><br>
				<img class="nosotros_head" src="/assets/img/e-commerce/nosotros_{{ \Illuminate\Support\Facades\App::getLocale() }}_2.jpg" alt="" style="max-width:100%;">
			</div>
		</div>
	</div>

	<!-- BEGIN #about-us-content -->
	<div id="about-us-content" class="section-container bg-white">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN about-us-content -->
			<div class="about-us-content">
{{--				<h2 class="title text-center"><b>@lang('messages.what_we_do')</b></h2>--}}
				<p class="desc text-center">
					@lang('messages.our_services_slogan_short')
				</p>



				<h4>@lang('messages.nosotros_saluda_title')</h4>
				<p>
					@lang('messages.nosotros_saluda_body')
					<br><br><br>
				</p>

				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Joakim.png" alt="">
							<p class="team_cargo"><strong>Joakim Byren</strong> <br> @lang('messages.cargo_joakim')</p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Sandra.png" alt="">
							<p class="team_cargo"> <strong>Sandra Pereira</strong> <br> @lang('messages.cargo_emily') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Claudia.png" alt="">
							<p class="team_cargo"> <strong>Claudia Byren</strong> <br> @lang('messages.cargo_claudia') </p>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_carla.png" alt="">
							<p class="team_cargo"> <strong>Carla Gutierrez</strong> <br> @lang('messages.cargo_carla') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Ernesto.png" alt="">
							<p class="team_cargo"> <strong>Ernesto Gonzalez</strong> <br> @lang('messages.cargo_ernesto') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Iracema.png" alt="">
							<p class="team_cargo"> <strong>Iracema Martins de Faria</strong> <br> @lang('messages.cargo_iracema') </p>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Jurgen.png" alt="">
							<p class="team_cargo"> <strong>Jurgen Robles</strong> <br> @lang('messages.cargo_jurgen') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Marco.png" alt="">
							<p class="team_cargo"> <strong>Marco A. Zeballos</strong> <br> @lang('messages.cargo_marco') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Jose_Julian.png" alt="">
							<p class="team_cargo"> <strong>José Julian Gutierrez</strong> <br> @lang('messages.cargo_jose') </p>
						</div>

					</div>
				</div>

				<br><br><br>

				<div class="row">
					<div class="col-md-12 text-center">
						<div class="image" data-animation="true" data-animation-type="flipInX">
							<img src="/assets/img/socios.png" alt="" style="width:70%;" />
						</div>
					</div>
				</div>

				<!-- BEGIN row -->
				{{--<div class="row" hidden>--}}
					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-4">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-muted"><i class="fa fa-history"></i></div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('about_us.history')</h4>--}}
								{{--<p class="desc">@lang('about_us.history_content')--}}
									{{--<a href="#modal-historia" data-toggle="modal">@lang('messages.see_more')...</a>--}}
								{{--</p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}
					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-4">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-primary"><i class="fas fa-random"></i></div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('about_us.process_flow')</h4>--}}
								{{--<p class="desc">@lang('about_us.process_flow_content')--}}
									{{--<a href="#modal-proceso" data-toggle="modal">@lang('messages.see_more')...</a>--}}
								{{--</p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}
					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-4">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-primary"><i class="icon-shuffle"></i></div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('about_us.duty_brokers')</h4>--}}
								{{--<p class="desc">@lang('about_us.duty_brokers_content')--}}
									{{--<a href="#modal-proceso" data-toggle="modal">@lang('messages.see_more')...</a>--}}
								{{--</p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}
					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-4">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-info"><i class="fa fa-lock"></i></div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('about_us.security')</h4>--}}
								{{--<p class="desc">@lang('about_us.security_content')--}}
									{{--<a href="#modal-seguridad" data-toggle="modal">@lang('messages.see_more')...</a> </p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}
					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-4">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-danger"><i class="fas fa-hourglass-half"></i></div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('about_us.sustainability')</h4>--}}
								{{--<p class="desc">@lang('about_us.sustainability_content')--}}
									{{--<a href="#modal-sostenibilidad" data-toggle="modal">@lang('messages.see_more')...</a></p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}
				{{--</div>--}}
				<!-- END row -->
			</div>
			<!-- END about-us-content -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-content -->

	<div class="modal fade" id="modal-historia">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('about_us.history_popup_title')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					@lang('about_us.history_popup_content')

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-proceso">
		<div class="modal-dialog" style="width:60% !important;">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('about_us.process_flow_popup_title')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					{{--<video id="example_video_1" class="video-js vjs-default-skin"--}}
					       {{--controls preload="auto" width="740" height="364"--}}
					       {{--poster="http://video-js.zencoder.com/oceans-clip.png"--}}
					       {{--data-setup='{"example_option":true}'>--}}
						{{--<source src="/about_us2.mp4" type="video/mp4" />--}}
						{{--<source src="http://video-js.zencoder.com/oceans-clip.webm" type="video/webm" />--}}
						{{--<source src="http://video-js.zencoder.com/oceans-clip.ogv" type="video/ogg" />--}}
						{{--<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>--}}
					{{--</video>--}}

					<img src="/assets/img/sobre_nosotros/1flujo_{{ App::getLocale() }}.png" alt="" style="width:100%; margin:auto;">

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modal-proceso-old">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">Flujo del proceso</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<ol>
						<li>Vendedor europeo anuncia su producto en <b>ExpoIndustri</b>.</li>
						<li>Comprador sudamericano busca en <b>ExpoIndustri</b> el equipo/maquinaria/herramienta requerida.</li>
						<li>El comprador cotiza los productos a través de la elección de una serie de alternativas.</li>
						<li>El comprador decide realizar la compra de un equipo y envía su solicitud de compra a <b>ExpoIndustri</b>.</li>
						<li><b>ExpoIndustri</b> realiza todas las verificaciones requeridas, en base a las alternativas elegidas por el comprador.</li>
						<li><b>ExpoIndustri</b> confirma la existencia real y disponibilidad de la maquinaria/equipo y envía confirmación al comprador para que realice el pago correspondiente.</li>
						<li><b>ExpoIndustri</b> realiza el pago al vendedor y recoge la maquinaria.</li>
						<li>La maquinaria es cargada en el tipo de contenedor especificado por el cliente y despachado a Sudamérica.</li>
						<li><b>ExpoIndustri</b> realiza la gestión administrativa y el proceso de consolidación del contenedor.</li>
						<li><b>ExpoIndustri</b> realiza la gestión administrativa y el proceso de envío de la maquinaria/equipo al destino final, especificado por el comprador.</li>
						<li>El comprador recoge su maquinaria en el destino final elegido.</li>
					</ol>

				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-seguridad">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('about_us.security_popup_title')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					@lang('about_us.security_popup_content')
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-sostenibilidad">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('about_us.sustainability_popup_title')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					@lang('about_us.sustainability_popup_content')
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

@endsection