@extends('frontend.layouts.default')
@push('css')
<style>
	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}
	.item.item-thumbnail .item-image {
		padding:0px;
	}
	.item-thumbnail{
		margin-bottom:10px;
	}
	.discount, .item-discount-price, .item-desc{
		display:none;
	}
</style>
@endpush
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			{{--<img src="/assets/img/e-commerce/cover/cover-15.jpg" alt="" />--}}
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -200px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>@lang('messages.provider_profile')</b> </h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #product -->
	<div id="product" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.buy_page') }}">@lang('messages.home')</a></li>
{{--				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>--}}
				<li class="active">@lang('messages.provider_profile')</li>
			</ul>
			<!-- END breadcrumb -->

			@if($prov->tieneStore != 1)
			<div class="row">
				<div class="alert alert-danger">
					Service available only for e-store customers.
				</div>
			</div>
			@endif

			<!-- BEGIN row -->
			<div class="row row-space-30">
				<!-- BEGIN col-8 -->
				<div class="col-md-8">

					<div id="providerItemsContainer"></div>

				</div>
				<!-- END col-8 -->
				<!-- BEGIN col-4 -->
				<div class="col-md-4" style="background-color:#ffffff; padding:10px; border:1px solid #e5e5e5;">
					<div style="font-size:18px; text-align:center; font-weight:600; padding:15px; margin-bottom:15px; background-color:#f0f3f4; /*background-color:#e2e5e6;*/ /*border:1px solid #c4ced4;*/">
						{{--{{ $cliente->Agenda->nombres_apellidos }}--}}
						{{ $companyName }}
					</div>

					@if($imagen)
					<div style="background-color:{{ $imagen->backgroundColor }}; /*background-color:#ffffff;*/ border:1px solid #c5ced4; text-align:center; padding:10px; margin:0px 0px 10px 0px;">
						<img src="/company_logo/{{ $logoUrl }}" alt="Logo proveedor" style="max-width:100%;">
					</div>
					@endif

					@if(trim($urlSitio) != '')
					<div style="background-color:#f0f3f4; /*background-color:#ffffff;*/ /*border:1px solid #c5ced4;*/ text-align:center; padding:10px; margin:0px 0px 10px 0px; font-size:12px;">
						<a href="{{ $urlSitioHttp }}" target="_blank">{{ str_replace('http://', '', $urlSitio) }}</a>
					</div>
					@endif

					@if(trim($companyDesc) != '')
					<div style="background-color:#f0f3f4; /*background-color:#ffffff;*/ /*border:1px solid #c5ced4;*/ text-align:left; padding:10px; margin:0px 0px 10px 0px; font-size:14px;">
						{!! nl2br($companyDesc) !!}
					</div>
					@endif

					<div style="background-color:#f0f3f4; /*background-color:#ffffff;*/ /*border:1px solid #c5ced4;*/ text-align:left; padding:10px; margin:0px 0px 10px 0px; font-size:12px;">
						<b>@lang('messages.address')</b><br>
						{!! nl2br($ubicacionPrimaria->direccion) !!}
					</div>

					{{--<style>--}}
						{{--#publish_map {--}}
							{{--height: 300px;--}}
							{{--width: 100%;--}}
						{{--}--}}
					{{--</style>--}}

					{{--<div id="publish_map"></div>--}}

					<div style="background-color:#f0f3f4; /*background-color:#ffffff;*/ /*border:1px solid #c5ced4;*/ text-align:left; padding:10px; margin:0px 0px 10px 0px; font-size:12px;">
					@foreach($ubicacionesSecundarias as $ubicacion)
						<div>
							<b>{{ $ubicacion->descripcion }}</b>
						</div>
						<p class="m-b-15">
							{!! nl2br($ubicacion->direccion) !!}
						</p>
					@endforeach
					</div>

					{{--<div><b>@lang('messages.e_mail')</b></div>--}}
					{{--<p class="m-b-15">--}}
						{{--<a href="mailto:hello@emailaddress.com" class="text-inverse">info@seantheme.com</a><br />--}}
						{{--<a href="mailto:hello@emailaddress.com" class="text-inverse">business@seantheme.com</a><br />--}}
						{{--<a href="mailto:hello@emailaddress.com" class="text-inverse">support@seantheme.com</a><br />--}}
					{{--</p>--}}
					@if($prov->urlFacebook == '' && $prov->urlTwitter == '' && $prov->urlGooglePlus == '' )
					@else
						<div class="m-b-5"><b>@lang('messages.social_networks')</b></div>
					@endif

					<p class="m-b-15">
						@if($prov->urlFacebook != '')
							<a href="{{ $prov->urlFacebook }}" class="btn btn-icon btn-white btn-circle"><i class="fab fa-facebook"></i></a>
						@endif
						@if($prov->urlTwitter != '')
								<a href="{{ $prov->urlTwitter }}" class="btn btn-icon btn-white btn-circle"><i class="fab fa-twitter"></i></a>
						@endif
						@if($prov->urlGooglePlus != '')
								<a href="{{ $prov->urlGooglePlus }}" class="btn btn-icon btn-white btn-circle"><i class="fab fa-google-plus"></i></a>
						@endif



						{{--<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-instagram"></i></a>--}}
						{{--<a href="#" class="btn btn-icon btn-white btn-circle"><i class="fab fa-dribbble"></i></a>--}}
					</p>
				</div>
				<!-- END col-4 -->
			</div>
			<!-- END row -->
		</div>
		<!-- END row -->
	</div>
	<!-- END #product -->

@endsection

@push('scripts')
<script>

	$(document).ready(function(){

		$('#btnContactoEnviar').click(function(e){
			$(this).html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
		});

		{{--var paises = {!! json_encode($global_paises) !!};--}}
		{{--console.log(paises.length);--}}
		{{--$.each(paises, function(k,v){--}}
			{{--$('#pais').append($('<option>', { value:v }).html(k));--}}
		{{--});--}}

		initAnnouncesByProvider()

	});

	function initAnnouncesByProvider(page){
		var url = '';
		if(page === undefined) {
			url = '{{ route('component.announcesByProvider', [$cliente->Usuario->id]) }}';
			$('#providerItemsContainer').load(url);
		} else {
			console.log(page);
			if(page < 1) {
				page = 1;
			}
			url = '{{ route('component.announcesByProvider', [$cliente->Usuario->id, 'page' => 'noPage']) }}';
			url = url.replace('noPage', page);
			$('#providerItemsContainer').load(url);
			curPage = page;
		}
	}

	// ----------
	var map = null;
	var geocoder = null;
	var marker = null;
	function initPublishMap() {
		@if($ubicacionPrimaria->latitud != 0)
			var uluru = {lat: {{ $ubicacionPrimaria->latitud }}, lng: {{ $ubicacionPrimaria->longitud }}};
		@else
			var uluru = {lat: -25.363, lng: 131.044};
		@endif

//		map = new google.maps.Map(document.getElementById('publish_map'), {
//			zoom: 16,
//			center: uluru,
////			query: 'Bolivia'
//		});

		{{--@if($ubicacionPrimaria->latitud != 0)--}}
			{{--marker = new google.maps.Marker({--}}
				{{--position: uluru,--}}
				{{--map: map,--}}
			{{--});--}}
			{{--map.panTo(uluru);--}}
		{{--@else--}}
			{{--geocoder = new google.maps.Geocoder();--}}
			{{--//		var address = $("#pais option:selected").text();--}}
			{{--var address = '{{ $global_country_name }}';--}}
			{{--geocodeAddress(geocoder, map, address);--}}

		{{--@endif--}}
	}
	function geocodeAddress(geocoder, resultsMap, address) {
//		var address = 'Bolivia';
//		var address = $("#pais option:selected").text();
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);
//				var marker = new google.maps.Marker({
//					map: resultsMap,
//					position: results[0].geometry.location
//				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	{{--@push('mapsInit')--}}
	{{--initPublishMap();--}}
	{{--@if($ubicacionPrimaria->latitud != 0)--}}

	{{--@endif--}}
	{{--@endpush--}}

</script>
@endpush