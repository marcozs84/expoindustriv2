@extends('frontend.layouts.default')

@section('content')

	<table class="table table-condensed table-striped">
		<tr>
			<th>Nombres y apellidos</th>
			<th>ID Stripe</th>
			<th>ID Stripe Test</th>
		</tr>
		@foreach($usuarios as $usuario)
			<tr>
				<td>{{ $usuario->Agenda->nombres_apellidos }}</td>
				<td><a href="javascript:;" onclick="getCustomer('{{ $usuario->idStripe }}')">{{ $usuario->idStripe }}</a></td>
				<td><a href="javascript:;" onclick="getCustomer('{{ $usuario->idStripeTest }}')">{{ $usuario->idStripeTest }}</a></td>
			</tr>
		@endforeach
	</table>

@endsection

@push('scripts')
<script>


	function getCustomer(code) {
		ajaxPost(url, data, {
			onSuccess: function( data ) {
				console.log(data);
			}
		});
	}
</script>
@endpush