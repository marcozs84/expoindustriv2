@extends('frontend.layouts.default')

@section('content')

	<table class="table table-condensed table-striped">
		<tr>
			<th>Nombres y apellidos</th>
			<th>Username</th>
			<th>ID Stripe</th>
			<th>ID Stripe Test</th>
		</tr>
		@foreach($usuarios as $usuario)
			<tr>
				<td>{{ $usuario->Owner->Agenda->nombres_apellidos }}</td>
				<td><a href="javascript:;" onclick="createCustomer('{{ $usuario->id }}')">{{ $usuario->username }}</a></td>
				<td><a href="javascript:;" onclick="getCustomer('{{ $usuario->id }}')">{{ $usuario->idStripe }}</a></td>
				<td><a href="javascript:;" onclick="getCustomer('{{ $usuario->id }}')">{{ $usuario->idStripeTest }}</a></td>
			</tr>
		@endforeach
	</table>

@endsection

@push('scripts')
<script>


	function getCustomer(id) {

		var url = '{{ route('public.customerInfo', 'idUsuario') }}';
		url = url.replace('idUsuario', id);
		ajaxGet(url, {}, {
			onSuccess: function( data ) {
				console.log( data );
			}
		} )
	}
	function createCustomer(id) {

		var url = '{{ route('public.createCustomer', 'idUsuario') }}';
		url = url.replace('idUsuario', id);
		ajaxPost(url, {}, {
			onSuccess: function( data ) {
				console.log( data );
			}
		} )
	}
</script>
@endpush