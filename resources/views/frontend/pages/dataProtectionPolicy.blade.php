@extends('frontend.layouts.default')
@push('css')
<style>


	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}
</style>
@endpush
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-tractor1.jpg" alt="" style="margin-top:-380px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.data_protection_policy')</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.data_protection_policy')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN panel-group -->
			<div class="panel-group faq" id="faq-list">
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="">
								<i class="fa fa-cubes fa-fw m-r-5 text-info"></i>
								@lang('messages.data_protection_policy')
							</a>
						</h4>
					</div>
					<div id="faq-" class="panel-collapse collapse in ">
						<div class="panel-body">
							@include('frontend.includes.dataProtectionPolicy_'.App::getLocale())
						</div>
					</div>
				</div>

			</div>
			<!-- END panel-group -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection