@extends('frontend.layouts.default')
@push('css')
<style>
	.inputError {
		border:1px solid #ff0000 !important;
	}

	/* Ref.: https://stackoverflow.com/a/16839906 */

	.tooltip{
		position:absolute;
		z-index:1020;
		display:block;
		visibility:visible;
		padding:5px;
		font-size:11px;
		opacity:0;
		filter:alpha(opacity=0)
	}
	.tooltip.in{
		opacity:.8;
		filter:alpha(opacity=80)
	}
	.tooltip.top{
		margin-top:-2px
	}
	.tooltip.right{
		margin-left:2px
	}
	.tooltip.bottom{
		margin-top:2px
	}
	.tooltip.left{
		margin-left:-2px
	}
	.tooltip.top .tooltip-arrow{
		bottom:0;
		left:50%;
		margin-left:-5px;
		border-left:5px solid transparent;
		border-right:5px solid transparent;
		border-top:5px solid #000
	}
	.tooltip.left .tooltip-arrow{
		top:50%;
		right:0;
		margin-top:-5px;
		border-top:5px solid transparent;
		border-bottom:5px solid transparent;
		border-left:5px solid #000
	}
	.tooltip.bottom .tooltip-arrow{
		top:0;
		left:50%;
		margin-left:-5px;
		border-left:5px solid transparent;
		border-right:5px solid transparent;
		border-bottom:5px solid #000
	}
	.tooltip.right .tooltip-arrow{
		top:50%;
		left:0;
		margin-top:-5px;
		border-top:5px solid transparent;
		border-bottom:5px solid transparent;
		border-right:5px solid #000
	}
	.tooltip-inner{
		max-width:350px;
		padding:3px 8px;
		color:#fff;
		text-align:center;
		text-decoration:none;
		background-color:#000;
		-webkit-border-radius:4px;
		-moz-border-radius:4px;
		border-radius:4px
	}
	.tooltip-arrow{
		position:absolute;
		width:0;
		height:0
	}
	.section-container.has-bg .cover-bg:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	.about-us h1{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
	}
	.about-us p{
		/*color:rgba(0,0,0,0.63);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
	}
	.breadcrumb>.active {
		color:#ffffff;
	}
</style>
@endpush
@section('content')

	<div id="banner" class="hidden-xs text-center hide">
		<div class="container">
			<img src="/assets/img/e-commerce/980x300_PanTrading_v2.gif"
			     alt=""
			     style="width:100%; /*max-width:1170px;*/ max-width:980px; margin:auto;">
		</div>
	</div>

	<!-- BEGIN #about-us-cover -->
	<div id="about-us-cover" class="has-bg section-container">
		<!-- BEGIN cover-bg -->
		<div class="cover-bg">
			{{--<img src="/assets/img/e-commerce/about-us-cover.jpg" alt="" />--}}
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -100px; width:100%;" />
			{{--<img src="/assets/img/e-commerce/cover/cover-tractor1.jpg" alt="" />--}}
		</div>
		<!-- END cover-bg -->
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.our_services')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN about-us -->
			<div class="about-us text-center">
				<h1>@lang('messages.our_services')</h1>
				<p>
					@lang('messages.our_services_slogan')
				</p>
			</div>
			<!-- END about-us -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


	<div id="about-us-content" class="section-container bg-white">
		<div class="container">
			<!-- BEGIN about-us-content -->
			<div class="about-us-content">
				<h2 class="title text-center" style="margin-bottom:40px;"><b>@lang('messages.what_we_do')</b></h2>
				{{--<p class="desc text-center">--}}
				{{--Todo lo que necesita saber acerca de que <b>ÉS</b> y que <b>LLEGARÁ A SER</b> ExpoIndustri.--}}
				{{--</p>--}}

				{{--<div class="row">--}}
					{{--<h4>@lang('services.service_for_sellers')</h4>--}}
				{{--</div>--}}
				<div class="row">

					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-3">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-muted">--}}
								{{--<a href="javascript:;"  onclick="modalContent('partner');">--}}
									{{--<img src="/assets/img/services/7_partner.png" style="max-width:80%; max-height:180px;" alt="Partner">--}}
								{{--</a>--}}
							{{--</div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('services.serv_partner_title')</h4>--}}
								{{--<p class="desc">--}}
									{{--<a href="javascript:;" onclick="modalContent('partner');" data-toggle="modal">--}}
										{{--@lang('services.service_partner')--}}
									{{--</a>--}}
									{{--<a href="#modal-historia" data-toggle="modal">ver más...</a>--}}
								{{--</p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}

					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-3">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-muted">--}}
								{{--<a href="javascript:;"  onclick="modalContent('banner');">--}}
									{{--<img src="/assets/img/services/1_anuncie.jpg" style="max-width:80%; max-height:180px;" alt="Announce">--}}
								{{--</a>--}}
							{{--</div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('services.serv_banner_title')</h4>--}}
								{{--<p class="desc">--}}
									{{--<a href="javascript:;" onclick="modalContent('banner');" data-toggle="modal">--}}
										{{--@lang('services.service_banner')--}}
									{{--</a>--}}
									{{--<a href="#modal-historia" data-toggle="modal">ver más...</a>--}}
								{{--</p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}

					<!-- begin col-4 -->
					<div class="col-md-3 col-sm-3">
						<div class="service">
							<div class="icon text-muted">
								<a href="javascript:;" onclick="modalContent('istore');" >
									<img src="/assets/img/services/5_tienda.jpg" style="max-width:80%; max-height:180px;" alt="Announce">
								</a>
							</div>
							<div class="info">
								<h4 class="title">@lang('services.serv_istore_title')</h4>
								<p class="desc">
									<a href="javascript:;" onclick="modalContent('istore');" >
										@lang('services.service_istore')
									</a>
									{{--<a href="#modal-historia" data-toggle="modal">ver más...</a>--}}
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->

					<!-- begin col-4 -->
					<div class="col-md-3 col-sm-3">
						<div class="service">
							<div class="icon text-muted">
								<a href="javascript:;" onclick="modalContent('support');" >
									<img src="/assets/img/services/4_soporte.jpg" style="max-width:80%; max-height:180px;" alt="Announce">
								</a>
							</div>
							<div class="info">
								<h4 class="title">@lang('services.serv_support_title')</h4>
								<p class="desc">
									<a href="javascript:;" onclick="modalContent('support');" >
										@lang('services.service_support')
									</a>
									{{--<a href="#modal-soporte" data-toggle="modal">ver más...</a>--}}
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->

				{{--</div>--}}

				{{--<div class="row">--}}
					{{--<h4>@lang('services.service_for_buyers')</h4>--}}
				{{--</div>--}}
				{{--<div class="row">--}}


					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-3">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-muted">--}}
								{{--<a href="javascript:;" onclick="modalContent('buyonline');" >--}}
									{{--<img src="/assets/img/services/6_buyonline.png" style="max-width:80%; max-height:180px;" alt="Announce">--}}
								{{--</a>--}}
							{{--</div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('services.serv_buyonline_title')</h4>--}}
								{{--<p class="desc">--}}
									{{--<a href="javascript:;" onclick="modalContent('buyonline');" >--}}
										{{--@lang('services.service_buyonline')--}}
									{{--</a>--}}
									{{--<a href="#modal-search" data-toggle="modal">ver más...</a>--}}
								{{--</p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}

					<!-- begin col-4 -->
					<div class="col-md-3 col-sm-3">
						<div class="service">
							<div class="icon text-muted">
								<a href="javascript:;" onclick="modalContent('search');" >
									<img src="/assets/img/services/2_busqueda.jpg" style="max-width:80%; max-height:180px;" alt="Announce">
								</a>
							</div>
							<div class="info">
								<h4 class="title">@lang('services.serv_search_title')</h4>
								<p class="desc">
									<a href="javascript:;" onclick="modalContent('search');" >
										@lang('services.service_search')
									</a>
									{{--<a href="#modal-search" data-toggle="modal">ver más...</a>--}}
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->

					{{--<!-- begin col-4 -->--}}
					{{--<div class="col-md-3 col-sm-3">--}}
						{{--<div class="service">--}}
							{{--<div class="icon text-muted">--}}
								{{--<a href="javascript:;" onclick="modalContent('support');" >--}}
									{{--<img src="/assets/img/services/4_soporte.jpg" style="max-width:80%; max-height:180px;" alt="Announce">--}}
								{{--</a>--}}
							{{--</div>--}}
							{{--<div class="info">--}}
								{{--<h4 class="title">@lang('services.serv_support_title')</h4>--}}
								{{--<p class="desc">--}}
									{{--<a href="javascript:;" onclick="modalContent('support');" >--}}
										{{--@lang('services.service_support')--}}
									{{--</a>--}}
									{{--<a href="#modal-soporte" data-toggle="modal">ver más...</a>--}}
								{{--</p>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
					{{--<!-- end col-4 -->--}}

					<!-- begin col-4 -->
					<div class="col-md-3 col-sm-3">
						<div class="service">
							<div class="icon text-muted">
								<a href="javascript:;" onclick="modalContent('inspection');" >
									<img src="/assets/img/services/3_inspeccion.jpg" style="max-width:80%; max-height:180px;" alt="Announce">
								</a>

							</div>
							<div class="info">
								<h4 class="title">@lang('services.serv_inspection_title')</h4>
								<p class="desc">
									<a href="javascript:;" onclick="modalContent('inspection');" >
										@lang('services.service_inspection')
									</a>
									{{--<a href="#modal-historia" data-toggle="modal">ver más...</a>--}}
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->

				</div>



			</div>
			<!-- END about-us-content -->
		</div>
	</div>


	<div class="modal fade" id="modal-announce">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title text-center titleh hide" id="title_banner">@lang('services.serv_banner_title')</h4>
					<h4 class="modal-title text-center titleh hide" id="title_search" >@lang('services.serv_search_title')</h4>
					<h4 class="modal-title text-center titleh hide" id="title_inspection" >@lang('services.serv_inspection_title')</h4>
					<h4 class="modal-title text-center titleh hide" id="title_support" >@lang('services.serv_support_title')</h4>
					<h4 class="modal-title text-center titleh hide" id="title_istore" >@lang('services.serv_istore_title')</h4>


					{{--<a class="close" data-dismiss="modal" aria-hidden="true">×</a>--}}
				</div>
				<div class="modal-body">

					<div id="content_banner" class="contenth hide">
						{{--<div style="background-image:url('/assets/img/services/banner_{{ $global_localization }}.jpg'); background-repeat:no-repeat; background-position:right 0px; padding-top:20px;">--}}
							{{--<img src="/assets/img/services/1_banner2_{{ $global_localization }}.jpg" class="" style="max-width:80%; max-height:160px;" alt="Announce">--}}
							{{--<br><br>--}}
						{{--</div>--}}
						{{--<div class="row">--}}
							{{--<div class="col col-md-12 text-center">--}}
								{{--<img src="/assets/img/services/banner_{{ $global_localization }}.jpg" class="" style="max-width:80%; max-height:160px;" alt="Announce">--}}
							{{--</div>--}}
							{{--<div class="col col-md-6 text-center">--}}
								{{--<img src="/assets/img/services/1_banner_plan_2_{{ $global_localization }}.jpg" class="" style="max-width:80%; max-height:160px;" alt="Announce">--}}
							{{--</div>--}}
						{{--</div>--}}
						<img src="/assets/img/services/banner_{{ $global_localization }}.jpg"  style="max-width:100%; " alt="Announce">
					</div>

					<div id="content_search" class="contenth hide">
						<div class="col col-md-3" style="padding:0px;">
							<img src="/assets/img/services/2_busqueda_iso.jpg" alt="" style="max-width:100%;">
						</div>
						<div class="col col-md-9">
							<p>
								@lang('services.serv_search_body')
							</p>
						</div>
						<div class="divider" style="clear:both;"></div>
					</div>

					<div id="content_inspection" class="contenth hide">
						<div class="col col-md-4" style="padding:0px;">
							<img src="/assets/img/services/3_inspeccion_iso.jpg" alt="" style="max-width:100%;">
						</div>
						<div class="col col-md-8">
							<p>
								@lang('services.serv_inspection_body')
							</p>
						</div>
						<div class="divider" style="clear:both;"></div>
					</div>

					<div id="content_support" class="contenth hide">
						<div class="col col-md-3" style="padding:0px;">
							<img src="/assets/img/services/4_soporte_iso.jpg" alt="" style="max-width:100%;">
						</div>
						<div class="col col-md-9">
							<p>
								@lang('services.serv_support_body')
							</p>
						</div>
						<div class="divider" style="clear:both;"></div>
						<table style="width:100%;">
							<tr>
								<td style="vertical-align:middle; text-align:center;">
									<a href="javascript:;" class="detail_content" data-title="@lang('services.serv_support_contcomp_msg')">
										<img src="/assets/img/services/4_soporte_montacargas.jpg" alt="" style="max-width:60%;">
									</a>
								</td>
								<td style="vertical-align:middle; text-align:center;">
									<a href="javascript:;" class="detail_content" data-title="@lang('services.serv_support_contex_msg')">
										<img src="/assets/img/services/4_soporte_container.jpg" alt="" style="max-width:60%;">
									</a>
								</td>
								<td style="vertical-align:middle; text-align:center;">
									<a href="javascript:;" class="detail_content" data-title="@lang('services.serv_support_contabi_msg')">
										<img src="/assets/img/services/4_soporte_barco.jpg" alt="" style="max-width:60%;">
									</a>
								</td>
							</tr>
							<tr>
								<td class="text-center"><h5>@lang('services.serv_support_contcomp')</h5></td>
								<td class="text-center"><h5>@lang('services.serv_support_contex')</h5></td>
								<td class="text-center"><h5>@lang('services.serv_support_contabi')</h5></td>
							</tr>
						</table>
					</div>

					<div id="content_istore" class="contenth hide">
						<div class="col col-md-3" style="padding:0px;">
							<img src="/assets/img/services/5_tienda_iso.jpg" alt="" style="max-width:100%;">
						</div>
						<div class="col col-md-9">
							<p>
								@lang('services.serv_istore_body')
							</p>
						</div>
						<div class="divider" style="clear:both;"></div>
					</div>

					<div id="content_partner" class="contenth hide">
						<div class="col col-md-3" style="padding:0px;">
							<img src="/assets/img/services/7_partner.png" alt="" style="max-width:100%;">
						</div>
						<div class="col col-md-9">
							<p>
								@lang('services.serv_partner_body')
							</p>
						</div>
						<div class="divider" style="clear:both;"></div>
					</div>

					<div id="content_buyonline" class="contenth hide">
						<div class="col col-md-3" style="padding:0px;">
							<img src="/assets/img/services/6_buyonline.png" alt="" style="max-width:100%;">
						</div>
						<div class="col col-md-9">
							<p>
								@lang('services.serv_buyonline_body')
							</p>

						</div>
						<div class="divider" style="clear:both;"></div>

						<p class="text-center">
							<a id="btnSearchQuestion" href="javascript:;"  class="promotion-btn m-t-20 m-b-10" onclick="openLoginRegister('register')" style="width:50%; font-size:16px;">@lang('messages.log_in') / @lang('messages.register')</a>
						</p>
					</div>

					<hr>
					<p class="search_alert_msgfrm">
						@lang('messages.search_alert_msgfrm')
					</p>

					<div id="sqError_messages" class="hide">
						<p class="alert alert-danger">@lang('messages.marked_fields_required')</p>
					</div>

					<div id="sqOk_messages" class="hide">
						<p class="alert alert-success">@lang('messages.su_consulta_enviada_correctamente')</p>
					</div>

					<div id="pricesLink">

						<a href="{{ route("public.{$glc}.pricing") }}" class="btn btn-theme btn-block" style="/*background-color:#00acac;*/ font-size:24px;">@lang('messages.choose_plan_business_plus')</a>

					</div>

					<form id="formSearchQuestion"  action="{{ route('public.registerAdPublisher') }}" method="post" class="text-center">

						<div class="row">
							<div class="col col-md-6">

								@if($auth_nombres_apellidos == '')
									<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')">
									<div id="sqError_apellidos"></div>
								@else
									<input type="text" name="sqApellidos" id="sqApellidos" class="login-input" placeholder="@lang('messages.lastname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->apellidos }}" disabled>
								@endif

									<input type="hidden" id="sqTipoConsulta" value="">

								@if($auth_nombres_apellidos == '')
									<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" >
									<div id="sqError_nombres"></div>
								@else
									<input type="text" name="sqNombres" id="sqNombres" class="login-input" placeholder="@lang('messages.firstname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->nombres }}" disabled >
								@endif

								@if($auth_nombres_apellidos == '')
									<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
									<div id="sqError_email"></div>
								@else
									<input type="text" name="sqEmail" id="sqEmail" class="login-input" placeholder="@lang('messages.e_mail')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->username }}" disabled >
								@endif

								{{--<select name="pais" id="sqPais" class="login-input">--}}
								{{--<option value="">@lang('messages.select_country')</option>--}}
								{{--</select>--}}

								@if($auth_nombres_apellidos == '')
									{!! Form::select('pais', $global_paises, $global_country_abr, [
										'id' => 'sqPais',
										'class' => 'login-input select2',
										'placeholder' => __('messages.select_country'),
										'style' => 'width:90% !important;  margin-top:10px;'
									]); !!}
									<div id="adPubError_pais"></div>
								@else
									{!! Form::select('pais', $global_paises, \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->pais, [
										'id' => 'sqPais',
										'class' => 'login-input select2',
										'placeholder' => __('messages.select_country'),
										'style' => 'width:90% !important; margin-top:10px;',
										'disabled'
									]); !!}
								@endif

								@if($auth_nombres_apellidos == '')
									<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
									<div id="sqError_telefono"></div>
								@else
									<input type="text" name="sqTelefono" id="sqTelefono" class="login-input" placeholder="@lang('messages.telephone')"
									       value="{{ (count(\Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos) > 0) ? \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos->first()->numero : '' }}">
								@endif

							</div>
							<div class="col col-md-6">
								<textarea class="login-input form-control" id="sqDescripcion" style="width:95%; height:193px;" placeholder="@lang('messages.comments')"></textarea>
							</div>
						</div>

						{{--<input type="submit" id="adPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
						<a id="btnSearchQuestion" href="javascript:;"  class="promotion-btn m-t-20 m-b-10" onclick="guardarConsultaExterna()" style="width:50%; font-size:16px;">@lang('messages.send')</a>
						{{--<a href="javascript:;" id="adPubBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}

					</form>

					<div style="width:100%; clear:both;"></div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

@endsection


@push('scripts')
<script src="/assets/plugins/select2/dist/js/select2.js"></script>
<script>
	$(document).ready(function(){
//		$('#modal-announce').modal('show');

//		modalContent('support');

		$('.select2').select2({
			escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
			templateResult: formatRepo,
			templateSelection: formatRepoSelection
		});

		$('#sqPais').val('{{ $global_localization }}');

		@if($service != 'none')
			modalContent('{{ $service }}');
		@endif

	})

	function modalContent(content){

		$('.contenth').addClass('hide');
		$('#content_' + content).removeClass('hide');

		$('.titleh').addClass('hide');
		$('#title_' + content).removeClass('hide');

		// ----------

		$('#sqNombres').removeClass('inputError');
		$('#sqApellidos').removeClass('inputError');
		$('#sqEmail').removeClass('inputError');
		$('#sqPais').removeClass('inputError');
		$('#sqTelefono').removeClass('inputError');
		$('#sqDescripcion').removeClass('inputError');
		$('.select2-container').removeClass('inputError');

		// ----------

		$('#sqNombres').val('');
		$('#sqApellidos').val('');
		$('#sqEmail').val('');
		$('#sqPais').val('{{ $global_localization }}');
		$('#sqTelefono').val('');
		$('#sqDescripcion').val('');
//		$('.select2-container').removeClass('inputError');
		
		$('#sqError_messages').addClass('hide');
		$('#formSearchQuestion').removeClass('hide');
		$('#sqOk_messages').addClass('hide');

		$('#sqTipoConsulta').val(content);

		if(content === 'support'){
			$('#modal-announce').find('.modal-dialog').addClass('modal-lg');
		} else {
			$('#modal-announce').find('.modal-dialog').removeClass('modal-lg');
		}

		$('#pricesLink').addClass('hide');
		$('.search_alert_msgfrm').removeClass('hide');
		if( content === 'istore' ) {
			$('#formSearchQuestion').addClass('hide');
			$('#pricesLink').removeClass('hide');
			$('.search_alert_msgfrm').addClass('hide');
		}

		$('#modal-announce').modal();
	}

	$('#modal-announce').on('shown.bs.modal', function (e) {
//		$('[data-toggle="tooltip"]').tooltip({
//			html: true,
//			animation: true,
//			placement: 'bottom',
//		})
		$('.detail_content').addClass('animated slideDown').tooltip({
			html: true,
			animation: true,
			placement: 'top',
			delay: {
				show: 300,
				hide: 500
			}
		});
//		$('.detail_content').addClass('animated swing');
	});

	function guardarConsultaExterna(){
		var url = '{{ route('public.consultaExterna.store') }}';
		var data = {
			nombres : $('#sqNombres').val(),
			apellidos : $('#sqApellidos').val(),
			email : $('#sqEmail').val(),
			pais : $('#sqPais').val(),
			telefono : $('#sqTelefono').val(),
			descripcion : $('#sqDescripcion').val(),
			tipo : $('#sqTipoConsulta').val(),
		};
		ajaxPost(url, data, {
			onSuccess: function(data){
				console.log(data);

				$('#modalSearchConsultant').modal('toggle');
				$('#sqNombres').val('');
				$('#sqApellidos').val('');
				$('#sqEmail').val('');
				$('#sqPais').val('{{ $global_localization }}');
				$('#sqTelefono').val('');
				$('#sqDescripcion').val('');
				swal("@lang('messages.comments_sent')!", "@lang('messages.su_consulta_enviada_correctamente')", "success");
				$('#sqError_messages').addClass('hide');
				$('#formSearchQuestion').addClass('hide');
				$('#sqOk_messages').removeClass('hide');
				{{--.then(function(response){--}}
				{{--redirect('{{ route("private.{$glc}.announcements") }}');--}}
				{{--});--}}
			},
			onValidation: function(data){

				$('#sqNombres').removeClass('inputError');
				$('#sqApellidos').removeClass('inputError');
				$('#sqEmail').removeClass('inputError');
				$('#sqPais').removeClass('inputError');
				$('#sqTelefono').removeClass('inputError');
				$('#sqDescripcion').removeClass('inputError');
				$('.select2-container').removeClass('inputError');

				$.each(data, function(key, value){

					key = key.replace(/^\w/, function (chr) {
						return chr.toUpperCase();
					});

					if(key === 'Pais'){
						console.log("tiene pais");
						$('.select2-container').addClass('inputError');
					}

					$('#sq' + key).addClass('inputError');



				});

				$('#sqError_messages').removeClass('hide');
			}
		});
	}

	var codigosPais = {!! json_encode($global_paisesCodigo) !!};

	function formatRepo (repo) {

		if (repo.loading) {
			return repo.text;
		}

		var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__title'>" + repo.text + "</div>";

		if (repo.description) {
			markup += "<div class='select2-result-repository__description'> " + repo.description + "</div>";
		}

		return markup;
	}

	function formatRepoSelection (repo) {
		return repo.full_name || getFullName(repo);
	}

	function getFullName(repo){
		if(repo.id != ''){
			return '<span class="flag-icon flag-icon-'+ repo.id +' "></span> &nbsp;&nbsp;' + '+' + codigosPais[repo.id] + ' ' + repo.text;
		} else {
			return repo.text;
		}

	}
</script>
@endpush









