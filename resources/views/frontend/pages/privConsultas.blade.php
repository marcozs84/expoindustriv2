@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">Inicio</a></li>
				<li><a href="/cuenta">Mi cuenta</a></li>
				<li class="active">Mis Consultas</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar">
					<div class="account-sidebar-cover">
						<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />
					</div>
					<div class="account-sidebar-content">
						<h4>Mis Consultas</h4>
						<p>
							Listado de consultas realizadas a nuestro equipo.
						</p>
						<p>
							Por favor no dude en contactarnos si requiere asistencia.
						</p>
					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN table-responsive -->
						<div class="table-responsive">
							<!-- BEGIN table-product -->
							<table class="table table-listing table-striped ">
								<thead>
								<tr>
									<th># ID</th>
									<th>Asunto</th>
									<th>Atendido por...</th>
									<th>Fecha de creacion</th>
									<th>Estado</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td class="field"><a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">Lorem ipsum dolor sit amet consectetuer dip adipisicing.</a>
									</td>
									<td>John Mayer</td>
									<td>14.08.2018</td>
									<td>Abierto</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">Lorem ipsum dolor sit amet consectetuer dip adipisicing.</a>
									</td>
									<td>John Mayer</td>
									<td>14.08.2018</td>
									<td>Abierto</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">Lorem ipsum dolor sit amet consectetuer dip adipisicing.</a>
									</td>
									<td>John Mayer</td>
									<td>14.08.2018</td>
									<td>Abierto</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">Lorem ipsum dolor sit amet consectetuer dip adipisicing.</a>
									</td>
									<td>John Mayer</td>
									<td>14.08.2018</td>
									<td>Abierto</td>
								</tr>
								<tr>
									<td class="field"><a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">15652</a></td>
									<td>
										<a href="{{ route('public.privConsultaDetalle', [0]) }}" style="color: #000;">Lorem ipsum dolor sit amet consectetuer dip adipisicing.</a>
									</td>
									<td>John Mayer</td>
									<td>14.08.2018</td>
									<td>Abierto</td>
								</tr>
								</tbody>
							</table>
							<!-- END table-product -->
						</div>
						<!-- END table-responsive -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->


@endsection