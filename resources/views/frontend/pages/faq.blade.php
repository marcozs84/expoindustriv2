@extends('frontend.layouts.default')
@push('css')
<style>
	.page-header-cover:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	#page-header h1.page-header{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:600;
		font-size:35px;
	}

</style>
@endpush
@section('content')

	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-tractor1.jpg" style="margin-top:-250px;" alt="" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header">@lang('messages.frequent_questions')</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">@lang('messages.home')</a></li>
				@if($auth_username != '')
					<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				@endif
				<li class="active">@lang('messages.frequent_questions')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN panel-group -->
			<div class="panel-group faq" id="faq-list">
				<!-- BEGIN panel -->

				<ul class="nav nav-pills nav-justified" style="width:100%; margin:auto; text-align:center">
					<li class="nav-items active">
						<a href="#faq-regular" data-toggle="tab" class=" nav-link active">
							<span class="d-sm-none">@lang('messages.faq_buyers')</span>
							{{--<span class="d-sm-block d-none">Pills Tab 1</span>--}}
						</a>
					</li>
					<li class="nav-items">
						<a href="#faq-sellers" data-toggle="tab" class=" nav-link ">
							<span class="d-sm-none">@lang('messages.faq_sellers')</span>
							{{--<span class="d-sm-block d-none">Pills Tab 2</span>--}}
						</a>
					</li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade active in" id="faq-regular">

				<br>

				@php
					$cant = 23;
					if( App::getLocale() == 'se' ) $cant = 27;
					if( App::getLocale() == 'en' ) $cant = 21;
					if( App::getLocale() == 'es' ) $cant = 20;
				@endphp
				@for($i = 1; $i <= $cant ; $i++)
					@if( __('faq.q_'.$i) != 'faq.q_'.$i)
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" href="#faq-{{ $i }}"><i class="fa fa-question-circle fa-fw m-r-5 text-info"></i>
									@lang('faq.q_'.$i)
								</a>
							</h4>
						</div>
						<div id="faq-{{ $i }}" class="panel-collapse collapse @if($i == 1) in @endif">
							<div class="panel-body">
								<p>
									@lang('faq.a_'.$i)
								</p>
							</div>
						</div>
					</div>
					@endif
				@endfor

					</div>
					<div class="tab-pane fade" id="faq-sellers">

						<br>
						@php
							$cant = 12;
							if( App::getLocale() == 'se' ) $cant = 11;
							if( App::getLocale() == 'en' ) $cant = 12;
							if( App::getLocale() == 'es' ) $cant = 12;
						@endphp
						@for($i = 1; $i <= $cant ; $i++)
							@if( _('faq_sellers.a_'.$i) != 'faq_sellers.q_'.$i)

							<div class="panel panel-inverse">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" href="#faq_sellers-{{ $i }}"><i class="fa fa-question-circle fa-fw m-r-5 text-info"></i>
											@lang('faq_sellers.q_'.$i)
										</a>
									</h4>
								</div>
								<div id="faq_sellers-{{ $i }}" class="panel-collapse collapse @if($i == 1) in @endif">
									<div class="panel-body">
										<p>
											@lang('faq_sellers.a_'.$i)
										</p>
									</div>
								</div>
							</div>
							@endif
						@endfor

					</div>
				</div>

			</div>
			<!-- END panel-group -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection