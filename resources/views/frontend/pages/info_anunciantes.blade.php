@extends('frontend.layouts.default')
@push('css')
<style>

	.section-container.has-bg .cover-bg:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	.about-us h1{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
	}
	.about-us p{
		/*color:rgba(0,0,0,0.63);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
	}

	.breadcrumb>.active {
		color:#ffffff;
	}
</style>
@endpush
@section('content')

	<div id="about-us-cover" class="has-bg section-container">
		<!-- BEGIN cover-bg -->
		<div class="cover-bg">
			<img src="/assets/img/e-commerce/cover/cover-tractor1.jpg" alt="" style="margin-top: 0px; width:100%;" />
		</div>
		<!-- END cover-bg -->
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.information_for', ['subject' => '<b><span style="text-transform:capitalize;">'.__('messages.announcers').'</span></b>'])</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN about-us -->
			<div class="about-us text-center">
				<h1>@lang('announcers.title')</h1>
				<p>
					@lang('announcers.slogan')
				</p>
			</div>
			<!-- END about-us -->
		</div>
		<!-- END container -->
	</div>

	<!-- BEGIN #faq -->
	<div id="faq" class="section-container">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN panel-group -->
			<div class="panel-group faq" id="faq-list">
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#faq-1"><i class="fa fa-question-circle fa-fw m-r-5"></i> 1.  @lang('announcers.title_1')</a>
						</h4>
					</div>
					<div id="faq-1" class="panel-collapse collapse in">
						<div class="panel-body">
							@lang('announcers.body_1')
						</div>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#faq-2"><i class="fa fa-question-circle fa-fw text-muted m-r-5"></i> 2. @lang('announcers.title_2')</a>
						</h4>
					</div>
					<div id="faq-2" class="panel-collapse collapse">
						<div class="panel-body">
							@lang('announcers.body_2')


						</div>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#faq-3"><i class="fa fa-question-circle fa-fw text-primary m-r-5"></i> 3. @lang('announcers.title_3')</a>
						</h4>
					</div>
					<div id="faq-3" class="panel-collapse collapse">
						<div class="panel-body">
							@lang('announcers.body_3')
						</div>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#faq-4"><i class="fa fa-question-circle fa-fw text-primary m-r-5"></i> 4. @lang('announcers.title_4')</a>
						</h4>
					</div>
					<div id="faq-4" class="panel-collapse collapse">
						<div class="panel-body">
							@lang('announcers.body_4')
						</div>
					</div>
				</div>
				<!-- END panel -->
				<div class="panel panel-inverse">
					<div class="row text-center"><img src="/assets/img/e-commerce/buena_suerte_{{ App::getLocale() }}.jpg" alt=""></div>
				</div>
				<!-- END panel -->
			</div>
			<!-- END panel-group -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection