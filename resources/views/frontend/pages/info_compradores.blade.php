@extends('frontend.layouts.default')
@push('css')
<style>

	.section-container.has-bg .cover-bg:before {
		/*background:rgba(235, 173, 21, 0.8);*/
		/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
		background:rgba(52, 143, 226, 0.74);  /* blue */
	}

	.about-us h1{
		/*color:rgba(0,0,0,0.75);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
	}
	.about-us p{
		/*color:rgba(0,0,0,0.63);*/
		/*color:#242a30;*/
		color:#ffffff;
		font-weight:400;
	}

	.breadcrumb>.active {
		color:#ffffff;
	}
</style>
@endpush
@section('content')

	<!-- BEGIN #page-header -->

	<div id="about-us-cover" class="has-bg section-container">
		<!-- BEGIN cover-bg -->
		<div class="cover-bg">
			<img src="/assets/img/e-commerce/cover/cover-tractor1.jpg" alt="" style="width:100%;" />
		</div>
		<!-- END cover-bg -->
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li class="active">
					@lang('messages.information_for_buyers')
				</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN about-us -->
			<div class="about-us text-center">
				<h1>@lang('buyers.title')</h1>
				<p>
					@lang('buyers.slogan')
				</p>
			</div>
			<!-- END about-us -->
		</div>
		<!-- END container -->
	</div>







	<!-- BEGIN #about-us-content -->
	<div id="about-us-content" class="section-container bg-white">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN about-us-content -->
			<div class="about-us-content">
				<h2 class="title text-center"><b>@lang('messages.how_we_do_it')</b></h2>

				<!-- BEGIN row -->
				<div class="row">
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-muted"><i class="fas fa-eye"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_1')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-primary"><i class="fa fa-search"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_2')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-info"><i class="fa fa-lock"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_3')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
				</div>
				<!-- END row -->
				<!-- BEGIN row -->
				<div class="row">
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-danger"><i class="fa fa-map-marker"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_4')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-muted"><i class="fa fa-user-secret"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_5')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-primary"><i class="fas fa-edit"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_6')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
				</div>
				<!-- END row -->
				<div class="row">

					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-info"><i class="fas fa-graduation-cap"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_7')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-danger"><i class="fa fa-check-square"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_8')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
					<!-- begin col-4 -->
					<div class="col-md-4 col-sm-4">
						<div class="service">
							<div class="icon text-danger"><i class="fas fa-hourglass-half"></i></div>
							<div class="info">
								<p class="desc">
									@lang('buyers.buyer_9')
								</p>
							</div>
						</div>
					</div>
					<!-- end col-4 -->
				</div>
			</div>
			<!-- END about-us-content -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-content -->








	<!-- BEGIN #faq -->
	<div id="faq" class="section-container hide" >
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="/">@lang('messages.home')</a></li>
				<li class="active">@lang('messages.information_for', ['subject' => '<b><span style="text-transform:capitalize;">'.__('messages.buyers').'</span></b>'])</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN panel-group -->
			<div class="panel-group faq" id="faq-list">
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-circle fa-fw m-r-5"></i> 1. Nuestro personal realiza controles continuos en anuncios y usuarios.
						</h4>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-circle fa-fw text-muted m-r-5"></i> 2. Revisamos cuidadosamente todos los anuncios antes de que sean aprobados y publicados en ExpoIndustri.com.
						</h4>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-circle fa-fw text-muted m-r-5"></i> 3. Ofrecemos opciones seguras para el pago, tales como, tarjeta de crédito o transferencia bancaria.
						</h4>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-question-circle fa-fw text-primary m-r-5"></i> 3. Ofrecemos opciones seguras para el pago, tales como, tarjeta de crédito o transferencia bancaria.
						</h4>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<i class="fa fa-question-circle fa-fw text-info m-r-5"></i> 4. Tenemos sucursales en los diferentes países en los que ExpoIndustri.com ofrece sus servicios.
						</h4>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#faq-5"><i class="fa fa-question-circle fa-fw text-success m-r-5"></i> 5. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</a>
						</h4>
					</div>
					<div id="faq-5" class="panel-collapse collapse">
						<div class="panel-body">
							<p>
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
								3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
								Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
								Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
								Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
								synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							</p>
						</div>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#faq-6"><i class="fa fa-question-circle fa-fw text-warning m-r-5"></i> 6. Ad vegan excepteur butcher vice lomo.</a>
						</h4>
					</div>
					<div id="faq-6" class="panel-collapse collapse">
						<div class="panel-body">
							<p>
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
								3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
								Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
								Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
								Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
								synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							</p>
						</div>
					</div>
				</div>
				<!-- END panel -->
				<!-- BEGIN panel -->
				<div class="panel panel-inverse">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" href="#faq-7"><i class="fa fa-question-circle fa-fw text-danger m-r-5"></i> 7.  Leggings occaecat craft beer farm-to-table, raw denim aesthetic.</a>
						</h4>
					</div>
					<div id="faq-7" class="panel-collapse collapse">
						<div class="panel-body">
							<p>
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
								3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
								Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
								Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
								Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
								synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							</p>
						</div>
					</div>
				</div>
				<!-- END panel -->
			</div>
			<!-- END panel-group -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #faq -->

@endsection