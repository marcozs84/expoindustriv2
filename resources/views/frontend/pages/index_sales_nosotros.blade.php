<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	{{--<title>ExpoIndustri - @lang('messages.expand_your_business')</title>--}}
	<title>{{ $seo_title }}</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	@foreach($seo_metas as $meta)
	<meta name="{{ $meta->name }}" content="{{ $meta->content }}" />
	@endforeach
	<meta name="author" content="Marco Antonio Zeballos Sanjines <marcozs84@gmail.com>" />
	<meta name="developer" content="Marco Antonio Zeballos Sanjines <marcozs84@gmail.com>" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="/assets/css/one-page-parallax/app.min.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->

	<link href="/assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet" />
	{{--<link href="/assets/css/one-page-parallax/theme/yellow.min.css" rel="stylesheet" id="theme-css-link">--}}
	<link href="/assets/css/one-page-parallax/theme/blue.min.css" rel="stylesheet" id="theme-css-link">
	{{--<link href="/assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css" rel="stylesheet" id="theme-css-link">--}}
	{{--<link href="/assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet">--}}

	<link href="/assets/css/e-commerce/landing_sales.css?v=1" rel="stylesheet" />
	<link href="/assets/css/e-commerce/modalHandler.css?v=2" rel="stylesheet" />


	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
	<link rel="stylesheet" href="/assets/css/carrousel.css">


	{{--<script src='https://www.google.com/recaptcha/api.js?render={{ $global_recaptcha_public_key }}'></script>--}}

	<style>
		.main_circles {
			position:absolute;
			top:100px;
			right:50px;
			max-width:600px;
		}
		.circle_1 {
			top:-40px;
			right:-100px;
			z-index:10;
		}
		.circle_2 {
			top:200px;
			right:70px;
			z-index:15;
		}
		.circle_3 {
			top:500px;
			right:-80px;
			z-index:11;
		}

		.navbar-sm .logotop_white {
			/*display:none;*/
			filter:invert(100);
		}
		/*.navbar-sm .logotop_black {*/
			/*display:block;*/
		/*}*/
		.header.navbar-transparent {
			background-color:#000000;
		}

		.content-bg.bge2:before {
			background: rgba(0, 0, 0, 0.34) !important;
		}

		@media (max-width:750px) {
			.main_circles {
				display:none;
			}
		}

		.team_member {
			text-align:center;
		}
		.team_pic {
			width:80%;
		}
		.team_cargo {
			text-align:center !important;
			font-size:15px;
		}

	</style>
</head>
<body data-spy="scroll" data-target="#header" data-offset="51">
<!-- begin #page-container -->
<div id="page-container" class="fade">


	<!-- begin #header -->
	<div id="header" class="header navbar navbar-transparent navbar-fixed-top navbar-expand-lg">


		<!-- begin container -->
		<div class="container">
			<!-- begin navbar-brand -->

			<a href="/" style="font-size:35px;" class="logotop logotop_white">
				{{--<span style=" color: #fdda01;">Expo</span>Industri--}}
				{{--<small>@lang('messages.import_export')</small>--}}
				<img src="/assets/img/logo/logo_outline_white.png" alt="ExpoIndustri">
			</a>
			{{--<a href="/" style="font-size:35px;" class="logotop logotop_black">--}}
				{{--<span style=" color: #fdda01;">Expo</span>Industri--}}
				{{--<small>@lang('messages.import_export')</small>--}}
				{{--<img src="/assets/img/logo/logo_outline_black.png" alt="ExpoIndustri">--}}
			{{--</a>--}}

			{{--<a href="index.html" class="navbar-brand">--}}
				{{--<span class="brand-logo"></span>--}}
				{{--<span class="brand-text">--}}
						{{--<span class="text-primary">Color</span> Admin--}}
					{{--</span>--}}
			{{--</a>--}}
			<!-- end navbar-brand -->
			<!-- begin navbar-toggle -->
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- end navbar-toggle -->
			<!-- begin navbar-collapse -->
			<div class="collapse navbar-collapse" id="header-navbar">
				<ul class="nav navbar-nav navbar-right">

					<li class="nav-item"><a class="nav-link" href="{{ route('public.index_sales') }}" >Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="{{ route('public.index_nosotros') }}" >Nosotros</a></li>
					<li class="nav-item"><a class="nav-link" href="{{ route('public.index_servicios') }}" >Servicios</a></li>

					<li class="nav-item dropdown">
						<a class="nav-link @if(App::getLocale() != 'es') hide @endif" href="javascript:;" data-click="scroll-to-target" data-scroll-target="#home" data-toggle="dropdown">&nbsp;&nbsp;@lang('messages.spanish') <b class="caret"></b></a>
						<a class="nav-link @if(App::getLocale() != 'en') hide @endif" href="javascript:;" data-click="scroll-to-target" data-scroll-target="#home" data-toggle="dropdown">&nbsp;&nbsp;@lang('messages.english') <b class="caret"></b></a>
						<a class="nav-link @if(App::getLocale() != 'se') hide @endif" href="javascript:;" data-click="scroll-to-target" data-scroll-target="#home" data-toggle="dropdown">&nbsp;&nbsp;@lang('messages.swedish') <b class="caret"></b></a>
						<div class="dropdown-menu dropdown-menu-left animated fadeInDown">
							<a class="dropdown-item @if(App::getLocale() == 'es') hide @endif" onclick="changeLanguage('es')" href="javascript:;">&nbsp;&nbsp;@lang('messages.spanish')</a>
							<a class="dropdown-item @if(App::getLocale() == 'en') hide @endif" onclick="changeLanguage('en')" href="javascript:;">&nbsp;&nbsp;@lang('messages.english')</a>
							<a class="dropdown-item @if(App::getLocale() == 'se') hide @endif" onclick="changeLanguage('se')" href="javascript:;">&nbsp;&nbsp;@lang('messages.swedish')</a>
						</div>
					</li>



					{{--<li class="nav-item"><a class="nav-link" href="#home" data-click="scroll-to-target">@lang('messages.home')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#about" data-click="scroll-to-target">@lang('messages.about_us')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#milestone" data-click="scroll-to-target">EXPOINDUSTRI</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#partners" data-click="scroll-to-target">@lang('messages.partners')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#client" data-click="scroll-to-target">VENDER</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#pricing" data-click="scroll-to-target">@lang('messages.pricing')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#news" data-click="scroll-to-target">@lang('messages.news')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#categories" data-click="scroll-to-target">@lang('messages.categories')</a></li>--}}
					{{--<li class="nav-item"><a class="nav-link" href="#contact" data-click="scroll-to-target">@lang('messages.contact_us')</a></li>--}}
					{{--<li class="nav-item mobile_announce mobile_login"><a class="nav-link" href="{{ route('public.login') }}" >@lang('messages.log_in')</a></li>--}}


					{{--Comprar - NO--}}
					{{--Inicio--}}
					{{--Nosotros--}}
					{{--ExpoIndustri--}}
					{{--Socios--}}
					{{--Vender--}}
					{{--Categorias--}}
					{{--Contacto--}}
				</ul>
			</div>
			<!-- end navbar-collapse -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #header -->

	<!-- begin #home -->
	<div id="home" class="content has-bg home" style="background-color: #000;">
		<!-- begin content-bg -->
		<div class="content-bg bge2" style="background-image: url(/assets/img/bg/bg-container-ship-small.jpg);"
		     data-paroller="true"
		     data-paroller-factor="0.5"
		     data-paroller-factor-xs="0.25">
		</div>


		<!-- begin container -->
		<div class="container home-content">
			{{--<h1>@lang('messages.welcome_to') ExpoIndustri</h1>--}}

			<h1 style="width:60%; text-align:left;" class="titleh1">Esto es Expoindustri</h1>
			<h3 style="width:50%; text-align:left;" class="titleh3 ">Existimos para ser parte de su equipo de trabajo</h3>
			{{--<p>--}}
				{{--We have created a multi-purpose theme that take the form of One-Page or Multi-Page Version.<br />--}}
				{{--Use our <a href="#">theme panel</a> to select your favorite theme color.--}}
			{{--</p>--}}
			{{--<a href="#" class="btn btn-theme">Buscar maquinaria</a>--}}
			{{--<a href="#" class="btn btn-outline">Deseo Expandir mis ventas</a><br />--}}
			{{--<br />--}}
			{{--or <a href="#">subscribe</a> newsletter--}}

			{{--<a href="{{ route('public.buy_page') }}" target="_blank" class="btn btn-theme btn-primary" style="background-color:#D9C400; border-color:#D9C400; color:#000000;"><i class="fa fa-search"></i> @lang('messages.find_your_machine')</a>--}}
			{{--<a href="{{ route('public.buy_page') }}" target="_blank" class="btn btn-theme btn-primary" style="background-color:#D9C400; border-color:#D9C400; color:#000000;"><i class="fa fa-search"></i> @lang('messages.machine_storage')</a>--}}
			{{--<a href="#pricing" data-click="scroll-to-target" class="btn btn-theme btn-white"><i class="fa fa-chart-line"></i> @lang('messages.start_selling')</a><br />--}}
			<br /><br />
			{{--<div style="color:#ffffff;" data-animation="true" data-animation-type="fadeInUp"><a href="javascript:;" onclick="displayWelcomeBanner()" style="color:#ffffff; font-weight:bold">@lang('messages.view_explained_video')</a></div>--}}
		</div>
		<!-- end container -->
	</div>
	<!-- end #home -->


	<!-- begin #team -->
	<div id="partners" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container">
			{{--<h2 class="content-title">¿ Porqué trabajar con nosotros ?</h2>--}}
			{{--<p class="content-desc">--}}
				{{--@lang('messages.our_services_slogan')--}}
			{{--</p>--}}
			{{--<p class="content-desc">--}}
				{{--Phasellus suscipit nisi hendrerit metus pharetra dignissim. Nullam nunc ante, viverra quis<br />--}}
				{{--ex non, porttitor iaculis nisi.--}}
			{{--</p>--}}
			<!-- begin row -->
			<div class="row">
				<!-- begin col-4 -->
				<div class="col-lg-6 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInLeft">
					<p style="font-size:1.6em; padding-bottom:60px;">
						Si tiene una pequeña empresa que esta iniciando su viaje al éxito o es una empresa mediana o grande, que desea expandirse, mejorar su tecnología o aumentar su producción, somos el socio perfecto para usted. Somos un jugador mas en su equipo, que trabaja para cumplir las metas de su empresa.
					</p>
				</div>

				<div class="col-lg-6 col-md-12 col-sm-12 text-center" style="padding-bottom:60px;" data-animation="true" data-animation-type="fadeInRight">
					<div style="width: 200%;
    height: 100%;
    background-color: #9fce62;
    position: absolute;
    z-index: -200;
    right: -150%;
    top: -10%;">

					</div>
					<div style="width:100%;" class="text-center">
						<img src="/assets/img/sales/compound_1.png" style="width:100%; margin:auto;" alt="">
					</div>
				</div>
				<!-- end col-4 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end container -->
	</div>
	<!-- end #team -->

	<!-- begin #team -->
	<div id="blue_block" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container">



			<div class="row">
				<!-- begin col-4 -->


				<div class="col-lg-6 col-md-12 col-sm-12 text-center" style="padding-bottom:60px;" data-animation="true" data-animation-type="fadeInLeft">
					<div style="width: 200%;
    height: 100%;
    background-color: #B4C7E7;
    position: absolute;
    z-index: -200;
    left: -150%;
    top: -10%;">

					</div>
					<div style="width:100%;" class="text-center">
						<img src="/assets/img/sales/puerto.jpg" style="width:100%; margin:auto;" alt="">
					</div>
				</div>

				<!-- end col-4 -->

				<div class="col-lg-6 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInRight">
					<p style="font-size:1.6em; padding-bottom:60px;">
						La calidad y la tecnología de las máquinas europeas puede ser una gran diferencia en sus resultados de producción/fabricación y más cuando tiene la ventaja de comprar a precios altamente competitivos.
					</p>
				</div>
			</div>
			<!-- end row -->



			<div class="row">
				<!-- begin col-4 -->
				<div class="col-lg-6 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInLeft">
					<p style="font-size:1.6em; padding-top:10px; padding-bottom:60px;">
						En Sudamérica, hay muchos compradores potenciales que no encuentran la máquina que buscan en Europa, o tal vez ya la hayan encontrado, pero necesitan quien les brinde <b>seguridad</b> y <b>garantía</b> sobre la compra, además de <b>ayuda con todo el proceso de envío</b>.
						<br><br>
						Para todos ellos esta EXPOINDUSTRI.

					</p>
				</div>

				<div class="col-lg-6 col-md-12 col-sm-12 text-center" style="padding-bottom:60px;" data-animation="true" data-animation-type="fadeInRight">
					<div style="width: 200%;
    height: 100%;
    background-color: #B4C7E7;
    position: absolute;
    z-index: -200;
    right: -150%;
    top: -10%;">

					</div>
					<div style="width:100%;" class="text-center">
						<img src="/assets/img/sales/planet.jpg" style="width:100%; margin:auto;" alt="">
					</div>
				</div>
				<!-- end col-4 -->
			</div>
		</div>
		<!-- end container -->
	</div>
	<!-- end #team -->

	<!-- begin #team -->
	<div id="historia" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container">

			<h2 class="content-title" data-animation="true" data-animation-type="fadeInUp">História</h2>

			<div class="row">
				<!-- begin col-4 -->


				<div class="col-lg-6 col-md-12 col-sm-12 text-center" style="padding-bottom:60px;" data-animation="true" data-animation-type="fadeInLeft">
					<div style="width: 200%;
    height: 100%;
    background-color: #9fce62;
    position: absolute;
    z-index: -200;
    left: -150%;
    top: -10%;">

					</div>
					<div style="width:100%;" class="text-center">
						<img src="/assets/img/sales/joakim_historia_bw.jpg" style="width:60%; margin:auto;" alt="">
					</div>
				</div>

				<!-- end col-4 -->

				<div class="col-lg-6 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInRight">
					<p style="font-size:1.5em; padding-bottom:60px;">
						Expoindustri fue fundada oficialmente en 2014 por Joakim Byrén, pero la historia comienza varias décadas antes, cuando la familia Byrén se mudó de Suecia a Bolivia en 1956 y con el tiempo creó fuertes relaciones comerciales entre Europa y Sudamérica. Joakim, al nacer en Suecia con una madre sueca y un padre boliviano, quienes le enseñaron a ser un emprendedor que siempre busque servir a través de su trabajo, sintió desde muy pequeño una gran pasión por los negocios que implicaban actividades entre Europa y Sudamérica. Joakim solía visitar con su padre a los proveedores de maquinaria industrial en Europa y a través de Expoindustri inició su carrera en el mismo rubro.
					</p>
				</div>
			</div>
			<!-- end row -->



			<div class="row" style="margin-top:60px;">
				<!-- begin col-4 -->
				<div class="col-lg-6 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInLeft">
					<p style="font-size:1.5em; padding-top:10px; padding-bottom:60px;">
						En 2014 Expoindustri Chile Ltda abrió sus puertas en Iquique, Chile y Expoindustri Sweden AB lo hizo en 2018 en Tenhult, Suecia. Todo el modelo de negocio se basa en la experiencia de Joakim y en el hecho de que todavía es muy difícil, tanto para distribuidores de maquinaria europeos como para compradores sudamericanos, hacer negocios de forma rápida, eficiente y segura, debido a factores como el idioma, la distancia y la búsqueda de socios confiables para todo el proceso logístico de principio a fin. Es así que Marco Zeballos y Jurgen Robles, sumaron su experiencia en Desarrollo-IT y Logística a esta gran idea y los tres, junto a su equipo de campo, unen esfuerzos para trabajar siempre en armonía con los valores iniciales.
					</p>
				</div>

				<div class="col-lg-6 col-md-12 col-sm-12 text-center" style="padding-bottom:60px;" data-animation="true" data-animation-type="fadeInRight">
					<div style="width: 200%;
    height: 100%;
    background-color: #9fce62;
    position: absolute;
    z-index: -200;
    right: -150%;
    top: -10%;">

					</div>
					<div style="width:100%; padding-top:20px;" class="text-center">
						<img src="/assets/img/sales/marco_jurgen_historia.jpg" style="width:120%; margin-left:20px;" alt="">
					</div>
				</div>
				<!-- end col-4 -->
			</div>
		</div>
		<!-- end container -->
	</div>
	<!-- end #team -->





	<!-- begin #team -->
	<div id="colegas" class="content" data-scrollview="true">
		<!-- begin container -->
		<div class="container">

			<h2 class="content-title" data-animation="true" data-animation-type="fadeInUp">Saluda a nuestros colegas</h2>

			<div class="row">
				<!-- begin col-4 -->


				<div class="col-lg-8 col-md-12 col-sm-12 text-center" style="padding-bottom:60px;" data-animation="true" data-animation-type="fadeInLeft">
					<div style="width: 200%;
    height: 100%;
    background-color: #9fce62;
    position: absolute;
    z-index: -200;
    left: -150%;
    top: -10%;">

					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Joakim.png" alt="">
							<p class="team_cargo"><strong>Joakim Byren</strong> <br> @lang('messages.cargo_joakim')</p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Sandra.png" alt="">
							<p class="team_cargo"> <strong>Sandra Pereira</strong> <br> @lang('messages.cargo_emily') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Claudia.png" alt="">
							<p class="team_cargo"> <strong>Claudia Byren</strong> <br> @lang('messages.cargo_claudia') </p>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_carla.png" alt="">
							<p class="team_cargo"> <strong>Carla Gutierrez</strong> <br> @lang('messages.cargo_carla') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Ernesto.png" alt="">
							<p class="team_cargo"> <strong>Ernesto Gonzalez</strong> <br> @lang('messages.cargo_ernesto') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Iracema.png" alt="">
							<p class="team_cargo"> <strong>Iracema Martins de Faria</strong> <br> @lang('messages.cargo_iracema') </p>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Jurgen.png" alt="">
							<p class="team_cargo"> <strong>Jurgen Robles</strong> <br> @lang('messages.cargo_jurgen') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Marco.png" alt="">
							<p class="team_cargo"> <strong>Marco A. Zeballos</strong> <br> @lang('messages.cargo_marco') </p>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6 team_member">
							<img class="team_pic" src="/assets/img/e-commerce/team/circle_Jose_Julian.png" alt="">
							<p class="team_cargo"> <strong>José Julian Gutierrez</strong> <br> @lang('messages.cargo_jose') </p>
						</div>

					</div>
				</div>

				<!-- end col-4 -->

				<div class="col-lg-4 col-md-12 col-sm-12" data-animation="true" data-animation-type="fadeInRight">
					<p style="font-size:1.5em; padding-bottom:60px;">
						Aquí encontrará las personas que conforman el equipo entusiasta y apasionado por las metas de su empresa. Somos un experimentado equipo internacional, con un servicio a medida y personalizado. Juntos, ayudamos a resolver problemas y responder preguntas sobre exportación-importación de máquinas, procesos logísticos, inspecciones técnicas y todo lo que su empresa necesita.
					</p>
				</div>

				<div class="row col-lg-12">
					<h3 class="content-title" data-animation="true" data-animation-type="fadeInUp">¡En Expoindustri trabajamos para dar  a nuestros clientes, una solución a medida y al precio mas competitivo!</h3>
				</div>
			</div>
			<!-- end row -->



		</div>
		<!-- end container -->
	</div>
	<!-- end #team -->





	<!-- begin #footer -->
	<div id="footer" class="footer">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-3 -->
				{{--<div class="col-md-3" hidden>--}}
					{{--<h4 class="footer-header">@lang('messages.about_us')</h4>--}}
					{{--<p>--}}
						{{--@lang('messages.about_us_footer')--}}
					{{--</p>--}}

				{{--</div>--}}
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.site_map')</h4>
					<ul class="fa-ul">
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.nosotros") }}">@lang('messages.about_us')</a></li>--}}
{{--						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_anunciantes") }}">@lang('messages.information_for_announcers')</a></li>--}}
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_compradores") }}">@lang('messages.information_for_buyers')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.faq') }}">@lang('messages.frequent_questions')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.servicios") }}">@lang('messages.services')</a></li>
						{{--							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.toc') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.termsAndConditions') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.dataProtectionPolicy') }}">@lang('messages.privacy_policy')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.contact") }}">@lang('messages.contact_us')</a></li>--}}
					</ul>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.terms')</h4>
					<ul class="fa-ul">
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.nosotros") }}">@lang('messages.about_us')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_anunciantes") }}">@lang('messages.information_for_announcers')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_compradores") }}">@lang('messages.information_for_buyers')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.faq') }}">@lang('messages.frequent_questions')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.servicios") }}">@lang('messages.services')</a></li>--}}
						{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.toc') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.termsAndConditions') }}">@lang('messages.legal_terms_conditions')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.dataProtectionPolicy') }}">@lang('messages.privacy_policy')</a></li>
						<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.index_sales") }}#contact">@lang('messages.contact_us')</a></li>
					</ul>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.central_office')</h4>
					<address>
						<strong><i class="flag-icon flag-icon-se" style="font-style: 20px;"></i> @lang('messages.sweden')</strong><br />
						Jönköpingsvägen 27 <br>
						561 61 Tenhult <br>
						{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +46 (0) 723 933 700<br />--}}
						<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />

						{{--<br />--}}
						{{--<strong>Bolivia</strong><br />--}}
						{{--1355 Market Street, Suite 900<br />--}}
						{{--San Francisco, CA 94103<br />--}}
						{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
						{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

						{{--<br />--}}
						{{--<strong><i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> Chile</strong><br />--}}
						{{--Oficina mapocho 48D<br />--}}
						{{--Zofri - Iquique<br />--}}
						{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
						{{--<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />--}}

						{{--<br />--}}
						{{--<strong>Peru</strong><br />--}}
						{{--1355 Market Street, Suite 900<br />--}}
						{{--San Francisco, CA 94103<br />--}}
						{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
						{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

						<br>
						<b><a href="{{ route("public.{$glc}.contact") }}">@lang('messages.our_offices_southamerica')</a></b>

					</address>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3">
					<h4 class="footer-header">@lang('messages.distrib_office')</h4>
					<address>
						<strong><i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> Chile</strong><br />
						Oficina mapocho 48D<br />
						Zofri - Iquique<br />
						{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
						<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />

						{{--<br />--}}
						{{--<strong>Peru</strong><br />--}}
						{{--1355 Market Street, Suite 900<br />--}}
						{{--San Francisco, CA 94103<br />--}}
						{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
						{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

					</address>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-md-3 hide">
					<h4 class="footer-header">Oficinas en Europa</h4>
					<address>
						<strong>Twitter, Inc.</strong><br />
						1355 Market Street, Suite 900<br />
						San Francisco, CA 94103<br /><br />
						<br>
						<strong>Av. Las palmas, Inc.</strong><br />
						1355 Market Street, Suite 900<br />
						San Francisco, CA 94103<br /><br />

						<abbr title="Phone">Phone:</abbr> (123) 456-7890<br />
						<abbr title="Fax">Fax:</abbr> (123) 456-7891<br />
						<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />
						<abbr title="Skype">Skype:</abbr> <a href="skype:myshop">myshop</a>
					</address>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				{{--<div class="col-md-3 p-0 text-center" style="/* padding:20px !important; background-color:#ffffff; */" hidden>--}}
					{{--<style>--}}
					{{--#loc_map {--}}
					{{--height: 270px;--}}
					{{--width: 100%;--}}
					{{--}--}}
					{{--</style>--}}
					{{--<div id="loc_map"></div>--}}
					{{--@if($global_isProvider == true)--}}
					{{--@endif--}}


					{{--@if(App::getLocale() == 'se')--}}
					{{--<img src="/assets/img/e-commerce/FooterLogoWhite_Export.png" alt="" style="width:100%; ">--}}
					{{--@else--}}
					{{--<img src="/assets/img/e-commerce/FooterLogoWhite_Import.png" alt="" style="width:100%; ">--}}
					{{--@endif--}}

					{{--<img src="/assets/img/e-commerce/uc_logo_{{ App::getLocale() }}.png" alt="" style="width:90%; ">--}}

					{{--<img src="/assets/img/e-commerce/SmallLogo.png" alt="Map Route Export" style="width:100%;">--}}
					{{--<img src="/assets/img/e-commerce/256_iso_fav.png" alt="Map Route Export" style="width:100%;">--}}
					{{--<img src="/assets/img/e-commerce/mapa_ruta_export.png" alt="Map Route Export" style="width:100%;">--}}
					{{--<img src="/assets/img/e-commerce/sello_footer1.png" alt="Map Route Export" style="max-height:230px; ">--}}

					{{--<img src="/assets/img/logo/logo_exind2_{{ session('lang', 'en') }}.png" alt="Map Route Export" style="width:100%; background-color:#ffffff; pading:10px;">--}}

				{{--</div>--}}
				<!-- END col-3 -->
			</div>
			<!-- END row -->
		</div>
		<!-- END container -->
	</div>
	<!-- end #footer -->

	<!-- begin theme-panel -->
	{{--<div class="theme-panel">--}}
		{{--<a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>--}}
		{{--<div class="theme-panel-content">--}}
			{{--<ul class="theme-list clearfix">--}}
				{{--<li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="../assets/css/one-page-parallax/theme/red.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-pink" data-theme="pink" data-theme-file="../assets/css/one-page-parallax/theme/pink.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Pink" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="../assets/css/one-page-parallax/theme/orange.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-yellow" data-theme="yellow" data-theme-file="../assets/css/one-page-parallax/theme/yellow.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Yellow" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-lime" data-theme="lime" data-theme-file="../assets/css/one-page-parallax/theme/lime.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Lime" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-green" data-theme="green" data-theme-file="../assets/css/one-page-parallax/theme/green.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Green" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li class="active"><a href="javascript:;" class="bg-teal" data-theme-file="../assets/css/one-page-parallax/theme/teal.min.css" data-theme="default" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-aqua" data-theme="aqua" data-theme-file="../assets/css/one-page-parallax/theme/aqua.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Aqua" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="../assets/css/one-page-parallax/theme/blue.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="../assets/css/one-page-parallax/theme/purple.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-indigo" data-theme="indigo" data-theme-file="../assets/css/one-page-parallax/theme/indigo.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Indigo" data-original-title="" title="">&nbsp;</a></li>--}}
				{{--<li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="../assets/css/one-page-parallax/theme/black.min.css" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black" data-original-title="" title="">&nbsp;</a></li>--}}
			{{--</ul>--}}
		{{--</div>--}}
	{{--</div>--}}
	<!-- end theme-panel -->




	<div class="modal fade" id="modal-newsletter">
		<div class="modal-dialog video-dialog" style="">
			<div class="modal-content" style="-webkit-box-shadow: none !important;  -moz-box-shadow: none !important; box-shadow: none !important; ">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">Newsletter</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body" style="background-color:#000000; padding:0;">

					<!-- beign #action-box -->
					<div id="action-box" class="content has-bg" data-scrollview="true" style="border: 5px solid #ffffff;">
						<!-- begin content-bg -->
						<div class="content-bg" style="background-image: url(/assets/img/e-commerce/cover/cover-crane_opt.jpg)"
						     data-paroller-factor="0.5"
						     data-paroller-factor-md="0.01"
						     data-paroller-factor-xs="0.01">
						</div>
						<!-- end content-bg -->

						<div class="container newsletter_info" style="display:none;" >
							<div class="row action-box" style="padding: 10px; background: rgb(30 32 39 / 0.7);">
								<p class="newsletter_description col-md-12 m-t-5 m-b-0">
									Subscribed!
								</p>
							</div>
						</div>
						<!-- begin container -->
						<div class="container newsletter_box" data-animation="true" data-animation-type="fadeInRight">
							<!-- begin row -->
							<div class="row action-box" style="padding: 10px; background: rgb(30 32 39 / 0.8);">
								<!-- begin col-9 -->
								<div class="col-md-9 col-sm-12">
									<div class="icon-large text-primary">
										<i class="fa fa-envelope-open-text" style="color:#ffffff !important; padding-top: 7px; margin-top:3px;"></i>
									</div>
									<h3 style="font-weight: 600 !important; color: #000000 !important; padding-top: 10px;"><input class="newsletter_email" type="text" id="newsletter_email_modal" name="newsletter_email_modal" placeholder="E-mail"></h3>
								</div>
								<!-- end col-9 -->
								<!-- begin col-3 -->
								<div class="col-md-3 col-sm-12">
									<a href="javascript:;" onclick="subscribe('newsletter_email_modal')" style="font-weight: 900; text-align:center; margin-top: 10px;" class="btn btn-primary btn-sm btn-theme btn-block btn-newsletter">@lang('messages.send')</a>
								</div>
								<!-- end col-3 -->

								<p class="newsletter_description col-md-12 m-t-5" style="font-weight:600;">
									@lang('messages.newsletter_description')
								</p>
							</div>
							<!-- end row -->
						</div>
						<!-- end container -->
					</div>
					<!-- end #action-box -->



				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modal-welcome-banner">
		<div class="modal-dialog video-dialog" style="">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('messages.welcome')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body" style="background-color:#000000; padding:0;">
					<video id="welcome_video" class="video-js vjs-default-skin"
					       controls preload="auto" width="100%"
							{{--@if( App::getLocale() == 'se' )--}}
								{{--poster="/assets/banner_se.jpeg"--}}
							{{--@elseif( App::getLocale() == 'en' )--}}
								{{--poster="/assets/banner_en_2.jpeg"--}}
							{{--@else--}}
								{{--@if($global_isProvider == true)--}}
									{{--poster="/assets/banner_es_2_vende.jpeg"--}}
								{{--@else--}}
									{{--poster="/assets/banner_es_compra.jpeg"--}}
								{{--@endif--}}
							{{--@endif--}}
					       data-setup='{"example_option":true}'>

						{{--<source src="http://video-js.zencoder.com/oceans-clip.webm" type="video/webm" />--}}
						{{--<source src="http://video-js.zencoder.com/oceans-clip.ogv" type="video/ogg" />--}}
						<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
					</video>
				</div>
			</div>
		</div>
	</div>



	<div class="modal fade" id="modal-noticias">
		<div class="modal-dialog modal-60">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" style="display: inline;">@lang('messages.news')</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					Loading...
				</div>
				<div class="modal-footer">

					<a href="mailto:info@expoindustri.com" target="_blank" class="btn btn-primary"><i class="fa fa-envelope"></i> @lang('messages.send_offer')</a>
					<a href="javascript:;" onclick="move_to_form()" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-paper-plane"></i> @lang('messages.contact_us2')</a>
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">@lang('messages.close')</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade loginRegion" id="modalLoginRegister">
		<div class="modal-dialog" >
			<div class="modal-content" style="margin-top:50px;">
				<div class="modal-body">
					<div class="login-form hide">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.log_in')</h4>
						</div>
						<form id="formLoginTop" action="{{ route('public.login') }}" method="post" class="text-center">
							{{ csrf_field() }}
							<input type="text" name="email" id="loginTopUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
							<input type="password" name="password" id="loginTopPassword" class="login-input" placeholder="@lang('messages.password')" >

							<div id="loginTopErrors"></div>

							<input type="checkbox" name="remember" id="frmRememberTop" style="margin-top:20px;" /> @lang('messages.remember_me')
							<br>
							{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.log_in')">--}}
							<button type="submit" id="regTopBtnEntrar" form="formLoginTop" value="@lang('messages.log_in')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.log_in')</button>
							{{--<a href="javascript:;" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" onclick="tryLogin()">Entrar</a>--}}
							{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Entrar">--}}
							<br>
							<a href="javascript:;" class="" onclick="activateLoginRegister('forgot')" style="font-size:12px; /*color:#333333;*/">@lang('messages.forgot_password')</a>

						</form>
						<hr />
						<p class="text-center">
							<a href="javascript:;" class="" onclick="activateLoginRegister('register')" style="font-size:12px; /*color:#333333;*/">@lang('messages.create_account')</a>
						</p>
					</div>
					<div class="register-form hide">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.register')</h4>
						</div>
						<form id="formRegisterTop"  action="{{ route('public.register') }}" method="post" class="text-center">
							<input type="text" name="email" id="regTopUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
							<div id="regTopError_email"></div>

							<input type="text" name="apellidos" id="regTopApellidos" class="login-input" placeholder="@lang('messages.lastname')" >
							<div id="regTopError_apellidos"></div>

							<input type="text" name="nombres" id="regTopNombres" class="login-input" placeholder="@lang('messages.firstname')" >
							<div id="regTopError_nombres"></div>

							<input type="text" name="empresa" id="regTopEmpresa" class="login-input" placeholder="@lang('messages.company')" >
							<div id="regTopError_empresa"></div>

							<input type="text" name="nit" id="regTopNit" class="login-input" placeholder="@lang('messages.company_nit')" >
							<div id="regTopError_nit"></div>

							{{--<select name="pais" id="regTopPais" class="login-input">--}}
							{{--<option value="">@lang('messages.select_country')</option>--}}
							{{--</select>--}}
							{!! Form::select('pais', $global_paises, $global_country_abr, [
										'id' => 'regTopPais',
										'class' => 'login-input',
										'placeholder' => __('messages.select_country')
							]); !!}
							<div id="regTopError_pais"></div>

							<input type="text" name="telefono" id="regTopTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
							<div id="regTopError_telefono"></div>

							<input type="password" name="password" id="regTopPassword" class="login-input" placeholder="@lang('messages.password')" >
							<div id="regTopError_password"></div>

							<input type="password" name="password_confirmation" id="regTopPassword2" class="login-input" placeholder="@lang('messages.password_confirmation')" >
							<div id="regTopError_password_confirm"></div>

							<br>
							{!! Form::checkbox('legalterms', 'legalterms', null, [
								'id' => 'legalterms',
								'style' => 'font-size:15px; margin:0; vertical-align:middle;'
							]) !!}
							<label class="control-label " for="legalterms">
								@lang('messages.accept_terms', ['link' => '<a href="javascript:;" onclick="modalTerms()" style="font-weight:bold;">'.__('messages.legal_terms').'</a>'])
							</label>

							{{--<input type="submit" id="regTopBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
							<button type="submit" id="regTopBtnEnviar" form="formRegisterTop" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
							{{--<a href="javascript:;" id="regTopBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}
						</form>
						<hr />
						<p class="text-center">
							<a href="javascript:;" class="" onclick="activateLoginRegister('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
						</p>
					</div>
					<div class="forgot-form hide">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.forgot_password')</h4>
						</div>
						<form id="formForgotTop"  action="{{ route('public.forgotPassword') }}" method="post" class="text-center">
							<p>
								@lang('messages.forgot_text')
							</p>
							<input type="text" name="forgotEmail" id="regTopForgotEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
							<div id="regTopError_forgotEmail"></div>

							{{--<input type="submit" id="regTopBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
							<button type="submit" id="regTopBtnForgot" form="formForgotTop" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
							{{--<a href="javascript:;" id="regTopBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}
						</form>
						<hr />
						<p class="text-center">
							<a href="javascript:;" class="" onclick="activateLoginRegister('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
						</p>
					</div>
				</div>
			</div>
		</div>




</div>
<!-- end #page-container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="/assets/js/one-page-parallax/app.min.js"></script>

<script src="/assets/js/e-commerce/modalHandler.js?r={{ rand(1,111) }}"></script>
<script src="/assets/js/e-commerce/layout.js?r={{ rand(1,111) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/gsap.min.js" integrity="sha512-1dalHDkG9EtcOmCnoCjiwQ/HEB5SDNqw8d4G2MKoNwjiwMNeBAkudsBCmSlMnXdsH8Bm0mOd3tl/6nL5y0bMaQ==" crossorigin="anonymous"></script>

	{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>

	{{--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
<!-- ================== END BASE JS ================== -->

<script>

	var totalHeight = $(window).height();
	var circleHeight = totalHeight / 2;
	$('.main_circles').css('max-height', circleHeight + "px");
	$('.circle_1').css('top', ( ( circleHeight / 2 ) * (1-1)) + "px");
	$('.circle_2').css('top', ( ( circleHeight / 2 ) * (2-1)) + "px");
	$('.circle_3').css('top', ( ( circleHeight / 2 ) * (3-1)) + "px");
	$('.circle_white').css({
		'top': "0",
		'right': "0",
		'width': (circleHeight / 3) + "px",
		'height': totalHeight + "px",
		'max-height': totalHeight + "px",
		'background-color': "#ffffff"
	});

//	$('#home').height($(window).height());
	$(window).on('resize', function() {
//		$('#home').height($(window).height());

		totalHeight = $(window).height();
		circleHeight = totalHeight / 2;
		$('.main_circles').css('max-height', circleHeight + "px");
		$('.circle_1').css('top', ( ( circleHeight / 2 ) * (1-1)) + "px");
		$('.circle_2').css('top', ( ( circleHeight / 2 ) * (2-1)) + "px");
		$('.circle_3').css('top', ( ( circleHeight / 2 ) * (3-1)) + "px");
		$('.circle_white').css({
			'top': "0",
			'right': "0",
			'width': (circleHeight / 3) + "px",
			'height': totalHeight + "px",
			'max-height': totalHeight + "px",
			'background-color': "#ffffff"
		});

	});



	var tl2 = gsap.timeline({defaults: { ease: Back.easeOut.config(2), opacity:0 }});
	var tl3 = gsap.timeline({defaults: { ease: "power4.out" }, repeat:-1});


	tl2.from('.titleh1', { delay:1, duration: 3, transformOrigin: 'center', marginLeft:-700})
		.from('.titleh3', { duration: 3, transformOrigin: 'center', marginLeft:-700}, '<0.3')
		.from('.titleh32', { duration: 3, transformOrigin: 'center', marginLeft:-700}, '<0.3');


	tl3.from('.title_1',    {duration: 3, transformOrigin:'center', display:'none', marginTop:-70})
		.to('.title_1',     {duration: 3, transformOrigin:'center', marginTop:150})
		.from('.title_2',   {duration: 3, transformOrigin:'center', display:'none', marginTop:-110}, '<0.3')
		.to('.title_2',     {duration: 3, transformOrigin:'center', marginTop:150})
		.from('.title_3',   {duration: 3, transformOrigin:'center', display:'none', marginTop:-110}, '<0.3')
		.to('.title_3',     {duration: 3, transformOrigin:'center', marginTop:150});

	$(document).ready(function(){



		// ---------- CAROUSEL BRANDS

		$('.brand-carousel').owlCarousel({
			loop:true,
			margin:10,
			autoplay:true,
			nav: false,
			dots:false,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},
				1000:{
					items:5
				}
			}
		})


	});


	// ---------- TOP BLACK BAR FUNCTIONS
	function changeLanguage(lang){
		var url = '{{ route('public.changeLanguage') }}';
		var data = {
			lang: lang,
		};
		ajaxPost(url, data,{
			silent:true,
			onSuccess: function(data){
				location.reload();
			},
			onError: function(e){

			},
			onValidation: function(e){

			}
		});

	}

	function displayWelcomeBanner() {

		var video = document.getElementById('welcome_video');
		var source = document.createElement('source');

		@if( App::getLocale() == 'es' )
			@if($global_isProvider == true)
				source.setAttribute('src', '/videos/welcome_video_es_vende_720.mp4');
		@else
source.setAttribute('src', '/videos/welcome_video_es_compra.mp4');
		@endif
	@elseif( App::getLocale() == 'en' )
source.setAttribute('src', '/videos/welcome_video_en_720.mp4');
		@else
			source.setAttribute('src', '/videos/welcome_video_se_720.mp4');
		@endif

		video.appendChild(source);


		var modalWelcome = $('#modal-welcome-banner').modal();
		{{--ajaxPost('{{ route('public.setWelcomeBanner') }}',{ nuevoUsuario: 1 } );--}}

		modalWelcome.on('shown.bs.modal', function (e) {
			$("#welcome_video").each(function () { this.play() });
		});

		modalWelcome.on('hidden.bs.modal', function (e) {
			$("#welcome_video").each(function () { this.pause() });
		});
	}
</script>

</body>
</html>
