@extends('frontend.layouts.default')
@section('content')

	@include('frontend.includes.topMessage')

	<!-- BEGIN #my-account -->
	<div id="about-us-cover" class="section-container p-t-20">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			<ul class="breadcrumb m-b-10 f-s-12">
				<li><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>
				<li><a href="{{ route("public.{$glc}.privCuenta") }}">@lang('messages.my_account')</a></li>
				<li class="active">@lang('messages.my_credit_debit_cards')</li>
			</ul>
			<!-- END breadcrumb -->
			<!-- BEGIN account-container -->
			<div class="account-container">
				<!-- BEGIN account-sidebar -->
				<div class="account-sidebar">
					<div class="account-sidebar-cover">
						<img src="/assets/img/e-commerce/cover/cover-14.jpg" alt="" />
					</div>
					<div class="account-sidebar-content">
						<h4>@lang('messages.my_credit_debit_cards')</h4>
						<p>
							@lang('messages.my_credit_debit_cards_msg')
						</p>
					</div>
				</div>
				<!-- END account-sidebar -->
				<!-- BEGIN account-body -->
				<div class="account-body">
					<!-- BEGIN row -->
					<div class="row">
						<!-- BEGIN col-6 -->
						<div class="col-md-12">
							<div class="row">
								<div class="col col-md-12">
									<h4><i class="fab fa-cc-visa fa-fw text-muted"></i> @lang('messages.my_credit_debit_cards')</h4>
									<br>
									<form id="formActualizarCorreo" action="{{ route('private.changePassword') }}" method="POST">
										{{ csrf_field() }}
										<fieldset>
											<div class="alert alert-warning">
												<p>Recuerde que el nuevo correo electrónico será su usuario de acceso al sistema.</p>
											</div>
											{{--<legend class="m-b-15">Actualizar correo y contraseña</legend>--}}
											<div class="form-group row m-b-15">
												<label class="col-md-4 col-form-label">Correo electrónico actual</label>
												<div class="col-md-8">
													<input id="email" type="text" value="{{ Auth::guard('clientes')->user()->username }}" class="form-control" placeholder="Contraseña actual">
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-3 col-offset-3">
													<a href="javascript:;" onclick="actualizarCorreo()" class="btn btn-sm btn-primary m-r-5">Guardar</a>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
						</div>
						<!-- END col-6 -->
					</div>
					<!-- END row -->
				</div>
				<!-- END account-body -->
			</div>
			<!-- END account-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END #about-us-cover -->

@endsection

@push('scripts')
<script>

	function formInit(){
	}
	formInit();

	// ----------

	$( "#formActualizarCorreo" ).submit(function( event ) {
		actualizarCorreo();
		event.preventDefault();
	});

	function actualizarCorreo(){
		var url = '{{ route('resource.usuario.update', [\Illuminate\Support\Facades\Auth::user()->id]) }}';
		var data = {
			username : $('#email').val(),
		};
		ajaxPatch(url, data, function(data){
			console.log('Cuenta de correo actualizada');
		}, function(data){
			console.log('No se pudo actualizar su cuenta de correo', 'warning');
		});
	}

	// ----------

	$( "#formActualizarContrasenia" ).submit(function( event ) {
		actualizarContrasenia();
		event.preventDefault();
	});

	function actualizarContrasenia(){
		var url = '{{ route('resource.usuario.update', [\Illuminate\Support\Facades\Auth::user()->id]) }}';
		var data = {
			password_current : $('#password_current').val(),
			password : $('#password').val(),
			password_confirmation : $('#password2').val()
		};
		ajaxPatch(url, data, function(data){
			console.log('Contraseña actualizada');
		}, function(data){
			console.log('No se pudo actualizar la contraseña', 'warning');
		});
	}

</script>
@endpush
