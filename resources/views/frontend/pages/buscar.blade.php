@extends('frontend.layouts.default')
@push('css')
<link href="/assets/plugins/datatables/css/dataTables.bootstrap4.css" rel="stylesheet" />
<link href="/assets/plugins/datatables/css/responsive/responsive.bootstrap4.css" rel="stylesheet" />
@endpush
@section('content')

	<style>

		.search-container .search-content {
			padding-left:20px !important;
		}

		@media (min-width:991px) {
			.search-content{
				/*width:60% !important;*/
			}
		}

		.dataTables_wrapper table thead{
			display:none;
		}

		.item-row {
			border:none;
		}
		.item.item-thumbnail{
			width:100% !important;
			border:1px solid #ccd0d4 !important;
			/*border-left:none !important;*/

			max-height:140px;
			height:140px;
			margin-top:20px;
		}
		.item.item-thumbnail .item-image{
			text-align:center;
			width:220px;
			float:left;

			height:100%;
			max-height:130px;
			margin-right:20px;


			padding:0px !important;
			margin:5px !important;
			background-color:#f0f3f4;

			overflow:hidden;

			/*margin-top:8px;*/
		}
		.item.item-thumbnail .item-title {
			margin-top:15px;
			margin-bottom:15px;
			max-height:57px;
		}
		.item.item-thumbnail .item-date{
			margin-right:10px;
		}
		.item.item-thumbnail .item-info{
			text-align:left;
			padding:0px;
			padding-left:240px;
		}
		.discount {
			display:none;
		}

		.linkAnnouncement {
			font-size:23px !important;
			line-height: 23px;
		}

		.item-provider {
			float: right;
			position: relative;
			/*border: 1px solid #333;*/
			/*width: 20%;*/
			height: 35px;
			margin-top: 0px;

			font-size: 14px;
			font-weight: bold;
			/*position:relative;*/
			right:10px;
			bottom:15px;
		}

		@media (max-width:635px) {
			.item-provider{
				display:none;
			}
			.search-container .search-content {
				padding-left:0px !important;
			}
		}

		@media (max-width:550px) {
			h4.item-title a {
				font-size:17px !important;
			}
			/*.item-image {*/
				/*display:block !important;*/
			/*}*/
			/*.item-info {*/
				/*display:block !important;*/
			/*}*/
		}

		.dataTable td {
			padding:0px !important;


			/*border:none !important;*/
		}
		.dataTables_paginate {
			text-align: center !important;
			margin-top:10px !important;
		}
		.dataTables_wrapper table{
			/*background-color:#ffffff;*/
			/*border: 1px solid #ccd0d4;*/
		}

		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
			border-top:none;
		}

		.page-header-cover:before {
			/*background:rgba(235, 173, 21, 0.8);*/
			/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
			background:rgba(52, 143, 226, 0.74);  /* blue */
		}

		#page-header h1.page-header{
			/*color:rgba(0,0,0,0.75);*/
			/*color:#242a30;*/
			color:#ffffff;
			font-weight:600;
			font-size:35px;
		}

		.cat_title {
			/*color:#585858;*/
			color:#63a9e3;
		}
		.cat_message {
			overflow: hidden;
			color:#8a8a8a;
			max-height:200px;
			-moz-transition: max-height 2s ease;
			-webkit-transition: max-height 2s ease;
			-o-transition: max-height 2s ease;
			transition: max-height 2s ease;
		}

		.truncate {
			max-height:30px;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;

			-moz-transition: max-height 2s ease;
			-webkit-transition: max-height 2s ease;
			-o-transition: max-height 2s ease;
			transition: max-height 2s ease;
		}
	</style>
	<!-- BEGIN #page-header -->
	<div id="page-header" class="section-container page-header-container bg-black">
		<!-- BEGIN page-header-cover -->
		<div class="page-header-cover">
			<img src="/assets/img/e-commerce/cover/cover-crane.jpg" alt="" style="margin-top: -360px; width:100%;" />
		</div>
		<!-- END page-header-cover -->
		<!-- BEGIN container -->
		<div class="container">
			<h1 class="page-header"><b>@if($page_title == '') @lang('messages.search')</b> @else {{ $page_title }} @endif</h1>
		</div>
		<!-- END container -->
	</div>
	<!-- BEGIN #page-header -->

	<!-- BEGIN search-results -->
	<div id="search-results" class="section-container bg-silver">
		<!-- BEGIN container -->
		<div class="container">
			<!-- BEGIN breadcrumb -->
			{{--<ul class="breadcrumb m-b-10 f-s-12">--}}
				{{--<li><a href="#">Home</a></li>--}}
				{{--<li><a href="#">Mobile Phone</a></li>--}}
				{{--<li><a href="#">Apple</a></li>--}}
				{{--<li class="active">iPhone 6S Plus</li>--}}
			{{--</ul>--}}
			<!-- END breadcrumb -->
			<!-- BEGIN search-container -->
			<div class="search-container">
				<!-- BEGIN search-content -->
				{{--<div class="mz_adverts" style="width:15% !important; float:right; padding-left:10px;" hidden>--}}
					{{--<div style="width:100%; height:480px; /*border:1px solid #333; background-color:#000;*/ margin-bottom:20px;">--}}
						{{--<table style="height:100%; width:100%;">--}}
							{{--<tr>--}}
								{{--<td style="vertical-align: middle; text-align: center;">--}}
									{{--<iframe src="/banner1/index.html" frameborder="0" scrolling="no" style="width:100%; height:100%; margin:0px; padding:0px;"></iframe>--}}
									{{--<a href="{{ route("public.{$glc}.contact") }}">--}}
										{{--<img src="/assets/img/banners/160X480PX.gif" alt="">--}}
										{{--<img src="/assets/banner_side_1_se.jpeg" alt="">--}}
									{{--</a>--}}
								{{--</td>--}}
							{{--</tr>--}}
						{{--</table>--}}
					{{--</div>--}}
					{{--<div style="width:100%; height:240px; /*border:1px solid #333; background-color:#000;*/ margin-bottom:20px; display:none;">--}}
						{{--<table style="height:100%; width:100%;">--}}
							{{--<tr>--}}
								{{--<td style="vertical-align: middle; text-align: center;">--}}
									{{--<iframe src="/banner1/index.html" frameborder="0" scrolling="no" style="width:100%; height:100%; margin:0px; padding:0px;"></iframe>--}}
								{{--</td>--}}
							{{--</tr>--}}
						{{--</table>--}}
					{{--</div>--}}
					{{--<div style="width:100%; height:240px; /*border:1px solid #333; background-color:#000;*/ margin-bottom:20px;">--}}
						{{--<table style="height:100%; width:100%;">--}}
							{{--<tr>--}}
								{{--<td style="vertical-align: middle; text-align: center;">--}}
									{{--<iframe src="/banner1/index.html" frameborder="0" scrolling="no" style="width:100%; height:100%; margin:0px; padding:0px;"></iframe>--}}
									{{--<a href="javascript:;">--}}
										{{--<img src="/assets/img/banners/160X240PX.jpg" alt="">--}}
									{{--</a>--}}
								{{--</td>--}}
							{{--</tr>--}}
						{{--</table>--}}
					{{--</div>--}}
				{{--</div>--}}


				<div class="search-content" style=" padding-left:10px;">
					<!-- BEGIN search-toolbar -->
					<div class="search-toolbar seo_content">
						<!-- BEGIN row -->
						<div class="row">
							<div class="col-md-12">
								<h2 class="cat_title" style="margin-top:0px;"></h2>
								<p class="cat_message truncate" style="margin-bottom:0px;"></p>
								<a href="javascript:;" class="pull-right" onclick="toggleTruncate()">Read more...</a>
							</div>
						</div>
						<!-- END row -->
					</div>
					<!-- BEGIN search-toolbar -->
					<div class="search-toolbar" style="margin-bottom:0px;">
						<!-- BEGIN row -->
						<div class="row">
							<div class="col-md-6">
								<h4 id="items_found">@lang('messages.items_found', ['number' => 0])</h4>
							</div>
							<!-- END col-6 -->
							<!-- BEGIN col-6 -->
							{{-- TODO: habilitar el ordenamiento segun: popular, new_arrival, price --}}
							<div class="col-md-6 text-right hide">
								<ul class="sort-list">
									<li class="text"><i class="fa fa-filter"></i> @lang('messages.order_by'):</li>
									<li class="active"><a href="#">@lang('messages.popular')</a></li>
									<li><a href="#">@lang('messages.new_arrival')</a></li>
									{{--<li><a href="#">@lang('messages.discount')</a></li>--}}
									<li><a href="#">@lang('messages.price')</a></li>
								</ul>
							</div>
							<!-- END col-6 -->
						</div>
						<!-- END row -->
					</div>
					<!-- END search-toolbar -->

					<!-- BEGIN search-item-container -->
					<div class="search-item-container" style="border:none; min-height:1300px;">

						<table id="data-table" class="table" style="margin-top:0px !important;" cellspacing="10"></table>

					</div>
					<!-- END search-item-container -->
					<!-- BEGIN pagination -->
					<!-- END pagination -->
				</div>

				<!-- END search-content -->
				@include('frontend.includes.sidebarSearch', ['ajaxSearch' => true,
					'grouped_tags' => $grouped_tags,
				])

				@include('frontend.includes.banners_left')

			</div>
			<!-- END search-container -->
		</div>
		<!-- END container -->
	</div>
	<!-- END search-results -->

	<div id="stub_item" class="hide">
		<div class="item item-thumbnail" style="/*border-bottom:1px solid #ccd0d4 !important;*/">
			<a href="javascript:;" class="item-image linkAnnouncement" style="">
				<img src="" alt="" />
				<div class="discount">32% OFF</div>
			</a>
			<div class="item-info">

				<p class="item-desc item-date" style="color: #bbbbbb; float: right;"></p>
				<p class="item-desc item-cats" style="margin-top:12px;"><a href="javascript:;"></a> / <a href="javascript:;"></a></p>
				<h4 class="item-title">
					<a href="javascript:;" target="_blank" class="linkAnnouncement"></a>
				</h4>
				<div class="item-price">$2999.00</div>
				<a href="javascript:;" class="item-provider"></a>
				{{--<div class="item-discount-price">$2599.00</div>--}}
				{{--<p class="item-desc" style="color: #bbbbbb; float: right;"><a href="javascript:;">YourCompanyName</a></p>--}}
				<div style="display: block; clear:both;"></div>

			</div>
		</div>
	</div>

@endsection

@push('scripts')
<script src="/assets/plugins/datatables/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/datatables/js/dataTables.bootstrap4.js"></script>
<script src="/assets/plugins/datatables/js/responsive/dataTables.responsive.js"></script>
<script src="/assets/plugins/datatables/js/responsive/responsive.bootstrap4.js"></script>
<script>
	var datatable = null;
	var modal = null;



	$(document).ready(function(){

		initDataTable();

	});

	function initDataTable(){
		if (!$.fn.DataTable.isDataTable('#data-table')) {
			datatable = $('#data-table').DataTable({
				lengthMenu: [5, 10, 30, 50, 100, 150],
				pageLength: 20,
				"bLengthChange": false,
				"bFilter": false,
				"bInfo": false,
//				dom: 'Bfrtip',
				dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
				"<'row'<'col-sm-12'tr>>" +
				"<'row'<'col-sm-12 col-md-12'i><'col-sm-12 col-md-12 text-center'p>>",
				buttons: [
					{
						text: 'Nuevo',
						action: function(e, dt, node, config){
							alert('Button activated');
						}
					}
				],
				ajax: {
					url: '{{ route('resource.anuncio.ds') }}',
					dataSrc: 'data',
					method: 'POST',
					'headers': {
						'X-CSRF-TOKEN': '{{ csrf_token() }}'
					},
					data: function (d){ // Procesa datos enviados
//	                d.element = $('#element').val();
						d.filtro = 'aprobados';

						d.idProductoTipo = $('#sideSearchTipoMaquina').val();
						d.idCategoria = $('#sideSearchCategoria').val();
						d.idSubCategoria = $('#sideSearchSubCategoria').val();
						d.idMarca = $('#sideSearchMarca').val();
						d.esNuevo = $('#sideSearchNuevo').val();
						d.precioFrom = $('#sideSearchPrecioFrom').val();
						d.precioTo = $('#sideSearchPrecioTo').val();
						d.anioFrom = $('#sideSearchAnioFrom').val();
						d.anioTo = $('#sideSearchAnioTo').val();
						d.tags = $('#sideSearchTags').val();
						// ...

						if( d.idCategoria > 0 ) {

							var strcat = '';
							var strsubcat = '';

							if( d.idSubCategoria > 0 ) {

								$.each(cats, function(k,v){
									if(parseInt(v.id, 10) === parseInt(d.idCategoria, 10)){

										strcat = v.traduccion.url_{{ $glc }};
										subs = v.subcategorias;
										$.each(subs, function(k1, v1){
											if(parseInt(v1.id, 10) === parseInt(d.idSubCategoria, 10)){
												strsubcat = v1.traduccion.url_{{ $glc }};
											}
										});
									}
								});

								window.history.pushState("", "", '/'+ strcat +'/'+ strsubcat );
							} else {
								$.each(cats, function(k,v){
									if(parseInt(v.id, 10) === parseInt(d.idCategoria, 10)){
										strcat = v.traduccion.url_{{ $glc }};
									}
								});
								window.history.pushState("", "", '/'+ strcat );
							}

						} else {
							$('.page-header').html('Search');
							window.history.pushState("", "", '/search' );
						}
					},
					dataFilter: function(data){ // Procesa datos recibidos
//						return data;
						var data = JSON.parse(data);
//						for(var i = 0 ; i < data.data.length; i++){
//							// ...
//						}

						if(data.title !== '' ) {
							$('h1.page-header').html(data.title);
							$('.cat_title').html(data.title);
							$('.seo_content').removeClass('hide');
						} else {
							$('h1.page-header').html('Search');
							$('.seo_content').addClass('hide');
						}
						if(data.message !== '' ) {
							$('.cat_message').html(data.message);
							$('.seo_content').removeClass('hide');
						} else {
							$('.seo_content').addClass('hide');
						}

						var text = '{{ __('messages.items_found', ['number' => '_nan_']) }}';
						text = text.replace('_nan_', data.recordsTotal);
						$('#items_found').html(text);
						return JSON.stringify( data );
					}
				},
				columns: [
					{ title: 'Id' },
				],
				columnDefs: [
					{
						targets: 0,
						render: function(data, type, row){

							var clone = $('#stub_item').clone();

							var name = '';
							var cat = '';
							var scat = '';
							if(row.producto){

								if( row.producto.producto_item.idTipoImportacion == 1 ) {
									name += row.producto.nombre_alter;
								} else {
									if(row.producto.marca === undefined){
										name += '<b>@lang('messages.no_brand')</b>';
									} else {
										name += row.producto.marca.nombre;
									}
									name += ' ';
									if(row.producto.modelo === undefined){
										name += '<b>@lang('messages.no_model')</b>';
									} else {
										name += row.producto.modelo.nombre;
									}
								}

								if(row.producto.producto_item.definicion.ano_de_fabricacion !== undefined && row.producto.producto_item.definicion.ano_de_fabricacion !== 'unknown_year'){
									name += ' <small>( ' + "@lang('messages.year'): ";
									name += row.producto.producto_item.definicion.ano_de_fabricacion + ' )</small>';
								}

								if( row.producto.categorias.length > 0 ) {
									cat = $('<a>', {
										href: row.producto.url_c
									}).html(row.producto.categorias[0].padre.traduccion['{{ $glc }}']);

									scat = $('<a>', {
										href: row.producto.url_cs
									}).html(row.producto.categorias[0].traduccion['{{ $glc }}']);
								}

							} else {
								name =  '<b class="text-red">@lang('messages.no_product')!</b>';
							}

							var strDate = row.created_at.replace(' ', 'T');
							strDate = row.created_at.replace(/-/g, '/');
							var dateParse = new Date(strDate);
							//var dateParse = new Date(Date.parse(row.created_at));
							clone.find('.item-date').html(dateParse.getDate()+'/'+(dateParse.getMonth() + 1)+'/'+dateParse.getFullYear());
							clone.find('.item-title a').html(name);
							clone.find('.item-cats').html('');
							if( cat !== '' ) {
								clone.find('.item-cats').append(cat).append(' / ').append(scat);
							}

							@if($global_isProvider == true)
							if(row.producto.precioSudamerica === true){
								clone.find('.item-price').html('@lang('messages.price_not_available')');
							} else {
								clone.find('.item-price').html('{{ strtoupper(session('current_currency', 'usd')) }} ' + parseFloat(row.producto.precio_publicacion).toFixed(2));
							}
							@else
							clone.find('.item-price').html('{{ strtoupper(session('current_currency', 'usd')) }} ' + parseFloat(row.producto.precio_publicacion).toFixed(2));
							@endif

							if(row.producto.idProductoTipo == {{ \App\Models\Producto::TIPO_ACCESORIO }}) {
								clone.find('.item-price').html('');
								{{--clone.find('.item-price').html('@lang('messages.multiple_prices')');--}}
							}

							var imgProv = '';

							if(row.owner.owner.idProveedor > 0){

								if(row.owner.owner.proveedor.tieneStore) {
									imgProv = row.owner.owner.proveedor.nombre;
									if( row.owner.owner.proveedor.galerias && row.owner.owner.proveedor.galerias.length > 0 ) {
										if( row.owner.owner.proveedor.galerias[0].imagenes && row.owner.owner.proveedor.galerias[0].imagenes.length > 0 ) {
											imgProv = '<img src="/company_logo/'+ row.owner.owner.proveedor.galerias[0].imagenes[0].filename +'" style="max-height:100%;" />';
										}
									}
								}
							}

							var urlProfile = '{{ route("public.{$glc}.perfilCliente", [ 'owner_id' ]) }}';
							urlProfile = urlProfile.replace('owner_id', row.owner.owner.id);
							clone.find('.item-provider').attr('href', urlProfile);
							clone.find('.item-provider').html(imgProv);

							if(row.producto){
								if(row.producto.galerias.length > 0){
									if(row.producto.galerias[0].imagenes.length > 0){
										clone.find('.item-image img').attr('src', row.producto.galerias[0].imagenes[0].ruta_publica_producto_thumb);
									}
								}
							}

							clone.find('a.linkAnnouncement').each(function(index){
								{{--var url = '{{ route('public.producto', ['idPublicacion']) }}';--}}
								{{--url = url.replace('idPublicacion', row.id);--}}

								{{--var url = '{{ route('public.producto_seo', ['categoria','subcat','marca','modelo','idpublicacion']) }}';--}}
								{{--url = url.replace('categoria', row.producto.categorias[0].padre.traduccion['{{ $global_localization }}']);--}}
								{{--url = url.replace('subcat', row.producto.categorias[0].traduccion['{{ $global_localization }}']);--}}
								{{--url = url.replace('marca', row.producto.marca.nombre );--}}
								{{--url = url.replace('modelo', row.producto.modelo.nombre );--}}
								{{--url = url.replace('idpublicacion', row.id);--}}

								url = row.producto.url_cs + row.producto.url_mm + "/" + row.id;
								$(this).attr('href', url);
							});


							return clone.html();
						}

					}
				],
				fixedHeader: {
					header: true,
					headerOffset: $('#header').height()
				},
				bSort : false,
				bAutoWidth: false,
				processing: true,
				serverSide: true,
				responsive: true,
				fixedColumns: true
			}).on('draw.dt', function(){
				$('#idsChecker').on('change', function(e){
					$('.chkId').prop('checked', $(this).prop('checked'));
				});

				$('html, body').animate({
					scrollTop: $('.search-content').offset().top
				}, 1000);

			});
		} else {
			datatable.ajax.reload();
		}

	}

	function getNumberFormat(x){
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
	}

	function triggerSearch(){
		datatable.ajax.reload();
	}

	function toggleTruncate() {
		$('.cat_message').toggleClass('truncate');
		if( $('.cat_message').hasClass('truncate') ) {
			$('.seo_content').find('a').html('Read more...');
		} else {
			$('.seo_content').find('a').html('Read less');
		}
	}
</script>
@endpush