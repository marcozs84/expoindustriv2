<h3 class="m-t-0">{{ $titulo }}</h3>
<p>
	{!! nl2br($resumen) !!}
</p>
<p class="text-right">
	<a href="{{ $noticia->fuenteUrl }}" target="_blank">@lang('messages.ver_noticia')</a>
</p>
<div class="text-right">@lang('messages.news_source'): {{ $noticia->fuente}}</div>
