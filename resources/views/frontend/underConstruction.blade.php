@extends('layouts.empty', ['paceTop' => true, 'bodyExtraClass' => 'bg-white'])

@section('title', 'En construcción')

@push('css')
    <link href="/assets/plugins/jquery.countdown/jquery.countdown.css" rel="stylesheet" />

<link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">
<script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>
@endpush

@section('content')
	<!-- begin coming-soon -->
	<div class="coming-soon">
		<!-- begin coming-soon-header -->
		<div class="coming-soon-header">
			<div class="bg-cover"></div>
			<div class="brand">
				<b style="color: #fdda01;">Expo</b>Industri
			</div>
			<div class="desc" style="color:rgba(255,255,255,.8) !important;">
				{{--Our website is almost there and it’s rebuilt from scratch! A lot of great new features <br />and improvements are coming.--}}
				Nuestro sitio está casi terminado! Muchas oportunidades <br />y ofertas están en camino.
			</div>
			<div class="timer">
				<div id="timer"></div>
			</div>
		</div>
		<!-- end coming-soon-header -->
		<!-- begin coming-soon-content -->
		<div class="coming-soon-content">
			<div class="desc">
				Lanzamos nuestra versión <b>beta</b> muy pronto!
				{{--We are launching a closed <b>beta</b> soon!<br /> Sign up to try it before others and be the first to know when we <b>launch</b>.--}}
			</div>
			<div class="input-group hide">
				<input type="text" class="form-control" placeholder="Email Address" />
				<div class="input-group-append">
					<button type="button" class="btn btn-inverse">Notify Me</button>
				</div>
			</div>
			{{--<p class="help-block m-b-25"><i>We don't spam. Your email address is secure with us.</i></p>--}}
			<p>
				{{--Follow us on--}}
				Síganos en
				<a href="javascript:;" class="text-inverse"><i class="fab fa-twitter fa-lg fa-fw text-info"></i> Twitter</a> y
				<a href="javascript:;" class="text-inverse"><i class="fab fa-facebook fa-lg fa-fw text-primary"></i> Facebook</a>
			</p>
		</div>
		<!-- end coming-soon-content -->
	</div>
	<!-- end coming-soon -->
@endsection

@push('scripts')
    <script src="/assets/plugins/jquery.countdown/jquery.plugin.js"></script>
    <script src="/assets/plugins/jquery.countdown/jquery.countdown.js"></script>
	<script src="/assets/js/demo/coming-soon.demo.js?r={{ rand(1, 111) }}"></script>
	<script>
		$(document).ready(function() {
			ComingSoon.init();
		});
	</script>
@endpush