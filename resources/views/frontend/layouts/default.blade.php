<!DOCTYPE html>
<!--[if IE 8]> <html lang="{{ session('lang', 'en') }}" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ session('lang', 'en') }}">
<!--<![endif]-->
<head>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-MF4LW2W');</script>
	<!-- End Google Tag Manager -->



	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156706020-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-156706020-2');
	</script>

	{{----}}

	{{--<!-- Global site tag (gtag.js) - Google Analytics -->--}}
	{{--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156706020-1">--}}
	{{--</script>--}}
	{{--<script>--}}
		{{--window.dataLayer = window.dataLayer || [];--}}
		{{--function gtag(){dataLayer.push(arguments);}--}}
		{{--gtag('js', new Date());--}}

		{{--gtag('config', 'UA-156706020-1');--}}
	{{--</script>--}}

	@include('frontend.includes.head')

	<style>
		.header {
			/*z-index:1050 !important;*/
		}
		/*@media (min-width:1920px) {*/
			/*.btnAnnouncement {*/
				/*!*margin-left:100px; margin-top:-8px;*!*/
				/*position:absolute; margin-top:40px; margin-left:940px;*/
			/*}*/
			/*.header-logo img {*/
				/*max-height:150px;*/
			/*}*/
			/*.mz_adverts {*/
			/*}*/
			/*.mobile_announce {*/
				/*display:none !important;*/
			/*}*/
		/*}*/
		@media (max-width:2919px) {
			.btnAnnouncement {
				/*margin-left:10px; margin-top:-8px;*/
				position:absolute; margin-top:32px; margin-left:800px;
			}
			.header-logo a img {
				max-height:90px;
			}
			.mobile_announce {
				display:none !important;
			}
		}
		@media (max-width:1200px) {
			.btnAnnouncement {
				/*margin-left:10px; margin-top:-8px;*/
				position:absolute; margin-top:32px; margin-left:680px;
			}
			.header-logo img {
				max-height:150px;
			}
			.mobile_announce {
				display:none !important;
			}
		}
		@media (max-width:1030px) {
			.btnAnnouncement {
				display:none;
			}
			.mz_adverts {
				display:none;
			}
			.mobile_announce {
				display:none !important;
			}
		}
		@media (max-width:991px) {
			.header-logo a img{
				max-height:70px;
			}
		}
		@media (max-width:892px) {
			.btnAnnouncement {
				/*margin-left:10px; margin-top:-8px;*/
				position:absolute; margin-top:32px; margin-left:715px;
			}
		}
		@media (max-width:768px) {
			.btnAnnouncement {
				display:none;
			}
			.header-logo {
				position: absolute;
				width:90%;
				overflow:hidden;
			}
			.header-logo a img {
				max-height:66px;
			}
			.mobile_announce {
				display: block !important;
			}
		}
		@media (max-width:560px) {
			/*.slider .carousel, .carousel .carousel-inner, .carousel .carousel-inner .item {*/
				/*min-height:190px !important;*/
			/*}*/

			.mobile_announce {
				display: block !important;
			}
		}

		/* ---------------- MODALS*/

		h5.modal-title{
			display:inline;
		}

		.account-container .account-sidebar .account-sidebar-cover:before {
			/*background:rgba(217, 196, 0, 0.84);*/  /* yellow */
			background:rgba(52, 143, 226, 0.74);  /* blue */
		}

		.dropzone .dz-preview .dz-image {
			border-radius:5px !important;
		}

		.mobile_login {
			font-weight:bold;
			background: rgb(253,226,72) !important;
			background: -moz-linear-gradient(top, rgba(253,226,72,1) 0%, rgba(255,217,0,1) 100%) !important;
			background: -webkit-linear-gradient(top, rgba(253,226,72,1) 0%,rgba(255,217,0,1) 100%) !important;
			background: linear-gradient(to bottom, rgba(253,226,72,1) 0%,rgba(255,217,0,1) 100%) !important;
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fde248', endColorstr='#ffd900',GradientType=0 ) !important;
		}
	</style>

	{!! $jivoscript !!}
	{{--<script src="//code.jivosite.com/widget/hZoJlMrJaL" async></script>--}}
</head>
<body class="bg-silver__">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MF4LW2W"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<!-- BEGIN #page-container -->
	<div id="page-container" class="fade">
		<!-- BEGIN #top-nav -->
		<div id="top-nav" class="top-nav">
			<!-- BEGIN container -->
			<div class="container">
				<div class="collapse navbar-collapse">





					<ul class="nav navbar-nav">
						<li class="dropdown dropdown-hover">
							<a href="javascript:;" class="@if(App::getLocale() != 'es') hide @endif" data-toggle="dropdown"><i class="flag-icon flag-icon-es " ></i> &nbsp;&nbsp;@lang('messages.spanish') <i class="fa fa-angle-down"></i></a>
							<a href="javascript:;" class="@if(App::getLocale() != 'en') hide @endif" data-toggle="dropdown"><i class="flag-icon flag-icon-gb " ></i> &nbsp;&nbsp;@lang('messages.english') <i class="fa fa-angle-down"></i></a>
							<a href="javascript:;" class="@if(App::getLocale() != 'se') hide @endif" data-toggle="dropdown"><i class="flag-icon flag-icon-se " ></i> &nbsp;&nbsp;@lang('messages.swedish') <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
								<li class=" @if(App::getLocale() == 'es') hide @endif"><a href="javascript:;" onclick="changeLanguage('es')"><i class="flag-icon flag-icon-es" ></i> &nbsp;&nbsp;@lang('messages.spanish')</a></li>
								<li class=" @if(App::getLocale() == 'en') hide @endif"><a href="javascript:;" onclick="changeLanguage('en')"><i class="flag-icon flag-icon-gb" ></i> &nbsp;&nbsp;@lang('messages.english')</a></li>
								<li class=" @if(App::getLocale() == 'se') hide @endif"><a href="javascript:;" onclick="changeLanguage('se')"><i class="flag-icon flag-icon-se" ></i> &nbsp;&nbsp;@lang('messages.swedish')</a></li>
							</ul>
						</li>
						<li class="dropdown dropdown-hover">
							<a href="javascript:;" class="@if(session('current_currency', 'usd') != 'usd') hide @endif" onclick="changeCurrency('usd')" data-toggle="dropdown">@lang('messages.usd') <i class="fa fa-angle-down"></i></a>
							<a href="javascript:;" class="@if(session('current_currency', 'usd') != 'eur') hide @endif" onclick="changeCurrency('eur')" data-toggle="dropdown">@lang('messages.eur') <i class="fa fa-angle-down"></i></a>
							<a href="javascript:;" class="@if(session('current_currency', 'usd') != 'sek') hide @endif" onclick="changeCurrency('sek')" data-toggle="dropdown">@lang('messages.sek') <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
								<li class=" @if(session('current_currency', 'usd') == 'usd') hide @endif"><a href="javascript:;" onclick="changeCurrency('usd')">@lang('messages.usd')</a></li>
								<li class=" @if(session('current_currency', 'usd') == 'eur') hide @endif"><a href="javascript:;" onclick="changeCurrency('eur')">@lang('messages.eur')</a></li>
								<li class=" @if(session('current_currency', 'usd') == 'sek') hide @endif"><a href="javascript:;" onclick="changeCurrency('sek')">@lang('messages.sek')</a></li>
							</ul>
						</li>
						@if($auth_username != '')
							{{--<li><a href="javascript:;">Customer Care</a></li>--}}
							<li><a href="{{ route("private.{$glc}.order.index") }}">@lang('messages.my_orders')</a></li>
						@endif
					</ul>
					<ul id="userMenuClient" class="nav navbar-nav navbar-right @if(!Auth::guard('clientes')->user()) hide @endif">
						<li class="dropdown dropdown-hover">
							<a href="javascript:;" class="client-name" data-toggle="dropdown"> {{ $auth_username }} <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route("public.{$glc}.privCuenta") }}"><i class="fa fa-user"></i> &nbsp;&nbsp;@lang('messages.my_account')</a></li>
								<li><a href="{{ route('public.logout') }}"><i class="fas fa-sign-out-alt"></i> &nbsp;&nbsp;@lang('messages.logout')</a></li>
							</ul>
						</li>
						<li><a href="https://www.facebook.com/expoindustri.sudamerica" target="_blank"><i class="fab fa-facebook-f f-s-14"></i></a></li>
					</ul>
					<ul id="userMenuGuest" class="nav navbar-nav navbar-right @if(Auth::guard('clientes')->user()) hide @endif">
						<li>
							<a href="javascript:;" onclick="openLoginRegister('login')">@lang('messages.log_in')</a>
						</li>
						<li><a href="javascript:;" style="padding:10px 0px;">/</a></li>
						<li>
							<a href="javascript:;" onclick="openLoginRegister('register')">@lang('messages.register')</a>
						</li>
						<li><a href="https://www.facebook.com/expoindustri.sudamerica" target="_blank"><i class="fab fa-facebook-f f-s-14"></i></a></li>
					</ul>
					{{--@if($global_isProvider == true)--}}
						{{--@if(App::getLocale() == 'se')--}}
							{{--<a href="{{ route('public.publish') }}" class="btnAnnouncement" --}}{{-- onclick="openAnnounceHereModal()" --}}{{--><img src="/assets/img/e-commerce/publish_1_se.png" alt="" class=""></a>--}}
						{{--@else--}}
							{{--<a href="{{ route('public.publish') }}" class="btnAnnouncement" --}}{{-- onclick="openAnnounceHereModal()" --}}{{--><img src="/assets/img/e-commerce/publish_1.png" alt="" class=""></a>--}}
						{{--@endif--}}
					{{--@endif--}}

				</div>
			</div>
			<!-- END container -->
		</div>
		<!-- END #top-nav -->

		<!-- BEGIN #header -->
		<div id="header" class="header">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN header-container -->
				<div class="header-container">
					<!-- BEGIN navbar-header -->
					<div class="navbar-header">
						<div class="header-logo text-center">
							<a href="/" style="font-size:35px;">
								{{--<span style=" color: #fdda01;">Expo</span>Industri--}}
								{{--<small>@lang('messages.import_export')</small>--}}
								<img style="" src="/assets/img/logo/logo_exind2_{{ session('lang', 'en') }}.gif" alt="ExpoIndustri">
							</a>
						</div>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

					</div>
					<!-- END navbar-header -->
					<!-- BEGIN header-nav -->
					<div class="header-nav">
						<div class=" collapse navbar-collapse" id="navbar-collapse">
							<ul class="nav">
{{--								<li class="active"><a href="{{ route('public.index') }}">@lang('messages.home')</a></li>--}}
								<li class="active"><a href="{{ route('public.buy_page') }}">@lang('messages.home')</a></li>
								<li class=""><a href="{{ route("public.{$glc}.nosotros") }}">@lang('messages.about_us')</a></li>
								<li class=""><a href="{{ route("public.{$glc}.servicios") }}">@lang('messages.services')</a></li>
								<li class=""><a href="{{ route('public.faq') }}">@lang('messages.questions')</a></li>
								<li class=""><a href="{{ route("public.{$glc}.pricing") }}">@lang('messages.pricing')</a></li>
								<li class=""><a href="{{ route("public.{$glc}.contact") }}">@lang('messages.contact_us')</a></li>
								<li class="mobile_announce"><a href="{{ route('public.publish') }}">@lang('messages.announce_here')</a></li>
								<li class="{{-- dropdown dropdown-full-width dropdown-hover --}}">
									<a href="{{ route("public.{$glc}.search") }}" {{-- data-toggle="dropdown" --}}>
										<i class="fa fa-search search-btn"></i>
										{{--<i class="fa fa-angle-down"></i>--}}
										<span class="arrow top"></span>
									</a>
									<!-- BEGIN dropdown-menu -->
									<div class="dropdown-menu p-0">
										<!-- BEGIN dropdown-menu-container -->
										<div class="dropdown-menu-container">
											<!-- BEGIN dropdown-menu-sidebar -->
											<div class="dropdown-menu-sidebar">
												<h4 class="title">@lang('messages.search_by_category')</h4>
												<ul class="dropdown-menu-list">
													<li><a href="{{ route("public.{$glc}.search") }}" style="text-transform:capitalize;">@lang('messages.all_f') <i class="fa fa-angle-right pull-right"></i></a></li>
													@foreach($global_categorias as $id => $categoria)
														<li><a href="{{ route("public.{$glc}.search", [$id]) }}">{{ $categoria }} <i class="fa fa-angle-right pull-right"></i></a></li>
													@endforeach
													{{--<li><a href="/buscar?cat=Transporte">@lang('messages.transportation') <i class="fa fa-angle-right pull-right"></i></a></li>--}}
													{{--<li><a href="/buscar?cat=Equipos">@lang('messages.equipments') <i class="fa fa-angle-right pull-right"></i></a></li>--}}
													{{--<li><a href="/buscar?cat=Industrial">@lang('messages.industrial')ç <i class="fa fa-angle-right pull-right"></i></a></li>--}}
													{{--<li><a href="/buscar?cat=Agricultura">@lang('messages.agriculture') <i class="fa fa-angle-right pull-right"></i></a></li>--}}
													{{--<li><a href="/buscar?cat=Montacargas">@lang('messages.forklifts') <i class="fa fa-angle-right pull-right"></i></a></li>--}}
													{{--<li><a href="/buscar?cat=Otros">@lang('messages.others') <i class="fa fa-angle-right pull-right"></i></a></li>--}}
												</ul>
											</div>
											<!-- END dropdown-menu-sidebar -->
											<!-- BEGIN dropdown-menu-content -->
											<div class="dropdown-menu-content">
												<h4 class="title hide">Shop By Popular Phone</h4>
												<h4 class="title">@lang('messages.prefered_brands')</h4>
												<ul class="dropdown-brand-list m-b-10">
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_deere.gif" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_cat.jpg" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_mercedes.png" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_iveco.png" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_challenger.png" alt="" /></a></li>--}}
													{{--<!-- <li><a href="{{ route("public.{$glc}.search") }}src="assets/img/blackberry.png" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}src="assets/img/sony.png" alt="" /></a></li> -->--}}
												{{--</ul>--}}
												{{--<ul class="dropdown-brand-list m-b-0">--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="assets/img/e-commerce/logo_jcb.png" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_newholland.jpg" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_krone.png" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_ferguson.png" alt="" /></a></li>--}}
													{{--<li><a href="{{ route("public.{$glc}.search") }}"><img src="/assets/img/e-commerce/logo_valtra.jpg" alt="" /></a></li>--}}
													<!-- <li><a href="/buscar?brand="><img src="assets/img/blackberry.png" alt="" /></a></li>
													<li><a href="/buscar?brand="><img src="assets/img/sony.png" alt="" /></a></li> -->
												</ul>
											</div>
											<!-- END dropdown-menu-content -->
										</div>
										<!-- END dropdown-menu-container -->
									</div>
									<!-- END dropdown-menu -->
								</li>
								{{--<li>--}}
									{{--<a href="javascript:;" class="btnAnnouncement p-0 m-0"><img src="/assets/img/e-commerce/publish_2.png" alt="" class="btnAnnouncement"></a>--}}
								{{--</li>--}}
								<li class="mobile_announce mobile_login"><a href="{{ route('public.login') }}">@lang('messages.log_in')</a></li>
							</ul>
						</div>
					</div>
					<!-- END header-nav -->
					<!-- BEGIN header-nav -->
					<div class="header-nav">
						<ul class="nav pull-right">
							<li class="hide">
								<a href="javascript:;" onclick="openLoginRegister('login')">@lang('messages.log_in')</a>
							</li>
							<li class="hide">
								<a href="javascript:;">/</a>
							</li>
							<li class="hide">
								<a href="javascript:;" onclick="openLoginRegister('register')">@lang('messages.register')</a>
							</li>
						</ul>
					</div>
					<!-- END header-nav -->
				</div>
				<!-- END header-container -->
			</div>
			<!-- END container -->
		</div>
		<!-- END #header -->


	@yield('content')


		<!-- BEGIN #footer -->
		<div id="footer" class="footer">
			<!-- BEGIN container -->
			<div class="container">
				<!-- BEGIN row -->
				<div class="row">
					<!-- BEGIN col-3 -->
					<div class="col-md-3" hidden>
						<h4 class="footer-header">@lang('messages.about_us')</h4>
						<p>
							@lang('messages.about_us_footer')
						</p>

					</div>
					<!-- END col-3 -->
					<!-- BEGIN col-3 -->
					<div class="col-md-3">
						<h4 class="footer-header">@lang('messages.site_map')</h4>
						<ul class="fa-ul">
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.nosotros") }}">@lang('messages.about_us')</a></li>--}}
							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_anunciantes") }}">@lang('messages.information_for_announcers')</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_compradores") }}">@lang('messages.information_for_buyers')</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.faq') }}">@lang('messages.frequent_questions')</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.servicios") }}">@lang('messages.services')</a></li>
{{--							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.toc') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.termsAndConditions') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.dataProtectionPolicy') }}">@lang('messages.privacy_policy')</a></li>--}}
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.contact") }}">@lang('messages.contact_us')</a></li>--}}
						</ul>
					</div>
					<!-- END col-3 -->
					<!-- BEGIN col-3 -->
					<div class="col-md-3">
						<h4 class="footer-header">@lang('messages.terms')</h4>
						<ul class="fa-ul">
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.nosotros") }}">@lang('messages.about_us')</a></li>--}}
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_anunciantes") }}">@lang('messages.information_for_announcers')</a></li>--}}
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.info_compradores") }}">@lang('messages.information_for_buyers')</a></li>--}}
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.faq') }}">@lang('messages.frequent_questions')</a></li>--}}
							{{--<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.servicios") }}">@lang('messages.services')</a></li>--}}
							{{--							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.toc') }}">@lang('messages.legal_terms_conditions')</a></li>--}}
							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.termsAndConditions') }}">@lang('messages.legal_terms_conditions')</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('public.dataProtectionPolicy') }}">@lang('messages.privacy_policy')</a></li>
							<li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route("public.{$glc}.contact") }}">@lang('messages.contact_us')</a></li>
						</ul>
					</div>
					<!-- END col-3 -->
					<!-- BEGIN col-3 -->
					<div class="col-md-3">
						<h4 class="footer-header">@lang('messages.central_office')</h4>
						<address>
							<strong><i class="flag-icon flag-icon-se" style="font-style: 20px;"></i> @lang('messages.sweden')</strong><br />
							Jönköpingsvägen 27 <br>
							561 61 Tenhult <br>
							{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +46 (0) 723 933 700<br />--}}
							<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />

							{{--<br />--}}
							{{--<strong>Bolivia</strong><br />--}}
							{{--1355 Market Street, Suite 900<br />--}}
							{{--San Francisco, CA 94103<br />--}}
							{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
							{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

							{{--<br />--}}
							{{--<strong><i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> Chile</strong><br />--}}
							{{--Oficina mapocho 48D<br />--}}
							{{--Zofri - Iquique<br />--}}
							{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
							{{--<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />--}}

							{{--<br />--}}
							{{--<strong>Peru</strong><br />--}}
							{{--1355 Market Street, Suite 900<br />--}}
							{{--San Francisco, CA 94103<br />--}}
							{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
							{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

							<br>
							<b><a href="{{ route("public.{$glc}.contact") }}">@lang('messages.our_offices_southamerica')</a></b>

						</address>
					</div>
					<!-- END col-3 -->


					<!-- BEGIN col-3 -->
					<div class="col-md-3">
						<h4 class="footer-header">@lang('messages.distrib_office')</h4>
						<address>
							<strong><i class="flag-icon flag-icon-cl" style="font-style: 20px;"></i> Chile</strong><br />
							Oficina mapocho 48D<br />
							Zofri - Iquique<br />
							{{--<abbr title="@lang('messages.phone')" style="text-transform: capitalize;">@lang('messages.phone'):</abbr> +56 (57) 2473844<br />--}}
							<abbr title="@lang('messages.e_mail')" style="text-transform: capitalize;">@lang('messages.e_mail'):</abbr> <a href="{{ route("public.{$glc}.contact") }}">info@expoindustri.com</a><br />

							{{--<br />--}}
							{{--<strong>Peru</strong><br />--}}
							{{--1355 Market Street, Suite 900<br />--}}
							{{--San Francisco, CA 94103<br />--}}
							{{--<abbr title="Phone">Teléfono:</abbr> (123) 456-7890<br />--}}
							{{--<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />--}}

						</address>
					</div>
					<!-- END col-3 -->



					<!-- BEGIN col-3 -->
					<div class="col-md-3 hide">
						<h4 class="footer-header">Oficinas en Europa</h4>
						<address>
							<strong>Twitter, Inc.</strong><br />
							1355 Market Street, Suite 900<br />
							San Francisco, CA 94103<br /><br />
							<br>
							<strong>Av. Las palmas, Inc.</strong><br />
							1355 Market Street, Suite 900<br />
							San Francisco, CA 94103<br /><br />

							<abbr title="Phone">Phone:</abbr> (123) 456-7890<br />
							<abbr title="Fax">Fax:</abbr> (123) 456-7891<br />
							<abbr title="Email">Email:</abbr> <a href="mailto:sales@myshop.com">sales@myshop.com</a><br />
							<abbr title="Skype">Skype:</abbr> <a href="skype:myshop">myshop</a>
						</address>
					</div>
					<!-- END col-3 -->
					<!-- BEGIN col-3 -->
					<div class="col-md-3 p-0 text-center" style="/* padding:20px !important; background-color:#ffffff; */" hidden>
						{{--<style>--}}
							{{--#loc_map {--}}
								{{--height: 270px;--}}
								{{--width: 100%;--}}
							{{--}--}}
						{{--</style>--}}
						{{--<div id="loc_map"></div>--}}
						{{--@if($global_isProvider == true)--}}
						{{--@endif--}}


						{{--@if(App::getLocale() == 'se')--}}
							{{--<img src="/assets/img/e-commerce/FooterLogoWhite_Export.png" alt="" style="width:100%; ">--}}
						{{--@else--}}
							{{--<img src="/assets/img/e-commerce/FooterLogoWhite_Import.png" alt="" style="width:100%; ">--}}
						{{--@endif--}}

						<img src="/assets/img/e-commerce/uc_logo_{{ App::getLocale() }}.png" alt="" style="width:90%; ">

						 {{--<img src="/assets/img/e-commerce/SmallLogo.png" alt="Map Route Export" style="width:100%;">--}}
						 {{--<img src="/assets/img/e-commerce/256_iso_fav.png" alt="Map Route Export" style="width:100%;">--}}
						 {{--<img src="/assets/img/e-commerce/mapa_ruta_export.png" alt="Map Route Export" style="width:100%;">--}}
						{{--<img src="/assets/img/e-commerce/sello_footer1.png" alt="Map Route Export" style="max-height:230px; ">--}}

						{{--<img src="/assets/img/logo/logo_exind2_{{ session('lang', 'en') }}.png" alt="Map Route Export" style="width:100%; background-color:#ffffff; pading:10px;">--}}

					</div>
					<!-- END col-3 -->
				</div>
				<!-- END row -->
			</div>
			<!-- END container -->
		</div>
		<!-- END #footer -->

		<!-- BEGIN #footer-copyright -->
		<div id="footer-copyright" class="footer-copyright">
			<!-- BEGIN container -->
			<div class="container">
				<div class="payment-method">
					{{--<img src="/assets/img/e-commerce/payment/payment-method.png" alt="" />--}}
				</div>
				<div class="copyright">
					Copyright &copy; <?=date('Y') ?> ExpoIndustri Sweden AB @lang('messages.all_rights_reserved')

				</div>
				<div class="pull-right">
					Powered by <a href="https://www.stripe.com" class=""><img style="width:50px;" src="/assets/img/logo/stripe_logo_white_sm.png" alt="Stripe"></a>
					{{--<a href="https://www.stripe.com" class=""><img style="" src="/assets/img/logo/powered_by_stripe.png" alt="Stripe"></a>--}}
				</div>
			</div>
			<!-- END container -->
		</div>
		<!-- END #footer-copyright -->
	</div>
	<!-- END #page-container -->

	<style>
		/*.loginRegion .nav-tabs {*/
			/*background-color:#000000;*/
		/*}*/
		/*.loginRegion .nav.nav-tabs > li.nav-item. > a:hover {*/
			/*background-color:#0000ff !important;*/
		/*}*/
		.loginRegion.modal {
			text-align: center;
			padding: 0!important;
		}

		.loginRegion.modal:before {
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}

		.loginRegion .modal-dialog {
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}
		.promotion-btn{
			border-color: #fff;
			color: #fff;
			text-decoration: none;
			background: #000;
			transition: .3s ease;
		}
		.promotion-btn:hover{
			border-color: #000;
			color: #000;
			text-decoration: none;
			background: #fff;
		}
		.nav-tabs.nav-tabs-inverse > li > a:hover{
			background-color:#666666 !important;
			border:1px solid #666666;
		}
		.nav-tabs.nav-tabs-inverse > li.active > a:hover{
			background-color:#ffffff !important;
			border:1px solid #ffffff;
		}

		.login-input {
			padding:5px;
			border: 1px solid #e5e5e5;
			width:90%;
			margin-top:10px;
		}
		::placeholder {
			color:#7a7a7a;
		}
		@media (min-width:768px) {
			.loginRegion .modal-dialog {
				width:400px;
			}
		}

	</style>

	<div class="modal fade loginRegion" id="modalLoginRegister">
	<div class="modal-dialog" >
		<div class="modal-content" style="margin-top:50px;">
			<div class="modal-body">
				<div class="login-form hide">
					<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
						<h4 class="text-center m-t-0">@lang('messages.log_in')</h4>
					</div>
					<form id="formLoginTop" action="{{ route('public.login') }}" method="post" class="text-center">
						{{ csrf_field() }}
						<input type="text" name="email" id="loginTopUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
						<input type="password" name="password" id="loginTopPassword" class="login-input" placeholder="@lang('messages.password')" >

						<div id="loginTopErrors"></div>

						<input type="checkbox" name="remember" id="frmRememberTop" /> @lang('messages.remember_me')
						<br>
						{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.log_in')">--}}
						<button type="submit" id="regTopBtnEntrar" form="formLoginTop" value="@lang('messages.log_in')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.log_in')</button>
						{{--<a href="javascript:;" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" onclick="tryLogin()">Entrar</a>--}}
						{{--<input type="submit" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="Entrar">--}}
						<br>
						<a href="javascript:;" class="" onclick="activateLoginRegister('forgot')" style="font-size:12px; /*color:#333333;*/">@lang('messages.forgot_password')</a>

					</form>
					<hr />
					<p class="text-center">
					<a href="javascript:;" class="btn btn-warning btn-lg" onclick="activateLoginRegister('register')" style="font-size:12px; /*color:#333333;*/">@lang('messages.create_account')</a>
					</p>
				</div>
				<div class="register-form hide">
					<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
						<h4 class="text-center m-t-0">@lang('messages.register')</h4>
					</div>
					<form id="formRegisterTop"  action="{{ route('public.register') }}" method="post" class="text-center">
						<input type="text" name="email" id="regTopUsername" class="login-input" placeholder="@lang('messages.e_mail')" >
						<div id="regTopError_email"></div>

						<input type="text" name="apellidos" id="regTopApellidos" class="login-input" placeholder="@lang('messages.lastname')" >
						<div id="regTopError_apellidos"></div>

						<input type="text" name="nombres" id="regTopNombres" class="login-input" placeholder="@lang('messages.firstname')" >
						<div id="regTopError_nombres"></div>

						<input type="text" name="empresa" id="regTopEmpresa" class="login-input" placeholder="@lang('messages.company')" >
						<div id="regTopError_empresa"></div>

						<input type="text" name="nit" id="regTopNit" class="login-input" placeholder="@lang('messages.company_nit')" >
						<div id="regTopError_nit"></div>

						{{--<select name="pais" id="regTopPais" class="login-input">--}}
							{{--<option value="">@lang('messages.select_country')</option>--}}
						{{--</select>--}}
						{!! Form::select('pais', $global_paises, $global_country_abr, [
									'id' => 'regTopPais',
									'class' => 'login-input',
									'placeholder' => __('messages.select_country')
						]); !!}
						<div id="regTopError_pais"></div>

						<input type="text" name="telefono" id="regTopTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
						<div id="regTopError_telefono"></div>

						<input type="password" name="password" id="regTopPassword" class="login-input" placeholder="@lang('messages.password')" >
						<div id="regTopError_password"></div>

						<input type="password" name="password_confirmation" id="regTopPassword2" class="login-input" placeholder="@lang('messages.password_confirmation')" >
						<div id="regTopError_password_confirm"></div>

						<br>
						{!! Form::checkbox('legalterms', 'legalterms', null, [
							'id' => 'legalterms',
							'style' => 'font-size:15px; margin:0px; vertical-align:middle;'
					    ]) !!}
						<label class="control-label " for="legalterms">
							@lang('messages.accept_terms', ['link' => '<a href="javascript:;" onclick="modalTerms()" style="font-weight:bold;">'.__('messages.legal_terms').'</a>'])
						</label>

						{{--<input type="submit" id="regTopBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
						<button type="submit" id="regTopBtnEnviar" form="formRegisterTop" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
						{{--<a href="javascript:;" id="regTopBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}
					</form>
					<hr />
					<p class="text-center">
						<a href="javascript:;" class="" onclick="activateLoginRegister('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
					</p>
				</div>
				<div class="forgot-form hide">
					<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
						<h4 class="text-center m-t-0">@lang('messages.forgot_password')</h4>
					</div>
					<form id="formForgotTop"  action="{{ route('public.forgotPassword') }}" method="post" class="text-center">
						<p>
							@lang('messages.forgot_text')
						</p>
						<input type="text" name="forgotEmail" id="regTopForgotEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
						<div id="regTopError_forgotEmail"></div>

						{{--<input type="submit" id="regTopBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
						<button type="submit" id="regTopBtnForgot" form="formForgotTop" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>
						{{--<a href="javascript:;" id="regTopBtnRegistrar" class="login-input" onclick="document.getElementById('formRegisterTop').submit();">@lang('messages.send')</a>--}}
					</form>
					<hr />
					<p class="text-center">
						<a href="javascript:;" class="" onclick="activateLoginRegister('login')" style="font-size:12px; /*color:#333333;*/">@lang('messages.have_account')</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	</div>

	<div class="modal fade" id="modalAnnounceHere">
	<div class="modal-dialog" >
		<div class="modal-content" style="margin-top:50px;">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6" style="background-image:url('/assets/img/e-commerce/ann_back_1.jpg'); background-repeat:no-repeat; background-position:/*center 55px;*/ center bottom; height:410px;">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.publish_a_machine')</h4>
						</div>

						<div class="text-center" style="margin-top:/*60px;*/ 45px;">
							@if(App::getLocale() == 'se')
								<a href="{{ route('public.publish') }}"><img src="/assets/img/e-commerce/publish_machine2_se.png" alt="Publish a machine"></a>
							@else
								<a href="{{ route('public.publish') }}"><img src="/assets/img/e-commerce/publish_machine2.png" alt="Publish a machine"></a>
							@endif
{{--								@lang('messages.publish_a_machine_soon')--}}
						</div>
					</div>
					<div class="col-md-6" style="border-left:1px dashed #e5e5e5 !important;">
						<div style="border-bottom:1px solid #D8E0E4; width:90%; margin:auto !important; padding:5px; margin-bottom:5px !important;">
							<h4 class="text-center m-t-0">@lang('messages.advertisement')</h4>
						</div>



						<form id="formAdPublisher"  action="{{ route('public.registerAdPublisher') }}" method="post" class="text-center">

							<p class="m-t-10">
								@lang('messages.leave_your_data_we_will_contact_you')
							</p>

							<span id="faError_messages" class="hide">
								<p class="alert alert-danger">@lang('messages.marked_fields_required')</p>
							</span>

							{{-- FORM INIT--}}
							@if($auth_nombres_apellidos == '')
								<input type="text" name="faApellidos" id="faApellidos" class="login-input" placeholder="@lang('messages.lastname')">
								<div id="faError_apellidos"></div>
							@else
								<input type="text" name="faApellidos" id="faApellidos" class="login-input" placeholder="@lang('messages.lastname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->apellidos }}" disabled>
							@endif

							<input type="hidden" id="faTipoConsulta" value="search">

							@if($auth_nombres_apellidos == '')
								<input type="text" name="faNombres" id="faNombres" class="login-input" placeholder="@lang('messages.firstname')" >
								<div id="faError_nombres"></div>
							@else
								<input type="text" name="faNombres" id="faNombres" class="login-input" placeholder="@lang('messages.firstname')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->nombres }}" disabled >
							@endif

							@if($auth_nombres_apellidos == '')
								<input type="text" name="faEmail" id="faEmail" class="login-input" placeholder="@lang('messages.e_mail')" >
								<div id="faError_email"></div>
							@else
								<input type="text" name="faEmail" id="faEmail" class="login-input" placeholder="@lang('messages.e_mail')" value="{{ \Illuminate\Support\Facades\Auth::guard('clientes')->user()->username }}" disabled >
							@endif


							<style>

							</style>
							{{--<select name="pais" id="faPais" class="login-input">--}}
							{{--<option value="">@lang('messages.select_country')</option>--}}
							{{--</select>--}}

							@if($auth_nombres_apellidos == '')
								{!! Form::select('pais', $global_paises, $global_localization, [
									'id' => 'faPais',
									'class' => 'login-input',
									'placeholder' => __('messages.select_country'),
									'style' => 'width:90% !important;  margin-top:10px;'
								]); !!}
								<div id="adPubError_pais"></div>
							@else
								{!! Form::select('pais', $global_paises, \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->pais, [
									'id' => 'faPais',
									'class' => 'login-input',
									'placeholder' => __('messages.select_country'),
									'style' => 'width:90% !important; margin-top:10px;',
									'disabled'
								]); !!}
							@endif

							@if($auth_nombres_apellidos == '')
								<input type="text" name="faTelefono" id="faTelefono" class="login-input" placeholder="@lang('messages.telephone')" >
								<div id="faError_telefono"></div>
							@else
								<input type="text" name="faTelefono" id="faTelefono" class="login-input" placeholder="@lang('messages.telephone')"
								       value="{{ (count(\Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos)) ? \Illuminate\Support\Facades\Auth::guard('clientes')->user()->Owner->Agenda->Telefonos->first()->numero : '' }}">
							@endif
							{{-- FORM END--}}

							{{--<input type="submit" id="adPubBtnEnviar" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;" value="@lang('messages.send')">--}}
{{--							<button type="submit" id="adPubBtnEnviar" form="formAdPublisher" value="@lang('messages.send')" class="promotion-btn m-t-20 m-b-10" style="width:50%; font-size:16px;">@lang('messages.send')</button>--}}
							<a id="btnFaPublisher" href="javascript:;"  class="promotion-btn m-t-20 m-b-10" onclick="guardarContactoPublicidad()" style="width:50%; font-size:16px;">@lang('messages.send')</a>
							{{--<a href="javascript:;" id="adPubBtnRegistrar" class="login-input" onclick="document.getElementById('formAdPublisher').submit();">@lang('messages.send')</a>--}}

							<div>
								@lang('messages.look_advertise_options') <a href="{{ route("public.{$glc}.servicios", ['service' => 'banner']) }}">@lang('messages.make_click_here').</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	@include('frontend.includes.page-js')

	<script>


//		$('img').attr('onerror', 'img_error(this);');
//
//		function img_error(img){
//			var nurl = $(img).prop('src');
//			nurl = nurl.replace('www2', 'www');
//			nurl = nurl.replace('expoindustriv2', 'expoindustri.com');
//			$(img).prop('src', nurl);
//		}


		var codigosPais = {!! json_encode($global_paisesCodigo) !!};

//		function initMap() {
//			// Temporarily disabled
//			return false;
//			var uluru = {lat: 57.7448628, lng: 14.1587678};
//			var map = new google.maps.Map(document.getElementById('loc_map'), {
//				zoom: 15,
//				center: uluru
//			});
//			var marker = new google.maps.Marker({
//				position: uluru,
//				map: map
//			});
//		}

		{{--$.getScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyALPqjAs5i_1KqcyQ_YMIEtxKblkrDIHaY').done(function() {--}}
			{{--initMap();--}}
			{{--@stack('mapsInit')--}}
		{{--});--}}

		$(document).ready(function(){
			$( "#formLoginTop" ).submit(function( event ) {
				event.preventDefault();
				tryLogin();
			});

			$( "#formRegisterTop" ).submit(function( event ) {
				event.preventDefault();
				tryRegister();
			});

			$( "#formForgotTop" ).submit(function( event ) {
				event.preventDefault();
				tryForgot();
			});

			$('#adPubPais, #regTopPais').select2({
				escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				templateResult: formatRepoPubAd,
				templateSelection: formatRepoSelectionPubAd
			});

			{{--var paises = {!! json_encode($global_paises) !!};--}}
			{{--$.each(paises, function(k,v){--}}
				{{--$('#regTopPais').append($('<option>', { value:v }).html(k));--}}
			{{--});--}}



			if($.fn.dataTable){
				$.fn.dataTable.ext.errMode = 'none';
				//$.fn.dataTable.ext.errMode = 'throw';
				$.extend( true, $.fn.dataTable.defaults, {
					language: {
						url: '{{ $global_dtlang }}'
					},
					ajax: {
						statusCode: {
							422: ajaxValidationHandler,
							500: ajaxErrorHandler,
							// 205: ajaxTokenMismatch,
							// 206: ajaxTokenMismatch,
							409: ajaxTokenMismatch
						}
					}
				} );
			}

			/*if ($.fn.datepicker.defaults) {
				$.fn.datepicker.defaults.format = "dd/mm/yyyy";
			}

			$.fn.modal.Constructor.prototype.enforceFocus = function() {};

			$.extend($.gritter.options, {
				// class_name: 'gritter-light', // for light notifications (can be added directly to $.gritter.add too)
				position: 'bottom-right', // possibilities: bottom-left, bottom-right, top-left, top-right
				// fade_in_speed: 100, // how fast notifications fade in (string or int)
				// fade_out_speed: 100, // how fast the notices fade out
				// time: 3000 // hang on the screen for...
			});*/


		});

		function openLoginRegister(action) {
			setModalHandler('loginRegister:loginSuccessful', function(){
				redirect('{{ route("public.{$glc}.privCuenta") }}');
			});
			launchLoginRegister(action);
		}

		function launchLoginRegister(action){
			activateLoginRegister(action);
			var modalLogin = $('#modalLoginRegister').modal();
			modalLogin.on('hidden.bs.modal', function (e) {
				$(document).trigger('modal:hidden');
			});
		}

		function openAnnounceHereModal(){
			$('#modalAnnounceHere').modal();
		}

		function activateLoginRegister(action){
			$('.login-form, .register-form, .forgot-form').addClass('hide');
			$('.'+action+'-form').removeClass('hide');
		}

		function tryLogin(){

			$('#regTopBtnEntrar').html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
			$('#regTopBtnEntrar').attr('disabled', 'disabled');

			var url = '{{ route('public.login') }}';
			var data = {
				email: $('#loginTopUsername').val(),
				password: $('#loginTopPassword').val(),
			};
			ajaxPost(url, data,{
				silent:true,
				onSuccess: function(data){
					$('#regTopBtnEntrar').html('@lang('messages.log_in')');
					$('#regTopBtnEntrar').removeAttr('disabled');
					$('#userMenuClient').removeClass('hide');
					$('#userMenuGuest').addClass('hide');
					$('#userMenuClient .client-name').prepend(data.data.email);
					console.log(data.data.email);
					$(document).trigger('loginRegister:loginSuccessful');

				},
				onError: function(e){
					$('#regTopBtnEntrar').html('@lang('messages.log_in')');
					$('#regTopBtnEntrar').removeAttr('disabled');
				},
				onValidation: function(e){
					$('#regTopBtnEntrar').html('@lang('messages.log_in')');
					$('#regTopBtnEntrar').removeAttr('disabled');
//					var errors = JSON.parse(e.responseText);
					var errors = e;
					var message = '';
					for(var elem in errors) {
						if(Array.isArray(errors[elem])){
							message += errors[elem].join('<br>') + '<br>';
						} else {
							message += errors[elem];
						}

					}
					$('#loginTopErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
					return false;
				}
			});
		}

		function tryRegister() {

			if($('#legalterms').prop('checked')) {
			} else {
{{--				alert('@lang('messages.please_accept_terms') ');--}}
				swal('', "@lang('messages.please_accept_terms')", "error");
				return false;
			}

			$('#regTopBtnEnviar').html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
			$('#regTopBtnEnviar').attr('disabled', 'disabled');

			var url = '{{ route('public.register') }}';
			var data = {
				email: $('#regTopUsername').val(),
				apellidos: $('#regTopApellidos').val(),
				nombres: $('#regTopNombres').val(),
				empresa: $('#regTopEmpresa').val(),
				nit: $('#regTopNit').val(),
				pais: $('#regTopPais').val(),
				telefono: $('#regTopTelefono').val(),
				password: $('#regTopPassword').val(),
				password_confirmation: $('#regTopPassword2').val(),
				lang: '{{ App::getLocale() }}',
			};
			ajaxPost(url, data,{
				silent:true,
				onSuccess: function(data){
					$('#regTopBtnEnviar').html('@lang('messages.send')');
					$('#regTopBtnEnviar').removeAttr('disabled');
					redirect('{{ route("public.{$glc}.privCuenta") }}');
				},
				onError: function(e){
					$('#regTopBtnEntrar').html('@lang('messages.log_in')');
					$('#regTopBtnEntrar').removeAttr('disabled');
				},
				onValidation: function(e){
					$('#regTopBtnEnviar').html('@lang('messages.send')');
					$('#regTopBtnEnviar').removeAttr('disabled');
//					var errors = JSON.parse(e.responseText);
					var errors = e;
					var message = '';

					var elements = [
						'email',
						'apellidos',
						'nombres',
						'empresa',
						'nit',
						'pais',
						'telefono',
						'password',
						'password_confirmation'
					];

					for(var elem2 in elements){
						$('#regTopError_'+elements[elem2]).html('');
//						$('#regTopError_'+elements[elem2]).parent().find('input').css({'border-color': '#e5e5e5'});
					}

					for(var elem in errors) {
						if(Array.isArray(errors[elem])){
							message = errors[elem].join('<br>') + '<br>';
						} else {
							message = errors[elem];
						}
						$('#regTopError_'+elem).html('<span class="help-block">'+message+'</span>');
//						$('#regTopError_'+elem).parent().find('input').css({'border-color': '#ff0000'});
					}
//					$('#loginTopErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
					return false;
				}
			});

		}

		function tryForgot() {
			$('#regTopBtnForgot').html('<i class="fas fa-circle-notch fa-spin"></i> @lang('messages.sending')...');
			$('#regTopBtnForgot').attr('disabled', 'disabled');

			var url = '{{ route('public.forgotPassword') }}';
			var data = {
				forgotEmail: $('#regTopForgotEmail').val(),
			};
			ajaxPost(url, data,{
				silent:true,
				onSuccess: function(data){
					$('#regTopBtnForgot').html('@lang('messages.send')');
					$('#regTopBtnForgot').removeAttr('disabled');
					//$('#msgMsgSent').removeClass('hide');
					{{--redirect('{{ route("public.{$glc}.privCuenta") }}');--}}
					swal("", "@lang('messages.forgot_message_sent')", "success")
						.then(function(response){
							{{--					redirect('{{ route("private.{$glc}.announcements") }}');--}}
							{{--redirect('{{ route('public.checkout') }}');--}}
							$('#modalLoginRegister').modal('hide');
						});

				},
				onError: function(e){
					$('#regTopBtnForgot').html('@lang('messages.send')');
					$('#regTopBtnForgot').removeAttr('disabled');
				},
				onValidation: function(e){
					$('#regTopBtnForgot').html('@lang('messages.send')');
					$('#regTopBtnForgot').removeAttr('disabled');
//					var errors = JSON.parse(e.responseText);
					var errors = e;
					var message = '';

					var elements = [
						'forgotEmail',
					];

					for(var elem2 in elements){
						$('#regTopError_'+elements[elem2]).html('');
					}

					for(var elem in errors) {
						if(Array.isArray(errors[elem])){
							message = errors[elem].join('<br>') + '<br>';
						} else {
							message = errors[elem];
						}
						$('#regTopError_'+elem).html('<span class="help-block"><strong>'+message+'</strong></span>')
					}
//					$('#loginTopErrors').html('<span class="help-block"><strong>'+message+'</strong></span>')
//					toast('Alerta', message, 'warning');
					return false;
				}
			});
		}

		function changeLanguage(lang){
			var url = '{{ route('public.changeLanguage') }}';
			var data = {
				lang: lang,
			};
			ajaxPost(url, data,{
				silent:true,
				onSuccess: function(data){
					location.reload();
				},
				onError: function(e){

				},
				onValidation: function(e){

				}
			});

		}

		function changeCurrency(cur){
			var url = '{{ route('public.changeCurrency') }}';
			var data = {
				cur: cur,
			};
			ajaxPost(url, data,{
				silent:true,
				onSuccess: function(data){
					location.reload();
				},
				onError: function(e){

				},
				onValidation: function(e){

				}
			});

		}

		function formatRepoPubAd (repo) {

			if (repo.loading) {
				return repo.text;
			}

			var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__title'>" + repo.text + "</div>";

			if (repo.description) {
				markup += "<div class='select2-result-repository__description'> " + repo.description + "</div>";
			}

			return markup;
		}

		function formatRepoSelectionPubAd (repo) {
			return repo.full_name || getFullNamePubAd(repo);
		}

		function getFullNamePubAd(repo){
			if(repo.id != ''){
				return '<span class="flag-icon flag-icon-'+ repo.id +' "></span> &nbsp;&nbsp;' + '+' + codigosPais[repo.id] + ' ' + repo.text;
			} else {
				return repo.text;
			}

		}

		function guardarContactoPublicidad() {
			var url = '{{ route('public.consultaExterna.store') }}';
			var data = {
				nombres : $('#faNombres').val(),
				apellidos : $('#faApellidos').val(),
				email : $('#faEmail').val(),
				pais : $('#faPais').val(),
				telefono : $('#faTelefono').val(),
//				descripcion : $('#faDescripcion').val(),
				descripcion : 'Formulario sin descripción, solo provee informacion de contacto para el interesado.',
				tipo : 'banner',
			};
			ajaxPost(url, data, {
				onSuccess: function(data){
					$('#modalAnnounceHere').modal('hide');
					$('#faNombres').val('');
					$('#faApellidos').val('');
					$('#faEmail').val('');
					$('#faPais').val('');
					$('#faTelefono').val('');
					$('#faDescripcion').val('');
					swal("@lang('messages.comments_sent')!", "@lang('messages.su_consulta_enviada_correctamente')", "success");
					{{--.then(function(response){--}}
					{{--redirect('{{ route("private.{$glc}.announcements") }}');--}}
					{{--});--}}
				},
				onValidation: function(data){

					$('#faNombres').removeClass('inputError');
					$('#faApellidos').removeClass('inputError');
					$('#faEmail').removeClass('inputError');
					$('#faPais').removeClass('inputError');
					$('#faTelefono').removeClass('inputError');
					$('#faDescripcion').removeClass('inputError');
					$('.select2-container').removeClass('inputError');

					$.each(data, function(key, value){

						key = key.replace(/^\w/, function (chr) {
							return chr.toUpperCase();
						});

						if(key === 'Pais'){
							$('.select2-container').addClass('inputError');
						}

						$('#fa' + key).addClass('inputError');

					});

					$('#faError_messages').removeClass('hide');
				}
			});
		}

	</script>

	<!-- BEGIN JIVOSITE CODE {literal} -->
	{{--<script type='text/javascript'>--}}
		{{--(function(){ var widget_id = 'hZoJlMrJaL';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();--}}
	{{--</script>--}}
	<!-- {/literal} END JIVOSITE CODE -->


</body>
</html>
