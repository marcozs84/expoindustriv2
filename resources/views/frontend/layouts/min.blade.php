<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	@include('frontend.includes.head')

	<style>
		.header {
			/*z-index:1050 !important;*/
		}

		@media (min-width:991px) {
			.btnAnnouncement {
				/*margin-left:100px; margin-top:-8px;*/
				position:absolute; margin-top:32px; margin-left:850px;
			}
			.header-logo img {
				max-height:150px;
			}
		}
		@media (max-width:1200px) {
			.btnAnnouncement {
				/*margin-left:10px; margin-top:-8px;*/
				position:absolute; margin-top:32px; margin-left:715px;
			}
			.header-logo img {
				max-height:150px;
			}
		}
		@media (max-width:992px) {
			.btnAnnouncement {
				display:none;
			}
		}
		@media (max-width:768px) {
			.btnAnnouncement {
				display:none;
			}
			.header-logo {
				position: absolute;
				width:100%;
			}
			.header-logo img {
				max-height:60px;
			}
		}

		/* ---------------- MODALS*/

		h5.modal-title{
			display:inline;
		}
	</style>
</head>
<body class="bg-silver">
	<!-- BEGIN #page-container -->
	<div id="page-container" class="fade">
		<!-- BEGIN #top-nav -->
		<!-- END #top-nav -->


	@yield('content')


	</div>
	<!-- END #page-container -->

	<style>
		/*.loginRegion .nav-tabs {*/
			/*background-color:#000000;*/
		/*}*/
		/*.loginRegion .nav.nav-tabs > li.nav-item. > a:hover {*/
			/*background-color:#0000ff !important;*/
		/*}*/
		.loginRegion.modal {
			text-align: center;
			padding: 0!important;
		}

		.loginRegion.modal:before {
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}

		.loginRegion .modal-dialog {
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}
		.promotion-btn{
			border-color: #fff;
			color: #fff;
			text-decoration: none;
			background: #000;
		}
		.promotion-btn:hover{
			border-color: #000;
			color: #000;
			text-decoration: none;
			background: #fff;
		}
		.nav-tabs.nav-tabs-inverse > li > a:hover{
			background-color:#666666 !important;
			border:1px solid #666666;
		}
		.nav-tabs.nav-tabs-inverse > li.active > a:hover{
			background-color:#ffffff !important;
			border:1px solid #ffffff;
		}

		.login-input {
			padding:5px;
			border: 1px solid #e5e5e5;
			width:90%;
			margin-top:10px;
		}
		::placeholder {
			color:#7a7a7a;
		}
		@media (min-width:768px) {
			.loginRegion .modal-dialog {
				width:400px;
			}
		}

	</style>

	@include('frontend.includes.page-js')

	<script>

		$(document).ready(function(){

		});

	</script>

</body>
</html>
