<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'e_mail' => 'E-mail',
    'password' => 'Password',
    'password_confirmation' => 'Password confirmation',
	'firstname' => 'Firstname',
	'lastname' => 'Lastname',
	'telephone' => 'Telephone',
	'country' => 'Country',
	'select_country' => 'Select your country',
	// ---------- layout/default
	'my_orders' => 'My Orders',
	'my_account' => 'My Account',
	'logout' => 'Logout',
	// ---------- TOP MENU
	'import_export' => 'Import & Export',
	'home' => 'Home',
	'about_us' => 'About us',
	'payment_methods' => 'Payment Methods',
	'questions' => 'Questions',
	'contact_us' => 'Contact Us',
	'contact_us2' => 'Contact Us',
	// ---------- Lenguajes
	'spanish' => 'Spanish',
	'english' => 'English',
	'swedish' => 'Swedish',
	// ---------- loginForm
	'welcome' => 'Welcome',
	'log_in' => 'Log in',
	'register' => 'Register',
	'create_account' => 'Create account',
	'forgot_password' => 'Forgot your password?',
	'remember_me' => 'Remember me',
	'send' => 'Send',
	'have_account' => 'I have an account',
	// ---------- CONFIRM CUENTA
	'confirm_cuenta' => 'Account confirmation',
//	'questions' => 'Questions',
	// ---------- CONTACT FORM
//	'contact_us' => 'Contact Us',
	'contact_form' => 'Contact Form',
//	'firstname' => '',
	'contact_form_info' => 'Do you want to sell or buy, consult one of our experts.', // 'Our international team has years of experience in the industry, so we know very well what we do. Make your inquiry!',
	'subject' => 'Subject',
	'message' => 'Message',
	'send_message' => 'Send Message',
	'our_contacts' => 'Our Contacts',
	'social_networks' => 'Social Networks',
	// ---------- SEARCH TERMS
	'search_by_category' => 'Search by category',
	'transportation' => 'Transportation',
	'equipments' => 'Equipments',
	'industrial' => 'Industrial',
	'agriculture' => 'Agriculture',
	'forklifts' => 'Forklifts',
	'others' => 'Others',
	'prefered_brands' => 'Prefered brands',
	// ---------- COMMON LAYOUT DEFAULT
	'site_map' => 'Tips', //'Site map',
	'information_for' => 'Information for :subject',
	'announcers' => 'Announcers',
	'buyers' => 'Buyers',
	'information_for_announcers' => 'Tips for <b>Announcers</b>',
	'information_for_buyers' => 'Information for <b>Buyers</b>',
	'frequent_questions' => 'Frequent questions',
//	'payment_methods' => 'Payment Methods',
	'legal_terms_conditions' => 'Legal Terms and Conditions',
	'privacy_policy' => 'Privacy Policy', // Política de privacidad
//	'about_us_footer' => '<b> ExpoIndustri </b> is a classified ads portal where you can find machinery and tools of different activity types, both new and used, from Europe to South America, at the final destination you choose, guaranteed with  more than 10 years of experience in the import-export sector between these two continents.',
	'about_us_footer' => '<b> Expoindustri.com </b> is a portal for companies in Europe that want to buy or sell in the simplest and most fluid way possible. <br><br> Behind the web portal is a team with more than 20 years of experience doing all the work for you, from picking up the machine anywhere in Europe, to delivery to all destinations, including South America.',
	'central_office' => 'Main Office',
	'phone' => 'Phone',

	'contact_form_sent' => 'Message sent succesfully',
	'contact_form_sent_info' => 'Thank you very much for your contact, a representative from our team will contact you very soon.',
	'sending' => 'Sending',
	'sent' => 'Sent',
	'services' => 'Services',
	'our_offices_southamerica' => 'Our offices in Sudamerica',
	'find_your_machine' => 'Find your machine',
	'search' => 'Search',
	'order_by' => 'Order by',
	'popular' => 'Popular',
	'new_arrival' => 'New Arrival',
	'discount' => 'Discount',
	'price' => 'Price',
	'items_found' => 'We found :number Items',
	'all_f' => 'all',
	'all_m' => 'all',
	'select_category' => 'Select category',
	'select_subcategory' => 'Select subcategory',
	'criteria' => 'Criteria',
	'category' => 'Category',
	'sub_category' => 'Sub Category',
	'brand' => 'Brand',
	'from' => 'From',
	'to' => 'To',
	'year' => 'Year',
	'hi_tweet' => 'Hi!, check this machine',
	'calculator' => 'Calculator',
	'additional_info' => 'Additional Information',
	'rating_reviews' => 'Rating & Reviews',
	'add_favorites' => 'Add to Favorites',
	'quote_final_destination' => 'Quote on final destination',
	'select_country_first' => 'Select your country first',
	'select_preferred_zone' => 'Select the zone of your preference',
	'publish' => 'Publish',
	'model' => 'Model',
	'next' => 'Next',
	'previous' => 'Previous',
	'subcategory' => 'Sub-category',
	'required' => 'Required',
	// ---------- currency
	'currency' => 'Currency',
	'dollars' => 'Dollars',
	'euros' => 'Euros',
	'crowns' => 'Swedish Crown',
	// ---------- status
	'active' => 'Active',
	'inactive' => 'Inactive',
	'approved' => 'Approved',
	'rejected' => 'Rejected',
	'awaiting_approval' => 'Awaiting approval',
	'reported' => 'Reported',
	'draft' => 'Draft',
	'removed' => 'Removed',
	// ---------- CLIENT TYPES
	'client' => 'Client',
	'provider' => 'Provider',
	'employee' => 'Employee',
	'employer' => 'Employer',


	'dz_upload' => 'Drop files here to upload',
	'invalid_image_file' => 'Not a valid image file',
	'invalid_file' => 'Not a valid file',
	'remove' => 'Remove',
	'machine_location' => 'Machine location',
	'machine_location_descr' => '<a target="_blank" href=":route">(View our data protection policy)</a>', //'Provided address will only be known and used by Expoindustri.<a target="_blank" href=":route">(View our data protection policy)</a>',
	'data_protection_policy' => 'Data protection policy',
	'city' => 'City',
	'postal_code' => 'Postal Code',
	'location' => 'Location',
	'address' => 'Address',
	'findInMap' => 'Find in map',
	'transport_info' => 'Transport information',
	'transport_info_descr' => 'Please provide extra information for transport calculation.',
	'longitude_mm' => 'Longitude (mm)',
	'width_mm' => 'Width (mm)',
	'height_mm' => 'Height (mm)',
	'weight_brute' => 'Weight brute (Kg)',
	'palletized' => 'Palletized',
	'confirm_removal' => 'Do you want to remove the selected image?',
	'user_info' => 'User Information',
	'user_info_descr' => 'Who is publishing this machine?',
	'select_your_category' => 'Select your machine category',
	'select_your_category_descr' => 'Group your machine with others of the same kind.',
	'basic_information' => 'Basic information',
	'basic_information_descr' => 'Brand and model',
	'new_announce' => 'Your new announce',
	'click_next' => 'Click Next to continue',
	'payment_options' => 'Payment options',
	'payment_options_descr' => 'How do you want to pay your announce?',
	'payment_announce_message' => 'In order to keep your announce active in our site, we offer a daily fee of <span id="txt_price" style="font-weight:bold;">:price (Taxes included)</span> 180 days minimum, please select how many days do you want your announce to be shown and then proceed with your payment in any of these options.',
	'payment_transfer' => 'Transfer to bank account',
	'payment_transfer_descr' => 'You can transfer the money to the following account: <br><br> <b>1-156345698545</b><br><b>Bank LoremDolar</b>',
	'payment_creditcard' => 'Credit Card Payment',
	'payment_creditcard_descr' => 'Please select the bank card with which you wish to pay.',
	'payment_faktura' => 'Bill payment',
	'payment_faktura_descr' => 'When you register your ad, we will send you an invoice with payment terms for 10 days.',
	'payment_cash' => 'Telephone call',
	'payment_cash_descr' => 'You can call to the following numbers: <br><br> <b>1-125465488454</b><br> <b>1-125465488454</b>.',
	'days' => 'Days',
	'accept_terms' => 'I have read and accepted the :link',
	'legal_terms' => 'Legal Terms and Conditions',
	'publish_pay' => 'Publish your announce',
	'payment_trustly' => 'Use Trustly',
	'payment_trustly_descr' => 'You can transfer the money through the trustly service.',
	'verify_ccard' => 'Please verify your credit card information',
	'announce_detail' => 'Announce details',
	'announce_detail_descr' => 'Great detail will bring better chances of interested people.',
	'provide_card_info' => 'Please provide your credit card information.',
	'card_number' => 'Card number',
	'expiration_date' => 'Expiration date',
	'security_code' => 'Security code',
	'time_extended_title' => 'Time extended announcement',
	'time_extended_description' => 'If you hire more than 100 days, your announce will be renewed automatically every 50 days, allowing it to appear in the <b>"Most recent announcements"</b> section in our home page.',
	'loading' => 'Loading',

	'info_buyers_title' => '¡IMPORTANT! YOU AS A CLIENT',
	'info_announcers_title' => 'A GOOD ADVICE FOR YOU AS ANNOUNCER!',
	'no_brand' => 'Without brand',
	'no_model' => 'Without model',
	'no_product' => 'Without product',

	'product' => 'Product',
	'remove_favorite' => 'Remove favorite',
	'is_favorite' => 'Is favorite',
	'my_favorites' => 'My favorites',
	'my_favorites.leftmessage' => 'Your announces marked as favorites.<br>Please remember all prices are subject to the exchange rate of the day.',
	'my_announces' => 'My announces',
	'my_announces.leftmessage' => 'All you need in a single place, please let us know if you need assitance.',
	'no_active_users' => 'There are not active users.',
	'favorites.leftmessage' => '',
	'request_quote' => 'Request offer', //'Create Order',

	// Mensaje mostrado cuando un cliente envía una cotización desde la página de producto.
//	'quote_requested_correctly' => 'Your order was sent correctly.  A copy of this order was sent to your e-mail account, a consultant will get in touch as soon as possible.',
	'quote_requested_correctly' => 'Thank you very much, we have received your order. A copy has been sent to your email account and a consultant will contact you as soon as possible.',

	// Mensaje mostrado cuando un cliente envía exitosamente una nueva publicación a través del formulario Publish Here.
	'publish_success_msg' => 'Our team will review your announce and it will be published in the following 2 hours, otherwise we will contact you as soon as possible.',
	'publish_successful' => 'Your announce was sent successfully',
	'price_in_europe' => 'Price in Europe',
	'owner_price' => 'Owner price',
	'exchange_rate' => 'Exchange rate',

	'includes_taxes' => 'Taxes included',
	'disabled' => 'Disabled',
	'select_preferred_transport' => 'You must select a transport method.',
	'publish_review_announce' => 'Incomplete ad',
	'publish_missing_images' => 'Please remember that you must provide at least 1 image for your ad.',
	'recent_announces' => 'Latest ads',
	'be_first_visitor' => 'Be the first to visit them.',

	'most_visited' => 'Most visited announcements.',
	'most_visited_small' => 'It could be of interest for you.',

	'company' => 'Company',
	'company_name' => 'Company name',
	'company_nit' => 'Corporate Identification Number',

	'longitude' => 'Longitude',
	'width' => 'Width',
	'height' => 'Height',

	'my_orders.leftmessage' => 'List of orders made on our website.', //'List of orders made on our website. <br> All you need in a single place, please let us know if you need assitance.',
	'order_detail' => 'Order detail',
	'payment' => 'Payment',
	'payments' => 'Payments',
	'description' => 'Description',
	'amount' => 'Monto',
	'dimention' => 'Dimension',
	'dimentions' => 'Dimensions',
	'weight' => 'Vikt',

	'order' => 'Order',
	'orders' => 'Orders',
	'save' => 'Save',
	'logo_background_color' => 'Background color (logo)',
	'no_image_available' => 'No image available',
	'urlSite' => 'Website',
	'urlFacebook' => 'Facebook',
	'urlTwitter' => 'Twitter',
	'urlGooglePlus' => 'Google Plus',

	'search_alert' => 'Didn\'t you find your machine?',
	'comments' => 'Comments',

	'marked_fields_required' => 'The highlighted fields are required.',
	'comments_sent' => 'Request sent',
	'su_consulta_enviada_correctamente' => 'Your message was sent correctly.',

	'search_alert_msg' => 'Could not you find the machine you need on our website? Do not worry!! <br>The ExpoIndustri team is there to support you with its multiple search channels to find what kind of machine on the European market.',
	'search_alert_msgfrm' => 'Please enter all the requested information and tell us what your specific request is. Shortly we will contact you.',

	'our_services' => 'Our Services',
	'our_services_slogan' => 'Much more than a solution!<br>We work for the prosperity of your company',
	'our_services_slogan_short' => 'We work for the prosperity of your company',

	'about_expoindustri' => 'About ExpoIndustri',

	'what_we_do' => 'What we offer',
	'all_you_need' => 'All you need to know about <b>ExpoIndustri</b>.  What <b>WE ARE</b> and what we <b>WILL BE</b>',

	'publish_a_machine' => 'Publish your machine',
	'leave_your_data_we_will_contact_you' => 'Please leave your contact information so we can contact you sooner.',
	'advertisement' => 'Advertisement',
	'look_advertise_options' => 'Take a look to our advertisement options.',
	'make_click_here' => 'Click here',

	'my_account_msg' => 'Modify an order, or track your cargo and update your personal information.
	<br><br>
	Everything you need in one place. Please do not hesitate to contact us if you require assistance.',

	'account_config' => 'Account configuration',
	'saved_announces' => 'Favorites',
	'udate_mail_password' => 'Update E-mail and password',
	'payments_finance' => 'Payments & finances',
	'list_enabled_cards' => 'Enabled credit cards list',
	'support' => 'Support',

	'top_message_unconfirmed_title' => 'Confirm your account!',
	'top_message_unconfirmed_msg' => 'We have sent an email to confirm your account: <b>:email</b>. Please confirm your account within 24 hours. (please check your spam as it may end up there)',

	'continue' => 'Continue',
	'account_confirmed' => 'Account confirmed',
	'account_confirmed_msg' => '
		Welcome and thanks for your confirmation!<br>
		We hope that your experience at ExpoIndustri will be positive.<br>
		Please contact us for questions.<br><br>
		
		Sincerely,<br>
		<strong>ExpoIndustri Team</strong>
	',

	'register_invalid_token' => 'Invalid token',
	'register_invalid_token_msg' => 'The provided token is invalid, it is possible that your account is already active or that it was disabled for some reason, please contact us through our <a href=":linkContact">contact form</a>.',
	'register_confirmation_error' => 'Confirmation error',
	'register_confirmation_error_msg' => 'An error occured during the confirmation process, our team has been notified and a consultant will contact you as soon as possible.',

	'gallery' => 'Gallery',
	'time_extended_checkbox' => 'Place your ad again in the top positions the first in each month automatically for only :price SEK.',
	'close' => 'Close',

	'payment_dibs' => 'Payment through DIBS',
	'all_rights_reserved' => 'All rights reserved',
	'search_alert_title' => 'Couldn\'t find what you were searching for?',
	'forgot_message_sent' => 'The message is sent correctly, check your inbox. If for some reason does not reach your tray, please check your \"Spam\"',
	'forgot_text' => 'Please enter your email, we will send you a link to update your password.',
	'user_not_found' => 'User not found',
	'password_reseted' => 'Your password has been updated correctly and will now be redirected to your private account. If you are not redirected automatically, use the following link ',
	'old_password_different' => 'Your current password does not match.',
	'password_updated_succesfully' => 'Password updated correctly',

	'current_password' => 'Current password',
	'new_password' => 'New password',
	'password_update' => 'Update password',
	'current_email' => 'Current email',
	'update_email' => 'Update email',
	'update_email_info' => 'Please remember, this is your current username to access your account.',
	'company_phone' => 'Número de teléfono', //'Your company\'s phone',
	'sweden' => 'Sweden',

	'see_more' => 'See more',
	'process_flow' => 'Process flow',
	'process_flow_content' => 'Learn step by step in the logistics process.',
	'duty_brokers' => 'Duty brokers',
	'duty_brokers_content' => 'To get help needed for your customs clearance',

	'aboutus_slogan' => 'Who we are! Our history and much more',
	'goodfaith' => 'I certify that the machine I publish is free from debts or commitments with third parties.',

	'index_process_flow_content' => 'To learn step by step the whole process.',

	'index_brokers_title' => 'Duty brokers',
	'index_brokers_content' => 'To get help needed for your customs clearance in South America.',

	'index_security_title' => 'Secure transaction',
	'index_security_content' => 'For your security and peace of mind, your credit cards and payments are managed through <a href="https://www.stripe.com" target="_blank">Stripe.com</a>.',

	'register_report_token_invalid' => 'The provided token is invalid, please contact us through our',
	'register_report_user_confirmed_title' => 'User activated previously',
	'register_report_user_already_confirmed' => 'YYour user has already been confirmed before, if you want to cancel your account, please make your request via our :contact_form',
	'register_report_unsubscribe_title' => 'Identity theft reported successfully',
	'register_report_unsubscribe' => '
Thank you very much for reporting this subscription, this message has been sent to a representative of our team who will unsubscribe this email account.
<br><br>
Best regards,<br>
<b>ExpoIndustri team</b>',
	'disabled_account' => 'Account disabled',
	'register_report_disabled_account_mail' => 'Thank you very much for reporting this subscription, this account has already been disabled in our system.
<br><br>
Greetings,<br>
<b>ExpoIndustri team</b>',

	'delete_message_not' => 'Wasn\'t possible to delete some records. Deleted :count of :total',
	'service_request_mistype' => 'The query must be one of the following types: banner, search, inspection, support, istore.',
	'record_created' => 'Record created',
	'added_favorites' => 'Added to favorites',
	'preorder_reported' => 'pre-order reported',
	'preorder_reported_message' => 'Thank you very much for reporting this publication, one of our consultants has been notified.',
	'you_might_also_like' => 'You may also be interested',
	'related_products_category' => 'Related products of the same category',

	'quote_equipment_in_final_destination' => 'Calculation of machine in final destination',
	'calculator_text' => '<p>With our practical calculation system, you can easily calculate the final price of your product. From order to selected additions as you made and to final destination.</p> 
	<p>If you have any questions, please contact us at once through our :form_link</p>',

	'price_machine_in_europe' => 'Price of machine', //'Price of machine in Europe',
	'require_additional_info' => 'Need additional information?',
	'technichal_information' => 'Technical information',
	'request_require_payment' => 'This option requires advance payment',
	'require_extra_assurance' => 'Supplementary insurance for your machine to final destination?',
	'extra_insurance' => 'Extra insurance',
	'how_transport_machine' => 'What shipping option do you want to choose?',
	'soon_at_expoindustri' => 'Coming soon on <br><b>ExpoIndustri.com</b>',
	'select_final_destination' => 'Select the final destination',
	'price_in_final_destiny' => 'Final price after delivery',
	'how_guarantee_buy' => 'Certified distributor', //'How to guarantee your purchase?',
	'guarantee_content' => '<p>
		All purchases of machines through ExpoIndustri are supported by an invoice, according to our terms and conditions
	</p>
	<p>
		When you send request via our site, we make sure the machine is still available. Once checked and confirmed, we will send a preliminary invoice (pre-invoice) and the steps that need to be followed to proceed with secure payment.
	</p>
	<ul class="fa-ul">
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			ExpoIndustri guarantees the purchase
		</li>
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			100% safe transportation
		</li>
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			Support of highly trained staff
		</li>
	</ul>',
	'is_secure_payment' => 'Safe payments', //'Is it safe to make the payment?',
	'is_secure_payment_content' => '<p>
	ExpoIndustri works with the most well-known companies in finance, to secure your payments
	</p>
	<p>
	Therefore, the ExpoIndustri offers you:
	</p>
	<ul class="fa-ul">
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Simple bank transfer
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Pay in your comfort zone
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Debit/credit card
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		PayPal
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Stripe
	</li>
	</ul>',

	'usd' => 'USD',
	'eur' => 'EUR',
	'sek' => 'SEK',

	'precios_not_match' => 'The prices do not match?',
	'precios_not_match_msg' => 'Remember that prices are calculated based on the current exchange rate.',

	'validity' => 'Validity',
	'visits' => 'Visits',
	'state' => 'State',
	'publication_date' => 'Publication date',
	'doubts_contact_us' => 'Doubts? Contact us',
	'go_to_contact' => 'Write to us here', //'Go to the contact form',
	'public_profile' => 'Public profile',
	'profile_info' => 'Públic information',
	'public_profile_msg' => 'Description of your company that will be visible in your public profile.',

	'password_msg' => 'Remember that your password must have at least 6 characters. If you have problems remembering your password, consult one of our consultants, they will gladly answer your request.',
	'all_you_need_one_place' => 'Everything you need in one place. Please do not hesitate to contact us if you require assistance.',
	'my_credit_debit_cards' => 'My debit / credit cards',
	'my_credit_debit_cards_msg' => 'Recently used cards, you must have at least 1 registered card.',
	'technical_info_company' => '<b>ExpoIndustri.com</b> uses an external company. Experienced in the field of technical inspections. That maintains a high level of well-trained staff who offer our customers a well-detailed report. Read more about 
		<a href="https://www.mevas.eu/" target="_blank">www.mevas.eu</a>',
	'insurande_for_machine' => 'Insurance for machine',
	'publish_a_machine_soon' => 'Public announcement will be available from November 14th 2018.',

	'provider_profile' => 'Supplier profile',
	'announcements_provider' => 'Ads by this supplier',

	'office_name' => 'Office name',
	'company_address' => 'Add more details',
	'preview_link' => 'To preview your profile, <a href=":link" class="btn btn-warning" style="font-weight:bold;">click here</a>',
	'company_information' => 'Company information',
	'company_name_required' => 'Company name is required.',
	'company_nit_required' => 'Corporate Identification Number is required.',
	'select_main_office_location' => 'Please add more information like: address, contact person, e-mail', //'Please provide your main office address and then press <b>Search</b> to locate it in the map, if it is not found try adding more details or just click the exact location directly in the map.',
	'add_secondary_addresses' => 'Add addresses',
	'filial_name' => 'Filial',
	'suggested_dimention' => 'Suggested dimentions are: <br>550px x 100px.',
	'announce_here' => 'Announce here',
	'go_to_invoice' => 'Go in and look at your payment receipt',
	'invoice' => 'Invoice',
	'wanna_remove_announcement' => 'Do you really want to eliminate this ad?',
	'announcement_removed' => 'Your ad was removed succesfully',
	'internal_error_title' => 'Oops!',
	'internal_error_message' => 'An error has occurred in our system and it has been reported to our team.  Please send us your comments to info@expoindustri.com, we will gladly answer your questions.',
	'charges_technical_report' => 'Los cargos por el informe técnico serán descontados al completar la orden de compra.',
	'handling_fee' => 'Operating costs',
	'handling_fee_description' => 'Necessary costs to be able to execute the service in its entirety, for activities such as preparation of documentation, import-export process, loading and freight, among others.',
	'how_we_do_it' => 'How we do it?',
	'use_different_card' => 'Use a new card.',
	'availability' => 'Availability',
	'create_announce' => 'Create new announcement',
	'price_southamerica_only' => 'If you only want the price to be visible in South America. Check here',
	'contact_consultant' => 'Contact a consultant',
	'product_contact_title' => 'Request more information',
	'product_contact_message' => 'I would like to receive more information related to this product:<br><h2 style="font-size:25px;">:product_name</h2>',
	'price_not_available' => 'Request a quote', //'Price not available',
	'news' => 'Machine demand', //'News',
	'news_source' => 'Source',
	'ver_noticia' => 'Read the full article',

	'active_subscriptions' => 'Active plans',
	'renew' => 'Renew',
	'requires_action' => 'Requires action',
	'due_date' => 'Due date',
	'view_all_your_plans' => 'View all your plans',
	'overdue' => 'Overdue',
	'no_active_plans' => 'You have no active plans',

	'our_price' => 'Our prices',
	'single_announcement_post' => 'Single machine per announcement', //'Single announcement per post',
	'thirty_announcements' => '30 announcements pack',
	'unlimited_announcements' => 'Unlimited announcements',

	'time_defined_user' => 'Time period defined by the user',
	'gallery_unlimited_images' => 'Gallery with unlimited images',
	'detailed_machine_description' => 'Detailed machine description in announcement',
	'one_year_availability' => '1 Year availability',
	'post_an_announcement' => 'Post an announcement',
	'customizable_online_store' => 'Customizable online store',
	'link_to_your_site' => 'Link to your website',
	'post_logo_address' => 'Post your company name, logo, address',

	'plan_basic' => 'Basic',
	'plan_business' => 'Business',
	'plan_plus' => 'Premium',

	'payment_per_announcement' => 'Payment per <br> announcement',
	'per_year' => 'per year',
	'buy_now' => 'Buy Now',
	'best_prices' => 'The best market prices, <br>take advantage and make your machines visible in the Latin American market.',

	'pricing' =>'Our Plans',
	'minimum_price_required' => 'Give us your lowest sale price', //'Minimum price required',
	'minimum_price_text' => 'The minimum price will only be shown to our SELLERS for negotiation purposes.',
	'per_180days' => 'per 180 days',
	'free_banner_bonus' => '1 Banner advertising', //'Banner advertising at expoindustri.com',
	'faq_buyers' => 'FREQUENTLY ASKED QUESTIONS FROM BUYERS',
	'faq_sellers' => 'FREQUENTLY ASKED QUESTIONS FROM ADVERTISERS ',
	'taxes' => 'taxes',
	'product_name' => 'Product name',
	'pay_with_card' => 'Card payment',
	'total' => 'Total',
	'payment_faktura_note' => 'Soon we will send the invoice to your billing address.',
	'announcement' => 'Announcement',
	'announcements' => 'Announcements',
	'edit' => 'Edit',
	'choose_plan_business_plus' => 'Choose your plan <br><b>BUSINESS</b> or <b>PLUS</b>',
	'automatic_translations' => 'Automatic translation into English, Swedish and Spanish',
	'our_price_slogan' => 'With our plans you get a team that works for you',
	'welcome_to' => 'Welcome to',
	'categories' => 'categories',
	'reach_your_clients' => 'With our effective marketing, you can find your customers in Europe and South America', //'Reach thousands of customers in Europe and South America with your machinery, with all the logistics included.',
	'start_selling' => 'Start selling',
	'expo_description' => 'Expoindustri.com is the online marketplace that enables companies to sell their machines / accessories more efficiently. We work hard to increase your sales opportunities in Europe and South America and also give you the best logistics and distribution experience.',
	'our_philosophy' => 'Our philosophy',
	'quote_joakim' => 'A satisfied customer is what drives us to do our best',
	'our_experience' => 'Our experience',
	'euro_south_market' => 'European and South American market',
	'logistics' => 'Logistics',
	'import_export_process' => 'Import / Export process',
	'experienced_team' => 'Experienced international team',
	'find_your_solution' => 'Find the Expoindustri solution for your business',
	'sale_3_languages' => 'Sell in 3 languages',
	'many_currencies' => 'Choose the currency in which you want to sell',
	'sale_overseas' => 'Set your sales price to suit the different markets you want to reach',
	'logistic_distribution' => 'Logistics and distribution',
	'partners' => 'Our partners',
	'just_single_announce' => 'You only have one machine to sell?',
	'our_categories' => 'Our categories',
	'contact_doubts' => 'Do you have any questions about how we can help you?',
	'contact_doubts_body' => 'We help our clients on a daily basis with the sale or purchase of machines, the export and import process, logistical support, technical inspections and more. Call or stop by the office at Jönköpingsvägen 27 in Tenhult, Jönköping. You will find all our contact details below.<br>Welcome!',
	'managing_director' => 'Development manager',
	'expand_your_business' => 'Expand your business',
	'view_explained_video' => 'Watch video',
	'experienced_salesman' => 'Personalized marketing',
	'distrib_office' => 'Distribution office',
	'terms' => 'Terms',
	'where_pick_machine' => '',
	'wish_to_pick_it' => 'Deseo recoger la máquina de almacen',
	'provide_final_destination' => 'Provide destiny location',
	'wanna_make_offer' => 'I wan\'t to make an offer',
	'offer' => 'Offer',
	'wanna_expand_business' => 'Want to expand your business?',
	'please_accept_terms' => 'I accept the Legal Terms and conditions',

	'paysuc_thankyou' => 'Thanks for your order!',
	'paysuc_transtatus' => 'Transaction status',
	'paysuc_succesful' => 'Succesful',
	'paysuc_referenceno' => 'Transaction Reference No.',
	'paysuc_datetime' => 'Transaction Date and Time',
	'paysuc_order' => 'Order',
	'paysuc_payment' => 'Payment Amount',
	'paysuc_detail' => 'Transaction has the following details:',
	'paysuc_needassitance' => 'If you need help, you can contact us by phone at: <b>+46 072 393 3700</b>',
	'factdet_fillyourinfo' => 'Fill out with billing information',
	'factdet_factaddress' => 'Billing address',
	'go_myprofile' => 'Update my profile',
	'go_myaccount' => 'My Account',
	'place' => 'Place',
	'status' => 'Status',
	'acc_serv_sales' => 'We find buyers for your machine!',
	'acc_serv_sales_body' => 'Your machines are published in more than 40 countries around Europe and South America!',
	'acc_serv_sales_body2' => 'With our Sales team we inform proactively.<br>Our global networks reach more than +10 million potential buyers.',
	'acc_serv_sales2' => 'Are you ready for additional sales?',
	'acc_serv_sales2_title' => 'Export ads from your site',
	'acc_serv_sales2_body' => 'We are happy to copy lists from your website for free. The information is updated regularly and automatically.',
	'acc_serv_sales3_title' => 'Do you prefer to advertise individually?',
	'acc_serv_sales3_body' => 'With our system for filling in ads it has never been easier! Start now.',
	'goto_store' => 'Visit Store',
	'see_more_desc' => 'About us', //acerca de empresa
	'hide_more' => 'Hide',
	'write_new_password' => 'Please write your new password',
	'descriptionCompany' => 'Company description',
	'service_requirement' => 'Service requirement',
	'service_requirement_autoimport' => 'It is necessary that one of our consultants contact you to obtain more information, please provide us with the address of your website and any additional questions or concerns in the text box.',
	'transport_sea' => 'Marine transport',
	'transport_terrain' => 'Ground transportation',
	'price_southamerica' => 'Price for South America',
	'price_europa' => 'Price for Europe',
	'exclude_moms' => 'Taxes not included',
	'price_europe_message' => 'Price available for customers in Europe',
	'make_an_offer' => 'Make an offer',
	'welcome_to_landing' => 'We help you win more global business',
	'saved_succesfully' => 'Your changes were saved succesfully',
	'remember_review' => 'Your announcement will be reviewed and enabled very soon',
	'nosotros_saluda_title' => 'Say hello to our colleagues',
	'nosotros_saluda_body' => 'Here you will find the people who make Expoindustri the leading consultant for Sweden and South America in the export / import of machines. We are an experienced and international team that helps your company find the right buyer. We offer personal service in combination with effective marketing that leads to a fast, profitable and secure business. Together, we help solve problems and answer questions about machine export-import, logistics processes, technical inspections and everything your company needs.',
	'cargo_joakim' => 'CEO',
	'cargo_emily' => 'Sales & Business Development Manager',
	'cargo_claudia' => 'CFO – Sweden & Sout America',
	'cargo_carla' => 'Administrative Assistant South America',
	'cargo_ernesto' => 'South America Seller',
	'cargo_iracema' => 'South American Accounting Economist',
	'cargo_jurgen' => 'Head of Warehouse & Logistics',
	'cargo_marco' => 'CTO',
	'cargo_jose' => 'South America Seller',
	'contact_details' => 'Contact details',
	'contact_details_body' => '<b>Email:</b> info@expoindustri.com<br>
<b>Telephone:</b> 072 393 37 00<br>
<br>
<b>EXPOINDUSTRI SWEDEN AB</b><br>
<b>Org. No:</b> 559174-5194',
	'find_here' => 'Find here',
	'hours_available' => 'Opening Hours',
	'hours_list' => '<b>Telephone</b><br>
Mon - Fri: 08:30 - 17:00<br>
<b>Chat</b><br>
Mon - Fri: 8am - 6pm<br>
<b>Office</b><br>
Mon - Thu: 08:30 - 16:00',
	'wanna_contact_us' => 'Want to contact us? Fill in the following form and we will respond as soon as possible.',
	'contact_disclaimer' => 'By submitting your personal information, you agree that we process it in accordance with our :link.',
	'privacy_politics' => 'privacy policy',
	'date' => 'Date',
	'interested' => 'Interested',
	'zone' => 'Zone',
	'newsletter_description' => 'ARE YOU INTERESTED IN GETTING UPDATES ABOUT THE PROJECTS, INDUSTRIES & MACHINES THAT WE REQUEST IN EUROPE & SOUTH AMERICA? - SUBSCRIBE TO OUR NEWSLETTER!',
	'business_opportunity_southamerica' => 'Business opportunities in South America',
	'have_stock_machine' => 'Do you have a stock of machines that you want to sell in South America?',
	'machine_storage' => 'Machine storage',
	'send_offer' => 'Send us your offer',
	'request_final_price' => 'Request price at final destination',
	'request_final_price_body' => 'A seller will send you your offer within 24 hours.',
	'offer_body' => 'Make an offer and a seller will get in touch.',
	'need_more_info' => 'Do you want more information?',
	'need_more_info_body' => 'Contact us directly. Open 6 days a week. We speak your language!',
	'request' => 'Request',
	'view_all' => 'View all',
	'name' => 'Name',
	'summary' => 'Summary',
	'price_at_origin' => 'Price at origin: (does not include transport)',
	'price_fixed_by_consultant' => 'Price set by the consultant',
	'tags' => 'Tags',
	'machines' => 'Machines',
	'accesories' => 'Accesories',
	'both' => 'Both',
	'machines_accesories' => 'Machines or accessories?',
	'used_new' => 'New or used?',
	'new' => 'New',
	'used' => 'Used',
	'type' => 'Type',
	'view_more_details' => 'View more details',
	'units' => 'Units',
	'write_to_us' => 'Write to us',
	'whatsapp_more_info' => 'I would like more information about this machine',
	'multiple_prices' => 'Multiple Prices',
	'attachments' => 'Attachments',
	'expo_description_sales' => 'A lo largo de los años, hemos ayudado a nuestros clientes de habla hispana, a aumentar su productividad, a través de la búsqueda inteligente en Europa, de maquinaria nueva o usada, para entregársela directamente en el destino que se nos indique, sin que esto signifique ningún tiempo, ni esfuerzo extra para el cliente, pero que SI es sinónimo de optimización, calidad, mejor tecnología y precio competitivo.',
	'quote_joakim_sales' => 'Expoindustri es un jugador más en su equipo de trabajo. Existimos para negociar y gestionar tiempos, calidad y precios, a favor de su empresa',
];
