<?php

return [

	'click_to_select' => 'Click to select',
	'choose_year' => 'Choose year',

	'status_new' => 'New',
	'status_operational' => 'Operational',
	'status_operationalWFaults' => 'Operational with faults',
	'status_notOperational' => 'Not Operational',
	'status_iDontKnow' => 'I dont know',

	// Label translations
	// -----------------------------

	'precio' => 'Price',

	'calificacion_general_del_vendedor' => 'General Seller Rating',
	'dimensiones_y_tamanos' => 'Dimensions and Size',
	'potencia_y_rendimiento' => 'Power and Performance',
//	'chasis' => 'Chassis',
	'otra_informacion' => 'Additional information',
//	'pais_de_fabricacion' => 'Production Country',

	'unknown_year' => 'Unknown year',
	'informacion_primaria' => 'Basic information',
	'ano_de_fabricacion' => 'Year of Production',
	'kilometraje' => 'Mileage',
	'estado' => 'Condition',
	'potencia_del_motor' => 'Engine Power',
	'configuracion_de_eje' => 'Axle Configuration', // !REVIEW
	'transmision' => 'Transmission',
	'informacion_adicional' => 'Additional Information',
	'calificacion_general' => 'General Rating',
	'ultima_revision_tecnica' => 'Latest Inspection',
	'no_de_propietarios_anteriores' => 'Number of Previous Owners',
	'color' => 'Color',
	'volante' => 'Steering wheel',
	'cabina' => 'Cabin',
	'condicion_llantas_delanteras' => 'Front Tires Condition (%)',
	'condicion_llantas_traseras' => 'Rear Tires Condition (%)',
	'medida_de_transporte_largo_x_ancho_x_alto' => 'Overall Dimensions for Transportation (Length x Width x Height)',
	'peso_bruto' => 'Gross weight', // !REVIEW
	'capacidad_de_carga' => 'Loading Capacity',
	'tipo_de_frenos' => 'Brake Types',
	'marca_de_los_neumaticos' => 'Tyre Brand',
	'volumen_del_tanque' => 'Fuel Tank Volume',
	'dimensiones_internas_largo_x_ancho_x_alto' => 'Internal Dimensions (Length x Width x Height)',
	'equipamiento_extra' => 'Additional Equipment',
	'numero_de_serie' => 'Serial Number',
	'motor_ambiental_emision' => 'Emission Class',
	'tipo_motor' => 'Engine Type',
	'cilindrada_del_motor' => 'Engine Displacement',
	'salida_de_torsion' => 'Engine Torque',
	'tipo_de_suspension' => 'Suspension Types',
	'distancia_entre_ejes' => 'Distance Between Axis',
	'no_de_chasis' => 'Chassis Number',
	'equipamiento' => 'Equipment',
	'tipo_de_tracto_camion' => 'Tractor Truck Type',
	'tornamesa_quinta_rueda' => 'Fifth Wheel Type',
	'tipo_de_carroceria' => 'Bodywork Type',
	'tipo_de_eje' => 'Axle Type',
	'no_de_serie' => 'Serie Number',
	'horas_de_trabajo' => 'Worked Hours',
	'accesorios' => 'Accessories',
	'pintura_original' => 'Original Paintwork.',
	'certificacion_ce' => 'CE-Certified ',
	'pais_de_fabricacion' => 'Production Country',
	'condicion_oruga_izquierda' => 'Left Track Condition (%)',
	'condicion_oruga_derecha' => 'Right Track Condition (%)',
	'capacidad_balde' => 'Bucket Capacity',
	'ancho_de_trabajo' => 'Working Width',
	'ancho_de_banda' => 'Bandwidth',
	'max_capacidad_de_elevacion' => 'Max. Lifting Capacity',
	'max_capacidad_de_empuje' => 'Max. Thrust Capacity',
	'motor' => 'Engine',
	'velocidad_maxima' => 'Max. Speed',
	'lubricacion_central' => 'Central Lubrication',
	'flujo_del_hidraulico' => 'Hydraulic Flow',
	'ancho_de_carril' => 'Lane Width',
	'longitud_de_palanca_y_draga' => 'Dipper/Stick Lenght',
	'tipo_de_cuchillas' => 'Blades Types',
	'tipo_de_compactadora' => 'Compactor Type',
	'condicion_del_tambor' => 'Drum Condition',
	'condicion_de_la_placa' => 'Stick Condition',
	'ancho_del_tambor' => 'Drum Width',
	'peso_de_compactacion' => 'Compacting Force',
	'tipo_de_poder' => 'Power',
	'tipo_de_traccion' => 'Drive Type',
	'maximos_ocupantes' => 'Max. Nr. Occupants',
	'altura_maxima_de_trabajo' => 'Max. Working Height',
	'largo_extension_plataforma' => '',
	'capacidad_de_levante' => 'Lifting Capacity',
	'maxima_pendiente_de_traslado' => '',
	'velocidad_de_desplazamiento' => 'Travel Speed',
	'velocidad_de_subida_y_bajada' => 'Rising/Lowering Speed',
	'apertura_de_alimentacion' => 'Feeding Hopper Dimensions',
	'tipo_de_trituradora' => 'Crusher Type',
	'volumen_de_produccion' => 'Production Volume',
	'nivel_de_funcionamiento' => 'Performance Level',
	'peso' => 'weight',
	'tamano_del_material' => 'Material Seize',
	'velocidad' => 'Speed',
	'potencia' => 'Power',
	'tipo_de_combustion' => 'Combustion Type',
	'chasis' => 'Chassis',
	'cilindros' => 'Cylinders',
	'frecuencia' => 'Frecuency',
	'voltaje' => 'Voltage',
	'tipo_de_compresor' => 'Compressor Type',
	'caudal' => 'Flow',
	'marca_del_motor' => 'Engine Brand',
	'presion_de_trabajo' => 'Working Pressure',
	'traccion' => 'Traction',
	'tamano_de_llantas_delanteras' => 'Front Tires Size',
	'tamano_de_llantas_traseras' => 'Rear Tires Size',
	'toma_fuerza' => 'Power Take Off',
	'anchura_de_trabajo' => 'Working Width',
	'configuracion_de_manejo' => 'Operating Configuration',
	'carro_de_la_horquilla' => 'Forklift Forks Functions',
	'altura_maxima_de_elevacion' => 'Max. Lifting Height',
	'longitud_de_unas' => 'Forks Lengt',
	'punto_de_carga' => 'Load center',
	'mastil_de_elevacion' => 'Lifting Mast',
	'tipo_de_neumatico' => 'Tyre Type',
	'marca_de_la_bateria' => 'Battery Brand',
	'voltaje_de_la_bateria' => 'Battery Voltage',
	'voltaje_del_cargador' => 'Charger Voltage',
	'capacidad_de_la_bateria' => 'Battery Capacity',
	'capacidad_max_de_levante' => 'Lifting Capacity',
	'altura_de_elevacion' => 'Lifting Height',
	'longitud_de_pluma_principal' => 'Main Boom Length',
	'extension_de_pluma_principal' => 'Main Boom Extension',
	'longitud_maxima_del_sistema' => 'Max. Boom Length',
	'contrapeso_maximo' => 'Max. Counterweight',
	'longitud_del_portador' => 'Crane Carrier Length',
	'maxima_pendiente' => 'Max. Slope',
	'capacidad' => 'Capacity',
	'giro_de_cabina' => 'Cabin Slewing',
	'tipo' => 'Type',
	'entre_punto' => 'Double distance',
	'volteo' => 'Swing over the bed',
	'husillo' => 'Spindle',
	'bomba_de_refrigeracion' => 'Coolant Pump',
	'volteo_con_escote' => 'Swing over slide',
	'volteo_sin_escote' => 'Swing in gap',
	'ancho_de_la_cama' => 'Width of the bed',
	'movimiento_cruz' => 'Cross Movement',
	'marca_del_visualizador' => 'Display Mark',
	'caja' => 'Casing',
	'velocidad_de_husillo' => 'Spindle Speed',
	'alimentacion' => 'Feeding',
	'iso' => 'ISO',
	'movimiento_longitud_x' => 'Longitudinal Movement X',
	'movimiento_transversal_y' => 'Transversal Movement Y',
	'movimiento_vertical_z' => 'Vertical Movement Z',
	'distancia_husillo_mesa' => 'Spindle-Table Distance ',
	'cambiador_de_herramientas' => 'Tool Changer',
	'sistema_de_programacion' => 'Programation System',
	'velocidad_del_husillo' => 'Spindle Speed',
	'alimentacion_x_y_z' => 'Feeding X,Y,Z',
	'marcha_rapida' => 'Fast Start-Up',
	'tipo_de_alimentacion' => 'Feeding Type',
	'tipo_de_cilindradora' => 'Roll Bending Type',
	'capacidad_max_esposor' => 'Max. Thickness',
	'diametro_rodillo_superior' => 'Upper Roller Diameter',
	'diametro_rodillo_inferior' => 'Lower Roller Diameter',
	'potencia_de_motor' => 'Engine Power',
	'flujo_hidraulico' => 'Hydraulic Flow',
	'velocidad_de_laminacion' => 'Laminating Speed ',
	'capacidad_max_de_plegado' => 'Max Bending Capacity',
	'distancia_del_golpe' => 'Hitting Distance',
	'distancia_entre_barras' => 'Distance Between Bars',
	'max_altura_de_apertura' => 'Max. Opening Height',
	'volumen_tanque_hidraulico' => 'Hydraulic Tank Volume',
	'capacidad_en_toneladas' => 'Ton Capacity',
	'industria' => 'Industry',
	'longitud_de_trabajo' => 'Working Length',
	'max_corte' => 'Max. Cutting Capacity',
	'flujo_de_hidraulico' => 'Hydraulic Flow In:',
	'flujo_de_hidraulico_en' => 'Hydraulic Flow In:',
	'dimensiones_mesa_lxa' => 'Bord Measurements (LxA)',
	'cono_del_husillo' => 'Spindel Cone',
	'movimiento_del_husillo' => 'Spindle Movement',
	'capacidad_maxima_de_perforacion' => 'Drilling Capacity',
	'distancia_husillo_columna' => 'Spindle-Column Distance',
	'amperaje' => 'Amperage',
	'velocidad_de_alimentacion' => 'Feeding Speed',
	'otros' => 'Others',
	'otros1' => 'Others',
	'otros2' => 'Others',
	'otros3' => 'Others',
	'otros4' => 'Others',
	'otros5' => 'Others',
	'volteo_transversal' => '',
	'reduccion_de_cubo' => '',
	'avance_rapido_de_carro' => 'Fast feed',

	// Configuracion del Eje
	// -----------------------------

	'axis_4X2' => '4X2',
	'axis_4X4' => '4X4',
	'axis_4X4*2' => '4X4*2',
	'axis_4X4*4' => '4X4*4',
	'axis_6X2' => '6X2',
	'axis_6X2*4' => '6X2*4',
	'axis_6X2/4' => '6X2/4',
	'axis_6X4' => '6X4',
	'axis_6X6' => '6X6',
	'axis_6X6*2' => '6X6*2',
	'axis_8X2' => '8X2',
	'axis_8X2/4' => '8X2/4',
	'axis_8X2*4' => '8X2*4',
	'axis_8X2/6' => '8X2/6',
	'axis_8X2*6' => '8X2*6',
	'axis_8X4' => '8X4',
	'axis_8X4/4' => '8X4/4',
	'axis_8X4*4' => '8X4*4',
	'axis_8X6' => '8X6',
	'axis_8X6/4' => '8X6/4',
	'axis_8X8' => '8X8',
	'axis_8X8/4' => '8X8/4',
	'axis_10X2' => '10X2',
	'axis_10X4' => '10X4',
	'axis_10X6' => '10X6',
	'axis_10X6/4' => '10X6/4',
	'axis_10X8' => '10X8',
	'axis_10X10' => '10X10',

	// Calificacion General
	// -----------------------------
	
	'grating_veryGood' => 'Very Good',
	'grating_good' => 'Good',
	'grating_regular' => 'Regular',
	'grating_bad' => 'Bad',
	
	'no_previous_owners' => 'Number of previous owners',
	'colour' => 'Colour',

	// Volante
	// -----------------------------

	'steering_wheel' => 'Steerig wheel',
	'steering_left' => 'Left',
	'steering_right' => 'Right',

	// Cabina
	// -----------------------------
	
	'cabin' => 'Cabin',
	'cabin_dayCab' => 'Day Cab',
	'cabin_sleeperCab' => 'Sleeper Cab',
	'cabin_highSleeperCab' => 'High Sleeper Cab',
	'cabin_crewCab' => 'Crew Cab',
	'cabin_extendedCab' => 'Extended Cab',
	
	'transport_measure' => 'Transportation measures (length x width x height)',
	'weight_brute' => 'Weight Brute',
	'load_capacity' => 'Load Capacity',

	// Tipo de frenos CAMIONES
	// -----------------------------
	
	'break_types' => 'Break types',

	'tpBreakCam_dontKnow' => "Don't know",
	'tpBreakCam_disk' => "Disc brakes",
	'tpBreakCam_drum' => "Drum brakes",
	'tpBreakCam_other' => "Other",

	// Marca de las llantas
	// -----------------------------
	
	'tires_brand' => 'Tires brand',

	'tiresBrand_michelin' => 'Michelin',
	'tiresBrand_goodyear' => 'Goodyear',
	'tiresBrand_continental' => 'Continental',
	'tiresBrand_bridgestone' => 'Bridgestone',
	'tiresBrand_pirelli' => 'Pirelli',
	'tiresBrand_nokian' => 'Nokian',
	'tiresBrand_blacklion' => 'Blacklion',
	'tiresBrand_hankook' => 'Hankook',
	'tiresBrand_kumho Tires' => 'Kumho Tires',
	'tiresBrand_dunlop' => 'Dunlop',
	'tiresBrand_bFGoodrich' => 'BFGoodrich',
	'tiresBrand_yokohama' => 'Yokohama',
	'tiresBrand_others' => 'Others',
	
	'fueltank_volume' => 'Fuel tank volume',
	
	'internal_measure' => 'Internal measures (Length, width, height)',
	
	'tandem_axis_lifting' => 'Tandem axis lifting',
	'levantamiento_del_eje_tandem' => 'Tandem axis lifting',

	// Equipamiento extra CAMIONES
	// --------------------------------

	'extra_equipment' => 'Extra equipment',

	'xtraEquCam_CoffeeMaker' => 'Coffee maker',
	'xtraEquCam_refrigerator' => 'Refrigerator',
	'xtraEquCam_toolbox' => 'Toolbox',
	'xtraEquCam_warningLamp' => 'Warning lamp',
	'xtraEquCam_workLampOutside' => 'Work lamp outside',
	'xtraEquCam_foglamps' => 'Foglamps',
	'xtraEquCam_outerVisor' => 'Outer visor',
	'xtraEquCam_bumperSpoiler' => 'Bumper spoiler',
	'xtraEquCam_frontsBumpers' => 'Fronts bumpers',
	'xtraEquCam_lowerLightBar' => 'Lower light bar',
	'xtraEquCam_headlightProtector' => 'Headlight protector',
	'xtraEquCam_mosquitoNetRadiator' => 'Mosquito net radiator',
	'xtraEquCam_aluminumRims' => 'Aluminum rims',
	'xtraEquCam_mudflap' => 'Mudflap',
	'xtraEquCam_fenderFlap' => 'Fender flap',
	'xtraEquCam_extinguishers' => 'Extinguishers',
	'xtraEquCam_other' => 'Other',
	
	'serial_no' => 'Serial number',
	'ambiental_engine' => '',

	// Tornamesa TRACTO CAMION
	// --------------------------

	'tornaCam_fixed' => 'Fixed',
	'tornaCam_adjustable' => 'Adjustable',

	// Tipo de TRACTO CAMION
	// ----------------------

	'tpTractoCam_standardTractorTrailer' => 'Standard tractor/trailer',
	'tpTractoCam_heavyLoad' => 'Heavy load',
	'tpTractoCam_hazardousLoad' => 'Hazardous load',
	'tpTractoCam_volumeTrailer' => 'Volume trailer',
	'tpTractoCam_other' => 'Other',

	// Tipo de carroceria OTROS
	// --------------------------

	'tpCarr_euro' => 'Euro',
	'tpCarr_jumbo' => 'Jumbo',
	'tpCarr_mega' => 'Mega',
	'tpCarr_extendible' => 'Extendible',
	'tpCarr_fixed' => 'Fixed',
	'tpCarr_other' => 'Other',

	// Tipo de eje OTROS
	// -----------------------

	'tpEje_1' => '1',
	'tpEje_1+1' => '1+1',
	'tpEje_1+2' => '1+2',
	'tpEje_1+3' => '1+3',
	'tpEje_2' => '2',
	'tpEje_2+1' => '2+1',
	'tpEje_2+2' => '2+2',
	'tpEje_2+3' => '2+3',
	'tpEje_3' => '3',
	'tpEje_4' => '4',

	// Motor Ambiental
	// ------------------------

	'motAmb_euro_0' => 'Euro 0',
	'motAmb_euro_1' => 'Euro 1',
	'motAmb_euro_2' => 'Euro 2',
	'motAmb_euro_3' => 'Euro 3',
	'motAmb_euro_4' => 'Euro 4',
	'motAmb_euro_5' => 'Euro 5',
	'motAmb_euro_6' => 'Euro 6',
	'motAmb_other' => 'Other',

	// Tipo de Suspension
	// -------------------------

	'tpSusp_parabolicParabolic' => 'Parabolic-Parabolic',
	'tpSusp_parabolicLeaf' => 'Parabolic-Leaf',
	'tpSusp_parabolicAir' => 'Parabolic-Air',
	'tpSusp_leafParabolic' => 'Leaf-Parabolic',
	'tpSusp_leafLeaf' => 'Leaf-Leaf',
	'tpSusp_leafAir' => 'Leaf-Air',
	'tpSusp_airAir' => 'Air-Air',
	'tpSusp_trapezoid' => 'Trapezoid',

	// Accesorios PALAS CARGADORAS
	// ----------------------------

	'accePalas_Forks' => 'Forks',
	'accePalas_Mudguards' => 'Mudguards',
	'accePalas_QuickCoupler' => 'Quick coupler',
	'accePalas_3rdHydraulicCircuit' => '3rd hydraulic circuit',
	'accePalas_4thHydraulicCircuit' => '4th hydraulic circuit',
	'accePalas_AutoLubricationSystem' => 'Auto lubrication system',
	'accePalas_Weighloader' => 'Weighloader',
	'accePalas_MainSwitch' => 'Main switch',
	'accePalas_WarningLight' => 'Warning light',
	'accePalas_DieselHeater' => 'Diesel heater',
	'accePalas_JoystickKontrol' => 'Joystick kontrol',
	'accePalas_AirConditioning' => 'Air conditioning',
	'accePalas_ComfortDriveControlCDC' => 'Comfort drive control CDC',
	'accePalas_AdditionalHydraulics' => 'Additional hydraulics',
	'accePalas_MachineControlSystem' => 'Machine control system',
	'accePalas_Aps' => 'Aps',

	// ACCESORIOS ORUGA
	// ------------------------

	'acceOruga_EngineHeater' => 'Engine heater',
	'acceOruga_Winch' => 'Winch',
	'acceOruga_AirConditioning' => 'Air conditioning',
	'acceOruga_AdditionalHydraulics' => 'Additional hydraulics',
	'acceOruga_MultiShankRipper' => 'Multi shank ripper',
	'acceOruga_ShankRipper' => 'Shank ripper',
	'acceOruga_MainSwitch' => 'Main switch',
	'acceOruga_WarningLight' => 'Warning light',

	// Accesorios EXCAVADORAS
	// -----------------------------

	'acceExcav_HammerHydraulic' => 'Hammer hydraulic ',
	'acceExcav_ShearHydraulic' => 'Shear hydraulic',
	'acceExcav_QuickCoupler' => 'Quick coupler',
	'acceExcav_LineValveDipperStick' => 'Line valve dipper/stick',
	'acceExcav_LineValveBoomHoist' => 'Line valve boom/hoist',
	'acceExcav_AutoLubricationSystem' => 'Auto lubrication system',
	'acceExcav_Rotator' => 'Rotator',
	'acceExcav_MainSwitch' => 'Main switch',
	'acceExcav_WarningLight' => 'Warning light',
	'acceExcav_DieselHeater' => 'Diesel heater',
	'acceExcav_JoystickKontrol' => 'Joystick kontrol',
	'acceExcav_AirConditioning' => 'Air conditioning',
	'acceExcav_ExtraEquipment' => 'Extra equipment',
	'acceExcav_AdditionalHydraulics' => 'Additional hydraulics',
	'acceExcav_MachineControlSystem' => 'Machine control system',
	'acceExcav_DepthControl' => 'Depth control',
	'acceExcav_AdjustableBoom' => 'Adjustable boom',
	'acceExcav_SteelTracks' => 'Steel tracks',
	'acceExcav_Weighloader' => 'Weighloader',
	'acceExcav_EstándarDiggingBucket' => 'Estándar digging bucket',
	'acceExcav_CableBucket' => 'Cable bucket',
	'acceExcav_DitchingGradingBucket' => 'Ditching/grading bucket',
	'acceExcav_VDitchBucket' => 'V ditch bucket',
	'acceExcav_RubberTracks' => 'Rubber tracks',

	// MOTOR AMBIENTAL EXCAVADORA |ORUGA|
	// -----------------------------------------

	'motorAmbExc_IDK' => 'I dont know',
	'motorAmbExc_Stage_2' => 'Stage | | ',
	'motorAmbExc_Stage_3a' => 'Stage | | | A',
	'motorAmbExc_Stage_3b' => 'Stage | | | B',
	'motorAmbExc_Stage_4' => 'Stage | V',
	'motorAmbExc_Stage_5' => 'Stage V',

	// CABINA |PALA CARGADORA |ORUGA|
	// ----------------------------------

	'cabina_palaOr_OpenCab' => 'Open cab',
	'cabina_palaOr_EnclosedCab' => 'Enclosed cab',

	// Tipo de cuchillas | ORUGA
	// ---------------------------

	'tpCuchillaOru_IDK' => 'I dont know',
	'tpCuchillaOru_SemiUBlade' => 'Semi u-blade',
	'tpCuchillaOru_UBlade' => 'U-blade',
	'tpCuchillaOru_6WayBlade' => '6 way blade',
	'tpCuchillaOru_StraightBlade' => 'Straight blade',

	// Accesorios para COMPACTADORA
	// ------------------------------

	'acceCompac_mainSwitch' => 'Main switch',
	'acceCompac_warningLight' => 'Warning light',
	'acceCompac_extraEquipment' => 'Extra equipment',
	'acceCompac_dieselHeater' => 'Diesel heater',
	'acceCompac_airConditioning' => 'Air conditioning',
	'acceCompac_additionalHydraulics' => 'Additional hydraulics',

	// Tipo de  COMPACTADORA
	// ------------------------

	'tpCompact_SingleDrumRollers' => 'Single drum rollers',
	'tpCompact_TwinDrumRollers' => 'Twin drum rollers',
	'tpCompact_CombiRollers' => 'Combi rollers',
	'tpCompact_PneumaticTiredRollers' => 'Pneumatic tired rollers',
	'tpCompact_TowedVibratoryRollers' => 'Towed vibratory rollers',
	'tpCompact_OtherRollers' => 'Other rollers',
	'tpCompact_SoilCompactors' => 'Soil compactors',
	'tpCompact_PlateCompactors' => 'Plate compactors',
	'tpCompact_Tampers' => 'Tampers',
	'tpCompact_Other' => 'Other',

	// Tipo de traccion ALZA HOMBRES
	// ----------------------------

	'tpTracAlza_2WD' => '2 WD',
	'tpTracAlza_4WD' => '4 WD',
	'tpTracAlza_Tracked' => 'Tracked',
	'tpTracAlza_Other' => 'Other',

	// Tipo de poder ALZA HOMBRES
	// -----------------------------

	'tpPoderAlza_Electric' => 'Electric',
	'tpPoderAlza_Diesel' => 'Diesel',
	'tpPoderAlza_LiquidGas' => 'Liquid gas',
	'tpPoderAlza_Gas' => 'Gas',
	'tpPoderAlza_Hybrid' => 'Hybrid',

	'maxPend_5' => '5°',
	'maxPend_10' => '10°',
	'maxPend_15' => '15°',
	'maxPend_20' => '20°',
	'maxPend_25' => '25°',
	'maxPend_30' => '30°',
	'maxPend_35' => '35°',
	'maxPend_40' => '40°',
	'maxPend_45' => '45°',
	'maxPend_50' => '50°',
	'maxPend_55' => '55°',
	'maxPend_60' => '60°',
	'maxPend_65' => '65°',
	'maxPend_70' => '70°',
	'maxPend_75' => '75°',
	'maxPend_80' => '80°',
	'maxPend_85' => '85°',
	'maxPend_90' => '90°',

	// Tipo de trituradoras
	// ---------------------

	'tpTritur_JawCrusher' => 'Jaw crusher',
	'tpTritur_ConeCrusher' => 'Cone crusher',
	'tpTritur_BallCrusher' => 'Ball crusher',
	'tpTritur_ImpactCrusher' => 'Impact crusher',
	'tpTritur_RodCrusher' => 'Rod crusher',
	'tpTritur_GyratoryCrusher' => 'Gyratory crusher',
	'tpTritur_RollCrusher' => 'Roll crusher',
	'tpTritur_ShredderCrusher' => 'Shredder crusher',
	'tpTritur_HammerCrusher' => 'Hammer crusher',

	// Nivel de fincionamiento TRITURADORAS
	// ------------------------------------

	'nvFunTrit_Primary' => 'Primary',
	'nvFunTrit_Secondary' => 'Secondary',
	'nvFunTrit_Tertiary' => 'Tertiary',
	'nvFunTrit_Quaternary' => 'Quaternary',
	'nvFunTrit_Other' => 'Other',

	// Tipo de combustion GENERADOR
	// ------------------------------

	'tpCombus_Diesel' => 'Diesel',
	'tpCombus_LiquidGas' => 'Liquid gas',
	'tpCombus_Electric' => 'Electric',

	// Caudal compresor
	// --------------------

	'caudComp_m3Min' => 'm3/min',
	'caudComp_lMin' => 'l/min',
	'caudComp_lSec' => 'l/sec',

// TIPO COMPRESOR
// ---------------------

	'tpCompres_PistonCompressor' => 'Piston compressor',
	'tpCompres_ScrewCompressor' => 'Screw compressor',
	'tpCompres_CentrifugeCompressor' => 'Centrifuge compressor',
	'tpCompres_Others' => 'Others',

// CHASIS COMPRESOR
// ---------------------

	'chasisCompr_Static' => 'Static',
	'chasisCompr_Mobile' => 'Mobile',

// Cilindros TRACTOR
// -------------------

	'cilind_Trac_2st' => '2 st',
	'cilind_Trac_3st' => '3 st',
	'cilind_Trac_4st' => '4 st',
	'cilind_Trac_5st' => '5 st',
	'cilind_Trac_6st' => '6 st',
	'cilind_Trac_8st' => '8 st',

// toma fuerza PTO
// ---------------

	'tomaPower_540' => '540',
	'tomaPower_750' => '750',
	'tomaPower_1000' => '1000',
	'tomaPower_540_750' => '540/750',
	'tomaPower_540_540e' => '540/540e',
	'tomaPower_540_1000' => '540/1000',
	'tomaPower_540_750_1000' => '540/750/1000',
	'tomaPower_540_540e_1000' => '540/540e/1000',
	'tomaPower_540_750e_1000' => '540/750e/1000',
	'tomaPower_540_1000_1000e' => '540/1000/1000e',
	'tomaPower_540_750_1000_1400' => '540/750/1000/1400',
	'tomaPower_540_540e_1000_1000e' => '540/540e/1000/1000e',

// Accesorios TRACTOR
// ------------------------

	'acceTrac_NoAccessories' => 'No accessories',
	'acceTrac_FrontLoader' => 'Front loader',
	'acceTrac_FrontPTO' => 'Front PTO',
	'acceTrac_FrontLinkage' => 'Front linkage',
	'acceTrac_ClutchlessForwardReverseShuttle' => 'Clutchless forward-reverse shuttle',
	'acceTrac_TransmissionCVT' => 'Transmission CVT',
	'acceTrac_TurbineClutch' => 'Turbine clutch',
	'acceTrac_ReverseDriveSystem' => 'Reverse drive system',
	'acceTrac_FrontAxleSuspension' => 'Front axle suspension',
	'acceTrac_AirConditioning' => 'Air conditioning',
	'acceTrac_ForestCab' => 'Forest cab',
	'acceTrac_CabinSuspension' => 'Cabin suspension',
	'acceTrac_GuidanceSystem' => 'Guidance system',
	'acceTrac_AdBlue' => 'Ad blue',
	'acceTrac_AirBrakes' => 'Air brakes',
	'acceTrac_CreeperGear' => 'Creeper Gear',

// Cabina TRACTOR
// ------------------

	'cabinTrac_Open' => 'Open',
	'cabinTrac_Forest' => 'Forest',
	'cabinTrac_Regular' => 'Regular',
	'cabinTrac_Other' => 'Other',

// Accesorios SEGADORAS
// -------------------

	'acceSega_NoAccessories' => 'No accessories',
	'acceSega_AirConditioning' => 'Air conditioning',
	'acceSega_GPS' => 'GPS',
	'acceSega_CornHead' => 'Corn head',
	'acceSega_GrainHead' => 'Grain head',
	'acceSega_HeaderTrailer' => 'Header trailer',
	'acceSega_SideKnife' => 'Side knife',
	'acceSega_CropLifters' => 'Crop lifters',
	'acceSega_CutterbarAutomaticLeveling' => 'Cutterbar automatic leveling',
	'acceSega_AssistedSteeringSystem' => 'Assisted steering system',
	'acceSega_StrawChopper' => 'Straw chopper',
	'acceSega_ChaffSpreader' => 'Chaff spreader',
	'acceSega_ElectricallyAdjustableSieves' => 'Electrically adjustable sieves',
	'acceSega_YieldAndMoistureMeasuring' => 'Yield and moisture measuring',


// Configuracion de transmision ENFARFADORA
// --------------------------------------

	'cnfTransEnfar_Trailed' => 'Trailed',
	'cnfTransEnfar_Mounted' => 'Mounted',

// Configuracion de manejo MONTACARGA
// -------------------------------

	'cnfManMont_Electric' => 'Electric',
	'cnfManMont_Diesel' => 'Diesel',
	'cnfManMont_Gas' => 'Gas',
	'cnfManMont_LiquidGas' => 'Liquid gas',
	'cnfManMont_Hybrid' => 'Hybrid (Dual fuel)',
	'cnfManMont_Manual' => 'Manual',
	'cnfManMont_Other' => 'Other',

// Mastil de elevacion MONTAGARGA
// -------------------------------
	'mastElevMont_Simplex' => 'Simplex',
	'mastElevMont_Duplex' => 'Duplex',
	'mastElevMont_Triplex' => 'Triplex',
	'mastElevMont_Quad' => 'Quad',
	'mastElevMont_DuplexFreeLift' => 'Duplex/free lift',
	'mastElevMont_TriplexFreeLift' => 'Triplex/free lift',
	'mastElevMont_Telescopic' => 'Telescopic',
	'mastElevMont_Other' => 'Other',



// Carro de la horquilla MONTACARGA
// ------------------------------

	'carrMont_NoCarriege' => 'No carriege',
	'carrMont_Sideshifter' => 'Sideshifter',
	'carrMont_PositionerSpread' => 'Positioner and spread',
	'carrMont_PositionerSpreadTilt' => 'Positioner,spread,and tilt',
	'carrMont_ForkSpreading' => 'Fork spreading',
	'carrMont_DemountableSystem' => 'Demountable system',
	'carrMont_MultiPalletCarriege' => 'Multi pallet carriege',
	'carrMont_Clamp' => 'Clamp',
	'carrMont_RollClamp' => 'Roll clamp',
	'carrMont_PaperRollClamp' => 'Paper roll clamp',
	'carrMont_Rotator' => 'Rotator',
	'carrMont_PusherForks' => 'Pusher forks',
	'carrMont_ReachForks' => 'Reach forks',
	'carrMont_Other' => 'Other',



// Tipo de llantas MONTACARGA
// --------------------------

	'tpLlantaMont_Pneumatic' => 'Pneumatic',
	'tpLlantaMont_SolidPneumatic' => 'Solid-pneumatic',
	'tpLlantaMont_SuperElastic' => 'Super elastic',
	'tpLlantaMont_Other' => 'Other',


// Cabina MONTACARGA
// -------------------------

	'cabMont_OverheadGuard' => 'Overhead guard',
	'cabMont_ClosedCabin' => 'Closed cabin',
	'cabMont_NoCabin' => 'No cabin',


// Tipo de carroceria GRUA
// -----------------------

	'tpCarGrua_Tracked' => 'Tracked',
	'tpCarGrua_Wheeled' => 'Wheeled',

// Tipo  TORNOS
// -----------------
	'tpTorno_Tracked' => 'Tracked',
	'tpTorno_CopyingLathe' => 'Copying lathe',
	'tpTorno_TurretLathe' => 'Turret lathe',
	'tpTorno_VerticalLathe' => 'Vertical lathe',
	'tpTorno_AutomaticLathe' => 'Automatic lathe',
	'tpTorno_CarouselLathe' => 'Carousel lathe',
	'tpTorno_CNCLathe' => 'CNC lathe',
	'tpTorno_PlanLathe' => 'Plan lathe',
	'tpTorno_Other' => 'Other',


// Accesorios para TORNO
// -------------------------

	'acceTorno_WithoutAccesories' => 'Without accesories',
	'acceTorno_3JawChuck' => '3 jaw chuck',
	'acceTorno_4JjawChuck' => '4 jaw chuck',
	'acceTorno_FlatChuck' => 'Flat chuck',
	'acceTorno_FixedSteady' => 'Fixed steady',
	'acceTorno_TravellingSteady' => 'Travelling steady',
	'acceTorno_FastRevolvingCenter' => 'Fast/ revolving center ',
	'acceTorno_DrillChuck' => 'Drill chuck',
	'acceTorno_QuickChangeTool ' => 'Quick change tool ',
	'acceTorno_VeeHolders' => 'Vee holders',
	'acceTorno_Other' => 'Other',



// Marca del visualizador TORNO
// --------------------------
	'branVisTorno_WithoutDigital' => 'Without digital',
	'branVisTorno_Heidenhain' => 'Heidenhain',
	'branVisTorno_Mitutoyo' => 'Mitutoyo',
	'branVisTorno_Sony' => 'Sony',
	'branVisTorno_Easson' => 'Easson',
	'branVisTorno_Delos' => 'Delos',
	'branVisTorno_Other' => 'Other',


// Tipo FRESADORA
// --------------------
	'tpFresa_BenchTypeMillingMachine' => 'Bench-type milling machine',
	'tpFresa_UniversalMillingMachine' => 'Universal milling machine',
	'tpFresa_VerticalMillingMachine' => 'Vertical milling machine',
	'tpFresa_HorisontalMillingMachine' => 'Horisontal milling machine',
	'tpFresa_BedMillingMachine' => 'Bed milling machine',
	'tpFresa_CNCMillingMachine' => 'CNC-milling machine',
	'tpFresa_CNCBedMillingMachine' => 'CNC-bed milling machine',
	'tpFresa_CopyingMillineMachine' => 'Copying milline machine',



// Accesorios FRESA
// ------------------

	'acceFresa_WithoutAccesories' => 'Without accesories',
	'acceFresa_MachineVice' => 'Machine vice',
	'acceFresa_UniversalDivider' => 'Universal divider',
	'acceFresa_UniversalDividerFastCenter' => 'Universal divider - fast center',
	'acceFresa_RoundCoordinatTable' => 'Round coordinat table',
	'acceFresa_CoordinatTableDividing' => 'Coordinat table - dividing ',
	'acceFresa_CoordinatTable' => 'Coordinat table ',
	'acceFresa_Other' => 'Other',

// ISO FRESA
// ----------------
	'isoFresa_ISO_01' => 'ISO 01',
	'isoFresa_ISO_10' => 'ISO 10',
	'isoFresa_ISO_20' => 'ISO 20',
	'isoFresa_ISO_30' => 'ISO 30',
	'isoFresa_ISO_40' => 'ISO 40',
	'isoFresa_ISO_50' => 'ISO 50',


// Bomba de refrigeracion FRESA
// ----------------------------
	'bombRefFresa_IDK' => 'I dont know',
	'bombRefFresa_YesGoodCondition' => 'Yes - Good condition',
	'bombRefFresa_YesBadCondition' => 'Yes - Bad condition',
	'bombRefFresa_No' => 'No',

// Tipo cilindradora
// --------------------
	'tpCilin_Manual' => 'Manual',
	'tpCilin_Electric' => 'Electric',
	'tpCilin_ElectricHydraulics' => 'Electric - hydraulics',
	'tpCilin_Hydraulics' => 'Hydraulics',
	'tpCilin_Other' => 'Other',

// Tipo de serie CILINDRADORA
// --------------------------
	'tpSerCilin_2RollVending' => '2 roll bending ',
	'tpSerCilin_3RollVending' => '3 roll bending ',
	'tpSerCilin_4RollVending' => '4 roll bending ',
	'tpSerCilin_AsymmetricalType' => 'Asymmetrical type',
	'tpSerCilin_Other' => 'Other',

// Tipo PLEGADORA
// --------------------
	'tpPleg_ManualFoldning' => 'Manual foldning',
	'tpPleg_MechanicalFoldingMachine' => 'Mechanical folding machine',
	'tpPleg_HydraulicMechanicalFoldingMachine' => 'Hydraulic-mechanical folding machine',
	'tpPleg_HydraulicFoldingMachine' => 'Hydraulic folding machine',
	'tpPleg_CNCFoldingMachine' => 'CNC - folding machine',
	'tpPleg_Other' => 'Other',

// Tipo GUILLOTINA
// ---------------------
	'tpGuillo_MechanicalGuillotineShear' => 'Mechanical guillotine shear',
	'tpGuillo_HydraulicGuillotineShear' => 'Hydraulic guillotine shear',

// Tipo RECTIFICADORA
// --------------------
	'tpRect_AbrasiveBeltGrinder' => 'Abrasive belt grinder',
	'tpRect_DrillgrindingMachine' => 'Drillgrinding machine',
	'tpRect_JigGrindingMachine' => 'Jig grinding machine',
	'tpRect_SwinFrameGrinder' => 'Swing frame grinder',
	'tpRect_FaceGrindingMachine' => 'Face grinding machine',
	'tpRect_ProfileGrindingMachine' => 'Profile grinding machine',
	'tpRect_GearedShapping' => 'Geared shapping',
	'tpRect_Tool&CutterGrindingMachine' => 'Tool & Cutter Grinding machine',
	'tpRect_CrankshaftLathe' => 'Crankshaft lathe',
	'tpRect_Other' => 'Other',

// Tipo TALADRO
// ----------------
	'tpTalad_2SpindleDrill' => '2 spindle drill',
	'tpTalad_CombinedDrillingMillingMachine' => 'Combined drilling - milling machine',
	'tpTalad_BenchDrill' => 'Bench drill',
	'tpTalad_MultiSpindleDrill' => 'Multi spindle drill',
	'tpTalad_JigDrillingMachine' => 'Jig drilling machine',
	'tpTalad_MagneticDrill' => 'Magnetic drill',
	'tpTalad_RadialDrillingMachine' => 'Radial drilling machine',
	'tpTalad_PillarDrillingMachine' => 'Pillar drilling machine',
	'tpTalad_DeepHoleDrillingMachine' => 'Deep hole drilling machine',
	'tpTalad_ExtractionDrill' => 'Extraction drill',
	'tpTalad_Other' => 'Other',

// Tipo SOLDADORAS
// ----------------------
	'tpSolda_ConventionalWeldingMachine' => 'Conventional welding machine',
	'tpSolda_StudWelding' => 'Stud welding',
	'tpSolda_InvertWelding' => 'Invert welding',
	'tpSolda_MigMag' => 'Mig/Mag',
	'tpSolda_GasEngineWelding' => 'Gas engine welding',
	'tpSolda_DieselEngineWelding' => 'Diesel engine welding',
	'tpSolda_Plasma' => 'Plasma',
	'tpSolda_PlasticWelding' => 'Plastic welding ',
	'tpSolda_SpotWelding' => 'Spot welding',
	'tpSolda_RobotWelding' => 'Robot welding',
	'tpSolda_TIG' => 'TIG',
	'tpSolda_Other' => 'Other',

	// Tipo BOOLEAN
	// ----------------------
	'yes' => 'Yes',
	'no' => 'No',
	'none' => "I don't know",

	// Tipo CABINA |PALA CARGADORA |ORUGA|
	// ----------------------
	'cabinPalaOr_OpenCab' => 'Open cab',
	'cabinPalaOr_EnclosedCab' => 'Enclosed cab',

	'escote' => 'Swing',
	'mm' => 'mm',
	'pulgada' => 'Inch',
	'rosca' => 'Thread',
	'digital' => 'Digital',
	'ventilador_de_refrigeracion' => 'Cooling fan',
	'pinza' => 'Electrode holder',
	'cable_de_tierra' => 'Ground cable',
	'con_alambre' => 'With welding wire',
	'medida_de_caudal' => 'Flow measurement',

	'contador_de_fardos' => 'Bale counter',



];
?>