<?php

return [

	// ========================================== INDUSTRIAL ==========================================
	'categoria_1' => 'Expoindustri has a variety of new and used industrial machinery available for sale. With products from industry leading manufacturers such as Colchester, Boehringer, Masak, Tos, Rosenfors, Köping, Herbert and more. In this section our clients can find, from any kind of lathes, milling cutters, compressors, presses, drills, generators to accessories and spare parts, for any type of need. Browse related categories.',

	// Tornos
	'categoria_2' => 'Expoindustri offers lathes for different types of work, whether in metal, wood or plastic. We have lathes of different sizes and with different functionalities, in the most recognized brands in the market, such as Colchester, Boehringer, Masak, Tos, Rosenfors, Köping, Herbert. If you have any questions, don\'t hesitate to contact us!',
	// Fresas
	'categoria_3' => 'Expoindustri offers milling machines for different types of work, whether in metal, wood or plastic. We have milling machines of different sizes and with different functionalities, in the most recognized brands in the market, such as Zayer, Correa, Huron, Knuth, Deckel, Köping. If you have any questions, don\'t hesitate to contact us!',
	// Cilindradoras
	'categoria_4' => 'Expoindustri offers you cylinder machines for different types of work. We have cylinders in different sizes and with different functionalities, in the most recognized brands in the market, such as Roundo, Froriep, Ungerer. If you have any questions, don\'t hesitate to contact us!',
	// Plegadoras
	'categoria_5' => 'Expoindustri offers you folding machines for different types of work. We have folders machines in different sizes and with different functionalities, in the most recognized brands in the market, such as Lagan, Mossini, Donewell, Guifil. If you have any questions, don\'t hesitate to contact us!',
	// Guillotinas
	'categoria_6' => 'Expoindustri offers guillotines for different types of work. We have guillotines in different sizes and with different functionalities, in the most recognized brands in the market, such as Ursviken, Darley, Hoan, Haco. If you have any questions, don\'t hesitate to contact us!',
	// Rectificadoras
	'categoria_7' => 'Expoindustri offers grinding machines for different types of work. We have grinding machines in different sizes and with different functionalities, in the most recognized brands in the market, such as Voumard, Hauser, Elliot, Mägerle, Studer.  If you have any questions, don\'t hesitate to contact us!',
	// Taladros
	'categoria_8' => 'Expoindustri offers drills for different types of work, whether in metal, wood or plastic. We have drills of different sizes and with different functionalities, in the most recognized brands in the market, such as Arenco, Ogawa, Profila Stanko, Bergonzi, Webo, Foradia, Oerlikon Arboga . If you have any questions, don\'t hesitate to contact us!',
	// Soldadoras
	'categoria_9' => 'Expoindustri offers welders for different types of work. We have welders in different sizes and with different functionalities, in the most recognized brands in the market, such as Esab, Kemppi, Mig Mag, Oerlikon, lincoln.  If you have any questions, don\'t hesitate to contact us!',
	// Herramientas
	'categoria_10' => 'Expoindustri offers tools for different types of work. We have tools in different sizes and with different functionalities, in the most recognized brands in the market, such as Sandvik coromant, Vertex. If you have any questions, don\'t hesitate to contact us!',


	// ========================================== TRANSPORTE ==========================================
	'categoria_11' => 'Expoindustri has different alternatives for both new and used transport equipment available for sale. With products from leading manufacturers in the industry, such as Volvo, Scania, Mercedes, Renault, Iveco and more. In this section our clients can find, from transport equipment to any kind of accessories, for any type of need. Browse related categories.',

	// "Camiones > 3,5ton"
	'categoria_12' => 'Expoindustri offers trucks for different types of work. In this subcategory you can find all kinds of trucks from 0 to 3.5 tons in the most recognized brands on the market, such as Volvo, Scania, Mercedes, Iveco, Daf. If you have any questions, don\'t hesitate to contact us!',
	// Camiones < 3ton
	'categoria_13' => 'Expoindustri offers trucks for different types of work. In this subcategory you can find all kinds of trucks over 3.5 tons in the most recognized brands on the market, such as Volvo, Scania, Mercedes, Iveco, Daf. If you have any questions, don\'t hesitate to contact us!',
	// Camion tractor
	'categoria_14' => 'Expoindustri offers tractor trucks for different types of work. In this subcategory you can find all kinds of tractor trucks in the most recognized brands on the market, such as Volvo, Scania, Mercedes, Iveco, Daf. If you have any questions, don\'t hesitate to contact us!',
	// Motores
	'categoria_15' => 'Expoindustri offers truck engines for different types of trucks. In this subcategory you can find all kinds of engines for the most recognized truck brands on the market, such Volvo, Scania, Mercedes, Iveco, Daf. If you have any questions, don\'t hesitate to contact us!',
	// Otros
	'categoria_16' => 'Expoindustri offers you all kinds of accessories and spare parts for different types of trucks. In this subcategory you can find all kinds of accessories and spare parts for trucks of the most recognized brands on the market, such as Volvo, Scania, Mercedes, Iveco, Daf. If you have any questions, don\'t hesitate to contact us!',


	// ========================================== EQUIPO ==========================================
	'categoria_17' => 'Expoindustri has a variety of new and used heavy equipment available for sale. With products from industry leading manufacturers such as Caterpillar, Volvo, Doosan, Case, JCB and more. In this section our clients can find, from any kind of heavy equipment to accessories and spare parts, for any type of need. Browse related categories.',

	// Palas frontales
	'categoria_18' => 'Expoindustri offers you front shovels for different types of work. In this subcategory you can find all kinds of front shovels in the most recognized brands on the market, such as Caterpillar, Volvo, Case, Ljungby maskin, Jcb If you have any questions, don\'t hesitate to contact us!',
	// Excavadoras
	'categoria_19' => 'Expoindustri offers you bulldozers for different types of work. In this subcategory you can find all kinds of bulldozers in the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere.. If you have any questions, don\'t hesitate to contact us!',
	// Orugas
	'categoria_20' => 'Expoindustri offers you dozers for different types of work. In this subcategory you can find all kinds of dozers in the most recognized brands on the market, such Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. If you have any questions, don\'t hesitate to contact us!',
	// Compactadoras
	'categoria_21' => 'Expoindustri offers you compactors for different types of work. In this subcategory you can find all kinds of compactors in the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. If you have any questions, don\'t hesitate to contact us!',
	// Alza hombres
	'categoria_22' => 'Expoindustri offers you lift men for different types of work. In this subcategory you can find all kinds of lift men in the most recognized brands on the market, such Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. If you have any questions, don\'t hesitate to contact us!',
	// Trituradoras
	'categoria_23' => 'Expoindustri offers you crushers for different types of work. In this subcategory you can find all kinds of crushers in the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. If you have any questions, don\'t hesitate to contact us!',
	// Generadores
	'categoria_24' => 'Expoindustri offers you generators for different types of work. In this subcategory you can find all kinds of generators in the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco. If you have any questions, don\'t hesitate to contact us!',
	// Compresoras
	'categoria_25' => 'Expoindustri offers you compressors for different types of work. In this subcategory you can find all kinds of compressors in the most recognized brands on the market, such Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco If you have any questions, don\'t hesitate to contact us!',
	// Otros
	'categoria_26' => 'Expoindustri offers you all kinds of accessories and spare parts for different types of equipment. In this subcategory you can find all kinds of accessories and spare parts for equipment of the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco. If you have any questions, don\'t hesitate to contact us!',


	// ========================================== AGRICOLA ==========================================
	

	// Tractores
	'categoria_28' => 'Expoindustri offers you tractors for different types of work. In this subcategory you can find all kinds of tractors in the most recognized brands on the market, such as Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. If you have any questions, don\'t hesitate to contact us!',
	// Cosechadoras agrícolas
	'categoria_29' => 'Expoindustri offers you agricultural harvesters for different types of work. In this subcategory you can find all kinds of agricultural harvesters in the most recognized brands on the market, such as Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. If you have any questions, don\'t hesitate to contact us!',
	// Máquina para Heno y forraje
	'categoria_30' => 'Expoindustri offers you hay and forage machines for different types of work. In this subcategory you can find all kinds of hay and forage machines in the most recognized brands on the market, such as Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. If you have any questions, don\'t hesitate to contact us!',
	// Maquinaria de labranza
	'categoria_31' => 'Expoindustri offers you tillage machinery for different types of work. In this subcategory you can find all kinds of tillage machinery in the most recognized brands on the market, such as Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. If you have any questions, don\'t hesitate to contact us!',
	// Otros
	'categoria_32' => 'Expoindustri offers you all kinds of accessories and spare parts for different types of agricultural machinery. In this subcategory you can find all kinds of accessories and spare parts for agricultural machinery of the most recognized brands on the market, such as Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. If you have any questions, don\'t hesitate to contact us!',


	// ========================================== MONTACARGA ==========================================
	'categoria_33' => 'Expoindustri has a variety of new and used forklifts available for sale. With products from industry leading manufacturers such as Cat, Toyota, Kalmar, Hyster and more. In this section our clients can find, from any kind for any type of need. Browse related categories.',

	// Montacargas
	'categoria_34' => 'Expoindustri offers you lift truck for different types of work. In this subcategory you can find all kinds of lift truck in the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. If you have any questions, don\'t hesitate to contact us!',
	// Gruas
	'categoria_35' => 'Expoindustri offers you cranes for different types of work. In this subcategory you can find all kinds of cranes in the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac If you have any questions, don\'t hesitate to contact us!',
	// Otros
	'categoria_36' => 'Expoindustri offers you all kinds of accessories and spare parts for different types of trucks and cranes. In this subcategory you can find all kinds of accessories and spare parts for trucks and cranes of the most recognized brands on the market, such as Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. If you have any questions, don\'t hesitate to contact us!',


	// ========================================== OTROS ==========================================
	'categoria_37' => 'In this section you can find different types of tools, accessories and spare parts from the most prestigious brands in their field, both in new and used products. Browse related categories.',

	// Otros
	'categoria_38' => 'In this section you can find different types of tools, accessories and spare parts from the most prestigious brands in their field, both in new and used products. If you have any questions, don\'t hesitate to contact us!',


];


