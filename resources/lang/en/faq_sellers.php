<?php

return [

	'q_1' => 'What is Expoindustri?',
	'a_1' => 'Expoindustri is the combination of an advanced web portal and an experienced work team, which aims to simplify the sale of new or used industrial machinery, from Europe to Latin America.',

	'q_2' => '2. How does the process of selling machinery to Latin America through Expoindustri work?',
	'a_2' => 'Companies in Europe that sell industrial machines such as lathes, mills, guillotines, welding machines, drills, CNC machines, folding machines, compressors, excavators, generators, cranes, front shovels and trucks among others, can publish and promote their products at www.expoindustri .com. The customer in Latin America buys through our website and we fix the rest. That easy is it!
	 	    <br><br>
Expoindustri takes over the entire process, picking up the machine in the place of the European seller, loading the containers and shipping to the final destination in Latin America, in addition to preparing all the necessary export documentation and managing the collection and payments',

	'q_3' => '3. Which company is in charge of expoindustri.com? ',
	'a_3' => 'Expoindustri Sweden AB is the owner of the name and all rights of www.expoindustri.com, a company registered in Sweden and with organization number 559174-5194.',

	'q_4' => '4. How can I get in contact with the company? Where are your offices located?',
	'a_4' => 'Head office is located in Jönköping, Sweden. We also have offices in Iquique Chile. Soon we will also open offices in Santa Cruz, Bolivia and Tacna, Peru. You can contact us through our Contact Form or to the following contact information:
			<table style="width:100%;">
				<tr>
					<td style="width:50%;">
						<b>Offices in Sweden</b><br>
						<b>Telf:</b> +46 72 3933 700<br>
						<b>Address:</b> Jönköpingsvägen 27<br>
						56161 Tenhult<br>
						<b>Email:</b> info@expoindustri.com
					</td>
					<td style="width:50%;">
						<b>Offices in Chile</b><br>
						<b>Telf:</b> +56 57 2473844​<br>
						<b>Address:</b> Oficina Mapocho, Mz. E, St. 48 D <br>
						Zofri - Iquique <br>
						<b>Email:</b> info@expoindustri.com
					</td>
				</tr>
			</table>
	',

	'q_5' => '5. How much does it cost to publish an ad? Are there different plans? ',
	'a_5' => 'We have different plans that are perfectly suited to different types of customers:
	 	    <br><br>
			<b>Basic:</b> With this plan you can load one machine´s announcement, this announcement will be available in Latin America for 180 days. The price is 360 sek before taxes per ad (aprox. 35 euro).
			<br><br>
			<b>Business:</b> This is a good plan for those companies that have a good number of machineries to advertise. With this package the seller can advertise 30 machines for a year.
			<br><br>
			In addition, our work team in Latin America promotes its machines more specifically to improve sales time and at the same time, the seller has the opportunity to create their online store within expoindustri.com, which allows create a link to your company\'s website (*), show your company name, logo, address and all your contact information.
			<br><br>
			<b>The price: 6,900 sek before taxes per package (aprox. 660 euro)</b>
			<br><br>			
			<b>Plus:</b> This plan is perfectly suited to those companies that have a large number of products to offer. This package includes unlimited loading of machinery ads for one year.
			In addition, our work team in Latin America promotes its machines more specifically to improve sales time and at the same time, the seller has the opportunity to create their online store within expoindustri.com, which allows create a link to your company\'s website (*), show your company name, logo, address and all your contact information.
			<br><br>
			Finally, with this plan your company has the option to upload a promotional banner at expoiundustri.com
			<br><br>
			<b>The price 12,900 sek before taxes per package (aprox. 1.225 euro)</b>
			<br><br>
			<b>(*)</b> If you also want to increase your arrival to the Spanish-speaking market even further, we can translate your website into Spanish. <a href="https://www.expoindustri.com/contact">Contact us for more information</a>.

',

	'q_6' => '6. Who is responsible for paying me if my machine sells?',
	'a_6' => 'We always contact the seller of the machine to confirm that it is still available.
			<br><br>
			The buyer in Latin America pays Expoindustri, who, at the same time, is responsible for paying the total amount to the seller of the machine in Europe. The advertiser of the machine in Europe issues an invoice in the name of Expoindustri Sweden AB.',

	'q_7' => '7. Why do you request a final sale price when the ad is uploaded?',
	'a_7' => 'It is good to include that last sale price so that our work team in Latin America can negotiate with potential buyers and have a greater opportunity to realize a business. That last sale price can ONLY be seen by the Expoindustri work team and will NEVER be shown to the buyer in Latin America. Placing this data when loading the ad is optional.',

	'q_8' => '8. Who is in charge of collecting, loading and shipping machinery to Latin America?',
	'a_8' => 'Expoindustri Sweden AB is always in charge of picking up the machine, loading it in the container and shipping it to the final destination in Latin America, in addition to all export documentation in its entirety.',

	'q_9' => '9. What does it mean to have an online store at www.expoindustri.com?',
	'a_9' => 'The online stores within expoindustri.com allow the machinery seller to group all their ads in the same account, which at the same time gives him the opportunity to promote his company, among other things, creating a link to his website (*), In addition to displaying all relevant information, such as name, logo, address etc.
			<br><br>
			<b>(*)</b> If you also want to increase your arrival to the Spanish-speaking market even further, we can translate your website into Spanish. <a href="https://www.expoindustri.com/contact">Contact us for more information</a>.',

	'q_10' => '10. What kind of machinery can my company advertise?',
	'a_10' => 'Your company, whether you are a manufacturer, representative or reseller of machinery, can advertise both new and used machinery in the following categories: transport, agriculture, equipment, forklifts, industrial, others.
			<br><br>
			Your company, whether you are a manufacturer, representative or reseller of machinery, can advertise both new and used machinery in the following categories: transport, agriculture, equipment, forklifts, industrial, others.',

	'q_11' => '11. Can I advertise a machine that is not operational?',
	'a_11' => 'Yes, you can advertise a machine that is not in an operational status, as long as this is clearly specified in your ad and have a detail of what are the disadvantages of the machinery.
			<br><br>
			If the Latin American buyer requests it, Expoindustri Sweden AB can even perform a technical inspection of the machine on site. This inspection has no extra cost for the advertiser of the machine.',

	'q_12' => '12. Can you make a special promotion of my brand / company in Latin America?',
	'a_12' => 'Yes, we can do this. Do not hesitate to contact us, to explain to us in what special way you would like us to help you.',

];
