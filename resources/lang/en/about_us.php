<?php
return [
	'history' => 'History',
	'history_content' => 'How it all started.',

	'process_flow' => 'Process flow', // esta en messages
	'process_flow_content' => 'Please take a look at our video about process flow.', // esta en messages

//	'duty_brokers' => 'Tullmäklare',
//	'duty_brokers_content' => 'För att få den hjälp som behövs vid din tullklarering.',

	'security' => 'Security',
	'security_content' => 'Your safety is the most important thing, protect your investment with us. ',

	'sustainability' => 'Sustainability',
	'sustainability_content' => 'Our efforts for the present and future',


	// --------------------------------------

	'history_popup_title' => 'Our history',
	'history_popup_content' => '
	<p>Expo industry is a combination of an advanced web portal and an experienced team that will simplify the sale of industrial and construction machinery between Europe and Latin America.</p>
	<p>With us you can get different types of solutions, from finding a partner for your company in Latin America to selling / buying machines directly from our web portal.</p>
	<p>It\'s as simple as "buying clothes online", thanks to our web portal\'s final price calculator, which calculates tariffs, transport, logistics and all related costs, from Europe to the final destination, as well as our team behind the whole process with more than 10 years of experience, which does all the hard work to eliminate all the complications of the export process and provides security for the supplier and the customer.</p>
	',

	'process_flow_popup_title' => 'Process flow',
	'process_flow_popup_content' => 'Coming soon.',

	'security_popup_title' => 'Seguridad',
	'security_popup_content' => '
<p>For Expoindustri, the customer\'s safety is the most important thing. Therefore, our main focus is to:</p>
<ul class="fa-ul">
	<li><span class="fa-li"><i class="fa fa-check"></i></span>To check the actual existence of the products the buyer wants to buy.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>To make technical inspections and edition report of product, if desired by buyers.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>To ensure that all transport, both on land and at sea, is carried out carefully and by competent staff. So that Exportindustri can deliver the product in the original condition.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>To systematically keep the buyer informed about the entire buying process.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>To ensure that the product is delivered at the specified delivery date.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Having offices and trained staff available in current countries. (See our office)</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>To insure the product through to the chosen insurance, if desired by the buyer</li>
</ul>
',

	'sustainability_popup_title' => 'Durability',
	'sustainability_popup_content' => '
<p>At Expoindustri, we target our energy so that everyone, both in Europe and in South America, will be satisfied when it comes to quality, prices and efficiency in services.</p>
<p>Our assignment is to make it easier for our customers to sell / buy between these two continents. Making the process effective.</p>
<p>Our <strong>VISION</strong> is to reach out to more countries in the world. To facilitate the commercial industry between countries. We want it to be as easy for both companies and private individuals to shop.</p>
<p>Our <strong>STRATEGY</strong> lies in offering a pleasant work environment so that we can offer high quality services.</p>
<p>We tailor solutions for the individual customer. In this way, we become more efficient and productive while we work towards our vision.</p>
',
	'broker_popup_content' => 'The agreement we have with the following customs companies will facilitate the process of customs clearance of your machinery.
<br><br>Contact the nearest agency.',


];