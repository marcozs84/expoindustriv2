<?php

return [

	'service_for_sellers' => 'For you who are <b>MANUFACTURER</b> or <b>MACHINE DEALER</b>, both new and used, we have different solutions',
	'service_for_buyers' => '<b>For those who want to buy machines from Europe</b>, both new and used, we have different solutions',

	'service_banner' => 'Advertise your business and reach thousands of potential customers.',
	'service_search' => 'Can not find what you are looking for? Is it a special machine? Let us help you with your search .',
	'service_inspection' => 'Make sure the equipment you want is in GOOD condition with our profesional inspectors.',
	'service_support' => 'We solve your logistics problems for all types of machines.',
	'service_istore' => 'Create your own online store through ExpoIndustri and group all your products under your own Brand and logo.',

	'serv_banner_title' => 'Banner advertising',
	'serv_banner_body' => 'Market Your Business to a NEW  World of potential customers in EUROPE AND  SOUTH AMERICA ',

	'serv_search_title' => 'Specialized search',
	'serv_search_body' => 'Could not you find the machine you need on our website? Do not worry!! <br>The ExpoIndustri team is there to support you with its multiple search channels to find what kind of machine on the European market.',

	'serv_support_title' => 'Logistics support',
	'serv_support_body' => 'ExpoIndustri team provides logistic support for the final destination you choose, with regular information about the status of your delivery, for all equipment purchased on our website. But we also solve the logistical support for all types of machines or equipment that already has. Our team is responsible for collecting the machine from its origin, loading it in one of our different options and achieving its final destination in South America. See our shipping option.',

	'serv_support_contcomp' => 'Shared container',
	'serv_support_contcomp_msg' => '<div style=\'text-align:left; width:200px; padding:0px; font-size:13px;\'>
This is a good solution if needed Lower shipping costs.<br><br> 
With this option, you will pay a combination of square meters used by your machine and the weight of the same.<br><br> 
The arrival time of the final destination is about 80-180 days. The most sought after transport of several.<br><br> 
<strong>CHECK UP NOW!</strong>!</div>',

	'serv_support_contex' => 'Exclusive container',
	'serv_support_contex_msg' => '<div style=\'text-align:left; width:200px; padding:0px; font-size:13px;\'>
An excellent option if you require your equipment at your final destination in a short time.<br><br> 
The arrival time to final destination is approximately 60-80 days.  <br><br> 
<strong>SEE PRICES NOW</strong>!</div>',

	'serv_support_contabi' => 'Open container',
	'serv_support_contabi_msg' => '<div style=\'text-align:left; width:200px; padding:0px; font-size:13px;\'>
The only option if it exceeds the dimensions of a regular container but with the same security and guarantee.<br><br> 
The arrival time to final destination is approximately 80-120 days. <br><br>
<strong>SEE PRICES NOW</strong>!</div>',

	'serv_inspection_title' => 'Technical inspections',
	'serv_inspection_body' => 'Our team has professional inspectors and we also work with an external company with great knowledge in inspections of high-weight machine classes in a safe, impartial and professional manner. Therefore, we have the experience and knowledge required to carry out inspections in general. If you are buying a machine, in our ads or outside, make sure the equipment is in good condition. Leave the heavy work for us!.',

	'serv_istore_title' => 'Online store',
	'serv_istore_body' => 'Accelerate your sales as well as to know your brand, gather your entire range of products in the same place, as well as expose your brand or company to a completely new world of potential customers OPEN YOUR STORE NOW!.',

	'serv_partner_title' => 'Find a partner in Latin America',
	'service_partner' => 'Or find out about our different services such as logistic support among others.',
	'serv_partner_body' => 'If you want to reach a new market with great potential like Latin America and you need a partner, a representative, an authorized workshop that can work with your products, you may only need logistic support to deliver your products. Whatever your need, we have a wide business network and contact to help you. Contact us and book an appointment so we can see the best solution for your company.',

	'serv_buyonline_title' => 'Buy machinery online, easy and safe',
	'service_buyonline' => 'Direct from Europe, through our advanced web portal',
	'serv_buyonline_body' => 'Access a wide variety of industrial machinery, both new and used, directly from Europe for you. Easy and safe. Register, choose and place your order, we will take care of the rest. As simple as that!',
];

?>