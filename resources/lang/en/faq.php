<?php

return [

	'q_1' => '1. What is Expoindustri?',
	'a_1' => 'Expoindustri is the combination of an advanced web portal and an experienced work team, which aims to simplify the sale of new or used industrial machinery, from Europe to Latin America.',
	'q_2' => '2. Which company is in charge of expoindustri.com?',
	'a_2' => 'Expoindustri Sweden AB is the owner of the name and all rights of www.expoindustri.com, a company registered in Sweden and with organization number 559174-5194.',
	'q_3' => '3. How can I get in contact with the company? Where are your offices located?',
	'a_3' => 'Head office is located in Jönköping, Sweden. We also have offices in Iquique Chile. Soon we will also open offices in Santa Cruz, Bolivia and Tacna, Peru. You can contact us through our Contact Form or to the following contact information:
			<table style="width:100%;">
				<tr>
					<td style="width:50%;">
						<b>Offices in Sweden</b><br>
						<b>Telf:</b> +46 72 3933 700<br>
						<b>Address:</b> Jönköpingsvägen 27<br>
						56161 Tenhult<br>
						<b>Email:</b> info@expoindustri.com
					</td>
					<td style="width:50%;">
						<b>Offices in Chile</b><br>
						<b>Telf:</b> +56 57 2473844​<br>
						<b>Address:</b> Oficina Mapocho, Mz. E, St. 48 D <br>
						Zofri - Iquique <br>
						<b>Email:</b> info@expoindustri.com
					</td>
				</tr>
			</table>
',
	'q_4' => '4. How can I know that the machines are operational?',
	'a_4' => 'The seller is the one who indicates the state of the machinery in his ad, and there are five options, New, Operational, Operational with problems, Not Operational and Unknown.
<br><br>
In the event that the machinery is operational with problems, Not operational or Unknown, we would recommend that the buyer request a technical review. Expoindustri uses an external company with years of experience in technical revisions of machinery in general, to make the technical report of said machinery.
<br><br>
This additional Technical Review service has an extra cost, which is shown in the quote requested by the buyer.
',
	'q_5' => '5. Do I have to pay the total cost of the machinery before receiving the technical review?',
	'a_5' => 'No, if you requested a technical review in your quotation, at the time of clicking on pay, you will only have to initially cancel the cost of said revision. After receiving the requested technical report, you can choose to continue with the purchase of the machinery or remove it from your shopping list.',
	'q_6' => '6. How much does it cost and in what time do you send me the technical review report?',
	'a_6' => 'The cost of the technical review report varies depending on the type of machinery to be inspected. By including the option of TECHNICAL REVIEW in your quotation, you will automatically obtain the value to be paid.
<br><br>
The report will be sent to your email within 2 to 5 business days.
',
	'q_7' => '7. Can I rent machinery at ExpoIndustri.com?',
	'a_7' => 'No, we do not rent machinery.
<br><br>
We recommend that you request information from your bank about the possibility of a lease (or credit if your project lasts more than 6 months)',
	'q_8' => '8. Where are the machinery located?',
	'a_8' => 'The machinery is located in different places in Europe, but you don\'t have to worry, since ExpoIndustri is 100% responsible for the transport process and the necessary documentation so that the machinery arrives directly at the destination you chose.',
	'q_9' => '9. Does the price indicated on the page include VAT? ',
	'a_9' => 'No, the price indicated on the page is excluded from VAT and nationalization costs. You can contact us if you want to know more details.',
	'q_10' => '10. How can I know if the payment is safe and that I will receive my machinery?',
	'a_10' => 'Each payment is backed by a contract, according to our legal terms and conditions. Once you send the purchase order through your account on expoindustri.com, we control that the machinery is still available, once controlled and confirmed the existence of it, we will send you a invoice and the steps to proceed with the secure payment.',
	'q_11' => '11. How much time do I have to pay the invoice?',
	'a_11' => 'Once ExpoIndustri has sent the invoice via expoindustri.com, you have 24 hours to make the deposit. (see payment options in the next question).',
	'q_12' => '12. What are the payment options?',
	'a_12' => 'ExpoIndustria works with the most prestigious banks in your country, so you will not have to pay extra for shipping abroad.
<br><br>
Bank Transfer<br>
Tarjeta de crédito o debito<br>
PayPal<br>
',
	'q_13' => '13. What happens if I don\'t pay before 24 hours and pay later?',
	'a_13' => 'If you do not pay before 24 hours, ExpoIndustri cannot guarantee the existence of the machinery. In the event that the machine is no longer available for having paid after 24 hours, we will refund the money. You can read more in our legal terms and conditions.
<br><br>
If the machinery is still available, there is no problem and we continue with the process of buying and delivering the machinery at the chosen destination.
',
	'q_14' => '14. Can I pay in installments?',
	'a_14' => 'No, you cannot pay in installments, see more our legal terms and conditions.',
	'q_15' => '15. Can I pay at your local office?',
	'a_15' => 'No, you cannot pay at our local offices, but the staff at our offices will gladly provide you with the necessary information for a secure purchase.',
	'q_16' => '16. What happens after I have made the payment?',
	'a_16' => 'Once we verify the payment made, we will send your invoice with the information of said machine, the payment you made and the delivery time. In addition, our qualified staff either at our branch or central office will provide you with the support and information you are requesting.',
	'q_17' => '17. Does ExpoIndustri offer financing?',
	'a_17' => 'We do not offer financing.',
	'q_18' => '18. Why can\'t I see the location of the machine?',
	'a_18' => 'As we are an online platform, we do not have a place where you can visit the equipment / machinery. The equipment / machinery we offer is located in different parts of Europe. For the privacy of the advertisers we do not publish the location, but you can contact one of our sales consultants for more information.',
	'q_19' => '19. How long does it take for the machine to arrive once the machinery is paid?',
	'a_19' => 'We offer two ways of sending, shared shipping or preferential shipping, if you chose shared shipping, the delivery time is 70 - 180 days and if you chose preferential shipping, the delivery time is 65 - 100 days, counting from the day You made the payment. Read more in our legal terms and conditions.',
	'q_20' => '20. Can I trust ExpoIndustri.com?',
	'a_20' => 'Of course. The ExpoIndustri team has more than 10 years of experience in importing machinery to Latin America, which guarantees the security of your money and the shipment of machinery, with the greatest care until your final destination.',
	'q_21' => '21. Does the machine I am interested in have accessories, does it also come to me?',
	'a_21' => 'You can see in advance the seller\'s announcement of the accessories that the machinery has. In addition, on your invoice specify exactly what you bought, including accessories in the cases that apply.',

];
