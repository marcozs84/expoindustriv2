<?php

return [
	'preo_subject' => 'PRE-ORDER OF PURCHASE',
	'preo_greetings' => 'Thank you very much for trusting us, we have correctly received your <b>purchase order</b> and one of our consultants will contact you within the following 24 Hrs., Then a copy of your request:',
	'preo_full_gallery' => 'See full gallery',
	'machine_detail' => 'Details of the machine',
	'go_original_ad' => 'Go to the original announcement',
	'machine_price' => 'Machine price',
	'additional_services' => 'Additional services',
	'tech_report' => 'Technical report',
	'paid_on_request' => 'Already paid when registering the order',
	'machine_insurance' => 'Machinery insurance',
	'transport' => 'Transport',
	'destination' => 'Destination',
	'important_note' => 'Important note',
	'price_in_final_destiny' => 'Final price after delivery',
	'report_order' => 'If you have not requested this information, you can report this message by clicking on the following link:',
];

?>