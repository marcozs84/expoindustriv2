<?php

return [
	'register_subject' => 'Welcome to ExpoIndustri',
	'register_message' => '
		Dear :nombres_apellidos,<br><br>
		
		We want to give a warm welcome to <b> ExpoIndustri </b>, to verify that this is your email account please click on the following link:
		<br><br>
		<a href=":url_confirm">:url_confirm</a>
		
		<br><br>
		Best regards,<br>
		<b>ExpoIndustri Team</b>
		
		<br><br>
		<hr>
		<span style="color:#666666;">If for any reason you haven\'t requested a membership in our site, please let us know by clicking in the link below:</span>
		<br><br>
		<a href=":url_report">:url_report</a>',

	'forgot_subject' => 'Expoindustri: Did you forgot your password?',
	'forgot_message' => '
		Dear :nombres_apellidos,<br><br>
		
		We have received a request to reset your password, if this is correct, please click on the following link to set a new password.                         
		<br><br>
		<a href=":url_confirm">:url_confirm</a>
		<br><br>
		
		If you have not requested a password change, please ignore this message.
		
		<br><br>
		A warm greeting,<br>
		<b>ExpoIndustri team</b>',
];

?>