<?php

return [
	'register_subject' => 'Välkommen till ExpoIndustri',
	'register_message' => '
		Hej :nombres_apellidos,<br><br>
		
		För att verifiera att detta är ditt e-postkonto, vänligen klicka på följande länk.
		<br><br>
		<a href=":url_confirm">:url_confirm</a>
		
		<br><br>
		Med vänliga hälsningar<br>
		<b>ExpoIndustri Team</b>
		
		<br><br>
		<hr>
		<span style="color:#666666;">Om du av någon anledning inte har begärt ett medlemskap på vår webbplats, var god och låt oss veta det så vi kan avregistrera ditt e-postkonto med hjälp av följande länk.</span>
		<br><br>
		<a href=":url_report">:url_report</a>',

	'forgot_subject' => 'Expoindustri: Har du glömt lösenordet??',
	'forgot_message' => '
		Hej! :nombres_apellidos,<br><br>
		
		Vi har fått förfrågan om att återställa ditt lösenord. Om det är korrekt, klicka på följande länk för att ange ett nytt lösenord.
		<br><br>
		<a href=":url_confirm">:url_confirm</a>
		<br><br>
		
		Om du inte har begärt att återställa ditt lösenord ignorerar du det här meddelande.
		
		<br><br>
		En hjärtlig hälsning,<br>
		<b>Expoindustri Team</b>',
];

?>