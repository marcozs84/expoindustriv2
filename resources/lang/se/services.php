<?php

return [

	'service_for_sellers' => '<b>För dig som är TILLVERKARE eller ÅTERFÖRSÄLJARE AV MASKINER,</b> både nya och begagnade, har vi olika lösningar ',
	'service_for_buyers' => '<b>För dig som vill köpa maskiner från Europa,</b> både nya och begagnade, har vi olika lösningar ',

	'service_banner' => 'Annonsera ditt företag och nå tusentals potentiella kunder.',
	'service_search' => 'Hittar du inte det du söker? Är det en speciell maskin? Låt oss hjälpa dig med din sökning.',
	'service_inspection' => 'Se till att den utrustning du vill ha är i GOTT skick med våra professionella inspektörer.',
	'service_support' => 'Vi löser dina logistikproblem för alla typer av maskiner.',
	'service_istore' => 'Skapa din egen webbutik genom ExpoIndustri och gruppera alla dina produkter under eget varumärke och logotyp.',

	'serv_banner_title' => 'Bannerreklam',
	'serv_banner_body' => 'Marknadsför ert företag till en NY värld av potentiella kunder i EUROPA OCH  SYDAMERIKA',

	'serv_search_title' => 'Specialiserad sökning',
	'serv_search_body' => 'Kunde du inte hitta den maskin du behöver på vår hemsida? Oroa dig inte!! <br>ExpoIndustri -teamet finns där för att stödja dig med sina flera sökkanaler  för att hitta vilken typ av maskin på den Europeiska marknaden.',

	'serv_support_title' => 'Logistik support',
	'serv_support_body' => 'ExpoIndustri-team tillhandahåller logistiskt stöd till det slutliga destination du väljer, med regelbunden information om status för din leverans, för all utrustning som köpts på vår hemsida. Men även vi löser det logistiska stödet för alla typer av maskiner eller utrustning som redan har. Vårt team ansvarar för att samla maskinen från ursprunget, lasta den i ett av våra olika alternativ och få det att nå sitt slutmål i Sydamerika. Se våra fraktalternativet.',

	'serv_support_contcomp' => 'Delad frakt',
	'serv_support_contcomp_msg' => '<div style=\'text-align:left; width:300px; padding:0px; font-size:15px;\'>
Det här är en bra lösning om det behövs Lägre sjöfartskostnader. <br><br> 
Med detta alternativ kommer du betala en kombination av kvadratmeter som används av din maskin och vikten på samma. <br><br> 
Ankomsttiden till slutdestinationen det är ungefär 80-180 dagar. Den mest eftertraktad transport av flera.<br><br> 
<strong>KOLLA UPP  PRISER NU</strong>!</div>',

	'serv_support_contex' => 'Exklusiv container',
	'serv_support_contex_msg' => '<div style=\'text-align:left; width:300px; padding:0px; font-size:15px;\'>
Ett utmärkt alternativ om du behöver din utrustning på ditt slutdestination på kort tid.<br><br> 
Ankomsttiden till slutdestinationen är ungefär 60-80 dagar. <br><br> 
<strong>KOLLA UPP  PRISER NU</strong>!</div>',

	'serv_support_contabi' => 'Öppen container',
	'serv_support_contabi_msg' => '<div style=\'text-align:left; width:300px; padding:0px; font-size:15px;\'>
Det enda alternativet om det överstiger dimensionerna för en vanlig behållare men med samma säkerhet och garanti.<br><br>
Ankomsttiden till slutdestinationen är cirka 80-120 dagar. <br><br>
<strong>SE PRISER NU!</strong>!</div>',

	'serv_inspection_title' => 'Teknisk inspektion',
	'serv_inspection_body' => 'Vårt team har professionella inspektörer och vi arbetar också med ett externt företag med stor kunskap inom inspektioner av höga viktklasser maskin på ett säkert, opartiskt och professionellt sätt. Därför har vi den erfarenhet och kunskap som krävs för att utföra inspektioner i allmänhet. Om du ska köpa en maskin, i våra annonser eller ute, se till att utrustningen är i gott skick. Lämna det tunga arbetet till oss!.',

	'serv_istore_title' => 'Onlinebutik',
	'serv_istore_body' => 'Snabba din försäljning såväl som att känna ditt varumärke, samla hela ditt sortiment av produkter på samma ställe, samt exponera ditt varumärke eller företag till en helt ny värld av potentiella kunder Öppna din butik nu!.',

	'serv_partner_title' => 'Hitta en Partner i Latinamerika',
	'service_partner' => 'Eller kolla efter  våra olika specialiserade tjänster som  logistik stöd och andra',
	'serv_partner_body' => 'Om du vill nå en ny marknad med stor potential som Latinamerika och du behöver en partner, en representant, en auktoriserad verkstad som kan arbeta med dina produkter, kanske du bara behöver logistiskt stöd för att leverera dina produkter. Oavsett ditt behov har vi ett brett affärsnätverk och kontakter för att hjälpa dig. Kontakta oss och boka en tid så att vi kan se de bästa lösningar för ditt företag.',

	'serv_buyonline_title' => 'Köpa maskiner online, trygg och enkel',
	'service_buyonline' => 'Direkt från Europa genom vår avancerade webbportal',
	'serv_buyonline_body' => 'Få tillgång till ett stort antal industriella- och entreprenadmaskiner, både nya och begagnade, direkt från Europa för dig. Enkelt och säkert. Registrera dig, välj och beställ, ordnar resten. Så enkelt är det!',
];

?>