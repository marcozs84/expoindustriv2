<?php

return [

	'q_1' => '1. Vad är Expoindustri?',
	'a_1' => 'Expoindustri är en kombination av en avancerad annonsportal och ett erfaret team, där vi hjälper företag att öka sin försäljning eller hitta en specifik maskin, på ett säkert, enkelt och effektivt sätt..',

	'q_2' => '2. Vilket företag ansvarar för www.expoindustri.se?',
	'a_2' => '  Alla expoindustri.se rättigheter och namn är egendom av Expoindustri Sweden AB, registrerad i Sverige med org. Nr 559174-5194
			',

	'q_3' => '3. Var ligger kontoren? Hur kan jag kontakta er?',
	'a_3' => 'Huvudkontor ligger i Tenhult, Sverige. Vi har även lager och kontor i Iquique Chile. Om ni vill kontakta oss.
<br><br>
				<table style="width:100%;">
				<tr>
					<td style="width:50%;">
						<b>Kontor i Sverige</b><br>
						<b>Telf:</b> +46 72 3933 700<br>
						<b>Adress:</b> Jönköpingsvägen 27<br>
						56161 Tenhult<br>
						<b>Email:</b> info@expoindustri.com
					</td>
					<td style="width:50%;">
						<b>Kontor i Chile</b><br>
						<b>Telf:</b> +56 57 2473844​<br>
						<b>Adress:</b> Oficina Mapocho, Mz. E, St. 48 D <br>
						Zofri - Iquique <br>
						<b>Email:</b> info@expoindustri.com
					</td>
				</tr>
			</table>
	',

	'q_4' => '4. Hur fungerar processen att sälja?',
	'a_4' => ' Processen för att sälja har 5 enkla steg.
a)	Steg 1: Ni väljer en av våra planer och annonserar.
b)	Steg 2: Säljteam i Europa och Sydamerika börjar marknadsföra dina annonserade maskiner eller transportfordon och kontaktar dem potentiella köpare.
c)	Steg 3: När vi har hittat en potentiell köpare kontaktar vi dig (säljaren), för att bekräfta om maskinen är fortfarande tillgänglig. Om det är fallet slutför vi affären med köparen.
d)	Steg 4: När vi har fått inbetalningen från köparen kontaktar vi dig ytterligare en gång till och du som säljaren fakturerar till Expoindustri Sweden AB och vi betalar direkt till ditt bankkonto. 
e)	Steg 5: Efter du har fått betalningen ordnar vi hämtning, lastning, export (om detta gäller) och leverans till slutdestination. 

Med denna process stödjer vi säljaren så han kan koncentrera sig i det som är mer betydelsefullt i sin verksamhet! Expoindustri löser det omständiga biten  i sin helhet. ',


	'q_5' => '5. Vad kostar att publicera en annons? Har Expoindustri olika planer?',
	'a_5' => 'Vi erbjuder 3 planer (*):
			<br><br>
			<b>Basic:</b> Här kan du ladda upp en annons som kommer vara synlig på vår hemsida i Europa och Sydamerika under 180 dagar. Pris 360 sek exkl. moms/annons. 
			<br><br>
			<b>Business:</b> Denna plan passar perfekt för företagen som har ett 20 tal maskiner att erbjuda. Med plan business kan du annonsera upp till 30 maskiner under ett helt 1 år.
			<br><br>
			Ni får tillgång till vårt försäljningsteam i Europa och Sydamerika som kommer att marknadsföra era maskiner eller transportfordon för att underlätta försäljningen, samtidigt kan säljaren skapa sin online butik på expoindustri.se där bland annat kan länka till sin egen hemsida (**), lägg upp sitt företagsnamn, logo, adress, telefonnummer, osv.
			<br><br>
			<b>Pris 6.900 sek exkl. moms/paket.</b>
	 	    <br><br>
			<b>Plus:</b> Denna plan passar perfekt för dem större företag som har ett större utbud av maskiner att erbjuda. Med plan plus kan du annonsera obegränsad med maskiner under ett helt 1 år.
			<br><br>
			Ni får tillgång till vårt försäljningsteam i Europa och Sydamerika som kommer att marknadsföra era maskiner eller transportfordon för att underlätta försäljningen.
	 	    <br><br>
			Säljaren kan skapa sin online butik på expoindustri.se där bland annat kan länka till sin hemsida (**), lägg upp sitt företagsnamn, logo, adress osv. dessutom får ni tillgång till en bannerreklam på expoindustri.se.
	 	    <br><br>
			<b>Pris 12.900 sek exkl. moms/paket.</b>
	 	    <br><br>
			<b>(*)</b> Alla annonser blir översatt till svenska, engelska och spanska.<br>
			<b> (**)</b> Vill ni även förbättra möjligheter med kunderna i dem spansktalande länder? Vi kan hjälpa er att översätta sina hemsidor på spanska. kontakta oss för mer information.<br>
			<b> (**)</b> Vill ni ha hjälp att ladda upp era annonser?, vi är mer en glada att kopiera listor från din webbsida kostnadsfritt.',
	'q_6' => '6. Om jag säljer en maskin eller transportfordon via er, vem är ansvarig att betala mig?',
	'a_6' => 'Finns 2 alternativ:
Om köparen direkt kontaktar er genom ert online butik, kan ni komma överens om hur ska betalningen sköta, men säljaren kan välja ändå att Expoindustri tar hand om hela affärsprocessen om köparen är t.ex från Sydamerika. Det vill säga att köparen betalar till Expoindustri och vi betalar till ditt bankkonto efter du har fakturerat till Expoindustri Sweden AB.
Om köparen vill köpa maskinen genom Expoindustri ordnar vi hela processen och vi betalar direkt till ert bankkonto. (läs punkt 4). ',

	'q_7' => '7. Vem är ansvarig för inhämtning, last och leverans till slutdestination?',
	'a_7' => 'Expoindustri Sweden AB ansvarar för att hämta, lasta och ordna transport till slutdestination i Europa eller Sydamerika, samt exportdokumentation i helhet (om detta gäller), så länge köparen och säljaren inte har bestämt något annat.',

	'q_8' => '8. Vad innebär en online butik på www.expoindustri.se?',
	'a_8' => 'En Online butik på expoindustri.se tillåter säljaren i Europa att gruppera alla annonser samt all information av sitt företag på samma plats och där kan säljaren bland annat länka till sin hemsida(*), lägga upp sitt företagsnamn, logo och kontakinformation.
<br><br>
(*) Vill ni även förbättra möjligheter med kunderna i dem spansktalande länder? Vi kan hjälpa er att översätta sina hemsidor på spanska.',

	'q_9' => '9. Vad för slags maskiner eller transportfordon kan mitt företag annonsera?',
	'a_9' => 'Om du är ett företag som tillverkar, återförsäljare eller helt enkelt ett företag som vill sälja sina maskiner eller transportfordon, då kan du annonsera på www.expoindustri.se, inom följande kategorier: Transport, Lantbruk, Entreprenad, Truck och Industri.

Vi är en marknadsplats där du kan sälja nya som begagnade lättlastbilan som tungdragare, hjullastare, grävmaskiner, kranar, dumprar, truckar, kompressorer, elverk, traktorer, skördar men också verkstadsmaskiner så som svarvar, fräsar, kantmaskiner, gradsaxar, svetsar, borrmaskiner, CNC maskiner med fler. 

Om ni tror att eran maskiner inte kan kategorisera i en av dessa nämna kategorier tveka inte och höra av er med kompletterande frågor eller funderingar.',

	'q_10' => '10. Kan jag annonsera en maskin eller transportfordon som inte fungerar?',
	'a_10' => 'Ja, du kan annonsera en maskin eller transportfordon i dåligt skick, så länge detta förklaras väl i annonsen. I vissa fall är köparen intresserade av maskiner eller transportfordon som dem kan reparera själva, men det är viktigt att ha en mycket detaljerad beskrivning.
	 	    <br><br>
			Om köparen vill har mer information av maskinens eller transportfordonens skick kan Expoindustri Sweden AB besöka säljarens adress för att göra en teknisk inspektion eller (outsourcat företag) Detta blir inga extra kostnader för säljaren. ',

	'q_11' => '11. Kan ni marknadsföra företag/produkt t.ex i Sydamerika på ett särskild sätt?',
	'a_11' => 'Absolut, vi kan göra det. Kontakta oss gärna och prata med en av våra experter.',
];
