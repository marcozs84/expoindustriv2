<?php

return [
	'title' => 'INFORMATION FÖR DIG SOM ANNONSÖR', //'SÄLJTIPS FÖR DIG, SOM ÄR EN STOR SÄLJARE',
	'slogan' => 'Att sälja på www.Expoindustri.com är väldigt enkelt och alla kan göra. Här kommer några säljtips.',
	'title_1' => 'SÄTT ETT BRA PRIS',
	'body_1' => '<b>Att sätta rätt pris är avgörande, allt går att sälja till rätt pris.</b><br>
<ul>
<li>Titta vad andra satt för priser för liknande varor. Du kan även ta kontakt med oss om du är osäker, vi har många års erfarenhet inom försäljning. Vi hjälper gärna dig.</li>
<li>För snabbare försäljning, sätt ett lägre pris.</li>
</ul>
',
	'title_2' => 'TA BRA BILDER',
	'body_2' => 'En bild säger mer än tusen ord.
<ul>
<li>Tänk att ha en ren miljö runt omkring din produkt så att bakgrunden inte tar fokus från det du vill sälja. ( ExpoIndustri kan eventuell avvisa din annons, om inte följer reglerna )</li>
<li>Fotografera gärna med dagsljus i så mycket medljus som möjligt för att visa din produkt bra.</li>
<li>Kolla så att din bild är skarp innan den publiceras. Suddiga och oskarpa bilder fångar inte köparens intresse lika väl.</li>
<li>Ta gärna fler bilder ur olika vinklar, Det uppskattar köparen</li>
</ul> 
',
	'title_3' => 'GRUNDLÄGGANDE INFORMATION',
	'body_3' => 'Fundera på vad du skulle vilja veta! ”I större detalj, större möjligheter att hitta köpare”
<ul>
<li>Gruppera din maskin tillsammans med andra av samma typ.</li>
<li> Ge oss den exacta adressen och mått på maskinen. Det kan påverka priset till slutdestination.</li>
<li>Fyll i gärna ytterliggare information. Köparen vill veta desto mer desto bättre. Tänk på det!</li>
<li>Var ärlig, skriv ut eventuella fel och skador.</li>
</ul>
',
	'title_4' => 'ANDRA TIPS!',
	'body_4' => '
<ul>
<li>Förnya och hamna först!</li>
<li>Genom att förnya annonsen hamnar din annons högst upp i sökresultatet.</li>
<li>Ändra annonsen</li>
<li>Får du inte din produkt såld? Prova att ändra annonsen för att få fler träff. T.ex. Sätt ett lägre pris / Lägg till ytterliggare information.</li>
</ul>
',
];

?>