<?php

return [
	'preo_subject' => 'KÖPORDEN',
	'preo_greetings' => 'Tack så mycket för att du har lita på oss, vi har korrekt fått din inköpsorder och en av våra konsulter kommer att kontakta dig inom följande 24 timmar, sedan en kopia av din förfrågan:',
	'preo_full_gallery' => 'Se hela galleriet',
	'machine_detail' => 'Detaljer om maskinen',
	'go_original_ad' => 'Gå till den ursprungliga annonsen',
	'machine_price' => 'Maskinpriset',
	'additional_services' => 'Ytterligare tjänster',
	'tech_report' => 'Teknisk rapport',
	'paid_on_request' => 'Redan betalat vid registrering av orden',
	'machine_insurance' => 'Maskinförsäkring',
	'transport' => 'Transport',
	'destination' => 'Destination',
	'important_note' => 'Viktig information',
	'price_in_final_destiny' => 'Slutpris',
	'report_order' => 'Om du inte har begärt denna information kan du rapportera det här meddelandet genom att klicka på följande länk:',
];

?>