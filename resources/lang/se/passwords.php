<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Su contraseña debe tener al menos 6 caracteres y coincidir la confirmación.',
    'reset' => 'Su contraseña ha sido reseteada!',
    'sent' => 'Hemos enviado a su correo un enlace para resetear su contraseña',
    'token' => 'Token de reseteado inválido. ',
    'user' => "No podemos encontrar un usuario con esta cuenta de correo.",

];
