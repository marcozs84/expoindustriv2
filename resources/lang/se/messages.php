<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'e_mail' => 'Email',
    'password' => 'Lösenord',
	'password_confirmation' => 'Bekräfta lösenord',
    'firstname' => 'Förnamn',
    'lastname' => 'Efternamn',
    'telephone' => 'Telefon',
	'country' => 'Land',
	'select_country' => 'Välj ditt land',
	// ---------- layout/default
	'my_orders' => 'Mina ordrar',
	'my_account' => 'Mitt konto',
	'logout' => 'Logga ut',
	// ---------- TOP MENU
	'import_export' => 'Import & Export',
	'home' => 'Hem',
	'about_us' => 'Om oss',
	'payment_methods' => 'Tjänster',
	'questions' => 'Frågor',
	'contact_us' => 'Kundtjänst', //'Kontakta oss',
	'contact_us2' => 'Kontakta oss',
	// ---------- Lenguajes
	'spanish' => 'Spanska',
	'english' => 'Engelska',
	'swedish' => 'Svenska',
	// ---------- loginForm
	'welcome' => 'Välkommen',
	'log_in' => 'Logga in',
	'register' => 'Registrera dig',
	'create_account' => 'Skapa ett konto ',
	'forgot_password' => 'Har du glömt lösenordet?',
	'remember_me' => 'Påminn mig',
	'send' => 'Skicka',
	'have_account' => 'Jag har redan ett konto',
	// ---------- CONFIRM CUENTA
	'confirm_cuenta' => 'Kontobekräftelse',
//	'questions' => 'Preguntas',
	// ---------- CONTACT FORM
//	'contact_us' => 'Contact Us',
	'contact_form' => 'Kontaktformulär',
//	'firstname' => '',
	'contact_form_info' => 'Vill du sälja eller köpa, fråga en av våra experter.', // 'Vårt internationella team har många års erfarenhet inom industri branschen, så vi vet väldigt bra vad vi gör. Gör din förfrågan!',
	'subject' => 'Ämne',
	'message' => 'Meddelande',
	'send_message' => 'Skicka meddelande',
	'our_contacts' => 'Våra kontakter ',
	'social_networks' => 'Sociala medier',
	// ---------- SEARCH TERMS
	'search_by_category' => 'Sök efter kategori',
	'transportation' => 'Transport',
	'equipments' => 'Entreprenad',
	'industrial' => 'Industri',
	'agriculture' => 'Lantbruk',
	'forklifts' => 'Truck',
	'others' => 'Övrigt',
	'prefered_brands' => 'Utvalda varumärken',
	// ---------- COMMON LAYOUT DEFAULT - FOOTER
	'site_map' => 'Tips', 'Sidokarta',
	'information_for' => 'Tips till :subject',
	'announcers' => 'Säljaren',
	'buyers' => 'Köpare',
	'information_for_announcers' => 'Tips till <b>Säljaren</b>',
	'information_for_buyers' => 'Information till <b>Köpare</b>',
	'frequent_questions' => 'Vanliga frågor',
//	'payment_methods' => 'Métodos de pago',
	'legal_terms_conditions' => 'Allmänna villkor',
	'privacy_policy' => 'Integritetspolicy',
//	'about_us_footer' => 'Expoindustri.com är en annonsportal mellan Europa och Sydamerika. Här kan du hitta maskiner och utrustning i de olika kategorierna, både nya och begagnade. Vi har mer än 10 års erfarenhet i bagaget inom import och export mellan dessa två kontinenter.',
	'about_us_footer' => '<b>Expoindustri.com</b> är en ny  marknadsplats för företag i Europa som vill köpa eller sälja så enkelt och smidigt som möjligt. <br><br> Bakom webbportalen finns ett team med mer än 20 års erfarenhet som gör allt arbete åt dig, från att hämta maskinen var som helst i Europa och leverera till destinationen, inklusive Sydamerika.',
	'central_office' => 'Centralkontor',
	'phone' => 'Telefon',

	'contact_form_sent' => 'Meddelande skickat',
	'contact_form_sent_info' => 'Tack så mycket för din kontakt, en representant för vårt team kommer att kontakta dig snarast möjligt.',
	'sending' => 'skickar',
	'sent' => 'skickat',
	'services' => 'Tjänster',
	'our_offices_southamerica' => 'Våra kontor i Sydamerika',
	'find_your_machine' => 'Hitta din maskin',
	'search' => 'Sök',
	'order_by' => 'Sorterat på',
	'popular' => 'Populär',
	'new_arrival' => 'Nyankomna',
	'discount' => 'Rabatt',
	'price' => 'Pris',
	'items_found' => 'Vi hittade :number objekt',
	'all_f' => 'Alla',
	'all_m' => 'Alla',
	'select_category' => 'Välj kategori',
	'select_subcategory' => 'Välj underkategori',
	'criteria' => 'Kriterier',
	'category' => 'Kategori',
	'sub_category' => 'Underkategori',
	'brand' => 'Märke',
	'from' => 'Från',
	'to' => 'Till',
	'year' => 'År',
	'hi_tweet' => 'Hej!, kolla på den här maskinen.',
	'calculator' => 'Beräkna kostnader', //'Kalkylator',
	'additional_info' => 'Ytterligare information',
	'rating_reviews' => 'Rating & Recensioner',
	'add_favorites' => 'Lägg till i favoriter',
	'quote_final_destination' => 'Pris på slutdestination',
	'select_country_first' => 'Välj ditt land först',
	'select_preferred_zone' => 'Välj önskad zon',
	'publish' => 'Publicera',
	'model' => 'Modell',
	'next' => 'Nästa',
	'previous' => 'Tidigare',
	'subcategory' => 'Underkategori',
	'required' => 'Krävs',
	// ---------- currency
	'currency' => 'Valuta',
	'dollars' => 'Dollar',
	'euros' => 'Euro',
	'crowns' => 'Svenska krona',
	// ---------- status
	'active' => 'Aktiv',
	'inactive' => 'Inaktiv',
	'approved' => 'Godkänd',
	'rejected' => 'Ej godkänd',
	'awaiting_approval' => 'Väntar på godkännande',
	'reported' => 'Rapporterad',
	'draft' => 'Förslag',
	'removed' => 'Raderade',
	// ---------- CLIENT TYPES
	'client' => 'Kund',
	'provider' => 'Leverantör',
	'employee' => 'Anställd',
	'employer' => 'Arbetsgivare',


	'dz_upload' => 'Dra filer här för att ladda upp',
	'invalid_image_file' => 'Det är inte en giltig bildfil',
	'invalid_file' => 'Det är inte en giltig fil',
	'remove' => 'Radera',
	'machine_location' => 'Var befinner sig maskinen',
	'machine_location_descr' => '<a target="_blank" href=":route">(Se integritetspolicy)</a>', //'Den angivna adressen kommer att användas och är känd endast av ExpoIndustri  <a target="_blank" href=":route">(Se integritetspolicy)</a>',
	'data_protection_policy' => 'Integritetspolicy',
	'city' => 'Stad',
	'postal_code' => 'Postnummer',
	'location' => 'Plats',
	'address' => 'Adress',
	'findInMap' => 'Hitta i karta',
	'transport_info' => 'Transport information',
	'transport_info_descr' => 'Vänligen ge extra information för transportberäkning.',
	'longitude_mm' => 'Längd (mm)',
	'width_mm' => 'Bredd (mm)',
	'height_mm' => 'hög (mm)',
	'weight_brute' => 'Bruttovikt (kg)',
	'palletized' => 'Palletering',
	'confirm_removal' => 'Vill du ta bort den valda bilden?',
	'user_info' => 'Användarinformation',
	'user_info_descr' => 'Vem publicerar den här maskinen?',
	'select_your_category' => 'Välj en kategori',
	'select_your_category_descr' => 'Gruppera din maskin tillsammans med andra av samma typ.',
	'basic_information' => 'Grundläggande information',
	'basic_information_descr' => 'Märke och modell',
	'new_announce' => 'Din nya annons',
	'click_next' => 'Klicka på Nästa för att fortsätta.',
	'payment_options' => 'Betalningsmöjligheter',
	'payment_options_descr' => 'Hur vill du betala för din annons?',
	'payment_announce_message' => 'För att hålla din annons aktiv på vår webbplats erbjuder vi en daglig avgift på <span id="txt_price" style="font-weight:bold;">:price (Inkl. Moms)</span> 180 dagar minimum, välj hur många dagar du vill att din annons ska visas och fortsätt sedan med betalning på något av följande sätt.',
	'payment_transfer' => 'Överföring till bankkonto',
	'payment_transfer_descr' => 'Du kan överföra till följande konto:',
	'payment_creditcard' => 'Betalning med kreditkort',
	'payment_creditcard_descr' => 'Vänligen välj det bankkort som du vill betala.',
	'payment_faktura' => 'Fakturabetalning',
	'payment_faktura_descr' => 'När du registrerar din annons skickar vi dig en faktura med betalningsvillkor i 10 dagar.',
	'payment_cash' => '',
	'payment_cash_descr' => '',
	'days' => 'Dagar',
	'accept_terms' => 'Jag har läst och accepterat :link',
	'legal_terms' => 'Allmänna villkor', //'användarvillkor', //'Allmänna användarvillkor',
	'publish_pay' => 'Lägg in annonsen',
	'payment_trustly' => 'Betala med Trustly',
	'payment_trustly_descr' => 'Gör din betalning genom Trustly.',
	'verify_ccard' => 'Vänligen kontrollera dina kortuppgifter.',
	'announce_detail' => 'Annonsdetaljer',
	'announce_detail_descr' => 'I större detalj, större möjligheter att hitta köpare.',
	'provide_card_info' => 'Vänligen ange din kortinformation',
	'card_number' => 'Kortnummer',
	'expiration_date' => 'Giltighetsdatum',
	'security_code' => 'Säkerhetskod',
	'time_extended_title' => 'Förlängd annons',
	'time_extended_description' => 'Om man gör en kontrakt i mer än 100 dagar förnyas din annons automatisk var 50:e dag, som visas på vår hemsida, I avdelningen <b>"Senaste annonserna"</b>',
	'loading' => 'Loading',

	'info_buyers_title' => 'SÄKERHET, FÖR DIG SOM KUND', //'VIKTIGT! DU SOM KUND',
	'info_announcers_title' => 'SÄLJTIPS FÖR DIG, SOM ÄR EN STOR SÄLJARE', //'TIPS FÖR DIG SOM ANNONSÖR!',
	'no_brand' => 'Inget märke',
	'no_model' => 'Utan modell',
	'no_product' => 'Utan produkt',

	'product' => 'Produkt',
	'remove_favorite' => 'Ta bort från mina favoriter',
	'is_favorite' => 'Är favorit',
	'my_favorites' => 'Mina favoriter',
	'my_favorites.leftmessage' => 'Dina annonser markerade som favoriter.<br>Kom ihåg att alla priser är avsedd för dagens växelkurs',
	'my_announces' => 'Mina annonser',
	'my_announces.leftmessage' => 'Allt du behöver på ett ställe. Tveka inte på att kontakta oss om du behöver hjälp.',
	'no_active_users' => 'Det finns inga aktiva användare',
	'favorites.leftmessage' => '',
	'request_quote' => 'Begära offert', //'Skapa beställning',

	// Mensaje mostrado cuando un cliente envía una cotización desde la página de producto.
//	'quote_requested_correctly' => 'Din beställning skickades korrekt. En konsult kommer att kontakta dig så snart som möjligt.',
	'quote_requested_correctly' => 'Tack så mycket, vi har fått din order. En kopia har skickats till ditt e-postkonto och en konsult kommer att kontakta dig så snart som möjligt.',

	// Mensaje mostrado cuando un cliente envía exitosamente una nueva publicación a través del formulario Publish Here.
	'publish_success_msg' => 'Vårt team granskar din annons och publicerar den inom 2 timmar. Om din annons inte godkänns så kontaktar vi dig snarast för eventuell justering.',
	'publish_successful' => 'Lyckad publicering!',
	'price_in_europe' => 'Pris i Europa',
	'owner_price' => 'Ägarens pris',
	'exchange_rate' => 'Växelkurs',

	'includes_taxes' => 'Inkl. moms',
	'disabled' => 'Inaktiverad',
	'select_preferred_transport' => 'Du måste välja en transportmetod.',
	'publish_review_announce' => 'Inkomplett annons',
	'publish_missing_images' => 'Kom ihåg att du måste ange minst en bild för din annons.',
	'recent_announces' => 'Senaste annonserna',
	'be_first_visitor' => 'Bli den första som besöker dem.',

	'most_visited' => 'Mest besökta annonser.',
	'most_visited_small' => 'Kan vara av intresse för dig.',

	'company' => 'Företaget',
	'company_name' => 'Företagsnamn',
	'company_nit' => 'Organisationsnummer',

	'longitude' => 'Längd',
	'width' => 'Bredd',
	'height' => 'hög',

	'my_orders.leftmessage' => 'Förteckning över order som har gjorts på vår webbplats. ', //'Förteckning över order som har gjorts på vår webbplats. <br> Allt du behöver på ett ställe. Tveka inte på att kontakta oss om du behöver hjälp.',
	'order_detail' => 'Beställningsdetalj',
	'payment' => 'Betalning',
	'payments' => 'Betalningar',
	'description' => 'Beskrivning',
	'amount' => 'Belopp',
	'dimention' => 'Mått',
	'dimentions' => 'Mått',
	'weight' => 'Vikt',

	'order' => 'Beställningar',
	'orders' => 'Beställningar',
	'save' => 'Spara',
	'logo_background_color' => 'Bakgrundsfärg (logotyp)',
	'no_image_available' => 'Ingen bild tillgänglig',
	'urlSite' => 'Webbplats',
	'urlFacebook' => 'Facebook',
	'urlTwitter' => 'Twitter',
	'urlGooglePlus' => 'Google Plus',

	'search_alert' => 'Hittade du inte det du letade efter?', //'Hittade du inte din maskin?',
	'comments' => 'Meddelande',

	'marked_fields_required' => 'De markerade fälten är obligatoriska',
	'comments_sent' => 'Meddelande skickat',
	'su_consulta_enviada_correctamente' => 'Ditt meddelande skickades korrekt.',

	'search_alert_msg' => 'Kunde du inte hitta den maskin du behöver på vår hemsida? Oroa dig inte!! <br>ExpoIndustri -teamet finns där för att stödja dig med sina flera sökkanaler  för att hitta vilken typ av maskin på den Europeiska marknaden.',
	'search_alert_msgfrm' => 'Vänligen ange alla uppgifter här nedan och berätta vad din specifika förfrågan är. Inom kort kommer vi att kontakta dig',

	'our_services' => 'Våra tjänster',
	'our_services_slogan' => 'MYCKET MER ÄN EN E-HANDELSPORTAL <br> VÅRT TEAM ARBETAR AKTIVT FÖR DITT FÖRETAGS FRAMGÅNG', //'Mycket mer än en lösning!<br>Vi arbetar för ditt företags välstånd',
	'our_services_slogan_short' => 'VÅRT TEAM ARBETAR AKTIVT FÖR DITT FÖRETAGS FRAMGÅNG', //'Mycket mer än en lösning!<br>Vi arbetar för ditt företags välstånd',

	'about_expoindustri' => 'Om ExpoIndustri',

	'what_we_do' => 'VI ERBJUDER...', //'Vad vi erbjuder',
	'all_you_need' => 'Allt du behöver veta om <b>ExpoIndustri</b>. Vad <b>vi är</b> och vad vi kommer att bli',

	'publish_a_machine' => 'Publicera din produkt',
	'leave_your_data_we_will_contact_you' => 'Vänligen lämna era kontaktuppgifter så hör vi av oss inom kort.',
	'advertisement' => 'Annons',
	'look_advertise_options' => 'Titta våra olika annonsalternativ.',
	'make_click_here' => 'Klicka här',

	'my_account_msg' => 'Ändra en order, eller spåra din försändelse och uppdatera din personliga information.
<br><br>
Allt du behöver på ett och samma ställe. Tveka inte att kontakta oss  om du behöver hjälp.',

	'account_config' => 'Kontoinställningar',
	'saved_announces' => 'Favoriter', //'Sparade produkter (favoriter)',
	'udate_mail_password' => 'Uppdatera e-post och lösenord',
	'payments_finance' => 'Betalningar & Ekonomi',
	'list_enabled_cards' => 'Samla mina aktiverad kreditkort till nästa annons/köp ',
	'support' => 'Support',

	'top_message_unconfirmed_title' => 'Bekräfta ditt konto!',
	'top_message_unconfirmed_msg' => 'Vi har skickat ett e-mail för bekräftelse av kontot: <b>:email</b>. Vänligen bekräfta ditt konto inom 24 timmar. (Kolla gärna din skräppost då det kan ha hamnat där)',

	'continue' => 'Fortsätta',
	'account_confirmed' => 'Konto bekräftat',
	'account_confirmed_msg' => '
		Välkommen och tack för din bekräftelse!<br>
		Vi hoppas på att din upplevelse hos ExpoIndustri blir positiv.<br>
		Kontakta oss gärna vid frågor.<br><br>
		
		Mvh,<br>
		<strong>ExpoIndustri Team</strong>
	',

	'register_invalid_token' => 'Ogiltigt token',
	'register_invalid_token_msg' => 'Den angivna token är ogiltig, det är möjligt att ditt konto redan är aktivt eller har blivit inaktiverat av någon anledning, kontakta oss via vår <a href=":linkContact">kontaktformulär</a>.',
	'register_confirmation_error' => 'Fel vid kontobekräftelse',
	'register_confirmation_error_msg' => 'Ett fel har uppstått under bekräftelseprocessen, vårt team har fått anmälan och de kommer att kontakta dig så snart som möjligt.',

	'gallery' => 'Galleri',
	'time_extended_checkbox' => 'Placera din annons igen i toppositionerna den första i varje månad automatisk för endast :price kr.',
	'close' => 'Stäng',

	'payment_dibs' => 'Pago a través de DIBS', // YA NO SE USA
	'all_rights_reserved' => 'Alla rättigheter förbehålls.',
	'search_alert_title' => 'Hittade du inte din maskin?',
	'forgot_message_sent' => 'Meddelandet har skickas korrekt, kolla in din inkorg. Om det av någon anledning inte kommit till din inkorg, vänligen kolla in din \"Skräppost\".',
	'forgot_text' => 'Vänligen skriv in din email, vi skickar dig en länk för att uppdatera ditt lösenord',
	'user_not_found' => 'Användaren inte hittad',
	'password_reseted' => 'Ditt lösenord har uppdaterats korrekt och kommer nu att omdirigeras till ditt privata konto.  Om du inte omdirigeras automatiskt använd följande länk till ',
	'old_password_different' => 'Din aktuell lösenord matchar inte',
	'password_updated_succesfully' => 'Lösenordet uppdaterat korrekt',

	'current_password' => 'Aktuell lösenord',
	'new_password' => 'Ny lösenord',
	'password_update' => 'Uppdaterad lösenord',
	'current_email' => 'Nuvarande email',
	'update_email' => 'Uppdatera email',
	'update_email_info' => 'Kom ihåg, det här är ditt nuvarande användarnamn för att komma in på ditt konto.',
	'company_phone' => 'Telefonnummer', //'Ditt företags telefon',
	'sweden' => 'Sverige',

	'see_more' => 'Se mer',
	'process_flow' => 'Processflöde',
	'process_flow_content' => 'Lära sig steg för steg i logistikprocessen.',
	'duty_brokers' => 'Tullmäklare',
	'duty_brokers_content' => 'För att få den hjälp som behövs vid din tullklarering.',

	'aboutus_slogan' => 'MYCKET MER ÄN EN E-HANDELSPORTAL', //Vem vi är! Vår historia och mycket mer',
	'goodfaith' => 'Jag intygar att maskinen som jag publicerar är fri från skulder eller åtaganden med tredje part.',

	'index_process_flow_content' => 'Titta gärna här, steg för steg hela processen', //Att lära steg för steg hela processen i Sydamerika.',

	'index_brokers_title' => 'Tullmäklare',
	'index_brokers_content' => 'För att få den hjälp som behövs vid din tullklarering.',

	'index_security_title' => 'Säkra transaktioner',
	'index_security_content' => 'För din egen trygghet hanteras dina kort och betalningar via <a href="https://www.stripe.com" target="_blank">Stripe.com</a>.',

	'register_report_token_invalid' => 'Den angivna token är ogiltig, kontakta oss via vår',
	'register_report_user_confirmed_title' => 'Användaren tidigare aktiverad ',
	'register_report_user_already_confirmed' => 'Din användare har redan bekräftats innan, om du vill avsluta ditt konto, vänligen gör din förfrågan via vår :contact_form',
	'register_report_unsubscribe_title' => 'Identitetsstöld rapporterades framgångsrikt',
	'register_report_unsubscribe' => 'Tack så mycket för att du anmälde detta abonnemang, det här meddelandet har skickats 
till en representant för vårt team som kommer att avregistrera det här email.
<br><br>
Hälsningar,<br>
<b>Expoindustri team</b>',
	'disabled_account' => 'Konto inaktiverat',
	'register_report_disabled_account_mail' => 'Tack så mycket för att rapportera detta abonnemang, det här kontot har redan inaktiverats i vårt system.
<br><br>
Hälsningar,<br>
<b>ExpoIndustri team</b>',

	'delete_message_not' => 'Registret kunde inte raderas.Raderad :count of :total',
	'service_request_mistype' => 'Frågan måste vara en av följande typer: banner, search, inspection, support, istore.',
	'record_created' => 'Registret skapad',
	'added_favorites' => 'Läggas till i favoriter',
	'preorder_reported' => 'förbeställning rapporterad',
	'preorder_reported_message' => 'Tack så mycket för att rapportera denna publikation, en av våra konsulter har blivit anmäld.',
	'you_might_also_like' => 'Du kanske också är intresserad',
	'related_products_category' => 'Relaterade produkter av samma kategori',

	'quote_equipment_in_final_destination' => 'Beräkning på maskin i slutdestination',
	'calculator_text' => '<p>Med vår praktiska räknesystem kan du lätt räkna ut det slutgiltiga priset på din produkt. Från beställning till valda tillägg som ni gjort och till slutdestination.                  
	</p><p>Vid frågor vad snäll och ta kontakt med oss på en gång genom vår :form_link.</p>',

	'price_machine_in_europe' => 'Pris på maskin', //'Pris på maskin i Europa',
	'require_additional_info' => 'Vill ha kompletterande information?',
	'technichal_information' => 'Teknisk information',
	'request_require_payment' => 'Detta tillval kräver förskottsbetalning',
	'require_extra_assurance' => 'Tilläggsförsäkring för er maskin till slutdestination?',
	'extra_insurance' => 'Tilläggsförsäkring',
	'how_transport_machine' => 'Vilket fraktalternativ vill ni välja?',
	'soon_at_expoindustri' => 'Kommer snart på <br><b>ExpoIndustri.com</b>',
	'select_final_destination' => 'Välj den slutliga destinationen',
	'price_in_final_destiny' => 'Slutpris efter leverans',
	'how_guarantee_buy' => 'Certifierad återförsäljare', //'Hur garanterar du ditt köp?',
	'guarantee_content' => '<p>
		Allt förvärv av maskiner via ExpoIndustri stöds av en faktura, enligt vår allmänna villkor 
	</p>
	<p>
		När du skickat begäran via vår sida kontrollerar vi att maskinen fortfarande är tillgänglig. En gång kontrollerad och bekräftat skickar vi en  preliminär faktura (pre-faktura)  och de steg som behöver följas för att fortsätta med säker betalning.
	</p>
	<ul class="fa-ul">
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			ExpoIndustri garanterar köpet
		</li>
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			100% säker transport
		</li>
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			Support av högt utbildad personal
		</li>
	</ul>',
	'is_secure_payment' => 'Säkra betalningar!', //'Är det säkert att göra betalningen?',
	'is_secure_payment_content' => '<p>
	ExpoIndustri arbetar med de mest kända företagen inom finans, för att trygga era betalningar
	</p>
	<p>
	Därför ExpoIndustri erbjuder dig:
	</p>
	<ul class="fa-ul">
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Enkel banköverföring
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Betala i din komfortzon
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Debit/kreditkort
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		PayPal
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Stripe
	</li>
	</ul>',

	'usd' => 'USD',
	'eur' => 'EUR',
	'sek' => 'SEK',

	'precios_not_match' => 'Priserna matchar inte?',
	'precios_not_match_msg' => 'Kom ihåg att priserna beräknas utifrån den nuvarande växelkursen.',

	'validity' => 'Giltighet',
	'visits' => 'Besök',
	'state' => 'Tillstånd',
	'publication_date' => 'Publiceringsdatum',
	'doubts_contact_us' => 'Tvivel? Kontakta oss',
	'go_to_contact' => 'Skriv till oss här!', //'Gå till kontaktformuläret',
	'public_profile' => 'Offentlig profil',
	'profile_info' => 'Allmän information',
	'public_profile_msg' => 'Beskrivning av ditt företag som kommer att vara synligt i din offentliga profil.',

	'password_msg' => 'Kom ihåg att ditt lösenord måste ha minst 6 tecken. Om du har problem med att komma ihåg ditt lösenord, kontakta en av våra konsulter, de svarar gärna på din begäran.',
	'all_you_need_one_place' => 'Allt du behöver på ett ställe. Tveka inte att kontakta oss om du behöver hjälp.',
	'my_credit_debit_cards' => 'Mina  debi/kreditkort',
	'my_credit_debit_cards_msg' => 'Nyligen använda kort, du måste ha minst 1 registrerat kort.',
	'technical_info_company' => '<b>ExpoIndustri.com</b> använder ett externt företag. Erfarna inom området på tekniska inspektioner. Som håller en hög nivå med välutbildad personal som erbjuder våra kunder en väldetaljerad rapport. läs mer om 
		<a href="https://www.mevas.eu/" target="_blank">www.mevas.eu</a>',
	'insurande_for_machine' => 'Försäkring för maskin',
	'publish_a_machine_soon' => 'Uppladdning av annonser kommer att finnas tillgänglig fran och med 14 november 2018.',

	'provider_profile' => 'Leverantörsprofil',
	'announcements_provider' => 'Annonser som publicerats av denna leverantör',

	'office_name' => 'Kontornamn',
	'company_address' => 'Ange mer information ',
	'preview_link' => 'Om du vill förhandsgranska din profil, <a href=":link" class="btn btn-warning" style="font-weight:bold;">klicka här</a>',
	'company_information' => 'Företagsinformation',
	'company_name_required' => 'Företagsnamn krävs',
	'company_nit_required' => 'Organisationsnummer krävs',
	'select_main_office_location' => 'Här kan du ange adress, kontaktperson, e-mail', //'Vänligen ange adressen till ditt huvudkontor och klicka sedan på <b>“Sök”</b> för att lokalisera den på kartan, om den inte hittas försök lägga till mer information eller klicka på den exakta platsen direkt på kartan.',
	'add_secondary_addresses' => 'Lägg till adress',
	'filial_name' => 'Filial',
	'suggested_dimention' => 'De föreslagna dimensionerna är: <br>550px x 100px.',
	'announce_here' => 'Publicera här',
	'go_to_invoice' => 'Gå in och titta på ditt betalningskvitto',
	'invoice' => 'Faktura',
	'wanna_remove_announcement' => 'Vill du verkligen ta bort?',
	'announcement_removed' => 'Din annons har tagits bort',
	'internal_error_title' => 'Oj!',
	'internal_error_message' => 'Ett fel har uppstått i vårt system och det har rapporterats till vårt team. Vänligen skicka dina kommentarer till info@expoindustri.com, vi svarar gärna dina frågor.',
	'charges_technical_report' => 'Avgifterna för den tekniska inspektionen avräknar vid beställningen.',
	'handling_fee' => 'Hanteringskostnad', //'Driftkostnader',
	'handling_fee_description' => 'Nödvändiga kostnader som uppkommer för att kunna utföra tjänsten i sin helhet för aktiviteter som förberedelse av dokumentation, import- exportprocess, lastning och frakt, leverans, med mera.',
	'how_we_do_it' => 'Hur gör vi det?',
	'use_different_card' => 'Använd ett nytt kort.',
	'availability' => 'Disponibel', //'Tillgänglighet',
	'create_announce' => 'Skapa ny annons',
	'price_southamerica_only' => 'Om du enbart vill att priset blir synligt i Sydamerika. kryssa här.',
	'contact_consultant' => 'Kontakta en konsult',
	'product_contact_title' => 'Be om mer information',
	'product_contact_message' => 'Jag skulle vilja få mer information relaterad till produkt:<br><h2 style="font-size:25px;">:product_name</h2>',
	'price_not_available' => 'Be om offert', //'Pris ej tillgängligt',
	'news' => 'Affärsmöjligheter', //'Nyheter',
	'news_source' => 'Källa',
	'ver_noticia' => 'Läs hela artikeln',

	'active_subscriptions' => 'Aktiva planer',
	'renew' => 'Förnya',
	'requires_action' => 'Kräver åtgärd',
	'due_date' => 'Utgångsdatum',
	'view_all_your_plans' => 'Se alla planer',
	'overdue' => 'Utgånget', //'Fördröjning',
	'no_active_plans' => 'Du har inga aktiva planer',

	'our_price' => 'Företagspaket', //'Planer', //'Våra planer',
	'single_announcement_post' => 'En maskin per annons',
	'thirty_announcements' => 'Paket med 30 annonser',
	'unlimited_announcements' => 'Obegränsade annonser',

	'time_defined_user' => 'Period definierad av användaren',
	'gallery_unlimited_images' => 'Galleri med obegränsade bilder',
	'detailed_machine_description' => 'Digital marknadsföring i Europa och Sydamerika', //'Detaljerad beskrivning i annonsen',
	'one_year_availability' => '1 års prenumeration', //'1 år tillgänglig',
	'post_an_announcement' => 'Publicera en annons',
	'customizable_online_store' => 'Anpassningsbar onlinebutik',
	'link_to_your_site' => 'Länka till din hemsida',
	'post_logo_address' => 'Lägg upp ditt företagsnamn, logo, adress',

	'plan_basic' => 'Basic',
	'plan_business' => 'Business',
	'plan_plus' => 'Premium',

	'payment_per_announcement' => 'Betalning per annons',
	'per_year' => 'Per år',
	'buy_now' => 'Köp nu',
	'best_prices' => 'De bästa marknadspriserna, <br>utnyttja och göra dina maskiner synliga på den latinamerikanska marknaden.',

	'pricing' => 'Företagspaket', //'Planer', //'Våra planer', //'Prissättning',
	'minimum_price_required' => 'Ge oss ditt lägsta försäljningspris', //'Minimipris krävs',
	'minimum_price_text' => 'Minimipriset kommer endast att synas för våra SÄLJARE i förhandlings ändamål.',
	'per_180days' => '180 dagar',
	'free_banner_bonus' => '1 Banner reklam', //'Bannerreklam på expoindustri.com',
	'faq_buyers' => 'VANLIGA FRÅGOR FRÅN KÖPARE',
	'faq_sellers' => 'VANLIGA FRÅGOR FRÅN SÄLJARE',
	'taxes' => 'moms',
	'product_name' => 'Produkt namn',
	'pay_with_card' => 'Kortbetalning',
	'total' => 'Totalt',
	'payment_faktura_note' => 'Inom kort skickar vi fakturan till din faktureringsadress.',
	'announcement' => 'Annonser',
	'announcements' => 'Annonser',
	'edit' => 'Redigera',
	'choose_plan_business_plus' => 'Välj ditt plan <br><b>BUSINESS ELLER PLUS</b>',
	'automatic_translations' => 'Översättning till engelska, svenska och spanska',  // 'Automatisk översättning till engelska, svenska och spanska',
	'our_price_slogan' => 'Med våra planer får du ett team som jobbar för dig',
	'welcome_to' => 'Välkommen till',
	'categories' => 'kategorier',
	'reach_your_clients' => 'Med vår effektiva marknadsföring kan du hitta dina kunder i Europa och Sydamerika', //'Nå tusentals kunder i Europa och Sydamerika med dina maskiner. Inklusive all logistik.',
	'start_selling' => 'Börja sälja',
//	'expo_description' => '<b>Expoindustri.se</b> är en  e-handelsportal som gör det möjligt för företag att sälja sina maskiner, transportfordon eller tillbehör på ett mer effektivt sätt. Vi arbetar hårt för att öka säljarens försäljningsmöjligheter i Europa och Sydamerika och erbjuder den mest fördelaktig logistik- och distributionstjänst till slutkunden.',
	'expo_description' => 'Med över 20 år i branschen är Expoindustri en referenspunkt genom att underlätta och driva igenom affärer mellan små och medelstora företag inom maskinförsäljning i Europa och Sydamerika – Vi erbjuder våra kunder expertkunskap, vägledning och stöd under hela export/importprocessen och vi har över 3M potentiella köpare i vårt nätverk!  Vi har ett erfaret internationellt team som aktivt arbetar för att hjälpa just ditt företag att sälja era maskiner. <br><br> Vi erbjuder en personlig service med specialiserade sökningar i kombination med en effektiv marknadsföring så att du kan du hitta dina kunder i Europa och Sydamerika.',
	'our_philosophy' => 'Vår filosofi',
	'quote_joakim' => 'En nöjd kund är det som driver oss till att prestera vårt bästa',
	'our_experience' => 'Vår erfarenhet',
	'euro_south_market' => 'Europeiska och sydamerikanska marknaden',
	'logistics' => 'Logistik',
	'import_export_process' => 'Import / Exportprocess',
	'experienced_team' => 'Erfaren internationell team',
	'find_your_solution' => 'Hitta Expoindustri-lösningen för ditt företag',
	'sale_3_languages' => 'Sälj på 3 språk',
	'many_currencies' => 'Välj vilken valuta du vill sälja i',
	'sale_overseas' => 'Sätt ditt försäljningspris anpassat för dem olika  marknader du vill nå ut till ',
	'logistic_distribution' => 'Logistik och distribution',
	'partners' =>  'Partners', //'Våra partners',
	'just_single_announce' => 'Har du bara en maskin att sälja?',
	'our_categories' => 'Kategorier', //'Våra kategorier',
	'contact_doubts' => 'Har du  några frågor om hur kan vi hjälpa dig?',
	'contact_doubts_body' => 'Vi hjälper dagligen våra kunder med försäljning eller inköp av maskiner, export-import processen, logistik support, tekniska inspektioner med mera. Ring oss eller kom förbi kontoret på Jönköpingsvägen 27 i Tenhult, Jönköping. Du hittar alla våra kontaktuppgifter nedan. <br>Välkommen!',
	'managing_director' => 'Development manager',
	'expand_your_business' => 'Utöka ditt företag',
	'view_explained_video' => 'Titta på video',
	'experienced_salesman' => 'Personlig marknadsföring',
	'distrib_office' => 'Kontor & Lager',
	'terms' => 'Villkor',
	'where_pick_machine' => '',
	'wish_to_pick_it' => 'Jag ordnar transport',
	'provide_final_destination' => 'Ange destinationsadress',
	'wanna_make_offer' => 'Jag vill göra ett erbjudande',
	'offer' => 'Erbjudande',
	'wanna_expand_business' => 'Vill du expandera ditt företag?',
	'please_accept_terms' => 'Jag godkänner  Allmänna villkor och Integritetspolicy',

	'paysuc_thankyou' => 'Tack för din beställning!',
	'paysuc_transtatus' => 'Transaktionsstatus',
	'paysuc_succesful' => 'Lyckad',
	'paysuc_referenceno' => 'Referensnummer',
	'paysuc_datetime' => 'Transaktionsdatum och -tid',
	'paysuc_order' => 'Order',
	'paysuc_payment' => 'Betalningsbelopp',
	'paysuc_detail' => 'Transaktion har följande detaljer:',
	'paysuc_needassitance' => 'Om du behöver hjälp kan du kontakta oss via telefonnummer:  <b>+46 072 393 3700</b>',
	'factdet_fillyourinfo' => 'Fyll i med information för fakturering',
	'factdet_factaddress' => 'Faktureringsadress',
	'go_myprofile' => 'Uppdatera din profil',
	'go_myaccount' => 'Mitt konto',
	'place' => 'Ort',
	'status' => 'Status',
	'acc_serv_sales' => 'Vi hittar köpare till din maskin!',
	'acc_serv_sales_body' => 'Publiceras i mer än 40 länder runtom Europa och Sydamerika', //'Dina maskiner publiceras i mer än 40 länder runt om i Europa och Sydamerika!',
//	'acc_serv_sales_body2' => 'Med vår Försäljningsteam informerar vi proaktivt. <br>Våra globala nätverk sträcker sig till mer än +10 miljoner potentiella köpare.',
	'acc_serv_sales_body2' => 'Vårt försäljningsteam arbetar aktivt för att matcha köpare till era maskiner',
	'acc_serv_sales2' => 'Är du redo för merförsäljning?',
	'acc_serv_sales2_title' => 'Exportera annonser från din webbplats',
	'acc_serv_sales2_body' => 'Vi är glada att kopiera listor från din webbplats kostnadsfritt. Informationen uppdateras regelbundet och automatiskt.',
	'acc_serv_sales3_title' => 'Föredrar du att annonsera individuellt?',
	'acc_serv_sales3_body' => 'Med vårt system för att fylla i annonser har det aldrig varit lättare! Börja NU.',
	'goto_store' => 'Besök butik',
	'see_more_desc' => 'Om företaget', //acerca de empresa
	'hide_more' => 'Dölj',
	'write_new_password' => 'Skriv ditt nya lösenord',
	'descriptionCompany' => 'Beskrivning av ditt företag',
	'service_requirement' => 'Serviceförfrågan',
	'service_requirement_autoimport' => 'Det är nödvändigt att en av våra konsulter kontaktar dig för att få mer information, ge oss adressen till din webbsida och eventuella frågor eller problem i textrutan.',
	'transport_sea' => 'Sjötransport',
	'transport_terrain' => 'Landtransport',
	'price_southamerica' => 'Pris för Sydamerika',
	'price_europa' => 'Pris för Europa',
	'exclude_moms' => 'exkl. Moms',
	'price_europe_message' => 'Pris tillgängligt för kunder i Europa',
	'make_an_offer' => 'Gör ett bud',
	'welcome_to_landing' => 'Vi hjälper dig att skapa fler globala affärer',
	'saved_succesfully' => 'Sus cambios fueron guardados',
	'remember_review' => 'Su anuncio será revisado y habilitado en breve',
	'nosotros_saluda_title' => 'Säg hej till våra medarbetare',
	'nosotros_saluda_body' => 'Här hittar du personerna som gör Expoindustri till Sveriges och Sydamerikas främsta konsult inom export/import av maskiner. Vi är ett erfaret och internationellt team som hjälper just ditt företag att hitta rätt köpare. Vi erbjuder en personlig service i kombination med en effektiv  marknadsföring som leder till en snabb, lönsam och säker affär. Tillsammans hjälps vi åt att lösa problem och svara på frågor om export-import av maskiner, logistikprocess, tekniska inspektioner och allt ditt företag behöver.',
	'cargo_joakim' => 'VD',
	'cargo_emily' => 'Försäljningschef',
	'cargo_claudia' => 'Redovisningsekonom Sverige/Sydamerika',
	'cargo_carla' => 'Administrativ assistent Sydamerika',
	'cargo_ernesto' => 'Säljare Sydamerika',
	'cargo_iracema' => 'Redovisningsekonom Sydamerika',
	'cargo_jurgen' => 'Logistik & Distributionschef Sydamerika',
	'cargo_marco' => 'IT - Chef',
	'cargo_jose' => 'Säljare Sydamerika',
	'contact_details' => 'Kontaktuppgifter',
	'contact_details_body' => '<b>E-post:</b> info@expoindustri.com<br>
<b>Telefon:</b> 072 393 37 00<br>
<br>
<b>EXPOINDUSTRI SWEDEN AB</b><br>
<b>Org.nr:</b> 559174-5194',
	'find_here' => 'Hitta hit',
	'hours_available' => 'Öppettider',
	'hours_list' => '<b>Telefon</b><br>
Mån – Fre: 08:30 – 17:00<br>
<b>Chatt</b><br>
Mån – Fre: 08:00 – 18:00<br>
<b>Kontor</b><br>
Mån – Tor: 08:30 – 16:00<br>',
	'wanna_contact_us' => 'Vill du komma i kontakt med oss? Fyll i formuläret nedan så återkommer vi så fort som möjligt.',
	'contact_disclaimer' => 'Genom att skicka dina personuppgifter samtycker du till att vi behandlar dem i enlighet med vår :link.',
	'privacy_politics' => 'integritetspolicy',
	'date' => 'Datum',
	'interested' => 'Intresserad',
	'zone' => 'Ort',
	'newsletter_description' => 'ÄR DU INTRESSERAD AV ATT FÅ UPPDATERINGAR OM DE PROJEKT, BRANSCHER & MASKINER SOM VI EFTERFRÅGAR I EUROPA & SYDAMERIKA - PRENUMERERA PÅ VÅRT NYHETSBREV!',
	'business_opportunity_southamerica' => 'Affärsmöjligheter i Sydamerika',
	'have_stock_machine' => 'Har du ett lager av maskiner som du vill sälja i Sydamerika?',
	'machine_storage' => 'Maskinlager',
	'send_offer' => 'Skicka din offert',
	'request_final_price' => 'Begär pris inklusive transport',
	'request_final_price_body' => 'En säljare skickar ett offert till dig inom 24 timmar.',
	'offer_body' => 'Lägg ett bud så kommer en säljare att kontakta dig inom kort.',
	'need_more_info' => 'Vill du ha mer information?',
	'need_more_info_body' => 'Kontakta oss direkt. Öppet 6 dagar i veckan. Vi pratar ditt språk!',
	'request' => 'Begär Nu!',
	'view_all' => 'Se alla',
	'name' => 'Namn',
	'summary' => 'Sammanfattning',
	'price_at_origin' => 'Ursprungspris: (exkl. transport)',
	'price_fixed_by_consultant' => 'Pris som fastställts av konsulten',
	'tags' => 'Taggar',
	'machines' => 'Maskiner',
	'accesories' => 'Tillbehör',
	'both' => 'Både',
	'machines_accesories' => 'Maskiner och tillbehör?',
	'used_new' => 'Ny och begagnad?',
	'new' => 'Ny',
	'used' => 'Begagnade',
	'type' => 'Typ',
	'view_more_details' => 'Visa mer information',
	'units' => 'Enheter',
	'write_to_us' => 'Skriv till oss',
	'whatsapp_more_info' => 'Jag vill ha mer information om den här maskinen',
	'multiple_prices' => 'Flera priser',
	'attachments' => 'Bilagor',
	'expo_description_sales' => 'A lo largo de los años, hemos ayudado a nuestros clientes de habla hispana, a aumentar su productividad, a través de la búsqueda inteligente en Europa, de maquinaria nueva o usada, para entregársela directamente en el destino que se nos indique, sin que esto signifique ningún tiempo, ni esfuerzo extra para el cliente, pero que SI es sinónimo de optimización, calidad, mejor tecnología y precio competitivo.',
	'quote_joakim_sales' => 'Expoindustri es un jugador más en su equipo de trabajo. Existimos para negociar y gestionar tiempos, calidad y precios, a favor de su empresa',
];
