<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	'accepted'             => 'Fältet :attribute måste accepteras.',
	'active_url'           => 'Fältet :attribute är inte en giltig webbadress.',
	'after'                => 'Fältet :attribute måste vara ett datum efter :date.',
	'after_or_equal'       => 'Fältet :attribute måste vara ett datum senare eller lika med :date.',
	'alpha'                => 'Fältet :attribute bör bara innehålla bokstäver.',
	'alpha_dash'           => 'Fältet :attribute bör bara innehålla bokstäver,siffror och bindestreck.',
	'alpha_num'            => 'Fältet :attribute bör bara innehålla bokstäver och siffror.',
	'array'                => 'Fältet :måste vara en matris.',
	'before'               => 'Fältet :attribute måste vara ett datum före :date.',
	'before_or_equal'      => 'Fältet :attribute måste vara ett datum före eller lika med :date.',
	'between'              => [
		'numeric' => 'Fältet :attribute måste vara ett tal mellan :min och :max.',
		'file'    => 'Filen :attribute måste ha mellan :min och :max kilobytes.',
		'string'  => 'Texten :attribute måste ha mellan :min och :max tecken.',
		'array'   => 'Anordningen :attribute måste ha mellan :min och :max element.',
	],
	'boolean'              => 'Fältet :attribute måste vara falskt eller sant.',
	'confirmed'            => 'Bekräftelsefältet :attribute matchar inte.',
	'date'                 => 'Fältet :attribute är inte ett giltigt datum.',
	'date_format'          => 'Datumet :attribute matchar inte formatet :format.',
	'different'            => 'Fältet :attribute och :other måste vara olika.',
	'digits'               => 'Fältet :attribute måste ha :digits siffror.',
	'digits_between'       => 'Fältet :attribute måste vara mellan :min och :max siffror.',
	'dimensions'           => 'Den :attribute har ogiltiga bilddimensioner.',
	'distinct'             => 'Fältet :attribute har ett duplikatvärde.',
	'email'                => 'Fältet :attribute måste vara ett giltigt mail.',
	'exists'               => 'Den valda :attribute är ogiltig.',
	'file'                 => ' :attribute måste vara en fil .',
	'filled'               => ' :attribute fältet måste ha ett värde.',
	'image'                => ' :attribute måste vara en bild.',
	'in'                   => 'Den valda :attribute är ogiltig.',
	'in_array'             => ' :attribute fältet existerar inte i :other.',
	'integer'              => ' :attribute måste vara ett heltal.',
	'ip'                   => ' :attribute måste vara en giltig ip-adress.',
	'ipv4'                 => ' :attribute måste vara en giltig IPv4-adress.',
	'ipv6'                 => ' :attribute måste vara en giltig IPv6-adress.',
	'json'                 => ' :attribute måste vara en giltig JSON-sträng.',
	'max'                  => [
		'numeric' => ' :attribute får inte vara större än :max.',
		'file'    => ' :attribute får inte vara större än :max kilobytes.',
		'string'  => ' :attribute får inte vara större än :max tecken.',
		'array'   => ' :attribute får inte ha mer än :max objekt.',
	],
	'mimes'                => ' :attribute kan vara en fil av type: :values.',
	'mimetypes'            => ' :attribute måste vara en fil av type: :values.',
	'min'                  => [
		'numeric' => ' :attribute måste vara minst :min.',
		'file'    => ' :attribute måste vara minst :min kilobytes.',
		'string'  => ' :attribute måste vara minst :min tecken.',
		'array'   => ' :attribute måste vara minst :min objekt.',
	],
	'not_in'               => 'Den valda :attribute är ogiltig.',
	'numeric'              => ' :attribute måste vara ett nummer.',
	'present'              => 'Fältet :attribute måste vara närvarande.',
	'regex'                => ' :attribute formatet är ogiltigt.',
	'required'             => 'Fältet är obligatoriskt!', //'Fältet :attribute är obligatoriskt.',
	'required_if'          => 'Fältet :attribute krävs när  :other är :value.',
	'required_unless'      => 'Fältet :attribute krävs om inte  :other är i :values.',
	'required_with'        => 'Fältet :attribute krävs när :values är present.',
	'required_with_all'    => 'Fältet :attribute är obligatoriskt när :values är närvarande.',
	'required_without'     => 'Fältet :attribute obligatoriskt när :values det inte närvarande.',
	'required_without_all' => 'Fältet :attribute krävs när ingen av :values är närvarande.',
	'same'                 => 'Fältet :attribute och :other måste matcha.',
	'size'                 => [
		'numeric' => ' :attribute måste vara :size.',
		'file'    => ' :attribute måste vara :size kilobytes.',
		'string'  => ' :attribute måste vara :size tecken.',
		'array'   => ' :attribute måste innehålla :size items.',
	],
	'string'               => ' :attribute måste vara en sträng.',
	'timezone'             => ' :attribute .',
	'unique'               => ' :attribute har redan tagits.',
	'uploaded'             => ' :attribute misslyckades med att ladda upp.',
	'url'                  => ' :attribute formatet är ogiltig.',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
		'email' => 'email',
		'password' => 'lösenord'
	],

];
