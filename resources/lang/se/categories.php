<?php

return [

	// ========================================== INDUSTRIAL ==========================================
	'categoria_1' => 'Expoindustri har olika alternativ för både nya och begagnade entreprenadmaskiner för att köpa. Med produkter från industriledande tillverkare som Caterpillar, Volvo, Doosan, Case, JCB och fler. I denna kategori kan våra kunder hitta från olika typer av entreprenadmaskiner till och med tillbehör och reservdelar, för alla typer av behov. Bläddra i relaterade kategorier.',

	// Tornos
	'categoria_2' => 'Expoindustri erbjuder svarvar för olika typer av arbete, vare sig det är metall, trä eller plast. Vi har svarvar i olika storlekar och med olika funktioner, i de mest erkända märkena på marknaden, till exempel Colchester, Boehringer, Masak, Tos, Rosenfors, Köping, Herbert. Om du har några frågor, tveka inte att kontakta oss!',
	// Fresas
	'categoria_3' => 'Expoindustri erbjuder fräsmaskiner för olika typer av arbete, vare sig det är i metall, trä eller plast. Vi har fräsmaskiner i olika storlekar och med olika funktionaliteter, på de mest erkända märkena på marknaden, till exempel Zayer, Correa, Huron, Knuth, Deckel, Köping. Om du har några frågor, tveka inte att kontakta oss!',
	// Cilindradoras
	'categoria_4' => 'Expoindustri erbjuder dig rundvalsar för olika typer av arbete. Vi har rundvalsar i olika storlekar och med olika funktioner, i de mest erkända märkena på marknaden, till exempel Roundo, Froriep, Ungerer. Om du har några frågor, tveka inte att kontakta oss!',
	// Plegadoras
	'categoria_5' => 'Expoindustri erbjuder dig pressmaskiner för olika typer av arbete. Vi har pressar i olika storlekar och med olika funktioner, i de mest erkända märkena på marknaden, till exempel Lagan, Mossini, Donewell, Guifil. Om du har några frågor, tveka inte att kontakta oss!',
	// Guillotinas
	'categoria_6' => 'Expoindustri erbjuder gradsaxar för olika typer av arbete. Vi har gradsaxar av olika storlekar och med olika funktionaliteter, på de mest erkända märkena på marknaden, till exempel Ursviken, Darley, Hoan, Haco. Om du har några frågor, tveka inte att kontakta oss!',
	// Rectificadoras
	'categoria_7' => 'Expoindustri erbjuder slipmaskiner för olika typer av arbete. Vi har slipmaskiner av olika storlekar och med olika funktionaliteter, på de mest erkända märkena på marknaden, till exempel Voumard, Hauser, Elliot, Mägerle, Studer.  Om du har några frågor, tveka inte att kontakta oss!',
	// Taladros
	'categoria_8' => 'Expoindustri erbjuder borrmaskiner för olika typer av arbete, vare sig det är metall, trä eller plast. Vi har borrmaskiner i olika storlekar och med olika funktioner, i de mest erkända märkena på marknaden, till exempel Arenco, Ogawa, Profila Stanko, Bergonzi, Webo, Foradia, Oerlikon Arboga . Om du har några frågor, tveka inte att kontakta oss!',
	// Soldadoras
	'categoria_9' => 'Expoindustri erbjuder svetsar för olika typer av arbete. Vi har svetsar av olika storlekar och med olika funktionaliteter, på de mest erkända märkena på marknaden, till exempel Esab, Kemppi, Mig Mag, Oerlikon, lincoln.  Om du har några frågor, tveka inte att kontakta oss!',
	// Herramientas
	'categoria_10' => 'Expoindustri erbjuder verktyg för olika typer av arbete. Vi har verktyg av olika storlekar och med olika funktionaliteter, på de mest erkända märkena på marknaden, till exempel Sandvik coromant, Vertex. Om du har några frågor, tveka inte att kontakta oss!',


	// ========================================== TRANSPORTE ==========================================
	'categoria_11' => 'Expoindustri har olika alternativ för både nya och begagnade transportfordon för att köpa. Med produkter från ledande tillverkare i branschen, som Volvo, Scania, Mercedes, Renault, Iveco och fler. I denna kategori kan våra kunder hitta, från transportfordon till och tillbehör, för alla typer av behov. Bläddra i relaterade kategorier.',

	// "Camiones > 3,5ton"
	'categoria_12' => 'Expoindustri erbjuder lastbilar för olika typer av arbete. I denna underkategori kan ni hitta alla typer av lastbilar från 0 till 3,5 ton på de mest erkända märkena på marknaden, till exempel Volvo, Scania, Mercedes, Iveco, Daf. Om du har några frågor, tveka inte att kontakta oss!',
	// Camiones < 3ton
	'categoria_13' => 'Expoindustri erbjuder lastbilar för olika typer av arbete. I denna underkategori kan ni hitta alla typer av lastbilar över 3,5 ton på de mest erkända märkena på marknaden, till exempel Volvo, Scania, Mercedes, Iveco, Daf. Om du har några frågor, tveka inte att kontakta oss!',
	// Camion tractor
	'categoria_14' => 'Expoindustri erbjuder dragbilar för olika typer av arbete. I denna underkategori kan ni hitta alla typer av dragbilar på de mest erkända märkena på marknaden, till exempel Volvo, Scania, Mercedes, Iveco, Daf. Om du har några frågor, tveka inte att kontakta oss!',
	// Motores
	'categoria_15' => 'Expoindustri erbjuder dig lastbilsmotorer för olika typer av lastbilar. I denna underkategori kan du hitta alla typer av motorer för de mest erkända lastbilsvarumärkena på marknaden, till exempel Volvo, Scania, Mercedes, Iveco, Daf. Om du har några frågor, tveka inte att kontakta oss!',
	// Otros
	'categoria_16' => 'Expoindustri erbjuder dig alla typer av tillbehör och reservdelar till olika typer av lastbilar. I denna underkategori kan du hitta alla typer av tillbehör och reservdelar till lastbilar av de mest erkända märkena på marknaden, till exempel Volvo, Scania, Mercedes, Iveco, Daf. Om du har några frågor, tveka inte att kontakta oss!',


	// ========================================== EQUIPO ==========================================
	'categoria_17' => 'Expoindustri har olika alternativ för både nya och begagnade entreprenadmaskiner för att köpa. Med produkter från industriledande tillverkare som Caterpillar, Volvo, Doosan, Case, JCB och fler. I denna kategori kan våra kunder hitta från olika typer av entreprenadmaskiner till och med tillbehör och reservdelar, för alla typer av behov. Bläddra i relaterade kategorier.',

	// Palas frontales
	'categoria_18' => 'Expoindustri erbjuder dig hjullastare för olika typer av arbete. I denna underkategori kan du hitta alla typer av hjullastare på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Case, Ljungby maskin, Jcb. Om du har några frågor, tveka inte att kontakta oss!',
	// Excavadoras
	'categoria_19' => 'Expoindustri erbjuder dig grävmaskiner för olika typer av arbete. I denna underkategori kan du hitta alla typer av grävmaskiner på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere.. Om du har några frågor, tveka inte att kontakta oss!',
	// Orugas
	'categoria_20' => 'Expoindustri erbjuder dig bandschaktare för olika typer av arbete. I denna underkategori kan du hitta alla typer av bandschaktare på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. Om du har några frågor, tveka inte att kontakta oss!',
	// Compactadoras
	'categoria_21' => 'Expoindustri erbjuder dig vältar för olika typer av arbete. I denna underkategori kan du hitta alla typer av vältar på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. Om du har några frågor, tveka inte att kontakta oss!',
	// Alza hombres
	'categoria_22' => 'Expoindustri erbjuder dig liftar för olika typer av arbete. I denna underkategori kan du hitta alla typer av liftar på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. Om du har några frågor, tveka inte att kontakta oss!',
	// Trituradoras
	'categoria_23' => 'Expoindustri erbjuder dig krossverk för olika typer av arbete. I denna underkategori kan du hitta alla typer av krossverk på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac Om du har några frågor, tveka inte att kontakta oss!',
	// Generadores
	'categoria_24' => 'Expoindustri erbjuder dig generatorer för olika typer av arbete. I denna underkategori kan du hitta alla typer av generatorer på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco. Om du har några frågor, tveka inte att kontakta oss!',
	// Compresoras
	'categoria_25' => 'Expoindustri erbjuder dig kompressorer för olika typer av arbete. I denna underkategori kan du hitta alla typer av kompressorer på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco. Om du har några frågor, tveka inte att kontakta oss!',
	// Otros
	'categoria_26' => 'Expoindustri erbjuder dig alla typer av tillbehör och reservdelar till olika typer av entreprenadmaskiner. I denna underkategori kan du hitta alla typer av tillbehör och reservdelar till entreprenadmaskiner av de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco. Om du har några frågor, tveka inte att kontakta oss!',


	// ========================================== AGRICOLA ==========================================
	'categoria_27' => 'Expoindustri har olika alternativ för både nya och begagnade lantbruksmaskiner för att köpa. Med produkter från industriledande tillverkare som New Holland, Masey Ferguson, Jhon Deere, fiat och fler. I denna kategori kan våra kunder hitta, från olika lantbruksmaskiner till och med tillbehör och reservdelar, för alla typer av behov. Bläddra i relaterade kategorier.',

	// Tractores
	'categoria_28' => 'Expoindustri erbjuder dig traktorer för olika typer av arbete. I denna underkategori kan du hitta alla typer av traktorer på de mest erkända märkena på marknaden, till exempel X Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. Om du har några frågor, tveka inte att kontakta oss!',
	// Cosechadoras agrícolas
	'categoria_29' => 'Expoindustri erbjuder dig tröskor för olika typer av arbete. I denna underkategori kan du hitta alla typer av tröskor på de mest erkända märkena på marknaden, till exempel Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. Om du har några frågor, tveka inte att kontakta oss!',
	// Máquina para Heno y forraje
	'categoria_30' => 'Expoindustri erbjuder dig vallskörd för olika typer av arbete. I denna underkategori kan du hitta alla typer av vallskörd på de mest erkända märkena på marknaden, till exempel Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. Om du har några frågor, tveka inte att kontakta oss!',
	// Maquinaria de labranza
	'categoria_31' => 'Expoindustri erbjuder dig jordbearbetning för olika typer av arbete. I denna underkategori kan du hitta alla typer av jordbearbetning på de mest erkända märkena på marknaden, till exempel Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. Om du har några frågor, tveka inte att kontakta oss!',
	// Otros
	'categoria_32' => 'Expoindustri erbjuder dig alla typer av tillbehör och reservdelar till olika typer av lantbruksmaskiner. I denna underkategori kan du hitta alla typer av tillbehör och reservdelar till lantbruksmaskiner av de mest erkända märkena på marknaden, till exempel Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. Om du har några frågor, tveka inte att kontakta oss!',


	// ========================================== MONTACARGA ==========================================
	'categoria_33' => 'Expoindustri har olika alternativ för både nya och begagnade truckar för att köpa. Med produkter från industriledande tillverkare som Cat, Toyota, Kalmar, Hyster och fler. I denna kategori kan våra kunder hitta från olika typer av truckar, för alla typer av behov. Bläddra i relaterade kategorier.',

	// Montacargas
	'categoria_34' => 'Expoindustri erbjuder dig truckar för olika typer av arbete. I denna underkategori kan du hitta alla typer av truckar på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac Om du har några frågor, tveka inte att kontakta oss!',
	// Gruas
	'categoria_35' => 'Expoindustri erbjuder dig kranar för olika typer av arbete. I denna underkategori kan du hitta alla typer av kranar på de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac Om du har några frågor, tveka inte att kontakta oss!',
	// Otros
	'categoria_36' => 'Expoindustri erbjuder dig alla typer av tillbehör och reservdelar till olika typer av truckar och kranar. I denna underkategori kan du hitta alla typer av tillbehör och reservdelar till truckar och kranar av de mest erkända märkena på marknaden, till exempel Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac Om du har några frågor, tveka inte att kontakta oss!',


	// ========================================== OTROS ==========================================
	'categoria_37' => 'I denna kategori kan våra kunder hitta olika typer av verktyg, tillbehör och reservdelar från de mest prestigefyllda märkena inom sitt område, både i nya och begagnade produkter. Bläddra i relaterade kategorier.',

	// Otros
	'categoria_38' => 'I denna kategori kan våra kunder hitta olika typer av verktyg, tillbehör och reservdelar från de mest prestigefyllda märkena inom sitt område, både i nya och begagnade produkter. Om du har några frågor, tveka inte att kontakta oss!',


];


