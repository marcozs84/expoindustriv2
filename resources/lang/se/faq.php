<?php

return [


	'q_1' => '1. Vad är Expoindustri?',
	'a_1' => 'Expoindustri är en kombination av en avancerad annonsportal och ett erfaret team, där vi hjälper företag att öka sin försäljning, på ett säkert, enkelt och effektivt sätt.',
	'q_2' => '2. Vilket företag ansvarar för www.expoindustri.se?',
	'a_2' => 'Alla expoindustri.se, www.expoindustri.com rättigheter och namn är egendom av Expoindustri Sweden AB, registrerad i Sverige med org. Nr 559174-5194',
	'q_3' => '3. Var ligger kontoren? Hur kan jag kontakta er?',
	'a_3' => 'Huvudkontor ligger i Tenhult, Sverige. Vi har även lager och kontor i Iquique Chile. Om ni vill kontakta oss, klicka gärna här.',


	'q_4' => '4. Vad är det för maskiner ni säljer?',
	'a_4' => 'Vi är en marknadsplats där du kan hitta från lastbilar, hjullastare, grävmaskiner, kranar, dumprar, truckar, kompressorer, elverk, traktorer, skördar men också verkstadsmaskiner så som svarvar, fräsar, kantmaskiner, gradsaxar, svetsar, borrmaskiner, CNC maskiner och fler. 
Vi jobbar med de 6 följande kategorier: Transport, Lantbruk, Entreprenad, Truck, Industri, Övrig.',
	'q_5' => '5. Hur fungerar processen att förvärva en maskin hos er?',
	'a_5' => 'Processen har 5 enkla steg:
a)	Steg 1: Köparen väljer en maskin via vår webbportal, eller kontaktar Expoindustri för att beskriva sin specifik maskin som han letar efter.  Vårt team letar efter maskinen och detaljerar informationen till köparen.
b)	Steg 2: Köparen gör sin beställning genom vårt försäljningsteam eller direkt på expoindustri.se
c)	Steg 3: Vi kontaktar säljaren av maskinen i Europa för att bekräfta om denna är fortfarande tillgänglig. 
d)	Steg 4: Om maskinen är tillgänglig skickar Expoindustri en faktura till köparen, fakturan måste vara betalt inom 24 timmar. 
e)	Steg 5: Expoindustri bekräftar inbetalningen och omgående ordnar vi hämtning, lastning, export (om detta gäller) och leverans till slutdestination som köparen valt. Köparen behöver inte lägga onödig tid att förvärva en maskin eller transportfordon utan att han kan lägga sin tid på det han kan, sin verksamhet.',


	'q_6' => '6. Vad inkluderar priset som visas i annonsen',
	'a_6' => 'Priset som visas i annonsen är själva maskinens pris. Genom att använda knapen ”Beräkna kostnader” kan du få det slutpriset som inkluderar de alternativ du väljer såsom teknisk inspektion, försäkring, transport, slutdestination.
Du har också möjlighet att kontakta oss och berätta vad är det för behov du har och vårt team kommer att förbereda och skicka en offert till de angivna kontaktuppgifterna.',
	'q_7' => '7. Är maskinerna i drift?',
	'a_7' => 'Säljaren är den som anger maskinens skick i sin annons, och det finns fem alternativ, Ny, operativ, operativ med defekter, haveri och vet ej. I händelse av att maskinen är operativ med defekter, haveri eller vet ej, rekommenderar vi att köparen ber om teknisk inspektion.',
	'q_8' => '8. Vad innebär teknisk inspektion?',
	'a_8' => 'Expoindustri erbjuder Teknisk inspektion. Med mycket noggrannhet går dem igenom maskinen tillstånd och du som kund får detta dokumenterad.',
	'q_9' => '9. Är det Expoindustri själva som gör det tekniska inspektionen?',
	'a_9' => 'Nej. Vi använder ett externt företag med många års erfarenhet av tekniska inspektioner av maskiner i allmänhet.',
	'q_10' => '10. Har teknisk inspektionen en extra kostnad?',
	'a_10' => 'Denna tilläggstjänst för teknisk inspektion har en extra kostnad, vilket visas i annonsen eller offerten.',
	'q_11' => '11. Måste jag betala hela kostnaden för maskinen innan jag får den tekniska rapporten?',
	'a_11' => 'Nej, om du begärde en teknisk inspektion via annonsen, genom att ”klicka” betala, kommer du enbart att betala summan för den själva maskininspektionen. Efter att ha mottagit den begärda tekniska rapporten kan du välja mellan att fortsätta med inköp av maskinen eller eliminera den från din inköpslista.',
	'q_12' => '12. Hur lång tid tar det innan jag kan få den dokumenterade tekniska inspektionen?',
	'a_12' => 'Rapporten skickas till ditt e-mail inom 5 och 10 arbetsdagar.',
	'q_13' => '13. Vad kostar den tekniska inspektionen?',
	'a_13' => 'Kostnaden för inspektionen varierar beroende på vilken typ av maskin eller transportfordon som ska inspekteras. Genom att inkludera alternativet TEKNISK INSPEKTION i din förfrågan får du automatiskt kostnaden för själva inspektionen.',
	'q_14' => '14. Kan jag hyra maskiner på Expoindustri.se?',
	'a_14' => 'Nej, vi hyr inte maskiner.',
	'q_15' => '15. Var finns maskinen?',
	'a_15' => 'Maskinen är belägen i olika platser i Europa, men du behöver inte oroa dig, eftersom Expoindustri ansvarar för 100% av logistik processen och dokumentation (om detta gäller) som behöver för maskinen eller transportfordon når direkt till den destination du väljer.',
	'q_16' => '16. Innehåller det angivna priset momsen?',
	'a_16' => 'Nej, det pris som anges på annonsen är exklusive moms och nationaliseringskostnader. Du kan kontakta oss om du vill veta mer.',
	'q_17' => '17. Hur kan jag veta om betalningen är säkert och jag får levererad min maskin?',
	'a_17' => 'Varje betalning stöds av ett kontrakt enligt våra juridiska villkor. När du skickar/skapar beställning via vår hemsida kontrollerar vi att maskinen fortfarande är tillgänglig, en gång detta är kontrollerad och bekräftat att maskinen finns skickar vi fakturan till dig. Om du har kontaktat oss via e-mail, telefon, möte, skickar vi en faktura som fungerar som avtal när du har bekräftat att du vill maskinen som beskriver i offerten vi skickade.',
	'q_18' => '18. Hur lång tid har jag på mig att betala maskinen?',
	'a_18' => 'När Expoindustri skickar fakturan har du 24 timmar att göra inbetalningen. (se betalningsalternativ i nästa fråga).',
	'q_19' => '19. Vilka betalningsmetoderna finns tillgängliga?',
	'a_19' => 'Expoindustri har banköverföring som betalningsmetod.',
	'q_20' => '20. Vad händer om jag inte betalar innan 24 timmar och betalar senare?',
	'a_20' => 'Om du inte betalar inom 24 timmar kan Expoindustri inte garantera maskinens existens. Om Expoindustri har fått inbetalningen efter 24 timmar kommer vi i första hand bekräfta  maskinens tillgänglighet. I ett positiv fall kommer Expoindustri gå vidare med affären i annat fall kommer Expoindustri betala tillbaka till köparen med avdrag för bankkostnader om detta gäller.',
	'q_21' => '21. Kan jag betala genom avbetalning?',
	'a_21' => 'Nej, du kan inte betala på avbetalning, se mer våra juridiska villkor.',
	'q_22' => '22. Kan jag betala på ditt lokala kontor?',
	'a_22' => 'Nej, du kan inte betala på våra lokala kontor, ta gärna kontakt med oss för ytterligare information.',
	'q_23' => '23. När maskinen är betalt vilken säkerhet har jag?',
	'a_23' => 'När vi ser betalningen, skickar vi en betalningsbekräftelse samt informationen om leveranstiden. Dessutom kommer vår kvalificerade personal, antingen hos vår filial eller centralkontor, att tillhandahålla det stöd och den information du begär.',
	'q_24' => '24. Erbjuder Expoindustri finnansering?',
	'a_24' => 'Vi erbjuder inte finansiering.',
	'q_25' => '25. Kan jag se var maskinen finns?',
	'a_25' => 'Det kan du göra om säljaren har uppgraderats sin plan hos expoindustri.se.',
	'q_26' => '26. Hur lång tid tar det för att få maskinen när maskinen har betalats?',
	'a_26' => 'När detta gäller Europa mellan 5 – 18 arbetsdagar.
När det gäller Sydamerika erbjuder vi 2 leveransformer, delad leverans eller förmånsleverans.
Om du väljer delad leverans är leveranstiden mellan 70 - 180 dagar och om du väljer förmånsleverans är leveranstiden mellan 65-100 dagar, räknat från den dag då du gjorde betalningen.',
	'q_27' => '27. Kan jag lita på Expoindustri.se.?',
	'a_27' => 'Självklart. Teamet i Expoindustri Sweden AB har mer än 15 års erfarenhet av leverans och Export/Import mellan Europa och Sydamerika',



];


