<?php
return [
	'history' => 'Historia',
	'history_content' => 'Hur det började allt. ',

	'process_flow' => 'Processflöde', // esta en messages
	'process_flow_content' => 'Titta gärna vår video om processflöde.', // esta en messages

//	'duty_brokers' => 'Tullmäklare',
//	'duty_brokers_content' => 'För att få den hjälp som behövs vid din tullklarering.',

	'security' => 'Säkerhet',
	'security_content' => 'Din trygghet är det viktigaste, skydda din investering med oss.',

	'sustainability' => 'Hållbarhet',
	'sustainability_content' => 'Våra ansträngningar för nutid och framtid.',


	// --------------------------------------

	'history_popup_title' => 'Vår historia',
	'history_popup_content' => '
	<p>Expoindustri är en kombination av en avancerad webbportal och ett erfaret team som kommer att förenkla försäljningen av industriella -och entreprenadmaskiner mellan Europa och Latinamerika.</p> 
	<p>Hos oss kan du få olika typer av lösningar, från att hitta en partner för ditt företag i Latinamerika till att sälja / köpa maskiner direkt från vår webbportal.</p> 
	<p>Det är så enkelt som att ”köpa kläder online”, detta tack vare vår webbportal slutliga priskalkylatorn som beräknar tariffer, transport, logistik och alla relaterade kostnader, från Europa till den slut destination och är visad i det slutliga priset, samt vårt team bakom hela processen med mer än 10 års erfarenhet som gör allt hårt arbete för att eliminera alla komplikationer som exportprocessen innebär samt ger tryggheten för leverantören och kunden. </p>
	',

	'process_flow_popup_title' => 'Processflöde',
	'process_flow_popup_content' => '', // video

	'security_popup_title' => 'Trygghet',
	'security_popup_content' => '
<p>För Expoindustri så är kundens trygghet det allra viktigast. Därför är vårt största fokus att:</p>
<ul class="fa-ul">
<li><span class="fa-li"><i class="fa fa-check"></i></span>Att kontrollera den faktiska existensen av det produkter köparen önskar köpa.</li>
<li><span class="fa-li"><i class="fa fa-check"></i></span>Att göra tekniska inspektioner och upplagan rapport av produkt, om så önskas av köpare. (se formulär)</li>
<li><span class="fa-li"><i class="fa fa-check"></i></span>Att se till att all transport, både på land och till havs, genomförs noggrant och av kompetent personal. Så att Exportindustri kan leverera produkten i ursprungsskick.</li>
<li><span class="fa-li"><i class="fa fa-check"></i></span>Att systematiskt hålla köparen informerad om hela köpprocessen.</li>
<li><span class="fa-li"><i class="fa fa-check"></i></span>Att se till att produkten levereras på angivet leveransdatum.</li>
<li><span class="fa-li"><i class="fa fa-check"></i></span>Att ha kontor och utbildad personal tillgänglig i aktuella länder. (Se vårat kontor)</li>
<li><span class="fa-li"><i class="fa fa-check"></i></span>Att försäkra produkten genom till valda försäkringen, om så önskas av köpare</li>
</ul>
',

	'sustainability_popup_title' => 'Hållbarhet',
	'sustainability_popup_content' => '
<p>På Expoindustri riktar vi vår energi så att alla, både i Europa och i Sydamerika, ska vara nöjda när det kommer till kvalité, priser och effektivitet i tjänster.</p>
<p>Vårt <strong>UPPDRAG</strong> är att underlätta för våra kunder när de ska sälja/köpa mellan dessa två kontinenter. Att göra processen effektiv.</p>
<p>Vår <strong>VISION</strong> är att nå ut till fler länder ute i världen. Att underlätta den kommersiella industrin mellan länder. Vi vill att det ska vara lika enkelt för både företag som privatpersoner att handla.</p>
<p>Vår <strong>STRATEGI</strong> ligger i att erbjuda en trevlig arbetsmiljö så att vi kan erbjuda högkvalitativa tjänster.</p> 
<p>Vi skräddarsyr lösningar för den enskilde kunden. På detta sätt blir vi mer effektiva och produktiva samtidigt som vi jobbar mot vår vision.</p>
',
	'broker_popup_content' => 'Avtalet vi har med följande tullföretag kommer att underlätta processen för tullklarering av din maskin.
<br><br>Kontakta närmaste byrå.',


];