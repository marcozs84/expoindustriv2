<?php

return [
	'title' => 'INFORMATION FOR DIG SOM KÖPARE', //'SÄKERHET, FÖR DIG SOM KUND',
	'slogan' => 'På ExpoIndustri arbetar vi konstant för att du, som kund, skall kunna göra trygga och säkra affärer. Det gör vi bland annat genom att.',
	'buyer_1' => 'Vår personal gör löpande kontroller av annonser och användare.',
	'buyer_2' => 'Vi kontrollerar noggrann alla annonser som kommer in,  innan dom Godkänner och lägger ut på Expoindustri.com',
	'buyer_3' => 'Vi utvecklar säkra alternativ för betalning. Kort, paypal eller banköverföring.',
	'buyer_4' => 'Vi erbjuder filialer i olika länder som ExpoIndustri.com erbjuder sina tjänster för en tryggare närvaro.',
	'buyer_5' => 'För att minska bedrägeri, så ber ALDRIG ExpoIndustri betala i kontanter eller ber om handpenning. Därför ska man ALLTID betala in i det utvalda betalingsmetoder som ExpoIndustri erbjuder. ',
	'buyer_6' => 'Vi anpassar regler för annonsering till hur omvärldens villkor och lagar förändras, och hur vi själva uppfattar att vissa varor eller tjänster är förenliga med hur vi vill att ExpoIndustri skall upplevas.',
	'buyer_7' => 'ExpoIndustri jobbar ständigt med olika utbildningar för last och säkerhet för att erbjuda våra kunder fri leveranskador till slutdestinationer.',
	'buyer_8' => 'När kunden skickar intressanmälan för en industrimaskin / entreprenad / eller annat / så kontrollerar ExpoIndustri Sweden AB noga existensen av den utvalda produkten, innan vi skickar fakturan. Därför kan du bara luta dig bakåt och känna dig trygg att handla hos ExpoIndustri.',
	'buyer_9' => 'Expoindustri.com erbjuder teknisk inspektion med utbildade personal för att ge större säkerhet när man vill förvärva en utrustning/maskin.',
];

?>