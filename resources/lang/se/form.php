<?php

return [

	'click_to_select' => 'Klicka för att välja',
	'choose_year' => 'Välj år',

	'status_new' => 'Ny',
	'status_operational' => 'Fungerande',
	'status_operationalWFaults' => 'Fungerande fel',
	'status_notOperational' => 'Haveri',
	'status_iDontKnow' => 'Vet ej',

	// Label translations
	// -----------------------------

	'precio' => 'Pris',

	'calificacion_general_del_vendedor' => 'Säljarens bedömning',
	'dimensiones_y_tamanos' => 'Dimensioner och storlek',
	'potencia_y_rendimiento' => 'Kraft och prestanda',
//	'chasis' => 'Chassi',
	'otra_informacion' => 'Annan information',
//	'pais_de_fabricacion' => 'Tillverkningsland',

	'unknown_year' => 'Okänt', //'Årsmodell okänt',
	'informacion_primaria' => 'Basinformation',
	'ano_de_fabricacion' => 'Tillverkningsår',
	'kilometraje' => 'Mätarställning',
	'estado' => 'Skick',
	'potencia_del_motor' => 'Motoreffekt',
	'configuracion_de_eje' => 'Axelkonfiguration',
	'transmision' => 'Växellåda',
	'informacion_adicional' => 'Ytterligare information',
	'calificacion_general' => 'Allmänt betyg',
	'ultima_revision_tecnica' => 'Senast besiktigad',
	'no_de_propietarios_anteriores' => 'Antal tidigare ägare',
	'color' => 'Färg',
	'volante' => 'Styrning',
	'cabina' => 'Hytt',
	'condicion_llantas_delanteras' => 'Framdäckens kondition (%)',
	'condicion_llantas_traseras' => 'Bakdäckens kondition (%)',
	'medida_de_transporte_largo_x_ancho_x_alto' => 'Transport dimensioner (Lång x bredd x hög)',
	'peso_bruto' => 'Bruttovikt',
	'capacidad_de_carga' => 'Tillåten lastvikt (kg)',
	'tipo_de_frenos' => 'Typ av bromsar',
	'marca_de_los_neumaticos' => 'Däckmärke',
	'volumen_del_tanque' => 'Tank volym',
	'dimensiones_internas_largo_x_ancho_x_alto' => 'Fordonets inre mått (Lång x bredd x hög)',
	'equipamiento_extra' => 'Extra utrustning',
	'numero_de_serie' => 'Serienummer',
	'motor_ambiental_emision' => 'Miljömotor',
	'tipo_motor' => 'Motortyp',
	'cilindrada_del_motor' => 'Motorvolym',
	'salida_de_torsion' => 'Torsionsstång',
	'tipo_de_suspension' => 'Typ av fjädring',
	'distancia_entre_ejes' => 'Hjulbas',
	'no_de_chasis' => 'Chassinummer',
	'equipamiento' => 'Utrustning',
	'tipo_de_tracto_camion' => 'Dragbil',
	'tornamesa_quinta_rueda' => 'Vändskivetyp',
	'tipo_de_carroceria' => 'Typ av chassi',
	'tipo_de_eje' => 'Axeltyp',
	'no_de_serie' => 'Serienummer',
	'horas_de_trabajo' => 'Drifttimmar',
	'accesorios' => 'Tillbehör',
	'pintura_original' => 'Orginalfärg',
	'certificacion_ce' => 'CE-märkning',
	'pais_de_fabricacion' => 'Tillverkningsland',
	'condicion_oruga_izquierda' => 'Vänster larv kondition (%)',
	'condicion_oruga_derecha' => 'Höger larv kondition (%)',
	'capacidad_balde' => 'Skopvolym',
	'ancho_de_trabajo' => 'Arbetsbredd',
	'ancho_de_banda' => 'Bandbredd',
	'max_capacidad_de_elevacion' => 'Maximal lyftkapacitet',
	'max_capacidad_de_empuje' => 'Maximal dragkraft',
	'motor' => 'Motor',
	'velocidad_maxima' => 'Topphastighet',
	'lubricacion_central' => 'Central smörjning',
	'flujo_del_hidraulico' => 'Hydraulisk flöde l/min',
	'ancho_de_carril' => 'Bandbredd',
	'longitud_de_palanca_y_draga' => 'Sticklängd',
	'tipo_de_cuchillas' => 'Bladtyp',
	'tipo_de_compactadora' => 'Typ av vibratorplattor',
	'condicion_del_tambor' => 'Trummans skick',
	'condicion_de_la_placa' => 'Plattans skick',
	'ancho_del_tambor' => 'Trummbredd',
	'peso_de_compactacion' => 'Kompakteringsvikt',
	'tipo_de_poder' => 'Kraft typ',
	'tipo_de_traccion' => 'Typ av dragkraft',
	'maximos_ocupantes' => 'Max antal personer',
	'altura_maxima_de_trabajo' => 'Max arbetshöjd',
	'largo_extension_plataforma' => 'Förlängningsplattform',
	'capacidad_de_levante' => 'Lyftkapacitet',
	'maxima_pendiente_de_traslado' => 'Max förflyttnings vinkel',
	'velocidad_de_desplazamiento' => 'Färdhastighet',
	'velocidad_de_subida_y_bajada' => 'Höj/sänk hastighet',
	'apertura_de_alimentacion' => 'Matarstorlek',
	'tipo_de_trituradora' => 'Typ av krossverk',
	'volumen_de_produccion' => 'Produktionsvolym',
	'nivel_de_funcionamiento' => 'Krossfunktion',
	'peso' => 'Vikt',
	'tamano_del_material' => 'Materialstorlek',
	'velocidad' => 'Hastighet',
	'potencia' => 'Effekt',
	'tipo_de_combustion' => 'Typ av förbränning',
	'chasis' => 'Chassi',
	'cilindros' => 'Cylindrar',
	'frecuencia' => 'Frekvens',
	'voltaje' => 'Spänning',
	'tipo_de_compresor' => 'Typ av kompressor',
	'caudal' => 'Luftflöde',
	'marca_del_motor' => 'Motor märke',
	'presion_de_trabajo' => 'Arbetstryck',
	'traccion' => 'Drivtyp',
	'tamano_de_llantas_delanteras' => 'Storlek på framdäck',
	'tamano_de_llantas_traseras' => 'Storlek på bakdäck',
	'toma_fuerza' => 'PTO',
	'anchura_de_trabajo' => 'Arbetsbredd',
	'configuracion_de_manejo' => 'Driftform',
	'carro_de_la_horquilla' => 'Gaffelvagn',
	'altura_maxima_de_elevacion' => 'Max Lyfthöjd',
	'longitud_de_unas' => 'Gaffellängd',
	'punto_de_carga' => 'Tyngdpunkts avstånd',
	'mastil_de_elevacion' => 'Lift mast',
	'tipo_de_neumatico' => 'Däcktyp',
	'marca_de_la_bateria' => 'Fabrikat batteri',
	'voltaje_de_la_bateria' => 'Batteriets spänning',
	'voltaje_del_cargador' => 'Laddspänning',
	'capacidad_de_la_bateria' => 'Batterikapacitet',
	'capacidad_max_de_levante' => 'Max. lyftkapacitet',
	'altura_de_elevacion' => 'Lyfthöjd',
	'longitud_de_pluma_principal' => 'Kranarmslängd',
	'extension_de_pluma_principal' => 'Kranarms förlängning',
	'longitud_maxima_del_sistema' => 'Maximal systemlängd',
	'contrapeso_maximo' => 'Maximal motvikt',
	'longitud_del_portador' => 'Bärarlängd',
	'maxima_pendiente' => 'Maximal lutning',
	'capacidad' => 'Kapacitet',
	'giro_de_cabina' => 'Svänghytt',
	'tipo' => 'Typ',
	'entre_punto' => 'Dubbavstånd',
	'volteo' => 'Sving över bädd',
	'husillo' => 'Spindelborrning',
	'bomba_de_refrigeracion' => 'Kylpump',
	'volteo_con_escote' => 'Sving över tvärslid',
	'volteo_sin_escote' => 'Sving i gap',
	'ancho_de_la_cama' => 'Bäddens bredd',
	'movimiento_cruz' => 'Toppslidsrörelse',
	'marca_del_visualizador' => 'Digital märke',
	'caja' => 'Växellåda',
	'velocidad_de_husillo' => 'Spindelvarvtal',
	'alimentacion' => 'Matning',
	'iso' => 'ISO',
	'movimiento_longitud_x' => 'Längdrörelse X',
	'movimiento_transversal_y' => 'Tvärrörelse Y',
	'movimiento_vertical_z' => 'Vertikalrörelse Z',
	'distancia_husillo_mesa' => 'Avstånd bord/spindel ',
	'cambiador_de_herramientas' => 'Verktygsväxlare',
	'sistema_de_programacion' => 'Programmeringssystem',
	'velocidad_del_husillo' => 'Spindelvarvtal',
	'alimentacion_x_y_z'  => 'Matning X/Y/Z',
	'marcha_rapida' => 'Snabbtransport',
	'tipo_de_alimentacion' => 'Kraft typ',
	'tipo_de_cilindradora' => 'Typ av valsmaskin',
	'capacidad_max_esposor' => 'Max. valskapacitet',
	'diametro_rodillo_superior' => 'Diameter övre valsen ',
	'diametro_rodillo_inferior' => 'Diameter nedre valsen',
	'potencia_de_motor' => 'Huvudmotor',
	'flujo_hidraulico' => 'Hydrauliskt flöde',
	'velocidad_de_laminacion' => 'Matning hastighet',
	'capacidad_max_de_plegado' => 'Maximal vikningskapacitet',
	'distancia_del_golpe' => 'Slaglängd',
	'distancia_entre_barras' => 'Avstånd mellan gavlar',
	'max_altura_de_apertura' => 'Max. öppningshöjd',
	'volumen_tanque_hidraulico' => 'Hydraulvolym',
	'capacidad_en_toneladas' => 'Presskraft',
	'industria' => 'Industri',
	'longitud_de_trabajo' => 'Arbetslängd',
	'max_corte' => 'Klippkapacitet',
	'flujo_de_hidraulico' => 'Hydrauliskt flöde',
	'flujo_de_hidraulico_en' => 'Hydrauliskt flöde',
	'dimensiones_mesa_lxa' => 'Bordets mått (LxA)',
	'cono_del_husillo' => 'Spindelkona',
	'movimiento_del_husillo' => 'Pinolrörelse',
	'capacidad_maxima_de_perforacion' => 'Borrkapacitet',
	'distancia_husillo_columna' => 'Avstånd spindel-pelare ',
	'amperaje' => 'Strömstyrka',
	'velocidad_de_alimentacion' => 'Spindelhastighet',
	'otros' => 'Övrigt',
	'otros1' => 'Övrigt',
	'otros2' => 'Övrigt',
	'otros3' => 'Övrigt',
	'otros4' => 'Övrigt',
	'otros5' => 'Övrigt',
	'volteo_transversal' => 'Volteo transversal',
	'reduccion_de_cubo' => 'Reducción de cubo',
	'avance_rapido_de_carro' => 'Snabbmatning',

	// Configuracion del Eje
	// -----------------------------

	'axis_4X2' => '4X2',
	'axis_4X4' => '4X4',
	'axis_4X4*2' => '4X4*2',
	'axis_4X4*4' => '4X4*4',
	'axis_6X2' => '6X2',
	'axis_6X2*4' => '6X2*4',
	'axis_6X2/4' => '6X2/4',
	'axis_6X4' => '6X4',
	'axis_6X6' => '6X6',
	'axis_6X6*2' => '6X6*2',
	'axis_8X2' => '8X2',
	'axis_8X2/4' => '8X2/4',
	'axis_8X2*4' => '8X2*4',
	'axis_8X2/6' => '8X2/6',
	'axis_8X2*6' => '8X2*6',
	'axis_8X4' => '8X4',
	'axis_8X4/4' => '8X4/4',
	'axis_8X4*4' => '8X4*4',
	'axis_8X6' => '8X6',
	'axis_8X6/4' => '8X6/4',
	'axis_8X8' => '8X8',
	'axis_8X8/4' => '8X8/4',
	'axis_10X2' => '10X2',
	'axis_10X4' => '10X4',
	'axis_10X6' => '10X6',
	'axis_10X6/4' => '10X6/4',
	'axis_10X8' => '10X8',
	'axis_10X10' => '10X10',

	// Calificacion General
	// -----------------------------
	
	'grating_veryGood' => 'Väldigt bra',
	'grating_good' => 'Bra',
	'grating_regular' => 'Medel',
	'grating_bad' => 'Dålig',
	
	'no_previous_owners' => 'Antal tidigare ägare',    // ********************
	'colour' => 'Color',    // ********************

	// Volante
	// -----------------------------

	'steering_wheel' => 'Volante',    // ********************
	'steering_left' => 'Vänster',
	'steering_right' => 'Höger',

	// Cabina
	// -----------------------------
	
	'cabin' => 'Cabina',    // ********************
	'cabin_dayCab' => 'Daghytt',
	'cabin_sleeperCab' => 'Sovhytt',
	'cabin_highSleeperCab' => 'XL Sovhytt',
	'cabin_crewCab' => 'Manskapshytt',
	'cabin_extendedCab' => 'Förlängd hytt',
	
	'transport_measure' => 'Medidas de transporte (Largo x ancho x alto)',    // ********************
	'weight_brute' => 'Bruttovikt',    // ********************
	'load_capacity' => 'Lastkapacitet',    // ********************

	// Tipo de frenos CAMIONES
	// -----------------------------
	
	'break_types' => 'Tipo de frenos',    // ********************

	'tpBreakCam_dontKnow' => 'Vet ej',
	'tpBreakCam_disk' => 'Skiv broms',
	'tpBreakCam_drum' => 'Trum broms',
	'tpBreakCam_other' => 'Andra',

	// Marca de las llantas
	// -----------------------------
	
	'tires_brand' => 'Marca de los neumaticos',    // ********************

	'tiresBrand_michelin' => 'Michelin',
	'tiresBrand_goodyear' => 'Goodyear',
	'tiresBrand_continental' => 'Continental',
	'tiresBrand_bridgestone' => 'Bridgestone',
	'tiresBrand_pirelli' => 'Pirelli',
	'tiresBrand_nokian' => 'Nokian',
	'tiresBrand_blacklion' => 'Blacklion',
	'tiresBrand_hankook' => 'Hankook',
	'tiresBrand_kumho Tires' => 'Kumho Tires',
	'tiresBrand_dunlop' => 'Dunlop',
	'tiresBrand_bFGoodrich' => 'BFGoodrich',
	'tiresBrand_yokohama' => 'Yokohama',
	'tiresBrand_others' => 'Andra',
	
	'fueltank_volume' => 'Tankvolymen',    // ********************
	
	'internal_measure' => 'Dimensiones internas (Largo x ancho x alto)', // *******************
	
	'tandem_axis_lifting' => 'Tandemaxellyft', // *************************
	'levantamiento_del_eje_tandem' => 'Tandemaxellyft', // *************************

	// Equipamiento extra CAMIONES
	// --------------------------------
	
	'extra_equipment' => 'Equipamiento extra', // ********************

	'xtraEquCam_CoffeeMaker' => 'Kaffebryggare',
	'xtraEquCam_refrigerator' => 'Kylskåp',
	'xtraEquCam_toolbox' => 'Verktygslåda',
	'xtraEquCam_warningLamp' => 'Varningsljus',
	'xtraEquCam_workLampOutside' => 'Arbetslampa ute',
	'xtraEquCam_foglamps' => 'Dimljus',
	'xtraEquCam_outerVisor' => 'Yttre visir',
	'xtraEquCam_bumperSpoiler' => 'Stötfångare spoiler',
	'xtraEquCam_frontsBumpers' => 'Extra stötfångare fram',
	'xtraEquCam_lowerLightBar' => 'Nedre ljusstången',
	'xtraEquCam_headlightProtector' => 'Skydd till Strålkastare',
	'xtraEquCam_mosquitoNetRadiator' => 'Myggnät radiator',
	'xtraEquCam_aluminumRims' => 'Aluminium fälgar',
	'xtraEquCam_mudflap' => 'Stänkskärm',
	'xtraEquCam_fenderFlap' => 'Fender flap',
	'xtraEquCam_extinguishers' => 'Brandsläckare ',
	'xtraEquCam_other' => 'Övrigt',
	
	'serial_no' => 'Número de serie', //*******************
	'ambiental_engine' => '',

	// Tornamesa TRACTO CAMION
	// --------------------------

	'tornaCam_fixed' => 'Fixerad',
	'tornaCam_adjustable' => 'Justerbar',

	// Tipo de TRACTO CAMION
	// ----------------------

	'tpTractoCam_standardTractorTrailer' => 'Standard dragbil/släp',
	'tpTractoCam_heavyLoad' => 'Tungt gods',
	'tpTractoCam_hazardousLoad' => 'Farligt gods',
	'tpTractoCam_volumeTrailer' => 'Volymtrailer',
	'tpTractoCam_other' => 'Andra',

	// Tipo de carroceria OTROS
	// --------------------------

	'tpCarr_euro' => 'Euro',
	'tpCarr_jumbo' => 'Jumbo',
	'tpCarr_mega' => 'Mega',
	'tpCarr_extendible' => 'Utbyggbar',
	'tpCarr_fixed' => 'Fixerad',
	'tpCarr_other' => 'Andra',

	// Tipo de eje OTROS
	// -----------------------

	'tpEje_1' => '1',
	'tpEje_1+1' => '1+1',
	'tpEje_1+2' => '1+2',
	'tpEje_1+3' => '1+3',
	'tpEje_2' => '2',
	'tpEje_2+1' => '2+1',
	'tpEje_2+2' => '2+2',
	'tpEje_2+3' => '2+3',
	'tpEje_3' => '3',
	'tpEje_4' => '4',

	// Motor Ambiental
	// ------------------------

	'motAmb_euro_0' => 'Euro 0',
	'motAmb_euro_1' => 'Euro 1',
	'motAmb_euro_2' => 'Euro 2',
	'motAmb_euro_3' => 'Euro 3',
	'motAmb_euro_4' => 'Euro 4',
	'motAmb_euro_5' => 'Euro 5',
	'motAmb_euro_6' => 'Euro 6',
	'motAmb_other' => 'Övrigt',

	// Tipo de Suspension
	// -------------------------

	'tpSusp_parabolicParabolic' => 'Parabel-Parabel',
	'tpSusp_parabolicLeaf' => 'Parabel-Blad',
	'tpSusp_parabolicAir' => 'Parabel-Luft',
	'tpSusp_leafParabolic' => 'Blad-Parabel',
	'tpSusp_leafLeaf' => 'Blad-Blad',
	'tpSusp_leafAir' => 'Blad-Luft',
	'tpSusp_airAir' => 'Luft-Luft',
	'tpSusp_trapezoid' => 'Trapets',

	// Accesorios PALAS CARGADORAS
	// ----------------------------

	'accePalas_Forks' => 'Pallgaflar',
	'accePalas_Mudguards' => 'Stänkskärmar',
	'accePalas_QuickCoupler' => 'Redskapsfäste',
	'accePalas_3rdHydraulicCircuit' => '3:e hydraulfunktion',
	'accePalas_4thHydraulicCircuit' => '4: hydraulfunktion',
	'accePalas_AutoLubricationSystem' => 'Centralsmörjning',
	'accePalas_Weighloader' => 'Vågsystem',
	'accePalas_MainSwitch' => 'Huvudströmbrytare',
	'accePalas_WarningLight' => 'Varningsljus',
	'accePalas_DieselHeater' => 'Dieselvärmare',
	'accePalas_JoystickKontrol' => 'Joystick Kontroll',
	'accePalas_AirConditioning' => 'Luftkonditionering',
	'accePalas_ComfortDriveControlCDC' => 'CDC (spakstyrning)',
	'accePalas_AdditionalHydraulics' => 'Ytterligare hydraulik',
	'accePalas_MachineControlSystem' => 'Grävsystem',
	'accePalas_Aps' => 'Aps',

	// ACCESORIOS ORUGA
	// ------------------------

	'acceOruga_EngineHeater' => 'Motorvärmare',
	'acceOruga_Winch' => 'Vinsch',
	'acceOruga_AirConditioning' => 'Luftkonditionering',
	'acceOruga_AdditionalHydraulics' => 'Ytterligare hydraulik',
	'acceOruga_MultiShankRipper' => 'Multi-Rivtand',
	'acceOruga_ShankRipper' => 'Rivtand',
	'acceOruga_MainSwitch' => 'Huvudströmbrytare',
	'acceOruga_WarningLight' => 'Varningsljus',

	// Accesorios EXCAVADORAS
	// -----------------------------

	'acceExcav_HammerHydraulic' => 'Hammar hydraulik',
	'acceExcav_ShearHydraulic' => 'Sax hydraulik',
	'acceExcav_QuickCoupler' => 'Redskapsfäste',
	'acceExcav_LineValveDipperStick' => 'Slangbrottsventil sticka',
	'acceExcav_LineValveBoomHoist' => 'Slangbrottsventil bom',
	'acceExcav_AutoLubricationSystem' => 'Centralsmörjning',
	'acceExcav_Rotator' => 'Rotator',
	'acceExcav_MainSwitch' => 'Huvudströmbrytare',
	'acceExcav_WarningLight' => 'Varningsljus',
	'acceExcav_DieselHeater' => 'Dieselvärmare',
	'acceExcav_JoystickKontrol' => 'Joystick Kontroll',
	'acceExcav_AirConditioning' => 'Luftkonditionering',
	'acceExcav_ExtraEquipment' => 'Extra utrustning',
	'acceExcav_AdditionalHydraulics' => 'Ytterligare hydraulik',
	'acceExcav_MachineControlSystem' => 'Grävsystem',
	'acceExcav_DepthControl' => 'Djupgrävningssystem',
	'acceExcav_AdjustableBoom' => 'Ställbar bom',
	'acceExcav_SteelTracks' => 'Stålband',
	'acceExcav_Weighloader' => 'Vågsystem',
	'acceExcav_EstándarDiggingBucket' => 'Standardskopa',
	'acceExcav_CableBucket' => 'Kabelskopa',
	'acceExcav_DitchingGradingBucket' => 'Planeringsskopa',
	'acceExcav_VDitchBucket' => 'Profilskopa',
	'acceExcav_RubberTracks' => 'Gummiband',

	// MOTOR AMBIENTAL EXCAVADORA |ORUGA|
	// -----------------------------------------

	'motorAmbExc_IDK' => 'Vet ej',
	'motorAmbExc_Stage_2' => 'Steg | |',
	'motorAmbExc_Stage_3a' => 'Steg | | | A',
	'motorAmbExc_Stage_3b' => 'Steg | | | B',
	'motorAmbExc_Stage_4' => 'Steg | V',
	'motorAmbExc_Stage_5' => 'Steg V',

	// CABINA |PALA CARGADORA |ORUGA|
	// ----------------------------------

	'cabina_palaOr_OpenCab' => 'Öppen hytt',
	'cabina_palaOr_EnclosedCab' => 'Sluten hytt',

	// Tipo de cuchillas | ORUGA
	// ---------------------------

	'tpCuchillaOru_IDK' => 'Vet ej',
	'tpCuchillaOru_SemiUBlade' => 'Halvsfäriskt blad',
	'tpCuchillaOru_UBlade' => 'Sfäriskt blad',
	'tpCuchillaOru_6WayBlade' => '6-Vägsblad',
	'tpCuchillaOru_StraightBlade' => 'Rakt blad',

	// Accesorios para COMPACTADORA
	// ------------------------------

	'acceCompac_mainSwitch' => 'Huvudströmbrytare',
	'acceCompac_warningLight' => 'Varningsljus',
	'acceCompac_extraEquipment' => 'Extra utrustning',
	'acceCompac_dieselHeater' => 'Dieselvärmare',
	'acceCompac_airConditioning' => 'Luftkonditionering',
	'acceCompac_additionalHydraulics' => 'Ytterligare hydraulik',

	// Tipo de  COMPACTADORA
	// ------------------------

	'tpCompact_SingleDrumRollers' => 'Envalsvältar',
	'tpCompact_TwinDrumRollers' => 'Tvåvalsvältar',
	'tpCompact_CombiRollers' => 'Kombivältar',
	'tpCompact_PneumaticTiredRollers' => 'Pneumatiska rullar',
	'tpCompact_TowedVibratoryRollers' => 'Bogserade vibratorvältar',
	'tpCompact_OtherRollers' => 'Andra vältar',
	'tpCompact_SoilCompactors' => 'Jordkompaktorer',
	'tpCompact_PlateCompactors' => 'Vibrator',
	'tpCompact_Tampers' => 'Stampar',
	'tpCompact_Other' => 'Övrigt',

	// Tipo de traccion ALZA HOMBRES
	// ----------------------------

	'tpTracAlza_2WD' => '2 WD',
	'tpTracAlza_4WD' => '4 WD',
	'tpTracAlza_Tracked' => 'Bandvagn',
	'tpTracAlza_Other' => 'Övrigt',

	// Tipo de poder ALZA HOMBRES
	// -----------------------------

	'tpPoderAlza_Electric' => 'Elektrisk',
	'tpPoderAlza_Diesel' => 'Diesel',
	'tpPoderAlza_LiquidGas' => 'Gasol',
	'tpPoderAlza_Gas' => 'Bensin',
	'tpPoderAlza_Hybrid' => 'Hybrid',

	'maxPend_5' => '5°',
	'maxPend_10' => '10°',
	'maxPend_15' => '15°',
	'maxPend_20' => '20°',
	'maxPend_25' => '25°',
	'maxPend_30' => '30°',
	'maxPend_35' => '35°',
	'maxPend_40' => '40°',
	'maxPend_45' => '45°',
	'maxPend_50' => '50°',
	'maxPend_55' => '55°',
	'maxPend_60' => '60°',
	'maxPend_65' => '65°',
	'maxPend_70' => '70°',
	'maxPend_75' => '75°',
	'maxPend_80' => '80°',
	'maxPend_85' => '85°',
	'maxPend_90' => '90°',

	// Tipo de trituradoras
	// ---------------------

	'tpTritur_JawCrusher' => 'Käftkross',
	'tpTritur_ConeCrusher' => 'Konkross',
	'tpTritur_BallCrusher' => 'Kulkross',
	'tpTritur_ImpactCrusher' => 'Stötkross',
	'tpTritur_RodCrusher' => 'Stångkross',
	'tpTritur_GyratoryCrusher' => 'Rotationskross',
	'tpTritur_RollCrusher' => 'Rullkross',
	'tpTritur_ShredderCrusher' => 'Valskross',
	'tpTritur_HammerCrusher' => 'Hammarkross',

	// Nivel de fincionamiento TRITURADORAS
	// ------------------------------------

	'nvFunTrit_Primary' => 'Primär',
	'nvFunTrit_Secondary' => 'Sekundär',
	'nvFunTrit_Tertiary' => 'Tertiär',
	'nvFunTrit_Quaternary' => 'Kvartär',
	'nvFunTrit_Other' => 'Övrigt',

	// Tipo de combustion GENERADOR
	// ------------------------------

	'tpCombus_Diesel' => 'Diesel',
	'tpCombus_LiquidGas' => 'Gasol',
	'tpCombus_Electric' => 'Elektrisk',

	// Caudal compresor
	// --------------------

	'caudComp_m3Min' => 'm3/min',
	'caudComp_lMin' => 'l/min',
	'caudComp_lSec' => 'l/sec',

// TIPO COMPRESOR
// ---------------------

	'tpCompres_PistonCompressor' => 'Kolvkompressor',
	'tpCompres_ScrewCompressor' => 'Skruvkompressor',
	'tpCompres_CentrifugeCompressor' => 'Centrifugkompressor',
	'tpCompres_Others' => 'Övrigt',

// CHASIS COMPRESOR
// ---------------------

	'chasisCompr_Static' => 'Statisk',
	'chasisCompr_Mobile' => 'Mobil',

// Cilindros TRACTOR
// -------------------

	'cilind_Trac_2st' => '2 st',
	'cilind_Trac_3st' => '3 st',
	'cilind_Trac_4st' => '4 st',
	'cilind_Trac_5st' => '5 st',
	'cilind_Trac_6st' => '6 st',
	'cilind_Trac_8st' => '8 st',

// toma fuerza PTO
// ---------------

	'tomaPower_540' => '540',
	'tomaPower_750' => '750',
	'tomaPower_1000' => '1000',
	'tomaPower_540_750' => '540/750',
	'tomaPower_540_540e' => '540/540e',
	'tomaPower_540_1000' => '540/1000',
	'tomaPower_540_750_1000' => '540/750/1000',
	'tomaPower_540_540e_1000' => '540/540e/1000',
	'tomaPower_540_750e_1000' => '540/750e/1000',
	'tomaPower_540_1000_1000e' => '540/1000/1000e',
	'tomaPower_540_750_1000_1400' => '540/750/1000/1400',
	'tomaPower_540_540e_1000_1000e' => '540/540e/1000/1000e',

// Accesorios TRACTOR
// ------------------------

	'acceTrac_NoAccessories' => 'Ej tillbehör',
	'acceTrac_FrontLoader' => 'Frontlastare',
	'acceTrac_FrontPTO' => 'Kraftuttag',
	'acceTrac_FrontLinkage' => 'Frontlyft',
	'acceTrac_ClutchlessForwardReverseShuttle' => 'Kopplingsfri fram/back',
	'acceTrac_TransmissionCVT' => 'Steglös transmission CVT',
	'acceTrac_TurbineClutch' => 'Turbin',
	'acceTrac_ReverseDriveSystem' => 'Vändbar förarplats',
	'acceTrac_FrontAxleSuspension' => 'Fjädrande framaxel',
	'acceTrac_AirConditioning' => 'Luftkonditionering',
	'acceTrac_ForestCab' => 'Skogshytt',
	'acceTrac_CabinSuspension' => 'Hytt upphängning',
	'acceTrac_GuidanceSystem' => 'Styrsystem',
	'acceTrac_AdBlue' => 'Ad blue',
	'acceTrac_AirBrakes' => 'Tryckluftbromsar',
	'acceTrac_CreeperGear' => 'Krypväxel',

// Cabina TRACTOR
// ------------------

	'cabinTrac_Open' => 'Öppen',
	'cabinTrac_Forest' => 'Skog',
	'cabinTrac_Regular' => 'Vanlig',
	'cabinTrac_Other' => 'Övrigt',

// Accesorios SEGADORAS
// -------------------

	'acceSega_NoAccessories' => 'Ej tillbehör',
	'acceSega_AirConditioning' => 'Luftkonditionering',
	'acceSega_GPS' => 'GPS',
	'acceSega_CornHead' => 'Majströska',
	'acceSega_GrainHead' => 'Spanmålströska',
	'acceSega_HeaderTrailer' => 'Trösktrailer',
	'acceSega_SideKnife' => 'Sidokniv',
	'acceSega_CropLifters' => 'Skördlyftare',
	'acceSega_CutterbarAutomaticLeveling' => 'Automatisk höjdjustering',
	'acceSega_AssistedSteeringSystem' => 'Assistans styrsystem',
	'acceSega_StrawChopper' => 'Halmhackare',
	'acceSega_ChaffSpreader' => 'Halmspridare',
	'acceSega_ElectricallyAdjustableSieves' => 'Elektrisk justerbara siktar',
	'acceSega_YieldAndMoistureMeasuring' => 'Avkastning  och fuktmätare',


// Configuracion de transmision ENFARFADORA
// --------------------------------------

	'cnfTransEnfar_Trailed' => 'Bogserad',
	'cnfTransEnfar_Mounted' => 'Monterad',

// Configuracion de manejo MONTACARGA
// -------------------------------

	'cnfManMont_Electric' => 'Elektrisk',
	'cnfManMont_Diesel' => 'Diesel',
	'cnfManMont_Gas' => 'Bensin',
	'cnfManMont_LiquidGas' => 'Gasol',
	'cnfManMont_Hybrid' => 'Hybrid',
	'cnfManMont_Manual' => 'Handdriven',
	'cnfManMont_Other' => 'Övrigt',

// Mastil de elevacion MONTAGARGA
// -------------------------------
	'mastElevMont_Simplex' => 'Simplex',
	'mastElevMont_Duplex' => 'Duplex',
	'mastElevMont_Triplex' => 'Triplex',
	'mastElevMont_Quad' => 'Quad',
	'mastElevMont_DuplexFreeLift' => 'Duplex/frilift',
	'mastElevMont_TriplexFreeLift' => 'Triplex/frilift',
	'mastElevMont_Telescopic' => 'Teleskopisk',
	'mastElevMont_Other' => 'Övrigt',



// Carro de la horquilla MONTACARGA
// ------------------------------

	'carrMont_NoCarriege' => 'Inget aggregat',
	'carrMont_Sideshifter' => 'Sidoföring',
	'carrMont_PositionerSpread' => 'Sidoföring och spridning',
	'carrMont_PositionerSpreadTilt' => 'Sidoföring,spridning,skevning',
	'carrMont_ForkSpreading' => 'Gaffelspridning',
	'carrMont_DemountableSystem' => 'Demonterbart system',
	'carrMont_MultiPalletCarriege' => 'Flerpallagregat',
	'carrMont_Clamp' => 'Klämaggregat',
	'carrMont_RollClamp' => 'Rullklämaggregat',
	'carrMont_PaperRollClamp' => 'Balklämaggregat',
	'carrMont_Rotator' => 'Rotator',
	'carrMont_PusherForks' => 'Utskjutgafflar',
	'carrMont_ReachForks' => 'Långa gafflar',
	'carrMont_Other' => 'Övrigt',



// Tipo de llantas MONTACARGA
// --------------------------

	'tpLlantaMont_Pneumatic' => 'Luftgummi',
	'tpLlantaMont_SolidPneumatic' => 'Fast- pneumatisk',
	'tpLlantaMont_SuperElastic' => 'Superfjädrin',
	'tpLlantaMont_Other' => 'Övrigt',


// Cabina MONTACARGA
// -------------------------

	'cabMont_OverheadGuard' => 'Förarskydd',
	'cabMont_ClosedCabin' => 'Hytt',
	'cabMont_NoCabin' => 'Saknas',


// Tipo de carroceria GRUA
// -----------------------

	'tpCarGrua_Tracked' => 'Bandvagn',
	'tpCarGrua_Wheeled' => 'Hjulvagn',

// Tipo  TORNOS
// -----------------
	'tpTorno_Tracked' => 'Supportsvarv',
	'tpTorno_CopyingLathe' => 'Kopieringsvarv',
	'tpTorno_TurretLathe' => 'Revolversvarv',
	'tpTorno_VerticalLathe' => 'Vertikalsvarv',
	'tpTorno_AutomaticLathe' => 'Automatsvarv',
	'tpTorno_CarouselLathe' => 'Karusellsvarv',
	'tpTorno_CNCLathe' => 'CNC svarv',
	'tpTorno_PlanLathe' => 'Plansvarv',
	'tpTorno_Other' => 'Övrigt',


// Accesorios para TORNO
// -------------------------

	'acceTorno_WithoutAccesories' => 'Utan tillbehör',
	'acceTorno_3JawChuck' => '3 backs chuck',
	'acceTorno_4JjawChuck' => '4 backs chuck',
	'acceTorno_FlatChuck' => 'Planskiva',
	'acceTorno_FixedSteady' => 'Fast stöddocka',
	'acceTorno_TravellingSteady' => 'Medlöpande stöddocka',
	'acceTorno_FastRevolvingCenter' => 'Fast/roterande dubb',
	'acceTorno_DrillChuck' => 'Borrhylsa',
	'acceTorno_QuickChangeTool' => 'Snabbstålfäste',
	'acceTorno_VeeHolders' => 'stålhållare',
	'acceTorno_Other' => 'Övrigt',



// Marca del visualizador TORNO
// --------------------------
	'branVisTorno_WithoutDigital' => 'Utan digital',
	'branVisTorno_Heidenhain' => 'Heidenhain',
	'branVisTorno_Mitutoyo' => 'Mitutoyo',
	'branVisTorno_Sony' => 'Sony',
	'branVisTorno_Easson' => 'Easson',
	'branVisTorno_Delos' => 'Delos',
	'branVisTorno_Other' => 'Övrigt',


// Tipo FRESADORA
// --------------------
	'tpFresa_BenchTypeMillingMachine' => 'Bänkfräs',
	'tpFresa_UniversalMillingMachine' => 'Universalfräs',
	'tpFresa_VerticalMillingMachine' => 'Vertikal fräs',
	'tpFresa_HorisontalMillingMachine' => 'Horisontal fräs',
	'tpFresa_BedMillingMachine' => 'Bäddfräs',
	'tpFresa_CNCMillingMachine' => 'CNC fräs',
	'tpFresa_CNCBedMillingMachine' => 'CNC bäddfräs',
	'tpFresa_CopyingMillineMachine' => 'Kopierfräs',



// Accesorios FRESA
// ------------------

	'acceFresa_WithoutAccesories' => 'Utan tillbehör',
	'acceFresa_MachineVice' => 'Maskinskruvstycke',
	'acceFresa_UniversalDivider' => 'Delningsapparat',
	'acceFresa_UniversalDividerFastCenter' => 'Delningsapparat - fastdubb',
	'acceFresa_RoundCoordinatTable' => 'Rund koordinat bord',
	'acceFresa_CoordinatTableDividing' => 'Rund Koordinat bord - delning',
	'acceFresa_CoordinatTable' => 'Koordinat bord',
	'acceFresa_Other' => 'Övrigt',

// ISO FRESA
// ----------------
	'isoFresa_ISO_01' => 'ISO 01',
	'isoFresa_ISO_10' => 'ISO 10',
	'isoFresa_ISO_20' => 'ISO 20',
	'isoFresa_ISO_30' => 'ISO 30',
	'isoFresa_ISO_40' => 'ISO 40',
	'isoFresa_ISO_50' => 'ISO 50',


// Bomba de refrigeracion FRESA
// ----------------------------
	'bombRefFresa_IDK' => 'Vet ej',
	'bombRefFresa_YesGoodCondition' => 'Ja - Bra skick',
	'bombRefFresa_YesBadCondition' => 'Ja - Dålig skick',
	'bombRefFresa_No' => 'Nej',

// Tipo cilindradora
// --------------------
	'tpCilin_Manual' => 'Manual',
	'tpCilin_Electric' => 'Elektrisk',
	'tpCilin_ElectricHydraulics' => 'Elektrisk - hydraulisk',
	'tpCilin_Hydraulics' => 'Hydraulisk',
	'tpCilin_Other' => 'Övrigt',

// Tipo de serie CILINDRADORA
// --------------------------
	'tpSerCilin_2RollVending' => '2 vallsmaskin',
	'tpSerCilin_3RollVending' => '3 valsmaskin',
	'tpSerCilin_4RollVending' => '4 valsmaskin',
	'tpSerCilin_AsymmetricalType' => 'Asymmetrisk vals',
	'tpSerCilin_Other' => 'Övrigt',

// Tipo PLEGADORA
// --------------------
	'tpPleg_ManualFoldning' => 'Manual kantpress',
	'tpPleg_MechanicalFoldingMachine' => 'Mekanisk kantpress',
	'tpPleg_HydraulicMechanicalFoldingMachine' => 'Hydraulisk-mekanisk kantpress',
	'tpPleg_HydraulicFoldingMachine' => 'Hydraulisk kantpress',
	'tpPleg_CNCFoldingMachine' => 'CNC- kantpress',
	'tpPleg_Other' => 'Övrigt',

// Tipo GUILLOTINA
// ---------------------
	'tpGuillo_MechanicalGuillotineShear' => 'Mekanisk gradsax',
	'tpGuillo_HydraulicGuillotineShear' => 'Hydraulisk gradsax',

// Tipo RECTIFICADORA
// --------------------
	'tpRect_AbrasiveBeltGrinder' => 'Bandslipmaskin',
	'tpRect_DrillgrindingMachine' => 'Borrslipmaskin',
	'tpRect_JigGrindingMachine' => 'Jiggslipmaskin',
	'tpRect_SwinFrameGrinder' => 'Pelarslipmaskin',
	'tpRect_FaceGrindingMachine' => 'Planslipmaskin',
	'tpRect_ProfileGrindingMachine' => 'Profilslipmaskin',
	'tpRect_GearedShapping' => 'Kipphyvel',
	'tpRect_Tool&CutterGrindingMachine' => 'Verktygsslipmaskin',
	'tpRect_CrankshaftLathe' => 'Vevaxelslipmaskin',
	'tpRect_Other' => 'Övrigt',

// Tipo TALADRO
// ----------------
	'tpTalad_2SpindleDrill' => '2-Spindlig',
	'tpTalad_CombinedDrillingMillingMachine' => 'Borr/fräsmaskin',
	'tpTalad_BenchDrill' => 'Bänkborrmaskin',
	'tpTalad_MultiSpindleDrill' => 'Flerspindlig',
	'tpTalad_JigDrillingMachine' => 'Jiggborrmaskin',
	'tpTalad_MagneticDrill' => 'Magnetborrmaskin',
	'tpTalad_RadialDrillingMachine' => 'Radialborrmaskin',
	'tpTalad_PillarDrillingMachine' => 'Pelarborrmaskin',
	'tpTalad_DeepHoleDrillingMachine' => 'Långhålsborrmaskin',
	'tpTalad_ExtractionDrill' => 'Kärnborrmaskin',
	'tpTalad_Other' => 'Övrigt',

// Tipo SOLDADORAS
// ----------------------
	'tpSolda_ConventionalWeldingMachine' => 'Konventionell svets',
	'tpSolda_StudWelding' => 'Bultsvets',
	'tpSolda_InvertWelding' => 'Invertsvets',
	'tpSolda_MigMag' => 'MIG/mag',
	'tpSolda_GasEngineWelding' => 'Motorsvets bensin',
	'tpSolda_DieselEngineWelding' => 'Motorsvets diesel',
	'tpSolda_Plasma' => 'Plasma',
	'tpSolda_PlasticWelding' => 'Plastsvets',
	'tpSolda_SpotWelding' => 'Punktsvets',
	'tpSolda_RobotWelding' => 'Robotsvets',
	'tpSolda_TIG' => 'TIG',
	'tpSolda_Other' => 'Övrigt',

	// Tipo BOOLEAN
	// ----------------------
	'yes' => 'Ja',
	'no' => 'Nej',
	'none' => 'Vet ej',

	// Tipo CABINA |PALA CARGADORA |ORUGA|
	// ----------------------
	'cabinPalaOr_OpenCab' => 'Öppen hytt',
	'cabinPalaOr_EnclosedCab' => 'Sluten hytt',

	'escote' => 'Gap',
	'mm' => 'mm',
	'pulgada' => 'Tum',
	'rosca' => 'Gänga',
	'digital' => 'Digital',
	'ventilador_de_refrigeracion' => 'Kylfläkt',
	'pinza' => 'Elektrodhållare',
	'cable_de_tierra' => 'Jordkabel',
	'con_alambre' => 'Med svetstråd',
	'medida_de_caudal' => 'Mätflöde',

	'contador_de_fardos' => 'Balräknare',

];
?>