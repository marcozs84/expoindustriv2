<?php

return [
	'title' => 'CONSEJOS PARA USTED COMO ANUNCIANTE!',
	'slogan' => 'Vender en ExpoIndustri.com es muy fácil y todos pueden hacerlo.',
	'title_1' => 'ESTABLECE UN PRECIO JUSTO.',
	'body_1' => '<b>Elegir el precio correcto es crucial, todo se puede vender al precio correcto.</b>
<ul>
	<li>Mire los precios de productos similares. También puede contactarnos. Tenemos muchos años de experiencia y estaremos encantados de ayudarle.</li>
	<li>Para ventas más rápidas, puede considerar colocar un precio más bajo.</li>
</ul>',
	'title_2' => 'TOMAR BUENAS FOTOS',
	'body_2' => '<p>Una imagen dice más que mil palabras.</p>
<ul>
	<li>
		Piense en tener un entorno limpio alrededor de su producto, para que el comprador se centre en lo que usted quiere vender. (ExpoIndustri.com puede rechazar su anuncio, si no se cumplen las condiciones de imagen.
		<a href="javascript:;">Consulte los Términos Legales y Condiciones</a>)
	</li>
	<li>Tome fotos en el día, con la mayor cantidad de luz posible, para mostrar bien su producto.</li>
	<li>Asegúrese de que su imagen sea nítida antes de publicarla. Las imágenes borrosas no capturan el buen interés del comprador.</li>
	<li>Por favor tome más fotos desde diferentes ángulos, es apreciado por el comprador.</li>
</ul>',
	'title_3' => 'INFORMACIÓN BÁSICA',
	'body_3' => '<b>¡Piensa lo que te gustaría saber! "A mayor detalle, mayores oportunidades para encontrar compradores"</b>
<ul>
	<li>Piense en colocar su producto en la categoría correcta.</li>
	<li>Proporcione la ubicación/dirección puntual, así como las medidas exactas de la máquina. Esto puede afectar el precio hasta su destino final. Puede influir en que su máquina sea más o menos atractiva para el comprador.</li>
	<li>Complete toda la información que tenga sobre el producto. Cuanta más información el comprador reciba, es mejor. Piénselo!</li>
	<li>¡Sea honesto! Describa cualquier error, falla o daño.</li>
</ul>',
	'title_4' => 'OTROS CONSEJOS UTILES!',
	'body_4' => '<ul>
		<li>¡Renueve su anuncio! Esto para ubicarse en la parte superior de los resultados de búsqueda.</li>
		<li>¡Modifique su anuncio! ¿No puede vender su producto? Intente cambiar su anuncio para obtener más visitas. Por ejemplo, coloque un precio más bajo / Agregue más información.</li>
	</ul>',

];

?>