<?php

return [

	'click_to_select' => 'Haga click para seleccionar',
	'choose_year' => 'Elegir año',

	'status_new' => 'Nuevo',
	'status_operational' => 'Operativa',
	'status_operationalWFaults' => 'Operativa con fallas',
	'status_notOperational' => 'No operativa',
	'status_iDontKnow' => 'Desconocido',

	// Label translations
	// -----------------------------

	'precio' => 'Precio',

	'calificacion_general_del_vendedor' => 'Calificación general del vendedor',
	'dimensiones_y_tamanos' => 'Dimensiones y tamaños',
	'potencia_y_rendimiento' => 'Potencia y rendimiento',
//	'chasis' => 'Chasis',
	'otra_informacion' => 'Otra informacion',
//	'pais_de_fabricacion' => 'País de fabricación',

	'unknown_year' => 'Año desconocido',
	'informacion_primaria' => 'Información primaria',
	'ano_de_fabricacion' => 'Año de fabricacion',
	'kilometraje' => 'Kilometraje',
	'estado' => 'Estado',
	'potencia_del_motor' => 'Potencia del motor',
	'configuracion_de_eje' => 'Configuración de eje',
	'transmision' => 'Transmisión',
	'informacion_adicional' => 'Información adicional',
	'calificacion_general' => 'Calificación general',
	'ultima_revision_tecnica' => 'Ultima revisión técnica',
	'no_de_propietarios_anteriores' => 'No. de propietarios anteriores',
	'color' => 'Color',
	'volante' => 'Volante',
	'cabina' => 'Cabina',
	'condicion_llantas_delanteras' => 'Condición llantas delanteras',
	'condicion_llantas_traseras' => 'Condición llantas traseras',
	'medida_de_transporte_largo_x_ancho_x_alto' => 'Dimensiones de transporte (Largo x ancho x alto)',
	'peso_bruto' => 'Peso bruto',
	'capacidad_de_carga' => 'Capacidad de carga',
	'tipo_de_frenos' => 'Tipo de frenos',
	'marca_de_los_neumaticos' => 'Marca de los neumáticos',
	'volumen_del_tanque' => 'Volumen del tanque',
	'dimensiones_internas_largo_x_ancho_x_alto' => 'Dimensiones internas (Largo x ancho x alto)',
	'equipamiento_extra' => 'Equipamiento extra',
	'numero_de_serie' => 'Número de serie',
	'motor_ambiental_emision' => 'Motor ambiental (Emisión)',
	'tipo_motor' => 'Tipo motor',
	'cilindrada_del_motor' => 'Cilindrada del motor',
	'salida_de_torsion' => 'Salida de torsión',
	'tipo_de_suspension' => 'Tipo de suspensión',
	'distancia_entre_ejes' => 'Distancia entre ejes',
	'no_de_chasis' => 'No. de chasis',
	'equipamiento' => 'Equipamiento',
	'tipo_de_tracto_camion' => 'Tipo de tracto camión',
	'tornamesa_quinta_rueda' => 'Tornamesa (quinta rueda)',
	'tipo_de_carroceria' => 'Tipo de carrocería',
	'tipo_de_eje' => 'Tipo de eje',
	'no_de_serie' => 'No. de serie',
	'horas_de_trabajo' => 'Horas de trabajo',
	'accesorios' => 'Accesorios',
	'pintura_original' => 'Pintura original',
	'certificacion_ce' => 'Certificación CE',
	'pais_de_fabricacion' => 'País de fabricación',
	'condicion_oruga_izquierda' => 'Condición oruga izquierda',
	'condicion_oruga_derecha' => 'Condición oruga derecha',
	'capacidad_balde' => 'Capacidad balde m3',
	'ancho_de_trabajo' => 'Ancho de trabajo',
	'ancho_de_banda' => 'Ancho de banda',
	'max_capacidad_de_elevacion' => 'Max. Capacidad de elevación',
	'max_capacidad_de_empuje' => 'Max. Capacidad de empuje',
	'motor' => 'Motor',
	'velocidad_maxima' => 'Velocidad máxima',
	'lubricacion_central' => 'Lubricación central',
	'flujo_del_hidraulico' => 'Flujo del Hidraulico l/min',
	'ancho_de_carril' => 'Ancho de carril',
	'longitud_de_palanca_y_draga' => 'Longitud de palanca y draga',
	'tipo_de_cuchillas' => 'Tipo de cuchillas',
	'tipo_de_compactadora' => 'Tipo de compactadora',
	'condicion_del_tambor' => 'Condición del tambor',
	'condicion_de_la_placa' => 'Condición de la placa',
	'ancho_del_tambor' => 'Ancho del tambor',
	'peso_de_compactacion' => 'Peso de compactación',
	'tipo_de_poder' => 'Tipo de poder',
	'tipo_de_traccion' => 'Tipo de tracción',
	'maximos_ocupantes' => 'Máximos Ocupantes',
	'altura_maxima_de_trabajo' => 'Altura máxima de trabajo',
	'largo_extension_plataforma' => 'Largo Extensión Plataforma',
	'capacidad_de_levante' => 'Capacidad de levante',
	'maxima_pendiente_de_traslado' => 'Máxima pendiente de traslado',
	'velocidad_de_desplazamiento' => 'Velocidad de desplazamiento',
	'velocidad_de_subida_y_bajada' => 'Velocidad de Subida y Bajada',
	'apertura_de_alimentacion' => 'Apertura de alimentación',
	'tipo_de_trituradora' => 'Tipo de trituradora',
	'volumen_de_produccion' => 'Volumen de producción',
	'nivel_de_funcionamiento' => 'Nivel de funcionamiento',
	'peso' => 'Peso',
	'tamano_del_material' => 'Tamaño del material',
	'velocidad' => 'Velocidad',
	'potencia' => 'Potencia',
	'tipo_de_combustion' => 'Tipo de combustion',
	'chasis' => 'Chasis',
	'cilindros' => 'Cilindros',
	'frecuencia' => 'Frecuencia',
	'voltaje' => 'Voltaje',
	'tipo_de_compresor' => 'Tipo de compresor',
	'caudal' => 'Caudal',
	'marca_del_motor' => 'Marca del motor',
	'presion_de_trabajo' => 'Presión de trabajo',
	'traccion' => 'Tracción',
	'tamano_de_llantas_delanteras' => 'Tamaño de llantas delanteras',
	'tamano_de_llantas_traseras' => 'Tamaño de llantas traseras',
	'toma_fuerza' => 'Toma fuerza (PTO)',
	'anchura_de_trabajo' => 'Anchura de trabajo',
	'configuracion_de_manejo' => 'Configuración de manejo',
	'carro_de_la_horquilla' => 'Carro de la horquilla',
	'altura_maxima_de_elevacion' => 'Altura Maxima de elevación',
	'longitud_de_unas' => 'Longitud de uñas',
	'punto_de_carga' => 'Punto de carga',
	'mastil_de_elevacion' => 'Mástil de elevación',
	'tipo_de_neumatico' => 'Tipo de neumático',
	'marca_de_la_bateria' => 'Marca de la bateria',
	'voltaje_de_la_bateria' => 'Voltaje de la bateria',
	'voltaje_del_cargador' => 'Voltaje del cargador',
	'capacidad_de_la_bateria' => 'Capacidad de la bateria',
	'capacidad_max_de_levante' => 'Capacidad máx. de levante',
	'altura_de_elevacion' => 'Altura de elevación',
	'longitud_de_pluma_principal' => 'Longitud de pluma principal',
	'extension_de_pluma_principal' => 'Extensión de pluma principal',
	'longitud_maxima_del_sistema' => 'Longitud máxima del sistema',
	'contrapeso_maximo' => 'Contrapeso máximo',
	'longitud_del_portador' => 'Longitud del portador',
	'maxima_pendiente' => 'Máxima pendiente',
	'capacidad' => 'Capacidad',
	'giro_de_cabina' => 'Giro de cabina',
	'tipo' => 'Tipo',
	'entre_punto' => 'Entre punto',
	'volteo' => 'Volteo',
	'husillo' => 'Husillo',
	'bomba_de_refrigeracion' => 'Bomba de refrigeración',
	'volteo_con_escote' => 'Volteo con escote',
	'volteo_sin_escote' => 'Volteo sin escote',
	'ancho_de_la_cama' => 'Ancho de la cama',
	'movimiento_cruz' => 'Movimiento cruz',
	'marca_del_visualizador' => 'Marca del visualizador',
	'caja' => 'Caja',
	'velocidad_de_husillo' => 'Velocidad de husillo',
	'alimentacion' => 'Alimentación',
	'iso' => 'ISO',
	'movimiento_longitud_x' => 'Movimiento longitud X',
	'movimiento_transversal_y' => 'Movimiento transversal Y',
	'movimiento_vertical_z' => 'Movimiento vertical Z',
	'distancia_husillo_mesa' => 'Distancia husillo-mesa',
	'cambiador_de_herramientas' => 'Cambiador de herramientas',
	'sistema_de_programacion' => 'Sistema de programación',
	'velocidad_del_husillo' => 'Velocidad del husillo',
	'alimentacion_x_y_z' => 'Alimentación (X / Y / Z)',
	'marcha_rapida' => 'Marcha rápida',
	'tipo_de_alimentacion' => 'Tipo de alimentación',
	'tipo_de_cilindradora' => 'Tipo de cilindradora',
	'capacidad_max_esposor' => 'Capacidad máx. esposor',
	'diametro_rodillo_superior' => 'Diametro rodillo superior',
	'diametro_rodillo_inferior' => 'Diametro rodillo inferior',
	'potencia_de_motor' => 'Potencia de motor',
	'flujo_hidraulico' => 'Flujo hidráulico l/min',
	'velocidad_de_laminacion' => 'Velocidad de laminación',
	'capacidad_max_de_plegado' => 'Capacidad máx. de plegado',
	'distancia_del_golpe' => 'Distancia del golpe',
	'distancia_entre_barras' => 'Distancia entre barras',
	'max_altura_de_apertura' => 'Max. altura de apertura',
	'volumen_tanque_hidraulico' => 'Volumen tanque hidráulico',
	'capacidad_en_toneladas' => 'Capacidad en toneladas',
	'industria' => 'Industria',
	'longitud_de_trabajo' => 'Longitud de trabajo',
	'max_corte' => 'Max. Corte',
	'flujo_de_hidraulico' => 'Flujo de hidráulico en l/min',
	'flujo_de_hidraulico_en' => 'Flujo de hidráulico en l/min',
	'dimensiones_mesa_lxa' => 'Dimensiones mesa (LxA)',
	'cono_del_husillo' => 'Cono del husillo',
	'movimiento_del_husillo' => 'Movimiento del husillo',
	'capacidad_maxima_de_perforacion' => 'Capacidad máxima de perforación',
	'distancia_husillo_columna' => 'Distancia husillo / columna',
	'amperaje' => 'Amperaje',
	'velocidad_de_alimentacion' => 'Velocidad de alimentación',
	'otros' => 'Otros',
	'otros1' => 'Otros',
	'otros2' => 'Otros',
	'otros3' => 'Otros',
	'otros4' => 'Otros',
	'otros5' => 'Otros',
	'volteo_transversal' => 'Volteo transversal',
	'reduccion_de_cubo' => 'Reducción de cubo',
	'avance_rapido_de_carro' => 'Avance rapido de carro',

	// Configuracion del Eje
	// -----------------------------

	'axis_4X2' => '4X2',
	'axis_4X4' => '4X4',
	'axis_4X4*2' => '4X4*2',
	'axis_4X4*4' => '4X4*4',
	'axis_6X2' => '6X2',
	'axis_6X2*4' => '6X2*4',
	'axis_6X2/4' => '6X2/4',
	'axis_6X4' => '6X4',
	'axis_6X6' => '6X6',
	'axis_6X6*2' => '6X6*2',
	'axis_8X2' => '8X2',
	'axis_8X2/4' => '8X2/4',
	'axis_8X2*4' => '8X2*4',
	'axis_8X2/6' => '8X2/6',
	'axis_8X2*6' => '8X2*6',
	'axis_8X4' => '8X4',
	'axis_8X4/4' => '8X4/4',
	'axis_8X4*4' => '8X4*4',
	'axis_8X6' => '8X6',
	'axis_8X6/4' => '8X6/4',
	'axis_8X8' => '8X8',
	'axis_8X8/4' => '8X8/4',
	'axis_10X2' => '10X2',
	'axis_10X4' => '10X4',
	'axis_10X6' => '10X6',
	'axis_10X6/4' => '10X6/4',
	'axis_10X8' => '10X8',
	'axis_10X10' => '10X10',

	// Calificacion General
	// -----------------------------
	
	'grating_veryGood' => 'Muy Bueno',
	'grating_good' => 'Bueno',
	'grating_regular' => 'Regular',
	'grating_bad' => 'Malo',
	
	'no_previous_owners' => 'Numero de propietarios anteriores',
	'colour' => 'Color',

	// Volante
	// -----------------------------

	'steering_wheel' => 'Volante',
	'steering_left' => 'Izquierda',
	'steering_right' => 'Derecha',

	// Cabina
	// -----------------------------
	
	'cabin' => 'Cabina',
	'cabin_dayCab' => 'Cabina Simple',
	'cabin_sleeperCab' => 'Camarote',
	'cabin_highSleeperCab' => 'Camarote XL',
	'cabin_crewCab' => 'Cabina tripulantes',
	'cabin_extendedCab' => 'Cabina Extendida',
	
	'transport_measure' => 'Medidas de transporte (Largo x ancho x alto)',
	'weight_brute' => 'Peso Bruto',
	'load_capacity' => 'Capacidad de carga',

	// Tipo de frenos CAMIONES
	// -----------------------------
	
	'break_types' => 'Tipo de frenos',

	'tpBreakCam_dontKnow' => 'No se',
	'tpBreakCam_disk' => 'Frenos de disco',
	'tpBreakCam_drum' => 'Frenos de tambor',
	'tpBreakCam_other' => 'Otros',

	// Marca de las llantas
	// -----------------------------
	
	'tires_brand' => 'Marca de los neumaticos',

	'tiresBrand_michelin' => 'Michelin',
	'tiresBrand_goodyear' => 'Goodyear',
	'tiresBrand_continental' => 'Continental',
	'tiresBrand_bridgestone' => 'Bridgestone',
	'tiresBrand_pirelli' => 'Pirelli',
	'tiresBrand_nokian' => 'Nokian',
	'tiresBrand_blacklion' => 'Blacklion',
	'tiresBrand_hankook' => 'Hankook',
	'tiresBrand_kumho Tires' => 'Kumho Tires',
	'tiresBrand_dunlop' => 'Dunlop',
	'tiresBrand_bFGoodrich' => 'BFGoodrich',
	'tiresBrand_yokohama' => 'Yokohama',
	'tiresBrand_others' => 'Otro',
	
	'fueltank_volume' => 'Volumen del tanque',
	
	'internal_measure' => 'Dimensiones internas (Largo x ancho x alto)',
	
	'tandem_axis_lifting' => 'Levantamiento del eje Tándem',
	'levantamiento_del_eje_tandem' => 'Levantamiento del eje Tándem',

	// Equipamiento extra CAMIONES
	// --------------------------------
	
	'extra_equipment' => 'Equipamiento extra',

	'xtraEquCam_CoffeeMaker' => 'Cafetera',
	'xtraEquCam_refrigerator' => 'Heladera',
	'xtraEquCam_toolbox' => 'Caja de herramientas',
	'xtraEquCam_warningLamp' => 'Farol de advertencia',
	'xtraEquCam_workLampOutside' => 'Lámpara de trabajo afuera',
	'xtraEquCam_foglamps' => 'Faros de niebla',
	'xtraEquCam_outerVisor' => 'Visera exterior ',
	'xtraEquCam_bumperSpoiler' => 'Alerón del parachoques',
	'xtraEquCam_frontsBumpers' => 'Parachoques delantero',
	'xtraEquCam_lowerLightBar' => 'Barra de luz más baja',
	'xtraEquCam_headlightProtector' => 'Protector de faros',
	'xtraEquCam_mosquitoNetRadiator' => 'Red para mosquito - radiador',
	'xtraEquCam_aluminumRims' => 'Aros de aluminio',
	'xtraEquCam_mudflap' => 'Guardabarros',
	'xtraEquCam_fenderFlap' => 'Aleta guardabarros',
	'xtraEquCam_extinguishers' => 'Extintor de incendios',
	'xtraEquCam_other' => 'Otros',
	
	'serial_no' => 'Número de serie',
	'ambiental_engine' => '',

	// Tornamesa TRACTO CAMION
	// --------------------------

	'tornaCam_fixed' => 'Fijo',
	'tornaCam_adjustable' => 'Ajustable',

	// Tipo de TRACTO CAMION
	// ----------------------

	'tpTractoCam_standardTractorTrailer' => 'Estándar tractor/remolque',
	'tpTractoCam_heavyLoad' => 'Carga pesada',
	'tpTractoCam_hazardousLoad' => 'Carga peligrosa',
	'tpTractoCam_volumeTrailer' => 'Remolque de volumen',
	'tpTractoCam_other' => 'Otros',

	// Tipo de carroceria OTROS
	// --------------------------

	'tpCarr_euro' => 'Euro',
	'tpCarr_jumbo' => 'Jumbo',
	'tpCarr_mega' => 'Mega',
	'tpCarr_extendible' => 'Extensible',
	'tpCarr_fixed' => 'Fijo',
	'tpCarr_other' => 'Otro',

	// Tipo de eje OTROS
	// -----------------------

	'tpEje_1' => '1',
	'tpEje_1+1' => '1+1',
	'tpEje_1+2' => '1+2',
	'tpEje_1+3' => '1+3',
	'tpEje_2' => '2',
	'tpEje_2+1' => '2+1',
	'tpEje_2+2' => '2+2',
	'tpEje_2+3' => '2+3',
	'tpEje_3' => '3',
	'tpEje_4' => '4',

	// Motor Ambiental
	// ------------------------

	'motAmb_euro_0' => 'Euro 0',
	'motAmb_euro_1' => 'Euro 1',
	'motAmb_euro_2' => 'Euro 2',
	'motAmb_euro_3' => 'Euro 3',
	'motAmb_euro_4' => 'Euro 4',
	'motAmb_euro_5' => 'Euro 5',
	'motAmb_euro_6' => 'Euro 6',
	'motAmb_other' => 'Otros',

	// Tipo de Suspension
	// -------------------------

	'tpSusp_parabolicParabolic' => 'Parabólica-Parabólica',
	'tpSusp_parabolicLeaf' => 'Parabólica-Lámina',
	'tpSusp_parabolicAir' => 'Parabólica-Globo',
	'tpSusp_leafParabolic' => 'Lámina-Parabólica',
	'tpSusp_leafLeaf' => 'Lámina-Lámina',
	'tpSusp_leafAir' => 'Lámina-Globo',
	'tpSusp_airAir' => 'Globo-Globo',
	'tpSusp_trapezoid' => 'Trapezoide',

	// Accesorios PALAS CARGADORAS
	// ----------------------------

	'accePalas_Forks' => 'Uñas',
	'accePalas_Mudguards' => 'Salpicaderas',
	'accePalas_QuickCoupler' => 'Acoplador rápido',
	'accePalas_3rdHydraulicCircuit' => '3: función hydraulica',
	'accePalas_4thHydraulicCircuit' => '4: función hydraulica',
	'accePalas_AutoLubricationSystem' => 'Sistema de lubricación',
	'accePalas_Weighloader' => 'Sistema de pesaje',
	'accePalas_MainSwitch' => 'Interruptor principal',
	'accePalas_WarningLight' => 'Farol giratorio',
	'accePalas_DieselHeater' => 'Calentador diesel',
	'accePalas_JoystickKontrol' => 'Control Joystick',
	'accePalas_AirConditioning' => 'Aire acondicionado',
	'accePalas_ComfortDriveControlCDC' => 'Control de manejo CDC',
	'accePalas_AdditionalHydraulics' => 'Hydraulico adicional',
	'accePalas_MachineControlSystem' => 'Sistema de control de maquinaria',
	'accePalas_Aps' => 'Aps',

	// ACCESORIOS ORUGA
	// ------------------------

	'acceOruga_EngineHeater' => 'Calentador diesel',
	'acceOruga_Winch' => 'cabrestante',
	'acceOruga_AirConditioning' => 'Aire acondicionado',
	'acceOruga_AdditionalHydraulics' => 'Hidráulico adicional',
	'acceOruga_MultiShankRipper' => 'Multi ripper',
	'acceOruga_ShankRipper' => 'Ripper',
	'acceOruga_MainSwitch' => 'Interruptor principal',
	'acceOruga_WarningLight' => 'Farol giratorio',

	// Accesorios EXCAVADORAS
	// -----------------------------

	'acceExcav_HammerHydraulic' => 'Martillo hidráulico',
	'acceExcav_ShearHydraulic' => 'Cizalla hidráulica',
	'acceExcav_QuickCoupler' => 'Acoplador rápido',
	'acceExcav_LineValveDipperStick' => 'Válvula de seguridad -palanca/draga',
	'acceExcav_LineValveBoomHoist' => 'Válvula de seguridad -brazo/torre',
	'acceExcav_AutoLubricationSystem' => 'Sistema de lubricación',
	'acceExcav_Rotator' => 'Rotador',
	'acceExcav_MainSwitch' => 'Interruptor principal',
	'acceExcav_WarningLight' => 'Farol giratorio',
	'acceExcav_DieselHeater' => 'Calentador diesel',
	'acceExcav_JoystickKontrol' => 'Control Joystick',
	'acceExcav_AirConditioning' => 'Aire acondicionado',
	'acceExcav_ExtraEquipment' => 'Equipo extra',
	'acceExcav_AdditionalHydraulics' => 'Hydraulico adicional',
	'acceExcav_MachineControlSystem' => 'Sistema de control de maquinaria',
	'acceExcav_DepthControl' => 'Sistema de control de excavación',
	'acceExcav_AdjustableBoom' => 'Pluma ajustable',
	'acceExcav_SteelTracks' => 'Orugas de acero',
	'acceExcav_Weighloader' => 'Sistema de pesaje',
	'acceExcav_EstándarDiggingBucket' => 'Balde estándar de excavación',
	'acceExcav_CableBucket' => 'Balde de cable',
	'acceExcav_DitchingGradingBucket' => 'Balde de graduación',
	'acceExcav_VDitchBucket' => 'Balde para zanjas en V',
	'acceExcav_RubberTracks' => 'Orugas de caucho',

	// MOTOR AMBIENTAL EXCAVADORA |ORUGA|
	// -----------------------------------------

	'motorAmbExc_IDK' => 'Desconocido',
	'motorAmbExc_Stage_2' => 'Fase | | ',
	'motorAmbExc_Stage_3a' => 'Fase | | | A',
	'motorAmbExc_Stage_3b' => 'Fase | | | B',
	'motorAmbExc_Stage_4' => 'Fase | V',
	'motorAmbExc_Stage_5' => 'Fase V',

	// CABINA |PALA CARGADORA |ORUGA|
	// ----------------------------------

	'cabina_palaOr_OpenCab' => 'Cabina abierta',
	'cabina_palaOr_EnclosedCab' => 'Cabina cerrada',

	// Tipo de cuchillas | ORUGA
	// ---------------------------

	'tpCuchillaOru_IDK' => 'Desconocido',
	'tpCuchillaOru_SemiUBlade' => 'Cuchilla/hoja semi esférica',
	'tpCuchillaOru_UBlade' => 'Cuchilla/hoja esférica',
	'tpCuchillaOru_6WayBlade' => 'Cuchilla/hoja 6 posiciones',
	'tpCuchillaOru_StraightBlade' => 'Cuchilla/hoja recta',

	// Accesorios para COMPACTADORA
	// ------------------------------

	'acceCompac_mainSwitch' => 'Interruptor principal',
	'acceCompac_warningLight' => 'Farol giratorio',
	'acceCompac_extraEquipment' => 'Equipo extra',
	'acceCompac_dieselHeater' => 'Calentador diesel',
	'acceCompac_airConditioning' => 'Aire acondicionado',
	'acceCompac_additionalHydraulics' => 'Hidráulico adicional',

	// Tipo de  COMPACTADORA
	// ------------------------

	'tpCompact_SingleDrumRollers' => 'Rodillo de un solo tambor',
	'tpCompact_TwinDrumRollers' => 'Rodillo de doble tambor',
	'tpCompact_CombiRollers' => 'Rodillos combinados',
	'tpCompact_PneumaticTiredRollers' => 'Rodillos sobre neumáticos',
	'tpCompact_TowedVibratoryRollers' => 'Rodillo vibratorio de arrastre',
	'tpCompact_OtherRollers' => 'Otros rodillos',
	'tpCompact_SoilCompactors' => 'Compactadoras de suelo',
	'tpCompact_PlateCompactors' => 'Placas compactadoras',
	'tpCompact_Tampers' => 'Pisones compactadores',
	'tpCompact_Other' => 'Otros',

	// Tipo de traccion ALZA HOMBRES
	// ----------------------------

	'tpTracAlza_2WD' => '2 WD',
	'tpTracAlza_4WD' => '4 WD',
	'tpTracAlza_Tracked' => 'Sobre orugas',
	'tpTracAlza_Other' => 'Otros',

	// Tipo de poder ALZA HOMBRES
	// -----------------------------

	'tpPoderAlza_Electric' => 'Eléctrico',
	'tpPoderAlza_Diesel' => 'Diesel',
	'tpPoderAlza_LiquidGas' => 'Gas liquido',
	'tpPoderAlza_Gas' => 'Gasolina',
	'tpPoderAlza_Hybrid' => 'Gas/gasolina',

	'maxPend_5' => '5°',
	'maxPend_10' => '10°',
	'maxPend_15' => '15°',
	'maxPend_20' => '20°',
	'maxPend_25' => '25°',
	'maxPend_30' => '30°',
	'maxPend_35' => '35°',
	'maxPend_40' => '40°',
	'maxPend_45' => '45°',
	'maxPend_50' => '50°',
	'maxPend_55' => '55°',
	'maxPend_60' => '60°',
	'maxPend_65' => '65°',
	'maxPend_70' => '70°',
	'maxPend_75' => '75°',
	'maxPend_80' => '80°',
	'maxPend_85' => '85°',
	'maxPend_90' => '90°',

	// Tipo de trituradoras
	// ---------------------

	'tpTritur_JawCrusher' => 'Trituradora de mandíbula',
	'tpTritur_ConeCrusher' => 'Trituradora de cono',
	'tpTritur_BallCrusher' => 'Trituradora de bolas',
	'tpTritur_ImpactCrusher' => 'Trituradora de impacto',
	'tpTritur_RodCrusher' => 'Trituradora de barras',
	'tpTritur_GyratoryCrusher' => 'Trituradora giratoria',
	'tpTritur_RollCrusher' => 'Trituradora de muelas',
	'tpTritur_ShredderCrusher' => 'Trituradora desmenuzadora',
	'tpTritur_HammerCrusher' => 'Trituradora martillo',

	// Nivel de fincionamiento TRITURADORAS
	// ------------------------------------

	'nvFunTrit_Primary' => 'Primario',
	'nvFunTrit_Secondary' => 'Secundario',
	'nvFunTrit_Tertiary' => 'Terciario',
	'nvFunTrit_Quaternary' => 'Cuaternario',
	'nvFunTrit_Other' => 'Otros',

	// Tipo de combustion GENERADOR
	// ------------------------------

	'tpCombus_Diesel' => 'Diesel',
	'tpCombus_LiquidGas' => 'Gas liquido',
	'tpCombus_Electric' => 'Electrico',

	// Caudal compresor
	// --------------------

	'caudComp_m3Min' => 'm3/min',
	'caudComp_lMin' => 'l/min',
	'caudComp_lSec' => 'l/sec',

// TIPO COMPRESOR
// ---------------------

	'tpCompres_PistonCompressor' => 'Compresor a pistón',
	'tpCompres_ScrewCompressor' => 'Compresor a tornillo',
	'tpCompres_CentrifugeCompressor' => 'Compresor de fuerza centrífuga',
	'tpCompres_Others' => 'Otros',

// CHASIS COMPRESOR
// ---------------------

	'chasisCompr_Static' => 'Estático',
	'chasisCompr_Mobile' => 'Móvil',

// Cilindros TRACTOR
// -------------------

	'cilind_Trac_2st' => '2 cilindros',
	'cilind_Trac_3st' => '3 cilindros',
	'cilind_Trac_4st' => '4 cilindros',
	'cilind_Trac_5st' => '5 cilindros',
	'cilind_Trac_6st' => '6 cilindros',
	'cilind_Trac_8st' => '8 cilindros',

// toma fuerza PTO
// ---------------

	'tomaPower_540' => '540',
	'tomaPower_750' => '750',
	'tomaPower_1000' => '1000',
	'tomaPower_540_750' => '540/750',
	'tomaPower_540_540e' => '540/540e',
	'tomaPower_540_1000' => '540/1000',
	'tomaPower_540_750_1000' => '540/750/1000',
	'tomaPower_540_540e_1000' => '540/540e/1000',
	'tomaPower_540_750e_1000' => '540/750e/1000',
	'tomaPower_540_1000_1000e' => '540/1000/1000e',
	'tomaPower_540_750_1000_1400' => '540/750/1000/1400',
	'tomaPower_540_540e_1000_1000e' => '540/540e/1000/1000e',

// Accesorios TRACTOR
// ------------------------

	'acceTrac_NoAccessories' => 'No lleva',
	'acceTrac_FrontLoader' => 'Cargador frontal',
	'acceTrac_FrontPTO' => 'Toma fuerza delantera',
	'acceTrac_FrontLinkage' => 'Articulación delantera',
	'acceTrac_ClutchlessForwardReverseShuttle' => 'Sistema de transmisión sin embrague',
	'acceTrac_TransmissionCVT' => 'Transmisión CVT',
	'acceTrac_TurbineClutch' => 'Embrague de turbina',
	'acceTrac_ReverseDriveSystem' => 'Asiento rotativo',
	'acceTrac_FrontAxleSuspension' => 'Suspensión eje delantero',
	'acceTrac_AirConditioning' => 'Aire acondicionado',
	'acceTrac_ForestCab' => 'Cabina forestal',
	'acceTrac_CabinSuspension' => 'Suspensión de cabina',
	'acceTrac_GuidanceSystem' => 'Sistema de manejo',
	'acceTrac_AdBlue' => 'Ad blue',
	'acceTrac_AirBrakes' => 'Frenos de aire',
	'acceTrac_CreeperGear' => 'Marcha lenta',

// Cabina TRACTOR
// ------------------

	'cabinTrac_Open' => 'Abierto',
	'cabinTrac_Forest' => 'Forestal',
	'cabinTrac_Regular' => 'Normal',
	'cabinTrac_Other' => 'Otro',

// Accesorios SEGADORAS
// -------------------

	'acceSega_NoAccessories' => 'No lleva',
	'acceSega_AirConditioning' => 'Aire acondicionado',
	'acceSega_GPS' => 'GPS',
	'acceSega_CornHead' => 'Cabezales para maíz',
	'acceSega_GrainHead' => 'Cabezal para grano',
	'acceSega_HeaderTrailer' => 'Remolque de trilla',
	'acceSega_SideKnife' => 'Cuchilla lateral',
	'acceSega_CropLifters' => 'Elevador de espigas',
	'acceSega_CutterbarAutomaticLeveling' => 'Sistema automatico MC',
	'acceSega_AssistedSteeringSystem' => 'Sistema de control de asistencia',
	'acceSega_StrawChopper' => 'Picadora de paja',
	'acceSega_ChaffSpreader' => 'Esparcidor',
	'acceSega_ElectricallyAdjustableSieves' => 'Cribas eléctricamente ajustables',
	'acceSega_YieldAndMoistureMeasuring' => 'Sistema de rendimiento y humedad',


// Configuracion de transmision ENFARFADORA
// --------------------------------------

	'cnfTransEnfar_Trailed' => 'Con remolque',
	'cnfTransEnfar_Mounted' => 'Montado',

// Configuracion de manejo MONTACARGA
// -------------------------------

	'cnfManMont_Electric' => 'Eléctrico',
	'cnfManMont_Diesel' => 'Diesel',
	'cnfManMont_Gas' => 'Gasolina',
	'cnfManMont_LiquidGas' => 'Gas',
	'cnfManMont_Hybrid' => 'Gas/Gasolina',
	'cnfManMont_Manual' => 'Manual',
	'cnfManMont_Other' => 'Otros',

// Mastil de elevacion MONTAGARGA
// -------------------------------
	'mastElevMont_Simplex' => 'Simple',
	'mastElevMont_Duplex' => 'Doble',
	'mastElevMont_Triplex' => 'Triple',
	'mastElevMont_Quad' => 'Cuádruple',
	'mastElevMont_DuplexFreeLift' => 'Duplo/elevación libre',
	'mastElevMont_TriplexFreeLift' => 'Triplex/elevación libre',
	'mastElevMont_Telescopic' => 'Telescópica',
	'mastElevMont_Other' => 'Otros',



// Carro de la horquilla MONTACARGA
// ------------------------------

	'carrMont_NoCarriege' => 'Sin extra',
	'carrMont_Sideshifter' => 'Desplazador lateral',
	'carrMont_PositionerSpread' => 'Posicionador y diseminador',
	'carrMont_PositionerSpreadTilt' => 'Posicionador,diseminador,basculante',
	'carrMont_ForkSpreading' => 'Diseminador horquilla',
	'carrMont_DemountableSystem' => 'Sistema desmontable',
	'carrMont_MultiPalletCarriege' => 'Carrocería multi pale',
	'carrMont_Clamp' => 'Abrazadera',
	'carrMont_RollClamp' => 'Abrazadera de rodillo',
	'carrMont_PaperRollClamp' => 'Abrazadera de rollo de papel',
	'carrMont_Rotator' => 'Rotador',
	'carrMont_PusherForks' => 'Uñas telescópicas',
	'carrMont_ReachForks' => 'Uñas largas',
	'carrMont_Other' => 'Otros',



// Tipo de llantas MONTACARGA
// --------------------------

	'tpLlantaMont_Pneumatic' => 'Neumática',
	'tpLlantaMont_SolidPneumatic' => 'Sólida-neumática',
	'tpLlantaMont_SuperElastic' => 'Super elástico',
	'tpLlantaMont_Other' => 'otros',


// Cabina MONTACARGA
// -------------------------

	'cabMont_OverheadGuard' => 'Protección superior',
	'cabMont_ClosedCabin' => 'Cabina cerrada',
	'cabMont_NoCabin' => 'Sin cabina',


// Tipo de carroceria GRUA
// -----------------------

	'tpCarGrua_Tracked' => 'Sobre orugas',
	'tpCarGrua_Wheeled' => 'Sobre ruedas',

// Tipo  TORNOS
// -----------------
	'tpTorno_Tracked' => 'Torno paralelo',
	'tpTorno_CopyingLathe' => 'Torno copiador',
	'tpTorno_TurretLathe' => 'Torno revolver',
	'tpTorno_VerticalLathe' => 'Torno vertical',
	'tpTorno_AutomaticLathe' => 'Torno automático',
	'tpTorno_CarouselLathe' => 'Torno carrusel',
	'tpTorno_CNCLathe' => 'Torno CNC',
	'tpTorno_PlanLathe' => 'Torno de piso',
	'tpTorno_Other' => 'Otros',


// Accesorios para TORNO
// -------------------------

	'acceTorno_WithoutAccesories' => 'Sin accesorios',
	'acceTorno_3JawChuck' => 'Plato universal - 3 garras',
	'acceTorno_4JjawChuck' => 'Plato independiente - 4 garras',
	'acceTorno_FlatChuck' => 'Plato de arrastre/plano',
	'acceTorno_FixedSteady' => 'Luneta Fija',
	'acceTorno_TravellingSteady' => 'Luneta móvil',
	'acceTorno_FastRevolvingCenter' => 'Contrapunto fijo/giratorio',
	'acceTorno_DrillChuck' => 'Porta broca - Mandril',
	'acceTorno_QuickChangeTool' => 'Torreta portaherramienta',
	'acceTorno_VeeHolders' => 'Portaherramientas',
	'acceTorno_Other' => 'Otros',



// Marca del visualizador TORNO
// --------------------------
	'branVisTorno_WithoutDigital' => 'No tiene',
	'branVisTorno_Heidenhain' => 'Heidenhain',
	'branVisTorno_Mitutoyo' => 'Mitutoyo',
	'branVisTorno_Sony' => 'Sony',
	'branVisTorno_Easson' => 'Easson',
	'branVisTorno_Delos' => 'Delos',
	'branVisTorno_Other' => 'Otros',


// Tipo FRESADORA
// --------------------
	'tpFresa_BenchTypeMillingMachine' => 'Fresadora de banco',
	'tpFresa_UniversalMillingMachine' => 'Fresadora universal',
	'tpFresa_VerticalMillingMachine' => 'Fresadora vertical',
	'tpFresa_HorisontalMillingMachine' => 'Fresadora horizontal',
	'tpFresa_BedMillingMachine' => 'Fresadora cama',
	'tpFresa_CNCMillingMachine' => 'Fresadora CNC',
	'tpFresa_CNCBedMillingMachine' => 'Fresadora cama CNC',
	'tpFresa_CopyingMillineMachine' => 'Fresa copiadora',



// Accesorios FRESA
// ------------------

	'acceFresa_WithoutAccesories' => 'Sin accesorios',
	'acceFresa_MachineVice' => 'Tornillo de banco',
	'acceFresa_UniversalDivider' => 'Plato divisor',
	'acceFresa_UniversalDividerFastCenter' => 'Plato divisor - contra punto',
	'acceFresa_RoundCoordinatTable' => 'Plato de coordenadas',
	'acceFresa_CoordinatTableDividing' => 'Plato de coordenas - divisor',
	'acceFresa_CoordinatTable' => 'Mesa de coordenadas',
	'acceFresa_Other' => 'Otros',

// ISO FRESA
// ----------------
	'isoFresa_ISO_01' => 'ISO 01',
	'isoFresa_ISO_10' => 'ISO 10',
	'isoFresa_ISO_20' => 'ISO 20',
	'isoFresa_ISO_30' => 'ISO 30',
	'isoFresa_ISO_40' => 'ISO 40',
	'isoFresa_ISO_50' => 'ISO 50',


// Bomba de refrigeracion FRESA
// ----------------------------
	'bombRefFresa_IDK' => 'Desconocido',
	'bombRefFresa_YesGoodCondition' => 'Si - Operativa',
	'bombRefFresa_YesBadCondition' => 'Si - Defectuoso',
	'bombRefFresa_No' => 'No',

// Tipo cilindradora
// --------------------
	'tpCilin_Manual' => 'Manual',
	'tpCilin_Electric' => 'Eléctrica',
	'tpCilin_ElectricHydraulics' => 'Eléctrica - hidráulica',
	'tpCilin_Hydraulics' => 'Hidráulica',
	'tpCilin_Other' => 'Otros',

// Tipo de serie CILINDRADORA
// --------------------------
	'tpSerCilin_2RollVending' => 'Cilindradora de 2 rodillos',
	'tpSerCilin_3RollVending' => 'Cilindradora de 3 rodillos',
	'tpSerCilin_4RollVending' => 'Cilindradora de 4 rodillos',
	'tpSerCilin_AsymmetricalType' => 'Cilindradora asimétrica variable',
	'tpSerCilin_Other' => 'Otros',

// Tipo PLEGADORA
// --------------------
	'tpPleg_ManualFoldning' => 'Plegadora manual',
	'tpPleg_MechanicalFoldingMachine' => 'Plegadora mecánica',
	'tpPleg_HydraulicMechanicalFoldingMachine' => 'Plegadora hidra-mecánica',
	'tpPleg_HydraulicFoldingMachine' => 'Plegadora hidráulica',
	'tpPleg_CNCFoldingMachine' => 'Plegadora CNC',
	'tpPleg_Other' => 'Otros',

// Tipo GUILLOTINA
// ---------------------
	'tpGuillo_MechanicalGuillotineShear' => 'Cizallas mecánica',
	'tpGuillo_HydraulicGuillotineShear' => 'Cizallas hidráulica',

// Tipo RECTIFICADORA
// --------------------
	'tpRect_AbrasiveBeltGrinder' => 'Lijadora de banda',
	'tpRect_DrillgrindingMachine' => 'Limadora de brocas',
	'tpRect_JigGrindingMachine' => 'Cepillo vertical',
	'tpRect_SwinFrameGrinder' => 'Esmeril',
	'tpRect_FaceGrindingMachine' => 'Lijadora',
	'tpRect_ProfileGrindingMachine' => 'Rectificadora de perfiles',
	'tpRect_GearedShapping' => 'Cepillo',
	'tpRect_Tool&CutterGrindingMachine' => 'Rectificadora para herramientas',
	'tpRect_CrankshaftLathe' => 'Rectificadora de cigüeñal',
	'tpRect_Other' => 'Otros',

// Tipo TALADRO
// ----------------
	'tpTalad_2SpindleDrill' => 'Doble cabezal',
	'tpTalad_CombinedDrillingMillingMachine' => 'Taladro/fresador',
	'tpTalad_BenchDrill' => 'Taladro de mesa',
	'tpTalad_MultiSpindleDrill' => 'Tres o más cabezales',
	'tpTalad_JigDrillingMachine' => 'Taladro rectificador',
	'tpTalad_MagneticDrill' => 'Taladro magnético',
	'tpTalad_RadialDrillingMachine' => 'Taladro radial',
	'tpTalad_PillarDrillingMachine' => 'Taladro pedestal',
	'tpTalad_DeepHoleDrillingMachine' => 'Taladro de agujero largo',
	'tpTalad_ExtractionDrill' => 'Taladro de extracción',
	'tpTalad_Other' => 'Otros',

// Tipo SOLDADORAS
// ----------------------
	'tpSolda_ConventionalWeldingMachine' => 'Soldadora convencional',
	'tpSolda_StudWelding' => 'Soldadora de pernos',
	'tpSolda_InvertWelding' => 'Soldador Inverter',
	'tpSolda_MigMag' => 'MIG/Mag',
	'tpSolda_GasEngineWelding' => 'Motosoldador - Gasolina',
	'tpSolda_DieselEngineWelding' => 'Motosoldador - Diesel',
	'tpSolda_Plasma' => 'Soldador plasma',
	'tpSolda_PlasticWelding' => 'Soldador de plastico',
	'tpSolda_SpotWelding' => 'Soldador de punto',
	'tpSolda_RobotWelding' => 'Soldador robot',
	'tpSolda_TIG' => 'TIG',
	'tpSolda_Other' => 'Otros',

	// Tipo BOOLEAN
	// ----------------------
	'yes' => 'Si',
	'no' => 'No',
	'none' => 'Desconocido',

	// Tipo CABINA |PALA CARGADORA |ORUGA|
	// ----------------------
	'cabinPalaOr_OpenCab' => 'Cabina abierta',
	'cabinPalaOr_EnclosedCab' => 'Cabina cerrada',

	'escote' => 'Escote',
	'mm' => 'mm',
	'pulgada' => 'Pulgada',
	'rosca' => 'Rosca',
	'digital' => 'Digital',
	'ventilador_de_refrigeracion' => 'Ventilador de refrigeracion',
	'pinza' => 'Pinza',
	'cable_de_tierra' => 'Cable de tierra',
	'con_alambre' => 'Con alambre',
	'medida_de_caudal' => 'Medida de caudal',

	'contador_de_fardos' => 'Contador de fardos',



];
?>