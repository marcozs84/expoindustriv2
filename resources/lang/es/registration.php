<?php

return [
	'register_subject' => 'Bienvenido to ExpoIndustri',
	'register_message' => '
		Estimado(a) :nombres_apellidos,<br><br>
		
		Queremos darle la mas cordial bienvenida a <b>ExpoIndustri</b>,
		para poder verificar que esta es su cuenta de correo
		por favor haga click en el siguiente enlace:
		<br><br>
		<a href=":url_confirm">:url_confirm</a>
		
		<br><br>
		Reciba un cordial saludo!<br>
		<b>Equipo de ExpoIndustri</b>
		
		<br><br>
		<hr>
		<span style="color:#666666;">Si por algun motivo usted no ha solicitado una membresía en nuestro sítio, por favor háganoslo saber para dar de baja su cuenta de correo utilizando el siguiente enlace:</span>
		<br><br>
		<a href=":url_report">:url_report</a>',

	'forgot_subject' => 'Expoindustri: Olvidó su contraseña?',
	'forgot_message' => '
		Estimado(a) :nombres_apellidos,<br><br>
		
		Hemos recibido una solicitud de reinicio de contraseña de parte suya, si esto es correcto por favor haga click en el siguiente enlace para fijar una nueva contraseña.
		<br><br>
		<a href=":url_confirm">:url_confirm</a>
		<br><br>
		
		Si usted no ha solicitado ningun cambio de contraseña por favor ignore este mensaje.
		
		<br><br>
		Reciba un cordial saludo,<br>
		<b>Equipo de ExpoIndustri</b>',
];

?>