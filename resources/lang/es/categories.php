<?php

return [

	// ========================================== INDUSTRIAL ==========================================
	'categoria_1' => 'Expoindustri tiene diversas alternativas de maquinaria para distintos tipos de industria, como la metalúrgica, minera, maderera y mas, tanto nuevos como usados disponibles para la venta. Con productos de fabricantes líderes en la industria, como Colchester, Boehringer, Masak, Tos, Rosenfors, Köping, Herbert y más. En esta sección nuestros clientes pueden encontrar, desde tornos, fresas, compresoras, prensas, taladros, generadores hasta accesorios y repuestos, para cualquier tipo de necesidad. Examine las categorías relacionadas.',

	// Tornos
	'categoria_2' => 'Expoindustri le ofrece tornos para distintos tipos de trabajo, ya sea en metal, madera o plástico. Contamos con tornos de distintos tamaños y con diferentes funcionalidades, en las marcas mas reconocidas en el mercado, como Colchester, Boehringer, Masak, Tos, Rosenfors, Köping, Herbert. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Fresas
	'categoria_3' => 'Expoindustri le ofrece fresadoras para distintos tipos de trabajo, ya sea en metal, madera o plástico. Contamos con fresadoras de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Zayer, Correa, Huron, Knuth, Deckel, Köping. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Cilindradoras
	'categoria_4' => 'Expoindustri le ofrece máquinas cilindradoras para distintos tipos de trabajo. Contamos con cilindradoras de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Roundo, Froriep, Ungerer. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Plegadoras
	'categoria_5' => 'Expoindustri le ofrece máquinas plegadoras para distintos tipos de trabajo. Contamos con plegadoras de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Lagan, Mossini, Donewell, Guifil. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Guillotinas
	'categoria_6' => 'Expoindustri le ofrece guillotinas para distintos tipos de trabajo. Contamos con guillotinas de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Ursviken, Darley, Hoan, Haco. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Rectificadoras
	'categoria_7' => 'Expoindustri le ofrece maquinas rectificadoras para distintos tipos de trabajo. Contamos con rectificadoras de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Voumard, Hauser, Elliot, Mägerle, Studer. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Taladros
	'categoria_8' => 'Expoindustri le ofrece taladros para distintos tipos de trabajo, ya sea en metal, madera o plástico. Contamos con taladros de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Arenco, Ogawa, Profila Stanko, Bergonzi, Webo, Foradia, Oerlikon Arboga. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Soldadoras
	'categoria_9' => 'Expoindustri le ofrece soldadoras para distintos tipos de trabajo. Contamos con soldadoras de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Esab, Kemppi, Mig Mag, Oerlikon, lincoln. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Herramientas
	'categoria_10' => 'Expoindustri le ofrece herramientas para distintos tipos de trabajo. Contamos con herramientas de distintos tamaños y con diferentes funcionalidades, en las marcas más reconocidas en el mercado, como Sandvik coromant, Vertex. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',


	// ========================================== TRANSPORTE ==========================================
	'categoria_11' => 'Expoindustri tiene diversas alternativas de equipos de transporte tanto nuevos como usados disponibles para la venta. Con productos de fabricantes lideres en la industria, como Volvo, Scania, Mercedes, Renault, Iveco y más. En esta sección nuestros clientes pueden encontrar, desde equipos de transportes de cualquier índole hasta accesorios y repuestos, para cualquier tipo de necesidad. Examine las categorías relacionadas.',

	// "Camiones > 3,5ton"
	'categoria_12' => 'Expoindustri le ofrece camiones para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de camiones de 0 a 3,5 toneladas en las marcas más reconocidas en el mercado, como Volvo, Scania, Mercedes, Iveco, Daf. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Camiones < 3ton
	'categoria_13' => 'Expoindustri le ofrece camiones para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de camiones mayores a 3,5 toneladas en las marcas más reconocidas en el mercado, como Volvo, Scania, Mercedes, Iveco, Daf. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Camion tractor
	'categoria_14' => 'Expoindustri le ofrece camiones tractores para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de camiones tractores en las marcas más reconocidas en el mercado, como Volvo, Scania, Mercedes, Iveco, Daf. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Motores
	'categoria_15' => 'Expoindustri le ofrece motores de camión para distintos tipos camiones. En esta subcategoría usted puede encontrar todo tipo de motores para las marcas de camiones más reconocidas en el mercado, como Volvo, Scania, Mercedes, Iveco, Daf. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Otros
	'categoria_16' => 'Expoindustri le ofrece todo tipo de accesorios y repuestos para distintos tipos de camiones. En esta subcategoría usted puede encontrar todo tipo de accesorios y repuestos para los camiones de las marcas más reconocidas en el mercado, como Volvo, Scania, Mercedes, Iveco, Daf. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',


	// ========================================== EQUIPO ==========================================
	'categoria_17' => 'Expoindustri tiene diversas alternativas de equipo pesado tanto nuevos como usados disponibles para la venta. Con productos de fabricantes líderes en la industria, como Caterpillar, Volvo, Doosan, Case, JCB y más. En esta sección nuestros clientes pueden encontrar, desde equipo pesado de cualquier índole hasta accesorios y repuestos, para cualquier tipo de necesidad. Examine las categorías relacionadas.',

	// Palas frontales
	'categoria_18' => 'Expoindustri le ofrece palas frontales para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de palas frontales  en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Case, Ljungby maskin, Jcb. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Excavadoras
	'categoria_19' => 'Expoindustri le ofrece excavadoras para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de excavadoras en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Orugas
	'categoria_20' => 'Expoindustri le ofrece excavadoras para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de excavadoras en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Compactadoras
	'categoria_21' => 'Expoindustri le ofrece rodillos compactadores para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de máquinas compactadoras en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Alza hombres
	'categoria_22' => 'Expoindustri le ofrece maquinas alza hombres para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de máquinas alza hombres en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Trituradoras
	'categoria_23' => 'Expoindustri le ofrece maquinas trituradoras para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de máquinas trituradoras en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Generadores
	'categoria_24' => 'Expoindustri le ofrece generadores para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de generadores en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco . ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Compresoras
	'categoria_25' => 'Expoindustri le ofrece compresoras para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de compresoras en las marcas más reconocidas en el mercado, Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Otros
	'categoria_26' => 'Expoindustri le ofrece todo tipo de accesorios y repuestos para distintos tipos de equipos pesados. En esta subcategoría usted puede encontrar todo tipo de accesorios y repuestos para los equipos pesados de las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Atlas Copco ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',



	// ========================================== AGRICOLA ==========================================
	'categoria_27' => 'Expoindustri tiene diversas alternativas de equipos agrícolas tanto nuevos como usados disponibles para la venta. Con productos de fabricantes lideres en la industria, como New Holland, Masey Ferguson, Jhon Deere, fiat y más. En esta sección nuestros clientes pueden encontrar, desde equipos agrícolas de cualquier índole hasta accesorios, para cualquier tipo de necesidad. Examine las categorías relacionadas.',

	// Tractores
	'categoria_28' => 'Expoindustri le ofrece tractores para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de tractores en las marcas más reconocidas en el mercado, como Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Cosechadoras agrícolas
	'categoria_29' => 'Expoindustri le ofrece maquinas cosechadoras para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de cosechadoras agricolas en las marcas más reconocidas en el mercado, como Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger.¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Máquina para Heno y forraje
	'categoria_30' => 'Expoindustri le ofrece maquinas para heno y forraje para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de máquinas para heno y forraje en las marcas más reconocidas en el mercado, como Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger.¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Maquinaria de labranza
	'categoria_31' => 'Expoindustri le ofrece maquinaria de labranza para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de maquinaria de labranza en las marcas más reconocidas en el mercado, como Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger.¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Otros
	'categoria_32' => 'Expoindustri le ofrece todo tipo de accesorios y repuestos para distintos tipos de maquinaria agricola. En esta subcategoría usted puede encontrar todo tipo de accesorios y repuestos para la maquinaria agricola de las marcas más reconocidas en el mercado, como Jhon Deere, Massey Ferguson, New Holland, Fiat, Challenger.¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',


	// ========================================== MONTACARGA ==========================================
	'categoria_33' => 'Expoindustri tiene diversas alternativas de montacargas tanto nuevos como usados disponibles para la venta. Con productos de fabricantes lideres en la industria, como Cat, Toyota, Kalmar, Hyster y más. En esta sección nuestros clientes pueden encontrar, desde montacargas de cualquier índole hasta accesorios y repuestos, para cualquier tipo de necesidad. Examine las categorías relacionadas.',

	// Montacargas
	'categoria_34' => 'Expoindustri le ofrece montacargas para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de montacargas en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Gruas
	'categoria_35' => 'Expoindustri le ofrece grúas para distintos tipos de trabajo. En esta subcategoría usted puede encontrar todo tipo de grúas en las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',
	// Otros
	'categoria_36' => 'Expoindustri le ofrece todo tipo de accesorios y repuestos para distintos tipos de montacargas y grúas. En esta subcategoría usted puede encontrar todo tipo de accesorios y repuestos para montacargas y gruas de las marcas más reconocidas en el mercado, como Caterpillar, Volvo, Komatsu, Jhon deere, JCB, Dynapac¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',


	// ========================================== OTROS ==========================================
	'categoria_37' => 'En esta sección podrá encontrar distintos tipos de herramientas, accesorios y repuestos de las marcas mas prestigiosas en su rubro, tanto en productos nuevos como usados. Examine las categorías relacionadas.',

	// Otros
	'categoria_38' => 'En esta sección podrá encontrar distintos tipos de herramientas, accesorios y repuestos de las marcas mas prestigiosas en su rubro, tanto en productos nuevos como usados. ¡Si tiene alguna consulta, no dude en ponerse en contacto con nosotros!',


];