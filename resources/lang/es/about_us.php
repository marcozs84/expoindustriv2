<?php
return [
	'history' => 'Historia',
	'history_content' => 'Como inicio todo. ',

	'process_flow' => 'Flujo del proceso', // esta en messages
	'process_flow_content' => 'Eche un vistazo a nuestro video sobre el flujo del proceso.', // esta en messages

//	'duty_brokers' => 'Tullmäklare',
//	'duty_brokers_content' => 'För att få den hjälp som behövs vid din tullklarering.',

	'security' => 'Seguridad',
	'security_content' => 'Porque su tranquilidad es lo mas importante, proteja su inversión con nosotros.',

	'sustainability' => 'Sostenibilidad',
	'sustainability_content' => 'Nuestros esfuerzos para el presente y el futuro.',


	// --------------------------------------

	'history_popup_title' => 'Nuestra historia',
	'history_popup_content' => '
	<p>ExpoIndustri es una combinación de un portal web avanzado y un equipo experimentado que facilita la venta de maquinaria industrial y de construcción entre Europa y América Latina.</p>
	<p>Con nosotros puede obtener diferentes tipos de soluciones, desde encontrar un socio para su empresa en América Latina hasta vender / comprar máquinas directamente desde nuestro portal web.</p>
	<p>Es tan simple como "comprar ropa en línea", esto gracias a la calculadora de precios finales de nuestro portal web (que calcula las tarifas del transporte, la logística y todos los costos relacionados, desde Europa hasta el destino elegido) mostrando el precio total final , así como al gran equipo de trabajo detrás de todo el proceso con más de 10 años de experiencia haciendo todo el trabajo duro para eliminar todas las complicaciones que conlleva el proceso de exportación, así como proporcionar seguridad para el proveedor y el comprador.</p>
	',

	'process_flow_popup_title' => 'Flujo de proceso',
	'process_flow_popup_content' => '', // video

	'security_popup_title' => 'Trygghet',
	'security_popup_content' => '
<p>Para ExpoIndustri la tranquilidad de sus clientes es lo MAS importante, es por eso que enfoca todos sus esfuerzos en:</p>
<ul class="fa-ul">
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Verificar la existencia real del equipo/maquinaria que el comprador desea adquirir.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Realizar verificaciones y emitir informes técnicos especiales al equipo/maquinaria, si así el comprador lo solicitase. (<a href="javascript:;">vea aquí el formulario</a>).</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Realizar los procesos de transporte terrestre y marítimo cuidadosamente,con personal capacitado para mantener el estado original en el que el comprador adquirió la maquinaria a través de ExpoIndustri.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Informar sistemáticamente al comprador sobre el estado de su proceso de compra.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Cumplir con la entrega de la mercadería en la fecha mencionada.</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Contar con oficinas y personal capacitado en los países mencionados, (<a href="javascript:;">ver nuestras oficinas</a>)</li>
	<li><span class="fa-li"><i class="fa fa-check"></i></span>Asegurar su mercadería a través de seguros adicionales ,si asi el comprador lo solicita.</li>
</ul>
',

	'sustainability_popup_title' => 'Sostenibilidad',
	'sustainability_popup_content' => '
<p>En ExpoIndustri nuestra energía la direccionamos para que todos, tanto en Europa como en Sudamerica, obtengamos el beneficio que buscamos, en calidad, precios y eficiencia en los servicios.</p>
<p>Nuestra <strong>MISIÓN</strong> es la de facilitar y simplificar la vida de nuestros clientes al momento de vender/comprar entre estos dos continentes, el equipo/maquinaria requerido, con procesos eficientes y eficaces.</p>
<p>Nuestra <strong>VISIÓN</strong> es la de llegar a todas las partes del mundo, permitiendo y facilitando los intercambios comerciales industriales, tanto para empresa, como para personas particulares, manteniendo el mismo grado de simplicidad.</p>
<p>Nuestra <strong>ESTRATEGIA</strong> radica en ofrecer ambientes laborales agradables para ofrecer servicios de alta calidad, trabajando para generar las soluciones a medida para cada uno de nuestros clientes y así permitir un uso eficiente y productivo de los recursos mientras nos esforzamos por conseguir nuestra visión.</p>
',
	'broker_popup_content' => 'El acuerdo que tenemos con las siguientes empresas aduaneras le facilitara el proceso de desaduanizacion de su maquinaria.
<br><br>Contacte a la agencia mas cercana.',



];