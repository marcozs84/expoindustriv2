<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'e_mail' => 'Correo electrónico',
    'password' => 'Contraseña',
	'password_confirmation' => 'Confirme su contraseña',
    'firstname' => 'Nombres',
    'lastname' => 'Apellidos',
    'telephone' => 'Telefono',
	'country' => 'Pais',
	'select_country' => 'Seleccione su país',
	// ---------- layout/default
	'my_orders' => 'Mis ordenes',
	'my_account' => 'Mi cuenta',
	'logout' => 'Salir',
	// ---------- TOP MENU
	'import_export' => 'Importación y distribución',
	'home' => 'Inicio',
	'about_us' => 'Sobre nosotros',
	'payment_methods' => 'Métodos de pago',
	'questions' => 'Preguntas',
	'contact_us' => 'Contáctenos',
	'contact_us2' => 'Contáctenos',
	// ---------- Lenguajes
	'spanish' => 'Español',
	'english' => 'Inglés',
	'swedish' => 'Sueco',
	// ---------- loginForm
	'welcome' => 'Bienvenido',
	'log_in' => 'Ingresar',
	'register' => 'Registrarse',
	'create_account' => 'Crear una cuenta',
	'forgot_password' => '¿Olvidó su contraseña?',
	'remember_me' => 'Recordarme',
	'send' => 'Enviar',
	'have_account' => 'Ya tengo una cuenta',
	// ---------- CONFIRM CUENTA
	'confirm_cuenta' => 'Confirmacion de cuenta',
//	'questions' => 'Preguntas',
	// ---------- CONTACT FORM
//	'contact_us' => 'Contact Us',
	'contact_form' => 'Formulario de contacto',
//	'firstname' => '',
	'contact_form_info' => 'Quiere vender o comprar, consulte con uno de nuestros expertos.',// 'Nuestro equipo internacional tiene años de experiencia  en la industria,  por lo que sabemos muy bien lo que hacemos. Haga su consulta!',
	'subject' => 'Asunto',
	'message' => 'Mensaje',
	'send_message' => 'Enviar mensaje',
	'our_contacts' => 'Nuestros contactos',
	'social_networks' => 'Redes Sociales',
	// ---------- SEARCH TERMS
	'search_by_category' => 'Busque por categoría',
	'transportation' => 'Transporte',
	'equipments' => 'Equipos',
	'industrial' => 'Industrial',
	'agriculture' => 'Agricultura',
	'forklifts' => 'Montacargas',
	'others' => 'Otros',
	'prefered_brands' => 'Marcas preferidas',
	// ---------- COMMON LAYOUT DEFAULT - FOOTER
	'site_map' => 'Tips', //'Mapa del sitio',
	'information_for' => 'Consejos para :subject',
	'announcers' => 'Anunciantes',
	'buyers' => 'Compradores',
	'information_for_announcers' => 'Consejos para <b>Anunciantes</b>',
	'information_for_buyers' => 'Información para <b>Compradores</b>',
	'frequent_questions' => 'Preguntas frecuentes',
//	'payment_methods' => 'Métodos de pago',
	'legal_terms_conditions' => 'Términos Legales y condiciones',
	'privacy_policy' => 'Politica de privacidad', // Política de privacidad
//	'about_us_footer' => '<b> ExpoIndustri </b> es un portal de anuncios clasificados donde podrá encontrar maquinaria y herramientas de distintos rubros, tanto nuevo como usados, desde Europa para Sudamérica, en el destino final que usted elija, avalados y con la garantía de más de 10 años de experiencia en el sector de la importación-exportación entre estos dos continentes.',
//	'about_us_footer' => '<b> Expoindustri.com </b> es un portal web para empresas en Europa que desean aumentar sus ventas de una manera SIMPLE, EFECTIVA Y ECONOMICA, a un gran mercado potencial "América Latina". <br><br> Detrás del portal web hay un equipo con más de 10 años de experiencia que hace todo el trabajo duro para eliminar todas las complicaciones, como el proceso de exportación, la carga y el envío. Somos la seguridad de un negocio exitoso, contáctenos y le contamos más.',
	'about_us_footer' => '<b> Expoindustri.com </b> es un portal para empresas en Europa que desean comprar o vender de la manera más simple y fluida posible. <br><br> Detrás del portal web hay un equipo con más de 20 años de experiencia haciendo todo el trabajo por usted, desde recoger la máquina en cualquier lugar de Europa, hasta la entrega en todos los destinos, incluyendo Sudamérica.',
	'central_office' => 'Oficina central',
	'phone' => 'Teléfono',

	'contact_form_sent' => 'Mensaje enviado',
	'contact_form_sent_info' => 'Muchas gracias por su contacto, un representante de nuestro equipo se pondrá en contacto con usted en la brevedad posible.',
	'sending' => 'Enviando',
	'sent' => 'Enviado',
	'services' => 'Servicios',
	'our_offices_southamerica' => 'Nuestras oficinas en Sudamerica',
	'find_your_machine' => 'Encuentra tu máquina',
	'search' => 'Buscar',
	'order_by' => 'Ordenar por',
	'popular' => 'Popular',
	'new_arrival' => 'Recien llegados',
	'discount' => 'Descuento',
	'price' => 'Precio',
	'items_found' => 'Hemos encontrado :number elementos',
	'all_f' => 'todas',
	'all_m' => 'todos',
	'select_category' => 'Seleccione una categoria',
	'select_subcategory' => 'Seleccione una subcategoria',
	'criteria' => 'Criterio',
	'category' => 'Categoria',
	'sub_category' => 'Sub Categoria',
	'brand' => 'Marca',
	'from' => 'Desde',
	'to' => 'Hasta',
	'year' => 'Año',
	'hi_tweet' => 'Hola!, mira esta máquina',
	'calculator' => 'Calculadora',
	'additional_info' => 'Información Adicional',
	'rating_reviews' => 'Rating & Revisiones',
	'add_favorites' => 'Agregar a favoritos',
	'quote_final_destination' => 'Cotizar precio en destino final',
	'select_country_first' => 'Primero debe seleccionar su país',
	'select_preferred_zone' => 'Seleccione la zona de su preferencia',
	'publish' => 'Publicar',
	'model' => 'Modelo',
	'next' => 'Siguiente',
	'previous' => 'Anterior',
	'subcategory' => 'Sub-categoria',
	'required' => 'Requerido',
	// ---------- currency
	'currency' => 'Moneda',
	'dollars' => 'Dólares',
	'euros' => 'Euros',
	'crowns' => 'Corona Sueca',
	// ---------- status
	'active' => 'Activo',
	'inactive' => 'Inactivo',
	'approved' => 'Aprobado',
	'rejected' => 'Rechazado',
	'awaiting_approval' => 'Esperando aprobación',
	'reported' => 'Reportado',
	'draft' => 'Borrador',
	'removed' => 'Borrado',
	// ---------- CLIENT TYPES
	'client' => 'Cliente',
	'provider' => 'Proveedor',
	'employee' => 'Empleado',
	'employer' => 'Empleador',


	'dz_upload' => 'Arrastre aquí sus archivos para subir',
	'invalid_image_file' => 'No es un archivo de imágen válido',
	'invalid_file' => 'No es un archivo válido',
	'remove' => 'Remover',
	'machine_location' => 'Ubicacion de la maquinaria',
	'machine_location_descr' => '<a target="_blank" href=":route">(Ver política de protección de datos)</a>', //'La dirección proporcionada sera utilizada y conocida solo por ExpoIndustri <a target="_blank" href=":route">(Ver política de protección de datos)</a>',
	'data_protection_policy' => 'Politica de protección de datos',
	'city' => 'Ciudad',
	'postal_code' => 'Código Postal',
	'location' => 'Ubicación',
	'address' => 'Dirección',
	'findInMap' => 'Buscar en mapa',
	'transport_info' => 'Información para el transporte',
	'transport_info_descr' => 'Por favor facilitenosinformaciónpara el cálculo del transporte.',
	'longitude_mm' => 'Longitud (mm)',
	'width_mm' => 'Ancho (mm)',
	'height_mm' => 'Alto (mm)',
	'weight_brute' => 'Peso bruto (Kg)',
	'palletized' => 'Paletizado',
	'confirm_removal' => 'Desea remover la imagen seleccionada?',
	'user_info' => 'Información de usuario',
	'user_info_descr' => 'Quién está publicando esta máquina?',
	'select_your_category' => 'Seleccione una categoria',
	'select_your_category_descr' => 'Agrupe su maquina junto con otras del mismo tipo.',
	'basic_information' => 'Información básica',
	'basic_information_descr' => 'Marca y modelo',
	'new_announce' => 'Su nuevo anuncio',
	'click_next' => 'Haga click en Siguiente para continuar.',
	'payment_options' => 'Opciones de pago',
	'payment_options_descr' => '¿Cómo desea pagar su anuncio?',
	'payment_announce_message' => 'Para mantener su anuncio activo en nuestro sitio, ofrecemos una cuota diaria de <span id="txt_price" style="font-weight:bold;">:price (Impuestos incluidos)</span> mínimo 180 días, por favor seleccione cuántos días desea que su anuncio se muestre y luego proceda con el pago en alguna de las siguientes modalidades.',
	'payment_transfer' => 'Transferencia a cuenta de banco',
	'payment_transfer_descr' => 'Usted puee transferir el dinero a la siguiente cuenta: <br><br> <b>1-156345698545</b><br><b>Banco LoremDolar</b>',
	'payment_creditcard' => 'Pago con tarjeta de credito',
	'payment_creditcard_descr' => 'Por favor seleccione la tarjeta con la que desea pagar.',
	'payment_faktura' => 'Pago con factura',
	'payment_faktura_descr' => 'Cuando registre su anuncio, le enviaremos una factura con las condiciones de pago a 10 días.',
	'payment_cash' => 'Llamada telefónica',
	'payment_cash_descr' => 'Los números disponibles para este efecto son: <br><br> <b>1-125465488454</b><br> <b>1-125465488454</b>.',
	'days' => 'Days',
	'accept_terms' => 'He leído y acepto los :link.',
	'legal_terms' => 'Términos legales y condiciones',
	'publish_pay' => 'Publique su anuncio',
	'payment_trustly' => 'Usar Trustly',
	'payment_trustly_descr' => 'Usted puede realizar su transferencia a través de Trustly.',
	'verify_ccard' => 'Por favor verifique los datos de su tajeta.',
	'announce_detail' => 'Detalles del anuncio',
	'announce_detail_descr' => 'A mayor detalle, mayores posibilidades de encontrar interesados.',
	'provide_card_info' => 'Por favor ingrese la información de su tarjeta.',
	'card_number' => 'Número de tarjeta',
	'expiration_date' => 'Fecha de expiracion',
	'security_code' => 'Código de seguridad',
	'time_extended_title' => 'Anuncio por tiempo extendido',
	'time_extended_description' => 'Si realiza un contrato de más de 100 días su anuncio será renovada automáticamente cada 50 días, apareciendo nuevamente en la página principal de nuestro sitio, en la sección <b>"Anuncios más recientes"</b>',
	'loading' => 'Cargando',

	'info_buyers_title' => '¡IMPORTANTE! USTED COMO CLIENTE',
	'info_announcers_title' => 'CONSEJOS PARA USTED COMO ANUNCIANTE!',
	'no_brand' => 'Sin marca',
	'no_model' => 'Sin modelo',
	'no_product' => 'Sin producto',

	'product' => 'Producto',
	'remove_favorite' => 'Quitar de mis favoritos',
	'is_favorite' => 'Es favorito',
	'my_favorites' => 'Mis favoritos',
	'my_favorites.leftmessage' => 'Sus anuncios marcados como favoritos.<br>Por favor recuerde que todos los precios están sujetos al tipo de cambio del día.',
	'my_announces' => 'Mis anuncios',
	'my_announces.leftmessage' => 'Todo lo que necesita en un solo lugar, por favor déjenos saber si necesita asistencia.',
	'no_active_users' => 'No hay usuarios activos.',
	'favorites.leftmessage' => '',
	'request_quote' => 'Solicitar oferta', //'Crear pedido', //'Solicitar cotización',

	// Mensaje mostrado cuando un cliente envía una cotización desde la página de producto.
//	'quote_requested_correctly' => 'Su cotización enviada correctamente.  Una copia de esta orden fué enviada a su cuenta de correo, un consultor se pondrá en contacto con usted en la brevedad posible.',
	'quote_requested_correctly' => 'Muchas gracias, hemos recibido su pedido.  Una copia ha sido enviada a su cuenta de correo y un consultor se pondrá en contacto con usted a la brevedad posible.',

	// Mensaje mostrado cuando un cliente envía exitosamente una nueva publicación a través del formulario Publish Here.
	'publish_success_msg' => 'Nuestro equipo revisará el anuncio y este será publicado en máximo 2 horas, caso contrario nos pondremos en contacto con usted en la brevedad posible.',
	'publish_successful' => 'Publicación exitosa',
	'price_in_europe' => 'Precio en Europa',
	'owner_price' => 'Precio del propietario',
	'exchange_rate' => 'Tipo de cambio',

	'includes_taxes' => 'Impuestos incluidos',
	'disabled' => 'Deshabilitado',
	'select_preferred_transport' => 'Debe seleccionar un método de transporte',
	'publish_review_announce' => 'Anuncio incompleto',
	'publish_missing_images' => 'Por favor recuerde que debe proveer al menos 1 imagen para su anuncio.',
	'recent_announces' => 'Publicaciones más recientes',
	'be_first_visitor' => 'Sea el primero en visitarlas.',

	'most_visited' => 'Los más visitados.',
	'most_visited_small' => 'Anuncios que han llamado mucho la atención.',

	'company' => 'Empresa',
	'company_name' => 'Nombre de la empresa',
	'company_nit' => 'Identificación tributaria',

	'longitude' => 'Longitud',
	'width' => 'Ancho',
	'height' => 'Alto',

	'my_orders.leftmessage' => 'Listado de ordenes que se han realizado en nuestro sitio.', //'Listado de ordenes que se han realizado en nuestro sitio. <br> Todo lo que necesita en un solo lugar.  Por favor no dude en contactarnos si requiere asistencia.',
	'order_detail' => 'Detalle de la orden',
	'payment' => 'Pago',
	'payments' => 'Pagos',
	'description' => 'Descripcion',
	'amount' => 'Monto',
	'dimention' => 'Dimension',
	'dimentions' => 'Dimensiones',
	'weight' => 'Peso',

	'order' => 'Orden',
	'orders' => 'Ordenes',
	'save' => 'Guardar',
	'logo_background_color' => 'Color de fondo (logo)',
	'no_image_available' => 'Sin imagen disponible',
	'urlSite' => 'Sitio web',
	'urlFacebook' => 'Facebook',
	'urlTwitter' => 'Twitter',
	'urlGooglePlus' => 'Google Plus',

	'search_alert' => '¿No encontró lo que buscaba?', //'Contactar a un consultor',
	'comments' => 'Comentarios',

	'marked_fields_required' => 'Los campos remarcados son requeridos',
	'comments_sent' => 'Solicitud enviada',
	'su_consulta_enviada_correctamente' => 'Su mensaje fue enviado correctamente.',

	'search_alert_msg' => 'No pudo encontrar la maquinaria que necesita en nuestro sitio web?  No se preocupe!! <br>El equipo de ExpoIndustri esta para brindarle apoyo con sus múltiples canales de búsqueda, para encontrar cualquier tipo de maquinaria en el mercado Europeo.',
	'search_alert_msgfrm' => 'Por favor ingrese toda la información solicitada y cuéntenos cual es su solicitud específica. En breve nos pondremos en contacto con usted.',

	'our_services' => 'Nuestros servicios',
	'our_services_slogan' => '¡Mucho más que una solución!<br>Somos uno más en su equipo de trabajo', //'¡Mucho más que una solución!<br>Trabajamos por la prosperidad de SU empresa',
	'our_services_slogan_short' => 'Trabajamos por la prosperidad de SU empresa',

	'about_expoindustri' => 'Acerca de ExpoIndustri',

	'what_we_do' => 'Qué le ofrecemos',
	'all_you_need' => 'Todo lo que necesita saber acerca de que <b>ÉS</b> y que <b>LLEGARÁ A SER</b> ExpoIndustri.',

	'publish_a_machine' => 'Publicar una maquina',
	'leave_your_data_we_will_contact_you' => 'Por favor deje sus datos y nos pondremos en contacto con usted.',
	'advertisement' => 'Publicidad',
	'look_advertise_options' => 'Mire nuestras diferentes opciones de publicidad.',
	'make_click_here' => 'Haga click aquí',

	'my_account_msg' => 'Modifique una orden, o haga seguimiento a su cargamento y actualice su información personal.
	<br><br>
	Todo lo que necesita en un solo lugar.  Por favor no dude en contactarnos si requiere asistencia.',

	'account_config' => 'Configuración de cuenta',
	'saved_announces' => 'Favoritos',
	'udate_mail_password' => 'Actualizar correo y contraseña',
	'payments_finance' => 'Pagos & Finanzas',
	'list_enabled_cards' => 'Listar mis tarjetas de crédito habilitadas',
	'support' => 'Soporte',

	'top_message_unconfirmed_title' => '¡Confirme su cuenta!',
	'top_message_unconfirmed_msg' => 'Hemos enviado un correo electrónico para confirmar su cuenta: <b>:email</b>. Confirme su cuenta dentro de las 24 horas. (compruebe su correo no deseado, ya que puede terminar allí)',

	'continue' => 'Continuar',
	'account_confirmed' => 'Cuenta confirmada',
	'account_confirmed_msg' => '
		Bienvenido y muchas gracias por confirmarsu cuenta!<br>
		Esperamos que su experiencia en ExpoIndustri sea muy positiva.<br>
		Por favor no dude en contactarnos ante cualquier duda o inquietud.<br><br>
		
		Sincerely,<br>
		<strong>ExpoIndustri Team</strong>
	',

	'register_invalid_token' => 'Token inválido',
	'register_invalid_token_msg' => 'El token provisto es inválido, es posible que su cuenta ya se encuentre activa ó que haya sido inhabilitada por algúna razón, por favor contáctenos a través de nuestro <a href=":linkContact">formulario de contacto</a>.',
	'register_confirmation_error' => 'Error durante la confirmación de cuenta',
	'register_confirmation_error_msg' => 'A ocurrido un error durante el proceso de confirmación, nuestro equipo ha sido notificado y se pondrán en contacto con usted en la brevedad posible.',

	'gallery' => 'Galería',
	'time_extended_checkbox' => 'Coloque su anuncio nuevamente en las primeras posiciones el primero de cada mes automáticamente por solo :price SEK.',
	'close' => 'Cerrar',

	'payment_dibs' => 'Pago a través de DIBS',
	'all_rights_reserved' => 'Todos los derechos reservados',
	'search_alert_title' => 'No encontró lo que buscaba?',
	'forgot_message_sent' => 'Mensaje enviado correctamente, por favor revise su bandeja de entrada. Si por alguna razon no le llego a su bandeja, favor revisar en \"Correo no deseados\"',
	'forgot_text' => 'Por favor ingrese su correo electrónico, le enviaremos un enlace para actualizar su contraseña.',
	'user_not_found' => 'Usuario no encontrado',
	'password_reseted' => 'Su contraseña fué actualizada correctamente y ahora será redirigido a su cuenta privada.<br>Si no es redirigido automaticamente por favor utilice el siguiente enlace a ',
	'old_password_different' => 'Su contraseña actual no coincide.',
	'password_updated_succesfully' => 'Contraseña actualizada correctamente',

	'current_password' => 'Contraseña actual',
	'new_password' => 'Nueva contraseña',
	'password_update' => 'Actualizacion de contraseña',
	'current_email' => 'E-mail actual',
	'update_email' => 'Actualizar e-mail',
	'update_email_info' => 'Por favor recuerde, este es su nombre de usuario actual para acceder a su cuenta.',
	'company_phone' => 'Número de teléfono', //'Teléfono de su empresa',
	'sweden' => 'Suecia',

	'see_more' => 'Ver más',
	'process_flow' => 'Flujo del proceso',
	'process_flow_content' => 'Para conocer paso a paso del proceso de logística.',
	'duty_brokers' => 'Despachantes de aduana',
	'duty_brokers_content' => 'Para obtener la ayuda necesaria en su gestion de desaduanizacion.',

	'aboutus_slogan' => 'Quienes somos!  Nuestra historia y mucho mas',
	'goodfaith' => 'Doy fe de que la maquina que estoy publicando se encuentra libre de deudas o compromisos con terceros. ',

	'index_process_flow_content' => 'Para conocer paso a paso todo el proceso.',

	'index_brokers_title' => 'Despachantes de aduana',
	'index_brokers_content' => 'Para obtener la ayuda necesaria en su gestión de desaduanización en Sudamérica.',

	'index_security_title' => 'Transacciones seguras',
	'index_security_content' => 'Por su seguridad y tranquilidad, sus tarjetas y pagos son gestionados a través de <a href="https://www.stripe.com" target="_blank">Stripe.com</a>.',

	'register_report_token_invalid' => 'El token provisto es inválido, por favor contactenos a través de nuesto :contact_form',
	'register_report_user_confirmed_title' => 'Usuario activado anteriormente',
	'register_report_user_already_confirmed' => 'Su usuario ya ha sido confirmado anteriormente, si desea darse de baja por favor haga su solicitud a través de nuestro :contact_form',
	'register_report_unsubscribe_title' => 'Suplantación de identidad reportada exitosamente',
	'register_report_unsubscribe' => 'Muchas gracias por reportar esta suscripción, este mensaje ha sido enviado
a un representante de nuestro equipo quien dará de baja esta cuenta de correo.
<br><br>
Reciba un cordial saludo,<br>
<b>Equipo de ExpoIndustri</b>',
	'disabled_account' => 'Cuenta desactivada',
	'register_report_disabled_account_mail' => 'Muchas gracias por reportar esta suscripción, esta cuenta ya ha sido deshabilitada en nuestro sistema.
<br><br>
Reciba un cordial saludo,<br>
<b>Equipo de ExpoIndustri</b>',

	'delete_message_not' => 'No se pudo eliminar el/los registro(s). Se eliminó :count de :total registros',
	'service_request_mistype' => 'La consulta debe ser uno de los siguientes tipos: banner, search, inspection, support, istore.',
	'record_created' => 'Registro creado',
	'added_favorites' => 'Agregado a favoritos',
	'preorder_reported' => 'Pre-orden reportada',
	'preorder_reported_message' => 'Muchas gracias por reportar, esta publicación ha sido notificada a uno de nuestros consultores.',
	'you_might_also_like' => 'Tambien podría interesarle',
	'related_products_category' => 'Maquinarias relacionados de la misma categoria',

	'quote_equipment_in_final_destination' => 'Cotizacion del equipo en destino final',
	'calculator_text' => '<p>Con nuestra practica calculadora, podrá obtener el precio final de su maquinaria, puesta en el destino que haya elegido y con todos los servicios adicionales solicitado por usted.</p>
	<p>Ante cualquier duda, comuníquese con nosotros AHORA mismo a travez de nuestro :form_link</p>',

	'price_machine_in_europe' => 'Precio de la maquinaria', //'Precio de la maquinaria en Europa',
	'require_additional_info' => '¿Requiere información adicional?',
	'technichal_information' => 'Informe tecnico',
	'request_require_payment' => 'La solicitud de este informe requiere pago adelantado',
	'require_extra_assurance' => '¿Requiere un seguro extra para su maquinaria hasta el destino final?',
	'extra_insurance' => 'Seguro extra',
	'how_transport_machine' => '¿Como desea transportar su maquina?',
	'soon_at_expoindustri' => 'Proximamente en <br><b>ExpoIndustri.com</b>',
	'select_final_destination' => 'Seleccione el destino final',
	'price_in_final_destiny' => 'Precio en destino final',
	'how_guarantee_buy' => 'Distribuidor certificado', //'¿Como garantizar su compra?',
	'guarantee_content' => '<p>
		Toda adquisición de una maquinaria vía ExpoIndustri es respaldada por una factura, de acuerdo a nuestros términos legales y condiciones.
	</p>
	<p>
		Una vez que envía la solicitud a través nuestra página, nosotros controlamos que la maquinaria siga disponible, una vez controlada y confirmada la existencia de la misma, le enviaremos una Pre factura y los pasos a seguir para proceder con el pago seguro.
	</p>
	<ul class="fa-ul">
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			ExpoIndustri garantiza la compra.
		</li>
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			100% transporte seguro
		</li>
		<li>
			<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
			Respaldado por personal altamente capacitado.
		</li>
	</ul>',
	'is_secure_payment' => 'Pagos seguros!', //'¿Es seguro realizar el pago?',
	'is_secure_payment_content' => '<p>
	ExpoIndustri trabaja con las empresas mas reconocidas para que usted tenga la tranquilidad de realizar los pagos de una forma segura.
	</p>
	<p>
	Por eso ExpoIndustri le ofrece:
	</p>
	<ul class="fa-ul">
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Simple transferencia bancaria
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Pagar en su zona de confort
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Tarjeta de bebito/Credito
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		PayPal
	</li>
	<li>
		<span class="fa-li"><i class="fas fa-check text-primary"></i></span>
		Stripe
	</li>
	</ul>',

	'usd' => 'USD',
	'eur' => 'EUR',
	'sek' => 'SEK',

	'precios_not_match' => '¿Los precios no coinciden?',
	'precios_not_match_msg' => 'Recuerde que los precios son calculados en base al tipo de cambio actual.',

	'validity' => 'Vigencia',
	'visits' => 'Visitas',
	'state' => 'Estado',
	'publication_date' => 'Fecha de publicación',
	'doubts_contact_us' => '¿Dudas? Contáctenos!',
	'go_to_contact' => 'Escríbenos aquí!', //'Ir al formulario de contacto',
	'public_profile' => 'Perfil público',
	'profile_info' => 'Información de perfil',
	'public_profile_msg' => 'Descripción de su empresa que será visible en su perfil público.',

	'password_msg' => 'Recuerde que su contraseña debe tener al menos 6 caracteres. Si tiene problemas recordando su contraseña consulte a uno de nuestros consultores, con gusto atenderán su solicitud.',
	'all_you_need_one_place' => 'Todo lo que necesita en un solo lugar. Por favor no dude en contactarnos si requiere asistencia.',
	'my_credit_debit_cards' => 'Mis tarjetas de debito/credito',
	'my_credit_debit_cards_msg' => 'Tarjetas utilizadas recientemente, debe tener al menos 1 tarjeta registrada.',
	'technical_info_company' => '<b>ExpoIndustri.com</b> utiliza una empresa externa experimentada en el área de revisiones técnicas de alto nivel, con un personal altamente capacitada, para brindar a nuestros clientes un informe al detalle. lea mas sobre
		<a href="https://www.mevas.eu/" target="_blank">www.mevas.eu</a>',
	'insurande_for_machine' => 'Seguro para maquinaria',
	'publish_a_machine_soon' => 'Los anuncios públicos estarán disponibles a partir del 14 de Noviembre de 2018.',

	'provider_profile' => 'Perfil de proveedor',
	'announcements_provider' => 'Anuncios por este proveedor',

	'office_name' => 'Nombre de sucursal',
	'company_address' => 'Ingrese más información',
	'preview_link' => 'Si desea previsualizar su perfil, haga <a href=":link" class="btn btn-warning" style="font-weight:bold;">click aquí</a>',
	'company_information' => 'Información de la empresa',
	'company_name_required' => 'Nombre de la empresa es requerido.',
	'company_nit_required' => 'Código de Identificación tributaria es requerido.',
	'select_main_office_location' => 'A continuación puede ingresar la dirección, persona de contacto, correo electrónico', //'Por favor provea la dirección de su oficina central y luego precione <b>“Buscar”</b> para ubicarla en el mapa, si no fuera encontrada intente agregando más detalles ó haga click en la ubicación exacta directamente en el mapa.',
	'add_secondary_addresses' => 'Añadir direcciones secundarias',
	'filial_name' => 'Nombre de sucursal',
	'suggested_dimention' => 'Las dimensiones sugeridas son: <br>550px x 100px.',
	'announce_here' => 'Anuncie aquí',
	'go_to_invoice' => 'Ir al recibo de este anuncio',
	'invoice' => 'Factura',
	'wanna_remove_announcement' => 'Realmente desea eliminar este anuncio?',
	'announcement_removed' => 'Su anuncio fué eliminado!',
	'internal_error_title' => 'Oops!',
	'internal_error_message' => 'A ocurrido un error en nuestro sistema y ha sido reportado a nuestro equipo.  Por favor envíenos sus comentarios a info@expoindustri.com, con gusto atenderemos sus consultas.',
	'charges_technical_report' => 'Los cargos por el informe técnico serán descontados al completar la orden de compra.',
	'handling_fee' => 'Costos operativos',
	'handling_fee_description' => 'Costos necesarios, en los que se incurre, para poder ejecutar el servicio en su totalidad por actividades tales como preparación de documentación, proceso de importación, exportación, carguío y despacho, entre otros.',
	'how_we_do_it' => '¿Como lo hacemos?',
	'use_different_card' => 'Utilizar una tarjeta diferente.',
	'availability' => 'Disponibilidad',
	'create_announce' => 'Crear nuevo anuncio',
	'price_southamerica_only' => 'Si solo quieres que el precio sea visible en Sudamérica.',
	'contact_consultant' => 'Contactar a un consultor',
	'product_contact_title' => 'Solicitud de mayor información',
	'product_contact_message' => 'Deseo recibir más información relacionada a este producto:<br><h2 style="font-size:25px;">:product_name</h2>',
	'price_not_available' => 'Pedir Cotizacion', //'Precio no disponible',
	'news' => 'Noticias',
	'news_source' => 'Fuente',
	'ver_noticia' => 'Lea la noticia completa aquí',

	'active_subscriptions' => 'Planes activos actualmente',
	'renew' => 'Renovar',
	'requires_action' => 'Requiere su atención',
	'due_date' => 'Vencimiento',
	'view_all_your_plans' => 'Ver todos sus planes',
	'overdue' => 'Vencido',
	'no_active_plans' => 'Usted no está suscrito a ningún plan',

	'our_price' => 'Nuestros Planes',
	'single_announcement_post' => '1 máquina por anuncio',
	'thirty_announcements' => 'Paquete de 30 anuncios',
	'unlimited_announcements' => 'Anuncios ilimitados',

	'time_defined_user' => 'Periodo definido por el usuario',
	'gallery_unlimited_images' => 'Galería con imágenes ilimitadas',
	'detailed_machine_description' => 'Descripción detallada en el anuncio',
	'one_year_availability' => 'Disponibilidad de 1 año',
	'post_an_announcement' => 'Publicar un anuncio',
	'customizable_online_store' => 'Tienda en línea personalizable',
	'link_to_your_site' => 'Enlace a su sitio web',
	'post_logo_address' => 'Publique el nombre de su empresa, logo y dirección',

	'plan_basic' => 'Basico',
	'plan_business' => 'Negocios',
	'plan_plus' => 'Premium',

	'payment_per_announcement' => 'Pago por anuncio',
	'per_year' => 'por año',
	'buy_now' => 'Comprar ahora',
	'best_prices' => 'Los mejores precios del mercado, <br>aproveche y haga visible sus maquinas en el mercado latinoamericano.',

	'pricing' =>'Planes',
	'minimum_price_required' => 'Precio minimo de venta', //'Precio mínimo requerido',
	'minimum_price_text' => 'El precio minimo sera visible unicamente por nuestros EJECUTIVOS DE VENTAS, para efectos de negociacion.',
	'per_180days' => 'por 180 días',
	'free_banner_bonus' => '1 Banner publicitario', //'Publicidad de banner en expoindustri.com',
	'faq_buyers' => 'PREGUNTAS FRECUENTES DE LOS COMPRADORES',
	'faq_sellers' => 'PREGUNTAS FRECUENTES DE LOS ANUNCIANTES ',
	'taxes' => 'impuestos',
	'product_name' => 'Producto',
	'pay_with_card' => 'Pago con tarjeta',
	'total' => 'Total',
	'payment_faktura_note' => 'Pronto enviaremos la factura a su dirección de facturación.',
	'announcement' => 'Anuncio',
	'announcements' => 'Anuncios',
	'edit' => 'Editar',
	'choose_plan_business_plus' => 'Elija su plán <br><b>NEGOCIOS</b> o <b>PLUS</b>',
	'automatic_translations' => 'Traducción automática al inglés, sueco y español',
	'our_price_slogan' => 'Con nuestros planes adquieres un equipo que trabaja por tí',
	'welcome_to' => 'Bienvenido a',
	'categories' => 'categorias',
	'reach_your_clients' => 'Con nuestro marketing efectivo, puede encontrar a sus clientes en Europa y Sudamerica.',
	'start_selling' => 'Comienza a vender',
	'expo_description' => 'Expoindustri.com es el mercado online que permite a las empresas vender su maquinaria / accesorio de una forma mas eficiente. Trabajamos duro para aumentar tus posibilidades de venta en Europa y Sudamérica y de igual manera darte la mejor experiencia de logística  y distribución.',
	'our_philosophy' => 'Nuestra filosofia',
	'quote_joakim' => 'Un cliente satisfecho es lo que nos impulsa a dar lo mejor de nosotros',
	'our_experience' => 'Nuestra Experiencia',
	'euro_south_market' => 'Mercado Europeo y Sudamericano',
	'logistics' => 'Logística',
	'import_export_process' => 'Proceso de Importación / Exportación',
	'experienced_team' => 'Experimentado equipo internacional',
	'find_your_solution' => 'Encuentra la solución de Expoindustri para tu negocio',
	'sale_3_languages' => 'Vender en 3 idiomas',
	'many_currencies' => 'Elija la moneda en la que desea vender',
	'sale_overseas' => 'Establezca su precio de venta para adaptarse a los diferentes mercados que desea alcanzar',
	'logistic_distribution' => 'Logística y distribución',
	'partners' => 'Nuestros socios',
	'just_single_announce' => '¿Sólo tienes Una maquina para vender?',
	'our_categories' => 'Nuestras categorias',
	'contact_doubts' => '¿Tiene alguna pregunta sobre cómo podemos ayudarlo?',
	'contact_doubts_body' => 'Ayudamos a nuestros clientes diariamente con la venta o compra de máquinas, el proceso de exportación e importación, el soporte logístico, las inspecciones técnicas y más. Llámenos o pase por la oficina en Jönköpingsvägen 27 en Tenhult, Jönköping. Encontrará todos nuestros datos de contacto a continuación.<br>¡Bienvenidos!',
	'managing_director' => 'CEO, Expoindustri', //'Gerente de desarrollo y estratégias', // Propietario
	'expand_your_business' => 'Expande tu negocio',
	'view_explained_video' => 'Ver video explicativo',
	'experienced_salesman' => 'Mercadeo personalizado',
	'distrib_office' => 'Oficina & almacen',
	'terms' => 'Terminos',
	'where_pick_machine' => '',
	'wish_to_pick_it' => 'Deseo recoger la máquina de almacen',
	'provide_final_destination' => 'Provea la dirección de destino',
	'wanna_make_offer' => 'Deseo hacer una oferta',
	'offer' => 'Oferta',
	'wanna_expand_business' => '¿Deseas expandir tu negocio?', //'¿Desea expandir su negocio?',
	'please_accept_terms' => 'Acepto los Términos y condiciones generales y la Política de privacidad',

	'paysuc_thankyou' => 'Gracias por su pedido!',
	'paysuc_transtatus' => 'Estado de su transacción',
	'paysuc_succesful' => 'Completado con éxito',
	'paysuc_referenceno' => 'Número de referencia',
	'paysuc_datetime' => 'Fecha y hora',
	'paysuc_order' => 'Pedido',
	'paysuc_payment' => 'Monto total',
	'paysuc_detail' => 'La transacción tiene los siguientes detalles:',
	'paysuc_needassitance' => 'Si necesita ayuda, puede contactarnos por teléfono al <b>+46 072 393 3700</b>',
	'factdet_fillyourinfo' => 'Complete sus datos de facturación',
	'factdet_factaddress' => 'Dirección de facturación',
	'go_myprofile' => 'Actualizar mi perfil',
	'go_myaccount' => 'Mi cuenta',
	'place' => 'Lugar',
	'status' => 'Estado',
	'acc_serv_sales' => '¡Encontramos compradores para su máquina!',
	'acc_serv_sales_body' => '¡Sus máquinas se publican en más de 40 países de Europa y América del Sur!',
	'acc_serv_sales_body2' => 'Con nuestro equipo de ventas informamos de manera proactiva. <br>Nuestras redes globales llegan a más de +10 millones de compradores potenciales.',
	'acc_serv_sales2' => '¿Estás listo para ventas adicionales?',
	'acc_serv_sales2_title' => 'Exportar anuncios desde su sitio',
	'acc_serv_sales2_body' => 'Nos complace copiar listas de su sitio web de forma gratuita. La información se actualiza de forma regular y automática.',
	'acc_serv_sales3_title' => '¿Prefieres anunciarte individualmente?',
	'acc_serv_sales3_body' => '¡Con nuestro sistema para completar anuncios nunca ha sido tan fácil! Comience AHORA.',
	'goto_store' => 'Visitar tienda en línea',
	'see_more_desc' => 'Acerca de la empresa', //acerca de empresa
	'hide_more' => 'Ocultar',
	'write_new_password' => 'Por favor introduzca su nueva contraseña',
	'descriptionCompany' => 'Descripción de su empresa',
	'service_requirement' => 'Solicitud de servicio',
	'service_requirement_autoimport' => 'Es necesario que uno de nuestros consultores se contacte con usted para recabar más información, por favor provea para nosotros la dirección de su página y cualquier duda o inquietud adicional en la caja de texto.',
	'transport_sea' => 'Transporte Marítimo',
	'transport_terrain' => 'Transporte Terrestre',
	'price_southamerica' => 'Precio para Sudamérica',
	'price_europa' => 'Precio para Europa',
	'exclude_moms' => 'Sin impuesto',
	'price_europe_message' => 'Precio disponible para clientes en Europa',
	'make_an_offer' => 'Haz una oferta', //'Hacer una oferta',
	'welcome_to_landing' => 'Te ayudamos a crear negocios internacionales', //Te ayudamos a ganar más negocios globales',
	'saved_succesfully' => 'Sus cambios fueron guardados',
	'remember_review' => 'Su anuncio será revisado y habilitado en breve',
	'nosotros_saluda_title' => 'Saluda a nuestros colegas',
	'nosotros_saluda_body' => 'Aquí encontrará las personas que hacen de Expoindustri el consultor líder de Suecia y Sudamérica en la exportación / importación de máquinas. Somos un equipo experimentado e internacional que ayuda a su empresa a encontrar el comprador adecuado. Ofrecemos un servicio personal en combinación con un marketing efectivo que conduce a un negocio rápido, rentable y seguro. Juntos, ayudamos a resolver problemas y responder preguntas sobre exportación-importación de máquinas, procesos logísticos, inspecciones técnicas y todo lo que su empresa necesita.',
	'cargo_joakim' => 'CEO',
	'cargo_emily' => 'Gerente de Ventas',
	'cargo_claudia' => 'Economista contable Suecia / Sudamérica',
	'cargo_carla' => 'Asistente Administrativo Sudamérica',
	'cargo_ernesto' => 'Ejecutivo comercial Sudamérica',
	'cargo_iracema' => 'Economista Contable de América del Sur',
	'cargo_jurgen' => 'Gerente de Logística y Distribución Sudamérica',
	'cargo_marco' => 'Director de tecnología',
	'cargo_jose' => 'Ejecutivo comercial Sudamérica',
	'contact_details' => 'Detalles de contacto',
	'contact_details_body' => '<b>E-mail</b>: info@expoindustri.com<br>
<b>Teléfono:</b> 072393 37 00
<br><br>
<b>EXPOINDUSTRI SWEDEN AB</b><br>
Org. No: 559174-5194',
	'find_here' => 'Encuentra aquí',
	'hours_available' => 'Horario de apertura',
	'hours_list' => '<b>Teléfono</b><br>
Lun - Vie: 08:30 - 17:00<br>
<b>Chat</b><br>
Lun - Vie: 8am - 6pm<br>
<b>Oficina</b><br>
Lun - Jue: 08:30 - 16:00',
	'wanna_contact_us' => '¿Quieres ponerte en contacto con nosotros? Rellene el siguiente formulario y le responderemos lo antes posible.',
	'contact_disclaimer' => 'Al enviar su información personal, acepta que la procesemos de acuerdo con nuestra :link.',
	'privacy_politics' => 'política de privacidad',
	'date' => 'Fecha',
	'interested' => 'Interesados',
	'zone' => 'Zona',
	'newsletter_description' => '¿ESTÁ INTERESADO EN RECIBIR ACTUALIZACIONES SOBRE LOS PROYECTOS, INDUSTRIAS Y MÁQUINAS QUE SOLICITAMOS EN EUROPA Y SUDAMÉRICA? - SUSCRÍBASE A NUESTRO BOLETÍN DE NOTICIAS!',
	'business_opportunity_southamerica' => 'Oportunidades de negocio en Sudamerica',
	'have_stock_machine' => '¿Tiene un stock de máquinas que quiere vender en Sudamérica?',
	'machine_storage' => 'Almacen de máquinas',
	'send_offer' => 'Envíenos su oferta',
	'request_final_price' => 'Solicite precio en destino final',
	'request_final_price_body' => 'Un vendedor le enviara su oferta dentro de las 24 horas.',
	'offer_body' => 'Haz una oferta y un vendedor se pondra en contacto.',
	'need_more_info' => 'Desea más información?',
	'need_more_info_body' => 'Contáctenos directamente. Abierto 6 días por semana. Hablamos tu idioma!',
	'request' => 'Solicitar',
	'view_all' => 'Ver todos',
	'name' => 'Nombre',
	'summary' => 'Resumen',
	'price_at_origin' => 'Precio en origen: (no incluye transporte)',
	'price_fixed_by_consultant' => 'Precio fijado por el consultor',
	'tags' => 'Etiquetas',
	'machines' => 'Máquinas',
	'accesories' => 'Accesorios',
	'both' => 'Ambos',
	'machines_accesories' => 'Máquinas ó accesorios?',
	'used_new' => 'Nuevos y usados?',
	'new' => 'Nuevos',
	'used' => 'Usados',
	'type' => 'Tipo',
	'view_more_details' => 'Ver más detalles',
	'units' => 'Unidades',
	'write_to_us' => 'Escríbenos',
	'whatsapp_more_info' => 'Quisiera más información sobre esta máquina',
	'multiple_prices' => 'Multiples precios',
	'attachments' => 'Archivos adjuntos',
	'expo_description_sales' => 'A lo largo de los años, hemos ayudado a nuestros clientes de habla hispana, a aumentar su productividad, a través de la búsqueda inteligente en Europa, de maquinaria nueva o usada, para entregársela directamente en el destino que se nos indique, sin que esto signifique ningún tiempo, ni esfuerzo extra para el cliente, pero que SI es sinónimo de optimización, calidad, mejor tecnología y precio competitivo.',
	'quote_joakim_sales' => 'Expoindustri es un jugador más en su equipo de trabajo. Existimos para negociar y gestionar tiempos, calidad y precios, a favor de su empresa',
];
