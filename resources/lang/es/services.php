<?php

return [

	'service_for_sellers' => 'Para ti que eres <b>FABRICANTE</b> o <b>DISTRIBUIDOR DE MÁQUINARIA</b>, tanto nuevas como usadas, tenemos diferentes soluciones',
	'service_for_buyers' => '<b>Para ti, que deseas comprar maquinaria desde Europa</b>, tanto nueva como usada, tenemos diferentes soluciones',

	'service_banner' => 'Anuncie su negocio y llegue a miles de clientes potenciales.',
	'service_search' => 'No encontró el equipo que requiere? Tiene una necesidad especial? Permítanos ayudarlo con su búsqueda.',
	'service_inspection' => 'Asegúrese que el equipo que desea esta en buenas condiciones con nuestros inspectores profesionales.',
	'service_support' => 'Solucionamos sus problemas logísticos para todo tipo de maquinaria.',
	'service_istore' => 'Cree su propia tienda en línea a través de ExpoIndustri y agrupe todos sus productos bajo su propia marca y logotipo .',

	'serv_banner_title' => 'Publicidad en banners',
	'serv_banner_body' => 'Comercialice su empresa a un nuevo mundo de clientes potenciales en EUROPA Y SUDAMERICA!',

	'serv_search_title' => 'Busqueda especializada',
	'serv_search_body' => 'No pudo encontrar la maquinaria que necesita en nuestro sitio web?  No se preocupe!! <br>El equipo de ExpoIndustri esta para brindarle apoyo con sus múltiples canales de búsqueda, para encontrar cualquier tipo de maquinaria en el mercado Europeo.',

	'serv_support_title' => 'Soporte logistico',
//	'serv_support_body' => 'El equipo de ExpoIndustri, le brinda el soporte logístico hasta el destino final de su elección, informandole periodicamente el estado de su envío, para todos los equipos adquiridos en nuestro sitio web.Pero también le solucionamos el soporte logístico para cualquier tipo de maquinaria o equipo con el que ya cuente.Nuestro equipo se encarga de recoger la maquinaria desde el origen, cargarlo en una de nuestras diferentes alternativas y hacerlo llegar hasta su destino final en Sudamerica.Vea nuestras alternativas de envio.',
	'serv_support_body' => 'El equipo de ExpoIndustri, le brinda el soporte logístico hasta el destino final de su elección, para todos los equipos adquiridos en nuestro sitio web.Pero también le solucionamos el soporte logístico para cualquier tipo de maquinaria o equipo con el que ya cuente.Nuestro equipo se encarga de recoger la maquinaria desde el origen, cargarlo en una de nuestras diferentes alternativas y hacerlo llegar hasta su destino final..Vea nuestras alternativas de envio.',

	'serv_support_contcomp' => 'Contenedor compartido',
	'serv_support_contcomp_msg' => '<div style=\'text-align:left; width:200px; padding:0px; font-size:13px;\'>
Esta es una gran solución si requiere bajar costos marítimos.<br><br>
Con esta opción usted estaría pagando por la combinación del metro cuadrado 
utilizado por su equipo y el peso del mismo.<br><br>
El tiempo de llegada a destino final es de 80-180 días aproximadamente.<br><br>
El transporte mas requerido por muchos.<br><br> 
<strong>CONSULTE PRECIOS YA</strong>!</div>',

	'serv_support_contex' => 'Contenedor exclusivo',
	'serv_support_contex_msg' => '<div style=\'text-align:left; width:200px; padding:0px; font-size:13px;\'>
Una excelente opción si requiere su equipo en su destino final en corto tiempo.<br> <br>
El tiempo de llegada a destino final es de 60 – 80 días aproximadamente<br><br> 
<strong>CONSULTE PRECIOS YA</strong>!</div>',

	'serv_support_contabi' => 'Contenedor abierto',
	'serv_support_contabi_msg' => '<div style=\'text-align:left; width:200px; padding:0px; font-size:13px;\'>
La única opción si se sobrepasa las dimensiones de un contenedor regular pero con la misma seguridad y garantía.<br><br> 
El tiempo de llegada a destino final es de 80-120 días aproximadamente.<br><br> 
<strong>CONSULTE PRECIOS YA</strong>!</div>',

	'serv_inspection_title' => 'Inspecciones tecnicas',
	'serv_inspection_body' => 'Nuestro equipo cuenta con inspectores profesionales y de igual forma trabajamos con una empresa externa con gran conocimiento en el rubro de las inspecciones de maquinaria de alto tonelaje de forma segura, imparcial y profesional. Por lo tanto tenemos la experiencia y conocimientos necesarios para realizar inspecciones  en general. Si usted esta por adquirir un equipo, dentro de nuestros anuncios o afuera, asegúrese que el equipo este en buenas condiciones. Déjenos el trabajo pesado a nosotros!.',

	'serv_istore_title' => 'Tienda online',
//	'serv_istore_body' => 'Acelere sus ventas asi también de a conocer su marca, reúna toda su gama de productos en el mismo lugar, como también expone su marca o compañía a un mundo completamente nuevo de clientes potenciales ABRA SU TIENDA YA!.',
	'serv_istore_body' => 'De a conocer su marca y toda la gama de productos con la que cuenta, todo concentrado en un solo lugar. ABRA SU TIENDA EN LÍNEA YA!',

	'serv_partner_title' => 'Encuentra un Socio en Latinoamerica',
	'service_partner' => 'O averigua sobre nuestros distintos servicios como el soporte logisitico entre otros.',
	'serv_partner_body' => 'Si deseas llegar a un nuevo mercado con gran potencial como Latinoamérica y necesitas un socio, un representante, un taller autorizado que trabaje con tus productos , tal vez  solo requieres de soporte logístico para hacer llegar tus productos. Sea cual sea tu necesidad, contamos con una amplia red de negocios y contactos para ayudarte. Contáctanos y reserva una cita para que veamos las mejores alternativas para tu empresa.',

	'serv_buyonline_title' => 'Compra maquinaria online, fácil y seguro',
	'service_buyonline' => 'Directo desde Europa, a través de nuestro avanzado portal web',
	'serv_buyonline_body' => 'Accede a una gran variedad de maquinaria industrial, tanto nueva como usada, directamente de Europa para ti. Fácil y seguro.  Registrate, elige y haz tu pedido, nosotros nos encargamos del resto. Así de sencillo!',
];

?>