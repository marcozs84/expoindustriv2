<?php

return [

	'q_1' => '1. ¿Que es Expoindustri?',
	'a_1' => 'Expoindustri es la combinación de un avanzado portal web y un experimentado equipo de trabajo, que tiene como objetivo el simplificar la venta de maquinaria industrial, nueva o usada, desde Europa hasta Latinoamérica.',

	'q_2' => '2. ¿Como funciona el proceso de vender maquinaria a Latinoamérica a través de Expoindustri?',
	'a_2' => 'Las empresas en Europa que venden máquinas industriales como, tornos, fresas, guillotinas, soldadoras, taladros, maquinas CNC, plegadoras, compresores, excavadoras, generadores, grúas, palas frontales y camiones entre otros, pueden publicar y promocionar sus productos en <a href="http://www.expoindustri.com" target="_blank">www.expoindustri.com</a>. El cliente en Latinoamérica compra a través de nuestra pagina web y nosotros arreglamos el resto. Así de fácil.  
			<br><br>
			Expoindustri se hace cargo de todo el proceso, recojo de la maquina en el lugar del vendedor europeo, el carguío en los contenedores y envío hasta el destino final en Latinoamérica, además de preparar toda la documentación de exportación necesaria y gestionar el cobro y los pagos respectivos. ',

	'q_3' => '3. ¿Que empresa está a cargo de expoindustri.com? ',
	'a_3' => 'Expoindustri Sweden AB es el dueño del nombre y todos los derechos de <a href="http://www.expoindustri.com" target="_blank">www.expoindustri.com</a>, empresa registrada en Suecia y con número de organización 559174-5194.',

	'q_4' => '4. ¿Como me puedo poner en contacto con ustedes? ¿Dónde están ubicadas sus oficinas?',
	'a_4' => 'La casa matriz se encuentra ubicada en Jönköping, Suecia, además contamos con oficinas en Iquique, Chile. Estamos pronto a abrir nuestras sucursales en Santa Cruz, Bolivia y Tacna, Peru. 
			<br><br>
			Puede ponerse en contacto con nosotros a través de nuestro formulario de contacto o a las siguientes direcciones:
			<table style="width:100%;">
				<tr>
					<td style="width:50%;">
						<b>Oficina en Suecia</b><br>
						<b>Telf:</b> +46 72 3933 700<br>
						<b>Dirección:</b> Jönköpingsvägen 27<br>
						56161 Tenhult<br>
						<b>Email:</b> info@expoindustri.com
					</td>
					<td style="width:50%;">
						<b>Oficina en Chile</b><br>
						<b>Telf:</b> +56 57 2473844​<br>
						<b>Dirección:</b> Oficina Mapocho, Mz. E, St. 48 D <br>
						Zofri - Iquique <br>
						<b>Email:</b> info@expoindustri.com
					</td>
				</tr>
			</table>

',

	'q_5' => '5. ¿Cuánto cuesta publicar un anuncio? ¿Existen diferentes planes? ',
	'a_5' => '
			Contamos con distintos planes que se adecúan perfectamente a los diferentes tipos de clientes:
			<br><br>
			<b>Basico:</b> Con este plan puede cargar el anuncio de una máquina, este anuncio estará disponible en Latinoamérica durante 180 días. El precio es de 360 sek antes de impuestos por anuncio.
			<br><br>
			<b>Business:</b> Este es un buen plan para aquellas empresas que disponen de un buen número de maquinaria para anunciar.  Con este paquete el vendedor puede anunciar 30 máquinas durante un año.
			<br><br>
			Además, al adquirir este paquete nuestro equipo de trabajo en Latinoamérica promociona sus máquinas de forma más específica para mejorar el tiempo de venta y al mismo tiempo, el vendedor tiene la oportunidad de crear su tienda online dentro de expoindustri.com, lo que le permite crear un enlace a la página web de su empresa(*), Mostrar el nombre de su empresa, el logo, dirección y toda su información de contacto. 
			<br><br>
			Pris 6.900 sek antes de impuestos por paquete.
			<br><br>
			<b>Plus:</b> Este plan se adecúa perfectamente a aquellas empresas que tienen una gran cantidad de productos que ofrecer. Este paquete incluye carga ilimitada de anuncios de maquinarias durante un año.
			<br><br>
			Además, al adquirir este paquete nuestro equipo de trabajo en Latinoamérica promociona sus máquinas de forma más específica para mejorar el tiempo de venta y al mismo tiempo, el vendedor tiene la oportunidad de crear su tienda online dentro de expoindustri.com, lo que le permite crear un enlace a la página web de su empresa(*), Mostrar el nombre de su empresa, el logo, dirección y toda su información de contacto.
			<br><br>
			Por último, con este plan su empresa tiene la opción de subir un banner promocional en expoiundustri.com
			<br><br>
			Pris 12.900 sek antes de impuestos por paquete.
			<br><br>
			<b>(*)</b> Si además, desea incrementar aún más su llegada al mercado hispanohablante, podemos traducir su página web al español. <a href="https://www.expoindustri.com/contact">Contáctenos para más información</a>. 


	',

	'q_6' => '6. ¿Quién es el responsable de pagarme si mi máquina logra venderse?',
	'a_6' => 'Nosotros siempre nos ponemos en contacto con el vendedor de la máquina para confirmar que la misma esta todavía disponible.
			<br><br>
			El comprador en Latinoamérica paga a Expoindustri, quien, a la vez, es el responsable por pagar la suma total al vendedor de la máquina en Europa. El anunciante de la máquina en Europa emite una factura a nombre de Expoindustri Sweden AB.',

	'q_7' => '7. ¿Por qué solicitan un precio ultimo de venta al momento de subir el anuncio? ',
	'a_7' => 'Es bueno incluir ese precio ultimo de venta para que nuestro equipo de trabajo en Latinoamérica pueda negociar con los posibles compradores y se tenga una mayor oportunidad de concretar un negocio. Ese precio ultimo de venta SÓLO puede ser visto por el equipo de trabajo de Expoindustri y NUNCA será mostrado al comprador en Latinoamérica. Colocar este dato al cargar el anuncio es opcional.',

	'q_8' => '8. ¿Quién está a cargo del recojo, carguío y envío de la maquinaria a Latinoamérica? ',
	'a_8' => 'Expoindustri Sweden AB es siempre quien se hace cargo del recojo de la máquina, el cargado de la misma en el contenedor y el envío hasta el destino final en Latinoamérica, además de toda la documentación de exportación en su totalidad.',

	'q_9' => '9. ¿Que significa tener una tienda online en www.expoindustri.com? ',
	'a_9' => 'Las tiendas online en expoindustri.com permiten al vendedor de maquinaria agrupar todos sus anuncios en una misma cuenta, misma que a la vez le da la oportunidad de promocionar su empresa, entre otras cosas, creando un enlace a su página web(*), además de mostrar toda la información relevante, como el nombre, el logo, dirección etc. 
			<br><br>	
			<b>(*)</b> Si además, desea incrementar aún más su llegada al mercado hispanohablante, podemos traducir su página web al español. <a href="https://www.expoindustri.com/contact">Contáctenos para más información</a>. 
	',

	'q_10' => '10. ¿Que tipo de maquinaria es la que puede anunciar mi empresa? ',
	'a_10' => 'Su empresa, ya sea usted fabricante, representante o revendedor de maquinaria puede anunciar maquinaria tanto nueva como usada en las siguientes categorías: transporte, agricultura, equipos, montacargas, industrial, otros.
			<br><br>
			Si cree que nuestras categorías no se adecúan al tipo de maquinaria que usted vende, por favor no dude en contactarse con nosotros para que podamos asesorarlo. ',

	'q_11' => '11. ¿Puedo anunciar una máquina que no está en estado operativo? ',
	'a_11' => 'Si, puede anunciar una máquina que no está en estado operativo, siempre que esto este <span style="text-decoration:underline; font-weight:bold;">claramente especificado</span> en su anuncio y cuente con un detalle de cuáles son los inconvenientes de la maquinaria. 
			<br><br>
			Si el comprador latinoamericano nos lo solicita, Expoindustri Sweden AB incluso puede realizar una inspección técnica de la máquina in situ. Esta inspección no tiene ningún costo extra para el anunciante de la máquina. ',

	'q_12' => '12. ¿Pueden realizar una promoción especial de mi marca/empresa en Latinoamérica? ',
	'a_12' => 'Sí, podemos hacerlo. No dude en contactarnos, para explicarnos de que forma especial desearía que lo ayudemos. ',

];
