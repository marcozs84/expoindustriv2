<?php

return [
	'title' => '¡IMPORTANTE! USTED COMO CLIENTE',
	'slogan' => 'En ExpoIndustri.com trabajamos constantemente para garantizar, que usted como cliente, pueda realizar negocios seguros y protegidos.',
	'buyer_1' => 'Nuestro personal realiza controles continuos en anuncios y usuarios.',
	'buyer_2' => 'Revisamos cuidadosamente todos los anuncios antes de que sean aprobados y publicados en ExpoIndustri.com.',
	'buyer_3' => 'Ofrecemos opciones seguras para el pago, tales como, tarjeta, Paypal o transferencia bancaria.',
	'buyer_4' => 'Tenemos sucursales en los diferentes países en los que ExpoIndustri.com ofrece sus servicios.',
	'buyer_5' => 'Con el fin de reducir el fraude, ExpoIndustri.com NUNCA le pedirá que pague en efectivo o le solicitará un anticipo. Por lo tanto, SIEMPRE debe pagar con los métodos de pago seleccionados que ofrece ExpoIndustri.com.',
	'buyer_6' => 'Adaptamos las reglas de publicidad de acuerdo al cambio de las condiciones y las leyes vigentes en los países donde trabajamos, y cómo queremos que ExpoIndustri.com sea percibido.',
	'buyer_7' => 'ExpoIndustri.com trabaja constantemente con cursos de capacitación sobre carga y seguridad para ofrecer a nuestros clientes una entrega sin daño alguno.',
	'buyer_8' => 'Cuando el cliente envía un informe de interés para una máquina industrial / equipo / u otro / entonces, ExpoIndustri.com comprueba cuidadosamente la existencia del producto seleccionado antes de enviar la factura. Por lo tanto, puede simplemente sentarse y sentirse seguro de comprar en ExpoIndustri.com.',
	'buyer_9' => 'Expoindustri.com ofrece revisión técnica con profesionales altamente capacitados para darle mayor seguridad al querer adquirir un equipo.',
];

?>