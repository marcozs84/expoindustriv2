<?php

return [

	'q_1' => '1. ¿Qué empresa está a cargo de expoindustri.com?',
	'a_1' => 'Todo los derechos y nombre de expoindustri.com son propiedad de Expoindustri Sweden AB, registrada en Suecia con número de organización 559174-5194.',
	'q_2' => '2. ¿Dónde están ubicadas las oficinas?',
	'a_2' => 'La oficina central está ubicada en Jönköping Suecia, también contamos con oficina en Iquique Chile. Pronto estaremos inaugurando oficina en Santa cruz Bolivia y Tacna Peru.   	Ir a nuestras oficinas',
	'q_3' => '3. ¿Cómo puedo saber que las máquinas están operativas?',
	'a_3' => 'El vendedor es quien indica el estado de la maquinaria en su anuncio, y existen cinco opciones, Nueva, Operativa, Operativa con defectos No Operativa y No sé. En el caso de que la maquinaria se encuentre Operativa con defectos, No operativa o No sé, recomendaríamos que el comprador solicitase una revisión técnica, Expoindustri.com utiliza una empresa externa con años de experiencia en revisiones técnicas de maquinarias en general, para realizar el informe técnico de dicha maquinaria. Este servicio adicional de Revisión Técnica tiene un costo extra, que se muestra en la cotización solicitada por el comprador.',


	'q_4' => '4. ¿Tengo que pagar el costo total de la maquinaria antes de recibir la revisión técnica?',
	'a_4' => 'No, si usted en su cotización solicitó revisión técnica, al momento de dar “click” en pagar, solo deberá cancelar inicialmente el costo de dicha revisión. Luego de recibir el informe técnico solicitado, usted podrá elegir entre continuar con la compra de la maquinaria o eliminarla de su lista de compras.',
	'q_5' => '5. ¿Cuánto cuesta y en qué tiempo me envían el informe de la revisión técnica?',
	'a_5' => 'El costo de la revisión varía en función al tipo de maquinaria a inspeccionar. Al incluir usted la opción de REVISION TECNICA en su cotización, obtendrá automáticamente el valor a pagar.
<br><br>El informe será enviando a su correo electrónico entre 2 y 5 días hábiles.',
	'q_6' => '6. ¿Puedo rentar maquinaria en ExpoIndustri.com?',
	'a_6' => 'No, no rentamos maquinarias.
<br><br>Nosotros le recomendamos que solicite información en su banco de la posibilidad de un leasing (o crédito si su proyecto dura más de 6 meses)',
	'q_7' => '7. ¿Dónde están ubicadas las maquinarias?',
	'a_7' => 'Las maquinarias se encuentran ubicadas en diferentes lugares en Europa, Pero usted no tiene de que preocuparse, ya que ExpoIndustri.com se encarga al 100% del proceso de transporte y la documentación necesaria para que la maquinaria llegue directamente al destino que usted eligió.',
	'q_8' => '8. ¿El precio indicado en la página incluye IVA?',
	'a_8' => 'No, el precio indicado en la página es excluido de IVA y de costos de nacionalización. Puede ponerse en contacto con nosotros si desea saber más detalles.',
	'q_9' => '9. ¿Cómo puedo saber si el pago es seguro y recibo mi maquinaria?',
	'a_9' => 'Cada pago es respaldado por un contrato, de acuerdo a nuestros términos legales y condiciones. Una vez que envía orden de compra vía nuestra página, nosotros controlamos que la maquinaria siga disponible, una vez controlada y confirmada la existencia de la misma, le enviaremos una pre-factura y los pasos a seguir para proceder con el pago seguro.',
	'q_10' => '10. ¿Cuánto tiempo tengo para pagar la factura?',
	'a_10' => 'Una vez que ExpoIndustri Sweden AB vía expoindustri.com haya enviado la pre-factura, usted tiene 24 horas para hacer el deposito. (ver opciones de pago en la siguiente pregunta).',
	'q_11' => '11. ¿Cuales son las formas de pago?',
	'a_11' => 'ExpoIndustri Sweden AB trabaja con los bancos más prestigiados de su país, así no tendrá que pagar extras para el envió al extranjero.
<ul>
<li>Transferencia Bancaria</li>
<li>Tarjeta de crédito o debito</li>
<li>Paypall</li>
</ul>
',


	'q_12' => '12. ¿Qué pasa si no pago antes de las 24 horas y pago después?',
	'a_12' => 'Si usted no paga antes de las 24 horas, ExpoIndustri Sweden AB  no podrá garantizar la existencia de la maquinaria, puede leer más en nuestros términos legales y condiciones.',
	'q_13' => '13. ¿Puedo pagar en cuotas?',
	'a_13' => 'No, no se puede pagar en cuotas, vea más nuestros términos legales y condiciones.',
	'q_14' => '14. ¿Puedo pagar en su oficina local?',
	'a_14' => 'No, no puede pagar en nuestras oficinas locales, pero el personal en nuestras oficinas le brindara con todo gusto, el apoyo con la información necesaria para una compra segura.',
	'q_15' => '15. ¿Una vez cancelado la maquinaria que seguridad tengo?',
	'a_15' => 'Una vez que nosotros verificamos el pago efectuado, le enviaremos su factura con la información de dicha máquina, el pago que realizo y el tiempo de entrega. Además, nuestro personal calificado ya sea en nuestra sucursal o en oficina central le brindara el apoyo y la información que usted está requiriendo.',

	'q_16' => '16. ¿ExpoIndustri ofrece financiamiento?',
	'a_16' => 'No ofrecemos financiamiento.',
	'q_17' => '17. ¿Por qué no puedo ver la ubicación de la máquina?',
	'a_17' => 'Como somos una plataforma online, no tenemos un local donde se puedan visitar los equipos/maquinarias. Los equipos/maquinarias que nosotros ofrecemos están localizadas en diferentes partes de Europa. Por la privacidad de los anunciantes no publicamos la ubicación, pero se puede comunicar con uno de nuestros asesores de venta para mayor información.',

	'q_18' => '18. ¿Cuánto tiempo tarda en llegar la maquina una vez pagada la maquinaria?',
	'a_18' => 'Nosotros le ofrecemos dos formas de envió, envió compartido o envió preferencial, si usted eligió envió compartido el plazo de entrega es 70 – 180 días y si usted eligió envió preferencial el plazo de entrega es de 65 – 100 días, contando desde el día que usted hizo el pago. Lea más en nuestros términos legales y condiciones',
	'q_19' => '19. ¿Puedo confiar en ExpoIndustri.com?',
	'a_19' => 'Por supuesto que sí. El equipo de ExpoIndustri Sweden AB tiene más de 10 años de experiencia en la importación de maquinarias a Sudamérica, que garantiza la seguridad de su dinero y el envío de la maquinaria, con el mayor cuidado hasta su destino final.',
	'q_20' => '20. ¿La máquina en la que estoy interesado tiene accesorios, también me llegan?',
	'a_20' => 'Usted de antemano podrá ver en el anuncio del vendedor los accesorios con los que cuenta dicha maquinaria. Además, en su factura especifica exactamente lo que usted compro, incluyendo los accesorios en los casos que aplique.',





];
