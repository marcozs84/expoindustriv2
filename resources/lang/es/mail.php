<?php

return [
	'preo_subject' => 'PRE-ORDEN DE COMPRA',
	'preo_greetings' => 'Estimado(a) :nombre_completo,
	<br>
	Muchas gracias por confiar en nosotros, hemos recibido correctamente su <b>orden de compra</b> y uno de nuestros consultores se pondrá en contacto dentro de las siguientes 24 Hrs., a continuación una copia de su solicitud:',
	'preo_full_gallery' => 'Ver galería completa',
	'machine_detail' => 'Detalles de la máquina',
	'go_original_ad' => 'Ir a la publicación original',
	'machine_price' => 'Precio maquinaria',
	'additional_services' => 'Servicios adicionales',
	'tech_report' => 'Informe técnico',
	'paid_on_request' => 'Ya se pagó al registrar la orden',
	'machine_insurance' => 'Seguro de maquinaria',
	'transport' => 'Transporte',
	'destination' => 'Destino',
	'important_note' => 'Nota importante',
	'price_in_final_destiny' => 'Precio en destino final',
	'report_order' => 'Si usted no ha solicitado esta información puede reportar este mensaje haciendo click en el siguiente enlace:',
];

?>